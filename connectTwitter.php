<?php

/*Including required twitter files*/
include(__DIR__.'/Configuration.php');
include(__DIR__.'/cms/includes/Database.class.php');
include(__DIR__.'/cms/includes/Twitter.class.php');
include(__DIR__.'/cms/includes/twitter/twitteroauth.php');
include(__DIR__.'/cms/includes/twitter/secret2.php');

$oTwitter = new Twitter;
$aValues = array ();
if (!isset ($_GET ['oauth_token']))
{
        $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);

        /* Get temporary credentials. */
        $request_token = $connection->getRequestToken(OAUTH_CALLBACK);
        if (!isset ($request_token ['Failed to validate oauth signature and token']))
        {
        		$aValues ['oauth_token'] = $request_token['oauth_token'];
        		$aValues ['oauth_token_secret'] = $request_token['oauth_token_secret'];

        		$oTwitter -> checkConn ($aValues);

                $token = $request_token['oauth_token'];
                $url = $connection->getAuthorizeURL($token);
                header('Location: ' . $url);
        }
        else 
        { 
            echo 'Application id or secret code is not valid';
        	return false;
        }
}
else 
{
		$twitterConn = $oTwitter -> getTwitterConn ();
		echo 'you can receive tweets now';
}


?>