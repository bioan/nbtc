<?php
/*Including required twitter files*/
include(__DIR__.'/Configuration.php');
include(__DIR__.'/cms/includes/Database.class.php');
include(__DIR__.'/cms/includes/Language.class.php');
include(__DIR__.'/cms/includes/Twitter.class.php');
include(__DIR__.'/cms/includes/twitter/twitteroauth.php');
include(__DIR__.'/cms/includes/twitter/secret2.php');

$oTwitter = new Twitter;
$twitterConn = $oTwitter -> getTwitterConn ();

if (!empty ($twitterConn))
{
        /* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
        $twitterConn = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $twitterConn['oauth_token'], $twitterConn['oauth_token_secret']);

        $method = 'https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=' . USER_ID . '&count=' . NR_OF_TWEETS;
        $status = $twitterConn -> get($method);
        
        if (!empty ($status) && !isset ($status -> errors))
        {
                file_put_contents (__DIR__ . '/twitter.cache', json_encode($status));
        }
        elseif (empty ($status) || isset ($status -> errors))
        {
                $headers = 'From: noreply@inforitus.nl' . "\r\n" .
                'Reply-To: noreply@inforitus.nl' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
                mail ('andrei@inforitus.nl,info@inforitus.nl', Configuration::WEBSITE_NAME . ' Twitter broke', '', $headers);
        }  
}


?>