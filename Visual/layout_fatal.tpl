<!doctype html>
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 ++                                                               ++
 ++     @copyright Copyright (c) 2008-2010 Inforitus V.O.F.       ++
 ++     @author Andrei Dragos <andrei@inforitus.nl>               ++
 ++                                                               ++
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --> 
<html lang="nl">
  <head>
    <meta charset="utf-8" />
    <meta name="robots" content="noindex" />
    <title>
      Error
    </title>
    <link rel="stylesheet" media="screen" href="/Visual/layout.css" />
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  </head>
  <body>
    <header>
      <!--<a href="/index.html"><img src="/images/header.jpg" alt="Stichting Hollandse Haringpartij" /></a>-->
    </header>
    <div id="container">
      <!--<img id="hp-titel" src="/images/title.jpg" alt="AGV Waterinnovatieprijs 2011" />-->
      <section id="content" class="stretched">
        <h1>Foutmelding</h1>
        <p>
          Tot onze spijt heeft er zich een storing voorgedaan op deze website. Onze technici zijn van dit probleem op de hoogte gesteld; er zal hier zo snel mogelijk naar gekeken worden.
        </p>
        <p class="error">
          {$message}
        </p>
      </section>
      <footer class="stretched">
        Heeft u vragen of opmerkingen? Neem dan contact op met<br />
        Inforitus: 020-2050788 via <a href="mailto:info@inforitus.nl">info@inforitus.nl</a>
      </footer>
    </div>
  </body>
</html>