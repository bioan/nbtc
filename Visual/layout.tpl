<!doctype html>
<html lang="en">
    <head>
        <title>{$website_name} - {$page_title}</title>
        <meta charset="utf-8" />
        <meta name="description" content="{$page_description}" />
        {foreach from=$stylesheets item=file}
        <link rel="stylesheet" href="{$file}" type="text/css" media="screen" />
        {/foreach}    
        <!--[if lt IE 9]>
        <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        {foreach from=$javascripts item=file}
        <script src="{$file}"></script> 
        {/foreach}  
    </head>
    <body class="{$device_type} {$page_name}"> 
        <div id="container">
            {include file=$content}        
        </div> 
        <footer>
        </footer>
    <script type="text/javascript"> Cufon.now(); </script>
    </body>
</html>