<br><br>
<div id="mobile_container">
	<img class="cycle" src="/images/bicicle.png" width="328" height="372" alt="bycicle" />

	<div class="timer">
		<span class="seconds"></span>
	</div>

	<div id="variants"></div>

	<div id="correctiveness"></div>

	<div id="navigate">
		<span class="go-cycling">Go cycling >></span>
	</div>

	<a class="reset" href="/index.html">RESET</a>
	<p class="testy"></p>
</div>