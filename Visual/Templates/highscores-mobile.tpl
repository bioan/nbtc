<br><br>
<h1>Today scores:</h1>
<br><br>

{foreach from=$highscores item=cyclist key=k}
{if $k < 10}
<div class="score">
	{$k+1}.&nbsp;<span>{$cyclist.name}</span><span>{$cyclist.points}/8</span>
</div>
{/if}
{/foreach}



<br><br><br>
<a href="/index.html">Back</a>
<br><br>

{if isset($error_text)}
	{$error_text}
{else}
	<a href="/start.html">Start game</a>
{/if}