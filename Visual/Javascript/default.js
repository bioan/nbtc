function trackOutboundLink(link, category, action) {
    try {
        _gaq.push(['_trackEvent', category , action]);
    } catch(err){}
}

var nr_of_questions = 8;

$(document).ready(function() {
	var timeoutId;
	var question_timer;
	var intervalID;
	if ($("body").hasClass("mobile"))
	{
		if ($("body").hasClass("cycling")){
			var q_nr = 1;

			function waitForQuestion(question_nr){
				//$(".testy").append("q_nr:"+question_nr+"<br>");
				$.ajax({
					url: "../../Announcers/ipad_announcer.php",
					type: "POST",
					data :{waitForQuestionn : question_nr}
				}).done(function( msg ) {
					if (msg != "empty"){
						clearTimeout(timeoutId);

						//$(".testy").append("optionsArray:"+msg+"<br>");
						$("#variants").html("");
						$(".cycle").css('display','none');
						$(".timer").css('display','block');
						$(".seconds").html("10");

						var obj = jQuery.parseJSON(msg);
						for (var i in obj) {
							letter = "A";
							if (i==1)
								letter = "B";
							if (i==2)
								letter = "C";
							if (i==3)
								letter = "D";

							$("#variants").append("<div class=\"variant\"><span>"+letter+".</span><span class=\"answer_text\">"+obj[i]+"</span></div>");
						}
						$("#variants").append("<div class=\"variant falser\"><span class=\"answer_text\">-23223</span></div>");

						$(".timer").css('color','white');
						
						question_timer = 1000;
						intervalID = setInterval(function(){
							question_timer = question_timer - 10;
							seconds = question_timer/100;

							if (question_timer < 301)
							{
								$(".timer").css('color','red');
							}


							//$(".seconds").html(seconds + (question_timer % 100 == 0)?".0":"");
							$(".seconds").html(seconds);
							
							if (question_timer < 1)
							{
								$( ".variant.falser" ).trigger( "click" );
								clearInterval(intervalID);
							}
						},100);

						$(".variant").on('click',function(){
							$(".timer").css('display','none');

							desc_timer = 1000 - question_timer;
							answer = $(this).find(".answer_text").html();
							$.ajax({
								url: "../../Announcers/ipad_announcer.php",
								type: "POST",
								data :{registerAnswer : question_nr, ans: answer, timer: desc_timer}
							}).done(function( msg2 ) {
								//$(".testy").append("correctiveness:"+msg2+"<br>");
								$("#variants").html("");

								if (msg2==1){
									correctiveness_text = "Good answer :)";
								}
								else{
									correctiveness_text = "Wrong answer :(";
								}

								if (q_nr == nr_of_questions){
									$("#navigate").fadeTo( "fast" , 0, function() {});

									$.ajax({
										url: "../../Announcers/ipad_announcer.php",
										type: "POST",
										data :{showHighscore : 1}
									}).done(function( score ) {
										//$(".testy").append("score:"+score+"<br>");

										$("#mobile_container").html("<div class=\"highscore\"><p>Your highscore is: "+score+"</p><a href=\"/index.html\">Done</a></div>");
									});
								}
								else{
									$("#correctiveness").html(correctiveness_text);
									$("#navigate").fadeTo( "fast" , 1, function() {});

								}
						  	});
						})
					}
					else{
						timeoutId = setTimeout(function(){waitForQuestion(q_nr)},1000);
					}
			  	});
			}

			$("#navigate .go-cycling").on('click',function(){
				q_nr = q_nr + 1;

				$.ajax({
					url: "../../Announcers/ipad_announcer.php",
					type: "POST",
					data :{showNextVideo:q_nr}
				}).done(function( msg3 ) {
					//$(".testy").append("setsvideoto:"+q_nr+"<br>");

					$("#correctiveness").html("");
					$("#navigate").css('display','none');
					$(".cycle").css('display','block');
					
					waitForQuestion(q_nr);

				});
			})

			waitForQuestion(q_nr);

			$(".reset").on('click',function(ev){
				ev.preventDefault();
				q_nr = 1;
				waitForQuestion(q_nr);

				$.ajax({
					url: "../../Announcers/ipad_announcer.php",
					type: "POST",
					data :{reset:1}
				}).done(function( msg4 ) {
					window.location = "http://nbtc.maarwaar.nl/index.html";
				});
			})

		}
	}


	else if ($("body").hasClass("pc"))
	{	
		var v_nr = 1;

		function checkCyclingHasStarted(video_nr){
			$.ajax({
				url: "../../Announcers/pc_announcer.php",
				type: "POST",
				data :{noticeWhenSomeoneStartsCycling : video_nr}
			}).done(function( msg ) {
				if (msg != "no"){
					$(".question_container").css("display","none");

					if (video_nr != 1){
						prev_video_nr = video_nr - 1;
						$("#video"+prev_video_nr).css('display','none');
					}

					$("#video"+video_nr).css('display','block');
					$("#video"+video_nr).get(0).play();	
					$("#cyclist_id").html(msg);
					$(".info_text").html('');

					$("#video"+video_nr).get(0).addEventListener('ended',showQuestionOnScreen,false);
				}
				else{
					setTimeout(function(){checkCyclingHasStarted(video_nr)},1000);
				}
		  	});
		}
		checkCyclingHasStarted(v_nr);

    	function showQuestionOnScreen(){
    		$.ajax({
				url: "../../Announcers/pc_announcer.php",
				type: "POST",
				data :{showQuestion: v_nr}
			}).done(function( msg ) {

				var question = jQuery.parseJSON(msg);
				
				$(".question_container").css("display","block");

				for (var i=0;i<question["question_variants"].length;i++)
				{
					letter = "A";
					if (i==1)
						letter = "B";
					if (i==2)
						letter = "C";
					if (i==3)
						letter = "D";

					$(".question_variants").append("<li class=\"variant\"><span>"+letter+"</span> <span class=\"answer_text\">"+question["question_variants"][i]+"</span></li>");
				}

				//setez textul cu textul imaginii (msg)
				$(".question_text").html(question["question_text"]);

				v_nr = v_nr + 1;
				if (v_nr > nr_of_questions){
					//$(".info_text").html('Waiting for cycling to start');
				}
				else{
					checkCyclingHasStarted(v_nr);	
				}
				
		  	});
    	}


		
	}

	

})
