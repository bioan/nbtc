<?php
#!/usr/local/bin/php
include(__DIR__.'/Configuration.php');   
include(__DIR__.'/cms/includes/Database.class.php');
include(__DIR__.'/cms/includes/Language.class.php');
include(__DIR__.'/cms/includes/OAuth/oauth.php');
include(__DIR__.'/cms/includes/Twitter.class.php');
include(__DIR__.'/cms/includes/twitter/twitteroauth.php');
include(__DIR__.'/cms/includes/twitter/secret.php');
include(__DIR__.'/cms/includes/News.class.php');
$oNews      = new News ();
$oTwitter  = new Twitter ();

$today_date = date("Y-m-d");
$aNews = $oNews ->getNews(true, 0, '', 0, '', 'twitter', 'asc', 'default', 0, $today_date, true);

/*Check if the connection with linkedin is established*/
$appTwitterData = $oTwitter ->getApplication();
$userId = $aNews [0]['twitter'];
$connection = '';
$nrConnection = 0;
$okTwitter = false;
foreach ($aNews as $k => $item)
{     
        $ok = false;
        $actual_time = time();
        while ($ok == false)
        {       
                /*Check if the connection with twitter is established*/

                if (!empty ($appTwitterData))
                {
                        if ($userId != $item ['twitter'] || $nrConnection == 0)
                        {
                                $userId = $item ['twitter'];
                                $aTwitterUser = $oTwitter ->getTwitterUser($item ['twitter']);
                                if (!empty ($aTwitterUser) && $appTwitterData ['correct'] == 1)
                                {
                                        $connection = new TwitterOAuth($appTwitterData ['consumer_key'], $appTwitterData ['consumer_secret'], $aTwitterUser['oauth_token'], $aTwitterUser['oauth_token_secret']);
                                        $method = 'account/verify_credentials';
                                        $status = $connection->get($method);

                                        if (isset ($status -> error)) 
                                        {
                                                $okTwitter = false;
                                        }
                                        else 
                                        {
                                                $okTwitter = true;
                                        }
                                }
                                $nrConnection = 1;
                        }
                        
                        if ($okTwitter)
                        {
                                $newtime = time();
                                if ($newtime > $actual_time + 1)  
                                {    
                                       $parameters = array('status' => $item ['twitter_text']);
                                       $status = $connection->post('statuses/update', $parameters);
                                       $ok = true;
                                }
                        }
                        else 
                        {
                                $ok = true;
                        }
                }
        }
}   
?>