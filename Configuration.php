<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id: Configuration.php
 */

/**
 * All configuration used by the website will be stored in this
 * class, as constants. This makes it fairly easy to transfer the system to another
 * server, if that might ever be nessesary.
 * 
 * @package Configuration
 */  

class Configuration
{
        /**
         * This constant defines the name of the website that we're dealing with. It will
         * be used as the default page-title, session name and whatever more.
         */
        
        const   WEBSITE_NAME            = 'Site';           
        
        /**
         * This constant defines if in case of an a site error should be send an email to the admin address.   
         */
         
        const   SAVE_ERROR             =  true;   
             
        /**
         * This constant defines the email of the admin where the error of site will be sent.   
         */
         
        const   ADMIN_EMAIL             = 'error@inforitus.nl';           
          
        
        /**
         * Here is the database data for the current site 
         * The following four defines indicate where the database should be connecting
         * to, namely the hostname, username, password and database name. (eppc_digitaal database)
         */
        
        const   DATABASE_HOSTNAME       = 'localhost';
        const   DATABASE_USERNAME       = 'ionut_nbtc';
        const   DATABASE_PASSWORD       = 'kMp4emqH';
        const   DATABASE_DATABASE       = 'ionut_nbtc';
        
        /**
         * Obviously we should be able to connect to the memory cache (Memcached) daemon
         * as well. For that we need the server IP address and port number. We also define
         * a prefix for data stored by this website, to avoid collision with other sites.
         */
        
        const   MEMCACHED_SERVER_IP     = '127.0.0.1';
        const   MEMCACHED_SERVER_PORT   = 34555; // 11211 - for Inforitus-1
        const   MEMCACHED_DATA_PREFIX   = 'wip_';

};

?>