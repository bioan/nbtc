<?php 
define ('INFORITUS_BASE',     __DIR__);

require __DIR__ . '/Configuration.php';
require __DIR__ . '/cms/includes/Database.class.php';

if (isset ($_POST ['id']) && isset ($_POST ['rating']) && 
is_numeric ($_POST ['id']) && is_numeric ($_POST ['rating']) &&
strpos ($_SERVER['HTTP_REFERER'], $_SERVER ['SERVER_NAME']) !== false)
{
        Database :: getInstance () -> query ("
                INSERT INTO
                    comments_rates
                (comment_id, note)
                VALUES
                (" . $_POST ['id'] . ", " . $_POST ['rating'] . ")
        ");
        
        $pResult = Database :: getInstance () -> query ("
                SELECT 
                    *
                FROM
                    comments_rates
                WHERE
                    comment_id = " . $_POST ['id'] . "
        ");
        
        $iTotal = 0;
        $iCount = 0;
        
        if ($pResult -> num_rows > 0)
        {         
                while ($aRow = $pResult -> fetch_assoc ()) 
                {          
                        $iTotal += $aRow ['note'];
                        $iCount++;
                }
        }
        
        Database :: getInstance () -> query ("
                UPDATE
                    comments
                SET 
                    rate = " . round ($iTotal/$iCount, 1) . "
                WHERE
                    id = " . $_POST ['id'] . "
        ");
        
        echo round ($iTotal/$iCount, 1);
}
?>