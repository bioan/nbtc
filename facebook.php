<?php
#!/usr/local/bin/php

include(__DIR__.'/Configuration.php');   
include(__DIR__.'/cms/includes/Database.class.php');
include(__DIR__.'/cms/includes/Language.class.php');
include(__DIR__.'/cms/includes/facebook/secret.php');
include(__DIR__.'/cms/includes/Facebook.class.php');
include(__DIR__.'/cms/includes/News.class.php');

$url = "http://cms.cmsforlife.nl/news/";

$oNews      = new News ();
$today_date = date("Y-m-d");
$aNews = $oNews ->getNews(true, 0, '', 0, '', 'facebook', 'asc', 'default', 0, $today_date, false, true, false);
foreach ($aNews as $k => $item)
{
        if ($item ['date_start'] == $today_date && $item ['facebook'] != 1)
        {
                $ok = false;
                $actual_time = time();
                while ($ok == false)
                {
                     $newtime = time();
                     if ($newtime > $actual_time + 1)  
                     {
                            $aData = array (0 => 100003169641959, 1 => 1163236666);
                            $ch = curl_init ();
                            $link = $url.$item ['link'];
                            curl_setopt ($ch, CURLOPT_URL, "https://graph.facebook.com/" . $item ['publish_id'] . "/feed");
                            $data = array (
                                    'access_token' => $item ['publish_accessToken'], 
                                    'message' => substr (strip_tags($item ['description']), 0, 100),
                                    'picture' => $_SERVER['SERVER_NAME'].$item ['photos'][0]['image_name'],
                                    'link' => $link,
                                    'name' => $item ['title'],
                                    'description' => substr (strip_tags($item ['description'], 0), 100),
                                    //'privacy' => json_encode (array ('value' => 'CUSTOM', 'friends' => 'SELF')),
                                    'tags' => json_encode ($aData),
                                    'place' => 111777152182368,
                                    //'to' => json_encode (array ('data' => $aData)),
                                    //'message_tags' => json_encode (array ('data' => $aData)),
                                    'from' => json_encode (array ('name' => $aData [0]['name'], 'id' => $aData [0]['id']))
                            );

                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                            $result = curl_setopt ($ch, CURLOPT_POSTFIELDS, $data);

                            // grab URL and pass it to the browser
                            $result = curl_exec ($ch);
                            // close cURL resource, and free up system resources
                            curl_close ($ch);
                            $ok = true;
                     }
                }
        }
}
?>