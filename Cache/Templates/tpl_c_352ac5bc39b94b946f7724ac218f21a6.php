<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2011-01-13 15:58:41 CET */ ?><div class="page default">   
    <h2><?php echo $this->_vars['title']; ?></h2>
    <?php if (count((array)$this->_vars['subpages'])): foreach ((array)$this->_vars['subpages'] as $this->_vars['k'] => $this->_vars['item']): ?>
    <div class="item">
        <h2><?php echo $this->_vars['item']['name']; ?></h2>
        <div class="text">
            <?php echo $this->_vars['item']['content']; ?>    
        </div>
    </div>
    <?php endforeach; endif; ?>
</div>
     