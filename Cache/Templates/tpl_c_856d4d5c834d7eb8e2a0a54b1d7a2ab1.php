<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2011-01-03 11:05:13 CET */ ?>        <form enctype="multipart/form-data" action="" method="post" accept-charset="utf-8">
          <fieldset>
            <ol>
              <li>
                <em>Deelnemend bedrijf/organisatie</em>
              </li>
              <li<?php if ($this->_vars['error']['bedrijf_naam']): ?> class="form-error"<?php endif; ?>>
                <label for="bedrijf_naam">Naam bedrijf/organisatie</label>
                <input type="text" id="bedrijf_naam" name="bedrijf_naam" value="<?php echo $this->_vars['form']['bedrijf_naam']; ?>" />
              </li>
              <li<?php if ($this->_vars['error']['bedrijf_adres']): ?> class="form-error"<?php endif; ?>>
                <label for="bedrijf_adres">Postadres</label>
                <input type="text" id="bedrijf_adres" name="bedrijf_adres" value="<?php echo $this->_vars['form']['bedrijf_adres']; ?>" />
              </li>
              <li<?php if ($this->_vars['error']['bedrijf_pc'] || $this->_vars['error']['bedrijf_pcl']): ?> class="form-error"<?php endif; ?>>
                <label for="bedrijf_pc">Postcode</label>
                <input type="text" name="bedrijf_pc" id="bedrijf_pc" value="<?php echo $this->_vars['form']['bedrijf_pc']; ?>" size="5" maxlength="5" />
                <input type="text" name="bedrijf_pcl" id="bedrijf_pcl" value="<?php echo $this->_vars['form']['bedrijf_pcl']; ?>" size="2" maxlength="2" />
              </li>
              <li<?php if ($this->_vars['error']['bedrijf_plaats']): ?> class="form-error"<?php endif; ?>>
                <label for="bedrijf_plaats">Plaats</label>
                <input type="text" id="bedrijf_plaats" name="bedrijf_plaats" value="<?php echo $this->_vars['form']['bedrijf_plaats']; ?>" />
              </li>
            </ol>
            <ol>
              <li>
                <em>Contactpersoon</em>
              </li>
              <li<?php if ($this->_vars['error']['persoon_geslacht']): ?> class="form-error"<?php endif; ?>>
                <label for="persoon_geslacht">Geslacht</label>
                <input type="radio" name="persoon_geslacht" id="persoon_geslacht_m" value="m" <?php if ($this->_vars['form']['persoon_geslacht'] == "m"): ?>checked="checked" <?php endif; ?>/> <label for="persoon_geslacht_m">De heer</label>
                <input type="radio" name="persoon_geslacht" id="persoon_geslacht_f" value="f" <?php if ($this->_vars['form']['persoon_geslacht'] == "f"): ?>checked="checked" <?php endif; ?>/> <label for="persoon_geslacht_f">Mevrouw</label>
              </li>
              <li<?php if ($this->_vars['error']['persoon_voorletters'] || $this->_vars['error']['persoon_tussenvoegsel']): ?> class="form-error"<?php endif; ?>>
                <label for="persoon_voorletters">Voorletter(s), tussenvoegsel</label>
                <input type="text" name="persoon_voorletters" id="persoon_voorletters" value="<?php echo $this->_vars['form']['persoon_voorletters']; ?>" size="5" maxlength="5" />
                <input type="text" name="persoon_tussenvoegsel" id="persoon_tussenvoegsel" value="<?php echo $this->_vars['form']['persoon_tussenvoegsel']; ?>" size="10" maxlength="10" />
              </li>
              <li<?php if ($this->_vars['error']['persoon_achternaam']): ?> class="form-error"<?php endif; ?>>
                <label for="persoon_achternaam">Achternaam</label>
                <input type="text" id="persoon_achternaam" name="persoon_achternaam" value="<?php echo $this->_vars['form']['persoon_achternaam']; ?>" />
              </li>
              <li<?php if ($this->_vars['error']['persoon_telefoonnummer']): ?> class="form-error"<?php endif; ?>>
                <label for="persoon_telefoonnummer">Telefoonnummer</label>
                <input type="text" id="persoon_telefoonnummer" name="persoon_telefoonnummer" value="<?php echo $this->_vars['form']['persoon_telefoonnummer']; ?>" />
              </li>
              <li<?php if ($this->_vars['error']['persoon_email']): ?> class="form-error"<?php endif; ?>>
                <label for="persoon_email">E-mailadres</label>
                <input type="text" id="persoon_email" name="persoon_email" value="<?php echo $this->_vars['form']['persoon_email']; ?>" />
              </li>
            </ol>
            <ol>
              <li>
                <em>Gegevens inzending</em>
              </li>
              <li<?php if ($this->_vars['error']['categorie']): ?> class="form-error"<?php endif; ?>>
                <label>In welke categorie doet u een inzending?</label>
                <ul>
<?php if (count((array)$this->_vars['categories'])): foreach ((array)$this->_vars['categories'] as $this->_vars['cat']): ?>
                  <li><input type="radio" name="categorie" value="<?php echo $this->_vars['cat']['cat_id']; ?>" id="cat_<?php echo $this->_vars['cat']['cat_id']; ?>"<?php if ($this->_vars['form']['categorie'] == $this->_vars['cat']['cat_id']): ?> checked="checked"<?php endif; ?> /><label for="cat_<?php echo $this->_vars['cat']['cat_id']; ?>"><?php echo $this->_vars['cat']['name']; ?></label></li>
<?php endforeach; endif; ?>
                </ul>
              </li>
              <li<?php if ($this->_vars['error']['naam_project']): ?> class="form-error"<?php endif; ?>>
                <label for="naam_project">Korte aanduiding/naam inzending</label>
                <input type="text" id="naam_project" name="naam_project" value="<?php echo $this->_vars['form']['naam_project']; ?>" />
              </li>
              <li<?php if ($this->_vars['error']['omschrijving']): ?> class="form-error"<?php endif; ?>>
                <label for="omschrijving">
                  <strong>A) Omschrijving</strong><br/>
                  Geef een korte omschrijving van uw inzending en beschrijf kort de essentie van het project, product of plan. (maximaal 250 woorden)
                </label>
                <textarea name="omschrijving" rows="5" cols="27"><?php echo $this->_vars['form']['omschrijving']; ?></textarea>
              </li>
              <li>
                <label for="motivatie">
                  <strong>B) Motivatie</strong><br/>
                  Geef in uw motivatie duidelijk aan waarom uw inzending in aanmerking komt voor een nominatie. Verwijs hierbij naar de onderdelen waarop de jury beoordeelt door onderstaande vragen te beantwoorden.
                </label>
              </li>
              <li<?php if ($this->_vars['error']['kwaliteit']): ?> class="form-error"<?php endif; ?>>
                <label for="kwaliteit">
                  <strong>1. Kwaliteit</strong><br/>
                  Is uw project klantgericht en duurzaam, is de vormgeving doordacht en getuigt uw project van kostenbewustzijn? (maximaal 200 woorden)
                </label>
                <textarea name="kwaliteit" rows="5" cols="27"><?php echo $this->_vars['form']['kwaliteit']; ?></textarea>
              </li>
              <li<?php if ($this->_vars['error']['uitvoerbaarheid']): ?> class="form-error"<?php endif; ?>>
                <label for="uitvoerbaarheid">
                  <strong>2. Uitvoerbaarheid</strong><br/>
                  Is uw inzending eenvoudig te gebruiken of te verwezenlijken door andere organisaties en in andere gebieden? (maximaal 200 woorden)
                </label>
                <textarea name="uitvoerbaarheid" rows="5" cols="27"><?php echo $this->_vars['form']['uitvoerbaarheid']; ?></textarea>
              </li>
              <li<?php if ($this->_vars['error']['innovatie']): ?> class="form-error"<?php endif; ?>>
                <label for="innovatie">
                  <strong>3. Innovatie</strong><br/>
                  Is uw project vernieuwend en origineel ten opzichte van het bestaande aanbod en verdient uw inzending navolging? (maximaal 200 woorden)
                </label>
                <textarea name="innovatie" rows="5" cols="27"><?php echo $this->_vars['form']['innovatie']; ?></textarea>
              </li>
              <li>
                <label for="bezwaar">
                  Alle projecten die worden ingezonden voor de AGV Prijs 2010 worden op deze website gepubliceerd. Zo krijgt u de mogelijkheid om uw project te presenteren en krijgt het project de aandacht die het verdient. Wilt u dit liever niet, vink dat dan hiernaast  aan.
                </label>
                <input type="checkbox" name="bezwaar" id="bezwaar" value="1" />
              </li>
              <li<?php if ($this->_vars['error']['foto']): ?> class="form-error"<?php endif; ?>>
                <label for="foto">
                  <strong>C) Foto</strong><br/>
                  Voeg minimaal een hoge resolutie foto bij in drukwerkkwaliteit (300 dpi) door deze te uploaden.
                </label>
<?php for($for1 = 0; ((0 < 3) ? ($for1 < 3) : ($for1 > 3)); $for1 += ((0 < 3) ? 1 : -1)):  $this->assign('i', $for1); ?>
  <?php if (isset ( $this->_vars['form']['foto'][$this->_vars['i']] )): ?>
                <?php echo $this->_vars['form']['foto'][$this->_vars['i']]; ?><br />
  <?php else: ?>
                <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $this->_vars['max_filesize_bytes']; ?>" />
                <input type="file" name="foto[]" />
  <?php endif; ?>
<?php endfor; ?>
              </li>
              <li<?php if ($this->_vars['error']['file']): ?> class="form-error"<?php endif; ?>>
                <label for="file">
                  <strong>D) Aanvullende informatie</strong> (optioneel)<br/>
                  Voeg eventuele aanvullende digitale informatie toe zoals brochures, digitaal foto- en videomateriaal, presentaties, persberichten etc. De maximale bestandsgrootte is <?php echo $this->_vars['max_filesize']; ?>.
                </label>
<?php for($for1 = 0; ((0 < 3) ? ($for1 < 3) : ($for1 > 3)); $for1 += ((0 < 3) ? 1 : -1)):  $this->assign('i', $for1); ?>
  <?php if (isset ( $this->_vars['form']['file'][$this->_vars['i']] )): ?>
                <?php echo $this->_vars['form']['file'][$this->_vars['i']]; ?><br />
  <?php else: ?>
                <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $this->_vars['max_filesize_bytes']; ?>" />
                <input type="file" name="file[]" />
  <?php endif; ?>
<?php endfor; ?>
              </li>
              <li class="submit">
                <input type="submit" value="Inschrijven" />
              </li>
            </ol>
          </fieldset>
        </form>