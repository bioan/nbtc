<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2011-01-13 15:55:59 CET */ ?>                <div class="page news">
                    <div class="page-content">
                        <h2>Laatste nieuws</h2>
                        <div class="text">
                            <?php echo $this->_vars['current']['bericht']; ?>
                        </div>
                    </div>
                    <div class="page-content right-part">
                        <h2>Nieuws overzicht</h2>
                        <?php if (count((array)$this->_vars['news'])): foreach ((array)$this->_vars['news'] as $this->_vars['k'] => $this->_vars['item']): ?>  
                        <p class="news-item <?php if ($this->_vars['k'] == $this->_run_modifier($this->_vars['news'], 'count', 'PHP', 0) - 1): ?>last<?php endif; ?>">
                            <span class="date"><?php echo $this->_vars['item']['datum_ne']; ?></span><br />
                            <a href="/nieuws/<?php echo $this->_vars['item']['slug']; ?>.html" <?php if ($this->_vars['item']['slug'] == $this->_vars['current']['slug']): ?>class="selected"<?php endif; ?>><?php echo $this->_vars['item']['titel']; ?></a>    
                        </p>
                        <?php endforeach; endif; ?>
                    </div>
                </div>
                