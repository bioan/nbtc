<?php /* Smarty version Smarty-3.0.7, created on 2013-03-15 11:03:18
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/Visual/Templates/forum.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2354645425142f1e6594f88-16632170%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7413256b3ef978f8aa09ba4c9e7cdab225c42ddb' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/Visual/Templates/forum.tpl',
      1 => 1362739877,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2354645425142f1e6594f88-16632170',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="forms <?php echo $_smarty_tpl->getVariable('page')->value;?>
">

<?php if ($_smarty_tpl->getVariable('page')->value=='openid'){?>
    <h3>Inloggen</h3>
    <?php if (isset($_smarty_tpl->getVariable('openid',null,true,false)->value)&&$_smarty_tpl->getVariable('openid')->value=='logged'){?>
        <?php if (isset($_smarty_tpl->getVariable('identity',null,true,false)->value)){?>
            <?php echo $_smarty_tpl->getVariable('identity')->value;?>
<br />
        <?php }?>
        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('user')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
            <?php echo $_smarty_tpl->tpl_vars['item']->value;?>
<br />
        <?php }} ?>
    <?php }else{ ?>
    
    <form action="" method="post">
        Account 
        <select name="type">
            <option value="google">google</option>
            <option value="default">other</option>
        </select>
        OpenID: <input type="text" name="openid_identifier" /> <button>Submit</button>
    </form>
    <script> 
    /* function displayUser(user) {
       var userName = document.getElementById('userName');
       var greetingText = document.createTextNode('Greetings, '
         + user.name + '.');
   userName.appendChild(greetingText);
     }

     var appID = "298638790179649";
     <?php if (isset($_smarty_tpl->getVariable('logged',null,true,false)->value)){?>
     if (window.location.hash.length == 0) {
       var path = 'https://www.facebook.com/dialog/oauth?';
   var queryParams = ['client_id=' + appID,
     'redirect_uri=' + window.location,
     'response_type=token'];
   var query = queryParams.join('&');
   var url = path + query;
   window.open(url, '_self');
     } else {
       var accessToken = window.location.hash.substring(1);
       var path = "https://graph.facebook.com/me?";
   var queryParams = [accessToken, 'callback=displayUser'];
   var query = queryParams.join('&');
   var url = path + query;

   // use jsonp to call the graph
       var script = document.createElement('script');
       script.src = url;
       document.body.appendChild(script);        
     }*/
     <?php }?>
   </script> 
   <p id="userName"><?php if (isset($_smarty_tpl->getVariable('username',null,true,false)->value)){?><?php echo $_smarty_tpl->getVariable('username')->value;?>
<?php }?></p> 
   <a href="/facebook.html">test</a>
    <?php }?>
<?php }?>

<?php if ($_smarty_tpl->getVariable('page')->value=='login'){?>

<h3>Inloggen</h3>
<form action="/forumuser/login.html" method="post">
    <table cellpadding="0" cellspacing="0" border="0">
    
    <?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['email'])){?>
    
        <tr>
            <td colspan="2">
                <span class="red">U heeft niet de juiste gegevens ingevuld.</span>
            </td>
        </tr>
    
    <?php }?>
    
        <tr>
            <td>E-mail:</td>
            <td><input type="text" name="email" value="" class="input" /></td>
        </tr>
        <tr>
            <td>Wachtwoord:</td>
            <td><input type="password" name="password" value="" class="input" /></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="submit" value="Submit" class="submit" /></td>
        </tr>
        <tr>
            <td><a href="/forumuser/register.html">Registreren</a></td>
            <td><a href="/forumuser/forgot-password.html">Wachtwoord vergeten?</a></td>
        </tr>
    </table>
    
<?php if (isset($_smarty_tpl->getVariable('register',null,true,false)->value)){?>

    <p>
        Er wordt een e-mail naar u toegestuurd met het wachtwoord om in te kunnen loggen, heeft u geen wachtwoord ontvangen mail info@inforitus.nl.   
    </p>
    
<?php }?>

</form>

<?php }?>

<?php if ($_smarty_tpl->getVariable('page')->value=='forgot-password'){?>

<h3>Wachtwoord vergeten?</h3>
<?php if (!isset($_POST['email'])){?>
<p>
    Vul onderstaand uw e-mail adres in, u ontvangt vervolgens een mail met daarop een link waar u uw wachtwoord opnieuw kunt invullen.
</p>
<?php }?>
<form action="/forumuser/forgot-password.html" method="post">
    <table cellpadding="0" cellspacing="0" border="0" class="forgot">
    
        <?php if (isset($_smarty_tpl->getVariable('update',null,true,false)->value)&&$_smarty_tpl->getVariable('update')->value==0){?>
        
        <tr>
            <td colspan="2">
                <?php if (isset($_smarty_tpl->getVariable('emailexist',null,true,false)->value)){?>
                <span class="red">Uw gegevens zijn niet bij ons bekend.</span>
                <?php }else{ ?>
                <span class="red">Uw e-mailadres is ongeldig.</span>
                <?php }?>
            </td>
        </tr>
        <?php }else{ ?>
            <?php if (isset($_smarty_tpl->getVariable('update',null,true,false)->value)&&$_smarty_tpl->getVariable('update')->value==1){?>
                <span class="red">Uw wachtwoord is gewijzigd <a href="/forumuser/login.html">Login</a></span>
            <?php }?>
        <?php }?>
        
        <?php if ((isset($_smarty_tpl->getVariable('update',null,true,false)->value)&&$_smarty_tpl->getVariable('update')->value==0)||isset($_smarty_tpl->getVariable('update',null,true,false)->value)==false){?>
        
        <tr>
            <td>E-mail</td>
            <td><input type="text" name="email" value="" class="input" /></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="submit" value="Submit" class="submit" /></td>
        </tr>
        
        <?php }?>
    </table>
</form>

<?php }?>

<?php if ($_smarty_tpl->getVariable('page')->value=='change-password'){?>

<h3>Wachtwoord vergeten?</h3>
<form action="" method="post">
    <table cellpadding="0" cellspacing="0" border="0" class="forgot">
    
        <?php if (isset($_smarty_tpl->getVariable('succes',null,true,false)->value)&&$_smarty_tpl->getVariable('succes')->value==0&&$_POST['password']!=''){?>
        <tr>
            <td colspan="2">
                <span class="red">Passwords have to be the same.</span>
            </td>
        </tr>
        <?php }elseif(isset($_smarty_tpl->getVariable('succes',null,true,false)->value)&&$_smarty_tpl->getVariable('succes')->value==0&&$_POST['password']==''){?>
        <tr>
            <td colspan="2">
                <span class="red">Write your new password.</span>
            </td>
        </tr>
        <?php }else{ ?>
            <?php if (isset($_smarty_tpl->getVariable('succes',null,true,false)->value)&&$_smarty_tpl->getVariable('succes')->value==1){?>
                <span class="red">Uw wachtwoord is gewijzigd <a href="/forumuser/login.html">Login</a></span>
            <?php }?>
        <?php }?>
        
        <?php if ((isset($_smarty_tpl->getVariable('succes',null,true,false)->value)&&$_smarty_tpl->getVariable('succes')->value==0)||isset($_smarty_tpl->getVariable('succes',null,true,false)->value)==false){?>
        
        <tr>
            <td>Wachtwoord</td>
            <td><input type="password" name="password" value="" class="input" /></td>
        </tr>
        <tr>
            <td>Herhaal Wachtwoord</td>
            <td><input type="password" name="password2" value="" class="input" /></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="submit" value="Submit" class="submit" /></td>
        </tr>
        
        <?php }?>
    </table>
</form>

<?php }?>

<?php if ($_smarty_tpl->getVariable('page')->value=='register'){?>

<?php if (isset($_smarty_tpl->getVariable('register',null,true,false)->value)&&$_smarty_tpl->getVariable('register')->value==0){?>
<h3>Registreren</h3>
<br />
<p>Vul de velden in gemarkeerd met een *.</p>
<br />
<form action="/forumuser/register.html" method="post" autocomplete="off">
    <table cellpadding="0" cellspacing="0" border="0" class="register-form">

        <?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['username'])){?>
        
        <tr>
            <td colspan="2">
                <span class="red">Alle velden zijn verplicht.</span>
            </td>
        </tr>
        
        <?php }?>
        
        <?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['emailexist'])&&$_smarty_tpl->getVariable('values')->value['emailexist']==false){?>
        
        <tr>
            <td colspan="2">
                <span class="red">Email allready exist.</span>
            </td>
        </tr>
        
        <?php }?>
        
        <tr>
            <td>Username*:</td>
            <td><input type="text" name="username" value="<?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['username'])&&$_smarty_tpl->getVariable('values')->value['username']!=false){?><?php echo $_smarty_tpl->getVariable('values')->value['username'];?>
<?php }?>" class="input <?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['username'])&&$_smarty_tpl->getVariable('values')->value['username']==false){?>error<?php }?>" /></td>
        </tr>
        <tr>
            <td>E-mail*:</td>
            <td><input type="text" name="email" value="<?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['email'])&&$_smarty_tpl->getVariable('values')->value['email']!=false){?><?php echo $_smarty_tpl->getVariable('values')->value['email'];?>
<?php }?>" class="input <?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['email'])&&$_smarty_tpl->getVariable('values')->value['email']==false){?>error<?php }?>" /></td>
        </tr>
        <tr>
            <td>Wachtwoord*:</td>
            <td><input type="password" name="password" value="" class="input <?php if ((isset($_smarty_tpl->getVariable('values',null,true,false)->value['password'])&&$_smarty_tpl->getVariable('values')->value['password']==false)||(isset($_smarty_tpl->getVariable('values',null,true,false)->value['password2'])&&$_smarty_tpl->getVariable('values')->value['password2']==false)){?>error<?php }?>" /></td>
        </tr>
        <tr>
            <td>Herhaal Wachtwoord*:</td>
            <td><input type="password" name="password2" value="" class="input <?php if ((isset($_smarty_tpl->getVariable('values',null,true,false)->value['password'])&&$_smarty_tpl->getVariable('values')->value['password']==false)||(isset($_smarty_tpl->getVariable('values',null,true,false)->value['password2'])&&$_smarty_tpl->getVariable('values')->value['password2']==false)){?>error<?php }?>" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" name="submit" value="Submit" class="submit" />
                <input type="hidden" name="action" value="register" />
            </td>
        </tr>
    </table>
</form>
<?php }else{ ?>
U heeft zich succesvol geregistreerd.<a href="/forumuser/login.html">Login</a>
<?php }?>
<?php }?>

<?php if ($_smarty_tpl->getVariable('page')->value=='change-profile'){?>

<?php if ($_smarty_tpl->getVariable('register')->value==0){?>
<h3>Gegevens wijzigen</h3>
<br />
<p>Vul de velden in gemarkeerd met een *.</p>
<br />
<form action="/forumuser/profile.html" method="post">
    <table cellpadding="0" cellspacing="0" border="0" class="register-form">

        <?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['username'])){?>
        
        <tr>
            <td colspan="2">
                <span class="red">Alle velden zijn verplicht.</span>
            </td>
        </tr>
        
        <?php }?>
        
        <?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['emailexist'])&&$_smarty_tpl->getVariable('values')->value['emailexist']==true){?>
        
        <tr>
            <td colspan="2">
                <span class="red">Email allready exist.</span>
            </td>
        </tr>
        
        <?php }?>
        
        <tr>
            <td>Username*:</td>
            <td><input type="text" name="username" value="<?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['username'])&&$_smarty_tpl->getVariable('values')->value['username']!=false){?><?php echo $_smarty_tpl->getVariable('values')->value['username'];?>
<?php }else{ ?><?php echo $_smarty_tpl->getVariable('user')->value['username'];?>
<?php }?>" class="input <?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['username'])&&$_smarty_tpl->getVariable('values')->value['username']==false){?>error<?php }?>" /></td>
        </tr>
        <!--<tr>
            <td>E-mail*:</td>
            <td><input type="text" name="email" value="<?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['email'])&&$_smarty_tpl->getVariable('values')->value['email']!=false){?><?php echo $_smarty_tpl->getVariable('values')->value['email'];?>
<?php }else{ ?><?php echo $_smarty_tpl->getVariable('user')->value['email'];?>
<?php }?>" class="input <?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['email'])&&$_smarty_tpl->getVariable('values')->value['email']==false){?>error<?php }?>" /></td>
        </tr>-->
        <tr>
            <td>Wachtwoord*:</td>
            <td><input type="password" name="passwordd" value="" class="input <?php if ((isset($_smarty_tpl->getVariable('values',null,true,false)->value['passwordd'])&&$_smarty_tpl->getVariable('values')->value['passwordd']==false)||(isset($_smarty_tpl->getVariable('values',null,true,false)->value['passwordd2'])&&$_smarty_tpl->getVariable('values')->value['passwordd2']==false)){?>error<?php }?>" /></td>
        </tr>
        <tr>
            <td>Herhaal Wachtwoord*:</td>
            <td><input type="password" name="passwordd2" value="" class="input <?php if ((isset($_smarty_tpl->getVariable('values',null,true,false)->value['passwordd'])&&$_smarty_tpl->getVariable('values')->value['passwordd']==false)||(isset($_smarty_tpl->getVariable('values',null,true,false)->value['passwordd2'])&&$_smarty_tpl->getVariable('values')->value['passwordd2']==false)){?>error<?php }?>" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" name="submit" value="Submit" class="submit" />
                <input type="hidden" name="action" value="register" />
            </td>
        </tr>
    </table>
</form>
<?php }else{ ?>
Uw gegevens zijn aangepast
<?php }?>
<?php }?>
<?php if ($_smarty_tpl->getVariable('page')->value=='logout'){?>
U bent uitgelogd.
<?php }?>
</div>