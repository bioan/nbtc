<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2011-01-13 16:02:15 CET */ ?><div class="page default">   
    <h2>Perspublicaties</h2>
    <?php if (count((array)$this->_vars['publicaties_groups'])): foreach ((array)$this->_vars['publicaties_groups'] as $this->_vars['k'] => $this->_vars['item']): ?>
    <div class="item">
        <h2><?php echo $this->_vars['item']['groepnaam']; ?></h2>
        <div class="text">
            <?php echo $this->_vars['item']['beschrijving']; ?>    
        </div>
        <table class="publicaties">
        <?php if (count((array)$this->_vars['item']['items'])): foreach ((array)$this->_vars['item']['items'] as $this->_vars['kp'] => $this->_vars['pitem']): ?>  
        <tr>
            <td class="dot">&bull;</td>
            <?php if ($this->_vars['pitem']['datum_ne'] == '00-00-0000' && $this->_vars['pitem']['source'] == ''): ?>
            <td colspan="2">
                <a href="/<?php echo $this->_vars['pitem']['file']; ?>"><?php echo $this->_vars['pitem']['title']; ?></a>
            </td>
            <?php endif; ?>
            <?php if ($this->_vars['pitem']['datum_ne'] != '00-00-0000' || $this->_vars['pitem']['source'] != ''): ?>
            <td class="source">
                <?php if ($this->_vars['pitem']['source'] != ''):  echo $this->_vars['pitem']['source']; ?> <?php endif;  if ($this->_vars['pitem']['datum_ne'] != ''): ?>- <?php echo $this->_vars['pitem']['datum_ne'];  endif; ?>
            </td>
            <td>
                <a href="<?php if ($this->_vars['pitem']['file'] != ''): ?>/<?php echo $this->_vars['pitem']['file'];  else:  echo $this->_vars['pitem']['url'];  endif; ?>" target="_blank"><?php echo $this->_vars['pitem']['title']; ?></a>
            </td>    
            <?php endif; ?>
        </tr>
        <?php endforeach; endif; ?>
        </table>
    </div>
    <?php endforeach; endif; ?>
</div>
     