<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2011-01-14 08:33:30 CET */ ?><!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="AGV Prijs 2011" />
        <title>Waterinnovatieprijs 2011</title>
        <?php if (count((array)$this->_vars['stylesheets'])): foreach ((array)$this->_vars['stylesheets'] as $this->_vars['file']): ?>
        <link rel="stylesheet" href="<?php echo $this->_vars['file']; ?>" type="text/css" media="screen" />
        <?php endforeach; endif; ?>    
        <!--[if lt IE 9]>
        <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <?php if (count((array)$this->_vars['javascripts'])): foreach ((array)$this->_vars['javascripts'] as $this->_vars['file']): ?>
        <script src="<?php echo $this->_vars['file']; ?>"></script> 
        <?php endforeach; endif; ?>  
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>   
    </head>
    <body>
        <div id="agv">
            <header>
                <img src="/images/logo.jpg" alt="Waterschap Amstel, Gooi en Vecht" class="logo" />
                <img src="/images/header_text.jpg" alt="AGV WATERINNOVATIEPRIJS 2011" class="header_text" />
            </header>
            <br class="clear" />
            <nav>
                <ul>
                    <li><a href="/" <?php if ($this->_vars['page'] == 'index'): ?> class="selected"<?php endif; ?>>Welkom</a></li>
                    <li><a href="/nieuws.html" <?php if ($this->_vars['page'] == 'nieuws'): ?> class="selected"<?php endif; ?>>Nieuws</a></li> 
                    <li><a href="/categorieen-criteria.html" <?php if ($this->_vars['page'] == 'categorieen-criteria'): ?> class="selected"<?php endif; ?>>CategorieŽn + criteria</a></li>
                    <li><a href="/inschrijfpagina.html" <?php if ($this->_vars['page'] == 'inschrijfpagina'): ?> class="selected"<?php endif; ?>>Inschrijfpagina</a></li>
                    <li><a href="/juryleden.html" <?php if ($this->_vars['page'] == 'juryleden'): ?> class="selected"<?php endif; ?>>Juryleden</a></li>
                    <li><a href="/inzendingen.html" <?php if ($this->_vars['page'] == 'inzendingen'): ?> class="selected"<?php endif; ?>>Inzendingen</a></li>
                    <li><a href="/perspublicaties.html" <?php if ($this->_vars['page'] == 'perspublicaties'): ?> class="selected"<?php endif; ?>>Perspublicaties</a></li> 
                </ul>
            </nav>
            <div class="content">  
                <div <?php if ($this->_vars['page'] == 'index'): ?>id="slideshow"<?php endif; ?> class="pics">
                    <?php if ($this->_vars['page'] == 'index'): ?>
                    <div id="nav"></div>
                    <img src="/images/header_image.jpg" alt="" width="774" height="252" />
                    <img src="/images/header_image2.jpg" alt="" width="774" height="252" />
                    <img src="/images/header_image3.jpg" alt="" width="774" height="252" />
                    <?php else: ?>
                    <img src="/images/header_<?php echo $this->_vars['page']; ?>.jpg" alt="" width="774" height="252" />
                    <?php endif; ?>
                    <div class="header_bars"></div>
                    <div class="text"></div>     
                </div>
                <img src="/images/content_bar.jpg" alt="" />          
                <?php echo $this->_vars['content']; ?>           
            </div>
            <br class="clear" />
        </div> 
        <footer>
            <img src="/images/footer_logo.jpg" alt="" class="logo" />
            <div class="text">
                <p class="copyright">
                    Waterschap Amstel, Gooi en Vecht © 2010. Alle rechten voorbehouden.    
                </p>                                                                   
                <p>
                    Secretariaat AGV Waterinnovatieprijs 2011 - EPP public relations - Telefoon: (030) 251 77 34  - E-mail: info@waterinnovatieprijs.nl<br /><br />
                    Alle beleidsvoorbereidende- en uitvoerende werkzaamheden van Waterschap Amstel, Gooi en Vecht worden verricht door Waternet. 
<a href="http://www.agv.nl">www.agv.nl</a>
                </p>
            </div>
        </footer>
    </body>
</html>