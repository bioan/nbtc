<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2011-01-13 16:00:13 CET */ ?><div class="page default">   
    <h2>Inzendingen 2010</h2>
    <p class="inzendingen-text">Op deze pagina vindt u een overzicht van de inzendingen die tot nu toe zijn gedaan. Klik op de inzending om deze te bekijken.</p>
    <?php if (count((array)$this->_vars['categories'])): foreach ((array)$this->_vars['categories'] as $this->_vars['k'] => $this->_vars['item']): ?>
    <div class="item">
        <h2>Categorie: <?php echo $this->_vars['item']['name']; ?></h2>
        <div class="header">
            <div>Inschrijvingnaam</div>
            <div>Bedrijfsnaam</div>
        </div>
        <?php if (count((array)$this->_vars['item']['items'])): foreach ((array)$this->_vars['item']['items'] as $this->_vars['kp'] => $this->_vars['proj']): ?>
        <div class="row collapse_entry">
            <div class="title"><?php echo $this->_vars['proj']['InschrijvingNaam']; ?><br /><span class="summaryleesmeer"> Lees meer...</span></div>
            <div><?php echo $this->_vars['proj']['BedrijfNaam'];  if ($this->_vars['proj']['Genomineerd'] == true): ?><span class="genomineerd">(genomineerd)</span><?php endif; ?></div>
            <br class="clear" />
        </div>                                    
        <div class="collapse">
            <div class="summary">
                <?php echo $this->_vars['proj']['Summary']; ?>
                <br class="clear" />
            </div>
        </div>
        <?php endforeach; endif; ?>
    </div>
    <?php endforeach; endif; ?>
</div>
     