<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2011-01-13 15:40:43 CET */ ?>                <div class="page">
                    <div class="page-content <?php echo $this->_vars['page']; ?>">
                        <h2><?php echo $this->_vars['content']['name']; ?></h2>
                        <div class="text">
                            <?php echo $this->_vars['content']['content'];  if ($this->_vars['page'] != 'welkom'): ?> <a href="/welkom.html" class="lees">Lees meer &raquo;</a><?php endif; ?>
                        </div>
                    </div>
                    <?php if ($this->_vars['page'] != 'welkom'): ?>
                    <div class="page-content right-part">
                        <h2>Laatste nieuws</h2>
                        <?php if (count((array)$this->_vars['news'])): foreach ((array)$this->_vars['news'] as $this->_vars['k'] => $this->_vars['item']): ?>  
                        <div class="news-item <?php if ($this->_vars['k'] == $this->_run_modifier($this->_vars['news'], 'count', 'PHP', 0) - 1): ?>last<?php endif; ?>">
                            <h3><?php echo $this->_vars['item']['titel']; ?></h3>
                            <div class="text">
                                <p>
                                    <img src="<?php echo $this->_vars['item']['photos']['0']['pathphoto']; ?>/<?php echo $this->_vars['item']['photos']['0']['id']; ?>_133x100<?php echo $this->_vars['item']['photos']['0']['image_ext']; ?>" alt="" <?php if ($this->_vars['k'] % 2 == 0): ?>class="left"<?php else: ?>class="right"<?php endif; ?> />         
                                    <?php echo $this->_vars['item']['bericht']; ?>...
                                    <a href="/nieuws/<?php echo $this->_vars['item']['slug']; ?>.html" class="lees">Lees meer &raquo;</a>
                                </p>    
                            </div>
                            <br class="clear" />
                        </div>
                        <?php endforeach; endif; ?>
                    </div>
                    <?php endif; ?>
                </div>
                