<?php /* Smarty version Smarty-3.0.7, created on 2013-03-08 19:01:24
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/Visual/layout.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1504277474513a2774974aa9-42593469%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '184390f25af5a42d0f38b3e088c2d0f74b730dd0' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/Visual/layout.tpl',
      1 => 1362739875,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1504277474513a2774974aa9-42593469',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!doctype html>
<html lang="en">
    <head>
        <title><?php echo $_smarty_tpl->getVariable('website_name')->value;?>
 - <?php echo $_smarty_tpl->getVariable('page_title')->value;?>
</title>
        <meta charset="utf-8" />
        <meta name="description" content="<?php echo $_smarty_tpl->getVariable('page_description')->value;?>
" />
        <?php  $_smarty_tpl->tpl_vars['file'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('stylesheets')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['file']->key => $_smarty_tpl->tpl_vars['file']->value){
?>
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['file']->value;?>
" type="text/css" media="screen" />
        <?php }} ?>    
        <!--[if lt IE 9]>
        <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <?php  $_smarty_tpl->tpl_vars['file'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('javascripts')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['file']->key => $_smarty_tpl->tpl_vars['file']->value){
?>
        <script src="<?php echo $_smarty_tpl->tpl_vars['file']->value;?>
"></script> 
        <?php }} ?>  
    </head>
    <body> 
        <div id="container">
            <header>
            </header>
            <br class="clear" />
            <nav>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('menu_items')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                
            <?php }} ?>
            </nav>
            <div class="content <?php echo $_smarty_tpl->getVariable('page_name')->value;?>
">
                aaadd
                <?php $_template = new Smarty_Internal_Template($_smarty_tpl->getVariable('content')->value, $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>        
            </div>
            <br class="clear" />
        </div> 
        <footer>
        </footer>
    <script type="text/javascript"> Cufon.now(); </script>
    </body>
</html>