<?php /* Smarty version Smarty-3.0.7, created on 2013-03-14 14:32:52
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/submenu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6438017865141d1841bcb72-26123997%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3bb111b0b9eb8016dbfc29992765a3a97f21b9e5' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/submenu.tpl',
      1 => 1363267959,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6438017865141d1841bcb72-26123997',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_smarty_tpl->getVariable('folder')->value['page']=='news'){?>
<ul class="submenu">
    <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/news.php?action=new">toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/news.php?action=overview">overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/groups.php?action=new">groepen toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/groups.php?action=overview">groepen overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/comment.php?action=overview">blog</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/users.php?action=overview">users</a></li>
</ul>
<?php }?> 
<?php if ($_smarty_tpl->getVariable('folder')->value['page']=='forum'){?>
<ul class="submenu">
    <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
forum/comment.php?action=overview">overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
forum/users.php">users</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
forum/settings.php?action=new">settings</a></li>
</ul>
<?php }?> 
<?php if ($_smarty_tpl->getVariable('folder')->value['page']=='newsletter'||$_smarty_tpl->getVariable('folder')->value['page']=='template'||$_smarty_tpl->getVariable('folder')->value['page']=='content'){?>
<ul class="submenu">
    <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=new">template toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=overview">template overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/items.php?action=new">content toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/items.php?action=overview">content overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/subscribers.php?action=new">subscriber toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/subscribers.php?action=overview">subscribers overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/subscribers_groups.php?action=new">subscriber group toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/subscribers_groups.php?action=overview">subscriber groups overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/monitor.php?action=overview">monitor</a></li>
</ul>
<?php }?>     
<?php if ($_smarty_tpl->getVariable('folder')->value['page']=='users'){?>
<ul class="submenu">
    <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
users.php?action=new">toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
users.php?action=overview">overzicht</a></li>
</ul>
<?php }?>
<?php if ($_smarty_tpl->getVariable('folder')->value['page']=='webshop'){?>
<ul class="submenu">
    <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
webshop/webshop.php?action=new">toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
webshop/webshop.php?action=overview">overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
webshop/cats.php?action=new">categorie&euml;n toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
webshop/cats.php?action=overview">categorie&euml;n overzicht</a></li>
</ul>
<?php }?>

<?php if ($_smarty_tpl->getVariable('folder')->value['page']=='social_media'){?>
<ul class="submenu">
    <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=facebook&action=new">Facebook</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=twitter&action=new">Twitter</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=linkedin&action=new">Linkedin</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=google&action=new">Google</a></li>
</ul>
<ul class="submenu">    
    <?php if (isset($_GET['media'])){?>
       <?php if ($_GET['media']=='facebook'){?>
           <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=facebook&action=new">Connect to Facebook</a></li>
       <?php }elseif($_GET['media']=='twitter'){?>
           <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=twitter&action=new">Connect to Twitter</a></li>
           <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=twitter&action=newWord">Add twitter word</a></li>
           <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=twitter&action=overviewWords">Overzicht twitter words</a></li>
           <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=twitter&action=overviewSearch">Overzicht twitter search results</a></li>
           <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=twitter&action=overview">overzicht Tweets</a></li>
       <?php }elseif($_GET['media']=='linkedin'){?>
           <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=linkedin&action=new">Connect to Linkedin</a></li>
       <?php }?>
    <?php }else{ ?>
        <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
social_media/social_media.php?media=facebook&action=new">Connect to Facebook</a></li>
    <?php }?>
</ul>
<?php }?> 
<?php if ($_smarty_tpl->getVariable('folder')->value['page']=='foto'){?>
<ul class="submenu">
    <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/photoalbum.php?action=new">toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/photoalbum.php?action=overview">overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/cats.php?action=new">categorie&euml;n toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/cats.php?action=overview">categorie&euml;n overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/upload.php?action=overview">ZIP upload</a></li>
</ul>
<?php }?>
<?php if ($_smarty_tpl->getVariable('folder')->value['page']=='videoalbum'){?>
<ul class="submenu">
    <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
videoalbum/videoalbum.php?action=new">toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
videoalbum/videoalbum.php?action=overview">overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
videoalbum/cats.php?action=new">categorie&euml;n toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
videoalbum/cats.php?action=overview">categorie&euml;n overzicht</a></li>
</ul>
<?php }?>
<?php if ($_smarty_tpl->getVariable('folder')->value['page']=='agenda'){?>
<ul class="submenu">
    <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
agenda/agenda.php?action=new">toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
agenda/agenda.php?action=overview">overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
agenda/groups.php?action=new">groepen toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
agenda/groups.php?action=overview">groepen overzicht</a></li>
</ul>
<?php }?>
<?php if ($_smarty_tpl->getVariable('folder')->value['page']=='language'){?>
<ul class="submenu">
    <li class="first"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
language/language.php?action=new">toevoegen</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
language/language.php?action=overview">overzicht</a></li>
    <li><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
language/language.php?action=stats">stats</a></li>
</ul>
<?php }?>