<?php /* Smarty version Smarty-3.0.7, created on 2012-11-08 11:52:00
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/newsletter/templates.tpl" */ ?>
<?php /*%%SmartyHeaderCode:609167867509b8ed07116c2-69148958%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6396ec7ee46db36351cbcb220fda31d4177da0ed' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/newsletter/templates.tpl',
      1 => 1352371907,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '609167867509b8ed07116c2-69148958',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_replace')) include '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/includes/plugins/modifier.replace.php';
?><script>

$ (document).ready (function (){
    $('#templateForm').submit(function() {
        var data = $('#templateForm').serialize();
        data = data + '&send=send';
        $.post('/cms/newsletter/templates.php?action=edit&id=' + $ ('#template').val (),
             data,
            function(result) {
                
            }
        );
        
        
        setTimeout('getProgress ()', 500);
        
        return false;
    });
});

function getProgress ()
{
    $.getJSON ('/cms/newsletter/progress.js', function(data){
        if (data)
        {
            $("#progressbar").progressbar({ value: data ['progress'] });
            
            if (data ['progress'] != 100)
                setTimeout('getProgress ()', 500);
        }
    })       
}

            

</script>
<?php if ($_GET['action']=='new'){?>
<div class="pagina-form modules">
    <div id="pagina-title" class="pagina-titel"><?php echo $_smarty_tpl->getVariable('folder')->value['text'];?>
</div>   
    <div id="content_in_content_div">
    <form method="post" action="" >
		<table style="width: 100%;">
		    <colgroup style="width: 25%"></colgroup>
            <colgroup style="width: 75%"></colgroup>
            <tbody>                               
            <?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?>
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?>
						<li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li>
					<?php }} ?>
					</ol>
				</td>
			</tr>
			<?php }?>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['id'], null, null);?>
			<tr>
				<td class="fieldName">
				    <label for="name_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Template <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
:</label> 
				</td>
				<td>
				    <input type="text" class="input" name="group_name[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="name_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" value="<?php if (isset($_POST['group_name'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php echo $_POST['group_name'][$_smarty_tpl->getVariable('id')->value];?>
<?php }?>" size="40" title="Max 100 tekens" maxlength="101" onchange="countAndTruncate(this, 100, 'f_540_counter_id');" onkeyup="countAndTruncate(this, 100, 'f_540_counter_id');"/>
				    <span id="f_540_counter_id"></span>
                    
                </td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Beschrijving <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
:</label> 
				</td>
				<td>
                    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value[$_smarty_tpl->getVariable('id')->value];?>

				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Default item <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
:</label>
                </td>
                <td>
                    <textarea name="page_description[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if (isset($_POST['page_description'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php echo $_POST['page_description'][$_smarty_tpl->getVariable('id')->value];?>
<?php }?></textarea>
                </td>
            </tr>
            
            <?php }} ?>
			<tr>
                <td></td>
				<td><br />
				<a href="#" onclick="document.forms[0].submit();"><img name="verzenden" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/save.png" alt="" /></a>
				</td>
			</tr>
		</table>
	</form>
    </div>
    <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/pagina_bottom.jpg" alt="" class="pagina_bottom" />
</div>
<?php }elseif($_GET['action']=='overview'){?> 
    Rijen weergeven :&nbsp;
    <select onchange="window.location='groups.php?action=overview&items=' + this.value + '<?php if (isset($_GET['sort'])){?>&sort=<?php echo $_GET['sort'];?>
<?php }?><?php if (isset($_GET['sorttype'])){?>&sorttype=<?php echo $_GET['sorttype'];?>
<?php }?>'">
        <option value="5" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==5){?> selected="selected"<?php }?>>5</option>
        <option value="10" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==10){?> selected="selected"<?php }?>>10</option>
        <option value="25" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==25){?> selected="selected"<?php }?>>25</option>
        <option value="50" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==50){?> selected="selected"<?php }?>>50</option>
        <option value="100" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==100){?> selected="selected"<?php }?>>100</option>
        <option value="10000" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==10000){?> selected="selected"<?php }?>>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="groups" cellpadding="2" cellspacing="0">
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 41%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=group_name&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Groepnaam</a>
                </td>
                <td class="first-row-column-white">
                    <a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=date_format&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Datum toegevoegd</a>
                </td>
                <td class="first-row-last-column">
                    <a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=filled&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Stats</a>
                </td>
            </tr>
             <?php if (!$_smarty_tpl->getVariable('newsGroup')->value){?>
            <tr nodrag="true" nodrop="true">
                <td class="first-column-gray">
                    Er zijn nog geen nieuwsgroepen toegevoegd.      
                </td>
                <td class="column-gray">
                    
                </td>
                <td class="last-column-gray">
                    &nbsp;
                </td>
            </tr>
            <?php }?>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('newsGroup')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']++;
?>
				
            <tr id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" order_number="<?php echo $_smarty_tpl->tpl_vars['item']->value['order_number'];?>
" ondblclick="window.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
'" class="pointer">
            
                <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter']['iteration']%2==1){?>   
                
                <td class="first-column-gray">
                    <?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>

                </td>
                <td class="column-gray">
                </td>
                <td class="last-column-gray">
                </td>
                <?php }else{ ?>
                <td class="first-column-white">
                    <?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>

                </td>
                <td class="column-white">
                </td>
                <td class="last-column-white">
                </td>
                <?php }?>
            </tr>
            <?php }} ?>
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        <?php echo $_smarty_tpl->getVariable('links')->value;?>

    </div>
<?php }elseif($_GET['action']=='edit'){?>
<script>

var items_1 = 0;
var items_2 = 0;
$ (document).ready (function (){
    
    $ ("#items_1,#items_2").delegate(".remove", "click", function() {
        
        //alert (CKEDITOR.instances[$ (this).attr ('content_id')]);
        
        CKEDITOR.instances[$ (this).attr ('content_id')].destroy();
        $ (this).parent ().parent ().parent ().parent ().parent ().remove ();
    });
    $ ('.additem').click (function (){
        $ ('#' + $ (this).attr ('block-id')).append ('<div class="item"><table style="width:100%;"><tr><td class="fieldName"><label id="items_1_title' + items_1 + '">Titel:</label><a href="javascript:void(0)" class="save-button"  onclick="document.forms[0].action=\'#items_1_title' + items_1 + '\';document.forms[0].submit();" >opslaan</a><br /></td></tr><tr><td class="field"><input class="input" type="text" name="items_1_title[' + items_1 + ']" value="" /> <a class="remove" content_id="items_1_t' + items_1 + '">x</a></td></tr><tr><td class="bar"></td></tr><tr><td class="fieldName"><label id="#items_1_text' + items_1 + '">Text:</label><a href="javascript:void(0)" class="save-button"  onclick="document.forms[0].action=\'#items_1_text' + items_1 + '\';document.forms[0].submit();" >opslaan</a><br /></td></tr><tr><td><textarea id="items_1_t' + items_1 + '" name="items_1_text[' + items_1 + ']"></textarea></td></tr><tr><td class="bar"></td></tr></table><br /></div>');
        CKEDITOR.replace( 'items_1_t' + items_1, {toolbar :'MyToolbar',enterMode : CKEDITOR.ENTER_BR} );
        
        items_1++;
    });
    
    $ ('.additem2').click (function (){
        $ ('#' + $ (this).attr ('block-id')).append ('<div><table style="width:100%;"><tr><td class="fieldName"><label id="items_2_title' + items_2 + '">Titel:</label><a class="save-button"  onclick="document.forms[0].action=\'#items_2_title' + items_2 + '\';document.forms[0].submit();" >opslaan</a><br /></td></tr><tr><td class="field"><input class="input" type="text" name="items_2_title[' + items_2 + ']" value="" /> <a class="remove" content_id="items_2_t' + items_2 + '">x</a></td></tr><tr><td class="bar"></td></tr><tr><td class="fieldName"><label id="items_2_text' + items_2 + '">Text:</label><a class="save-button"  onclick="document.forms[0].action=\'#items_2_text' + items_2 + '\';document.forms[0].submit();" >opslaan</a><br /></td></tr><tr><td class="field"><textarea id="items_2_t' + items_2 + '" name="items_2_text[' + items_2 + ']"></textarea></td></tr><tr><td class="bar"></td></tr></table><br /></div>');
        CKEDITOR.replace( 'items_2_t' + items_2, {toolbar :'MyToolbar',enterMode : CKEDITOR.ENTER_BR} );
        items_2++;
    });
});

</script>
<div id="content_in_content_div">
    <div class="pagina-form modules newsletter">
        <div id="pagina-title" class="pagina-titel"><?php echo $_smarty_tpl->getVariable('folder')->value['text'];?>
</div>   
        <div id="content_in_content_div">
            <form method="post" action="" id="templateForm" >
        		<table style="width: 100%;">
                    <tbody>
                    <?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?>
        			<tr>
        				<td>
        					De volgende velden zijn niet (goed) ingevuld: <br />
        					<ol>
        					<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?>
        						<li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li>
        					<?php }} ?>
        					</ol>
        				</td>
        			</tr>
        			<?php }?> 
                    <tr>
                        <td class="fieldName">
                            <label id="logo">Logo:</label>
                            <a class="save-button"  onclick="document.forms[0].action='#logo';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=image_logo&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_logo'])&&$_smarty_tpl->getVariable('private')->value['image_logo']==1){?>0<?php }else{ ?>1<?php }?>#image_logo">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_logo'])&&$_smarty_tpl->getVariable('private')->value['image_logo']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_logo" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_logo" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_logo" ></span><br class="clear" />
                            <span id="image_logo">
                            <?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['image_logo'])){?>
                                <?php  $_smarty_tpl->tpl_vars['photoOid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('newsGroup')->value['data']['image_logo']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photoOid']->key => $_smarty_tpl->tpl_vars['photoOid']->value){
?>
                                    <img id="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
/100/image.php" />
                                    <input name="image_logo[<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" type="hidden" />
                                <?php }} ?>
                            <?php }?>
                            </span><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
                        <td class="fieldName">
                            <label id="mail_logo">Mail Logo:</label>
                            <a class="save-button"  onclick="document.forms[0].action='#mail_logo';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=image_maillogo&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_maillogo'])&&$_smarty_tpl->getVariable('private')->value['image_maillogo']==1){?>0<?php }else{ ?>1<?php }?>#image_maillogo">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_maillogo'])&&$_smarty_tpl->getVariable('private')->value['image_maillogo']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_maillogo" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_maillogo" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_maillogo" ></span><br class="clear" />
                            <span id="image_maillogo">
                            <?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['image_maillogo'])){?>
                                <?php  $_smarty_tpl->tpl_vars['photoOid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('newsGroup')->value['data']['image_maillogo']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photoOid']->key => $_smarty_tpl->tpl_vars['photoOid']->value){
?>
                                    <img id="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
/100/image.php" />
                                    <input name="image_maillogo[<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" type="hidden" />
                                <?php }} ?>
                            <?php }?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_1">Tekst 1:</label>
                            <a class="save-button"  onclick="document.forms[0].action='#text_1';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=text_text1&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text1'])&&$_smarty_tpl->getVariable('private')->value['text_text1']==1){?>0<?php }else{ ?>1<?php }?>#text_text1">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text1'])&&$_smarty_tpl->getVariable('private')->value['text_text1']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value['text_text1'];?>

        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_1" id="title_1">Titel 2 (#titel_1):</label><a class="save-button"  onclick="document.forms[0].action='#title_1';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=title_1&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_1'])&&$_smarty_tpl->getVariable('private')->value['title_1']==1){?>0<?php }else{ ?>1<?php }?>#title_1">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_1'])&&$_smarty_tpl->getVariable('private')->value['title_1']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_1" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['title_1'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['title_1'];?>
<?php }?>" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_2">Tekst 2:</label><a class="save-button"  onclick="document.forms[0].action='#text_2';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=text_text2&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text2'])&&$_smarty_tpl->getVariable('private')->value['text_text2']==1){?>0<?php }else{ ?>1<?php }?>#text_text2">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text2'])&&$_smarty_tpl->getVariable('private')->value['text_text2']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value['text_text2'];?>

        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_3">Tekst 3:</label><a class="save-button"  onclick="document.forms[0].action='#text_3';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=text_text3&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text3'])&&$_smarty_tpl->getVariable('private')->value['text_text3']==1){?>0<?php }else{ ?>1<?php }?>#text_text3">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text3'])&&$_smarty_tpl->getVariable('private')->value['text_text3']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value['text_text3'];?>

        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_2" id="title_2">Titel 3 (#titel_2):</label><a class="save-button"  onclick="document.forms[0].action='#title_2';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=title_2&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_2'])&&$_smarty_tpl->getVariable('private')->value['title_2']==1){?>0<?php }else{ ?>1<?php }?>#title_2">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_2'])&&$_smarty_tpl->getVariable('private')->value['title_2']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_2" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['title_2'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['title_2'];?>
<?php }?>" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
                        <td class="fieldName">
                            <label id="photo_1">Foto tekst 4:</label><a class="save-button"  onclick="document.forms[0].action='#photo_1';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=image_photo&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_photo'])&&$_smarty_tpl->getVariable('private')->value['image_photo']==1){?>0<?php }else{ ?>1<?php }?>#image_photo">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_photo'])&&$_smarty_tpl->getVariable('private')->value['image_photo']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_photo" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_photo" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_photo"></span><br class="clear" />
                            <span id="image_photo">
                            <?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['image_photo'])){?>
                                <?php  $_smarty_tpl->tpl_vars['photoOid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('newsGroup')->value['data']['image_photo']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photoOid']->key => $_smarty_tpl->tpl_vars['photoOid']->value){
?>
                                    <img id="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
/100/image.php" />
                                    <input name="image_photo[<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" type="hidden" />
                                <?php }} ?>
                            <?php }?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_3" id="title_3">Titel foto tekst 4 (#titel_3):</label><a class="save-button"  onclick="document.forms[0].action='#title_3';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=title_3&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_3'])&&$_smarty_tpl->getVariable('private')->value['title_3']==1){?>0<?php }else{ ?>1<?php }?>#title_3">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_3'])&&$_smarty_tpl->getVariable('private')->value['title_3']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_3" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['title_3'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['title_3'];?>
<?php }?>" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_4">Tekst 4:</label><a class="save-button"  onclick="document.forms[0].action='#text_4';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=text_text4&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text4'])&&$_smarty_tpl->getVariable('private')->value['text_text4']==1){?>0<?php }else{ ?>1<?php }?>#text_text4">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text4'])&&$_smarty_tpl->getVariable('private')->value['text_text4']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value['text_text4'];?>

        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_4" id="title_4">Titel 5 (#titel_4):</label><a class="save-button"  onclick="document.forms[0].action='#title_4';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=title_4&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_4'])&&$_smarty_tpl->getVariable('private')->value['title_4']==1){?>0<?php }else{ ?>1<?php }?>#title_4">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_4'])&&$_smarty_tpl->getVariable('private')->value['title_4']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_4" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['title_4'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['title_4'];?>
<?php }?>" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_5">Tekst 5:</label><a class="save-button"  onclick="document.forms[0].action='#text_5';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=text_text5&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text5'])&&$_smarty_tpl->getVariable('private')->value['text_text5']==1){?>0<?php }else{ ?>1<?php }?>#text_text5">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text5'])&&$_smarty_tpl->getVariable('private')->value['text_text5']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value['text_text5'];?>

        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_5" id="title_5">Titel 6 A (#titel_5):</label><a class="save-button"  onclick="document.forms[0].action='#title_5';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=title_5&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_5'])&&$_smarty_tpl->getVariable('private')->value['title_5']==1){?>0<?php }else{ ?>1<?php }?>#title_5">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_5'])&&$_smarty_tpl->getVariable('private')->value['title_5']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_5" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['title_5'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['title_5'];?>
<?php }?>" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
                        <td class="fieldName">
        				    <label id="items_11">Items:</label><a class="save-button"  onclick="document.forms[0].action='#items_11';document.forms[0].submit();" >opslaan</a><a class="additem" block-id="items_1">item toevoegen</a><br /> 
        				</td>
                    </tr>
                    <tr>
                        <td id="items_1">
                            <?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['items_1_title'])){?>
                            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('newsGroup')->value['data']['items_1_title']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                                <div>
                                    <table style="width:100%;">
                                        <tr>
                                            <td class="fieldName"><label id="items_1_title<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">Titel 6 A:</label><a class="save-button"  onclick="document.forms[0].action='#items_1_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
';document.forms[0].submit();" >opslaan</a><br /></td>
                                        </tr>
                                        <tr>
                                            <td class="field"><input class="input" type="text" name="items_1_title[<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
" /> <a class="remove" content_id="items_1_t<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">x</a></td>
                                        </tr>
                                        <tr>
                                            <td class="bar"></td>
                                        </tr>
                                        <tr>
                                            <td class="fieldName"><label id="items_1_text<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">Tekst 6 A:</label><a class="save-button"  onclick="document.forms[0].action='#items_1_text<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
';document.forms[0].submit();" >opslaan</a><br /></td>
                                        </tr>
                                        <tr>
                                            <td class="field">
                                                <textarea id="items_1_t<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" name="items_1_text[<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]"><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['items_1_text'][$_smarty_tpl->getVariable('k')->value];?>
</textarea>
                                                <script>CKEDITOR.replace( 'items_1_t<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
', {toolbar :'MyToolbar',enterMode : CKEDITOR.ENTER_BR} );items_1 = <?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
;</script>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bar"></td>
                                        </tr>
                                    </table>
                                    <br />
                                </div>
                                
                            <?php }} ?>
                            <?php }?>
                        </td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_6" id="title_6">Titel 6 B (#titel_6):</label><a class="save-button"  onclick="document.forms[0].action='#title_6';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=title_6&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_6'])&&$_smarty_tpl->getVariable('private')->value['title_6']==1){?>0<?php }else{ ?>1<?php }?>#title_6">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_6'])&&$_smarty_tpl->getVariable('private')->value['title_6']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_6" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['title_6'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['title_6'];?>
<?php }?>" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_6">Tekst 6 B:</label><a class="save-button"  onclick="document.forms[0].action='#text_6';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=text_text6&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text6'])&&$_smarty_tpl->getVariable('private')->value['text_text6']==1){?>0<?php }else{ ?>1<?php }?>#text_text6">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text6'])&&$_smarty_tpl->getVariable('private')->value['text_text6']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value['text_text6'];?>

        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_7" id="title_7">Titel 7 (#titel_7):</label><a class="save-button"  onclick="document.forms[0].action='#title_7';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=title_7&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_7'])&&$_smarty_tpl->getVariable('private')->value['title_7']==1){?>0<?php }else{ ?>1<?php }?>#title_7">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_7'])&&$_smarty_tpl->getVariable('private')->value['title_7']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_7" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['title_7'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['title_7'];?>
<?php }?>" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
                        <td class="fieldName">
                            <label id="photo_2">Foto 7:</label><a class="save-button"  onclick="document.forms[0].action='#photo_2';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=image_1&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_1'])&&$_smarty_tpl->getVariable('private')->value['image_1']==1){?>0<?php }else{ ?>1<?php }?>#image_1">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_1'])&&$_smarty_tpl->getVariable('private')->value['image_1']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_1" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_1" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_1" ></span><br class="clear" />
                            <span id="image_1">
                            <?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['image_1'])){?>
                                <?php  $_smarty_tpl->tpl_vars['photoOid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('newsGroup')->value['data']['image_1']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photoOid']->key => $_smarty_tpl->tpl_vars['photoOid']->value){
?>
                                    <img id="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
/100/image.php" />
                                    <input name="image_1[<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" type="hidden" />
                                <?php }} ?>
                            <?php }?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_7">Tekst 7:</label><a class="save-button"  onclick="document.forms[0].action='#text_7';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=text_text7&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text7'])&&$_smarty_tpl->getVariable('private')->value['text_text7']==1){?>0<?php }else{ ?>1<?php }?>#text_text7">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text7'])&&$_smarty_tpl->getVariable('private')->value['text_text7']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value['text_text7'];?>

        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_8" id="title_8">Titel 8 (#titel_8):</label><a class="save-button"  onclick="document.forms[0].action='#title_8';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=title_8&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_8'])&&$_smarty_tpl->getVariable('private')->value['title_8']==1){?>0<?php }else{ ?>1<?php }?>#title_8">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_8'])&&$_smarty_tpl->getVariable('private')->value['title_8']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_8" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['title_8'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['title_8'];?>
<?php }?>" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
                        <td class="fieldName">
        				    <label id="items_22">Items 2:</label><a class="save-button"  onclick="document.forms[0].action='#items_22';document.forms[0].submit();" >opslaan</a><a class="additem2" block-id="items_2">item toevoegen</a><br /> 
        				</td>
                    </tr>
                    <tr>
                        <td id="items_2">
                            <?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['items_2_title'])){?>
                            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('newsGroup')->value['data']['items_2_title']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                                <div>
                                    <table style="width:100%;">
                                        <tr>
                                            <td class="fieldName"><label id="items_2_title<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">Titel 6 C:</label><a class="save-button"  onclick="document.forms[0].action='#items_2_title<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
';document.forms[0].submit();" >opslaan</a><br /></td>
                                        </tr>
                                        <tr>
                                            <td class="field"><input class="input" type="text" name="items_2_title[<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
" /> <a class="remove" content_id="items_2_t<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">x</a></td>
                                        </tr>
                                        <tr>
                                            <td class="bar"></td>
                                        </tr>
                                        <tr>
                                            <td class="fieldName" id="items_2_text<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><label>Tekst 6 C:</label><a class="save-button"  onclick="document.forms[0].action='#items_2_text<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
';document.forms[0].submit();" >opslaan</a><br /></td>
                                        </tr>
                                        <tr>
                                            <td class="field">
                                                <textarea id="items_2_t<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" name="items_2_text[<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]"><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['items_2_text'][$_smarty_tpl->getVariable('k')->value];?>
</textarea>
                                                <script>CKEDITOR.replace( 'items_2_t<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
', {toolbar :'MyToolbar',enterMode : CKEDITOR.ENTER_BR} );items_2 = <?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
;</script>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bar"></td>
                                        </tr>
                                    </table>
                                    <br />
                                </div>
                            <?php }} ?>
                            <?php }?>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldName">
                            <label id="photo_3">Foto:</label><a class="save-button"  onclick="document.forms[0].action='#photo_3';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=image_2&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_2'])&&$_smarty_tpl->getVariable('private')->value['image_2']==1){?>0<?php }else{ ?>1<?php }?>#image_2">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_2'])&&$_smarty_tpl->getVariable('private')->value['image_2']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_2" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_2" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_2" ></span><br class="clear" />
                            <span id="image_2">
                            <?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['image_2'])){?>
                                <?php  $_smarty_tpl->tpl_vars['photoOid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('newsGroup')->value['data']['image_2']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photoOid']->key => $_smarty_tpl->tpl_vars['photoOid']->value){
?>
                                    <img id="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
/100/image.php" />
                                    <input name="image_2[<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" type="hidden" />
                                <?php }} ?>
                            <?php }?>
                            </span>
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_9" id="title_9">Titel 9 (#titel_9):</label><a  class="save-button"  onclick="document.forms[0].action='#title_9';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=title_9&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_9'])&&$_smarty_tpl->getVariable('private')->value['title_9']==1){?>0<?php }else{ ?>1<?php }?>#title_9">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_9'])&&$_smarty_tpl->getVariable('private')->value['title_9']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_9" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['title_9'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['title_9'];?>
<?php }?>" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_8">Tekst 8:</label><a class="save-button"  onclick="document.forms[0].action='#text_8';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=text_text8&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text8'])&&$_smarty_tpl->getVariable('private')->value['text_text8']==1){?>0<?php }else{ ?>1<?php }?>#text_text8">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text8'])&&$_smarty_tpl->getVariable('private')->value['text_text8']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value['text_text8'];?>

        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_9">Tekst 9:</label><a class="save-button"  onclick="document.forms[0].action='#text_9';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=text_text8&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text9'])&&$_smarty_tpl->getVariable('private')->value['text_text9']==1){?>0<?php }else{ ?>1<?php }?>#text_text9">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['text_text9'])&&$_smarty_tpl->getVariable('private')->value['text_text9']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value['text_text9'];?>

        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_10" id="title_10">Titel 10 (#titel_10):</label><a  class="save-button"  onclick="document.forms[0].action='#title_10';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=title_10&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_10'])&&$_smarty_tpl->getVariable('private')->value['title_10']==1){?>0<?php }else{ ?>1<?php }?>#title_10">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_10'])&&$_smarty_tpl->getVariable('private')->value['title_10']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_10" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['title_10'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['title_10'];?>
<?php }?>" />
        				</td>
        			</tr>
                    <tr>
                        <td class="fieldName">
                            <label id="footer_1">Footer 1:</label><a class="save-button"  onclick="document.forms[0].action='#footer_1';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=image_f1&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_f1'])&&$_smarty_tpl->getVariable('private')->value['image_f1']==1){?>0<?php }else{ ?>1<?php }?>#image_f1">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_f1'])&&$_smarty_tpl->getVariable('private')->value['image_f1']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_f1" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_f1" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_f1" ></span><br class="clear" />
                            <span id="image_f1">
                            <?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['image_f1'])){?>
                                <?php  $_smarty_tpl->tpl_vars['photoOid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('newsGroup')->value['data']['image_f1']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photoOid']->key => $_smarty_tpl->tpl_vars['photoOid']->value){
?>
                                    <img id="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
/100/image.php" />
                                    <input name="image_f1[<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" type="hidden" />
                                <?php }} ?>
                            <?php }?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_11" id="title_11">Titel 11 (#titel_11):</label><a  class="save-button"  onclick="document.forms[0].action='#title_11';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=title_11&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_11'])&&$_smarty_tpl->getVariable('private')->value['title_11']==1){?>0<?php }else{ ?>1<?php }?>#title_11">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['title_11'])&&$_smarty_tpl->getVariable('private')->value['title_11']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_11" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['title_11'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['data']['title_11'];?>
<?php }?>" />
        				</td>
        			</tr>
                    <tr>
                        <td class="fieldName">
                            <label id="footer_2">Footer 2:</label><a class="save-button"  onclick="document.forms[0].action='#footer_2';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
newsletter/templates.php?action=private&id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
&field=image_f2&value=<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_f2'])&&$_smarty_tpl->getVariable('private')->value['image_f2']==1){?>0<?php }else{ ?>1<?php }?>#image_f2">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/<?php if (isset($_smarty_tpl->getVariable('private',null,true,false)->value['image_f2'])&&$_smarty_tpl->getVariable('private')->value['image_f2']==1){?>disable<?php }else{ ?>enable<?php }?>.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_f2" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_f2" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_f2" ></span><br class="clear" />
                            <span id="image_f2">
                            <?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['data']['image_f2'])){?>
                                <?php  $_smarty_tpl->tpl_vars['photoOid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('newsGroup')->value['data']['image_f2']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photoOid']->key => $_smarty_tpl->tpl_vars['photoOid']->value){
?>
                                    <img id="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
/100/image.php" />
                                    <input name="image_f2[<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" type="hidden" />
                                <?php }} ?>
                            <?php }?>
                            </span>
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title">Template:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="title" id="title" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value['title'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value['title'];?>
<?php }?>" size="40" title="Max 100 tekens" maxlength="101" />
        				</td>
        			</tr>
        			<?php if (isset($_smarty_tpl->getVariable('send_test',null,true,false)->value)){?>
                    <tr>
        				<td class="fieldName" colspan="2">
        				    Test verzenden Succed
                        </td>
        			</tr>
                    <?php }?>
                    <tr>
        				<td class="fieldName">
        				    <label for="subject">Onderwerp:</label>&nbsp;&nbsp;&nbsp;(Mag niet meer dan 78 tekens bedragen)
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="subject" id="subject" value="<?php if (isset($_POST['subject'])){?><?php echo $_POST['subject'];?>
<?php }else{ ?>Test<?php }?>" size="40"  maxlength="101" /> 
        				</td>
        			</tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="fname">Van naam:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="send_fname" id="fname" value="<?php if (isset($_POST['send_fname'])){?><?php echo $_POST['send_fname'];?>
<?php }else{ ?>Inforitus<?php }?>" size="40"  maxlength="101" /><?php if (isset($_POST['send'])&&isset($_smarty_tpl->getVariable('send_v',null,true,false)->value)&&$_smarty_tpl->getVariable('send_v')->value['name']==false){?><span style="color:red;">* Verplicht</span><?php }?>
        				</td>
        			</tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="femail">Van email:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="send_femail" id="femail" value="<?php if (isset($_POST['send_femail'])){?><?php echo $_POST['send_femail'];?>
<?php }else{ ?>info@inforitus.nl<?php }?>" size="40"  maxlength="101" /><?php if (isset($_POST['send'])&&isset($_smarty_tpl->getVariable('send_v',null,true,false)->value)&&$_smarty_tpl->getVariable('send_v')->value['email']==false){?><span style="color:red;">* Verplicht</span><?php }?>
        				</td>
        			</tr>             
                    <tr>
        				<td class="fieldName">
        				    <label for="name">Naam:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="send_name" id="name" value="<?php if (isset($_POST['send_name'])){?><?php echo $_POST['send_name'];?>
<?php }?>" size="40"  maxlength="101" />
        				</td>
        			</tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="email">Email:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="send_email" id="email" value="<?php if (isset($_POST['send_email'])){?><?php echo $_POST['send_email'];?>
<?php }?>" size="40"  maxlength="101" />
        			    </td>
        			</tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="gender">Geslacht:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="send_gender" id="gender" value="<?php if (isset($_POST['send_gender'])){?><?php echo $_POST['send_gender'];?>
<?php }?>" size="40"  maxlength="101" /><br />
        				    <input type="submit" name="send_test" value="Test verzenden" class="send_test" />
                            
                        </td>
        			</tr>
                    <tr>
                        <td class="fieldName">
                            <label for="group_id">Group subscriber:</label> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <select name="group_id" id="group_id">
                                <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('groups')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
" <?php if (isset($_POST['group_id'])&&$_POST['group_id']==$_smarty_tpl->tpl_vars['value']->value['id']){?> selected="selected" <?php }?> ><?php echo stripslashes($_smarty_tpl->tpl_vars['value']->value['group_name']);?>
</option>
                                <?php }} ?>
                            </select><br />
                        </td>
                    </tr>
                    <?php if (isset($_smarty_tpl->getVariable('links',null,true,false)->value)){?>
                    <tr>
                        <td class="fieldName">
                            <label for="links">Links:</label> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('links')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
                                <?php if ('{unsubscribe}'!=smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['value']->value,'href="',''),'"','')){?>
                            <input type="checkbox" name="links[<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]" value="<?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['value']->value,'href="',''),'"','');?>
" <?php if (isset($_POST['links'])&&in_array($_smarty_tpl->tpl_vars['value']->value,$_POST['links'])){?> selected="selected" <?php }?> /> <?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['value']->value,'href="',''),'"','');?>
<br />
                                <?php }?>
                            <?php }} ?>
                            <input type="submit" name="send" value="Verzenden" class="send" /><br />
                            <div id="progressbar"></div>
                        </td>
                    </tr>
                    <?php }?>
                    <tr>
        				<td class="field">
                            <br />
                            <input type="hidden" id="module"  value="newsletter" name="module"  />
                            <input type="hidden" id="template"  value="<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
" name="module"  />
        				    <a href="#" onclick="document.forms[0].submit();" id="save"></a>
                            <a class="verwijderen" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/newsletter/templates.php?action=delete&amp;id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
" onclick="return confirm('Weet u zeker dat u deze nieuwsgroep wilt verwijderen?'); return false;"></a>
                        </td>
        			</tr>
                    <tr>
                        <td class="fieldName">
                            Template naam:
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <input type="text" name="template_name" value="" class="input" />
                        </td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="new_template" class="new-template" value="Opslaan als nieuw template" /></td>
                    </tr>
        		</table>
       	     </form>
             
        </div>
        <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/pagina_bottom.jpg" alt="" class="pagina_bottom" />
    </div>
    <div class="pagina-form modules template" style="width:600px">
        <div id="pagina-title" class="pagina-titel" style="width:576px;"><?php echo $_smarty_tpl->getVariable('folder')->value['text'];?>
 <a href="/mail/<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
.html" target="_blank">Klik hier om het template los in de browser te bekijken</a></div>   
        <div id="content_in_content_div" style="margin:0px;padding:0px;">
            <iframe style="border:none;" src="/mail/<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
.html" width="600" height="3500"></iframe> 
        </div>
    </div>
</div>
<script>
$ (document).ready (function (){
    setTimeout("Jump()",1000);
});
function Jump() {
    if (window.location.hash != '')
        location.href=window.location.hash;
}

</script>
<?php }?>
