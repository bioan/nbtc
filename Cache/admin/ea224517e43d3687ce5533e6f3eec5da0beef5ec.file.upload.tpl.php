<?php /* Smarty version Smarty-3.0.7, created on 2013-02-21 12:19:15
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/photoalbum/upload.tpl" */ ?>
<?php /*%%SmartyHeaderCode:688607342512602b316c8f4-22855544%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ea224517e43d3687ce5533e6f3eec5da0beef5ec' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/photoalbum/upload.tpl',
      1 => 1321903688,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '688607342512602b316c8f4-22855544',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript">/* <![CDATA[ */
  function showDiv($divId)
  {
    var $obj = document.getElementById($divId);
    $obj.style.display = 'block';
  }
  
	function checkForm()
	{
		var obj = document.getElementById('photo_area');
		if (!obj.hasChildNodes()) {
			alert('Foto\'s toevoegen!');
			return false;	
		}
		for (var i=0; i < obj.childNodes.length; i++) {
			if (obj.childNodes.item(i).nodeName.match(/^IMG$/))
				return true;
		}
		alert('Foto\'s toevoegen!');
		return false;	
	}

  	function checkIfDisabled(obj)
	{
		if (obj.options[obj.selectedIndex].disabled == true) {
			alert('Deze categorie kan niet gekozen worden omdat deze midden in de tree zit!');
			for (var i =0; i < obj.options.length; i++) {
				if (!obj.options[i].disabled) {
					obj.options[i].selected = true;
					return;
				}
			}
			
		}
	}
  /* ]]> */</script><form method="post" action="" enctype="multipart/form-data"><table style="width: 100%;"><colgroup style="width: 10%"></colgroup><colgroup style="width: 90%"></colgroup><tbody><?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?><tr><td colspan="2">De volgende velden zijn niet (goed) ingevuld: <br /><ol><?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?><li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li><?php }} ?></ol></td></tr><?php }?><tr><td class="formoptieNaam"><label for="category">Categorie</label>:</td><td><select name="category" id="category" onchange="checkIfDisabled(this);"><?php if (isset($_smarty_tpl->getVariable('nodes',null,true,false)->value)){?><?php  $_smarty_tpl->tpl_vars['node'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('nodes')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['node']->key => $_smarty_tpl->tpl_vars['node']->value){
?><?php if (in_array($_smarty_tpl->tpl_vars['node']->value['oid'],$_smarty_tpl->getVariable('leafNodes')->value)&&$_smarty_tpl->tpl_vars['node']->value['oid']!=0){?><option value="<?php echo $_smarty_tpl->tpl_vars['node']->value['oid'];?>
" <?php if (isset($_POST['category'])&&$_POST['category']==$_smarty_tpl->tpl_vars['node']->value['oid']){?> selected="selected" <?php }?>><?php echo str_repeat("&nbsp;",$_smarty_tpl->tpl_vars['node']->value['level']*2);?>
<?php echo stripslashes($_smarty_tpl->tpl_vars['node']->value['name']);?>
</option><?php }else{ ?><option value="<?php echo $_smarty_tpl->tpl_vars['node']->value['oid'];?>
" disabled="disabled" style="color: #CCCCCC;"><?php echo str_repeat("&nbsp;",$_smarty_tpl->tpl_vars['node']->value['level']*2);?>
<?php echo stripslashes($_smarty_tpl->tpl_vars['node']->value['name']);?>
</option><?php }?><?php }} ?><?php }else{ ?><option value="0">Er zijn geen categorie&euml;n aangemaakt.</option><?php }?></select></td></tr><tr><td class="formoptieNaam"><label for="category">Zip</label>:</td><td><input type="file" name="zip" value="" /></td></tr><tr><td></td><td><br /><input type="image" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/save.png"></td></tr></tbody></table></form>
