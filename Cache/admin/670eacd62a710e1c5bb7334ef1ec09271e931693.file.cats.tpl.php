<?php /* Smarty version Smarty-3.0.7, created on 2013-02-21 12:19:04
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/photoalbum/cats.tpl" */ ?>
<?php /*%%SmartyHeaderCode:597183751512602a8836873-62600235%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '670eacd62a710e1c5bb7334ef1ec09271e931693' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/photoalbum/cats.tpl',
      1 => 1330951305,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '597183751512602a8836873-62600235',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript">
/* <![CDATA[ */
	function checkIfDisabled(obj)
	{
		if (obj.options[obj.selectedIndex].disabled == true) {
			alert('Deze categorie kan niet gekozen worden omdat deze midden in de tree zit!');	
			for (var i =0; i < obj.options.length; i++) {
				if (!obj.options[i].disabled) {
					obj.options[i].selected = true;
					return;
				}
			}
			
		}
	}
	
  
/* ]]> */
</script>
<?php if ($_GET['action']=='new'){?>
    <form method="post" action="">
        <table style="width: 100%;">
            <colgroup style="width: 30%"></colgroup>
            <colgroup style="width: 70%"></colgroup>
            <tbody>
            <?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?>
            <tr>
                <td colspan="2">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?>
                        <li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li>
                    <?php }} ?>
                    </ol>
                </td>
            </tr>
            <?php }?>
            
            <tr>
                <td class="fieldName">
                    <label for="private">Private</label>:
                </td>
                <td>
                    <input type="checkbox" value="1" name="private" id="private" <?php if (isset($_POST['private'])&&$_POST['private']==1){?> checked="checked" <?php }?> />       
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="category">Onder categorie</label>:
                </td>
                <td>
                    <select name="category" onchange="checkIfDisabled(this);">
                        <?php  $_smarty_tpl->tpl_vars['node'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('Trees')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['node']->key => $_smarty_tpl->tpl_vars['node']->value){
?>
                            <?php if ($_smarty_tpl->tpl_vars['node']->value['level']==0){?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['node']->value['oid'];?>
" style="font-weight: bold;" <?php if (isset($_POST['category'])&&$_POST['category']==$_smarty_tpl->tpl_vars['node']->value['oid']){?> selected="selected" <?php }?>><?php echo stripslashes($_smarty_tpl->tpl_vars['node']->value['name']);?>
</option>
                            <?php }elseif($_smarty_tpl->tpl_vars['node']->value['level']>0&&$_smarty_tpl->tpl_vars['node']->value['level']!=$_smarty_tpl->getVariable('settings')->value['subcats']+intval(1)){?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['node']->value['oid'];?>
" <?php if ($_smarty_tpl->tpl_vars['node']->value['level']==3){?>disabled="disabled"<?php }?> style="font-weight: bold;"><?php echo str_repeat("&nbsp;",$_smarty_tpl->tpl_vars['node']->value['level']*2);?>
<?php echo stripslashes($_smarty_tpl->tpl_vars['node']->value['name']);?>
</option>
                            <?php }else{ ?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['node']->value['oid'];?>
" <?php if ($_smarty_tpl->tpl_vars['node']->value['level']==3){?>disabled="disabled"<?php }?> style="color: #CCCCCC;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo stripslashes($_smarty_tpl->tpl_vars['node']->value['name']);?>
</option>
                            <?php }?>
                        <?php }} ?>
                    </select>
                </td>
            </tr>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['id'], null, null);?>
            
            <tr>
				<td class="fieldName">
				    <label>Foto</label>: 
				</td>
				<td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" ></span><br class="clear" />
				    <span id="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">
                    <?php if (isset($_POST['photos'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?>
                        <?php  $_smarty_tpl->tpl_vars['photoOid'] = new Smarty_Variable;
 $_from = $_POST['photos'][$_smarty_tpl->getVariable('id')->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photoOid']->key => $_smarty_tpl->tpl_vars['photoOid']->value){
?>
                            <img id="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
/100/image.php" />
                            <input name="photos[<?php echo $_smarty_tpl->getVariable('id')->value;?>
][<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" type="hidden" />
                        <?php }} ?>
                        <?php }?>
                    </span>
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label id="name_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Naam <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:
                </td>
                <td>
                <input name="name[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" type="text" class="input" id="name_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" value="<?php if (isset($_POST['name'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php echo $_POST['name'][$_smarty_tpl->getVariable('id')->value];?>
<?php }?>" size="40" />
                </td>
            </tr>
            <tr>
				<td class="fieldName">
				    <label for="f_id_2">Bericht <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>: 
				</td>
				<td>
				    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value[$_smarty_tpl->getVariable('id')->value];?>

				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Page description <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:
                </td>
                <td>
                    <textarea name="page_description[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if (isset($_POST['page_description'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php echo $_POST['page_description'][$_smarty_tpl->getVariable('id')->value];?>
<?php }?></textarea>
                </td>
            </tr>
            <?php }} ?>
            <tr>
                <td>
                </td>
				<td>
                    <input type="hidden" id="module"  value="photoalbum" name="module"  />
                    <br />
				    <a href="#" onclick="document.forms[0].submit();"><img name="verzenden" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/save.png" alt="" /></a>
				</td>
			</tr>
    		</tbody>
        </table>
    </form>
<?php }elseif($_GET['action']=='overview'){?>  
    <ul class="tree">
        <li class="first-child">
            <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_left.jpg" id="head_left" alt="" />
            <h2 class="font">Overzicht</h2>
            <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_right.jpg" id="head_right" alt="" />    
        </li>
        <li class="fields">
            <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/first_row_left_white.jpg" class="left" alt="" />
            <div style="width:30%"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/cats.php?action=overview&sort=name&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Categorie</a></div>
            <div style="width:30%"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/cats.php?action=overview&sort=filled&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Stats</a></div>
            <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/first_row_right_white.jpg" class="right" alt="" />
        </li>
    </ul>
    <ul class="tree" id="tmenu">
        <?php if (!$_smarty_tpl->getVariable('existsCats')->value){?>
        <li class="gray">
            <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/first_column_gray_left.jpg" class="left" alt="" />
            <div style="width:30%">Er zijn nog geen categorie&euml;n aangemaakt</div>
            <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/last_column_gray_right.jpg" class="right" alt="" /> 
        </li>
        <?php }?>
        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('parents')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']++;
?>
			<li id="i_1_3_<?php echo $_smarty_tpl->tpl_vars['item']->value['oid'];?>
" class="pointer <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter']['iteration']%2==1){?>gray<?php }else{ ?>white<?php }?>">
                <div class="row" ondblclick="window.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/cats.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['item']->value['oid'];?>
'">
                    <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/first_column_gray_left.jpg" class="left" alt="" />
                    <div style="width:30%"><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/arrow.png" alt="move" width="10" height="10" class="handle-tmenu arrow-drag" /><?php echo str_repeat("&nbsp;",$_smarty_tpl->tpl_vars['item']->value['level']*2);?>
<?php echo stripslashes($_smarty_tpl->tpl_vars['item']->value['name']);?>
</div>
                    <div style="width:30%"><span class="<?php if ($_smarty_tpl->tpl_vars['item']->value['filled']==count($_smarty_tpl->getVariable('languages')->value)){?>green<?php }else{ ?>red<?php }?>"><?php echo $_smarty_tpl->tpl_vars['item']->value['filled'];?>
 / <?php echo count($_smarty_tpl->getVariable('languages')->value);?>
</span></div>
                    <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/last_column_gray_right.jpg" class="right" alt="" />
                </div>
                <?php if (is_array($_smarty_tpl->tpl_vars['item']->value['childs'])&&count($_smarty_tpl->tpl_vars['item']->value['childs'])>0){?>
                <br class="clear" />
                <ul id="t<?php echo $_smarty_tpl->tpl_vars['item']->value['oid'];?>
">
                <?php  $_smarty_tpl->tpl_vars['citem'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['ck'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item']->value['childs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter2"]['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['citem']->key => $_smarty_tpl->tpl_vars['citem']->value){
 $_smarty_tpl->tpl_vars['ck']->value = $_smarty_tpl->tpl_vars['citem']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter2"]['iteration']++;
?>
        			<li id="i_2_<?php echo $_smarty_tpl->tpl_vars['item']->value['oid'];?>
_<?php echo $_smarty_tpl->tpl_vars['citem']->value['oid'];?>
" class="pointer <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter2']['iteration']%2==0){?>gray<?php }else{ ?>white<?php }?>">
                        <div class="row" ondblclick="window.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/cats.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['citem']->value['oid'];?>
'">
                            <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/first_column_gray_left.jpg" class="left" alt="" />
                            <div style="width:30%"><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/arrow.png" alt="move" width="10" height="10" class="handle-t<?php echo $_smarty_tpl->tpl_vars['item']->value['oid'];?>
 arrow-drag" /><?php echo str_repeat("&nbsp;",$_smarty_tpl->tpl_vars['citem']->value['level']*2);?>
<?php echo stripslashes($_smarty_tpl->tpl_vars['citem']->value['name']);?>
</div>
                            <div style="width:30%"><span class="<?php if ($_smarty_tpl->tpl_vars['citem']->value['filled']==count($_smarty_tpl->getVariable('languages')->value)){?>green<?php }else{ ?>red<?php }?>"><?php echo $_smarty_tpl->tpl_vars['citem']->value['filled'];?>
 / <?php echo count($_smarty_tpl->getVariable('languages')->value);?>
</span></div>
                            <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/last_column_gray_right.jpg" class="right" alt="" />
                        </div>
                        <?php if (is_array($_smarty_tpl->tpl_vars['citem']->value['childs'])&&count($_smarty_tpl->tpl_vars['citem']->value['childs'])>0){?>
                        <br class="clear" />
                        <ul id="t<?php echo $_smarty_tpl->tpl_vars['citem']->value['oid'];?>
">
                        <?php  $_smarty_tpl->tpl_vars['ccitem'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['cck'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['citem']->value['childs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter3"]['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['ccitem']->key => $_smarty_tpl->tpl_vars['ccitem']->value){
 $_smarty_tpl->tpl_vars['cck']->value = $_smarty_tpl->tpl_vars['ccitem']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter3"]['iteration']++;
?>
                			<li id="i_2_<?php echo $_smarty_tpl->tpl_vars['citem']->value['oid'];?>
_<?php echo $_smarty_tpl->tpl_vars['ccitem']->value['oid'];?>
" ondblclick="window.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/cats.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['ccitem']->value['oid'];?>
'" class="pointer <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter3']['iteration']%2==0){?>gray<?php }else{ ?>white<?php }?>">
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/first_column_gray_left.jpg" class="left" alt="" />
                                <div style="width:30%"><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/arrow.png" alt="move" width="10" height="10" class="handle-t<?php echo $_smarty_tpl->tpl_vars['citem']->value['oid'];?>
 arrow-drag" /><?php echo str_repeat("&nbsp;",$_smarty_tpl->tpl_vars['ccitem']->value['level']*2);?>
<?php echo stripslashes($_smarty_tpl->tpl_vars['ccitem']->value['name']);?>
</div>
                                <div style="width:30%"><span class="<?php if ($_smarty_tpl->tpl_vars['ccitem']->value['filled']==count($_smarty_tpl->getVariable('languages')->value)){?>green<?php }else{ ?>red<?php }?>"><?php echo $_smarty_tpl->tpl_vars['ccitem']->value['filled'];?>
 / <?php echo count($_smarty_tpl->getVariable('languages')->value);?>
</span></div>
                                <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/last_column_gray_right.jpg" class="right" alt="" />
                            </li>
                        <?php }} ?>
                        </ul>
                        <?php }?>
                    </li>
                <?php }} ?>
                </ul>
                <?php }?> 
            </li>   
        <?php }} ?>
    </ul>
    <ul class="tree">
        <li class="bottom">
            <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/last_row_left.jpg" class="left" alt="" /><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/last_row_right.jpg" class="right" alt="" />
            <div style="width:30%;"></div>        
        </li>
    </ul>
    <div style="padding-top: 0.6em; text-align: center;">
        <?php echo $_smarty_tpl->getVariable('links')->value;?>

    </div>
<?php }elseif($_GET['action']=='edit'){?>
     <form method="post" action="" >
    	<table style="width: 100%;">
    	    <colgroup style="width: 30%"></colgroup>
            <colgroup style="width: 70%"></colgroup>
            <tbody>
            <?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?>
    		<tr>
    			<td colspan="2">
    				De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?>
                        <li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li>
                    <?php }} ?>
                    </ol>
    			</td>
    		</tr>
    		<?php }?>
            
            <tr>
                <td class="fieldName">
                    <label for="private">Private</label>:
                </td>
                <td>
                    <input type="checkbox" value="1" name="private" id="private" <?php if ($_smarty_tpl->getVariable('category')->value['private']==1){?> checked="checked" <?php }?> />
                </td>
            </tr>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['id'], null, null);?>
    		
            <tr>
				<td class="fieldName">
				<label>Afbeelding</label>: 
				</td>
				<td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" ></span><br class="clear" />
					<span id="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">
                    <?php  $_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('category')->value[$_smarty_tpl->getVariable('id')->value]['photos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photo']->key => $_smarty_tpl->tpl_vars['photo']->value){
?>
                    	<?php if (isset($_smarty_tpl->tpl_vars['photo']->value['id'])){?>
	                    	<img id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto/<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
/100/image.jpg" />
							<input name="photos[<?php echo $_smarty_tpl->getVariable('id')->value;?>
][<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
" type="hidden" />
						<?php }?>
					<?php }} ?>
                    </span>
				</td>
			</tr>
            <tr>
    			<td class="fieldName">
    			     <label for="name_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Categorie naam <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:
    			</td>
    			<td>
    			     <input type="text" class="input" name="name[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="name_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" size="40" value="<?php if (isset($_smarty_tpl->getVariable('category',null,true,false)->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['name'])){?><?php echo $_smarty_tpl->getVariable('category')->value[$_smarty_tpl->getVariable('id')->value]['name'];?>
<?php }?>" />
    			 </td>
            </tr>
            <tr>
				<td class="fieldName">
				    <label>Omschrijving <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>: 
				</td>
				<td>
				    <?php echo $_smarty_tpl->getVariable('FCKeditor')->value[$_smarty_tpl->getVariable('id')->value];?>

				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Page description <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:
                </td>
                <td>
                    <textarea name="page_description[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if (isset($_smarty_tpl->getVariable('category',null,true,false)->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['page_description'])){?><?php echo $_smarty_tpl->getVariable('category')->value[$_smarty_tpl->getVariable('id')->value]['page_description'];?>
<?php }?></textarea>
                </td>
            </tr>
            <?php }} ?>
            <tr>
                <td></td>
    			<td>
                    <input type="hidden" id="module"  value="photoalbum" name="module"  />
                    <br />
    				<a href="#" onclick="document.forms[0].submit();" id="save"></a>
                    <a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/photoalbum/cats.php?action=delete&amp;id=<?php echo $_smarty_tpl->getVariable('category')->value['oid'];?>
" onclick="return confirm('Wilt u dit product verwijderen?');" class="verwijderen"></a>
    			</td>
    		</tr>
        </table>
    </form>
<?php }?>
