<?php /* Smarty version Smarty-3.0.7, created on 2012-10-10 09:01:44
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/google/google.tpl" */ ?>
<?php /*%%SmartyHeaderCode:139213925950751d58888da5-65293753%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0393a5423ee405eece0b5ff4377dd0d99cc71de0' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/google/google.tpl',
      1 => 1349852502,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '139213925950751d58888da5-65293753',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<form action="" method="post">
    <table cellspacing="0" cellpadding="0" style="100%">
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 80%"></colgroup>
        <tr>
            <td class="fieldTitle" colspan="2">
               1. Google Connection
            </td>
        </tr>
        <?php if (isset($_smarty_tpl->getVariable('appErrors',null,true,false)->value['app_name'])){?>
        <tr>
            <td class="error" colspan="2">
               <?php echo $_smarty_tpl->getVariable('appErrors')->value['app_name'];?>

            </td>
        </tr>
        <?php }?>
        <tr>
            <td class="fieldName">
                Application Name:
            </td>
            <td>
                <?php if (isset($_POST['app_name'])){?>
                    <?php $_smarty_tpl->tpl_vars["appName"] = new Smarty_variable($_POST['app_name'], null, null);?>
                <?php }elseif(isset($_smarty_tpl->getVariable('appValues',null,true,false)->value['app_name'])){?>
                    <?php $_smarty_tpl->tpl_vars["appName"] = new Smarty_variable($_smarty_tpl->getVariable('appValues')->value['app_name'], null, null);?>
                <?php }else{ ?>
                    <?php $_smarty_tpl->tpl_vars["appName"] = new Smarty_variable('', null, null);?>
                <?php }?>
                <input type="text" class="input" name="app_name" value="<?php echo $_smarty_tpl->getVariable('appName')->value;?>
" size="30">
            </td>
        </tr>
        <?php if (isset($_smarty_tpl->getVariable('appErrors',null,true,false)->value['client_id'])){?>
        <tr>
            <td class="error" colspan="2">
               <?php echo $_smarty_tpl->getVariable('appErrors')->value['client_id'];?>

            </td>
        </tr>
        <?php }?>
        <tr>
            <td class="fieldName">
                Client ID:
            </td>
            <td>
                <?php if (isset($_POST['client_id'])){?>
                    <?php $_smarty_tpl->tpl_vars["clientID"] = new Smarty_variable($_POST['client_id'], null, null);?>
                <?php }elseif(isset($_smarty_tpl->getVariable('appValues',null,true,false)->value['client_id'])){?>
                    <?php $_smarty_tpl->tpl_vars["clientID"] = new Smarty_variable($_smarty_tpl->getVariable('appValues')->value['client_id'], null, null);?>
                <?php }else{ ?>
                    <?php $_smarty_tpl->tpl_vars["clientID"] = new Smarty_variable('', null, null);?>
                <?php }?>
                <input type="text" class="input" name="client_id" value="<?php echo $_smarty_tpl->getVariable('clientID')->value;?>
" size="30">
            </td>
        </tr>
        <?php if (isset($_smarty_tpl->getVariable('appErrors',null,true,false)->value['client_secret'])){?>
        <tr>
            <td class="error" colspan="2">
               <?php echo $_smarty_tpl->getVariable('appErrors')->value['client_secret'];?>

            </td>
        </tr>
        <?php }?>
        <tr>
            <td class="fieldName">
                Client Secret:
            </td>
            <td>
                <?php if (isset($_POST['client_secret'])){?>
                    <?php $_smarty_tpl->tpl_vars["clientSecret"] = new Smarty_variable($_POST['client_secret'], null, null);?>
                <?php }elseif(isset($_smarty_tpl->getVariable('appValues',null,true,false)->value['client_secret'])){?>
                    <?php $_smarty_tpl->tpl_vars["clientSecret"] = new Smarty_variable($_smarty_tpl->getVariable('appValues')->value['client_secret'], null, null);?>
                <?php }else{ ?>
                    <?php $_smarty_tpl->tpl_vars["clientSecret"] = new Smarty_variable('', null, null);?>
                <?php }?>
                <input type="text" class="input" name="client_secret" value="<?php echo $_smarty_tpl->getVariable('clientSecret')->value;?>
" size="30">
            </td>
        </tr>
        <?php if (isset($_smarty_tpl->getVariable('appErrors',null,true,false)->value['redirect_uri'])){?>
        <tr>
            <td class="error" colspan="2">
               <?php echo $_smarty_tpl->getVariable('appErrors')->value['redirect_uri'];?>

            </td>
        </tr>
        <?php }?>
        <tr>
            <td class="fieldName">
                Redirect Url:
            </td>
            <td>
                <?php if (isset($_POST['redirect_uri'])){?>
                    <?php $_smarty_tpl->tpl_vars["redirectUrl"] = new Smarty_variable($_POST['redirect_uri'], null, null);?>
                <?php }elseif(isset($_smarty_tpl->getVariable('appValues',null,true,false)->value['redirect_uri'])){?>
                    <?php $_smarty_tpl->tpl_vars["redirectUrl"] = new Smarty_variable($_smarty_tpl->getVariable('appValues')->value['redirect_uri'], null, null);?>
                <?php }else{ ?>
                    <?php $_smarty_tpl->tpl_vars["redirectUrl"] = new Smarty_variable('', null, null);?>
                <?php }?>
                <input type="text" class="input" name="redirect_uri" value="<?php echo $_smarty_tpl->getVariable('redirectUrl')->value;?>
" size="30">
            </td>
        </tr>
        <?php if (isset($_smarty_tpl->getVariable('appErrors',null,true,false)->value['developer_key'])){?>
        <tr>
            <td class="error" colspan="2">
               <?php echo $_smarty_tpl->getVariable('appErrors')->value['developer_key'];?>

            </td>
        </tr>
        <?php }?>
        <tr>
            <td class="fieldName">
                Developer key:
            </td>
            <td>
                <?php if (isset($_POST['developer_key'])){?>
                    <?php $_smarty_tpl->tpl_vars["developerKey"] = new Smarty_variable($_POST['developer_key'], null, null);?>
                <?php }elseif(isset($_smarty_tpl->getVariable('appValues',null,true,false)->value['developer_key'])){?>
                    <?php $_smarty_tpl->tpl_vars["developerKey"] = new Smarty_variable($_smarty_tpl->getVariable('appValues')->value['developer_key'], null, null);?>
                <?php }else{ ?>
                    <?php $_smarty_tpl->tpl_vars["developerKey"] = new Smarty_variable('', null, null);?>
                <?php }?>
                <input type="text" class="input" name="developer_key" value="<?php echo $_smarty_tpl->getVariable('developerKey')->value;?>
" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName" colspan="2">
                <input type="submit" value="Save Changes" name="saveChangesApp" />
                <?php if (isset($_smarty_tpl->getVariable('url',null,true,false)->value)&&$_smarty_tpl->getVariable('url')->value!=''){?>
                <a href="<?php echo $_smarty_tpl->getVariable('url')->value;?>
" title="connect to Google">Grant permissions</a>
                <?php }else{ ?>
                You have access for Google application
                <?php }?>
           </td>
        </tr>
    </table>
</form>
<?php if (isset($_smarty_tpl->getVariable('user',null,true,false)->value)){?>
<table>
    <tr>
        <td class="fieldTitle" colspan="2">
           2. Google User Data
        </td>
    </tr>
    <?php  $_smarty_tpl->tpl_vars['prop'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('user')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['prop']->key => $_smarty_tpl->tpl_vars['prop']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['prop']->key;
?>
    <tr>
        <td class="fieldName">
            <?php echo $_smarty_tpl->tpl_vars['k']->value;?>

        </td>
        <td>
            <?php if ($_smarty_tpl->tpl_vars['k']->value=='picture'){?>
                <img src="<?php echo $_smarty_tpl->tpl_vars['prop']->value;?>
" alt="" />
            <?php }else{ ?>
                <?php echo $_smarty_tpl->tpl_vars['prop']->value;?>

            <?php }?>
        </td>
    </tr>
    <?php }} ?>
</table>
<?php }?>