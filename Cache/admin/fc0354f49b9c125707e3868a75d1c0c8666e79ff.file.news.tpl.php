<?php /* Smarty version Smarty-3.0.7, created on 2013-03-08 11:54:46
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/news/news.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19934679295139c37643ef92-48856455%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fc0354f49b9c125707e3868a75d1c0c8666e79ff' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/news/news.tpl',
      1 => 1362739530,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19934679295139c37643ef92-48856455',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_GET['action']=='new'){?><form method="post" action="" ><table style="width: 100%;"><colgroup style="width: 20%"></colgroup><colgroup style="width: 80%"></colgroup><tbody><?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?><tr><td colspan="2">De volgende velden zijn niet (goed) ingevuld: <br /><ol><?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?><li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li><?php }} ?></ol></td></tr><?php }?><tr><td class="fieldName"><label for="link">Link</label>:</td><td><input type="text" class="input" name="link" id="link" value="<?php if (isset($_POST['link'])){?><?php echo $_POST['link'];?>
<?php }?>" size="40" /></td></tr><tr><td class="fieldName"><label for="date_start">Datum</label>:</td><td><input type="text" class="input" name="date_start" id="date_start" value="<?php echo $_smarty_tpl->getVariable('sCurrentDate')->value;?>
" size="10" /></td></tr><tr><td class="fieldName"><label for="date_end">Datum tot</label>:</td><td><input type="text" class="input" name="date_end" id="date_end" value="<?php if (isset($_POST['date_end'])){?><?php echo $_POST['date_end'];?>
<?php }?>" size="10" /></td></tr><tr><td class="fieldName"><label for="group_id">Nieuwsgroep</label>:</td><td><select name="group_id" id="group_id"><?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('NewsGroups')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?><option value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
" <?php if (isset($_POST['group_id'])&&$_POST['group_id']==$_smarty_tpl->tpl_vars['value']->value['id']){?> selected="selected" <?php }?> ><?php echo stripslashes($_smarty_tpl->tpl_vars['value']->value['group_name']);?>
</option><?php }} ?></select></td></tr><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?><?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['id'], null, null);?><tr><td class="fieldName"><label>Afbeelding <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><ul class="photo-menu"><li><div id="upload_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><span>kiezen</span></div></li><li><a class="greybox" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a></li><li class="last"><a class="greybox browser" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a></li></ul><span id="status_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" ></span><br class="clear" /><span id="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if (isset($_POST['photos'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php  $_smarty_tpl->tpl_vars['photoOid'] = new Smarty_Variable;
 $_from = $_POST['photos'][$_smarty_tpl->getVariable('id')->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photoOid']->key => $_smarty_tpl->tpl_vars['photoOid']->value){
?><img id="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
/100/image.php" /><input name="photos[<?php echo $_smarty_tpl->getVariable('id')->value;?>
][<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" type="hidden" /><?php }} ?><?php }?></span></td></tr><tr><td class="fieldName" style="width:200px;"><label for="title_<?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
">Titel <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td style="width:200px;"><input type="text" class="input" name="title[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="title_<?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
" value="<?php if (isset($_POST['title'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php echo $_POST['title'][$_smarty_tpl->getVariable('id')->value];?>
<?php }?>" size="40" /></td></tr><tr><td class="fieldName"><label>Bericht <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><?php echo $_smarty_tpl->getVariable('FCKeditor')->value[$_smarty_tpl->getVariable('id')->value];?>
</td></tr><tr><td class="fieldName"><label for="page_description_<?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
">Pagina omschrijving <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><textarea name="page_description[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="page_description_<?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
"><?php if (isset($_POST['page_description'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php echo $_POST['page_description'][$_smarty_tpl->getVariable('id')->value];?>
<?php }?></textarea></td></tr><?php }} ?><tr><td class="fieldName"><label for="twitter_text">Twitter text</label>:</td><td><?php if (isset($_smarty_tpl->getVariable('publishTwitter',null,true,false)->value)&&$_smarty_tpl->getVariable('publishTwitter')->value=='true'){?><textarea cols="113" rows="10" name="twitter_text" id="twitter_text"><?php if (isset($_POST['twitter_text'])){?><?php echo $_POST['twitter_text'];?>
<?php }?></textarea><br />Number of characters remaining: <span id="count">140</span><?php }else{ ?><?php echo $_smarty_tpl->getVariable('publishTwitter')->value;?>
<?php }?></td></tr><?php if (isset($_smarty_tpl->getVariable('publishTwitter',null,true,false)->value)&&$_smarty_tpl->getVariable('publishTwitter')->value=='true'){?><tr><td class="fieldName">Post on twitter:</td><td><input type="checkbox" name="postTwitter" <?php if (isset($_POST['postTwitter'])){?>checked="checked"<?php }?> /></td></tr><?php }?><?php if (isset($_smarty_tpl->getVariable('publishLinkedin',null,true,false)->value)&&$_smarty_tpl->getVariable('publishLinkedin')->value=='true'){?><tr><td class="fieldName">Post on linkedin:</td><td><input type="checkbox" name="postLinkedin" <?php if (isset($_POST['postLinkedin'])){?>checked="checked"<?php }?> /></td></tr><?php }?><tr><td class="fieldName">Post facebook:</td><td><?php if (isset($_smarty_tpl->getVariable('publishFacebook',null,true,false)->value)&&$_smarty_tpl->getVariable('publishFacebook')->value=='true'){?><input type="checkbox" name="postFacebook" <?php if (isset($_POST['postFacebook'])){?>checked="checked"<?php }?> /><?php }else{ ?><?php echo $_smarty_tpl->getVariable('publishFacebook')->value;?>
<?php }?></td></tr><tr><td></td><td><br /><input type="hidden" id="module"  value="news" name="module"  /><a href="#" onclick="document.forms[0].submit();" class="save"><img name="verzenden" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/save.png" alt="" /></a><input type="submit" class="keywords" value="" name="keywords"  /></td></tr><?php if (isset($_smarty_tpl->getVariable('keywords',null,true,false)->value)){?><tr><td class="fieldName">Keywords</td><td><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('keywords')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value;?>
<br /><?php }} ?></td></tr><?php }?></tbody></table></form><script>
	$(function() {
		$( "#date_start" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    $(function() {
		$( "#date_end" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    </script><?php }elseif($_GET['action']=='overview'){?>Rijen weergeven :&nbsp;<select onchange="window.location='news.php?action=overview&items=' + this.value + '<?php if (isset($_GET['sort'])){?>&sort=<?php echo $_GET['sort'];?>
<?php }?><?php if (isset($_GET['sorttype'])){?>&sorttype=<?php echo $_GET['sorttype'];?>
<?php }?>'"><option value="5" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==5){?> selected="selected"<?php }?>>5</option><option value="10" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==10){?> selected="selected"<?php }?>>10</option><option value="25" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==25){?> selected="selected"<?php }?>>25</option><option value="50" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==50){?> selected="selected"<?php }?>>50</option><option value="100" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==100){?> selected="selected"<?php }?>>100</option><option value="10000" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==10000){?> selected="selected"<?php }?>>all</option></select><table style="width: 100%;" id="t1" class="news" cellpadding="2" cellspacing="0"><colgroup style="width: 25%"></colgroup><colgroup style="width: 25%"></colgroup><colgroup style="width: 21%"></colgroup><colgroup style="width: 19%"></colgroup><colgroup style="width: 10%"></colgroup><tbody><tr nodrag="true" nodrop="true"><td colspan="5" class="head"><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_left.jpg" id="head_left" alt="" /><h2 class="font">Overzicht</h2><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_right.jpg" id="head_right" alt="" /></td></tr><tr nodrag="true" nodrop="true"><td class="first-row-first-column-white"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/news.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=title&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Titel</a></td><td class="first-row-column-white"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/news.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=group_name&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Groepsnaam</a></td><td class="first-row-column-white"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/news.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=date_register_format&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Datum toegevoegd</a></td><td class="first-row-column-white"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/news.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=filled&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Stats</a></td><td class="first-row-last-column">Social Media</td></tr><?php if (!$_smarty_tpl->getVariable('news')->value){?><tr nodrag="true" nodrop="true"><td class="first-column-gray">Er zijn nog geen nieuws items gepost.</td><td class="column-gray"></td><td class="column-gray"></td><td class="column-gray"></td><td class="last-column-gray"></td></tr><?php }?><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('news')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']++;
?><tr id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" order_number="<?php echo $_smarty_tpl->tpl_vars['item']->value['order_number'];?>
" ondblclick="window.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/news.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
'" class="pointer <?php if (isset($_smarty_tpl->tpl_vars['item']->value['user'])){?>tooltip<?php }?>" <?php if (isset($_smarty_tpl->tpl_vars['item']->value['user'])&&isset($_smarty_tpl->tpl_vars['item']->value['user']['user_name'])){?>title="<?php echo $_smarty_tpl->tpl_vars['item']->value['user']['user_name'];?>
"<?php }?>><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter']['iteration']%2==1){?><td class="first-column-gray"><?php echo stripslashes($_smarty_tpl->tpl_vars['item']->value['title']);?>
</td><td class="column-gray"><?php echo $_smarty_tpl->tpl_vars['item']->value['group_name'];?>
</td><td class="column-gray"><?php echo $_smarty_tpl->tpl_vars['item']->value['date_register_format'];?>
</td><td class="column-gray"><span class="<?php if ($_smarty_tpl->tpl_vars['item']->value['filled']==count($_smarty_tpl->getVariable('languages')->value)){?>green<?php }else{ ?>red<?php }?>"><?php echo $_smarty_tpl->tpl_vars['item']->value['filled'];?>
 / <?php echo count($_smarty_tpl->getVariable('languages')->value);?>
</span></td><td class="last-column-gray social"><?php if ($_smarty_tpl->tpl_vars['item']->value['facebook']!=0){?><img src="/cms/images/facebook_icon.jpg" width="16" height="16"/><?php }?><?php if ($_smarty_tpl->tpl_vars['item']->value['twitter']!=0){?><img src="/cms/images/twitter_icon.jpg" width="16" height="16"/><?php }?><?php if ($_smarty_tpl->tpl_vars['item']->value['linkedin']!=0){?><img src="/cms/images/linkedin.gif" width="16" height="16"/><?php }?></td><?php }else{ ?><td class="first-column-white"><?php echo stripslashes($_smarty_tpl->tpl_vars['item']->value['title']);?>
</td><td class="column-white"><?php echo $_smarty_tpl->tpl_vars['item']->value['group_name'];?>
</td><td class="column-white"><?php echo $_smarty_tpl->tpl_vars['item']->value['date_register_format'];?>
</td><td class="column-white"><span class="<?php if ($_smarty_tpl->tpl_vars['item']->value['filled']==count($_smarty_tpl->getVariable('languages')->value)){?>green<?php }else{ ?>red<?php }?>"><?php echo $_smarty_tpl->tpl_vars['item']->value['filled'];?>
 / <?php echo count($_smarty_tpl->getVariable('languages')->value);?>
</span></td><td class="last-column-white social"><?php if ($_smarty_tpl->tpl_vars['item']->value['facebook']!=0){?><img src="/cms/images/facebook_icon.jpg" width="16" height="16"/><?php }?><?php if ($_smarty_tpl->tpl_vars['item']->value['twitter']!=0){?><img src="/cms/images/twitter_icon.jpg" width="16" height="16"/><?php }?><?php if ($_smarty_tpl->tpl_vars['item']->value['linkedin']!=0){?><img src="/cms/images/linkedin.gif" width="16" height="16"/><?php }?></td><?php }?></tr><?php }} ?><tr nodrag="true" nodrop="true"><td class="last-row-first-column"></td><td class="last-row-column-white"></td><td class="last-row-column-white"></td><td class="last-row-column-white"></td><td class="last-row-last-column"></td></tr></tbody></table><div style="padding-top: 0.6em; text-align: center;"><?php echo $_smarty_tpl->getVariable('links')->value;?>
</div><?php }elseif($_GET['action']=='edit'){?><form method="post" action="" ><table style="width: 100%;"><colgroup style="width: 20%"></colgroup><colgroup style="width: 80%"></colgroup><tbody><?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?><tr><td colspan="3">De volgende velden zijn niet (goed) ingevuld: <br /><ol><?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?><li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li><?php }} ?></ol></td></tr><?php }?><tr><td class="fieldName"><label for="link">Link</label>:</td><td><input type="text" class="input" name="link" id="link" value="<?php echo $_smarty_tpl->getVariable('values')->value['link'];?>
" size="40" /></td></tr><tr><td class="fieldName"><label for="date_start">Datum</label>:</td><td><input type="text" class="input" name="date_start" id="date_start" value="<?php echo $_smarty_tpl->getVariable('values')->value['date_start_format'];?>
" size="10" /></td></tr><tr><td class="fieldName"><label for="date_end">Datum tot</label>:</td><td><input type="text" class="input" name="date_end" id="date_end" value="<?php echo $_smarty_tpl->getVariable('values')->value['date_end_format'];?>
" size="10" /></td></tr><tr><td class="fieldName"><label for="group_id">Nieuwsgroep</label>:</td><td><select name="group_id" id="group_id"><?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('NewsGroups')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?><option value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
" <?php if ($_smarty_tpl->getVariable('values')->value['group_id']==$_smarty_tpl->tpl_vars['value']->value['id']){?> selected="selected" <?php }?>><?php echo stripslashes($_smarty_tpl->tpl_vars['value']->value['group_name']);?>
</option><?php }} ?></select></td></tr><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?><?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['id'], null, null);?><tr><td class="fieldName"><label>Afbeelding <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><ul class="photo-menu"><li><div id="upload_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><span>kiezen</span></div></li><li><a class="greybox" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a></li><li class="last"><a class="greybox browser" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a></li></ul><span id="status_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" ></span><br class="clear" /><span id="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if ($_smarty_tpl->getVariable('values')->value[$_smarty_tpl->getVariable('id')->value]['photos']!=''){?><?php  $_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('values')->value[$_smarty_tpl->getVariable('id')->value]['photos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photo']->key => $_smarty_tpl->tpl_vars['photo']->value){
?><img id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
/100/image.php" /><input name="photos[<?php echo $_smarty_tpl->getVariable('id')->value;?>
][<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
" type="hidden" /><?php }} ?><?php }?></span></td></tr><tr><td class="fieldName"><label for="title_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Titel <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><input type="text" class="input" name="title[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="title_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" value="<?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['title'])){?><?php echo $_smarty_tpl->getVariable('values')->value[$_smarty_tpl->getVariable('id')->value]['title'];?>
<?php }?>" size="40" /></td></tr><tr><td class="fieldName"><label for="f_id_2">Bericht <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><?php echo $_smarty_tpl->getVariable('FCKeditor')->value[$_smarty_tpl->getVariable('id')->value];?>
</td></tr><tr><td class="fieldName"><label for="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Pagina omschrijving <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><textarea name="page_description[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['page_description'])){?><?php echo $_smarty_tpl->getVariable('values')->value[$_smarty_tpl->getVariable('id')->value]['page_description'];?>
<?php }?></textarea></td></tr><?php }} ?><tr><td class="fieldName"><label for="twitter_text">Twitter_text</label>:</td><td><?php if (isset($_smarty_tpl->getVariable('publishTwitter',null,true,false)->value)&&$_smarty_tpl->getVariable('publishTwitter')->value=='true'){?><textarea cols="113" rows="10" name="twitter_text" id="twitter_text"><?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['twitter_text'])){?><?php echo $_smarty_tpl->getVariable('values')->value['twitter_text'];?>
<?php }?></textarea><br />Number of characters remaining: <span id="count">140</span><?php }else{ ?><?php echo $_smarty_tpl->getVariable('publishTwitter')->value;?>
<?php }?></td></tr><?php if (isset($_smarty_tpl->getVariable('publishTwitter',null,true,false)->value)&&$_smarty_tpl->getVariable('publishTwitter')->value=='true'){?><tr><td class="fieldName">Post on twitter:</td><td><input type="checkbox" name="postTwitter" <?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['twitter'])&&$_smarty_tpl->getVariable('values')->value['twitter']!=0){?>checked="checked"<?php }?> /></td></tr><?php }?><?php if (isset($_smarty_tpl->getVariable('publishLinkedin',null,true,false)->value)&&$_smarty_tpl->getVariable('publishLinkedin')->value=='true'){?><tr><td class="fieldName">Post on linkedin:</td><td><input type="checkbox" name="postLinkedin" <?php if (isset($_smarty_tpl->getVariable('values',null,true,false)->value['linkedin'])&&$_smarty_tpl->getVariable('values')->value['linkedin']!=0){?>checked="checked"<?php }?> /></td></tr><?php }?><tr><td class="fieldName">Post facebook:</td><td><?php if (isset($_smarty_tpl->getVariable('publishFacebook',null,true,false)->value)&&$_smarty_tpl->getVariable('publishFacebook')->value=='true'){?><input type="checkbox" name="postFacebook" <?php if ($_smarty_tpl->getVariable('values')->value['facebook']!=0){?>checked="checked"<?php }?> /><?php }else{ ?><?php echo $_smarty_tpl->getVariable('publishFacebook')->value;?>
<?php }?></td></tr><tr><td></td><td><br /><input type="hidden" id="module"  value="news" name="module"  /><input class="button" type="submit" name="submit" id="save" value="" /><a class="verwijderen" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/news/news.php?action=delete&amp;id=<?php echo $_smarty_tpl->getVariable('values')->value['id'];?>
" onclick="return confirm('Weet u zeker dat u dit nieuws item wilt verwijderen?'); return false;"></a><input type="submit" class="keywords" value="" name="keywords"  /></td></tr><?php if (isset($_smarty_tpl->getVariable('keywords',null,true,false)->value)){?><tr><td class="fieldName">Keywords</td><td><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('keywords')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value;?>
<br /><?php }} ?></td></tr><?php }?></tbody></table></form><script>
	$(function() {
		$( "#date_start" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    $(function() {
		$( "#date_end" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    </script><?php }else{ ?>Deze pagina is niet helemaal ok!<?php }?>
