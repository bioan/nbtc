<?php /* Smarty version Smarty-3.0.7, created on 2013-03-08 14:12:13
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18258027315139e3addab916-80671004%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e57da97fccb58409b41a211e6e7d6e713eaeb2a6' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/login.tpl',
      1 => 1362739492,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18258027315139e3addab916-80671004',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Inforitus - Content Manament System</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
        <meta http-equiv="Content-Language" content="NL" />
        <style type="text/css">
        body, html
        {
            text-align: center;
            width: 100%;
            height: 100%;
        
            padding: 0px;
            margin: 0px;
        }
        #identify
        {
            width: 344px;
            height: 350px;
        
            position: absolute;
            left: 50%;
            top: 50%;
        
            margin-left: -163px;
            margin-top: -267px;
        }
        #login
        {
            font-family: Verdana;
            font-size: 12px;
            color: #616161;
            background: url(images/login-form.jpg) no-repeat;
            line-height: 26px;
            text-align: left;
            padding:64px 0px 0px 0px;
            width: 344px;
            height: 123px;
        }
        #login input
        {
            font-family: Verdana;
            font-size: 10px;
        }
        
        .input {
            border: 1px solid #e5e5e5;
            width: 202px;
            height: 23px;
            background: url(images/input.jpg);
        }
        
        #submit {
            border: none;
            background: url(images/inloggen.jpg) no-repeat;
            width: 86px;
            height: 29px;
            float: right;
            margin: 6px 0px 0px 0px;
        }
        
        #login table {
            margin: 0px 0px 0px 20px;
        }
        
        #login table td {
            padding: 0px 20px 0px 0px;
            font-size: 11px;
            line-height: 34px;
        }
        </style>
        <meta name="description" content="Inforitus Content Management systeem." />
        <meta name="keywords" content="Inforitus, content, management, systeem" />
        <meta name="author" content="Inforitus" />
    </head>
    <body>
        <div id="identify">
            <table cellpadding="0" cellspacing="0" border="0" style="width:100%">
                <tr>
                    <td valign="middle" style="width:162px" align="left" height="135">
                        <img src="images/inforitus2.jpg" alt="Inforitus" style="padding:0px 0px 0px 15px;" />
                    </td>
                    <td valign="middle" style="width:162px" align="right" height="135">
                        <img src="images/logo.jpg" alt="" style="padding:0px 15px 0px 0px;max-height:135px;max-width:147px;" />    
                    </td>
                </tr>
            </table>
            <div id="login">
                <form action="index.php" method="post">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>Naam:</td>
                            <td><input type="text" class="input" name="user_name" /></td>
                        </tr>
                        <tr>
                            <td>Wachtwoord:</td>
                            <td><input type="password" class="input" name="password" /></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" value="" id="submit" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>