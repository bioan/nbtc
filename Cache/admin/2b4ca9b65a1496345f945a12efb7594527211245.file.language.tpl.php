<?php /* Smarty version Smarty-3.0.7, created on 2013-03-13 11:13:41
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/language/language.tpl" */ ?>
<?php /*%%SmartyHeaderCode:58940166951405155970b09-32441628%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2b4ca9b65a1496345f945a12efb7594527211245' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/language/language.tpl',
      1 => 1362739530,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '58940166951405155970b09-32441628',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_replace')) include '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/includes/plugins/modifier.replace.php';
?><script type="text/javascript">/* <![CDATA[ */
  function showDiv($divId)
  {
    var $obj = document.getElementById($divId);
    $obj.style.display = 'block';
  }
  
    function checkForm()
    {
        var obj = document.getElementById('photo_area');
        if (!obj.hasChildNodes()) {
            alert('Foto\'s toevoegen!');
            return false;    
        }
        for (var i=0; i < obj.childNodes.length; i++) {
            if (obj.childNodes.item(i).nodeName.match(/^IMG$/))
                return true;
        }
        alert('Foto\'s toevoegen!');
        return false;    
    }

      function checkIfDisabled(obj)
    {
        if (obj.options[obj.selectedIndex].disabled == true) {
            alert('Deze categorie kan niet gekozen worden omdat deze midden in de tree zit!');
            for (var i =0; i < obj.options.length; i++) {
                if (!obj.options[i].disabled) {
                    obj.options[i].selected = true;
                    return;
                }
            }
            
        }
    }
  /* ]]> */</script><?php if ($_GET['action']=='new'){?><form method="post" action="" enctype="multipart/form-data"><table style="width: 100%;"><colgroup style="width: 20%"></colgroup><colgroup style="width: 80%"></colgroup><tbody><?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?><tr><td colspan="2">De volgende velden zijn niet (goed) ingevuld: <br /><ol><?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?><li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li><?php }} ?></ol></td></tr><?php }?><tr><td class="fieldName" style="width:200px;"><label for="private">Private</label>:</td><td style="width:200px;"><input type="checkbox" class="input" name="private" id="private" value="1" <?php if (isset($_POST['private'])){?> checked="checked" <?php }?> size="40" /></td></tr><tr><td class="fieldName" style="width:200px;"><label for="language">Name</label>:</td><td style="width:200px;"><input type="text" class="input" name="language" id="language" value="<?php if (isset($_POST['language'])){?><?php echo $_POST['language'];?>
<?php }?>" size="40" /></td></tr><tr><td class="fieldName"><label for="short_name">Short Name</label>:</td><td><input type="text" class="input" name="short_name" id="short_name" value="<?php if (isset($_POST['short_name'])){?><?php echo $_POST['short_name'];?>
<?php }?>" size="40" /></td></tr><tr><td class="fieldName"><label>Image</label>:</td><td><input type="file" name="image"  value="" /></td></tr><tr><td></td><td><br /><a href="#" onclick="document.forms[0].submit();" class="save"><img name="verzenden" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/save.png" alt="" /></a></td></tr></tbody></table></form><?php }elseif($_GET['action']=='overview'){?>Rijen weergeven :&nbsp;<select onchange="window.location='language.php?action=overview&items=' + this.value + '<?php if (isset($_GET['sort'])){?>&sort=<?php echo $_GET['sort'];?>
<?php }?><?php if (isset($_GET['sorttype'])){?>&sorttype=<?php echo $_GET['sorttype'];?>
<?php }?>'"><option value="5" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==5){?> selected="selected"<?php }?>>5</option><option value="10" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==10){?> selected="selected"<?php }?>>10</option><option value="25" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==25){?> selected="selected"<?php }?>>25</option><option value="50" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==50){?> selected="selected"<?php }?>>50</option><option value="100" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==100){?> selected="selected"<?php }?>>100</option><option value="10000" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==10000){?> selected="selected"<?php }?>>all</option></select><table style="width: 100%;" id="t1" class="news" cellpadding="2" cellspacing="0"><colgroup style="width: 40%"></colgroup><colgroup style="width: 41%"></colgroup><colgroup style="width: 19%"></colgroup><tbody><tr><td colspan="3" class="head"><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_left.jpg" id="head_left" alt="" /><h2 class="font">Overzicht</h2><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_right.jpg" id="head_right" alt="" /></td></tr><tr><td class="first-row-first-column-white"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
language/language.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=language&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Name</a></td><td class="first-row-column-white"></td><td class="first-row-last-column"></td></tr><?php if (!$_smarty_tpl->getVariable('news')->value){?><tr><td class="first-column-gray">Er zijn nog geen nieuws items gepost.</td><td class="column-gray"></td><td class="last-column-gray"></td></tr><?php }?><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('news')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']++;
?><tr id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" ondblclick="window.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
language/language.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
'" class="pointer"><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter']['iteration']%2==1){?><td class="first-column-gray"><?php echo stripslashes($_smarty_tpl->tpl_vars['item']->value['language']);?>
</td><td class="column-gray"></td><td class="last-column-gray"></td><?php }else{ ?><td class="first-column-white"><?php echo stripslashes($_smarty_tpl->tpl_vars['item']->value['language']);?>
</td><td class="column-white"></td><td class="last-column-white"></td><?php }?></tr><?php }} ?><tr><td class="last-row-first-column"></td><td class="last-row-column-white"></td><td class="last-row-last-column"></td></tr></tbody></table><div style="padding-top: 0.6em; text-align: center;"><?php echo $_smarty_tpl->getVariable('links')->value;?>
</div><?php }elseif($_GET['action']=='edit'){?><form method="post" action="" enctype="multipart/form-data"><table style="width: 100%;"><colgroup style="width: 20%"></colgroup><colgroup style="width: 80%"></colgroup><tbody><?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?><tr><td colspan="3">De volgende velden zijn niet (goed) ingevuld: <br /><ol><?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?><li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li><?php }} ?></ol></td></tr><?php }?><tr><td class="fieldName"><label for="private">Private</label>:</td><td><input type="checkbox" name="private" id="private" value="1" <?php if ($_smarty_tpl->getVariable('values')->value['private']==1){?> checked="checked" <?php }?> /></td></tr><tr><td class="fieldName"><label for="language">Name</label>:</td><td><input type="text" class="input" name="language" id="language" value="<?php echo stripslashes($_smarty_tpl->getVariable('values')->value['language']);?>
" size="40" /></td></tr><tr><td class="fieldName"><label for="short_name">Short Name</label>:</td><td><input type="text" class="input" name="short_name" id="short_name" value="<?php echo $_smarty_tpl->getVariable('values')->value['short_name'];?>
" size="40" /></td></tr><tr><td class="fieldName"><label>Image</label>:</td><td><input type="file" name="image"  value="" /></td></tr><tr><td class="fieldName" colspan="2"><a href="language.php?action=texts&id=<?php echo $_smarty_tpl->getVariable('values')->value['id'];?>
">Standaard teksten aanpassen</a></td></tr><tr><td></td><td><br /><input class="button" type="submit" name="submit" id="save" value="" /><?php if (count($_smarty_tpl->getVariable('languages')->value)>1){?><a class="verwijderen" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/language/language.php?action=delete&amp;id=<?php echo $_smarty_tpl->getVariable('values')->value['id'];?>
" onclick="return confirm('Weet je zeker dat je taalset wil verwijderen? Hiermee verwijder je alle inhoud'); return false;"></a><?php }?></td></tr></tbody></table></form><?php }elseif($_GET['action']=='stats'){?><table style="width: 100%;" id="t1" class="news" cellpadding="2" cellspacing="0"><colgroup style="width: 40%"></colgroup><colgroup style="width: 41%"></colgroup><colgroup style="width: 19%"></colgroup><tbody><tr><td colspan="3" class="head"><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_left.jpg" id="head_left" alt="" /><h2 class="font">Overzicht</h2><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_right.jpg" id="head_right" alt="" /></td></tr><tr><td class="first-row-first-column-white">Name</td><td class="first-row-column-white">Progress</td><td class="first-row-last-column"></td></tr><?php if (!$_smarty_tpl->getVariable('languages')->value){?><tr><td class="first-column-gray">Er zijn nog geen nieuws items gepost.</td><td class="column-gray"></td><td class="last-column-gray"></td></tr><?php }?><?php if (!isset($_GET['lang'])){?><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']++;
?><tr id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" class="pointer" onclick="window.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
language/language.php?action=stats&lang=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
'"><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter']['iteration']%2==1){?><td class="first-column-gray"><?php echo stripslashes($_smarty_tpl->tpl_vars['item']->value['language']);?>
</td><td class="column-gray"><span class="<?php if ($_smarty_tpl->tpl_vars['item']->value['filled']!=$_smarty_tpl->tpl_vars['item']->value['all']){?>red<?php }else{ ?>green<?php }?>"><?php echo $_smarty_tpl->tpl_vars['item']->value['filled'];?>
 / <?php echo $_smarty_tpl->tpl_vars['item']->value['all'];?>
</span></td><td class="last-column-gray"></td><?php }else{ ?><td class="first-column-white"><?php echo stripslashes($_smarty_tpl->tpl_vars['item']->value['language']);?>
</td><td class="column-white"><span class="<?php if ($_smarty_tpl->tpl_vars['item']->value['filled']!=$_smarty_tpl->tpl_vars['item']->value['all']){?>red<?php }else{ ?>green<?php }?>"><?php echo $_smarty_tpl->tpl_vars['item']->value['filled'];?>
 / <?php echo $_smarty_tpl->tpl_vars['item']->value['all'];?>
</span></td><td class="last-column-white"></td><?php }?></tr><?php }} ?><?php }elseif(isset($_GET['lang'])&&!isset($_GET['module'])){?><?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_GET['lang'], null, null);?><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value[$_smarty_tpl->getVariable('id')->value]['modules']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']++;
?><tr class="pointer" onclick="window.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
language/language.php?action=stats&lang=<?php echo $_smarty_tpl->getVariable('id')->value;?>
&module=<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
'"><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter']['iteration']%2==1){?><td class="first-column-gray"><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
</td><td class="column-gray"><span class="<?php if ($_smarty_tpl->tpl_vars['item']->value['filled']!=$_smarty_tpl->tpl_vars['item']->value['all']){?>red<?php }else{ ?>green<?php }?>"><?php echo $_smarty_tpl->tpl_vars['item']->value['filled'];?>
 / <?php echo $_smarty_tpl->tpl_vars['item']->value['all'];?>
</span></td><td class="last-column-gray"></td><?php }else{ ?><td class="first-column-white"><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
</td><td class="column-white"><span class="<?php if ($_smarty_tpl->tpl_vars['item']->value['filled']!=$_smarty_tpl->tpl_vars['item']->value['all']){?>red<?php }else{ ?>green<?php }?>"><?php echo $_smarty_tpl->tpl_vars['item']->value['filled'];?>
 / <?php echo $_smarty_tpl->tpl_vars['item']->value['all'];?>
</span></td><td class="last-column-white"></td><?php }?></tr><?php }} ?><?php }elseif(isset($_GET['module'])){?><?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_GET['lang'], null, null);?><?php $_smarty_tpl->tpl_vars['module'] = new Smarty_variable($_GET['module'], null, null);?><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value[$_smarty_tpl->getVariable('id')->value]['modules'][$_smarty_tpl->getVariable('module')->value]['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']++;
?><tr class="pointer" onclick="window.location.href = '<?php if (isset($_smarty_tpl->tpl_vars['item']->value['bitem'])){?><?php echo $_smarty_tpl->getVariable('languages')->value[$_smarty_tpl->getVariable('id')->value]['modules'][$_smarty_tpl->getVariable('module')->value]['item'];?>
<?php }else{ ?><?php echo $_smarty_tpl->getVariable('languages')->value[$_smarty_tpl->getVariable('id')->value]['modules'][$_smarty_tpl->getVariable('module')->value]['cat'];?>
<?php }?><?php if ($_smarty_tpl->getVariable('module')->value!='pages'){?>id=<?php if (!isset($_smarty_tpl->tpl_vars['item']->value['id'])){?><?php echo $_smarty_tpl->tpl_vars['item']->value['oid'];?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
<?php }?><?php }else{ ?>oid=<?php echo $_smarty_tpl->tpl_vars['item']->value['oid'];?>
<?php }?>'"><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter']['iteration']%2==1){?><td class="first-column-gray"><?php if ($_smarty_tpl->getVariable('module')->value!='pages'){?><?php if (isset($_smarty_tpl->tpl_vars['item']->value['title'])){?><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
<?php }elseif(isset($_smarty_tpl->tpl_vars['item']->value['group_name'])){?><?php echo $_smarty_tpl->tpl_vars['item']->value['group_name'];?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
<?php }?><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
<?php }?></td><td class="column-gray"><span class="<?php if (!isset($_smarty_tpl->tpl_vars['item']->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['filled'])){?>red<?php }else{ ?>green<?php }?>"><?php if (!isset($_smarty_tpl->tpl_vars['item']->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['filled'])){?>not filled<?php }else{ ?>filled<?php }?></span></td><td class="last-column-gray"></td><?php }else{ ?><td class="first-column-white"><?php if ($_smarty_tpl->getVariable('module')->value!='pages'){?><?php if (isset($_smarty_tpl->tpl_vars['item']->value['title'])){?><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
<?php }elseif(isset($_smarty_tpl->tpl_vars['item']->value['group_name'])){?><?php echo $_smarty_tpl->tpl_vars['item']->value['group_name'];?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
<?php }?><?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
<?php }?></td><td class="column-white"><span class="<?php if (!isset($_smarty_tpl->tpl_vars['item']->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['filled'])){?>red<?php }else{ ?>green<?php }?>"><?php if (!isset($_smarty_tpl->tpl_vars['item']->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['filled'])){?>not filled<?php }else{ ?>filled<?php }?></span></td><td class="last-column-white"></td><?php }?></tr><?php }} ?><?php }?><tr><td class="last-row-first-column"></td><td class="last-row-column-white"></td><td class="last-row-last-column"></td></tr></tbody></table><?php }elseif($_GET['action']=='texts'&&isset($_GET['id'])){?><form method="post" action=""><table style="width: 100%;"><colgroup style="width: 30%"></colgroup><colgroup style="width: 70%"></colgroup><tbody><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('texts')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?><tr><td class="fieldName" style="width:200px;"><label for="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['k']->value,'_',' ');?>
</label>:</td><td style="width:200px;"><input type="text" class="input" name="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" id="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
"  size="60" /></td></tr><?php }} ?><tr><td></td><td><br /><a href="#" onclick="document.forms[0].submit();" class="save"><img name="verzenden" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/save.png" alt="" /></a></td></tr></tbody></table></form><?php }?>
