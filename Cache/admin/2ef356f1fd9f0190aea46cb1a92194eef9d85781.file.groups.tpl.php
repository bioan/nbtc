<?php /* Smarty version Smarty-3.0.7, created on 2013-03-14 15:31:25
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/news/groups.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19769738735141df3deb0617-65050494%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2ef356f1fd9f0190aea46cb1a92194eef9d85781' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/news/groups.tpl',
      1 => 1363271474,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19769738735141df3deb0617-65050494',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_GET['action']=='new'){?>
    <form method="post" action="" >
		<table style="width: 100%;">
		    <colgroup style="width: 25%"></colgroup>
            <colgroup style="width: 75%"></colgroup>
            <tbody>                               
            <?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?>
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?>
						<li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li>
					<?php }} ?>
					</ol>
				</td>
			</tr>
			<?php }?>
            <tr>
                <td class="fieldName">
                    <label for="private">Private</label>: 
                </td>
                <td>
                    <input type="checkbox" name="private" value="1" id="private" <?php if (isset($_POST['private'])&&$_POST['private']==1){?> checked="checked" <?php }?> />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label>Blog</label>: 
                </td>
                <td>
                    <input type="radio" name="blog" value="0" id="blog" <?php if ((isset($_POST['blog'])&&$_POST['blog']==0)||!isset($_POST['blog'])){?> checked="checked" <?php }?> /> none <input type="radio" name="blog" value="1" id="blog" <?php if ((isset($_POST['blog'])&&$_POST['blog']==1)){?> checked="checked" <?php }?> /> forum <input type="radio" name="blog" value="2" id="blog" <?php if (isset($_POST['blog'])&&$_POST['blog']==2){?> checked="checked" <?php }?> /> facebook
                </td>
            </tr>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['id'], null, null);?>
			<tr>
				<td class="fieldName">
				    <label for="name_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Groepnaam <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="group_name[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="name_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" value="<?php if (isset($_POST['group_name'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php echo $_POST['group_name'][$_smarty_tpl->getVariable('id')->value];?>
<?php }?>" size="40" title="Max 100 tekens" maxlength="101" onchange="countAndTruncate(this, 100, 'f_540_counter_id');" onkeyup="countAndTruncate(this, 100, 'f_540_counter_id');"/>
				    <span id="f_540_counter_id"></span>
                </td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Beschrijving <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>: 
				</td>
				<td>
				    <textarea name="description[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" cols="43" rows="6"><?php if (isset($_POST['description'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php echo $_POST['description'][$_smarty_tpl->getVariable('id')->value];?>
<?php }?></textarea>
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Pagina omschrijving <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:
                </td>
                <td>
                    <textarea name="page_description[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if (isset($_POST['page_description'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php echo $_POST['page_description'][$_smarty_tpl->getVariable('id')->value];?>
<?php }?></textarea>
                </td>
            </tr>
            <?php }} ?>
			<tr>
                <td></td>
				<td><br />
				<a href="#" onclick="document.forms[0].submit();"><img name="verzenden" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/save.png" alt="" /></a>
				</td>
			</tr>
		</table>
	</form>
<?php }elseif($_GET['action']=='overview'){?> 
    Rijen weergeven :&nbsp;
    <select onchange="window.location='groups.php?action=overview&items=' + this.value + '<?php if (isset($_GET['sort'])){?>&sort=<?php echo $_GET['sort'];?>
<?php }?><?php if (isset($_GET['sorttype'])){?>&sorttype=<?php echo $_GET['sorttype'];?>
<?php }?>'">
        <option value="5" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==5){?> selected="selected"<?php }?>>5</option>
        <option value="10" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==10){?> selected="selected"<?php }?>>10</option>
        <option value="25" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==25){?> selected="selected"<?php }?>>25</option>
        <option value="50" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==50){?> selected="selected"<?php }?>>50</option>
        <option value="100" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==100){?> selected="selected"<?php }?>>100</option>
        <option value="10000" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==10000){?> selected="selected"<?php }?>>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="groups" cellpadding="2" cellspacing="0">
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 41%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/groups.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=group_name&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Groepnaam</a>
                </td>
                <td class="first-row-column-white">
                    <a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/groups.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=date_format&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Datum toegevoegd</a>
                </td>
                <td class="first-row-last-column">
                    <a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/groups.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=filled&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Stats</a>
                </td>
            </tr>
             <?php if (!$_smarty_tpl->getVariable('newsGroup')->value){?>
            <tr nodrag="true" nodrop="true">
                <td class="first-column-gray">
                    Er zijn nog geen nieuwsgroepen toegevoegd.      
                </td>
                <td class="column-gray">
                    
                </td>
                <td class="last-column-gray">
                    &nbsp;
                </td>
            </tr>
            <?php }?>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('newsGroup')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']++;
?>
				
            <tr id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" order_number="<?php echo $_smarty_tpl->tpl_vars['item']->value['order_number'];?>
" ondblclick="window.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
news/groups.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
'" class="pointer">
            
                <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter']['iteration']%2==1){?>   
                
                <td class="first-column-gray">
                    <?php echo $_smarty_tpl->tpl_vars['item']->value['group_name'];?>

                </td>
                <td class="column-gray">
                    <?php echo $_smarty_tpl->tpl_vars['item']->value['date_format'];?>

                </td>
                <td class="last-column-gray">
                    <span class="<?php if ($_smarty_tpl->tpl_vars['item']->value['filled']==count($_smarty_tpl->getVariable('languages')->value)){?>green<?php }else{ ?>red<?php }?>"><?php echo $_smarty_tpl->tpl_vars['item']->value['filled'];?>
 / <?php echo count($_smarty_tpl->getVariable('languages')->value);?>
</span>
                </td>
                <?php }else{ ?>
                <td class="first-column-white">
                    <?php echo $_smarty_tpl->tpl_vars['item']->value['group_name'];?>

                </td>
                <td class="column-white">
                    <?php echo $_smarty_tpl->tpl_vars['item']->value['date_format'];?>

                </td>
                <td class="last-column-white">
                    <span class="<?php if ($_smarty_tpl->tpl_vars['item']->value['filled']==count($_smarty_tpl->getVariable('languages')->value)){?>green<?php }else{ ?>red<?php }?>"><?php echo $_smarty_tpl->tpl_vars['item']->value['filled'];?>
 / <?php echo count($_smarty_tpl->getVariable('languages')->value);?>
</span>
                </td>
                <?php }?>
            </tr>
            <?php }} ?>
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        <?php echo $_smarty_tpl->getVariable('links')->value;?>

    </div>
<?php }elseif($_GET['action']=='edit'){?>
    <form method="post" action="" >
		<table style="width: 100%;">
		    <colgroup style="width: 25%"></colgroup>
            <colgroup style="width: 75%"></colgroup>
            <tbody>
            <?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?>
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?>
						<li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li>
					<?php }} ?>
					</ol>
				</td>
			</tr>
			<?php }?>
            <tr>
                <td class="fieldName">
                    <label for="private">Private</label>: 
                </td>
                <td>
                    <input type="checkbox" name="private" value="1" id="private" <?php if ($_smarty_tpl->getVariable('newsGroup')->value['private']==1){?> checked="checked" <?php }?> />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label>Blog</label>: 
                </td>
                <td>
                    <input type="radio" name="blog" value="0" id="blog" <?php if ($_smarty_tpl->getVariable('newsGroup')->value['blog']==0){?> checked="checked" <?php }?> /> none <input type="radio" name="blog" value="1" id="blog" <?php if ($_smarty_tpl->getVariable('newsGroup')->value['blog']==1){?> checked="checked" <?php }?> /> forum <input type="radio" name="blog" value="2" id="blog" <?php if ($_smarty_tpl->getVariable('newsGroup')->value['blog']==2){?> checked="checked" <?php }?> /> facebook
                </td>
            </tr>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['id'], null, null);?>
			<tr>
				<td class="fieldName">
				    <label for="name_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Groepnaam <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="group_name[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="name_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" value="<?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['group_name'])){?><?php echo stripslashes($_smarty_tpl->getVariable('newsGroup')->value[$_smarty_tpl->getVariable('id')->value]['group_name']);?>
<?php }?>" size="40" title="Max 100 tekens" maxlength="101" onchange="countAndTruncate(this, 100, 'f_540_counter_id');" onkeyup="countAndTruncate(this, 100, 'f_540_counter_id');"/>
				    <span id="f_540_counter_id"></span>
                </td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Beschrijving <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>: 
				</td>
				<td>
				    <textarea name="description[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id= "description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" cols="43" rows="6"><?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['description'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value[$_smarty_tpl->getVariable('id')->value]['description'];?>
<?php }?></textarea>
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Pagina omschrijving <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:
                </td>
                <td>
                    <textarea name="page_description[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if (isset($_smarty_tpl->getVariable('newsGroup',null,true,false)->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['page_description'])){?><?php echo $_smarty_tpl->getVariable('newsGroup')->value[$_smarty_tpl->getVariable('id')->value]['page_description'];?>
<?php }?></textarea>
                </td>
            </tr>
            <?php }} ?>
			<tr>
                <td></td>
				<td>
                    <br />
				    <a href="#" onclick="document.forms[0].submit();" id="save"></a>
                    <a class="verwijderen" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/news/groups.php?action=delete&amp;id=<?php echo $_smarty_tpl->getVariable('newsGroup')->value['id'];?>
" onclick="return confirm('Weet u zeker dat u deze nieuwsgroep wilt verwijderen?'); return false;"></a>
				</td>
			</tr>
		</table>
	</form>
<?php }?>
