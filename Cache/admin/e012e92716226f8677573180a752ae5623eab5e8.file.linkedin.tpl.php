<?php /* Smarty version Smarty-3.0.7, created on 2013-03-14 14:29:34
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/linkedin/linkedin.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3305093085141d0be2c5066-89741142%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e012e92716226f8677573180a752ae5623eab5e8' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/linkedin/linkedin.tpl',
      1 => 1362739529,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3305093085141d0be2c5066-89741142',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<form action="" method="post">
    <table cellspacing="0" cellpadding="0" style="100%">
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 80%"></colgroup>
        <tr>
            <td class="fieldTitle" colspan="2">
               1. Linkedin Connection
            </td>
        </tr>
        <?php if (isset($_smarty_tpl->getVariable('successMessage',null,true,false)->value)&&count($_smarty_tpl->getVariable('appErrors')->value)==0){?>
        <tr>
            <td class="successMessage" colspan="2">
               <?php echo $_smarty_tpl->getVariable('successMessage')->value;?>

            </td>
        </tr>
        <?php }?>
        <?php if (isset($_smarty_tpl->getVariable('successAccToken',null,true,false)->value)){?>
        <tr>
            <td class="successMessage" colspan="2">
               <?php echo $_smarty_tpl->getVariable('successAccToken')->value;?>

            </td>
        </tr>
        <?php }?>
        <?php if (isset($_smarty_tpl->getVariable('appErrors',null,true,false)->value['consumer_key'])){?>
        <tr>
            <td class="error" colspan="2">
               <?php echo $_smarty_tpl->getVariable('appErrors')->value['consumer_key'];?>

            </td>
        </tr>
        <?php }?>
        <tr>
            <td class="fieldName">
                Consumer key:
            </td>
            <td>
                <input type="text" class="input" name="consumer_key" value="<?php if (isset($_POST['consumer_key'])){?><?php echo $_POST['consumer_key'];?>
<?php }elseif(isset($_smarty_tpl->getVariable('appValues',null,true,false)->value['consumer_key'])){?><?php echo $_smarty_tpl->getVariable('appValues')->value['consumer_key'];?>
<?php }else{ ?><?php }?>" size="30">
            </td>
        </tr>
        <?php if (isset($_smarty_tpl->getVariable('appErrors',null,true,false)->value['consumer_secret'])){?>
        <tr>
            <td class="error" colspan="2">
               <?php echo $_smarty_tpl->getVariable('appErrors')->value['consumer_secret'];?>

            </td>
        </tr>
        <?php }?>
        <tr>
            <td class="fieldName">
                Consumer secret:
            </td>
            <td>
                <input type="text" class="input" name="consumer_secret" value="<?php if (isset($_POST['consumer_secret'])){?><?php echo $_POST['consumer_secret'];?>
<?php }elseif(isset($_smarty_tpl->getVariable('appValues',null,true,false)->value['consumer_secret'])){?><?php echo $_smarty_tpl->getVariable('appValues')->value['consumer_secret'];?>
<?php }else{ ?><?php }?>" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName" colspan="2">
                <input type="submit" value="Save Changes" name="saveChangesApp" />
                <?php if (isset($_smarty_tpl->getVariable('errorCode',null,true,false)->value)){?>
                    <?php echo $_smarty_tpl->getVariable('errorCode')->value;?>

                <?php }else{ ?>
                    <?php if ($_smarty_tpl->getVariable('url')->value!='false'){?><a href="<?php echo $_smarty_tpl->getVariable('url')->value;?>
" title="Grant Permissions">Grant permissions</a>
                    <?php }else{ ?> You must fill in application data
                    <?php }?>
                <?php }?>
           </td>
        </tr>
    </table>
</form>