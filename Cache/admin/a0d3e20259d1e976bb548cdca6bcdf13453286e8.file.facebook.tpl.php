<?php /* Smarty version Smarty-3.0.7, created on 2012-10-08 15:01:21
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/facebook/facebook.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1714391125072cea1e66151-39588781%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a0d3e20259d1e976bb548cdca6bcdf13453286e8' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/facebook/facebook.tpl',
      1 => 1349183157,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1714391125072cea1e66151-39588781',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<form action="" method="post">
    <table cellspacing="0" cellpadding="0" style="100%">
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 80%"></colgroup>
        <tr>
            <td class="fieldTitle" colspan="2">
               1. Facebook Connection
            </td>
        </tr>
        <?php if (isset($_smarty_tpl->getVariable('successMessage',null,true,false)->value)&&count($_smarty_tpl->getVariable('appErrors')->value)==0){?>
        <tr>
            <td class="successMessage" colspan="2">
               <?php echo $_smarty_tpl->getVariable('successMessage')->value;?>

            </td>
        </tr>
        <?php }?>
        <?php if (isset($_smarty_tpl->getVariable('successAccToken',null,true,false)->value)){?>
        <tr>
            <td class="successMessage" colspan="2">
               <?php echo $_smarty_tpl->getVariable('successAccToken')->value;?>

            </td>
        </tr>
        <?php }?>
        <?php if (isset($_smarty_tpl->getVariable('appErrors',null,true,false)->value['appID'])){?>
        <tr>
            <td class="error" colspan="2">
               <?php echo $_smarty_tpl->getVariable('appErrors')->value['appID'];?>

            </td>
        </tr>
        <?php }?>
        <tr>
            <td class="fieldName">
                Application ID:
            </td>
            <td>
                <input type="text" class="input" name="appID" value="<?php if (isset($_POST['appID'])){?><?php echo $_POST['appID'];?>
<?php }elseif(isset($_smarty_tpl->getVariable('appValues',null,true,false)->value['appID'])){?><?php echo $_smarty_tpl->getVariable('appValues')->value['appID'];?>
<?php }else{ ?><?php }?>" size="30">
            </td>
        </tr>
        <?php if (isset($_smarty_tpl->getVariable('appErrors',null,true,false)->value['appSecret'])){?>
        <tr>
            <td class="error" colspan="2">
               <?php echo $_smarty_tpl->getVariable('appErrors')->value['appSecret'];?>

            </td>
        </tr>
        <?php }?>
        <tr>
            <td class="fieldName">
                Application secret:
            </td>
            <td>
                <input type="text" class="input" name="appSecret" value="<?php if (isset($_POST['appSecret'])){?><?php echo $_POST['appSecret'];?>
<?php }elseif(isset($_smarty_tpl->getVariable('appValues',null,true,false)->value['appSecret'])){?><?php echo $_smarty_tpl->getVariable('appValues')->value['appSecret'];?>
<?php }else{ ?><?php }?>" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName" colspan="2">
                <input type="submit" value="Save Changes" name="saveChangesApp" />
                <?php if (isset($_smarty_tpl->getVariable('loginUrl',null,true,false)->value)&&$_smarty_tpl->getVariable('loginUrl')->value!=''&&$_smarty_tpl->getVariable('loginUrl')->value!='false'){?>
                    <a href="<?php echo $_smarty_tpl->getVariable('loginUrl')->value;?>
" title="Grant Permissions">Grant permissions</a>
                <?php }elseif(isset($_smarty_tpl->getVariable('loginUrl',null,true,false)->value)&&$_smarty_tpl->getVariable('loginUrl')->value=='false'&&$_smarty_tpl->getVariable('loginUrl')->value!=''){?>
                    You can't obtain permissions for this app. Enter some valid data !
                <?php }else{ ?>
                    Access Granted
                <?php }?>
           </td>
        </tr>
    </table>
</form>

<?php if (isset($_smarty_tpl->getVariable('facebookUser',null,true,false)->value['pages'])){?>
<form action="" method="post">
    <table cellspacing="0" cellpadding="0" style="100%">
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 80%"></colgroup>
        <tr>
            <td class="fieldTitle" colspan="2">
               2. Publishing
            </td>
        </tr>
        <tr>
            <td colspan="2" class="fieldMessage">
               Specify the posts to publish and decide on which Facebook wall they will appear.
            </td>
        </tr>
        <tr>
            <td class="fieldName">
               Page or profile ID:
            </td>
            <td>
                <input type="text" class="input" name="profileID" value="<?php if (isset($_smarty_tpl->getVariable('facebookUser',null,true,false)->value)){?><?php echo $_smarty_tpl->getVariable('facebookUser')->value['publishID'];?>
<?php }?>" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName" colspan="2">
                <ul>
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('facebookUser')->value['pages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                        <?php if (isset($_smarty_tpl->tpl_vars['item']->value['name'])){?>
                        <li><a href="/cms//social_media/social_media.php?media=facebook&action=new&page=<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></li>
                        <?php }?>
                    <?php }} ?>
                </ul>
            </td>
        </tr>
    </table>
</form>
<?php }?>