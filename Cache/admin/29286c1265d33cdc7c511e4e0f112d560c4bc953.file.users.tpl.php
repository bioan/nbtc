<?php /* Smarty version Smarty-3.0.7, created on 2013-03-11 16:16:02
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/users.tpl" */ ?>
<?php /*%%SmartyHeaderCode:56905001513df5321adf78-78215455%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '29286c1265d33cdc7c511e4e0f112d560c4bc953' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/users.tpl',
      1 => 1362739492,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '56905001513df5321adf78-78215455',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript">/* <![CDATA[ */
function setCheckBoxen($value)
{
    var $obj = document.getElementsByTagName('INPUT');
    for (var $i=0; $i < $obj.length; $i++) {
        if($obj[$i].type == 'checkbox') {
            if ($value == 1) {
                $obj[$i].checked = true;
            } else {
                $obj[$i].checked = false;
            }
        } 
    }
}
  
function getSelectedValue($thisForm)
{
    
    $obj = document.forms[0].sStatus;
    if ($obj.options[$obj.selectedIndex].value == 1) {
        setCheckBoxen(1);
    } else {
        setCheckBoxen(2);
    }
}
/* ]]> */</script><?php if (isset($_smarty_tpl->getVariable('new_user',null,true,false)->value)){?><p class="info">Hieronder heeft u de mogelijkheid een extra gebruiker aan te maken.</p><br /><br /><table style="width: 100%;" cellspacing="0"><form action="" method="post"><colgroup style="width: 3%;"></colgroup><colgroup style="width: 10%;"></colgroup><colgroup style="width: 87%;"></colgroup><tbody><?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?><tr><td colspan="3">De volgende velden zijn niet (goed) ingevuld: <br /><ol><?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?><li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li><?php }} ?></ol></td></tr><?php }?><tr><td>&nbsp;</td><td class="formoptieNaam"><label for="user_name">GebruikersNaam</label>:</td><td><input type="text" name="user_name" id="user_name" value="<?php if (isset($_smarty_tpl->getVariable('smary',null,true,false)->value['post']['user_name'])){?><?php echo $_smarty_tpl->getVariable('smary')->value['post']['user_name'];?>
<?php }?>" size="40" /></td></tr><tr><td>&nbsp;</td><td class="formoptieNaam"><label for="password">Wachtwoord</label>:</td><td><input type="password" name="password" id="password" value="" size="40" /></td></tr><tr><td>&nbsp;</td><td class="formoptieNaam"><label for="email">Emailadres</label>:</td><td><input type="text" name="email" id="email" value="<?php if (isset($_POST['email'])){?><?php echo $_POST['email'];?>
<?php }?>" size="40" /></td></tr><tr><td>&nbsp;</td><td class="formoptieNaam"><label for="status">Status</label>:</td><td><select name="status" id="status" onchange="getSelectedValue(); return false"><option value="2">Klant</option><option value="1">Admin</option></select></td></tr><tr><td>&nbsp;</td><td class="formoptieNaam" style="vertical-align: top;">Permissies:</td><td><?php  $_smarty_tpl->tpl_vars['page'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('menuPages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['page']->key => $_smarty_tpl->tpl_vars['page']->value){
?><input type="checkbox" name="permissions[]" value=<?php echo $_smarty_tpl->tpl_vars['page']->value['id'];?>
 id="f_000<?php echo $_smarty_tpl->tpl_vars['page']->value['id'];?>
" /><label for="f_000<?php echo $_smarty_tpl->tpl_vars['page']->value['id'];?>
"><?php echo stripslashes($_smarty_tpl->tpl_vars['page']->value['show_name']);?>
</label><br /><?php }} ?></td></tr><tr><td>&nbsp;</td><td class="formoptieNaam">Actief:</td><td><input type="radio" name="active" value="0">Nee</option><input type="radio" name="active" value="1">Ja</option></select></td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td><a href="#" onclick="document.forms[0].submit();"><img name="verzenden" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/save.png" alt="Toevoegen" /></a></td></tr></tbody></table></form><?php }elseif(isset($_smarty_tpl->getVariable('succes',null,true,false)->value)){?><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/button_ok.gif" alt="Ok" />&nbsp;<?php echo $_smarty_tpl->getVariable('succes')->value;?>
<br /><br />U wordt binnen 2 seconden doorgestuurd naar het overview.<meta http-equiv=refresh content="2; url=<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/users.php?action=overview"><?php }elseif(isset($_smarty_tpl->getVariable('edit_user',null,true,false)->value)){?><table style="width: 100%;" cellspacing="0"><form action="" method="post"><colgroup style="width: 3%;"></colgroup><colgroup style="width: 10%;"></colgroup><colgroup style="width: 87%;"></colgroup><tbody><?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?><tr><td colspan="3">De volgende velden zijn niet (goed) ingevuld: <br /><ol><?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?><li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li><?php }} ?></ol></td></tr><?php }?><tr><td>&nbsp;</td><td class="formoptieNaam"><label for="user_name">GebruikersNaam</label>:</td><td><input type="text" name="user_name" id="user_name" value="<?php echo $_smarty_tpl->getVariable('userData')->value['user_name'];?>
" size="40" /></td></tr><tr><td>&nbsp;</td><td class="formoptieNaam"><label for="password" title="Laat het wachtwoord leeg indien deze niet gewijzigd dient te worden!" >Wachtwoord</label>:</td><td><input type="password" name="password" id="password" value="" size="40" title="Laat het wachtwoord leeg indien deze niet gewijzigd dient te worden!"  /><input type="hidden" name="password_hidden" value="<?php echo $_smarty_tpl->getVariable('userData')->value['password'];?>
" /></td></tr><tr><td>&nbsp;</td><td class="formoptieNaam"><label for="email">Emailadres</label>:</td><td><input type="text" name="email" id="email" value="<?php echo $_smarty_tpl->getVariable('userData')->value['email'];?>
" size="40" /></td></tr><tr><td>&nbsp;</td><td class="formoptieNaam"><label for="status">Status</label>:</td><td><select name="status" id="status" onchange="getSelectedValue(); return false"><option value="2" <?php if ($_smarty_tpl->getVariable('userData')->value['status']==2){?> selected <?php }?> >Klant</option><option value="1" <?php if ($_smarty_tpl->getVariable('userData')->value['status']==1){?> selected <?php }?> >Admin</option></select></td></tr><tr><td>&nbsp;</td><td class="formoptieNaam" style="vertical-align: top;">Permissies:</td><td><?php  $_smarty_tpl->tpl_vars['page'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('menuPages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['page']->key => $_smarty_tpl->tpl_vars['page']->value){
?><input type="checkbox" name="permissions[]" value=<?php echo $_smarty_tpl->tpl_vars['page']->value['id'];?>
 id="f_000<?php echo $_smarty_tpl->tpl_vars['page']->value['id'];?>
" <?php  $_smarty_tpl->tpl_vars['rechten'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('permissions')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['rechten']->key => $_smarty_tpl->tpl_vars['rechten']->value){
?><?php if ($_smarty_tpl->tpl_vars['rechten']->value['menu_id']==$_smarty_tpl->tpl_vars['page']->value['id']){?>checked=checked<?php }?> <?php }} ?> /><label for="f_000<?php echo $_smarty_tpl->tpl_vars['page']->value['id'];?>
"><?php echo stripslashes($_smarty_tpl->tpl_vars['page']->value['show_name']);?>
</label><br /><?php }} ?></td></tr><tr><td>&nbsp;</td><td class="formoptieNaam">Actief:</td><td><input type="radio" name="active" value="0" <?php if ($_smarty_tpl->getVariable('userData')->value['active']==0){?> checked <?php }?>>Nee</option><input type="radio" name="active" value="1" <?php if ($_smarty_tpl->getVariable('userData')->value['active']==1){?> checked <?php }?>>Ja</option></select></td></tr><tr class="inputHeight"><td>&nbsp;</td><td>&nbsp;</td><td><a href="#" onclick="document.forms[0].submit();" id="save"><img name="verzenden" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/save.png" alt="Toevoegen" /></a><a class="verwijderen" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/users.php?action=delete&amp;id=<?php echo $_smarty_tpl->getVariable('userData')->value['id'];?>
" onclick="return confirm('Wilt u deze afbeelding verwijderen?!');"></a></td></tr></tbody></table></form><?php }else{ ?><table style="width: 100%;" id="t1" class="sortable" cellpadding="2" cellspacing="0"><colgroup style="width: 18%"></colgroup><colgroup style="width: 23%"></colgroup><colgroup style="width: 24%"></colgroup><colgroup style="width: 35%"></colgroup><tbody><tr><td colspan="4" class="head"><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_left.jpg" id="head_left" alt="" /><h2 class="font">Overzicht</h2><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_right.jpg" id="head_right" alt="" /></td></tr><tr><td class="first-row-first-column-white"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
users.php?action=overview&sort=active&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Actief</a></td><td class="first-row-column-white"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
users.php?action=overview&sort=user_name&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Gebruikersnaam</a></td><td class="first-row-column-white"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
users.php?action=overview&sort=status&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Status</a></td><td class="first-row-last-column"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
users.php?action=overview&sort=date_format&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Datum aangemaakt</a></td></tr><?php if (!isset($_smarty_tpl->getVariable('users',null,true,false)->value)){?><tr><td class="first-column-gray">Er zijn nog geen gebruikers toegevoegd</td><td class="column-gray" colspan="2"></td><td class="last-column-gray"></td></tr><?php }else{ ?><?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('users')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['iCounter']['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['iCounter']['iteration']++;
?><tr ondblclick="window.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
users.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
'" class="pointer"><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter']['iteration']%2==1){?><td class="first-column-gray"><?php if ($_smarty_tpl->tpl_vars['user']->value['active']==1){?><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/green.png" style="cursor: pointer;margin:4px 0px 0px 4px;" onclick="parent.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/users.php?action=actief&amp;id=<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
&amp;status=non';" alt="Actief" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/red.png" style="cursor: pointer;margin:4px 0px 0px 4px;" onclick="parent.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/users.php?action=actief&amp;id=<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
&amp;status=actief';" alt="Non-Actief" /><?php }?></td><td class="column-gray"><?php echo $_smarty_tpl->tpl_vars['user']->value['user_name'];?>
</td><td class="column-gray"><?php if ($_smarty_tpl->tpl_vars['user']->value['status']==1){?>Admin<?php }else{ ?>klant<?php }?></td><td class="last-column-gray"><?php echo $_smarty_tpl->tpl_vars['user']->value['date_format'];?>
</td><?php }else{ ?><td class="first-column-white"><?php if ($_smarty_tpl->tpl_vars['user']->value['active']==1){?><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/green.png" style="cursor: pointer;margin:4px 0px 0px 4px;" onclick="parent.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/users.php?action=actief&amp;id=<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
&amp;status=non';" alt="Actief" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/red.png" style="cursor: pointer;margin:4px 0px 0px 4px;" onclick="parent.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/users.php?action=actief&amp;id=<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
&amp;status=actief';" alt="Non-Actief" /><?php }?></td><td class="column-white"><?php echo $_smarty_tpl->tpl_vars['user']->value['user_name'];?>
</td><td class="column-white"><?php if ($_smarty_tpl->tpl_vars['user']->value['status']==1){?>Admin<?php }else{ ?>klant<?php }?></td><td class="last-column-white"><?php echo $_smarty_tpl->tpl_vars['user']->value['date_format'];?>
</td><?php }?></tr><?php }} ?><?php }?><tr><td class="last-row-first-column"></td><td class="last-row-column-white"></td><td class="last-row-column-white"></td><td class="last-row-last-column"></td></tr></tbody></table><div style="padding-top: 0.6em; text-align: center;"><?php if (isset($_smarty_tpl->getVariable('links',null,true,false)->value)){?><?php echo $_smarty_tpl->getVariable('links')->value;?>
<?php }?></div><br /><?php }?>
