<?php /* Smarty version Smarty-3.0.7, created on 2012-10-23 15:00:44
         compiled from "/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/photoalbum/photoalbum.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1751521203508694fcd34714-96484657%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'adc79db3394743adc090643af5adba30ccc46136' => 
    array (
      0 => '/home/andrei/domains/cmsforlife.nl/public_html/cms/cms/tpls/photoalbum/photoalbum.tpl',
      1 => 1337175098,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1751521203508694fcd34714-96484657',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_GET['action']=='new'){?><form method="post" action="" enctype="multipart/form-data"><table style="width: 100%;"><colgroup style="width: 20%"></colgroup><colgroup style="width: 80%"></colgroup><tbody><?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?><tr><td colspan="2">De volgende velden zijn niet (goed) ingevuld: <br /><ol><?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?><li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li><?php }} ?></ol></td></tr><?php }?><tr><td class="fieldName"><label for="category">Categorie</label>:</td><td><select name="category" id="category" onchange="checkIfDisabled(this);"><?php if (isset($_smarty_tpl->getVariable('nodes',null,true,false)->value)){?><?php  $_smarty_tpl->tpl_vars['node'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('nodes')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['node']->key => $_smarty_tpl->tpl_vars['node']->value){
?><?php if ($_smarty_tpl->tpl_vars['node']->value['name']!='_root'){?><?php if (in_array($_smarty_tpl->tpl_vars['node']->value['oid'],$_smarty_tpl->getVariable('leafNodes')->value)&&$_smarty_tpl->tpl_vars['node']->value['oid']!=0){?><option value="<?php echo $_smarty_tpl->tpl_vars['node']->value['oid'];?>
" <?php if (isset($_POST['category'])&&$_POST['category']==$_smarty_tpl->tpl_vars['node']->value['oid']){?> selected="selected" <?php }?>><?php echo str_repeat("&nbsp;",$_smarty_tpl->tpl_vars['node']->value['level']*2);?>
<?php echo stripslashes($_smarty_tpl->tpl_vars['node']->value['name']);?>
</option><?php }else{ ?><option value="<?php echo $_smarty_tpl->tpl_vars['node']->value['oid'];?>
" disabled="disabled" style="color: #CCCCCC;"><?php echo str_repeat("&nbsp;",$_smarty_tpl->tpl_vars['node']->value['level']*2);?>
<?php echo stripslashes($_smarty_tpl->tpl_vars['node']->value['name']);?>
</option><?php }?><?php }?><?php }} ?><?php }else{ ?><option value="0">Er zijn geen categorie&euml;n aangemaakt.</option><?php }?></select></td></tr><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?><?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['id'], null, null);?><tr><td class="fieldName"><label>Foto</label>:</td><td><ul class="photo-menu"><li><div id="upload_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><span>kiezen</span></div></li><li><a class="greybox" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a></li><li class="last"><a class="greybox browser" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a></li></ul><span id="status_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" ></span><br class="clear" /><span id="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if (isset($_POST['photos'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php  $_smarty_tpl->tpl_vars['photoOid'] = new Smarty_Variable;
 $_from = $_POST['photos'][$_smarty_tpl->getVariable('id')->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photoOid']->key => $_smarty_tpl->tpl_vars['photoOid']->value){
?><img id="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
/100/image.php" /><input name="photos[<?php echo $_smarty_tpl->getVariable('id')->value;?>
][<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photoOid']->value;?>
" type="hidden" /><?php }} ?><?php }?></span></td></tr><tr><td class="fieldName"><label for="title_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Titel <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><input type="text" class="input" name="title[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" value="<?php if (isset($_POST['title'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php echo $_POST['title'][$_smarty_tpl->getVariable('id')->value];?>
<?php }?>" id="title_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" size="40" /></td></tr><tr><td class="fieldName"><label for="f_id_2">Bericht <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><?php echo $_smarty_tpl->getVariable('FCKeditor')->value[$_smarty_tpl->getVariable('id')->value];?>
</td></tr><tr><td class="fieldName"><label for="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Page description <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><textarea name="page_description[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if (isset($_POST['page_description'][$_smarty_tpl->getVariable('id',null,true,false)->value])){?><?php echo $_POST['page_description'][$_smarty_tpl->getVariable('id')->value];?>
<?php }?></textarea></td></tr><?php }} ?><?php if (isset($_smarty_tpl->getVariable('keywords',null,true,false)->value)){?><tr><td class="fieldName">Keywords</td><td><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('keywords')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value;?>
<br /><?php }} ?></td></tr><?php }?><tr><td></td><td><br /><input type="hidden" id="module"  value="photoalbum" name="module"  /><input type="image" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/save.png"></td></tr></tbody></table></form><?php }elseif($_GET['action']=='overview'){?><script type="text/javascript">/* <![CDATA[ */ 
  	function showHide(obj)
  	{
  		var item = obj.parentNode.nextSibling.nextSibling;
		if (item.style.display == 'none') {
			item.style.display = 'block';
		} else {
			item.style.display = 'none';
		}
	}/* ]]> */</script>Rijen weergeven :&nbsp;<select onchange="window.location='photoalbum.php?action=overview&items=' + this.value + '<?php if (isset($_GET['sort'])){?>&sort=<?php echo $_GET['sort'];?>
<?php }?><?php if (isset($_GET['sorttype'])){?>&sorttype=<?php echo $_GET['sorttype'];?>
<?php }?>'"><option value="5" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==5){?> selected="selected"<?php }?>>5</option><option value="10" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==10){?> selected="selected"<?php }?>>10</option><option value="25" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==25){?> selected="selected"<?php }?>>25</option><option value="50" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==50){?> selected="selected"<?php }?>>50</option><option value="100" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==100){?> selected="selected"<?php }?>>100</option><option value="10000" <?php if (isset($_smarty_tpl->getVariable('items',null,true,false)->value)&&$_smarty_tpl->getVariable('items')->value==10000){?> selected="selected"<?php }?>>all</option></select><table style="width: 100%;" id="t1" class="photoalbum" cellpadding="2" cellspacing="0"><colgroup style="width: 30%"></colgroup><colgroup style="width: 30%"></colgroup><colgroup style="width: 20%"></colgroup><colgroup style="width: 20%"></colgroup><tbody><tr nodrag="true" nodrop="true"><td colspan="4" class="head"><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_left.jpg" id="head_left" alt="" /><h2 class="font">Overzicht</h2><img src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/images/head_right.jpg" id="head_right" alt="" /></td></tr><tr nodrag="true" nodrop="true"><td class="first-row-first-column-white"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/photoalbum.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=title&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Naam</a></td><td class="first-row-column-white"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/photoalbum.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=category_name&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Categorie</a></td><td class="first-row-column-white"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/photoalbum.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=date&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Project datum</a></td><td class="first-row-last-column"><a href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/photoalbum.php?action=overview&items=<?php echo $_smarty_tpl->getVariable('items')->value;?>
&sort=filled&sorttype=<?php if ($_smarty_tpl->getVariable('sorttype')->value=='desc'){?>asc<?php }else{ ?>desc<?php }?>&pageID=<?php echo $_smarty_tpl->getVariable('pageID')->value;?>
">Stats</a></td></tr><?php if (!$_smarty_tpl->getVariable('photos')->value){?><tr nodrag="true" nodrop="true"><td class="first-column-gray">Er zijn nog geen photoalbum items gepost.</td><td class="column-gray"></td><td class="column-gray"></td><td class="last-column-gray"></td></tr><?php }?><?php  $_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('photos')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photo']->key => $_smarty_tpl->tpl_vars['photo']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["iCounter"]['iteration']++;
?><tr id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
" order_number="<?php echo $_smarty_tpl->tpl_vars['photo']->value['order_number'];?>
" ondblclick="window.location.href = '<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
photoalbum/photoalbum.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['photo']->value['id'];?>
'" class="pointer" ><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['iCounter']['iteration']%2==1){?><td class="first-column-gray"><?php echo stripslashes($_smarty_tpl->tpl_vars['photo']->value['title']);?>
</td><td class="column-gray"><?php echo stripslashes($_smarty_tpl->tpl_vars['photo']->value['category_name']);?>
</td><td class="column-gray"><?php echo $_smarty_tpl->tpl_vars['photo']->value['date_format'];?>
</td><td class="last-column-gray"><span class="<?php if ($_smarty_tpl->tpl_vars['photo']->value['filled']==count($_smarty_tpl->getVariable('languages')->value)){?>green<?php }else{ ?>red<?php }?>"><?php echo $_smarty_tpl->tpl_vars['photo']->value['filled'];?>
 / <?php echo count($_smarty_tpl->getVariable('languages')->value);?>
</span></td><?php }else{ ?><td class="first-column-white"><?php echo stripslashes($_smarty_tpl->tpl_vars['photo']->value['title']);?>
</td><td class="column-white"><?php echo stripslashes($_smarty_tpl->tpl_vars['photo']->value['category_name']);?>
</td><td class="column-white"><?php echo $_smarty_tpl->tpl_vars['photo']->value['date_format'];?>
</td><td class="last-column-white"><span class="<?php if ($_smarty_tpl->tpl_vars['photo']->value['filled']==count($_smarty_tpl->getVariable('languages')->value)){?>green<?php }else{ ?>red<?php }?>"><?php echo $_smarty_tpl->tpl_vars['photo']->value['filled'];?>
 / <?php echo count($_smarty_tpl->getVariable('languages')->value);?>
</span></td><?php }?></tr><?php }} ?><tr nodrag="true" nodrop="true"><td class="last-row-first-column"></td><td class="last-row-column-white"></td><td class="last-row-column-white"></td><td class="last-row-last-column"></td></tr></tbody></table><div style="padding-top: 0.6em; text-align: center;"><?php echo $_smarty_tpl->getVariable('links')->value;?>
</div><?php }elseif($_GET['action']=='edit'){?><form method="post" action="" enctype="multipart/form-data"><table style="width: 100%;"><colgroup style="width: 10%"></colgroup><colgroup style="width: 90%"></colgroup><tbody><?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?><tr><td colspan="2">De volgende velden zijn niet (goed) ingevuld: <br /><ol><?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('error')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
?><li><span style="color: #FF0000;"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li><?php }} ?></ol></td></tr><?php }?><tr><td class="fieldName"><label for="category">Categorie</label>:</td><td><select name="category" id="category" onchange="checkIfDisabled(this);"><?php if (isset($_smarty_tpl->getVariable('nodes',null,true,false)->value)){?><?php  $_smarty_tpl->tpl_vars['node'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('nodes')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['node']->key => $_smarty_tpl->tpl_vars['node']->value){
?><?php if ($_smarty_tpl->tpl_vars['node']->value['name']!='_root'){?><?php if (in_array($_smarty_tpl->tpl_vars['node']->value['oid'],$_smarty_tpl->getVariable('leafNodes')->value)&&$_smarty_tpl->tpl_vars['node']->value['oid']!=0){?><option value="<?php echo $_smarty_tpl->tpl_vars['node']->value['oid'];?>
" <?php if ($_smarty_tpl->tpl_vars['node']->value['oid']==$_smarty_tpl->getVariable('photo')->value['category_id']){?> selected="selected" <?php }?>><?php echo str_repeat("&nbsp;",$_smarty_tpl->tpl_vars['node']->value['level']*2);?>
<?php echo stripslashes($_smarty_tpl->tpl_vars['node']->value['name']);?>
</option><?php }else{ ?><option value="<?php echo $_smarty_tpl->tpl_vars['node']->value['oid'];?>
" disabled="disabled" style="color: #CCCCCC;"><?php echo str_repeat("&nbsp;",$_smarty_tpl->tpl_vars['node']->value['level']*2);?>
<?php echo stripslashes($_smarty_tpl->tpl_vars['node']->value['name']);?>
</option><?php }?><?php }?><?php }} ?><?php }else{ ?><option value="0">Er zijn geen categorie&euml;n aangemaakt.</option><?php }?></select></td></tr><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('languages')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?><?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['id'], null, null);?><tr><td class="fieldName"><label>Afbeelding</label>:</td><td><ul class="photo-menu"><li><div id="upload_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><span>kiezen</span></div></li><li><a class="greybox" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/crop.php">kiezen + bijsnijden</a></li><li class="last"><a class="greybox browser" area="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/browser.php">bladeren</a></li></ul><span id="status_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" ></span><br class="clear" /><span id="photos_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if (!empty($_smarty_tpl->getVariable('photo',null,true,false)->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['photos'])){?><?php  $_smarty_tpl->tpl_vars['photos'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('photo')->value[$_smarty_tpl->getVariable('id')->value]['photos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['photos']->key => $_smarty_tpl->tpl_vars['photos']->value){
?><img id="<?php echo $_smarty_tpl->tpl_vars['photos']->value['id'];?>
" onclick="removePhoto(this);" style="cursor: pointer;" src="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/foto.php/<?php echo $_smarty_tpl->tpl_vars['photos']->value['id'];?>
/100/image.php" /><input name="photos[<?php echo $_smarty_tpl->getVariable('id')->value;?>
][<?php echo $_smarty_tpl->tpl_vars['photos']->value['id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['photos']->value['id'];?>
" type="hidden" /><?php }} ?><?php }?></span></td></tr><tr><td class="fieldName"><label for="title_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Titel <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><input type="text" class="input" name="title[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" value="<?php if (isset($_smarty_tpl->getVariable('photo',null,true,false)->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['title'])){?><?php echo $_smarty_tpl->getVariable('photo')->value[$_smarty_tpl->getVariable('id')->value]['title'];?>
<?php }?>" id="title_<?php echo $_smarty_tpl->getVariable('id')->value;?>
" size="40" /></td></tr><tr><td class="fieldName"><label>Omschrijving <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><?php echo $_smarty_tpl->getVariable('FCKeditor')->value[$_smarty_tpl->getVariable('id')->value];?>
</td></tr><tr><td class="fieldName"><label for="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
">Page description <?php echo $_smarty_tpl->tpl_vars['item']->value['language'];?>
</label>:</td><td><textarea name="page_description[<?php echo $_smarty_tpl->getVariable('id')->value;?>
]" id="page_description_<?php echo $_smarty_tpl->getVariable('id')->value;?>
"><?php if (isset($_smarty_tpl->getVariable('photo',null,true,false)->value[$_smarty_tpl->getVariable('id',null,true,false)->value]['page_description'])){?><?php echo $_smarty_tpl->getVariable('photo')->value[$_smarty_tpl->getVariable('id')->value]['page_description'];?>
<?php }?></textarea></td></tr><?php }} ?><?php if (isset($_smarty_tpl->getVariable('keywords',null,true,false)->value)){?><tr><td class="fieldName">Keywords</td><td><?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('keywords')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value;?>
<br /><?php }} ?></td></tr><?php }?><tr><td></td><td><br /><input type="hidden" id="module"  value="photoalbum" name="module"  /><input type="submit" src="" value="" id="save"><a class="verwijderen" href="<?php echo $_smarty_tpl->getVariable('SiteRoot')->value;?>
/photoalbum/photoalbum.php?action=delete&amp;id=<?php echo $_smarty_tpl->getVariable('photo')->value['id'];?>
" onclick="return confirm('Wilt u deze afbeelding verwijderen?!');"></a><input type="submit" class="keywords" value="" name="keywords"  /></td></tr></table></form><?php }?>
