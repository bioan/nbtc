<?php
require_once '../cms/fixpath.php';
require_once '../cms/includes/Smarty.class.php';
require_once '../Configuration.php';
require_once '../cms/includes/Database.class.php';
require_once '../cms/includes/Language.class.php';
require_once '../cms/includes/Newsletter.class.php';

$oNewsletter = new Newsletter ();
$oTemplate   = new Smarty ();

$aLink = explode ('?', substr ($_SERVER ['REQUEST_URI'], 1));

$sPageRequest = str_replace ('.html', '', $aLink [0]);
$aPageRequest = explode ('/', $sPageRequest);

$aGroup = $oNewsletter -> getTemplates ($aPageRequest [1]);

$aData = json_decode ($aGroup ['values'], true);


if (!empty($aData))
foreach ($aData as $k => $item)
{
        if (preg_match ('%text_+%s', $k) && $k != 'text_text2')
        {
                $sText = strip_tags ($aData [$k], '<strong><b><i><u><span><a><br><img>');
                $sText = str_replace ('<br />', '<br>', $sText);
                if (preg_match_all ('%<img[^>]+>%s', $sText, $aMatch))
                {
                        $aStyles = $aMatch [0];
                }
                
                if (isset($aStyles)) 
                {
                        foreach ($aStyles as $img)
                        {
                                $newimg = '';
                                if (strpos ($img, 'float: right;') !== false)
                                {
                                        $newimg = substr (str_replace ('float: right;', '', $img), 0, -2) . ' align="right" ' . ' >';
                                }
                                
                                if (strpos ($img, 'float: left;') !== false)
                                {
                                        $newimg = substr (str_replace ('float: left;', '', $img), 0, -2) . ' align="left" ' . ' >';
                                }
                                
                                if ($newimg != '')
                                        $sText = str_replace ($img, $newimg, $sText);
                        }
                        
                }
                
                
                
                if (preg_match_all ('%src="[^"]+"%s', $sText, $aMatch))
                {
                        $aImgs = $aMatch [0];
                }
                
                if (isset($aImgs))
                        foreach ($aImgs as $img)
                        {
                                $sText = str_replace ($img, 'src="' . str_replace(' ', '%20', (str_replace (array ('src="', '"'), '', $img))) . '"', $sText);
                        }
                
                $sText = str_replace ('src="/userfiles', 'src="' . $GLOBALS ['cfg']['sWebsiteUrl'] . 'userfiles', $sText);
                $aData [$k] = str_replace ('[lees_meer]', '', strip_tags ($aData [$k], '<strong><b><i><u><span><a><br><img>'));
                $aText = explode ('[lees_meer]', $sText); 
                
                if (isset ($aText [1]) && isset ($aPageRequest [2]))
                        $aText [0] = $aText [0] . '<a href="' . $GLOBALS ['cfg']['sWebsiteUrl'] . 'mail/' . ($aPageRequest [2] != 0 ? 'history/' . $aPageRequest [2] : $aTemplate ['id']) . '.html#' . $k . '">Lees meer &raquo;</a>';
                
                if (isset ($aLink [1]))
                {
                        $aData [$k] = $aText [0];
                }
                else
                        $aData [$k] = $sText;
                //if ($k == 'text_text5'){print_r($aData [$k]); die();}
                
        }
        elseif ($k == 'text_text2')
        {
                $aData ['text_text2'] = str_replace ('<li>', '<li style="margin:0px 0px 0px 0px;padding:0px 0px 0px 10px;color: #123a87;font-family: Verdana;font-size: 12px;font-weight: bold;line-height: 21px;">', $aData ['text_text2']);
                $aData ['text_text2'] = str_replace ('<ul>', '<ul style="float:left;padding: 0px 15px 0px 20px;margin:0px 0px 0px 0px;display: block;height: 100%;"><li style="list-style: none;"><span style="height: 13px;line-height: 13px;display: block;">&nbsp;</span></li>', $aData ['text_text2']);
                $aData ['text_text2'] = str_replace ('</ul>', '<li style="margin:0px 0px 0px 0px;list-style: none;"><span style="height: 13px;line-height: 13px;display: block;">&nbsp;</span></li></ul>', $aData ['text_text2']);       
        }
        
        if (preg_match ('%image_+%s', $k))
        {
                if ($k != 'image_1')
                {
                        $aImagePath = array ();
                        $aImage = $oNewsletter -> getPhotosByOids (current ($aData [$k]));
                        
                        $aImagePath = glob ('../' . substr ($aImage [0]['pathphoto'], 1) . '/' . $aImage [0]['id'] . '_newsletter*');
                        
                        if (isset ($aImagePath [0]))
                        {
                                $aData [$k] = str_replace ('../', $GLOBALS ['cfg']['sWebsiteUrl'], $aImagePath [0]);
                        }
                        else
                        {
                                $aData [$k] = '';
                        }
                }
                
                if ($k == 'image_1')
                {
                        $sImage = '';
                        $aData ['image_1'] = $oNewsletter -> getPhotosByOids (implode (',', $aData [$k]));
                        
                        foreach ($aData ['image_1'] as $kp => $itemp)
                        {
                                $aEImage = array ();
                                $aEImage = glob ('../' . substr ($itemp ['pathphoto'], 1) . '/' . $itemp ['id'] . '_newsletter*');
                                
                                if (isset ($aEImage [0]))
                                {
                                        $aData ['image_1'][$kp] = str_replace ('../', $GLOBALS ['cfg']['sWebsiteUrl'], $aEImage [0]);
                                }
                                else
                                {
                                        $aData ['image_1'][$kp] = '';
                                }
                        }
                }
        }
         
        if (preg_match ('%items_+%s', $k))
        {
                if (strpos ($k, 'text') !== false)
                {
                        foreach ($aData [$k] as $ki => $itemi)
                        {
                                
                                if (preg_match_all ('%<img[^>]+>%s', $aData [$k][$ki], $aMatch))
                                {
                                        $aStyles = $aMatch [0];
                                }
                                
                                if (isset($aStyles)) 
                                {
                                        foreach ($aStyles as $img)
                                        {
                                                $newimg = '';
                                                if (strpos ($img, 'float: right;') !== false)
                                                {
                                                        $newimg = substr (str_replace ('float: right;', '', $img), 0, -2) . ' align="right" ' . ' >';
                                                }
                                                
                                                if (strpos ($img, 'float: left;') !== false)
                                                {
                                                        $newimg = substr (str_replace ('float: left;', '', $img), 0, -2) . ' align="left" ' . ' >';
                                                }
                                                
                                                $aData [$k][$ki] = str_replace ($img, $newimg, $aData [$k][$ki]);
                                        }
                                        
                                }
                                
                                if (preg_match_all ('%src="[^"]+"%s', $aData [$k][$ki], $aMatch))
                                {
                                        $aImgs = $aMatch [0];
                                }
                                
                                if (isset($aImgs))
                                        foreach ($aImgs as $img)
                                        {
                                                $aData [$k][$ki] = str_replace ($img, 'src="' . str_replace(' ', '%20', (str_replace (array ('src="', '"'), '', $img))) . '"', $aData [$k][$ki]);
                                        }
                                $aData [$k][$ki] = str_replace ('src="/userfiles', 'src="' . $GLOBALS ['cfg']['sWebsiteUrl'] . 'userfiles', $aData [$k][$ki]);
                                $aData [$k][$ki] = str_replace ('<br />', '<br>', $aData [$k][$ki]);
                                $sText = $aData [$k][$ki];
                                $aData [$k][$ki] = strip_tags (str_replace ('[lees_meer]', '', $aData [$k][$ki]), '<img><strong><b><i><u><span><a><br>');
                                $aText = explode ('[lees_meer]', $sText);
                                
                                if (isset ($aText [1]) && isset ($aPageRequest [2]))
                                        $aText [0] = $aText [0] . '<a href="http://' . $_SERVER ['SERVER_NAME'] . '/mail/' . ($aPageRequest [2] != 0 ? 'history/' . $aPageRequest [2] : $aTemplate ['id']) . '.html#' . $k . '_' . $ki . '">Lees meer &raquo;</a>';
                                
                                if (isset ($aLink [1]))
                                {
                                        $aData [$k][$ki] = $aText [0];
                                }
                                 
                                
                        }
                }
                elseif (strpos ($k, 'x') !== false)
                {
                        foreach ($aData [$k] as $ki => $itemi)
                        {
                                $aAImage = array ();
                                                
                                if (isset ($aData [$k][$ki]))
                                {
                                        $aPhoto = current ($oNewsletter -> getPhotosByOids ($aData [$k][$ki]));
                                        
                                        $aAImage = glob ('../' . substr ($aPhoto ['pathphoto'], 1) . '/' . $aPhoto ['id'] . '_newsletter*');
                                        
                                        if (isset ($aAImage [0]))
                                                $aData [$k] = $aAImage [0];
                                        else
                                                $aData [$k] = '';
                                        
                                }
                        }
                }   
        }  
}

//echo '<pre>' . print_r($aData,true);
$oTemplate -> assign ('online', !isset ($aPageRequest [2]) && is_numeric ($aPageRequest [1]) ? false : true);  
$oTemplate -> assign ('siteurl', $GLOBALS ['cfg']['sWebsiteUrl']);  
$oTemplate -> assign ('idsent', isset ($aPageRequest [2]) && is_numeric ($aPageRequest [2]) ? $aPageRequest [2] : 0);  
$oTemplate -> assign ('template', $aData);
$oTemplate -> assign ('private', json_decode ($aGroup ['private'], true));
$oTemplate -> display ('1.tpl');
?>

