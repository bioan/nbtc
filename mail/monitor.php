<?php
require '../Configuration.php';
require '../cms/includes/Database.class.php';
require '../cms/includes/Language.class.php';
require '../cms/includes/Newsletter.class.php';

$oNewsletter = new Newsletter ();

if (isset ($_GET ['mailing_id']) && isset ($_GET ['subscriber_id']) && 
isset ($_GET ['email']) && isset ($_GET ['l']) && is_numeric ($_GET ['mailing_id'])
&& is_numeric ($_GET ['subscriber_id']) && is_numeric ($_GET ['l']))
{
        
        $aMailing = $oNewsletter -> getMailings ($_GET ['mailing_id']);
        $aMonitor = $oNewsletter -> getMonitor ($_GET ['mailing_id'], $_GET ['subscriber_id']);
        
        if (!isset ($aMonitor [$_GET ['subscriber_id']]))
        {
                $aLinks = array ();
                
                foreach ($aMailing ['links'] as $k => $item)
                        $aLinks [$k] = 0;    
                
                $aLinks [$_GET ['l']] = 1;
                
                Database :: getInstance () -> query ("
                        INSERT INTO 
                            newsletter_monitor
                        (`mailing_id`, `subscriber_id`, `values`)
                        VALUES
                        (" . $_GET ['mailing_id'] . ", " . $_GET ['subscriber_id'] . ", 
                        '" . json_encode ($aLinks) . "')    
                ");
        }
        else
        {
                $aMonitor [$_GET ['subscriber_id']]['values'][$_GET ['l']]++;
                
                Database :: getInstance () -> query ("
                        UPDATE
                            newsletter_monitor
                        SET
                            `values` = '" . json_encode ($aMonitor [$_GET ['subscriber_id']]['values']) . "'
                        WHERE
                            mailing_id = " . $_GET ['mailing_id'] . " 
                        AND
                            subscriber_id = " . $_GET ['subscriber_id'] . "
                ");
        }
        if (!isset ($_GET ['redirect']))
        {
                header ("Content-type: " . mime_type ($_GET ['img']));
		        header ("Cache-Control: no-store, no-cache, must-revalidate");
		        header ("Cache-Control: post-check=0, pre-check=0", false);
		        header ("Pragma: no-cache");
                
                echo file_get_contents ($_GET ['img']);
        }
        else
        {
                header ('Location: ' . $_GET ['redirect']);
        }
}


function mime_type ($filename) 
{

        $mime_types = array(

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml'
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
}
?>