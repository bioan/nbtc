<?php 
require '../Configuration.php';
require '../cms/includes/Database.class.php';
require '../cms/includes/Language.class.php';
require '../cms/includes/Newsletter.class.php';

if (isset ($_GET ['subscriber_id']) && is_numeric ($_GET ['subscriber_id']) && strlen ($_GET ['c']) == 32)
        Database :: getInstance () -> query ("
                                UPDATE
                                    newsletter_subscribers
                                SET
                                    unsubscribed = 1
                                WHERE
                                        id = " . $_GET ['subscriber_id'] . "
                                    AND
                                        code = '" . Database :: getInstance () -> real_escape_string ($_GET ['c']) . "'
                        ");
?>
<html>
    <head>
    </head>
    <body>
        You are removed from newsletter.
    </body>
</html>