<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Bouwpluim</title>
    </head>
    <body style="text-align:center;margin:0;padding:0;height:auto;font-family: Verdana;font-size: 12px;">
        <div style="background: #f1f1f1;width:100%;height:100%;margin:0px;">
            {if $online == true}
            <p style="text-align: center;margin:0px 0px 0px 0px;">
                <a class="browser" href="" style="color:#737373;font-family:Verdana;font-size:11px;">Kun je deze nieuwsbrief niet goed lezen? Bekijk deze nieuwsbrief dan online.</a>
            </p>
            {/if}
            <table cellpadding="0" cellspacing="0" border="0" width="600" style="margin:auto;padding:auto;background-color: #fff;text-align: left;">
                <tr>
                    <td valign="top" style="width: 15px;line-height: 10px;">&nbsp;</td>
                    <td valign="top" style="width:569px;">
                        <table cellpadding="0" cellspacing="0" border="0" width="569" style="margin:auto;padding:auto;background-color: #fff;text-align: left;">
                            {if !isset($private.image_logo) || (isset($private.image_logo) && $private.image_logo == 0)}
                            <tr>
                                <td valign="top" width="569" style="text-align: right;">
                                    <a href=""><img src="{if isset($template.image_logo)}{$template.image_logo}{/if}" alt="Logo" /></a>  
                                </td>
                            </tr>
                            {/if}
                            {if !isset($private.image_maillogo) || (isset($private.image_logo) && $private.image_maillogo == 0)}
                            <tr>
                                <td valign="top" style="width: 569px;height: 1px;text-align: center;">
                                    <img src="{$siteurl}edm_images/bar3.jpg" alt="" style="display: block;" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;height: 60px;">
                                    <img src="{if isset($template.image_maillogo)}{$template.image_maillogo}{/if}" alt="Header" style="display: block;" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;height: 1px;text-align: center;">
                                    <img src="{$siteurl}edm_images/bar3.jpg" alt="" style="display: block;" />
                                </td>
                            </tr>
                            {/if}
                            {if !isset($private.text_text1) || (isset($private.text_text1) && $private.text_text1 == 0)}
                            <tr>
                                <td valign="top" style="height: 12px;line-height: 12px;width: 569px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;">
                                    <p id="text_text1" style="margin:0px;font-family: Verdana;font-size: 13px;font-weight: bold;line-height: 22px;width: 569px;color: #23408b;">
                                        {$template.text_text1}
                                    </p>
                                </td>
                            </tr>
                            {/if}
                            {if !isset($private.title_1) || (isset($private.title_1) && $private.title_1 == 0)}
                            <tr>
                                <td valign="middle" height="10" style="width: 569px;vertical-align: middle;height: 10px;line-height: 10px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;line-height: 35px;vertical-align: middle;">
                                    <a name="titel_1" style="text-decoration:none;margin:0px;font-family: Arial;font-size: 27px;color: #23408b;">
                                        {$template.title_1}    
                                    </a>
                                </td>
                            </tr>
                            {/if}
                            <!--<tr>
                                <td valign="middle" height="10" style="width: 569px;vertical-align: middle;height: 10px;line-height: 10px;">&nbsp;</td>
                            </tr>-->
                            {if !isset($private.text_text2) || (isset($private.text_text2) && $private.text_text2 == 0)}
                            <tr>
                                <td valign="top" style="width: 569px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="569">
                                        <tr>
                                            <td valign="top" style="width: 16px;line-height: 10px;">&nbsp;</td>
                                            <td id="text_text2" valign="top" style="width: 537px;">
                                                {$template.text_text2}
                                            </td>
                                            <td valign="top" style="width: 16px;line-height: 10px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            {/if}
                            <tr>
                                <td valign="middle" height="48" style="width: 569px;vertical-align: middle;height: 48px;">
                                    <img src="{$siteurl}edm_images/bar.jpg" alt="" />
                                </td>
                            </tr>
                            {if !isset($private.text_text3) || (isset($private.text_text3) && $private.text_text3 == 0)}
                            <tr>
                                <td valign="top" style="width: 569px;background-color: #f89820;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="569">
                                        <tr>
                                            <td valign="top" colspan="3" style="width: 569px;height: 14px;line-height: 14px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="width: 16px;line-height: 10px;">&nbsp;</td>
                                            <td valign="top" style="width: 569px;">
                                                <p id="text_text3" style="margin:0px;color: #23408b;font-family: Verdana;font-size: 13px;font-weight: bold;line-height: 21px;">
                                                    {$template.text_text3}
                                                </p>
                                            </td>
                                            <td valign="top" style="width: 16px;line-height: 10px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" colspan="3" style="width: 569px;height: 16px;line-height: 16px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            {/if}
                            {if !isset($private.title_2) || (isset($private.title_2) && $private.title_2 == 0)}
                            <tr>
                                <td valign="middle" height="10" style="width: 569px;vertical-align: middle;height: 10px;line-height: 10px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;line-height: 35px;vertical-align: middle;">
                                    <a name="titel_2" style="text-decoration:none;margin:0px;font-family: Arial;font-size: 23px;color: #23408b;">
                                        {$template.title_2}
                                    </a>
                                </td>
                            </tr>
                            {/if}
                            {if !isset($private.image_photo) || (isset($private.image_photo) && $private.image_photo == 0) || (!isset($private.text_text4) || (isset($private.text_text4) && $private.text_text4 == 0))}
                            <tr>
                                <td valign="middle" height="10" style="width: 569px;vertical-align: middle;height: 10px;line-height: 10px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="569">
                                        <tr>
                                            {if !isset($private.image_photo) || (isset($private.image_photo) && $private.image_photo == 0)}
                                            <td valign="top" style="width: 139px;">
                                                <table cellpadding="0" cellspacing="0" border="0" width="139">
                                                    {if !isset($private.image_photo) || (isset($private.image_photo) && $private.image_photo == 0)}
                                                    <tr>
                                                        <td valign="top" style="width: 139px;">
                                                            <img src="{if isset($template.image_photo)}{$template.image_photo}{/if}" alt="Photo" style="display: block;" />
                                                        </td>
                                                    </tr>
                                                    {/if}
                                                    {if !isset($private.title_3) || (isset($private.title_3) && $private.title_3 == 0)}
                                                    <tr>
                                                        <td valign="top" style="background: #ccd3e4;width: 139px;">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="139">
                                                                <tr>
                                                                    <td colspan="3" style="line-height: 10px;width: 5px;">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 10px;width: 5px;">&nbsp;</td>
                                                                    <td style="width: 129px;">
                                                                        <a name="titel_3" style="text-decoration:none;margin:0px;line-height:18px;font-family: Arial;font-size: 11px;color: #23408B;font-weight: bold;">
                                                                            {$template.title_3}
                                                                        </a>    
                                                                    </td>
                                                                    <td style="line-height: 10px;width: 5px;">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" style="line-height: 10px;width: 5px;">&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    {/if}
                                                </table>
                                            </td>
                                            <td valign="top" style="width: 20px;line-height: 10px;">&nbsp;</td>
                                            {/if}
                                            {if !isset($private.text_text4) || (isset($private.text_text4) && $private.text_text4 == 0)}
                                            <td valign="top" {if (!isset($private.image_photo) || (isset($private.image_photo) && $private.image_photo == 0)) || (!isset($private.title_3) || (isset($private.title_3) && $private.title_3 == 0))}style="width: 430px;"{else}style="width: 569px;"{/if} valign="top">       
                                                <p id="text_text4" style="margin:0px;line-height:21px;font-family: Verdana;font-size: 12px;color: #23408B;">
                                                    {$template.text_text4} 
                                                </p>
                                            </td>
                                            {/if}
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            {/if}
                            <tr>
                                <td valign="middle" height="48" style="width: 569px;vertical-align: middle;height: 48px;">
                                    <img src="{$siteurl}edm_images/bar.jpg" alt="" />
                                </td>
                            </tr>
                            {if !isset($private.text_text5) || (isset($private.text_text5) && $private.text_text5 == 0)}
                            <tr>
                                <td valign="top" style="background-color: #fef0de;width: 569px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="569">
                                        <tr>
                                            <td colspan="3" style="height: 16px;width: 569px;line-height: 16px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="width: 17px;line-height: 20px;">&nbsp;</td>
                                            <td valign="top" style="width: 535px;">
                                                <p id="text_text5" style="margin:0px 0px 0px 0px;line-height:20px;font-family: Verdana;font-size: 12px;color: #23408B;">
                                                    <a name="titel_4" style="text-decoration:none;margin:0px;font-family: Arial;font-size: 23px;color: #f7931e;line-height: 28px;">{$template.title_4}</a><br><br>
                                                    {$template.text_text5}
                                                </p>
                                            </td>
                                            <td valign="top" style="width: 17px;line-height: 20px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="height: 20px;width: 569px;line-height: 20px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            {/if}
                            {if !isset($private.title_5) || (isset($private.title_5) && $private.title_5 == 0)}
                            <tr>
                                <td valign="middle" height="12" style="width: 569px;vertical-align: middle;height: 12px;line-height: 12px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;vertical-align: middle;line-height: 30px;">
                                    <a name="titel_5" style="text-decoration:none;margin:0px;font-family: Arial;font-size: 23px;color: #23408b;">
                                        {$template.title_5}
                                    </a>
                                </td>
                            </tr>
                            {/if}
                            {if isset($template.items_1_title) && $template.items_1_title|count > 0}
                            <tr>
                                <td valign="middle" height="13" style="width: 569px;vertical-align: middle;height: 13px;line-height: 13px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="569" style="width: 569px;line-height:20px;">
                                        {if isset($template.items_1_title)}
                                        {foreach from=$template.items_1_title item=item key=k}
                                        <tr>
                                            <td valign="top" style="width: 569px;">
                                                <p id="items_1_text_{$k}" style="margin:0px 0px 0px 0px;line-height:20px;font-family: Verdana;font-size: 12px;color: #23408B;">
                                                    {if $item != ''}
                                                    <span style="color: #23408b;line-height: 28px;">{$item}</span><br>
                                                    {/if}
                                                    {$template.items_1_text.$k}
                                                </p>
                                            </td>
                                        </tr>
                                        {if $k+1 != $template.items_1_title|count} 
                                        <tr>
                                            <td colspan="3" valign="middle" style="width:569px;vertical-align: middle;height: 19px;line-height: 19px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" valign="middle" style="width:569px;vertical-align: middle;height: 1px;line-height: 19px;"><img src="{$siteurl}edm_images/bar3.jpg" alt="" style="display:block;" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" valign="middle" style="width:569px;vertical-align: middle;height: 19px;line-height: 19px;">&nbsp;</td>
                                        </tr>
                                        {else}
                                        <tr>
                                            <td colspan="3" valign="middle" style="width:569px;vertical-align: middle;height: 10px;line-height: 10px;">&nbsp;</td>
                                        </tr>
                                        {/if}
                                        {/foreach}
                                        {/if}
                                    </table>
                                </td>
                            </tr>
                            {/if}
                            {if (!isset($private.title_6) || (isset($private.title_6) && $private.title_6 == 0)) || (!isset($private.text_text6) || (isset($private.text_text6) && $private.text_text6 == 0))}
                            <tr>
                                <td valign="middle" height="48" style="width: 569px;vertical-align: middle;height: 48px;">
                                    <img src="{$siteurl}edm_images/bar.jpg" alt="" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;background-color: #dce2ed;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="569">
                                        <tr>
                                            <td colspan="3" style="height: 15px;line-height: 15px;width: 569px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="width: 17px;line-height: 20px;">&nbsp;</td>
                                            <td valign="top" style="width: 435px;">
                                                <p id="text_text6" style="margin:0px;line-height:21px;font-family: Verdana;font-size: 12px;color: #23408B;">
                                                    {if !isset($private.title_6) || (isset($private.title_6) && $private.title_6 == 0)}
                                                    <a name="titel_6" style="margin:0px;line-height:28px;font-family: Arial;font-size: 23px;color: #123a87;">{$template.title_6}</a><br><br>
                                                    {/if}
                                                    {if !isset($private.text_text6) || (isset($private.text_text6) && $private.text_text6 == 0)}
                                                    {$template.text_text6}
                                                    {/if}
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="height: 15px;line-height: 15px;width: 569px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            {/if}
                            {if !isset($private.title_7) || (isset($private.title_7) && $private.title_7 == 0)}
                            <tr>
                                <td valign="middle" height="11" style="width: 569px;vertical-align: middle;height: 11px;line-height: 11px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;line-height:35px;vertical-align: middle;">
                                    <a name="titel_7" style="text-decoration:none;margin:0px;font-family: Arial;font-size: 23px;color: #f7931e;">
                                        {$template.title_7}
                                    </a>
                                </td>
                            </tr>
                            {/if}
                            {if (!isset($private.image_1) || (isset($private.image_1) && $private.image_1 == 0)) || (!isset($private.text_text7) || (isset($private.text_text7) && $private.text_text7 == 0))}
                            <tr>
                                <td valign="middle" height="12" style="width: 569px;vertical-align: middle;height: 12px;line-height: 12px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0" border="0" width="569">
                                        <tr>
                                            {if !isset($private.image_1) || (isset($private.image_1) && $private.image_1 == 0)}
                                            <td valign="top" style="width: 150px;">
                                                <table cellpadding="0" cellspacing="0" border="0" width="151">
                                                    {if isset($template.image_1)}
                                                    {foreach from=$template.image_1 item=item key=k}
                                                    <tr>
                                                        <td valign="top" style="width: 150px;height: 190px;vertical-align:top;">
                                                            <img src="{$item}" alt="" style="display: block;width: 150px;height: 190px;" />
                                                        </td>
                                                    </tr>
                                                    {if $k+1 != $template.image_1|count}
                                                    <tr>
                                                        <td valign="top" style="width: 150px;line-height: 12px;height: 12px;">&nbsp;</td>
                                                    </tr>
                                                    {/if}
                                                    {/foreach}
                                                    {/if}
                                                </table>
                                            </td>
                                            <td style="width: 13px;line-height: 10px;">&nbsp;</td>
                                            {/if}
                                            {if !isset($private.text_text7) || (isset($private.text_text7) && $private.text_text7 == 0)}
                                            <td valign="top" {if !isset($private.image_1) || (isset($private.image_1) && $private.image_1 == 0)}style="width: 406px;"{/if}>
                                                <p id="text_text7" style="margin:0px;line-height:22px;font-family: Verdana;font-size: 12px;color: #23408B;width: 370px;">
                                                    {$template.text_text7}
                                                </p>
                                            </td>
                                            {/if}
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            {/if}
                            <tr>
                                <td valign="middle" height="48" style="width: 569px;vertical-align: middle;height: 56px;">
                                    <img src="{$siteurl}edm_images/bar.jpg" alt="" />
                                </td>
                            </tr>
                            {if !isset($private.title_8) || (isset($private.title_8) && $private.title_8 == 0)}
                            <tr>
                                <td valign="middle" height="5" style="width: 569px;vertical-align: middle;height: 5px;line-height: 5px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="middle" style="width: 569px;vertical-align: middle;line-height: 30px;">
                                    <a name="titel_8" style="text-decoration:none;margin:0px;font-family: Arial;font-size: 23px;color: #123a87;">
                                        {$template.title_8}
                                    </a>
                                </td>
                            </tr>
                            {/if}
                            {if isset($template.items_2_title) && $template.items_2_title|count > 0}
                            <tr>
                                <td valign="middle" height="20" style="width: 569px;vertical-align: middle;height: 20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="569">
                                        {if isset($template.items_2_title)}
                                        {foreach from=$template.items_2_title item=item key=k}
                                        <tr>
                                            <td valign="top" style="width: 170px;">
                                                <p id="items_2_text_{$k}" style="text-align:right;margin:0px;font-weight:bold;line-height:21px;font-family: Verdana;font-size: 12px;color: #123a87;">
                                                    {$item}
                                                </p>
                                            </td>
                                            <td valign="top" style="width: 40px;border-right:1px solid #123a87;line-height: 22px;">&nbsp;</td>
                                            <td valign="top" style="width: 32px;line-height: 22px;">&nbsp;</td>
                                            <td valign="top">
                                                <p style="margin:0px;color: #123a87;line-height: 22px;font-family: Verdana;font-size: 12px;">
                                                    {$template.items_2_text.$k}
                                                </p>
                                            </td>
                                        </tr>
                                        {if $k+1 != $template.items_2_title|count} 
                                        <tr>
                                            <td valign="top" style="width: 170px;"></td>
                                            <td valign="top" style="height:21px;width: 40px;border-right:1px solid #123a87;line-height: 21px;">&nbsp;</td>
                                            <td valign="top" style="height:21px;width: 32px;line-height: 21px;">&nbsp;</td>
                                            <td valign="top"></td>
                                        </tr>
                                        {/if}
                                        {/foreach}
                                        {/if}
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" height="48" style="width: 569px;vertical-align: middle;height: 48px;">
                                    <img src="{$siteurl}edm_images/bar.jpg" alt="" />
                                </td>
                            </tr>
                            {/if}
                            <tr>
                                <td valign="middle" height="8" style="width: 569px;vertical-align: middle;height: 8px;line-height: 8px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="569">
                                        <tr>
                                            {if !isset($private.image_2) || (isset($private.image_2) && $private.image_2 == 0)}
                                            <td valign="top" width="285" style="width: 285px;">
                                                <img src="{if isset($template.image_2)}{$template.image_2}{/if}" alt="image_2" />    
                                            </td>
                                            <td valign="top" style="width: 17px;line-height: 20px;">&nbsp;</td>
                                            {/if}
                                            <td valign="top" {if !isset($private.image_2) || (isset($private.image_2) && $private.image_2 == 0)}style="width: 267px;"{/if}>
                                                <p id="text_text8" style="margin:0px;color: #123a87;line-height:21px;font-family: Arial;font-size: 12px;">
                                                    {if !isset($private.title_9) || (isset($private.title_9) && $private.title_9 == 0)}
                                                    <a name="titel_9" style="text-decoration:none;font-size: 23px;font-family: Arial;line-height:53px;">{$template.title_9}</a><br>
                                                    {/if}
                                                    {if !isset($private.text_text8) || (isset($private.text_text8) && $private.text_text8 == 0)}
                                                    {$template.text_text8}<br><br>
                                                    {/if}
                                                </p>  
                                                <table cellpadding="0" cellspacing="0" border="0" width="77">
                                                    <tr>
                                                        <td valign="top" style="width: 39px;height: 39px;"><a href="https://twitter.com/#!/Bouwpluim" target="_blank"><img src="{$siteurl}edm_images/t.jpg" alt="" /></a></td>
                                                        <td style="width: 9px;height: 10px;line-height: 10px;">&nbsp;</td>
                                                        <td valign="top" style="width: 39px;height: 39px;"><a href="http://www.linkedin.com/groups/Bouwpluim-4310387" target="_blank"><img src="{$siteurl}edm_images/in.jpg" alt="" /></a></td>
                                                    </tr>
                                                </table> 
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px;line-height: 20px;width: 569px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 569px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="569">
                                        <tr>
                                            {if !isset($private.image_f1) || (isset($private.image_f1) && $private.image_f1 == 0)}
                                            <td valign="top" style="width: 281px;">
                                                {if isset($template.title_10) && $template.title_10 != ''}<a href="{$template.title_10}">{/if}<img src="{if isset($template.image_f1)}{$template.image_f1}{/if}" alt="footer 1" style="display: block;" />{if isset($template.title_10) && $template.title_10 != ''}</a>{/if}    
                                            </td>
                                            <td valign="top" style="width: 11px;">&nbsp;</td>
                                            {/if}
                                            {if !isset($private.image_f2) || (isset($private.image_f2) && $private.image_f2 == 0)}
                                            <td valign="top" style="width: 277px;">
                                                {if isset($template.title_11) && $template.title_11 != ''}<a href="{$template.title_11}">{/if}<img src="{if isset($template.image_f2)}{$template.image_f2}{/if}" alt="footer 2" style="display: block;" />{if isset($template.title_11) && $template.title_11 != ''}</a>{/if}    
                                            </td>
                                            {/if}
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="height: 18px;width: 569px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" style="width: 15px;line-height: 10px;">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" valign="top" style="height: 15px;width: 569px;background-color: #f1f1f1;">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" style="width: 15px;line-height: 10px;background-color: #f1f1f1;">&nbsp;</td>
                    <td valign="top" style="height: 18px;width: 569px;background-color: #f1f1f1;text-align: center;">
                        <p id="text_text9" style="margin:0px;line-height:22px;font-family: Verdana;font-size: 11px;color: #123a87;">
                            {$template.text_text9}<br><br>
                        </p>
                    </td>
                    <td valign="top" style="width: 15px;line-height: 10px;background-color: #f1f1f1;">&nbsp;</td>
                </tr>
            </table>
        </div>  
    </body>
</html>
