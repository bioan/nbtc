<?php
######### COPYRIGHT #################
 
/*
	ALL RIGHTS RESERVED
	homepage: http://inforitus.nl
	file: removeImage
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once 'includes/default.inc.php';

foreach (glob ('../files/' . $_POST ['module'] . '/' . $_POST ['id'] . '*') as $image) 
{
	  unlink ($image);
}

Database :: getInstance () -> query (sprintf ("
	DELETE 
	FROM 
		images 
	WHERE 
		id = %d", 
	$_POST ['id']
));

?>