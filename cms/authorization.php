<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: authorization
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once 'includes/default.inc.php';
           
$aFolderData = array (
        'url'  => 'folder_home.gif',
        'text' => 'Home',
        'page' => 'authorization'
);

$oTemplate -> assign ('javascriptBody', array (
        'jquery.min.js', 
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js'));
        
if ($_COOKIE ['home_slide'] == 1)
{
        $aFolderData ['home_slide'] = 1;
        setcookie ('home_slide', 0, time () + 60 * 60 * 1 * 1, '/', $_SERVER ['SERVER_NAME']);           
}
else
{
        $aFolderData ['home_slide'] = 0;
}

$oTemplate -> assign ('page', 'authorization');
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('user_name', $_COOKIE ['user_name']);
$oTemplate -> assign ('contentInclude', 'index.tpl');
$oTemplate -> display ('default.tpl');
?>
