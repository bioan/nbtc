<?php
######### COPYRIGHT #################
 
/*
	ALL RIGHTS RESERVED
	homepage: http://inforitus.nl
	file: setting
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once 'includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Setting.class.php');

$oSetting = new Setting ();
$oCKeditor  = new CKEditor () ;

$oCKeditor -> basePath          = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/';
$oCKeditor -> returnOutput      = true;
$oCKeditor -> config ['width']  = 590; 
$oCKeditor -> config ['height'] = 300; 

$aConfig = array ();
$aConfig ['toolbar'] = array (
        array ( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),
        array ( 'Image', 'Link', 'Unlink', 'Anchor', 'MediaEmbed' )
);

$oCKeditor -> config ['filebrowserBrowseUrl']      = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserImageBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserFlashBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserUploadUrl']      = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=File';
$oCKeditor -> config ['filebrowserImageUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
$oCKeditor -> config ['filebrowserFlashUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';


$aFolderData = array (
        'url' => 'configuratie.gif',
        'text' => 'Configuratie',
        'page' => 'setting'
);

if (!empty ($_POST)) 
{
        if (isset ($_FILES ['logo']['tmp_name']) && $_FILES ['logo']['size'] > 0)
                copy ($_FILES ['logo']['tmp_name'], 'images/logo.jpg');
        
        if ($oSetting -> updateSettings ($_POST))
                redirect ($SiteRoot . '/setting.php?type=logo&action=new'); 
}

$aSettings = $oSetting -> getSettings ();

$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'updateForm.js'
                                )
                            );
                            

$aContent = $oCKeditor -> editor ("functie_description", (isset ($_POST ['value']) ? $_POST ['value'] : $aSettings ['functie_description']), $aConfig);   

$oTemplate -> assignByRef ('FCKeditor', $aContent);

$oTemplate -> assign ('settings', $oSetting -> getSettings ());
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'setting.tpl');
$oTemplate -> display ('default.tpl');
?>
