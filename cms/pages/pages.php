<?php
######### COPYRIGHT #################
 
/*
	ALL RIGHTS RESERVED
	@author Andrei Dragos <andrei@inforitus.nl>
	file: pages
*/

######### COPYRIGHT #################
    
require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php'); 
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Tree.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Page.class.php');
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/ValidateFields.class.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Setting.class.php');

$aFolderData = array (
        'url'  => 'paginas.gif',
        'text' => 'Pagina beheer',
        'page' => 'pagina' 
);

$oSetting  = new Setting ();
$oMenu     = new Page ();
$oLanguage = new Language ();
$oCKeditor = new CKEditor ();
$oValidate = new ValidateFields ();

$oCKeditor -> basePath          = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/';
$oCKeditor -> returnOutput      = true;
$oCKeditor -> config ['width']  = 590; 
$oCKeditor -> config ['height'] = 300;
$oCKeditor -> config ['extraAllowedContent'] = 'img[alt,border,width,height,align,vspace,hspace,!src];';
$oCKeditor -> config ['allowedContent'] = true; 

$aConfig = array ();
$aConfig ['toolbar'] = array (
        array ( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),
        array ( 'Image', 'Link', 'Unlink', 'Anchor', 'MediaEmbed' )
);

$oCKeditor -> config ['filebrowserBrowseUrl']      = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserImageBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserFlashBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserUploadUrl']      = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=File';
$oCKeditor -> config ['filebrowserImageUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
$oCKeditor -> config ['filebrowserFlashUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';

if (isset ($_GET ['action']) && !empty ($_GET ['action'])) 
{
	    $oTemplate -> assign ('existingNodes', $oMenu -> getAllNodes ());
        
        $aDefaultLanguage = $oLanguage -> getLanguages(0, '', 'id', 'asc', true);
                        
        $oTemplate -> assign ('default_lang', $aDefaultLanguage);
        
        $aLanguages = $oLanguage -> getLanguages ();
        
        $oTemplate -> assign ('languages', $aLanguages);
        
	    if ($_GET ['action'] == 'new') 
        {
		        $oTemplate -> assign ('action','new');
	    } 
        elseif ($_GET ['action'] == 'overview') 
        {
		        $oTemplate -> assign ('action','overview');
                
                $aNodes = $oMenu -> getAllNodes ();
                
                $aParents = $oMenu -> getAllNodes (1, 0, 'order_number', 'asc', true);
                
                for ($i = 0; $i < count ($aParents); $i++)
                {       
                        $aParents [$i]['childs'] = $oMenu -> getChilds ($aParents [$i]['oid'], '', 2, array (), "default", true); 
                        
                        for ($j = 0; $j < count ($aParents [$i]['childs']); $j++)
                        {
                                $aParents [$i]['childs'][$j]['childs'] = $oMenu -> getChilds ($aParents [$i]['childs'][$j]['oid'], '', 3, array (), "default", true);   
                        
                                for ($k = 0; $k < count ($aParents [$i]['childs'][$j]['childs']); $k++)
                                {
                                        $aParents [$i]['childs'][$j]['childs'][$k]['childs'] = $oMenu -> getChilds ($aParents [$i]['childs'][$j]['childs'][$k]['oid'], '', 4, array (), "default", true);   
                                } 
                        }   
                }
                //echo '<pre>'.print_r($aParents,true);
                $oTemplate -> assign ('parents', $aParents);
                
                if (isset ($_GET ['existCat']))
                {
                        $iId = $_GET ['existCat'];
                }
                
                if (isset ($_GET ['paroid']))
                {
                        $iId = $_GET ['paroid'];
                }
                
                if (isset ($iId) && empty ($_POST))
                {
                        $aNode = $oMenu  -> getNodeData ($iId);
                        
                        if ($aNode ['level'] == 2)
                        {
                                $oTemplate -> assign ('level', 1);     
                        }
                }
                
                $aContent   = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("content[" . $aLanguages [$i]['id'] . "]", (isset ($_POST ['content'][$aLanguages [$i]['id']]) ? $_POST ['content'][$aLanguages [$i]['id']] : ''), $aConfig);
                }
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent);
                
                $oTemplate -> assign ('existsCats', $aNodes);   
                
                $aDefaultLanguage = $oLanguage -> getLanguages(0, '', 'id', 'asc', true);
                $oTemplate -> assign ('default_lang', $aDefaultLanguage);
                
                $aRequiredFields = array (
                        'name'   => array ($aDefaultLanguage ['id'] => ''),
                        'slug'   => array ($aDefaultLanguage ['id'] => '')
                );
                
                $aFieldsNames = array (
                        'name'   => array ($aDefaultLanguage ['id'] => 'Page titel'),
                        'slug'   => array ($aDefaultLanguage ['id'] => 'Page naam')
                );
                
                if (isset ($_POST ['keywords']))
                {
                        $aKeywords = keywords ($_POST ['content']);
                        
                        $oTemplate -> assign ('keywords', $aKeywords);
                }
                else
                {
                        if (!empty ($_POST)) 
                        {
                                if (isset ($_GET ['paroid']))
                                {
                                        $iId = $_GET ['paroid']; 
                                }
                                elseif (isset ($_GET ['existCat']))
                                {
                                        $iId = $_GET ['existCat'];
                                }
                                
                                $aNode = $oMenu -> GettreeRaw ($iId);
                                
                                if ($aNode ['level'] > 1)
                                {
                                        $aParents = $oMenu -> getParents ($iId);
                                        $aChilds  = $oMenu -> getChilds ($aParents [0]['oid'], '', $aParents [0]['level'] + 1, array (), "default", true);
                                }
                                else
                                {      
                                        $aChilds = $oMenu -> getChilds ($aNode ['oid'], '', $aNode ['level'] + 1, array (), "default", true);   
                                }
                                
                                $bExist = false;
                                
                                foreach ($aChilds as $item)
                                {
                                        foreach ($oMenu -> aLanguages as $lang)
                                        {       
                                                if ($item [$lang ['id']]['slug'] == $oMenu -> urlize ($_POST ['slug'][$lang ['id']]) && $_POST ['slug'][$lang ['id']] != '')
                                                {       
                                                        $bExist = true;
                                                        break;   
                                                }          
                                        }
                                        
                                }
                                
        			            if (!$oValidate -> checkFields ($aRequiredFields) && !isset ($_POST ['dell']) || $bExist == true) 
                                { 
                                        foreach ($oValidate -> getLastError () as $k => $v)
                                        {
                                                $oValidate -> aErrors [$k][$aDefaultLanguage ['id']] = $aFieldsNames [$k][$aDefaultLanguage ['id']];
                                        }
                                        
                                        if ($bExist == true)
                                        {
                                                $oValidate -> aErrors ['slug'][$aDefaultLanguage ['id']] = 'Deze naam voor de pagina bestaat al, kies een andere \'Pagina naam\'.';       
                                        }
                                        
                                        $oTemplate -> assign ('error', $oValidate -> getLastError ());
        			            } 
                                else 
                                {    
        				                $aData = $_POST;
                                        
        				                if (isset ($aData ['add_to'])) 
                                        {
        					                    $sAction = 'add';
        				                } 
                                        elseif (isset ($aData ['add_under'])) 
                                        {
        					                    $sAction = 'after';
        				                } 
                                        elseif (isset ($aData ['add_up'])) 
                                        {	
        					                    $sAction = 'before';
        				                } 
                                        
                                        if (isset ($_GET ['paroid']))
                                        {
                                                $sAction = 'before';
                                        }
                                        
                                        if (isset ($_GET ['add']))
                                        {        
                                                $sAction = 'after';
                                        }
                                        
                                        if (isset ($_GET ['after']))
                                        {        
                                                $sAction = 'after';
                                        }                      
                                        
                                        if (isset ($_GET ['nochilds']))
                                        {
                                                $sAction = 'add';
                                        }
                                         
                                        if (!isset ($_GET ['existCat']) && !isset ($_GET ['paroid']))
                                        {
                                                redirect ($SiteRoot . '/pages/pages.php?action=overview');             
                                        }    
                                         
        				                if ($oMenu -> add ($aData, $iId, $sAction)) 
                                        {      
        					                    redirect ($SiteRoot . '/pages/pages.php?action=overview');
        				                } 
                                        else 
                                        {        
        					                    $oTemplate -> assign ('error_warning', true);
        				                }
                                }
        		        }
                }
	    } 
        elseif ($_GET ['action'] == 'change') 
        {
                if (isset ($_POST ['dell'])) 
                {   
                        if (isset ($_GET ['oid']))
                        {         
                                $oMenu -> deleteNode ($_GET ['oid'], 1);
                        }
                        
                        redirect ($SiteRoot . '/pages/pages.php?action=overview');
                            
                        exit;
                }
                else
                {
                        
                        
                        $aNodeData = $oMenu -> getNodeData ($_GET ['oid']);
                        
                        $aParents = $oMenu -> getAllNodes (1, 0, 'order_number', 'asc', true);

                        for ($i = 0; $i < count ($aParents); $i++)
                        {       
                                $aParents [$i]['childs'] = $oMenu -> getChilds ($aParents [$i]['oid'], '', 2, array (), "default", true); 
                                
                                for ($j = 0; $j < count ($aParents [$i]['childs']); $j++)
                                {
                                        $aParents [$i]['childs'][$j]['childs'] = $oMenu -> getChilds ($aParents [$i]['childs'][$j]['oid'], '', 3, array (), "default", true);   
                                        
                                        for ($k = 0; $k < count ($aParents [$i]['childs'][$j]['childs']); $k++)
                                        {
                                                $aParents [$i]['childs'][$j]['childs'][$k]['childs'] = $oMenu -> getChilds ($aParents [$i]['childs'][$j]['childs'][$k]['oid'], '', 4, array (), "default", true);   
                                        } 
                                }   
                        }
                        
                        $oTemplate -> assign ('parents', $aParents);
                        
                        $aContent = array ();
                        
                        for ($i = 0; $i < count ($aLanguages); $i++)
                        {
                                if (isset ($aNodeData [$aLanguages [$i]['id']]['slug']))
                                        $aNodeData [$aLanguages [$i]['id']]['slug']  = str_replace ('-', ' ', $aNodeData [$aLanguages [$i]['id']]['slug']);
                                
                                $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("content[" . $aLanguages [$i]['id'] . "]", (isset ($aNodeData [$aLanguages [$i]['id']]['content']) ? html_entity_decode ($aNodeData [$aLanguages [$i]['id']]['content']) : ''), $aConfig);                 
                        }
		                
                        $oTemplate -> assignByRef ('FCKeditor', $aContent);
                        $oTemplate -> assign ('languages', $aLanguages);
                        $oTemplate -> assign ('nodeData', $aNodeData);
                        $oTemplate -> assign ('existsCats', $oMenu -> getAllNodes ()); 
                        $oTemplate -> assign ('action', 'change');
                        
                        $oValidate = new ValidateFields ();
                
                        $aRequiredFields = array (
                        'name'   => array ($aDefaultLanguage ['id'] => ''),
                        'slug'   => array ($aDefaultLanguage ['id'] => '')
                        );
                        
                        $aFieldsNames = array (
                                'name'   => array ($aDefaultLanguage ['id'] => 'Page titel'),
                                'slug'   => array ($aDefaultLanguage ['id'] => 'Page naam')
                        );
                        
                        if (isset ($_POST ['keywords']))
                        {
                                $aKeywords = keywords ($_POST ['content']);
                                
                                $oTemplate -> assign ('keywords', $aKeywords);
                        }
                        else
                        { 
                                if (!empty ($_POST)) 
                                {
                                        $oTemplate -> assign ('action', 'change');
                                        $aNode = $oMenu -> GettreeRaw ($_GET ['oid']);
                                        $aParents = $oMenu -> getParents ($_GET ['oid']);
                                        $aChilds = $oMenu -> getChilds ($aParents [0]['oid'], '', $aParents [0]['level'] + 1, array (), "default", true);
                                        $bExist = false;
                                        
                                        foreach ($aChilds as $item)
                                        {
                                                foreach ($oMenu -> aLanguages as $lang)
                                                {
                                                        if (isset ($item [$lang ['id']]['slug']) && $item [$lang ['id']]['slug'] == $oMenu -> urlize ($_POST ['slug'][$lang ['id']]) && $item ['oid'] != $_GET ['oid'])
                                                        {
                                                                $bExist = true;
                                                                break;   
                                                        }
                                                }
                                        }
                                        
                                        if (!$oValidate -> checkFields ($aRequiredFields) && !isset ($_POST ['dell']) || $bExist == true) 
                                        { 
                                                foreach ($oValidate -> getLastError () as $k => $v)
                                                {
                                                        $oValidate -> aErrors [$k][$aDefaultLanguage ['id']] = $aFieldsNames [$k][$aDefaultLanguage ['id']];
                                                }
                                                
                                                if ($bExist == true)
                                                {
                                                        $oValidate -> aErrors ['slug'][$aDefaultLanguage ['id']] = 'Deze naam voor de pagina bestaat al, kies een andere \'Pagina naam\'';       
                                                }
                                                
                				                $oTemplate -> assign ('error', $oValidate -> getLastError ());
                			            }  
                                        else 
                                        {
                                                $aData = $_POST;
                                                
                                                if ($oMenu -> updateNodeData ($_GET ['oid'], $aData)) 
                                                {
                                                        redirect (sprintf ('%s/pages/pages.php?action=change&oid=%d&existCat=%d', $SiteRoot, $_GET ['oid'], $_GET ['oid']));
                                                }
                                        }
     		                    }
                        }
	            }
        }
        elseif ($_GET ['action'] == 'private') 
        {
                $aNodeData = $oMenu -> getNodeData ($_GET ['oid']);
                
                $aNodeData ['private'] = $_GET ['private'];
                
                $aData = $aNodeData;
                
                if ($oMenu -> updateNodeData ($_GET ['oid'], $aData)) 
                {
                        redirect ($SiteRoot . '/pages/pages.php?action=overview');
                }
        }
} 
else 
{
        redirect ($SiteRoot . '/pages/pages.php?action=overview' . (isset ($_GET ['after']) ? '&after=' . $_GET ['after'] : '') . (isset ($_GET ['catoid']) ? '&existCat='.$_GET ['catoid'] : '') . (isset ($_GET ['paroid']) ? '&paroid='.$_GET ['paroid'] : '') . (isset ($_GET ['add']) ? '&add='.$_GET ['add'] : '') . (isset ($_GET ['nochilds']) ? '&nochilds='.$_GET ['nochilds'] : ''));
}

$oTemplate -> assign ('settings', $oSetting -> getSettings ());
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('javascriptBody', 
    array (
        'jquery.min.js', 
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js',
        'jquery-ui-1.7.1.custom.min.js',
        'pages.js.php?type_oid=2')
        );
        
$oTemplate -> assign ('contentInclude', 'pages/pages.tpl');
$oTemplate -> display ('default.tpl');
?>
