<?php
######### COPYRIGHT #################
 
/*
	ALL RIGHTS RESERVED
	homepage: http://inforitus.nl
	file: linkbrowser
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################

$GLOBALS ['cfg']['auth'] = false;

require_once 'includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Tree.class.php');

$oTree =& new Tree (2);

$oTemplate -> assign ('menuItems', $oTree -> getAllNodes ());
$oTemplate -> display ('linkbrowser.tpl');
?>
