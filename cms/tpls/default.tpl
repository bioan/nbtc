{*
	Default template for cms of inforitus
	
	@copyright inforitus.nl
	
	You can contact me by mail: info@inforitus.nl

*}
{strip}

	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl">
	<head>
	      <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	      <meta name="generator" content="PSPad editor, www.pspad.com" />
	      <style type="text/css" media="all">@import url("{$SiteRoot}/css/default.css");</style>
	      {if isset($cssBody)}
	            {foreach from=$cssBody item=css}
					<style type="text/css" media="all">@import url("{$SiteRoot}/{$css}");</style>
				{/foreach}
			{/if}
	      <title>::. Inforitus CMS .::</title>
			{if $javascriptBody}
				{foreach from=$javascriptBody item=javascript}
                    {if $javascript != ''}
					<script type="text/javascript" src="{$SiteRoot}js/{$javascript}"></script>
                    {/if}
				{/foreach}
			{/if}
			<script type="text/javascript" src="{$SiteRoot}/js/default.js"></script>
	</head>
	<body><div id="info"></div>
			<script type="text/javascript">/* <[CDATA[ */{literal}
	        sfHover = function() {
	        	var sfEls = document.getElementById("nav").getElementsByTagName("LI");
	        	for (var i=0; i<sfEls.length; i++) {
	        		sfEls[i].onmouseover=function() {
	        			this.className+=" sfhover";
	        		}
	        		sfEls[i].onmouseout=function() {
	        			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
	        		}
	        	}
	        }
	        if (window.attachEvent) window.attachEvent("onload", sfHover);{/literal}
	            /* ]]> */
	        </script>
    {if $folder.page == "authorization" && isset($folder.home_slide) && $folder.home_slide == "1"}
    <div id="slide">
    {/if}
	    <div id="header"><img src="{$SiteRoot}/images/dfl.jpg" alt="dfl" /><img src="{$SiteRoot}/images/inforitus.jpg" alt="inforitus" /></div>
	    <div id="menu_div">
            <div id="menu_div_item">

            {include file="nav.inc.tpl"}    
            <td id="right-position">
                <table id="menu-right">
                    <tr>
                        {if 'users'|in_array:$securePages}
                        <td>
                            <a href="{$SiteRoot}/users.php?action=overview" id="gebruikersbeheer" class="font {if $folder.page == 'user'}user-selected{/if}">Gebruikersbeheer</a>
                        </td>
                        {/if}
                        <td>                                                                                                      
                            <a href="{$SiteRoot}/logout.php" id="logout"><img src="{$SiteRoot}/images/logout.jpg" alt="logout" /></a>
                        </td>
                    </tr>
                </table>     
            </td>
            </tr>
            </table>          
            </div>
        </div>    
    {if $folder.page == "authorization" && isset($folder.home_slide) && $folder.home_slide == "1"}
    </div>
    {/if}
        {if  $folder.page == "forum" || $folder.page == "template" || $folder.page == "content" || $folder.page == "newsletter" || $folder.page == "news" || $folder.page == "social_media" || $folder.page == "users" || $folder.page == "webshop" || $folder.page == "foto" || $folder.page == "agenda" || $folder.page == "videoalbum" || $folder.page == "language" || $folder.page == "setting"}
        {include file="submenu.tpl"}    
        {/if}
    <div id="container_div">
              
	    <div id="pathInfo" style="z-index: 3;">
			<div id="pathInfoItem" >
				{if $path}
					<span style="color: #FF6606; font-weight: bold;">{$SiteRootShort}</span>/{$path}
				{else}
					<span style="color: #FF6606; font-weight: bold;">{$SiteRootShort}</span>
				{/if}
			</div>
			{if $help}
			<div style="padding-bottom: 0.2em; margin-left: 0.5em; float: left; width: 7em;">
				<table>
					<tr>
						<td><a href="#"><img onclick="popup('{$help.url}', 200, 500);" src="{$SiteRoot}/images/help.gif" alt="{$help.alt}" /></a></td>
						<td style="vertical-align: middle; font-style: italic">Help</td>
					</tr>
				</table>
			</div>
			{/if}
        
		</div>
		{if ('#pagina_beheer#i'|preg_match:$contentInclude && $smarty.get.action != 'new') || (isset($folder.page) && $folder.page == "template" && $smarty.get.action == 'edit')}
	    	{include file=$contentInclude}   
            <br class="clear" />        
	    {else}
		    <div id="content_div">
		        <!--<table>
		            <tr>
		                <td>
		                <img src="{$SiteRoot}/images/{$folder.url}" alt="" />
		                </td>
		                <td style="vertical-align: middle">
		                    <span id="login_kop">{$folder.tekst}</span>
		                </td>
		                {if $extraOption}
		                	<td style="padding-left: 4em;">
		                	{$extraOption} 
		                	</td>
		                {/if}
		            </tr>
		        </table> -->
		    	<div id="content_in_content_div">
                {if isset($smarty.get.action) && ($smarty.get.action == "new" || $smarty.get.action == "newWord" || $smarty.get.action == "edit" || $smarty.get.action == "texts" || $smarty.get.action == "ga") && ($folder.page == "forum" || $folder.page == "newsletter" || $folder.page == "ga" || $folder.page == "news" || $folder.page == "social_media" || $folder.page == "users" || $folder.page == "webshop" || $folder.page == "foto" || $folder.page == "agenda" || $folder.page == "videoalbum" || $folder.page == "language" || $folder.page == "setting")}
                    <div class="pagina-form modules">
                        <div id="pagina-title" class="pagina-titel">{$folder.text}</div>   
                        <div id="content_in_content_div">
                {/if}   
		        	    {include file=$contentInclude}
                {if isset($smarty.get.action) && ($smarty.get.action == "new" || $smarty.get.action == "newWord"  || $smarty.get.action == "edit" || $smarty.get.action == "texts" || $smarty.get.action == "ga") && ($folder.page == "forum" || $folder.page == "newsletter" || $folder.page == "ga" || $folder.page == "news" || $folder.page == "social_media" || $folder.page == "users" || $folder.page == "webshop" || $folder.page == "foto" || $folder.page == "agenda" || $folder.page == "videoalbum" || $folder.page == "language" || $folder.page == "setting")} 
                        </div>
                        <img src="{$SiteRoot}/images/pagina_bottom.jpg" alt="" class="pagina_bottom" />
                    </div>
                {/if}  
		    	</div>
		    </div>
		{/if}
        <br class="clear" />
	</div>
	<div style="right: 2px; top: 6.5em; position: absolute; z-index: +99;">
		
	</div>
  {if !isset($page)}    
	<script type="text/javascript">/* <[CDATA[ */
	{literal}
	    setWidthOfMenus();
	{/literal}
	/* ]]> */ </script>         {/if}
	</body>
	</html>

{/strip}
      

