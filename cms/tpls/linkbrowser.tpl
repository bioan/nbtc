{strip}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl">
<head>
      <meta http-equiv="content-type" content="text/html; charset=windows-1250" />
      <meta name="generator" content="PSPad editor, www.pspad.com" />
      <style type="text/css" media="all">@import url("{$SiteRoot}/css/default.css");</style>
      <title>::. CMS | Linkbrowser.:: </title>
</head>
<body>
	<script type="text/javascript">
	 /* <![CDATA[ */{literal}
	   	function add()
	   	{
	   		try {
				var $target = window.opener.document.getElementById('txtUrl');
				var $select = document.getElementById('pages');
				var $selectedPage = $select.options[$select.selectedIndex].value.split('|');
				if ($selectedPage[1] == '_root') {
					alert('Het is niet mogelijk deze \'pagina\' toe te voegen!');
					return false;
				}
				$target.value = '{/literal}{$smarty.server.SERVER_NAME}{literal}/' + $selectedPage[0] + '/' + $selectedPage[1] + '.html';
				if (confirm('Dit venster wordt gesloten')) {
					window.close();
				}
				return false;
			} catch (e) {
				alert('Er moet wel een pagina geselecteerd zijn');
				return false;
			}
	   	}{/literal}
	 /* ]]> */
	 </script>
	<div style="height: 15%; border-bottom: 1px dashed #D5D5D5">
		<img src="{$SiteRoot}/images/folder_photoalbum.gif" alt="" name="photoalbum" />
		<div style="position: absolute; top: 1px; left: 55px; font-size: 140%; font-weight: bold;">Pagina's koppelen</div>
	</div>
	<div style="padding-top: 2em; padding-left: 0.4em;">
		<form>
			<select id="pages" name="existCat" size="18" style="width: 15em;">
				{foreach from=$menuItems item=node name=couter}
					<option value="{$node.oid}|{$node.name|urlencode}" >{"&nbsp;"|str_repeat:$node.level*2}{$node.name|stripslashes|regex_replace:'#_root#':$smarty.server.SERVER_NAME|regex_replace:'#_#':''}</option>
				{/foreach}
			</select><br/ >
			<input type="submit" name="submit" onclick="return add();" value="Toevoegen" />
		</form>
	</div>
</body>
</html>
{/strip}
