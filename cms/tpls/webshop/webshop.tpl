{strip}
{if $smarty.get.action == 'new'}
    <form method="post" action="" enctype="multipart/form-data"  onsubmit="if (checkForm(this)) showDiv('uploadMelding'); else return false;">
		<table style="width: 100%;">
		    <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
            
            <tr>
				<td class="fieldName">
				<label for="price">Prijs</label>: 
				</td>
				<td>
				<input type="text" class="input" name="price" id="price" value="{if isset($smarty.post.price)}{$smarty.post.price}{/if}" size="15" />
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				<label for="category">Categorie</label>: 
				</td>
				<td>
				<select name="category" onchange="checkIfDisabled(this);">
                    {if isset($nodes)}
                        {foreach from=$nodes item=node}
                            {if $node.name != '_root'}
                            {if $node.oid|in_array:$leafNodes && $node.oid != 0}
                                <option value="{$node.oid}" {if isset($smarty.post.category) && $smarty.post.category == $node.oid} selected="selected" {/if}>{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                            {else}
                                <option value="{$node.oid}" disabled="disabled" style="color: #CCCCCC;">{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                            {/if}
                            {/if}
                        {/foreach}
                    {else}
                        <option value="0">Er zijn geen categorie&euml;n aangemaakt.</option>
                    {/if}
                </select>
				</td>
			</tr>
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
            
            <tr>
                <td class="fieldName">
                    <label>Photos</label>: 
                </td>
                <td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_{$id}"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_{$id}" ></span><br class="clear" />
                    <span id="photos_{$id}">
                    {if isset($smarty.post.photos.$id)}
                        {foreach from=$smarty.post.photos.$id item=photoOid}
                            <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                            <input name="photos[{$id}][{$photoOid}]" value="{$photoOid}" type="hidden" />
                        {/foreach}
                    {/if}
                    </span>
                </td>
            </tr>
			<tr>
				<td class="fieldName">
				<label for="title_{$id}">Product naam {$item.language}</label>: 
				</td>
				<td>
				<input type="text" class="input" name="title[{$id}]" id="title_{$id}" value="{if isset($smarty.post.title.$id)}{$smarty.post.title.$id}{/if}" size="40" />
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				<label>Omschrijving {$item.language}</label>: 
				</td>
				<td>
				    {$FCKeditor.$id}
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($smarty.post.page_description.$id)}{$smarty.post.page_description.$id}{/if}</textarea>
                </td>
            </tr>
            {/foreach}
			<tr>
                <td></td>
				<td>
                     <input type="hidden" id="module"  value="webshop" name="module"  />
                     <br />
    			     <input type="image" src="{$SiteRoot}/images/save.png" class="save">
                     <input type="submit" class="keywords" value="" name="keywords"  />
				</td>
			</tr>
		</table>
	</form>
	<div id="uploadMelding">
	   Een moment geduld astublieft...
	</div>
{elseif $smarty.get.action == 'overview'}
	<script type="text/javascript">
 	/* <![CDATA[ */ {literal}
  	function showHide(obj)
  	{
  		var item = obj.parentNode.nextSibling.nextSibling;
		if (item.style.display == 'none') {
			item.style.display = 'block';
		} else {
			item.style.display = 'none';
		}
	}{/literal}
 	/* ]]> */
 	</script>
    Rijen weergeven :&nbsp;
    <select onchange="window.location='webshop.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
	<table style="width: 100%;" cellpadding="2" cellspacing="0" class="webshop_products" id="t1">
		<colgroup style="width: 25%"></colgroup>
		<colgroup style="width: 30%"></colgroup>
		<colgroup style="width: 25%"></colgroup>
        <colgroup style="width: 20%"></colgroup>
		<tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="4" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
			<tr nodrag="true" nodrop="true">
				<td class="first-row-first-column-white">
                    <a href="{$SiteRoot}webshop/webshop.php?action=overview&items={$items}&sort=title&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Naam</a>
                </td>
				<td class="first-row-column-white">
                    <a href="{$SiteRoot}webshop/webshop.php?action=overview&items={$items}&sort=category_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Categorie</a>
                </td>
				<td class="first-row-column-white">
                    <a href="{$SiteRoot}webshop/webshop.php?action=overview&items={$items}&sort=date_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum</a>
                </td>
                <td class="first-row-last-column">
                    <a href="{$SiteRoot}webshop/webshop.php?action=overview&items={$items}&sort=filled&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Stats</a>
                </td>
			</tr>
		{if !$products}
			<tr nodrag="true" nodrop="true">
                <td class="first-column-gray">
                    Er zijn nog geen producten toegevoegd   
                </td>
                <td class="column-gray" colspan="2">
                </td>
                <td class="last-column-gray">
                </td>
            </tr>
		{/if}
		{foreach from=$products item=product name="iCounter"}
			
                <tr id="{$product.id}" order_number="{$product.order_number}" ondblclick="window.location.href = '{$SiteRoot}webshop/webshop.php?action=edit&id={$product.id}'" class="pointer">
            	{if $smarty.foreach.iCounter.iteration % 2 == 1} 
                <td class="first-column-gray">
				    {$product.title|stripslashes}
				</td>
				<td class="column-gray">
				    {$product.category_name|stripslashes}
				<td class="column-gray">
				    {$product.date_format}
				</td>
                <td class="last-column-gray">
				    <span class="{if $product.filled == $languages|@count}green{else}red{/if}">{$product.filled} / {$languages|@count}</span>
				</td>
				<!--<td style="vertical-align: top;">
                    <a href="{$SiteRoot}/webshop/webshop.php?action=view&amp;id={$product.id}" ><img src="{$SiteRoot}/images/view.png" alt="Product bekijken" />
                </td>
				<td style="vertical-align: top;">
                    <a href="{$SiteRoot}/webshop/webshop.php?action=wijzigen&amp;id={$product.id}" ><img src="{$SiteRoot}/images/change.png" alt="Nieuwsgroep wijzigen" />
                </td>
                <td style="vertical-align: top;">
                    <a href="{$SiteRoot}/webshop/webshop.php?action=verwijderen&amp;id={$product.id}" onclick="return confirm('Wilt u dit product verwijderen?');"><img src="{$SiteRoot}/images/delete.png" alt="Bericht verwijderen" />
                </td>-->
                {else}
                <td class="first-column-white">
                    {$product.title|stripslashes}
                </td>
                <td class="column-white">
                    {$product.category_name|stripslashes}
                <td class="column-white">
                    {$product.date_format}
                </td>
                <td class="last-column-white">
                    <span class="{if $product.filled == $languages|@count}green{else}red{/if}">{$product.filled} / {$languages|@count}</span>
                </td>
                {/if}
			</tr>	
		{/foreach}
        <tr nodrag="true" nodrop="true">
            <td class="last-row-first-column">
            </td>
            <td class="last-row-column-white" colspan="2">
            </td>
            <td class="last-row-last-column">
            </td>
        </tr>
	</table>
	<div style="padding-top: 0.6em; text-align: center;">
		{$links}
	</div>
{elseif $smarty.get.action == 'edit'}
     <form method="post" action="" enctype="multipart/form-data" onsubmit="if (checkForm(this)) showDiv('uploadMelding'); else return false;">
		<table style="width: 100%;">
		    <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
            <tr>
				<td class="fieldName">
				<label for="price">Prijs</label>: 
				</td>
				<td>
				<input type="text" class="input" name="price" id="price" value="{$product.price}" size="15" />
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				<label for="category">Categorie</label>: 
				</td>
				<td>
				<select name="category" onchange="checkIfDisabled(this);">
				    {if $nodes}
                        {foreach from=$nodes item=node}
                            {if $node.name != '_root'}
                            {if $node.oid|in_array:$leafNodes && $node.oid != 0}
                                <option value="{$node.oid}" {if $node.oid == $product.category_id} selected="selected" {/if}>{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                            {else}
                                <option value="{$node.oid}" disabled="disabled" style="color: #CCCCCC;">{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                            {/if}
                            {/if}
                        {/foreach}
                    {else}
                        <option value="0">Er zijn geen categorie&euml;n aangemaakt.</option>
                    {/if}
                </select>
				</td>
			</tr>
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
            
            <tr>
				<td class="fieldName">
				    <label for="f_id_1">Afbeelding</label>: 
				</td>
				<td id="photoplace">
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_{$id}"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_{$id}" ></span><br class="clear" />
                    <span id="photos_{$id}">
                    {if !empty($product.$id.photos)}
                    {foreach from=$product.$id.photos item=photo}
                    	<img id="{$photo.id}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto/{$photo.id}/100/image.php" />
						<input name="photos[{$id}][{$photo.id}]" value="{$photo.id}" type="hidden" />
					{/foreach}
                    {/if}
                    </span>
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				<label for="title_{$id}">Product naam {$item.language}</label>: 
				</td>
				<td>
				<input type="text" class="input" name="title[{$id}]" id="title_{$id}" value="{if isset($product.$id.title)}{$product.$id.title}{/if}" size="40" />
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label>Omschrijving {$item.language}</label>: 
				</td>
				<td>
				    {$FCKeditor.$id}
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($product.$id.page_description)}{$product.$id.page_description}{/if}</textarea>
                </td>
            </tr>
            {/foreach}
			<tr>
                <td></td>
                <td>
                    <input type="hidden" id="module"  value="webshop" name="module"  />
                    <br />
    				<input type="submit" value="" id="save">
                    <a href="{$SiteRoot}/webshop/webshop.php?action=delete&amp;id={$product.id}" onclick="return confirm('Wilt u dit product verwijderen?');" class="verwijderen"></a>
                    <input type="submit" class="keywords" value="" name="keywords"  />
                </td>
			</tr>
		</table>
	</form>
	<div id="uploadMelding">
	   Een moment geduld astublieft...
	</div>
{/if}
{/strip}
