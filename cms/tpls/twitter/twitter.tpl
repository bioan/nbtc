{strip}
{if $smarty.get.action == 'newWord'}
<form action="" method="post">
    <table cellspacing="0" cellpadding="0" style="100%">
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 80%"></colgroup>
        <tr>
            <td class="fieldName">
                Twitter word:
            </td>
            <td>
                <input type="text" class="input" name="twitterWord" value="{if isset($smarty.post.twitterWord)}{$smarty.post.twitterWord}{/if}" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName" colspan="2">
                <input type="submit" value="Add word" name="addWord" />
           </td>
        </tr>
    </table>
</form>
{else if $smarty.get.action == 'editWord'}
<form action="" method="post">
    <table cellspacing="0" cellpadding="0" style="100%">
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 80%"></colgroup>
        <tr>
            <td class="fieldName">
                Twitter word:
            </td>
            <td>
                <input type="text" class="input" name="twitterWord" value="{if isset($values.word)}{$values.word}{/if}" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName" colspan="2">
                <input type="submit" value="Edit word" name="editWord" />
           </td>
        </tr>
    </table>
</form>
{else if $smarty.get.action == 'overviewWords'}
    Rijen weergeven :&nbsp;
    <select onchange="window.location='twitter.php?action=overviewWords&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="photoalbum" cellpadding="2" cellspacing="0">
        <colgroup style="width: 70%"></colgroup>
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 15%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">{if isset($folder.text)}{$folder.text}{else}Overzicht{/if}</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=overviewWords&items={$items}&sort=title&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Word</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=overviewWords&items={$items}&sort=date&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum</a>
                </td>
                <td class="first-row-last-column">
                    
                </td>
            </tr>
            {if !$twitter}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                        Er zijn nog geen twitter items gepost.          
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {else}
                {if isset($twitter.0)}
                    {foreach from=$twitter item=tweet name="iCounter"}
                        <tr id="{$tweet.id}" order_number="{$tweet.order_number}"  ondblclick="window.location.href = '{$SiteRoot}social_media/social_media.php?media=twitter&action=editWord&id={$tweet.id}'"  class="pointer" >
                            {if $smarty.foreach.iCounter.iteration % 2 == 1}   
                            <td class="first-column-gray">
                                {$tweet.word}
                            </td>
                            <td class="column-gray">
                                {$tweet.date}
                            </td>
                            <td class="last-column-gray">
                            <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=deleteWord&id={$tweet.id}">Verwijderen</a>
                            </td>
                            {else}
                            <td class="first-column-white">
                                {$tweet.word}
                            </td>
                            <td class="column-white">
                                {$tweet.date}
                            </td>
                            <td class="last-column-white">
                                <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=deleteWord&id={$tweet.id}">Verwijderen</a>
                            </td>
                            {/if}
                        </tr>	
                    {/foreach}
                {else}
                    dasdasd
                    <tr id="{$twitter.id}" order_number="{$twitter.order_number}"  ondblclick="window.location.href = '{$SiteRoot}social_media/social_media.php?media=twitter&action=editWord&id={$tweet.id}'"  class="pointer" >
                        <td class="first-column-gray">
                            {$twitter.word}
                        </td>
                        <td class="column-gray">
                            {$twitter.date}
                        </td>
                        <td class="last-column-gray">
                        <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=deleteWord&id={$twitter.id}">Verwijderen</a>
                        </td>
                    </tr>
                {/if}
            {/if}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>     	
        </tbody>
    </table>
       <div style="padding-top: 0.6em; text-align: center;">
		{$links}
	</div>
{else if $smarty.get.action == 'new'}
<form action="" method="post">
    <table cellspacing="0" cellpadding="0" style="100%">
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 80%"></colgroup>
        {if isset($successMessage) && $appErrors|count == 0}
        <tr>
            <td class="successMessage" colspan="2">
               {$successMessage}
            </td>
        </tr>
        {/if}
        <tr>
            <td class="fieldTitle" colspan="2">
               1. Twitter Connection
            </td>
        </tr>
        
        <tr>
            <td class="fieldName">
                Twitter Consumer Key:
            </td>
            <td>
                <input type="text" class="input" name="consumer_key" value="{if isset($smarty.post.consumer_key)}{$smarty.post.consumer_key}{elseif isset($appValues.consumer_key)}{$appValues.consumer_key}{else}{/if}" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName">
                Twitter Consumer Secret:
            </td>
            <td>
                <input type="text" class="input" name="consumer_secret" value="{if isset($smarty.post.consumer_secret)}{$smarty.post.consumer_secret}{elseif isset($appValues.consumer_secret)}{$appValues.consumer_secret}{else}{/if}" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName" colspan="2">
                <input type="submit" value="Save Changes" name="saveChangesApp" />
                {if isset($errorCode)}
                    {$errorCode}
                {else}
                    {if $url != 'false'}<a href="{$url}" title="Grant Permissions">Grant permissions</a>
                    {else} You must fill in application data
                    {/if}
                {/if}
           </td>
        </tr>
    </table>
</form>
{elseif $smarty.get.action == 'overview'} 
    Rijen weergeven :&nbsp;
    <select onchange="window.location='twitter.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="photoalbum" cellpadding="2" cellspacing="0">
        <colgroup style="width: 70%"></colgroup>
        <colgroup style="width: 15%"></colgroup>
        <colgroup style="width: 15%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">{if isset($folder.text)}{$folder.text}{else}Overzicht{/if}</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=overview&items={$items}&sort=title&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Message</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=overview&items={$items}&sort=date&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum</a>
                </td>
                <td class="first-row-last-column">
                    
                </td>
            </tr>
            {if !$twitter}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                        Er zijn nog geen twitter items gepost.          
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$twitter item=tweet name="iCounter"}
    			<tr id="{$tweet.id}" order_number="{$tweet.order_number}" ondblclick="window.location.href = '{$SiteRoot}social_media/social_media.php?media=twitter&action=edit&id={$tweet.id}'" class="pointer" >
                    {if $smarty.foreach.iCounter.iteration % 2 == 1}   
    				<td class="first-column-gray">
    				    {$tweet.message}
    				</td>
    				<td class="column-gray">
    				    {$tweet.date_format}
    				</td>
    				<td class="last-column-gray">
                        <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=delete&id={$tweet.id}">Verwijderen</a>
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$tweet.message}
                    </td>
                    <td class="column-white">
                        {$tweet.date_format}
                    </td>
                    <td class="last-column-white">
                        <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=delete&id={$tweet.id}">Verwijderen</a>
                    </td>
                    {/if}
    			</tr>	
        	{/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>     	
        </tbody>
    </table>
       <div style="padding-top: 0.6em; text-align: center;">
		{$links}
	</div>
{else if $smarty.get.action == 'overviewSearch'} 
    Rijen weergeven :&nbsp;
    <select onchange="window.location='twitter.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="photoalbum" cellpadding="2" cellspacing="0">
        <colgroup style="width: 70%"></colgroup>
        <colgroup style="width: 15%"></colgroup>
        <colgroup style="width: 15%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=overview&items={$items}&sort=title&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Message</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=overview&items={$items}&sort=date_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum</a>
                </td>
                <td class="first-row-last-column">
                    
                </td>
            </tr>
            {if !$twitter}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                        Er zijn nog geen twitter items gepost.          
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$twitter item=tweet name="iCounter"}
    			<tr id="{$tweet.id}" order_number="{$tweet.order_number}" ondblclick="window.location.href = '{$SiteRoot}social_media/social_media.php?media=twitter&action=edit&id={$tweet.id}'" class="pointer" >
                    {if $smarty.foreach.iCounter.iteration % 2 == 1}   
    				<td class="first-column-gray">
    				    {$tweet.message}
    				</td>
    				<td class="column-gray">
    				    {$tweet.date_format}
    				</td>
    				<td class="last-column-gray">
                        <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=deleteSearch&id={$tweet.id}">Verwijderen</a>
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$tweet.message}
                    </td>
                    <td class="column-white">
                        {$tweet.date_format}
                    </td>
                    <td class="last-column-white">
                        <a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=deleteSearch&id={$tweet.id}">Verwijderen</a>
                    </td>
                    {/if}
    			</tr>	
        	{/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>     	
        </tbody>
    </table>
       <div style="padding-top: 0.6em; text-align: center;">
		{$links}
	</div>
{/if}
{/strip}
