{strip}
<form action="" method="post">
    <input type="text" class="input" name="date_start" id="date_start" value="{if isset($smarty.post.date_start)}{$smarty.post.date_start}{/if}" size="10" />
    <span id="sDateCalendar"><img src="{$SiteRoot}/images/calendar.gif" alt="" /></span>
    <input type="text" class="input" name="date_end" id="date_end" value="{if isset($smarty.post.date_end)}{$smarty.post.date_end}{/if}" size="10" />
    <span id="sDateEndCalendar"><img src="{$SiteRoot}/images/calendar.gif" alt="" /></span>
    <select name="interval">
        <option value="1 day" {if isset($smarty.post.interval) && $smarty.post.interval == '1 day'} selected="selected" {/if}>1 day</option>
        <option value="7 days" {if isset($smarty.post.interval) && $smarty.post.interval == '7 days'} selected="selected" {/if}>1 week</option>
        <option value="1 month" {if isset($smarty.post.interval) && $smarty.post.interval == '1 month'} selected="selected" {/if}>1 month</option>
        <option value="6 months" {if isset($smarty.post.interval) && $smarty.post.interval == '6 months'} selected="selected" {/if}>6 months</option>
        <option value="1 year" {if isset($smarty.post.interval) && $smarty.post.interval == '1 year'} selected="selected" {/if}>1 year</option>
    </select>
    <input type="submit" name="submit" value="submit" />
</form>
<table>
    <tr>
      <td>Total Pageviews</td>
      <td>{$pageviews}</td>
    </tr>
    <tr>
      <td>Total Visits</td>
      <td>{$visits}</td>
    </tr>
</table>
{if $date_start != ''}
<div id="visits" class="jqplot" style="height:350px; width:650px;"></div>
<script type="text/javascript">{literal}
    
    $(document).ready(function(){
      var cosPoints = {/literal}{$visitschart}{literal}; 
      $.jqplot.config.enablePlugins = true;
      var plot1 = $.jqplot('visits', [cosPoints], {  
          gridPadding:{right:35},
  
            	  axes: {
            	    xaxis: {
                      min:{/literal}'{$date_start}'{literal},
                      tickInterval:{/literal}'{$smarty.post.interval}'{literal},
                      tickOptions:{formatString:'%m-%e'},
                      renderer:$.jqplot.DateAxisRenderer, 
                    }
            	  },
                  highlighter: {sizeAdjust: 7.5},
                  cursor: {show: false},
                  seriesDefaults: {
                  color: '#255674'
                  },
                  grid: {
                  background: '#dfecf8' 
                  }
      });
      
   });
{/literal}
</script>
<div id="pageviews" class="jqplot" style="height:350px; width:650px;"></div>
<script type="text/javascript">{literal}
    
    $(document).ready(function(){
      var cosPoints = {/literal}{$pageviewschart}{literal}; 
      $.jqplot.config.enablePlugins = true;
      var plot1 = $.jqplot('pageviews', [cosPoints], {  
          gridPadding:{right:35},
  
            	  axes: {
            	    xaxis: {
                      min:{/literal}'{$date_start}'{literal},
                      tickInterval:{/literal}'{$smarty.post.interval}'{literal},
                      tickOptions:{formatString:'%m-%e'},
                      renderer:$.jqplot.DateAxisRenderer, 
                    }
            	  },
                  highlighter: {sizeAdjust: 7.5},
                  cursor: {show: false},
                  seriesDefaults: {
                  color: '#255674'
                  },
                  grid: {
                  background: '#dfecf8' 
                  }
      });
      
   });
{/literal}
</script>

<table>
{foreach from=$pages item=value key=link}
    <tr>
        <td>
            {$link}
        </td>
        <td>
            {$value.entrances}
        </td>
    </tr>
{/foreach}
</table>
{/if}
<script type="text/javascript">{literal}
   Calendar.setup(
       {
            inputField : "date_start",
            ifFormat :  "%Y-%m-%d",
            button : "sDateCalendar"
       }
    );
    Calendar.setup(
       {
            inputField : "date_end",
            ifFormat :  "%Y-%m-%d",
            button : "sDateEndCalendar"
       }
   
   );{/literal}
</script>

<script class="code" type="text/javascript">

</script>
{/strip}
