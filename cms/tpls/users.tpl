{strip}
<script type="text/javascript">
/* <![CDATA[ */{literal}
function setCheckBoxen($value)
{
    var $obj = document.getElementsByTagName('INPUT');
    for (var $i=0; $i < $obj.length; $i++) {
        if($obj[$i].type == 'checkbox') {
            if ($value == 1) {
                $obj[$i].checked = true;
            } else {
                $obj[$i].checked = false;
            }
        } 
    }
}
  
function getSelectedValue($thisForm)
{
    
    $obj = document.forms[0].sStatus;
    if ($obj.options[$obj.selectedIndex].value == 1) {
        setCheckBoxen(1);
    } else {
        setCheckBoxen(2);
    }
}
{/literal}
/* ]]> */
</script>
{if isset($new_user)}
	<p class="info">Hieronder heeft u de mogelijkheid een extra gebruiker aan te maken.</p><br /><br />
    <table style="width: 100%;" cellspacing="0">  	
	<form action="" method="post">  
	   <colgroup style="width: 3%;"></colgroup>
	   <colgroup style="width: 10%;"></colgroup>
	   <colgroup style="width: 87%;"></colgroup>
	   <tbody>
			{if isset($error)}
			<tr>
				<td colspan="3">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
            <tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam">
    		        <label for="user_name">GebruikersNaam</label>: 
    		    </td>
    			<td>
    			    <input type="text" name="user_name" id="user_name" value="{if isset($smary.post.user_name)}{$smary.post.user_name}{/if}" size="40" /> 
    			</td>
    		</tr>
			<tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam">
    		        <label for="password">Wachtwoord</label>: 
    		    </td>
    			<td>
    			    <input type="password" name="password" id="password" value="" size="40" /> 
    			</td>
    		</tr>
    		<tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam">
    		        <label for="email">Emailadres</label>: 
    		    </td>
    			<td>
    			    <input type="text" name="email" id="email" value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}" size="40" />            
    			</td>
    		</tr>
            <tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam">
    		        <label for="status">Status</label>: 
    		    </td>
    			<td>
    			    <select name="status" id="status" onchange="getSelectedValue(); return false">
					     <option value="2">Klant</option>
    			         <option value="1">Admin</option>
    		        </select>
    			</td>
    		</tr>  	
    		<tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam" style="vertical-align: top;">
    		        Permissies: 
    		    </td>
    			<td>
    			    {foreach from=$menuPages item=page}
    			         <input type="checkbox" name="permissions[]" value={$page.id} id="f_000{$page.id}" /><label for="f_000{$page.id}">{$page.show_name|stripslashes}</label><br />
                    {/foreach}
    			</td>
    		</tr>  	
			<tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam">
    		        Actief:
    		    </td>
    			<td>
    			    <input type="radio" name="active" value="0">Nee</option>
    			    <input type="radio" name="active" value="1">Ja</option>
    		      </select>
    			</td>
    		</tr>   
    		<tr>
				<td>&nbsp;</td><td>&nbsp;</td>
				<td>
				<a href="#" onclick="document.forms[0].submit();"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="Toevoegen" /></a>
				</td>
			</tr>	
    	</tbody>
	</table>  
	</form>	
{elseif isset($succes)}
	<img src="{$SiteRoot}/images/button_ok.gif" alt="Ok" />&nbsp;{$succes}<br /><br />
	U wordt binnen 2 seconden doorgestuurd naar het overview.
	<meta http-equiv=refresh content="2; url={$SiteRoot}/users.php?action=overview">
{elseif isset($edit_user)}
	<table style="width: 100%;" cellspacing="0">  	
	<form action="" method="post">  
	   <colgroup style="width: 3%;"></colgroup>
	   <colgroup style="width: 10%;"></colgroup>
	   <colgroup style="width: 87%;"></colgroup>
	   <tbody>
			{if isset($error)}
			<tr>
				<td colspan="3">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
            <tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam">
    		        <label for="user_name">GebruikersNaam</label>: 
    		    </td>
    			<td>
    			    <input type="text" name="user_name" id="user_name" value="{$userData.user_name}" size="40" /> 
    			</td>
    		</tr>
			<tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam">
    		        <label for="password" title="Laat het wachtwoord leeg indien deze niet gewijzigd dient te worden!" >Wachtwoord</label>: 
    		    </td>
    			<td>
    			    <input type="password" name="password" id="password" value="" size="40" title="Laat het wachtwoord leeg indien deze niet gewijzigd dient te worden!"  /> 
    			    <input type="hidden" name="password_hidden" value="{$userData.password}" />
    			</td>
    		</tr>
    		<tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam">
    		        <label for="email">Emailadres</label>: 
    		    </td>
    			<td>
    			    <input type="text" name="email" id="email" value="{$userData.email}" size="40" />            
    			</td>
    		</tr>
            <tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam">
    		        <label for="status">Status</label>: 
    		    </td>
    			<td>
    			    <select name="status" id="status" onchange="getSelectedValue(); return false">
					     <option value="2" {if $userData.status == 2} selected {/if} >Klant</option>
    			         <option value="1" {if $userData.status == 1} selected {/if} >Admin</option>
    		        </select>
    			</td>
    		</tr>
            <tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam" style="vertical-align: top;">
    		        Permissies: 
    		    </td>
    			<td>
    			    {foreach from=$menuPages item=page}
    			         <input type="checkbox" name="permissions[]" value={$page.id} id="f_000{$page.id}" {foreach from=$permissions item=rechten}{if $rechten.menu_id == $page.id}checked=checked{/if} {/foreach} /><label for="f_000{$page.id}">{if $page.show_name|stripslashes == "Fotoalbum"}Countries{else}{$page.show_name|stripslashes}{/if}</label><br />
                    {/foreach}
    			</td>
    		</tr>  	  	
			<tr>
    		    <td>
    		   	    &nbsp;
    		    </td>
    		    <td class="formoptieNaam">
    		        Actief:
    		    </td>
    			<td>
    			    <input type="radio" name="active" value="0" {if $userData.active == 0} checked {/if}>Nee</option>
    			    <input type="radio" name="active" value="1" {if $userData.active == 1} checked {/if}>Ja</option>
    		      </select>
    			</td>
    		</tr>   
    		<tr class="inputHeight">
				<td>&nbsp;</td><td>&nbsp;</td>
				<td>
				<a href="#" onclick="document.forms[0].submit();" id="save"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="Toevoegen" /></a>
                <a class="verwijderen" href="{$SiteRoot}/users.php?action=delete&amp;id={$userData.id}" onclick="return confirm('Wilt u deze afbeelding verwijderen?!');"></a>
				</td>
			</tr>	
    	</tbody>
	</table>  
	</form>	
{else}
	<table style="width: 100%;" id="t1" class="sortable" cellpadding="2" cellspacing="0">
        <colgroup style="width: 18%"></colgroup>
        <colgroup style="width: 23%"></colgroup>
		<colgroup style="width: 24%"></colgroup>
        <colgroup style="width: 35%"></colgroup>
        <tbody>
            <tr>
                <td colspan="4" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr>
                <td class="first-row-first-column-white">
                <a href="{$SiteRoot}users.php?action=overview&sort=active&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Actief</a>
                </td>
                <td class="first-row-column-white">
                <a href="{$SiteRoot}users.php?action=overview&sort=user_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Gebruikersnaam</a>
                </td>
                <td class="first-row-column-white">
                <a href="{$SiteRoot}users.php?action=overview&sort=status&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Status</a>
                </td>
				<td class="first-row-last-column">
                <a href="{$SiteRoot}users.php?action=overview&sort=date_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum aangemaakt</a>
                </td>
            </tr>	

			{if !isset($users)}
            <tr>
                    <td class="first-column-gray">
                    Er zijn nog geen gebruikers toegevoegd
                    </td>
                    <td class="column-gray" colspan="2">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
			{else}
                {foreach from=$users item=user name=iCounter}
					
                        <tr ondblclick="window.location.href = '{$SiteRoot}users.php?action=edit&id={$user.id}'" class="pointer">
                        {if $smarty.foreach.iCounter.iteration % 2 == 1}   
                    	<td class="first-column-gray">
						{if $user.active == 1}
							<img src="{$SiteRoot}/images/green.png" style="cursor: pointer;margin:4px 0px 0px 4px;" onclick="parent.location.href = '{$SiteRoot}/users.php?action=actief&amp;id={$user.id}&amp;status=non';" alt="Actief" />
						{else}
							<img src="{$SiteRoot}/images/red.png" style="cursor: pointer;margin:4px 0px 0px 4px;" onclick="parent.location.href = '{$SiteRoot}/users.php?action=actief&amp;id={$user.id}&amp;status=actief';" alt="Non-Actief" />
						{/if}
						</td>
						<td class="column-gray">
							{$user.user_name}
						</td>
						<td class="column-gray">
							{if $user.status == 1}
								Admin
							{else}
								klant
							{/if}
						</td>
						<td class="last-column-gray">
							{$user.date_format}
						</td>
                        {else}
                        <td class="first-column-white">
                        {if $user.active == 1}
                            <img src="{$SiteRoot}/images/green.png" style="cursor: pointer;margin:4px 0px 0px 4px;" onclick="parent.location.href = '{$SiteRoot}/users.php?action=actief&amp;id={$user.id}&amp;status=non';" alt="Actief" />
                        {else}
                            <img src="{$SiteRoot}/images/red.png" style="cursor: pointer;margin:4px 0px 0px 4px;" onclick="parent.location.href = '{$SiteRoot}/users.php?action=actief&amp;id={$user.id}&amp;status=actief';" alt="Non-Actief" />
                        {/if}
                        </td>
                        <td class="column-white">
                            {$user.user_name}
                        </td>
                        <td class="column-white">
                            {if $user.status == 1}
                                Admin
                            {else}
                                klant
                            {/if}
                        </td>
                        <td class="last-column-white">
                            {$user.date_format}
                        </td>
                        {/if}
					</tr>
				{/foreach}
			{/if}
            <tr>
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
		{if isset($links)}{$links}{/if}
	</div>
	<br />
{/if}
{/strip}
