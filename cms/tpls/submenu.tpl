{if $folder.page == 'news'}
<ul class="submenu">
    <li class="first"><a href="{$SiteRoot}news/news.php?action=new">toevoegen</a></li>
    <li><a href="{$SiteRoot}news/news.php?action=overview">overzicht</a></li>
    <li><a href="{$SiteRoot}news/groups.php?action=new">groepen toevoegen</a></li>
    <li><a href="{$SiteRoot}news/groups.php?action=overview">groepen overzicht</a></li>
    <li><a href="{$SiteRoot}news/comment.php?action=overview">blog</a></li>
    <li><a href="{$SiteRoot}news/users.php?action=overview">users</a></li>
</ul>
{/if} 
{if $folder.page == 'forum'}
<ul class="submenu">
    <li class="first"><a href="{$SiteRoot}forum/comment.php?action=overview">overzicht</a></li>
    <li><a href="{$SiteRoot}forum/users.php">users</a></li>
    <li><a href="{$SiteRoot}forum/settings.php?action=new">settings</a></li>
</ul>
{/if} 
{if $folder.page == 'newsletter' || $folder.page == 'template' || $folder.page == 'content'}
<ul class="submenu">
    <li class="first"><a href="{$SiteRoot}newsletter/templates.php?action=new">template toevoegen</a></li>
    <li><a href="{$SiteRoot}newsletter/templates.php?action=overview">template overzicht</a></li>
    <li><a href="{$SiteRoot}newsletter/items.php?action=new">content toevoegen</a></li>
    <li><a href="{$SiteRoot}newsletter/items.php?action=overview">content overzicht</a></li>
    <li><a href="{$SiteRoot}newsletter/subscribers.php?action=new">subscriber toevoegen</a></li>
    <li><a href="{$SiteRoot}newsletter/subscribers.php?action=overview">subscribers overzicht</a></li>
    <li><a href="{$SiteRoot}newsletter/subscribers_groups.php?action=new">subscriber group toevoegen</a></li>
    <li><a href="{$SiteRoot}newsletter/subscribers_groups.php?action=overview">subscriber groups overzicht</a></li>
    <li><a href="{$SiteRoot}newsletter/monitor.php?action=overview">monitor</a></li>
</ul>
{/if}     
{if $folder.page == 'users'}
<ul class="submenu">
    <li class="first"><a href="{$SiteRoot}users.php?action=new">toevoegen</a></li>
    <li><a href="{$SiteRoot}users.php?action=overview">overzicht</a></li>
</ul>
{/if}
{if $folder.page == 'webshop'}
<ul class="submenu">
    <li class="first"><a href="{$SiteRoot}webshop/webshop.php?action=new">toevoegen</a></li>
    <li><a href="{$SiteRoot}webshop/webshop.php?action=overview">overzicht</a></li>
    <li><a href="{$SiteRoot}webshop/cats.php?action=new">categorie&euml;n toevoegen</a></li>
    <li><a href="{$SiteRoot}webshop/cats.php?action=overview">categorie&euml;n overzicht</a></li>
</ul>
{/if}

{if $folder.page == 'social_media'}
<ul class="submenu">
    <li class="first"><a href="{$SiteRoot}social_media/social_media.php?media=facebook&action=new">Facebook</a></li>
    <li><a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=new">Twitter</a></li>
    <li><a href="{$SiteRoot}social_media/social_media.php?media=linkedin&action=new">Linkedin</a></li>
    <li><a href="{$SiteRoot}social_media/social_media.php?media=google&action=new">Google</a></li>
</ul>
<ul class="submenu">    
    {if isset($smarty.get.media)}
       {if $smarty.get.media == 'facebook'}
           <li class="first"><a href="{$SiteRoot}social_media/social_media.php?media=facebook&action=new">Connect to Facebook</a></li>
       {elseif $smarty.get.media == 'twitter'}
           <li class="first"><a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=new">Connect to Twitter</a></li>
           <li><a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=newWord">Add twitter word</a></li>
           <li><a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=overviewWords">Overzicht twitter words</a></li>
           <li><a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=overviewSearch">Overzicht twitter search results</a></li>
           <li><a href="{$SiteRoot}social_media/social_media.php?media=twitter&action=overview">overzicht Tweets</a></li>
       {elseif $smarty.get.media == 'linkedin'}
           <li class="first"><a href="{$SiteRoot}social_media/social_media.php?media=linkedin&action=new">Connect to Linkedin</a></li>
       {/if}
    {else}
        <li class="first"><a href="{$SiteRoot}social_media/social_media.php?media=facebook&action=new">Connect to Facebook</a></li>
    {/if}
</ul>
{/if} 
{if $folder.page == 'foto'}
<ul class="submenu">
    <li class="first"><a href="{$SiteRoot}photoalbum/cats.php?action=new">countries toevoegen</a></li>
    <li><a href="{$SiteRoot}photoalbum/cats.php?action=overview">countries overzicht</a></li>
</ul>
{/if}
{if $folder.page == 'videoalbum'}
<ul class="submenu">
    <li class="first"><a href="{$SiteRoot}videoalbum/videoalbum.php?action=new">toevoegen</a></li>
    <li><a href="{$SiteRoot}videoalbum/videoalbum.php?action=overview">overzicht</a></li>
    <li><a href="{$SiteRoot}videoalbum/cats.php?action=new">categorie&euml;n toevoegen</a></li>
    <li><a href="{$SiteRoot}videoalbum/cats.php?action=overview">categorie&euml;n overzicht</a></li>
</ul>
{/if}
{if $folder.page == 'agenda'}
<ul class="submenu">
    <li class="first"><a href="{$SiteRoot}agenda/agenda.php?action=new">toevoegen</a></li>
    <li><a href="{$SiteRoot}agenda/agenda.php?action=overview">overzicht</a></li>
    <li><a href="{$SiteRoot}agenda/groups.php?action=new">groepen toevoegen</a></li>
    <li><a href="{$SiteRoot}agenda/groups.php?action=overview">groepen overzicht</a></li>
</ul>
{/if}
{if $folder.page == 'language'}
<ul class="submenu">
    <li class="first"><a href="{$SiteRoot}language/language.php?action=new">toevoegen</a></li>
    <li><a href="{$SiteRoot}language/language.php?action=overview">overzicht</a></li>
    <li><a href="{$SiteRoot}language/language.php?action=stats">stats</a></li>
</ul>
{/if}