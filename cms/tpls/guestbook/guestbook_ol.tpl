{if $smarty.get.action == 'wijzigen'}
<form method="post" action="" >
            <table style="width: 100%;">
            <colgroup style="width: 10%"></colgroup>
            <colgroup style="width: 90%"></colgroup>
            <tbody>
            {if $error}
            <tr>
                <td colspan="3">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            <tr>
                <td class="formoptieNaam">
                <label for="f_id_1">Naam</label>: 
                </td>
                <td>
                <input type="text" name="naam" id="f_id_1" value="{$guestbookItems.naam|stripslashes}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="formoptieNaam">
                <label for="sDatumVan">Datum</label>: 
                </td>
                <td>
                <input type="text" name="date" id="date" value="{$guestbookItems.date}" size="10" />
                    <span id="dateButton"><img src="{$SiteRoot}/images/calendar.gif" alt="" /></span>
                </td>
            </tr>
            <tr>
                <td class="formoptieNaam">
                <label for="f_id_2">Bericht</label>: 
                </td>
                <td>
                {$FCKeditor}
                </td>
            </tr>
            <tr><td></td>
                <td>
                <input class="button" type="submit" name="submit" value="Wijzigen" />
                </td>
            </tr>
        </table>
    </form>
    <script type="text/javascript">{literal}
       Calendar.setup(
           {
                inputField : "date",
                ifFormat :  "%d-%m-%Y",
                button : "dateButton"
            }
        )
       {/literal}
    </script>
{elseif $smarty.get.action == 'overzicht'}    
<table style="width: 100%;" id="t1" class="sortable" cellpadding="2" cellspacing="0">
        <colgroup style="width: 40%"></colgroup>
        <colgroup style="width: 51%"></colgroup>
        <colgroup style="width: 3%"></colgroup>
        <colgroup style="width: 3%"></colgroup>
        <tbody>
            <tr style="background-color: #ABBEDE; height: 25px; vertical-align: middle; font-weight: bold;">
                <td>
                    Titel
                </td>
                <td>
                    Datum
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </th>
            </tr>
            
            {if !$guestbookItems}
                <tr style="background-color: #FFFFFF; height: 25px; width: 100%">
                    <td colspan="4">
                    Er zijn nog geen guestbook items gepost.
                    </td>
                </tr>
            {/if}
            {foreach from=$guestbookItems item=item name="iCounter"}
                {if $smarty.foreach.iCounter.iteration % 2 == 0}
                    <tr style="background-color: #F5F5F5; height: 25px;" class="showItemsBorder">
                {else}
                    <tr style="background-color: #FFFFFF; height: 25px" class="showItemsBorder">
                {/if}
                    <td>
                        {$item.naam|stripslashes}
                    </td>
                    <td>
                        {$item.date}
                    </td>
                    <td>
                        <a href="{$SiteRoot}/guestbook/guestbook_ol.php?action=wijzigen&amp;id={$item.id}" ><img src="{$SiteRoot}/images/change.png" alt="Bericht wijzigen" />  
                    </td>
                    <td>
                        <a href="{$SiteRoot}/guestbook/guestbook_ol.php?action=verwijderen&amp;id={$item.id}" onclick="return confirm('Weet u zeker dat u dit nieuws item wilt verwijderen?'); return false;"><img src="{$SiteRoot}/images/delete.png" alt="Bericht verwijderen" />
                    </td>
                </tr>
            {/foreach}
            </tbody>
    </table>
    {/if}