{strip}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl">
<head>
      <meta http-equiv="content-type" content="text/html; charset=windows-1250" />
      <meta name="generator" content="PSPad editor, www.pspad.com" />
      <style type="text/css" media="all">@import url("{$SiteRoot}/css/default.css");</style>
      {if isset($cssBody)}
            {foreach from=$cssBody item=css}
				<style type="text/css" media="all">@import url("{$SiteRoot}/{$css}");</style>
			{/foreach}
		{/if}
      <title>::. CMS .:: </title>
		{if isset($javascriptBody)}
			{foreach from=$javascriptBody item=javascript}
				<script type="text/javascript" src="{$SiteRoot}/js/{$javascript}"></script>
			{/foreach}
		{/if}
		<script src="{$SiteRoot}/js/lib/prototype.js" type="text/javascript"></script>	
 	<script src="{$SiteRoot}/js/lib/scriptaculous.js?load=builder,dragdrop" type="text/javascript"></script>
	<script src="{$SiteRoot}/js/cropper.js" type="text/javascript"></script>	
	{if isset($smarty.get.type)}
		{assign var=type value=$smarty.get.type}
	{else}
		{assign var=type value=1}
	{/if}
	<script type="text/javascript" charset="utf-8">{literal}
		// setup the callback function
		function onEndCrop( coords, dimensions ) {
			$('x1').value = coords.x1;
			$('y1').value = coords.y1;
			$('x2').value = coords.x2;
			$('y2').value = coords.y2;
			$('width').value = dimensions.width;
			$('height').value = dimensions.height;
		}

		function loadDifferentTypes( type ) {
			switch( type ) {
				case( '1' ) :
					new Cropper.Img( 'LWImage', { ratioDim: { x: 220, y: 165 }, displayOnInit: true, onEndCrop: onEndCrop } );
					break;
				case( '2' ) :
					new Cropper.Img( 'LWImage', { ratioDim: { x: 220, y: 124}, displayOnInit: true, onEndCrop: onEndCrop } );
					break;
                case( '4' ) :
                    new Cropper.Img( 'LWImage', { ratioDim: { x: 100, y: 100}, displayOnInit: true, onEndCrop: onEndCrop } );
                    break;
				case( '3' ):
				default :
					new Cropper.Img( 'LWImage', { onEndCrop: onEndCrop } );
			}
		}
		Event.observe( window, 'load', function() { loadDifferentTypes('{/literal}{$type}{literal}'); } );
		{/literal}		
	</script>
	<link rel="stylesheet" type="text/css" href="{$SiteRoot}/css/cropper.css" media="all" />
	<style type="text/css">
 	/* <![CDATA[ */ {literal}
 	body {
		background-color: #F5F5F5; 	
		padding: 0px;
		margin: 0px;
	}
	
	div {
		position: relative;
	}
	
	.top {
		padding: 0px;
		margin: 0px;
		background-color: #FFCC00;
		width: 100%;
		font-weight: bold;
		height: 3em;
	}
	{/literal}
	/* ]]> */
 	</style>
</head>
<body>
	{if isset($succesvol)}
		<script type="text/javascript">
	  /* <![CDATA[ */
	    window.close();
	  /* ]]> */
	  </script>
	{else}
		<div class="top" style="width: {$smarty.session.image.width}px;">
			<div style="float: left; ">
			Kies het formaat hoe u uw foto wilt bijsnijden
			</div>
			<div style="float: left;">
				<form action="" method="get">
					{if isset($smarty.get.action)}
						<input type="hidden" name="action" value="change" />
						<input type="hidden" name="oid" value="{$smarty.get.oid}" />
					{/if}
					<select name="type" onchange="this.form.submit();">
						<option value="2" {if $type == 2} selected="selected" {/if} >Ratio 16:9</option>
						<option value="1" {if $type == 1} selected="selected" {/if} >Ratio 4:3</option>
                        <option value="4" {if $type == 4} selected="selected" {/if} >100:100</option>  
						<option value="3" {if $type == 3} selected="selected" {/if} >Basis</option>
					</select>
				</form>
			</div>
		</div>
		<br style="clear: left;" />
		<div style="width:{$smarty.session.image.width}px; height:{$smarty.session.image.height}px;">
			<div id="testWrap">
				<img src="{$sWebsiteUrl}/{$smarty.session.image.location}/{$smarty.session.image.name}" id="LWImage" width="{$smarty.session.image.width}" height="{$smarty.session.image.height}" />
			</div>
		</div>
		<div style="clear: float;">
			<form method="post" onsubmit="window.opener.reloadRemote();" action="{$SiteRoot}/browser/image_save.php{if isset($smarty.get.action)}?action={$smarty.get.action}{/if}">
				<input type="hidden" name="x1" id="x1" />
				<input type="hidden" name="y1" id="y1" />
				<input type="hidden" name="x2" id="x2" />
				<input type="hidden" name="y2" id="y2" />
				<input type="hidden" name="width" id="width" />
				<input type="hidden" name="height" id="height" />
				<input type="submit" value="Opslaan" name="submit" />
			</form>
		</div>
	{/if}
		<script type="text/javascript">
	  /* <![CDATA[ */
	  	{if $smarty.session.image.height}
	  		list = {$smarty.session.image.height};
	  	{else}
	  		list = 400;
	  	{/if}
	  	{if $smarty.session.image.height}
	  		imgHeight = {$smarty.session.image.height};
	  	{else}
	  		imgHeight = 500;
	  	{/if}
	  	var height = (list < 600) ? 600 : imgHeight;
	    window.resizeTo({$smarty.session.image.width+30}, height);
	  /* ]]> */
	  </script>
</body>
</html>
{/strip}
      

