{strip}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl">
  <head>
	<title></title>
  	<meta name="robots" content="noindex, nofollow" />
	<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
	<meta http-equiv="imagetoolbar" content="no" />
	<style type="text/css" media="all">@import url("{$SiteRoot}/css/default.css");</style>
  </head>
  	<body style="margin: 0px; padding: 0px; height: 100%;">
  	 <div id="header">
  	     &nbsp;
  	 </div><br />
      <div style="width: 100%; margin-left: 0px; padding: 0px; text-align: left; " >
         {include file=$contentInclude}
       </div>
	</body>
</html>
{/strip}
