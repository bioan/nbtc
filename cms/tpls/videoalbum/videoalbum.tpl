{strip}
{if $smarty.get.action == 'new'} 
        <form method="post" action="" enctype="multipart/form-data">
		    <table style="width: 100%;">
		        <colgroup style="width: 25%"></colgroup>
                <colgroup style="width: 75%"></colgroup>
                <tbody>
                {if isset($error)}
			    <tr>
				    <td colspan="2">
					    De volgende velden zijn niet (goed) ingevuld: <br />
					    <ol>
					    {foreach from=$error item=value}
						    <li><span style="color: #FF0000;">{$value}</span></li>
					    {/foreach}
					    </ol>
				    </td>
			    </tr>
			    {/if}
                
                <tr>
				    <td class="fieldName">
				        <label for="category">Categorie</label>: 
				    </td>
				    <td>
				        <select name="category" id="category" onchange="checkIfDisabled(this);">
                            {if isset($nodes)}
                                {foreach from=$nodes item=node}
                                    {if $node.name != '_root'}
                                    {if $node.oid|in_array:$leafNodes && $node.oid != 0}
                                        <option value="{$node.oid}" {if isset($smarty.post.category) && $smarty.post.category == $node.oid} selected="selected" {/if}>{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                                    {else}
                                        <option value="{$node.oid}" disabled="disabled" style="color: #CCCCCC;">{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                                    {/if}
                                    {/if}
                                {/foreach}
                            {else}
                                <option value="0">Er zijn geen categorie&euml;n aangemaakt.</option>
                            {/if}
                        </select>
				    </td>
			    </tr>
                <tr>
                    <td class="fieldName">
                        <label>Video</label>: 
                    </td>
                    <td>
                        <input type="file" name="video" value="" />
                    </td>
                </tr>
                {foreach from=$languages item=item key=k}
                {assign var=id value=$item.id}
                
                <tr>
                    <td class="fieldName">
                        <label>Photos</label>: 
                    </td>
                    <td>
                        <ul class="photo-menu">
                            <li>
                                <div id="upload_{$id}"><span>kiezen</span></div>
                            </li>
                            <li>
                                <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                            </li>
                            <li class="last">
                                <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                            </li>
                        </ul>
                        <span id="status_{$id}" ></span><br class="clear" />
                        <span id="photos_{$id}">
                        {if isset($smarty.post.photos.$id)}
                            {foreach from=$smarty.post.photos.$id item=photoOid}
                                <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                                <input name="photos[{$id}][{$photoOid}]" value="{$photoOid}" type="hidden" />
                            {/foreach}
                        {/if}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="fieldName">
                        <label for="title_{$id}">Naam {$item.language}</label>: 
                    </td>
                    <td>
                        <input type="text" class="input" name="title[{$id}]" value="{if isset($smarty.post.title.$id)}{$smarty.post.title.$id}{/if}" id="title_{$id}" size="40" />
                    </td>
                </tr>
                <tr>
				    <td class="fieldName">
				        <label>Beschrijving {$item.language}</label>: 
				    </td>
				    <td>
				    {$FCKeditor.$id}
				    </td>
			    </tr>
                <tr>
                    <td class="fieldName">
                        <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                    </td>
                    <td>
                        <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($smarty.post.page_description.$id)}{$smarty.post.page_description.$id}{/if}</textarea>
                    </td>
                </tr>
                {/foreach}
                {if isset($keywords)}
                <tr>
                    <td class="fieldName">
                        Keywords    
                    </td>
                    <td>
                        {foreach from=$keywords item=item key=k}
                        {$k} - {$item}<br />
                        {/foreach}
                    </td>
                </tr>
                {/if}
			    <tr>
                    <td>
                    </td>
				    <td>
                        <input type="hidden" id="module"  value="videoalbum" name="module"  />
                        <br />
				        <input type="image" src="{$SiteRoot}/images/save.png" class="save" />
                        <input type="submit" class="keywords" value="" name="keywords"  />
				    </td>
			    </tr>
		      </tbody>
           </table>
        </form>
{elseif $smarty.get.action == 'overview'} 
    <script type="text/javascript">
 	/* <![CDATA[ */ {literal}
  	function showHide(obj)
  	{
  		var item = obj.parentNode.nextSibling.nextSibling;
		if (item.style.display == 'none') {
			item.style.display = 'block';
		} else {
			item.style.display = 'none';
		}
	}{/literal}
 	/* ]]> */
 	</script>
    Rijen weergeven :&nbsp;
    <select onchange="window.location='videoalbum.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="videoalbum" cellpadding="2" cellspacing="0">
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 20%"></colgroup>    
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="4" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}videoalbum/videoalbum.php?action=overview&items={$items}&sort=title&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Naam</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}videoalbum/videoalbum.php?action=overview&items={$items}&sort=category_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Categorie</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}videoalbum/videoalbum.php?action=overview&items={$items}&sort=date_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Project datum</a>
                </td>
                <td class="first-row-last-column">
                    <a href="{$SiteRoot}videoalbum/videoalbum.php?action=overview&items={$items}&sort=filled&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Stats</a>
                </td>
            </tr>
            {if !$videos}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                        Er zijn nog geen photoalbum items gepost.          
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$videos item=video name="iCounter"}
    			<tr id="{$video.id}" order_number="{$video.order_number}" ondblclick="window.location.href = '{$SiteRoot}videoalbum/videoalbum.php?action=edit&id={$video.id}'" class="pointer" >
                    {if $smarty.foreach.iCounter.iteration % 2 == 1}   
    				<td class="first-column-gray">
    				    {$video.title|stripslashes}
    				</td>
    				<td class="column-gray">
    				    {$video.category_name|stripslashes}
    				</td>
                    <td class="column-gray">
    				    {$video.date_format}
    				</td>
                    <td class="last-column-gray">
    				    <span class="{if $video.filled == $languages|@count}green{else}red{/if}">{$video.filled} / {$languages|@count}</span>
    				</td>
                    {else}
                    <td class="first-column-white">
                        {$video.title|stripslashes}
                    </td>
                    <td class="column-white">
                        {$video.category_name|stripslashes}
                    </td>
                    <td class="column-white">
                        {$video.date_format}
                    </td>
                    <td class="last-column-white">
                        <span class="{if $video.filled == $languages|@count}green{else}red{/if}">{$video.filled} / {$languages|@count}</span>
                    </td>
                    {/if}
    			</tr>	
        	{/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>     	
        </tbody>
    </table>
       <div style="padding-top: 0.6em; text-align: center;">
		{$links}
	</div>
{elseif $smarty.get.action == 'edit'}  

    <form method="post" action="" enctype="multipart/form-data">
		<table style="width: 100%;">
		    <colgroup style="width: 25%"></colgroup>
            <colgroup style="width: 75%"></colgroup>
            <tbody>
            {if isset($error)}
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
            
            <tr>
				<td class="fieldName">
				    <label for="category">Categorie</label>: 
				</td>
				<td>
				<select name="category" id="category" onchange="checkIfDisabled(this);">
				    {if $nodes}
                        {foreach from=$nodes item=node}
                            {if $node.name != '_root'}
                            {if $node.oid|in_array:$leafNodes && $node.oid != 0}
                                <option value="{$node.oid}" {if $node.oid == $video.category_id} selected="selected" {/if}>{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                            {else}
                                <option value="{$node.oid}" disabled="disabled" style="color: #CCCCCC;">{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                            {/if}
                            {/if}
                        {/foreach}
                    {else}
                        <option value="0">Er zijn geen categorie&euml;n aangemaakt.</option>
                    {/if}
                </select>
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label>Video</label>: 
                </td>
                <td>
                    <input type="file" name="video" value="" />
                </td>
            </tr>
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
            
            <tr>
                <td class="fieldName">
                    <label>Photos</label>: 
                </td>
                <td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_{$id}"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_{$id}" ></span><br class="clear" />
                    <span id="photos_{$id}">
                    {if !empty($video.$id.photos)}
                    {foreach from=$video.$id.photos item=photo}
                        <img id="{$photo.id}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photo.id}/100/image.php" />
                        <input name="photos[{$id}][{$photo.id}]" value="{$photo.id}" type="hidden" />
                    {/foreach}
                    {/if}
                    </span>
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="title_{$id}">Name {$item.language}</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="title[{$id}]" value="{if isset($video.$id.title)}{$video.$id.title}{/if}" id="title_{$id}" size="40" />
                </td>
            </tr>
            <tr>
				<td class="fieldName">
				    <label>Beschrijving {$item.language}</label>: 
				</td>
				<td>
				    {$FCKeditor.$id}
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($video.$id.page_description)}{$video.$id.page_description}{/if}</textarea>
                </td>
            </tr>
            {/foreach}
			<tr>
                <td>
                </td>
				<td> 
                    <input type="hidden" id="module"  value="videoalbum" name="module"  />
                    <br />
				    <input type="submit" id="save" value="">
                    <a class="verwijderen" href="{$SiteRoot}/videoalbum/videoalbum.php?action=delete&amp;id={$video.id}" onclick="return confirm('Wilt u deze afbeelding verwijderen?!');"></a>
				    <input type="submit" class="keywords" value="" name="keywords"  />
                </td>
			</tr>
		</table>
	</form>
    
{/if}
{/strip}
