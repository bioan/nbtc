{if $smarty.get.action == 'new'}
    <form method="post" action="">
        <table style="width: 100%;">
            <colgroup style="width: 25%"></colgroup>
            <colgroup style="width: 75%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="2">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            
            <tr>
                <td class="fieldName">
                    <label for="private">Private</label>:
                </td>
                <td>
                    <input type="checkbox" value="1" name="private" id="private" {if isset($smarty.post.private) && $smarty.post.private == 1} checked="checked" {/if} />       
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="category">Onder categorie</label>:
                </td>
                <td>
                    <select name="category" id="category" onchange="checkIfDisabled(this);">
                        {foreach from=$Trees item=node}
                            {if $node.level == 0}
                                <option value="{$node.oid}" style="font-weight: bold;" {if isset($smarty.post.category) && $smarty.post.category == $node.oid} selected="selected" {/if}>{$node.name|stripslashes}</option>
                            {elseif $node.level > 0 && $node.level < 3}
                                <option value="{$node.oid}" {if $node.level == 3}disabled="disabled"{/if} style="font-weight: bold;">{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                            {else}
                                <option value="{$node.oid}" disabled="disabled" style="color: #CCCCCC;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$node.name|stripslashes}</option>
                            {/if}
                        {/foreach}
                    </select>
                </td>
            </tr>
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
            
            <tr>
				<td class="fieldName">
				    <label>Foto</label>: 
				</td>
				<td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_{$id}"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_{$id}" ></span><br class="clear" />
				    <span id="photos_{$id}">
                    {if isset($smarty.post.photos.$id)}
                        {foreach from=$smarty.post.photos.$id item=photoOid}
                            <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                            <input name="photos[{$id}][{$photoOid}]" value="{$photoOid}" type="hidden" />
                        {/foreach}
                        {/if}
                    </span>
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <lable for="name_{$id}">Naam {$item.language}</label>:
                </td>
                <td>
                    <input name="name[{$id}]" id="name_{$id}" type="text" class="input" value="{if isset($smarty.post.name.$id)}{$smarty.post.name.$id}{/if}" size="40" />
                </td>
            </tr>
            <tr>
				<td class="fieldName">
				    <label for="f_id_2">Bericht {$item.language}</label>: 
				</td>
				<td>
				    {$FCKeditor.$id}
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($smarty.post.page_description.$id)}{$smarty.post.page_description.$id}{/if}</textarea>
                </td>
            </tr>
            {/foreach}
            <tr>
                <td>
                </td>
				<td>
                    <input type="hidden" id="module"  value="videoalbum" name="module"  />
                    <br />
				    <a href="#" onclick="document.forms[0].submit();"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="" /></a>
				</td>
			</tr>
    		</tbody>
        </table>
    </form>
{elseif $smarty.get.action == 'overview'}
    <ul class="tree">
        <li class="first-child">
            <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
            <h2 class="font">Overzicht</h2>
            <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
        </li>
        <li class="fields">
            <img src="{$SiteRoot}/images/first_row_left_white.jpg" class="left" alt="" />
            <div style="width:30%"><a href="{$SiteRoot}videoalbum/cats.php?action=overview&sort=name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Categorie</a></div>
            <div style="width:30%"><a href="{$SiteRoot}videoalbum/cats.php?action=overview&sort=filled&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Stats</a></div>
            <img src="{$SiteRoot}/images/first_row_right_white.jpg" class="right" alt="" />
        </li>
    </ul>
    <ul class="tree" id="tmenu">
        {if !$existsCats}
        <li class="gray">
            <img src="{$SiteRoot}/images/first_column_gray_left.jpg" class="left" alt="" /><div style="width:30%">Er zijn nog geen categorie&euml;n aangemaakt</div><img src="{$SiteRoot}/images/last_column_gray_right.jpg" class="right" alt="" /> 
        </li>
        {/if}
        {foreach from=$parents item=item name="iCounter" key=k}
			<li id="i_1_4_{$item.oid}" class="pointer {if $smarty.foreach.iCounter.iteration % 2 == 1}gray{else}white{/if}">
                <div class="row" ondblclick="window.location.href = '{$SiteRoot}videoalbum/cats.php?action=edit&id={$item.oid}'"> 
                    <img src="{$SiteRoot}/images/first_column_gray_left.jpg" class="left" alt="" />
                    <div style="width:30%"><img src="{$SiteRoot}/images/arrow.png" alt="move" width="10" height="10" class="handle-tmenu arrow-drag" />{str_repeat("&nbsp;",$item.level*2)}{$item.name|stripslashes}</div>
                    <div style="width:30%"><span class="{if $item.filled == $languages|@count}green{else}red{/if}">{$item.filled} / {$languages|@count}</span></div>
                    <img src="{$SiteRoot}/images/last_column_gray_right.jpg" class="right" alt="" />
                </div>
                {if is_array($item.childs) && $item.childs|@count > 0}
                <br class="clear" />
                <ul id="t{$item.oid}">
                {foreach from=$item.childs item=citem name="iCounter2" key=ck}
        			<li id="i_2_{$item.oid}_{$citem.oid}" class="pointer {if $smarty.foreach.iCounter2.iteration % 2 == 0}gray{else}white{/if}">
                        <div class="row" ondblclick="window.location.href = '{$SiteRoot}videoalbum/cats.php?action=edit&id={$citem.oid}'">
                            <img src="{$SiteRoot}/images/first_column_gray_left.jpg" class="left" alt="" />
                            <div style="width:30%"><img src="{$SiteRoot}/images/arrow.png" alt="move" width="10" height="10" class="handle-t{$item.oid} arrow-drag" />{str_repeat("&nbsp;",$citem.level*2)}{$citem.name|stripslashes}</div>
                            <div style="width:30%"><span class="{if $citem.filled == $languages|@count}green{else}red{/if}">{$citem.filled} / {$languages|@count}</span></div>
                            <img src="{$SiteRoot}/images/last_column_gray_right.jpg" class="right" alt="" />
                        </div>
                        {if is_array($citem.childs) && $citem.childs|@count > 0}
                        <br class="clear" />
                        <ul id="t{$citem.oid}">
                        {foreach from=$citem.childs item=ccitem name="iCounter3" key=cck}
                			<li id="i_2_{$citem.oid}_{$ccitem.oid}" ondblclick="window.location.href = '{$SiteRoot}videoalbum/cats.php?action=edit&id={$ccitem.oid}'" class="pointer {if $smarty.foreach.iCounter3.iteration % 2 == 0}gray{else}white{/if}">
                                <img src="{$SiteRoot}/images/first_column_gray_left.jpg" class="left" alt="" />
                                <div style="width:30%"><img src="{$SiteRoot}/images/arrow.png" alt="move" width="10" height="10" class="handle-t{$citem.oid} arrow-drag" />{str_repeat("&nbsp;",$ccitem.level*2)}{$ccitem.name|stripslashes}</div>
                                <div style="width:30%"><span class="{if $ccitem.filled == $languages|@count}green{else}red{/if}">{$ccitem.filled} / {$languages|@count}</span></div>
                                <img src="{$SiteRoot}/images/last_column_gray_right.jpg" class="right" alt="" />
                            </li>
                        {/foreach}
                        </ul>
                        {/if}
                    </li>
                {/foreach}
                </ul>
                {/if} 
            </li>   
        {/foreach}
    </ul>
    <ul class="tree">
        <li class="bottom">
            <img src="{$SiteRoot}/images/last_row_left.jpg" class="left" alt="" /><img src="{$SiteRoot}/images/last_row_right.jpg" class="right" alt="" />
            <div style="width:30%;"></div>        
        </li>
    </ul>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{elseif $smarty.get.action == 'edit'}
     <form method="post" action="" >
    	<table style="width: 100%;">
            <colgroup style="width: 25%"></colgroup>
            <colgroup style="width: 75%"></colgroup>
            <tbody>
            {if isset($error)}
    		<tr>
    			<td colspan="2">
    				De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
    				{foreach from=$error item=value}
    					<li><span style="color: #FF0000;">{$value}</span></li>
    				{/foreach}
    				</ol>
    			</td>
    		</tr>
    		{/if}
            
            <tr>
                <td class="fieldName">
                    <label for="private">Private</label>:
                </td>
                <td>
                    <input type="checkbox" value="1" name="private" id="private" {if $category.private == 1} checked="checked" {/if} />
                </td>
            </tr>
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
            
            <tr>
				<td class="fieldName">
				<label>Afbeelding</label>: 
				</td>
				<td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_{$id}"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_{$id}" ></span><br class="clear" />
					<span id="photos_{$id}">
                    {if !empty($category.$id.photos)}
                    {foreach from=$category.$id.photos item=photo}
	                    	<img id="{$photo.id}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto/{$photo.id}/100/image.jpg" />
							<input name="photos[{$id}][{$photo.id}]" value="{$photo.id}" type="hidden" />
					{/foreach}
                    {/if}
                    </span>
				</td>
			</tr>
    		<tr>
    			<td class="fieldName">
    			     <label for="name_{$id}">Categorie naam {$item.language}</label>:
    			</td>
    			<td>
    			     <input type="text" class="input" name="name[{$id}]" id="name_{$id}" size="40" value="{if isset($category.$id.name)}{$category.$id.name}{/if}" />
    			 </td>
            </tr>
            <tr>
				<td class="fieldName">
				    <label>Omschrijving {$item.language}</label>: 
				</td>
				<td>
				    {$FCKeditor.$id}
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($category.$id.name)}{$category.$id.page_description}{/if}</textarea>
                </td>
            </tr>
            {/foreach}
            <tr>
                <td></td>
    				<td>
                        <input type="hidden" id="module"  value="videoalbum" name="module"  />
                        <br />
    				    <a href="#" onclick="document.forms[0].submit();" id="save"></a>
                        <a href="{$SiteRoot}/videoalbum/cats.php?action=delete&amp;id={$category.oid}" onclick="return confirm('Wilt u dit product verwijderen?');" class="verwijderen"></a>
    				</td>
    			</tr>
        </table>
    </form>
{/if}
