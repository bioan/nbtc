<form action="" method="post">
    <table cellspacing="0" cellpadding="0" style="100%">
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 80%"></colgroup>
        <tr>
            <td class="fieldTitle" colspan="2">
               1. Linkedin Connection
            </td>
        </tr>
        {if isset($successMessage) && $appErrors|count == 0}
        <tr>
            <td class="successMessage" colspan="2">
               {$successMessage}
            </td>
        </tr>
        {/if}
        {if isset($successAccToken)}
        <tr>
            <td class="successMessage" colspan="2">
               {$successAccToken}
            </td>
        </tr>
        {/if}
        {if isset($appErrors.consumer_key)}
        <tr>
            <td class="error" colspan="2">
               {$appErrors.consumer_key}
            </td>
        </tr>
        {/if}
        <tr>
            <td class="fieldName">
                Consumer key:
            </td>
            <td>
                <input type="text" class="input" name="consumer_key" value="{if isset($smarty.post.consumer_key)}{$smarty.post.consumer_key}{elseif isset($appValues.consumer_key)}{$appValues.consumer_key}{else}{/if}" size="30">
            </td>
        </tr>
        {if isset($appErrors.consumer_secret)}
        <tr>
            <td class="error" colspan="2">
               {$appErrors.consumer_secret}
            </td>
        </tr>
        {/if}
        <tr>
            <td class="fieldName">
                Consumer secret:
            </td>
            <td>
                <input type="text" class="input" name="consumer_secret" value="{if isset($smarty.post.consumer_secret)}{$smarty.post.consumer_secret}{elseif isset($appValues.consumer_secret)}{$appValues.consumer_secret}{else}{/if}" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName" colspan="2">
                <input type="submit" value="Save Changes" name="saveChangesApp" />
                {if isset($errorCode)}
                    {$errorCode}
                {else}
                    {if $url != 'false'}<a href="{$url}" title="Grant Permissions">Grant permissions</a>
                    {else} You must fill in application data
                    {/if}
                {/if}
           </td>
        </tr>
    </table>
</form>