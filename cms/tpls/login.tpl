<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Inforitus - Content Manament System</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
        <meta http-equiv="Content-Language" content="NL" />
        <style type="text/css">{literal}
        body, html
        {
            text-align: center;
            width: 100%;
            height: 100%;
        
            padding: 0px;
            margin: 0px;
        }
        #identify
        {
            width: 344px;
            height: 350px;
        
            position: absolute;
            left: 50%;
            top: 50%;
        
            margin-left: -163px;
            margin-top: -267px;
        }
        #login
        {
            font-family: Verdana;
            font-size: 12px;
            color: #616161;
            background: url(images/login-form.jpg) no-repeat;
            line-height: 26px;
            text-align: left;
            padding:64px 0px 0px 0px;
            width: 344px;
            height: 123px;
        }
        #login input
        {
            font-family: Verdana;
            font-size: 10px;
        }
        
        .input {
            border: 1px solid #e5e5e5;
            width: 202px;
            height: 23px;
            background: url(images/input.jpg);
        }
        
        #submit {
            border: none;
            background: url(images/inloggen.jpg) no-repeat;
            width: 86px;
            height: 29px;
            float: right;
            margin: 6px 0px 0px 0px;
        }
        
        #login table {
            margin: 0px 0px 0px 20px;
        }
        
        #login table td {
            padding: 0px 20px 0px 0px;
            font-size: 11px;
            line-height: 34px;
        }
        {/literal}</style>
        <meta name="description" content="Inforitus Content Management systeem." />
        <meta name="keywords" content="Inforitus, content, management, systeem" />
        <meta name="author" content="Inforitus" />
    </head>
    <body>
        <div id="identify">
            <table cellpadding="0" cellspacing="0" border="0" style="width:100%">
                <tr>
                    <td valign="middle" style="width:162px" align="left" height="135">
                        <img src="images/inforitus2.jpg" alt="Inforitus" style="padding:0px 0px 0px 15px;" />
                    </td>
                    <td valign="middle" style="width:162px" align="right" height="135">
                        <img src="images/dfl.jpg" alt="" style="padding:0px 15px 0px 0px;max-height:135px;max-width:147px;" />    
                    </td>
                </tr>
            </table>
            <div id="login">
                <form action="index.php" method="post">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>Naam:</td>
                            <td><input type="text" class="input" name="user_name" /></td>
                        </tr>
                        <tr>
                            <td>Wachtwoord:</td>
                            <td><input type="password" class="input" name="password" /></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" value="" id="submit" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>
