{strip}
{if $smarty.get.action == 'overview'} 
    Rijen weergeven :&nbsp;
    <select onchange="window.location='errors.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="photoalbum" cellpadding="2" cellspacing="0">
        <colgroup style="width: 28%"></colgroup>
        <colgroup style="width: 7%"></colgroup>
        <colgroup style="width: 8%"></colgroup>
        <colgroup style="width: 51%"></colgroup>
        <colgroup style="width: 6%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="5" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}errors/errors.php?action=overview&items={$items}&sort=url&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Url</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}errors/errors.php?action=overview&items={$items}&sort=ip&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Ip</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}errors/errors.php?action=overview&items={$items}&sort=date&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}errors/errors.php?action=overview&items={$items}&sort=message&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Message</a>
                </td>
                <td class="first-row-last-column">
                    
                </td>
            </tr>
            {if !$errors}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                        Er zijn nog geen errors items gepost.          
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$errors item=error name="iCounter"}
    			<tr id="{$error.id}" order_number="{$error.order_number}" ondblclick="window.location.href = '{$SiteRoot}errors/errors.php?action=edit&id={$error.id}'" class="pointer" >
                    {if $smarty.foreach.iCounter.iteration % 2 == 1}   
    				<td class="first-column-gray">
    				    {$error.url}
    				</td>
    				<td class="column-gray">
    				    {$error.ip}
    				</td>
                    <td class="column-gray">
    				    {$error.date_format}
    				</td>
    				<td class="column-gray">
                        {$error.message}
                    </td>
                    <td class="last-column-gray">
                        <a href="{$SiteRoot}errors/errors.php?action=delete&id={$error.id}">Verwijderen</a>
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$error.url}
                    </td>
                    <td class="column-white">
                        {$error.ip}
                    </td>
                    <td class="column-white">
                        {$error.date_format}
                    </td>
                    <td class="column-white">
                        {$error.message} 
                    </td>
                    <td class="last-column-white">
                        <a href="{$SiteRoot}errors/errors.php?action=delete&id={$error.id}">Verwijderen</a>
                    </td>
                    {/if}
    			</tr>	
        	{/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>     	
        </tbody>
    </table>
       <div style="padding-top: 0.6em; text-align: center;">
		{$links}
	</div>
{/if}
{/strip}
