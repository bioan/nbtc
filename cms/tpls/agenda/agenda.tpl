{strip}
{if $smarty.get.action == 'new'}
	<form method="post" action="" enctype="multipart/form-data">
		<table style="width: 100%;">
		    <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
            <tr>
				<td class="fieldName">
				    <label for="date">Datum</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="date" id="date" value="{$sCurrentDate}" size="10" />
                </td>
			</tr>
            <tr>
				<td class="fieldName">
				    <label for="price">Prijs</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="price" id="price" value="{if isset($smarty.post.price)}{$smarty.post.price}{/if}" size="40" />
				</td>
			</tr>
            <tr>
				<td class="fieldName">
				    <label for="group_id">Agendagroep</label>: 
				</td>
				<td>
				    <select name="group_id" id="group_id">
				        {foreach from=$AgendaGroups item=value}
				            <option value="{$value.id}" {if isset($smarty.post.group_id) && $smarty.post.group_id == $value.id} selected="selected" {/if}>{$value.group_name|stripslashes}</option>
				        {/foreach}
				    </select>
				</td>
			</tr>
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
            
            <tr>
				<td class="fieldName">
				    <label for="f_id_1">Foto {$item.language}</label>: 
				</td>
				<td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_{$id}"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_{$id}" ></span><br class="clear" />
				    <span id="photos_{$id}">
                    {if isset($smarty.post.photos.$id)}
                        {foreach from=$smarty.post.photos.$id item=photoOid}
                            <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                            <input name="photos[{$id}][{$photoOid}]" value="{$photoOid}" type="hidden" />
                        {/foreach}
                    {/if}
                    </span>
				</td>
			</tr>
            
            <tr>
				<td class="fieldName">
				    <label for="title">Titel {$item.language}</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="title[{$id}]" id="title" value="{if isset($smarty.post.title.$id)}{$smarty.post.title.$id}{/if}" size="40" />
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label>Bericht {$item.language}</label>: 
				</td>
				<td>
				    {$FCKeditor.$k}
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description">Pagina omschrijving {$item.language}</label>:
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description">{if isset($smarty.post.page_description.$id)}{$smarty.post.page_description.$id}{/if}</textarea>
                </td>
            </tr>
            {/foreach}
            {if isset($keywords)}
            <tr>
                <td class="fieldName">
                    Keywords    
                </td>
                <td>
                    {foreach from=$keywords item=item key=k}
                    {$k} - {$item}<br />
                    {/foreach}
                </td>
            </tr>
            {/if}
			<tr>
                <td></td>
				<td>
                    <br />
                    <input type="hidden" id="module"  value="agenda" name="module"  />
				    <a href="#" onclick="document.forms[0].submit();" class="save"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="" /></a>
                    <input type="submit" class="keywords" value="" name="keywords"  />
				</td>
			</tr>
        </table>
	</form>
    <script>
    {literal}
	$(function() {
		$( "#date" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    {/literal}
	</script>
{elseif $smarty.get.action == 'overview'}
    Rijen weergeven :&nbsp; 
    <select onchange="window.location='agenda.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="agenda" cellpadding="2" cellspacing="0">
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 21%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="4" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}agenda/agenda.php?action=overview&items={$items}&sort=title&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Titel</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}agenda/agenda.php?action=overview&items={$items}&sort=group_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Groepsnaam</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}agenda/agenda.php?action=overview&items={$items}&sort=date_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum</a>
                </td>
                <td class="first-row-last-column">
                    <a href="{$SiteRoot}agenda/agenda.php?action=overview&items={$items}&sort=filled&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Stats</a>
                </td>
            </tr>
            {if !$AgendaItems}
                <tr nodrag="true" nodrop="true">     
                    <td class="first-column-gray">
                    Er zijn nog geen agenda items gepost.         
                    </td>
                    <td class="column-gray">
                    
                    </td>
                    <td class="column-gray">
                    
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$AgendaItems item=item name="iCounter"}
				
                    
                <tr id="{$item.id}" order_number="{$item.order_number}" ondblclick="window.location.href = '{$SiteRoot}agenda/agenda.php?action=edit&id={$item.id}'" class="pointer">
                {if $smarty.foreach.iCounter.iteration % 2 == 1}  
                    <td class="first-column-gray">
                        {$item.title|stripslashes}
                    </td>
                    <td class="column-gray">
                        {$item.group_name}
                    </td>
                    <td class="column-gray">
                        {$item.date_format}
                    </td>
                    <td class="last-column-gray">
                        <span class="{if $item.filled == $languages|@count}green{else}red{/if}">{$item.filled} / {$languages|@count}</span>
                    </td>
                {else}
                    <td class="first-column-white">
                        {$item.title|stripslashes}
                    </td>
                    <td class="column-white">
                        {$item.group_name}
                    </td>
                    <td class="column-white">
                        {$item.date_format}
                    </td> 
                    <td class="last-column-white">
                        <span class="{if $item.filled == $languages|@count}green{else}red{/if}">{$item.filled} / {$languages|@count}</span>
                    </td>   
                {/if}
                </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
		{$links}
	</div>
{elseif $smarty.get.action == 'edit'}

	<form method="post" action="" >
	    <table style="width: 100%;">
		    <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
			<tr>
				<td colspan="3">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
            <tr>
				<td class="fieldName">
				    <label for="date">Datum</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="date" id="date" value="{$values.date_format}" size="10" />
                </td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="price">Prijs</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="price" id="price" value="{$values.price}" size="40" />
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="group_id">Agendagroep</label>: 
				</td>
				<td>
				    <select name="group_id" id="group_id">
				        {foreach from=$AgendaGroups item=value}
				            <option value="{$value.id}" {if $values.group_id == $value.id} selected="selected" {/if}>{$value.group_name|stripslashes}</option>
				        {/foreach}
				    </select>
				</td>
			</tr>
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
            
            <tr>
				<td class="fieldName">
				    <label>Foto(s) {$item.language}</label>: 
				</td>
				<td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_{$id}"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_{$id}" ></span><br class="clear" />
					<span id="photos_{$id}">
					{if !empty($values.$id.photos)}
	                    {foreach from=$values.$id.photos item=photo}
	                    	<img id="{$photo.id}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto/{$photo.id}/100/image.php" />
							<input name="photos[{$id}][{$photo.id}]" value="{$photo.id}" type="hidden" />
						{/foreach}
					{/if}
                    </span>
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="title_{$id}">Titel {$item.language}</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="title[{$id}]" id="title_{$id}" value="{if isset($values.$id.title)}{$values.$id.title}{/if}" size="40" />
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="f_id_2">Bericht {$item.language}</label>: 
				</td>
				<td>
				    {$FCKeditor.$k}
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($values.$id.page_description)}{$values.$id.page_description}{/if}</textarea>
                </td>
            </tr>
            {/foreach}
            {if isset($keywords)}
            <tr>
                <td class="fieldName">
                    Keywords    
                </td>
                <td>
                    {foreach from=$keywords item=item key=k}
                    {$k} - {$item}<br />
                    {/foreach}
                </td>
            </tr>
            {/if}
			<tr>
                <td></td>
				<td>    
                    <br />
                    <input type="hidden" id="module"  value="agenda" name="module"  />
				    <input class="button" type="submit" name="submit" value="" id="save" />  
                    <a class="verwijderen" href="{$SiteRoot}/agenda/agenda.php?action=delete&amp;id={$values.id}" onclick="return confirm('Weet u zeker dat u dit nieuws item wilt verwijderen?'); return false;"></a>
				    <input type="submit" class="keywords" value="" name="keywords"  />
                </td>
			</tr>
        </table>
	</form>
	<script>
    {literal}
	$(function() {
		$( "#date" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    {/literal}
	</script>
{else}
	Deze pagina is niet helemaal ok!
{/if}
{/strip}
