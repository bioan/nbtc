{if $smarty.get.action == 'new'}
    <form method="post" action="" >
		<table style="width: 100%;">
		    <colgroup style="width: 25%"></colgroup>
            <colgroup style="width: 75%"></colgroup>
            <tbody>                               
            {if isset($error)}
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
			<tr>
				<td class="fieldName">
				    <label for="name_{$id}">Naam {$item.language}</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="group_name[{$id}]" id="name_{$id}" value="{if isset($smarty.post.group_name.$id)}{$smarty.post.group_name.$id}{/if}" size="40" title="Max 100 tekens" maxlength="101" onchange="countAndTruncate(this, 100, 'f_540_counter_id');" onkeyup="countAndTruncate(this, 100, 'f_540_counter_id');"/>
				    <span id="f_540_counter_id"></span>
                    <input type="hidden" name="page_description[{$id}]" value="" />
                </td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="description_{$id}">Beschrijving {$item.language}</label>: 
				</td>
				<td>
				    {$FCKeditor.$id}
				</td>
			</tr>
            
            {/foreach}
			<tr>
                <td></td>
				<td><br />
				<a href="#" onclick="document.forms[0].submit();"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="" /></a>
				</td>
			</tr>
		</table>
	</form>
{elseif $smarty.get.action == 'overview'} 
    Rijen weergeven :&nbsp;
    <select onchange="window.location='groups.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="groups" cellpadding="2" cellspacing="0">
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 41%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}newsletter/subscribers_groups.php?action=overview&items={$items}&sort=group_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Groepnaam</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}newsletter/subscribers_groups.php?action=overview&items={$items}&sort=date_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum toegevoegd</a>
                </td>
                <td class="first-row-last-column">
                    <a href="{$SiteRoot}newsletter/subscribers_groups.php?action=overview&items={$items}&sort=filled&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Stats</a>
                </td>
            </tr>
             {if !$newsGroup}
            <tr nodrag="true" nodrop="true">
                <td class="first-column-gray">
                    Er zijn nog geen nieuwsgroepen toegevoegd.      
                </td>
                <td class="column-gray">
                    
                </td>
                <td class="last-column-gray">
                    &nbsp;
                </td>
            </tr>
            {/if}
            {foreach from=$newsGroup item=item name="iCounter"}
				
            <tr id="{$item.id}" order_number="{$item.order_number}" ondblclick="window.location.href = '{$SiteRoot}newsletter/subscribers_groups.php?action=edit&id={$item.id}'" class="pointer">
            
                {if $smarty.foreach.iCounter.iteration % 2 == 1}   
                
                <td class="first-column-gray">
                    {$item.group_name}
                </td>
                <td class="column-gray">
                    {$item.date_format}
                </td>
                <td class="last-column-gray">
                    <span class="{if $item.filled == $languages|@count}green{else}red{/if}">{$item.filled} / {$languages|@count}</span>
                </td>
                {else}
                <td class="first-column-white">
                    {$item.group_name}
                </td>
                <td class="column-white">
                    {$item.date_format}
                </td>
                <td class="last-column-white">
                    <span class="{if $item.filled == $languages|@count}green{else}red{/if}">{$item.filled} / {$languages|@count}</span>
                </td>
                {/if}
            </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{elseif $smarty.get.action == 'edit'}
    <form method="post" action="" >
		<table style="width: 100%;">
		    <colgroup style="width: 25%"></colgroup>
            <colgroup style="width: 75%"></colgroup>
            <tbody>
            {if isset($error)}
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
			<tr>
				<td class="fieldName">
				    <label for="name_{$id}">Naam {$item.language}</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="group_name[{$id}]" id="name_{$id}" value="{if isset($newsGroup.$id.group_name)}{$newsGroup.$id.group_name|stripslashes}{/if}" size="40" title="Max 100 tekens" maxlength="101" onchange="countAndTruncate(this, 100, 'f_540_counter_id');" onkeyup="countAndTruncate(this, 100, 'f_540_counter_id');"/>
				    <span id="f_540_counter_id"></span>
                    <input type="hidden" name="page_description[{$id}]" value="" />
                </td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="description_{$id}">Beschrijving {$item.language}</label>: 
				</td>
				<td>
				    {$FCKeditor.$id}
				</td>
			</tr>
            {/foreach}
			<tr>
                <td></td>
				<td>
                    <br />
				    <a href="#" onclick="document.forms[0].submit();" id="save"></a>
                    <a class="verwijderen" href="{$SiteRoot}/newsletter/subscribers_groups.php?action=delete&amp;id={$newsGroup.id}" onclick="return confirm('Weet u zeker dat u deze nieuwsgroep wilt verwijderen?'); return false;"></a>
				</td>
			</tr>
		</table>
	</form>
{/if}
