{strip}
{if $smarty.get.action == 'new'}
    <form method="post" action="" enctype="multipart/form-data">
        <table style="width: 100%;">
            <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="2">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                    {if isset($list)}
                    <ol>
                    {foreach from=$list item=value}
                        <li><span style="color: #FF0000;">{$value.1} {$value.0}</span></li>
                    {/foreach}
                    </ol>
                    {/if}
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName">
                    <label>CSV</label>: 
                </td>
                <td>
                    <input type="file" name="file" value="" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="delimiter">Fields terminated by</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="delimiter" id="delimiter" value="{if isset($smarty.post.delimiter)}{$smarty.post.delimiter}{else},{/if}" size="1" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="link">Naam</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="name" id="name" value="{if isset($smarty.post.name)}{$smarty.post.name}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="email">Email</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="email" id="email" value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="group_id">Group</label>: 
                </td>
                <td>
                    <select name="group_id" id="group_id">
                        {foreach from=$templates item=value}
                            <option value="{$value.id}" {if isset($smarty.post.group_id) && $smarty.post.group_id == $value.id} selected="selected" {/if} >{$value.group_name|stripslashes}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br />
                    <input type="hidden" id="module"  value="newsletter" name="module"  />
                    <a href="#" onclick="document.forms[0].submit();" class="save"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="" /></a>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
{elseif $smarty.get.action == 'overview'}
    Rijen weergeven :&nbsp;
    <select onchange="window.location='subscribers.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="subscribers" cellpadding="2" cellspacing="0">
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 21%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="4" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}newsletter/subscribers.php?action=overview&items={$items}&sort=name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Naam</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}newsletter/subscribers.php?action=overview&items={$items}&sort=email&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Email</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}newsletter/subscribers.php?action=overview&items={$items}&sort=group_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Group</a>
                </td>
                <td class="first-row-last-column">
                    <a href="{$SiteRoot}newsletter/subscribers.php?action=overview&items={$items}&sort=date&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum toegevoegd</a>
                </td>
            </tr>
            {if !$photos}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                    Er zijn nog geen nieuws items gepost.    
                    </td>
                    <td class="column-gray">
                    
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$photos item=item name="iCounter"}
                
                <tr id="{$item.id}" order_number="{$item.order_number}" ondblclick="window.location.href = '{$SiteRoot}newsletter/subscribers.php?action=edit&id={$item.id}'" class="pointer">
                    {if $smarty.foreach.iCounter.iteration % 2 == 1} 
                    <td class="first-column-gray">
                        {$item.name|stripslashes}
                    </td>
                    <td class="column-gray">
                        {$item.email}
                    </td>
                    <td class="column-gray">
                        {$item.group_name}
                    </td>
                    <td class="last-column-gray">
                        {$item.date_register_format}
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$item.name|stripslashes}
                    </td>
                    <td class="column-white">
                        {$item.email}
                    </td>
                    <td class="column-white">
                        {$item.group_name}
                    </td>
                    <td class="last-column-white">
                        {$item.date_register_format}
                    </td>
                    {/if}
                </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{elseif $smarty.get.action == 'edit'}
    <form method="post" action="" >
        <table style="width: 100%;">
            <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="3">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            
            <tr>
                <td class="fieldName">
                    <label for="link">Naam</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="name" id="name" value="{$values.name}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="email">Email</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="email" id="email" value="{$values.email}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="group_id">Group</label>: 
                </td>
                <td>
                    <select name="group_id" id="group_id">
                        {foreach from=$templates item=value}
                            <option value="{$value.id}" {if $values.group_id == $value.id} selected="selected" {/if}>{$value.group_name|stripslashes}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            
            <tr>
                <td></td>
                <td>
                    <br />
                    <input type="hidden" id="module"  value="newsletter" name="module"  />
                    <input class="button" type="submit" name="submit" id="save" value="" />
                    <a class="verwijderen" href="{$SiteRoot}/newsletter/subscribers.php?action=delete&amp;id={$values.id}" onclick="return confirm('Weet u zeker dat u dit nieuws item wilt verwijderen?'); return false;"></a>
                    
                </td>
            </tr>
            </tbody>
        </table>
    </form>
{else}
    Deze pagina is niet helemaal ok!
{/if}
{/strip}
