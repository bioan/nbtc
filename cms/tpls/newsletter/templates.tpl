<script>
{literal}

$ (document).ready (function (){
    $('#templateForm').submit(function(ev) {
        
        
        
        var val = $("input[type=submit][clicked=true]").val();
        if ($ ('input[type="submit"][value="'+val+'"]').attr('name') == 'send')
        {
            var data = $('#templateForm').serialize();
            data = data + '&send=send';
            $.post('/cms/newsletter/templates.php?action=edit&id=' + $ ('#template').val (),
                 data,
                function(result) {
                    
                }
            );
            
            
            setTimeout('getProgress ()', 500);
            
            return false;
        }
        else
            return true;
    });
    
    $("form input[type=submit]").click(function() {
        $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
        $(this).attr("clicked", "true");
    });
});

function getProgress ()
{
    $.getJSON ('/cms/newsletter/progress.js', function(data){
        if (data)
        {
            $("#progressbar").progressbar({ value: data ['progress'] });
            
            if (data ['progress'] != 100)
                setTimeout('getProgress ()', 500);
        }
    })       
}

            
{/literal}
</script>
{if $smarty.get.action == 'new'}
<div class="pagina-form modules">
    <div id="pagina-title" class="pagina-titel">{$folder.text}</div>   
    <div id="content_in_content_div">
    <form method="post" action="" >
		<table style="width: 100%;">
		    <colgroup style="width: 25%"></colgroup>
            <colgroup style="width: 75%"></colgroup>
            <tbody>                               
            {if isset($error)}
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
			<tr>
				<td class="fieldName">
				    <label for="name_{$id}">Template {$item.language}:</label> 
				</td>
				<td>
				    <input type="text" class="input" name="group_name[{$id}]" id="name_{$id}" value="{if isset($smarty.post.group_name.$id)}{$smarty.post.group_name.$id}{/if}" size="40" title="Max 100 tekens" maxlength="101" onchange="countAndTruncate(this, 100, 'f_540_counter_id');" onkeyup="countAndTruncate(this, 100, 'f_540_counter_id');"/>
				    <span id="f_540_counter_id"></span>
                    
                </td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="description_{$id}">Beschrijving {$item.language}:</label> 
				</td>
				<td>
                    {$FCKeditor.$id}
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_{$id}">Default item {$item.language}:</label>
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($smarty.post.page_description.$id)}{$smarty.post.page_description.$id}{/if}</textarea>
                </td>
            </tr>
            
            {/foreach}
			<tr>
                <td></td>
				<td><br />
				<a href="#" onclick="document.forms[0].submit();"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="" /></a>
				</td>
			</tr>
		</table>
	</form>
    </div>
    <img src="{$SiteRoot}/images/pagina_bottom.jpg" alt="" class="pagina_bottom" />
</div>
{elseif $smarty.get.action == 'overview'} 
    Rijen weergeven :&nbsp;
    <select onchange="window.location='groups.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="groups" cellpadding="2" cellspacing="0">
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 41%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}newsletter/templates.php?action=overview&items={$items}&sort=group_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Groepnaam</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}newsletter/templates.php?action=overview&items={$items}&sort=date_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum toegevoegd</a>
                </td>
                <td class="first-row-last-column">
                    <a href="{$SiteRoot}newsletter/templates.php?action=overview&items={$items}&sort=filled&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Stats</a>
                </td>
            </tr>
             {if !$newsGroup}
            <tr nodrag="true" nodrop="true">
                <td class="first-column-gray">
                    Er zijn nog geen nieuwsgroepen toegevoegd.      
                </td>
                <td class="column-gray">
                    
                </td>
                <td class="last-column-gray">
                    &nbsp;
                </td>
            </tr>
            {/if}
            {foreach from=$newsGroup item=item name="iCounter"}
				
            <tr id="{$item.id}" order_number="{$item.order_number}" ondblclick="window.location.href = '{$SiteRoot}newsletter/templates.php?action=edit&id={$item.id}'" class="pointer">
            
                {if $smarty.foreach.iCounter.iteration % 2 == 1}   
                
                <td class="first-column-gray">
                    {$item.title}
                </td>
                <td class="column-gray">
                </td>
                <td class="last-column-gray">
                </td>
                {else}
                <td class="first-column-white">
                    {$item.title}
                </td>
                <td class="column-white">
                </td>
                <td class="last-column-white">
                </td>
                {/if}
            </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{elseif $smarty.get.action == 'edit'}
<script>
{literal}
var items_1 = 0;
var items_2 = 0;
$ (document).ready (function (){
    
    $ ("#items_1,#items_2").delegate(".remove", "click", function() {
        
        //alert (CKEDITOR.instances[$ (this).attr ('content_id')]);
        
        CKEDITOR.instances[$ (this).attr ('content_id')].destroy();
        $ (this).parent ().parent ().parent ().parent ().parent ().remove ();
    });
    $ ('.additem').click (function (){
        $ ('#' + $ (this).attr ('block-id')).append ('<div class="item"><table style="width:100%;"><tr><td class="fieldName"><label id="items_1_title' + items_1 + '">Titel:</label><a href="javascript:void(0)" class="save-button"  onclick="document.forms[0].action=\'#items_1_title' + items_1 + '\';document.forms[0].submit();" >opslaan</a><br /></td></tr><tr><td class="field"><input class="input" type="text" name="items_1_title[' + items_1 + ']" value="" /> <a class="remove" content_id="items_1_t' + items_1 + '">x</a></td></tr><tr><td class="bar"></td></tr><tr><td class="fieldName"><label id="#items_1_text' + items_1 + '">Text:</label><a href="javascript:void(0)" class="save-button"  onclick="document.forms[0].action=\'#items_1_text' + items_1 + '\';document.forms[0].submit();" >opslaan</a><br /></td></tr><tr><td><textarea id="items_1_t' + items_1 + '" name="items_1_text[' + items_1 + ']"></textarea></td></tr><tr><td class="bar"></td></tr></table><br /></div>');
        CKEDITOR.replace( 'items_1_t' + items_1, {toolbar :'MyToolbar',enterMode : CKEDITOR.ENTER_BR} );
        
        items_1++;
    });
    
    $ ('.additem2').click (function (){
        $ ('#' + $ (this).attr ('block-id')).append ('<div><table style="width:100%;"><tr><td class="fieldName"><label id="items_2_title' + items_2 + '">Titel:</label><a class="save-button"  onclick="document.forms[0].action=\'#items_2_title' + items_2 + '\';document.forms[0].submit();" >opslaan</a><br /></td></tr><tr><td class="field"><input class="input" type="text" name="items_2_title[' + items_2 + ']" value="" /> <a class="remove" content_id="items_2_t' + items_2 + '">x</a></td></tr><tr><td class="bar"></td></tr><tr><td class="fieldName"><label id="items_2_text' + items_2 + '">Text:</label><a class="save-button"  onclick="document.forms[0].action=\'#items_2_text' + items_2 + '\';document.forms[0].submit();" >opslaan</a><br /></td></tr><tr><td class="field"><textarea id="items_2_t' + items_2 + '" name="items_2_text[' + items_2 + ']"></textarea></td></tr><tr><td class="bar"></td></tr></table><br /></div>');
        CKEDITOR.replace( 'items_2_t' + items_2, {toolbar :'MyToolbar',enterMode : CKEDITOR.ENTER_BR} );
        items_2++;
    });
});
{/literal}
</script>
<div id="content_in_content_div">
    <div class="pagina-form modules newsletter">
        <div id="pagina-title" class="pagina-titel">{$folder.text}</div>   
        <div id="content_in_content_div">
            <form method="post" action="" id="templateForm" >
        		<table style="width: 100%;">
                    <tbody>
                    {if isset($error)}
        			<tr>
        				<td>
        					De volgende velden zijn niet (goed) ingevuld: <br />
        					<ol>
        					{foreach from=$error item=value}
        						<li><span style="color: #FF0000;">{$value}</span></li>
        					{/foreach}
        					</ol>
        				</td>
        			</tr>
        			{/if} 
                    <tr>
                        <td class="fieldName">
                            <label id="logo">Logo:</label>
                            <a class="save-button"  onclick="document.forms[0].action='#logo';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=image_logo&value={if isset($private.image_logo) && $private.image_logo == 1}0{else}1{/if}#image_logo">
                                <img src="{$SiteRoot}/images/{if isset($private.image_logo) && $private.image_logo == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_logo" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_logo" href="{$SiteRoot}/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_logo" ></span><br class="clear" />
                            <span id="image_logo">
                            {if isset($newsGroup.data.image_logo)}
                                {foreach from=$newsGroup.data.image_logo item=photoOid}
                                    <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                                    <input name="image_logo[{$photoOid}]" value="{$photoOid}" type="hidden" />
                                {/foreach}
                            {/if}
                            </span><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
                        <td class="fieldName">
                            <label id="mail_logo">Mail Logo:</label>
                            <a class="save-button"  onclick="document.forms[0].action='#mail_logo';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=image_maillogo&value={if isset($private.image_maillogo) && $private.image_maillogo == 1}0{else}1{/if}#image_maillogo">
                                <img src="{$SiteRoot}/images/{if isset($private.image_maillogo) && $private.image_maillogo == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_maillogo" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_maillogo" href="{$SiteRoot}/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_maillogo" ></span><br class="clear" />
                            <span id="image_maillogo">
                            {if isset($newsGroup.data.image_maillogo)}
                                {foreach from=$newsGroup.data.image_maillogo item=photoOid}
                                    <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                                    <input name="image_maillogo[{$photoOid}]" value="{$photoOid}" type="hidden" />
                                {/foreach}
                            {/if}
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_1">Tekst 1:</label>
                            <a class="save-button"  onclick="document.forms[0].action='#text_1';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=text_text1&value={if isset($private.text_text1) && $private.text_text1 == 1}0{else}1{/if}#text_text1">
                                <img src="{$SiteRoot}/images/{if isset($private.text_text1) && $private.text_text1 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    {$FCKeditor.text_text1}
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_1" id="title_1">Titel 2 (#titel_1):</label><a class="save-button"  onclick="document.forms[0].action='#title_1';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=title_1&value={if isset($private.title_1) && $private.title_1 == 1}0{else}1{/if}#title_1">
                                <img src="{$SiteRoot}/images/{if isset($private.title_1) && $private.title_1 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_1" value="{if isset($newsGroup.data.title_1)}{$newsGroup.data.title_1}{/if}" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_2">Tekst 2:</label><a class="save-button"  onclick="document.forms[0].action='#text_2';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=text_text2&value={if isset($private.text_text2) && $private.text_text2 == 1}0{else}1{/if}#text_text2">
                                <img src="{$SiteRoot}/images/{if isset($private.text_text2) && $private.text_text2 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    {$FCKeditor.text_text2}
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_3">Tekst 3:</label><a class="save-button"  onclick="document.forms[0].action='#text_3';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=text_text3&value={if isset($private.text_text3) && $private.text_text3 == 1}0{else}1{/if}#text_text3">
                                <img src="{$SiteRoot}/images/{if isset($private.text_text3) && $private.text_text3 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    {$FCKeditor.text_text3}
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_2" id="title_2">Titel 3 (#titel_2):</label><a class="save-button"  onclick="document.forms[0].action='#title_2';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=title_2&value={if isset($private.title_2) && $private.title_2 == 1}0{else}1{/if}#title_2">
                                <img src="{$SiteRoot}/images/{if isset($private.title_2) && $private.title_2 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_2" value="{if isset($newsGroup.data.title_2)}{$newsGroup.data.title_2}{/if}" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
                        <td class="fieldName">
                            <label id="photo_1">Foto tekst 4:</label><a class="save-button"  onclick="document.forms[0].action='#photo_1';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=image_photo&value={if isset($private.image_photo) && $private.image_photo == 1}0{else}1{/if}#image_photo">
                                <img src="{$SiteRoot}/images/{if isset($private.image_photo) && $private.image_photo == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_photo" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_photo" href="{$SiteRoot}/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_photo"></span><br class="clear" />
                            <span id="image_photo">
                            {if isset($newsGroup.data.image_photo)}
                                {foreach from=$newsGroup.data.image_photo item=photoOid}
                                    <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                                    <input name="image_photo[{$photoOid}]" value="{$photoOid}" type="hidden" />
                                {/foreach}
                            {/if}
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_3" id="title_3">Titel foto tekst 4 (#titel_3):</label><a class="save-button"  onclick="document.forms[0].action='#title_3';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=title_3&value={if isset($private.title_3) && $private.title_3 == 1}0{else}1{/if}#title_3">
                                <img src="{$SiteRoot}/images/{if isset($private.title_3) && $private.title_3 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_3" value="{if isset($newsGroup.data.title_3)}{$newsGroup.data.title_3}{/if}" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_4">Tekst 4:</label><a class="save-button"  onclick="document.forms[0].action='#text_4';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=text_text4&value={if isset($private.text_text4) && $private.text_text4 == 1}0{else}1{/if}#text_text4">
                                <img src="{$SiteRoot}/images/{if isset($private.text_text4) && $private.text_text4 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    {$FCKeditor.text_text4}
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_4" id="title_4">Titel 5 (#titel_4):</label><a class="save-button"  onclick="document.forms[0].action='#title_4';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=title_4&value={if isset($private.title_4) && $private.title_4 == 1}0{else}1{/if}#title_4">
                                <img src="{$SiteRoot}/images/{if isset($private.title_4) && $private.title_4 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_4" value="{if isset($newsGroup.data.title_4)}{$newsGroup.data.title_4}{/if}" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_5">Tekst 5:</label><a class="save-button"  onclick="document.forms[0].action='#text_5';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=text_text5&value={if isset($private.text_text5) && $private.text_text5 == 1}0{else}1{/if}#text_text5">
                                <img src="{$SiteRoot}/images/{if isset($private.text_text5) && $private.text_text5 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    {$FCKeditor.text_text5}
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_5" id="title_5">Titel 6 A (#titel_5):</label><a class="save-button"  onclick="document.forms[0].action='#title_5';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=title_5&value={if isset($private.title_5) && $private.title_5 == 1}0{else}1{/if}#title_5">
                                <img src="{$SiteRoot}/images/{if isset($private.title_5) && $private.title_5 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_5" value="{if isset($newsGroup.data.title_5)}{$newsGroup.data.title_5}{/if}" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
                        <td class="fieldName">
        				    <label id="items_11">Items:</label><a class="save-button"  onclick="document.forms[0].action='#items_11';document.forms[0].submit();" >opslaan</a><a class="additem" block-id="items_1">item toevoegen</a><br /> 
        				</td>
                    </tr>
                    <tr>
                        <td id="items_1">
                            {if isset($newsGroup.data.items_1_title)}
                            {foreach from=$newsGroup.data.items_1_title item=item key=k}
                                <div>
                                    <table style="width:100%;">
                                        <tr>
                                            <td class="fieldName"><label id="items_1_title{$k}">Titel 6 A:</label><a class="save-button"  onclick="document.forms[0].action='#items_1_{$k}';document.forms[0].submit();" >opslaan</a><br /></td>
                                        </tr>
                                        <tr>
                                            <td class="field"><input class="input" type="text" name="items_1_title[{$k}]" value="{$item}" /> <a class="remove" content_id="items_1_t{$k}">x</a></td>
                                        </tr>
                                        <tr>
                                            <td class="bar"></td>
                                        </tr>
                                        <tr>
                                            <td class="fieldName"><label id="items_1_text{$k}">Tekst 6 A:</label><a class="save-button"  onclick="document.forms[0].action='#items_1_text{$k}';document.forms[0].submit();" >opslaan</a><br /></td>
                                        </tr>
                                        <tr>
                                            <td class="field">
                                                <textarea id="items_1_t{$k}" name="items_1_text[{$k}]">{$newsGroup.data.items_1_text.$k}</textarea>
                                                <script>{literal}CKEDITOR.replace( 'items_1_t{/literal}{$k}{literal}', {toolbar :'MyToolbar',enterMode : CKEDITOR.ENTER_BR} );items_1 = {/literal}{$k+1}{literal};{/literal}</script>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bar"></td>
                                        </tr>
                                    </table>
                                    <br />
                                </div>
                                
                            {/foreach}
                            {/if}
                        </td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_6" id="title_6">Titel 6 B (#titel_6):</label><a class="save-button"  onclick="document.forms[0].action='#title_6';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=title_6&value={if isset($private.title_6) && $private.title_6 == 1}0{else}1{/if}#title_6">
                                <img src="{$SiteRoot}/images/{if isset($private.title_6) && $private.title_6 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_6" value="{if isset($newsGroup.data.title_6)}{$newsGroup.data.title_6}{/if}" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_6">Tekst 6 B:</label><a class="save-button"  onclick="document.forms[0].action='#text_6';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=text_text6&value={if isset($private.text_text6) && $private.text_text6 == 1}0{else}1{/if}#text_text6">
                                <img src="{$SiteRoot}/images/{if isset($private.text_text6) && $private.text_text6 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    {$FCKeditor.text_text6}
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_7" id="title_7">Titel 7 (#titel_7):</label><a class="save-button"  onclick="document.forms[0].action='#title_7';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=title_7&value={if isset($private.title_7) && $private.title_7 == 1}0{else}1{/if}#title_7">
                                <img src="{$SiteRoot}/images/{if isset($private.title_7) && $private.title_7 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_7" value="{if isset($newsGroup.data.title_7)}{$newsGroup.data.title_7}{/if}" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
                        <td class="fieldName">
                            <label id="photo_2">Foto 7:</label><a class="save-button"  onclick="document.forms[0].action='#photo_2';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=image_1&value={if isset($private.image_1) && $private.image_1 == 1}0{else}1{/if}#image_1">
                                <img src="{$SiteRoot}/images/{if isset($private.image_1) && $private.image_1 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_1" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_1" href="{$SiteRoot}/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_1" ></span><br class="clear" />
                            <span id="image_1">
                            {if isset($newsGroup.data.image_1)}
                                {foreach from=$newsGroup.data.image_1 item=photoOid}
                                    <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                                    <input name="image_1[{$photoOid}]" value="{$photoOid}" type="hidden" />
                                {/foreach}
                            {/if}
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_7">Tekst 7:</label><a class="save-button"  onclick="document.forms[0].action='#text_7';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=text_text7&value={if isset($private.text_text7) && $private.text_text7 == 1}0{else}1{/if}#text_text7">
                                <img src="{$SiteRoot}/images/{if isset($private.text_text7) && $private.text_text7 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    {$FCKeditor.text_text7}
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_8" id="title_8">Titel 8 (#titel_8):</label><a class="save-button"  onclick="document.forms[0].action='#title_8';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=title_8&value={if isset($private.title_8) && $private.title_8 == 1}0{else}1{/if}#title_8">
                                <img src="{$SiteRoot}/images/{if isset($private.title_8) && $private.title_8 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_8" value="{if isset($newsGroup.data.title_8)}{$newsGroup.data.title_8}{/if}" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
                        <td class="fieldName">
        				    <label id="items_22">Items 2:</label><a class="save-button"  onclick="document.forms[0].action='#items_22';document.forms[0].submit();" >opslaan</a><a class="additem2" block-id="items_2">item toevoegen</a><br /> 
        				</td>
                    </tr>
                    <tr>
                        <td id="items_2">
                            {if isset($newsGroup.data.items_2_title)}
                            {foreach from=$newsGroup.data.items_2_title item=item key=k}
                                <div>
                                    <table style="width:100%;">
                                        <tr>
                                            <td class="fieldName"><label id="items_2_title{$k}">Titel 6 C:</label><a class="save-button"  onclick="document.forms[0].action='#items_2_title{$k}';document.forms[0].submit();" >opslaan</a><br /></td>
                                        </tr>
                                        <tr>
                                            <td class="field"><input class="input" type="text" name="items_2_title[{$k}]" value="{$item}" /> <a class="remove" content_id="items_2_t{$k}">x</a></td>
                                        </tr>
                                        <tr>
                                            <td class="bar"></td>
                                        </tr>
                                        <tr>
                                            <td class="fieldName" id="items_2_text{$k}"><label>Tekst 6 C:</label><a class="save-button"  onclick="document.forms[0].action='#items_2_text{$k}';document.forms[0].submit();" >opslaan</a><br /></td>
                                        </tr>
                                        <tr>
                                            <td class="field">
                                                <textarea id="items_2_t{$k}" name="items_2_text[{$k}]">{$newsGroup.data.items_2_text.$k}</textarea>
                                                <script>{literal}CKEDITOR.replace( 'items_2_t{/literal}{$k}{literal}', {toolbar :'MyToolbar',enterMode : CKEDITOR.ENTER_BR} );items_2 = {/literal}{$k+1}{literal};{/literal}</script>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bar"></td>
                                        </tr>
                                    </table>
                                    <br />
                                </div>
                            {/foreach}
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldName">
                            <label id="photo_3">Foto:</label><a class="save-button"  onclick="document.forms[0].action='#photo_3';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=image_2&value={if isset($private.image_2) && $private.image_2 == 1}0{else}1{/if}#image_2">
                                <img src="{$SiteRoot}/images/{if isset($private.image_2) && $private.image_2 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_2" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_2" href="{$SiteRoot}/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_2" ></span><br class="clear" />
                            <span id="image_2">
                            {if isset($newsGroup.data.image_2)}
                                {foreach from=$newsGroup.data.image_2 item=photoOid}
                                    <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                                    <input name="image_2[{$photoOid}]" value="{$photoOid}" type="hidden" />
                                {/foreach}
                            {/if}
                            </span>
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_9" id="title_9">Titel 9 (#titel_9):</label><a  class="save-button"  onclick="document.forms[0].action='#title_9';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=title_9&value={if isset($private.title_9) && $private.title_9 == 1}0{else}1{/if}#title_9">
                                <img src="{$SiteRoot}/images/{if isset($private.title_9) && $private.title_9 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_9" value="{if isset($newsGroup.data.title_9)}{$newsGroup.data.title_9}{/if}" />
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_8">Tekst 8:</label><a class="save-button"  onclick="document.forms[0].action='#text_8';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=text_text8&value={if isset($private.text_text8) && $private.text_text8 == 1}0{else}1{/if}#text_text8">
                                <img src="{$SiteRoot}/images/{if isset($private.text_text8) && $private.text_text8 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    {$FCKeditor.text_text8}
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="f_id_2" id="text_9">Tekst 9:</label><a class="save-button"  onclick="document.forms[0].action='#text_9';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=text_text8&value={if isset($private.text_text9) && $private.text_text9 == 1}0{else}1{/if}#text_text9">
                                <img src="{$SiteRoot}/images/{if isset($private.text_text9) && $private.text_text9 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    {$FCKeditor.text_text9}
        				</td>
        			</tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_10" id="title_10">Titel 10 (#titel_10):</label><a  class="save-button"  onclick="document.forms[0].action='#title_10';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=title_10&value={if isset($private.title_10) && $private.title_10 == 1}0{else}1{/if}#title_10">
                                <img src="{$SiteRoot}/images/{if isset($private.title_10) && $private.title_10 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_10" value="{if isset($newsGroup.data.title_10)}{$newsGroup.data.title_10}{/if}" />
        				</td>
        			</tr>
                    <tr>
                        <td class="fieldName">
                            <label id="footer_1">Footer 1:</label><a class="save-button"  onclick="document.forms[0].action='#footer_1';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=image_f1&value={if isset($private.image_f1) && $private.image_f1 == 1}0{else}1{/if}#image_f1">
                                <img src="{$SiteRoot}/images/{if isset($private.image_f1) && $private.image_f1 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_f1" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_f1" href="{$SiteRoot}/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_f1" ></span><br class="clear" />
                            <span id="image_f1">
                            {if isset($newsGroup.data.image_f1)}
                                {foreach from=$newsGroup.data.image_f1 item=photoOid}
                                    <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                                    <input name="image_f1[{$photoOid}]" value="{$photoOid}" type="hidden" />
                                {/foreach}
                            {/if}
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title_11" id="title_11">Titel 11 (#titel_11):</label><a  class="save-button"  onclick="document.forms[0].action='#title_11';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=title_11&value={if isset($private.title_11) && $private.title_11 == 1}0{else}1{/if}#title_11">
                                <img src="{$SiteRoot}/images/{if isset($private.title_11) && $private.title_11 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input class="input" type="text" name="title_11" value="{if isset($newsGroup.data.title_11)}{$newsGroup.data.title_11}{/if}" />
        				</td>
        			</tr>
                    <tr>
                        <td class="fieldName">
                            <label id="footer_2">Footer 2:</label><a class="save-button"  onclick="document.forms[0].action='#footer_2';document.forms[0].submit();" >opslaan</a>
                            <a class="private-button" href="{$SiteRoot}newsletter/templates.php?action=private&id={$newsGroup.id}&field=image_f2&value={if isset($private.image_f2) && $private.image_f2 == 1}0{else}1{/if}#image_f2">
                                <img src="{$SiteRoot}/images/{if isset($private.image_f2) && $private.image_f2 == 1}disable{else}enable{/if}.gif" alt="" />
                            </a><br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <ul class="photo-menu">
                                <li>
                                    <a class="greybox" area="image_f2" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                                </li>
                                <li class="last">
                                    <a class="greybox browser" area="image_f2" href="{$SiteRoot}/browser.php">bladeren</a>
                                </li>
                            </ul>
                            <span id="status_image_f2" ></span><br class="clear" />
                            <span id="image_f2">
                            {if isset($newsGroup.data.image_f2)}
                                {foreach from=$newsGroup.data.image_f2 item=photoOid}
                                    <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                                    <input name="image_f2[{$photoOid}]" value="{$photoOid}" type="hidden" />
                                {/foreach}
                            {/if}
                            </span>
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="bar"></td>
                    </tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="title">Template:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="title" id="title" value="{if isset($newsGroup.title)}{$newsGroup.title}{/if}" size="40" title="Max 100 tekens" maxlength="101" />
        				</td>
        			</tr>
        			{if isset($send_test)}
                    <tr>
        				<td class="fieldName" colspan="2">
        				    Test verzenden Succed
                        </td>
        			</tr>
                    {/if}
                    <tr>
        				<td class="fieldName">
        				    <label for="subject">Onderwerp:</label>&nbsp;&nbsp;&nbsp;(Mag niet meer dan 78 tekens bedragen)
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="subject" id="subject" value="{if isset($smarty.post.subject)}{$smarty.post.subject}{else}Test{/if}" size="40"  maxlength="101" /> 
        				</td>
        			</tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="fname">Van naam:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="send_fname" id="fname" value="{if isset($smarty.post.send_fname)}{$smarty.post.send_fname}{else}Inforitus{/if}" size="40"  maxlength="101" />{if isset($smarty.post.send) && isset($send_v) && $send_v.name == false}<span style="color:red;">* Verplicht</span>{/if}
        				</td>
        			</tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="femail">Van email:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="send_femail" id="femail" value="{if isset($smarty.post.send_femail)}{$smarty.post.send_femail}{else}info@inforitus.nl{/if}" size="40"  maxlength="101" />{if isset($smarty.post.send) && isset($send_v) && $send_v.email == false}<span style="color:red;">* Verplicht</span>{/if}
        				</td>
        			</tr>             
                    <tr>
        				<td class="fieldName">
        				    <label for="name">Naam:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="send_name" id="name" value="{if isset($smarty.post.send_name)}{$smarty.post.send_name}{/if}" size="40"  maxlength="101" />
        				</td>
        			</tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="email">Email:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="send_email" id="email" value="{if isset($smarty.post.send_email)}{$smarty.post.send_email}{/if}" size="40"  maxlength="101" />
        			    </td>
        			</tr>
                    <tr>
        				<td class="fieldName">
        				    <label for="gender">Geslacht:</label> 
        				</td>
                    </tr>
                    <tr>
        				<td class="field">
        				    <input type="text" class="input" name="send_gender" id="gender" value="{if isset($smarty.post.send_gender)}{$smarty.post.send_gender}{/if}" size="40"  maxlength="101" /><br />
        				    <input type="submit" name="send_test" value="Test verzenden" class="send_test" />
                            
                        </td>
        			</tr>
                    <tr>
                        <td class="fieldName">
                            <label for="group_id">Group subscriber:</label> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <select name="group_id" id="group_id">
                                {foreach from=$groups item=value}
                                    <option value="{$value.id}" {if isset($smarty.post.group_id) && $smarty.post.group_id == $value.id} selected="selected" {/if} >{$value.group_name|stripslashes}</option>
                                {/foreach}
                            </select><br />
                        </td>
                    </tr>
                    {if isset($links)}
                    <tr>
                        <td class="fieldName">
                            <label for="links">Links:</label> 
                        </td>
                    </tr>
                    <tr>
                        <td class="field links">
                            {foreach from=$links item=value key=k}
                                {if '{unsubscribe}' != $value|replace:'href="':''|replace:'"':''}
                            <input type="checkbox" name="links[{$k}]" value="{$value|replace:'href="':''|replace:'"':''}" {if isset($smarty.post.links) && $value|in_array:$smarty.post.links} selected="selected" {/if} /> {$value|replace:'href="':''|replace:'"':''} -> {$texts.$k}<br />
                                {/if}
                            {/foreach}
                            <input type="submit" name="send" value="Verzenden" class="send" /><br />
                            <div id="progressbar"></div>
                        </td>
                    </tr>
                    {/if}
                    <tr>
        				<td class="field">
                            <br />
                            <input type="hidden" id="module"  value="newsletter" name="module"  />
                            <input type="hidden" id="template"  value="{$newsGroup.id}" name="module"  />
        				    <a href="#" onclick="document.forms[0].submit();" id="save"></a>
                            <a class="verwijderen" href="{$SiteRoot}/newsletter/templates.php?action=delete&amp;id={$newsGroup.id}" onclick="return confirm('Weet u zeker dat u deze nieuwsgroep wilt verwijderen?'); return false;"></a>
                        </td>
        			</tr>
                    <tr>
                        <td class="fieldName">
                            Template naam:
                        </td>
                    </tr>
                    <tr>
                        <td class="field">
                            <input type="text" name="template_name" value="" class="input" />
                        </td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="new_template" class="new-template" value="Opslaan als nieuw template" /></td>
                    </tr>
        		</table>
       	     </form>
             
        </div>
        <img src="{$SiteRoot}/images/pagina_bottom.jpg" alt="" class="pagina_bottom" />
    </div>
    <div class="pagina-form modules template" style="width:600px">
        <div id="pagina-title" class="pagina-titel" style="width:576px;">{$folder.text} <a href="/mail/{$newsGroup.id}.html" target="_blank">Klik hier om het template los in de browser te bekijken</a></div>   
        <div id="content_in_content_div" style="margin:0px;padding:0px;">
            <iframe style="border:none;" src="/mail/{$newsGroup.id}.html" width="600" height="3500"></iframe> 
        </div>
    </div>
</div>
<script>{literal}
$ (document).ready (function (){
    setTimeout("Jump()",1000);
});
function Jump() {
    if (window.location.hash != '')
        location.href=window.location.hash;
}
{/literal}
</script>
{/if}
