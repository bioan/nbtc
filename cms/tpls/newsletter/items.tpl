{strip}
{if $smarty.get.action == 'new'}
<div class="pagina-form modules">
    <div id="pagina-title" class="pagina-titel">{$folder.text}</div>   
    <div id="content_in_content_div">
    <form method="post" action="" >
        <table style="width: 100%;">
            <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="2">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName">
                    <label>Afbeelding</label>: 
                </td>
                <td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status" ></span><br class="clear" />
                    <span id="photos">
                    {if isset($smarty.post.photos)}
                        {foreach from=$smarty.post.photos item=photoOid}
                            <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                            <input name="photos[{$photoOid}]" value="{$photoOid}" type="hidden" />
                        {/foreach}
                    {/if}
                    </span>
                    
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="link">Link</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="link" id="link" value="{if isset($smarty.post.link)}{$smarty.post.link}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="group_id">Template</label>: 
                </td>
                <td>
                    <select name="group_id" id="group_id">
                        {foreach from=$templates item=value}
                            <option value="{$value.id}" {if isset($smarty.post.group_id) && $smarty.post.group_id == $value.id} selected="selected" {/if} >{$value.group_name|stripslashes}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td class="fieldName" style="width:200px;">
                    <label for="title">Titel</label>: 
                </td>
                <td style="width:200px;">
                    <input type="text" class="input" name="title" id="title" value="{if isset($smarty.post.title)}{$smarty.post.title}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label>Bericht </label>: 
                </td>
                <td>
                    {$FCKeditor}
                </td>
            </tr>
            
            <tr>
                <td></td>
                <td>
                    <br />
                    <input type="hidden" id="module"  value="newsletter" name="module"  />
                    <a href="#" onclick="document.forms[0].submit();" class="save"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="" /></a>
                    <input type="submit" name="test" value="Test" />
                </td>
            </tr>
            </tbody>
        </table>
    </form>
    </div>
    <img src="{$SiteRoot}/images/pagina_bottom.jpg" alt="" class="pagina_bottom" />
</div>
<div class="pagina-form modules template">
    <div id="pagina-title" class="pagina-titel">{$folder.text}</div>   
    <div id="content_in_content_div">
        {if isset($template)}{$template}{/if}
    </div>
    <img src="{$SiteRoot}/images/pagina_bottom.jpg" alt="" class="pagina_bottom" />
</div>
{elseif $smarty.get.action == 'overview'}
    Rijen weergeven :&nbsp;
    <select onchange="window.location='items.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="newsletter" cellpadding="2" cellspacing="0">
        <colgroup style="width: 33%"></colgroup>
        <colgroup style="width: 33%"></colgroup>
        <colgroup style="width: 33%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}newsletter/items.php?action=overview&items={$items}&sort=title&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Titel</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}newsletter/items.php?action=overview&items={$items}&sort=group_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Template</a>
                </td>
                <td class="first-row-last-column">
                    <a href="{$SiteRoot}newsletter/items.php?action=overview&items={$items}&sort=date_register_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum toegevoegd</a>
                </td>
            </tr>
            {if !$photos}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                    Er zijn nog geen nieuws items gepost.    
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$photos item=item name="iCounter"}
                
                <tr id="{$item.id}" order_number="{$item.order_number}" ondblclick="window.location.href = '{$SiteRoot}newsletter/items.php?action=edit&id={$item.id}'" class="pointer">
                    {if $smarty.foreach.iCounter.iteration % 2 == 1} 
                    <td class="first-column-gray">
                        {$item.title|stripslashes}
                    </td>
                    <td class="column-gray">
                        {$item.group_name}
                    </td>
                    <td class="last-column-gray">
                        {$item.date_register_format}
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$item.title|stripslashes}
                    </td>
                    <td class="column-white">
                        {$item.group_name}
                    </td>
                    <td class="last-column-white">
                        {$item.date_register_format}
                    </td>
                    {/if}
                </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{elseif $smarty.get.action == 'edit'}
<div class="pagina-form modules">
    <div id="pagina-title" class="pagina-titel">{$folder.text}</div>   
    <div id="content_in_content_div">
    <form method="post" action="" >
        <table style="width: 100%;">
            <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="3">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName">
                    <label>Afbeelding</label>: 
                </td>
                <td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status" ></span><br class="clear" />
                    <span id="photos">
                    {if isset($smarty.post.photos)}
                        {foreach from=$smarty.post.photos item=photoOid}
                            <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                            <input name="photos[{$photoOid}]" value="{$photoOid}" type="hidden" />
                        {/foreach}
                    {elseif $values.photos != ''}
                    {foreach from=$values.photos item=photo}
                        <img id="{$photo.id}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photo.id}/100/image.php" />
                        <input name="photos[{$photo.id}]" value="{$photo.id}" type="hidden" />
                    {/foreach}
                    {/if}
                    </span>
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="link">Link</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="link" id="link" value="{if !isset($smarty.post.link)}{$values.link}{else}{$smarty.post.link}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="group_id">Template</label>: 
                </td>
                <td>
                    <select name="group_id" id="group_id">
                        {foreach from=$templates item=value}
                            <option value="{$value.id}" {if (isset($smarty.post.group_id) && $smarty.post.group_id == $value.id) || (!isset($smarty.post.group_id) && $values.group_id == $value.id)} selected="selected" {/if}>{$value.group_name|stripslashes}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="title">Titel</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="title" id="title" value="{if isset($smarty.post.title)}{$smarty.post.title}{else}{$values.title}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="f_id_2">Bericht</label>: 
                </td>
                <td>
                    {$FCKeditor}
                </td>
            </tr>
            
            <tr>
                <td></td>
                <td>
                    <br />
                    <input type="hidden" id="module"  value="newsletter" name="module"  />
                    <input class="button" type="submit" name="submit" id="save" value="" />
                    <input type="submit" name="test" value="Test" />
                    <a class="verwijderen" href="{$SiteRoot}/newsletter/items.php?action=delete&amp;id={$values.id}" onclick="return confirm('Weet u zeker dat u dit nieuws item wilt verwijderen?'); return false;"></a>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
    </div>
    <img src="{$SiteRoot}/images/pagina_bottom.jpg" alt="" class="pagina_bottom" />
</div>
<div class="pagina-form modules template">
    <div id="pagina-title" class="pagina-titel">{$folder.text}</div>   
    <div id="content_in_content_div">
        {if isset($template)}{$template}{/if}
    </div>
    <img src="{$SiteRoot}/images/pagina_bottom.jpg" alt="" class="pagina_bottom" />
</div>
{else}
    Deze pagina is niet helemaal ok!
{/if}
{/strip}
