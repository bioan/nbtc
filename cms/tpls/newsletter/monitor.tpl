{if $smarty.get.action == 'overview' && !isset($smarty.get.template_id) && !isset($smarty.get.id)} 
    Rijen weergeven :&nbsp;
    <select onchange="window.location='groups.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="groups" cellpadding="2" cellspacing="0">
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 41%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}newsletter/monitor.php?action=overview&items={$items}&sort=group_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Template</a>
                </td>
                <td class="first-row-column-white">
                    
                </td>
                <td class="first-row-last-column">
                    
                </td>
            </tr>
             {if !$newsGroup}
            <tr nodrag="true" nodrop="true">
                <td class="first-column-gray">
                    Er zijn nog geen nieuwsgroepen toegevoegd.      
                </td>
                <td class="column-gray">
                    
                </td>
                <td class="last-column-gray">
                    &nbsp;
                </td>
            </tr>
            {/if}
            {foreach from=$newsGroup item=item name="iCounter"}
				
            <tr id="{$item.id}" ondblclick="window.location.href = '{$SiteRoot}newsletter/monitor.php?action=overview&template_id={$item.template_id}{if $item.count_sends == 1}&id={$item.id}{/if}'" class="pointer">
            
                {if $smarty.foreach.iCounter.iteration % 2 == 1}   
                
                <td class="first-column-gray">
                    {$item.group_name}
                </td>
                <td class="column-gray">
                    
                </td>
                <td class="last-column-gray">
                    
                </td>
                {else}
                <td class="first-column-white">
                    {$item.group_name}
                </td>
                <td class="column-white">
                    
                </td>
                <td class="last-column-white">
                    
                </td>
                {/if}
            </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{elseif isset($smarty.get.template_id) && !isset($smarty.get.id)}
    Rijen weergeven :&nbsp;
    <select onchange="window.location='groups.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}&template_id={$smarty.get.template_id}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="groups" cellpadding="2" cellspacing="0">
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 41%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}newsletter/monitor.php?action=overview&template_id={$smarty.get.template_id}&items={$items}&sort=date&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}newsletter/monitor.php?action=overview&template_id={$smarty.get.template_id}&items={$items}&sort=group_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Groepnaam</a>
                </td>
                <td class="first-row-last-column">
                    
                </td>
            </tr>
             {if !$newsGroup}
            <tr nodrag="true" nodrop="true">
                <td class="first-column-gray">
                    Er zijn nog geen nieuwsgroepen toegevoegd.      
                </td>
                <td class="column-gray">
                    
                </td>
                <td class="last-column-gray">
                    &nbsp;
                </td>
            </tr>
            {/if}
            {foreach from=$newsGroup item=item name="iCounter"}
				
            <tr id="{$item.id}" ondblclick="window.location.href = '{$SiteRoot}newsletter/monitor.php?action=overview&template_id={$smarty.get.template_id}&id={$item.id}'" class="pointer">
            
                {if $smarty.foreach.iCounter.iteration % 2 == 1}   
                
                <td class="first-column-gray">
                    {$item.date_register_format}
                </td>
                <td class="column-gray">
                    {$item.subscribers}
                </td>
                <td class="last-column-gray">
                    
                </td>
                {else}
                <td class="first-column-white">
                    {$item.date_register_format}
                </td>
                <td class="column-white">
                    {$item.subscribers}
                </td>
                <td class="last-column-white">
                    
                </td>
                {/if}
            </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{else}
    Rijen weergeven :&nbsp;
    <select onchange="window.location='groups.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}&template_id={$smarty.get.template_id}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select><a href="{$SiteRoot}newsletter/monitor.php?action=overview&template_id={$smarty.get.template_id}&id={$smarty.get.id}&items={$items}{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}&export=1"><img src="../images/excel.gif" alt="" /></a>
    <table style="width: 100%;" id="t1" class="groups" cellpadding="2" cellspacing="0">
        <colgroup style="width: 12%"></colgroup>
        {foreach from=$template.links item=item key=k}
        <colgroup style="width: {88/$template.links|count}%"></colgroup>
        {/foreach}
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="{$template.links|count+1}" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}newsletter/monitor.php?action=overview&template_id={$smarty.get.template_id}&id={$smarty.get.id}&items={$items}&sort=date&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum</a>
                </td>
                {foreach from=$template.links item=item key=k}
                <td class="{if $k != $template.links|count-1}first-row-column-white{else}first-row-last-column{/if}">
                    <input type="text" id="l{$k}" value="{if $item.name != ''}{$item.name}{else}{$item.link}{/if}" /><a onclick="save_link({$k}, {$smarty.get.id})">save</a>&nbsp;<a href="{$SiteRoot}newsletter/monitor.php?action=overview&template_id={$smarty.get.template_id}&id={$smarty.get.id}&items={$items}&sort={$k}&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">sort</a>
                </td>
                {/foreach}
            </tr>
             {if !$monitor}
            <tr nodrag="true" nodrop="true">
                <td class="first-column-gray">
                    Er zijn nog geen nieuwsgroepen toegevoegd.      
                </td>
                {foreach from=$template.links item=item key=k}
                <td class="{if $k != $template.links|count}column-gray{else}last-column-gray{/if}">
                </td>
                {/foreach}
            </tr>
            {/if}
            {foreach from=$monitor item=item name="iCounter"}
				
            <tr id="{$item.id}" ondblclick="window.location.href = '{$SiteRoot}newsletter/monitor.php?action=overview&template_id={$smarty.get.template_id}&id={$item.id}'" class="pointer">
            
                {if $smarty.foreach.iCounter.iteration % 2 == 1}   
                
                <td class="first-column-gray">
                    {$item.email}
                </td>
                {foreach from=$item.values item=values key=k}
                <td class="{if $k != $item.values|count-1}column-gray{else}last-column-gray{/if}">
                    {$values}    
                </td>
                {/foreach}
                {else}
                <td class="first-column-white">
                    {$item.email}
                </td>
                {foreach from=$item.values item=values key=k}
                <td class="{if $k != $item.values|count-1}column-white{else}last-column-white{/if}">
                    {$values}   
                </td>
                {/foreach}
                {/if}
            </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                {foreach from=$template.links item=item key=k}
                <td class="{if $k != $template.links|count-1}last-row-column-white{else}last-row-last-column{/if}">
                </td>
                {/foreach}
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{/if}
