{strip}
{if $smarty.get.action == 'new'} 
    <form method="post" action="" enctype="multipart/form-data">
		<table style="width: 100%;">
		    <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
			<tr>
				<td class="fieldName">
				    <label for="category">Categorie</label>: 
				</td>
				<td>
				    <select name="category" id="category" onchange="checkIfDisabled(this);">
                        {if isset($nodes)}
                            {foreach from=$nodes item=node}
                                {if $node.name != '_root'}
                                {if $node.oid|in_array:$leafNodes && $node.oid != 0}
                                    <option value="{$node.oid}" {if isset($smarty.post.category) && $smarty.post.category == $node.oid} selected="selected" {/if}>{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                                {else}
                                    <option value="{$node.oid}" disabled="disabled" style="color: #CCCCCC;">{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                                {/if}
                                {/if}
                            {/foreach}
                        {else}
                            <option value="0">Er zijn geen categorie&euml;n aangemaakt.</option>
                        {/if}
                    </select>
				</td>
			</tr>
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
            
            <tr>
				<td class="fieldName">
				    <label>Foto</label>: 
				</td>
				<td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_{$id}"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_{$id}" ></span><br class="clear" />
				    <span id="photos_{$id}">
                    {if isset($smarty.post.photos.$id)}
                        {foreach from=$smarty.post.photos.$id item=photoOid}
                            <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                            <input name="photos[{$id}][{$photoOid}]" value="{$photoOid}" type="hidden" />
                        {/foreach}
                    {/if}
                    </span>
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="title_{$id}">Titel {$item.language}</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="title[{$id}]" value="{if isset($smarty.post.title.$id)}{$smarty.post.title.$id}{/if}" id="title_{$id}" size="40" />
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="f_id_2">Bericht {$item.language}</label>: 
				</td>
				<td>
				    {$FCKeditor.$id}
				</td>
			</tr>
			<tr>
                <td class="fieldName">
                    <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($smarty.post.page_description.$id)}{$smarty.post.page_description.$id}{/if}</textarea>
                </td>
            </tr>
            {/foreach}
            {if isset($keywords)}
            <tr>
                <td class="fieldName">
                    Keywords    
                </td>
                <td>
                    {foreach from=$keywords item=item key=k}
                    {$k} - {$item}<br />
                    {/foreach}
                </td>
            </tr>
            {/if}
			<tr>
                <td></td>
				<td>
                    <br />
                    <input type="hidden" id="module"  value="photoalbum" name="module"  />
				    <input type="image" src="{$SiteRoot}/images/save.png">
				</td>
			</tr>
          </tbody>
       </table>
    </form>
    
{elseif $smarty.get.action == 'overview'} 
    <script type="text/javascript">
 	/* <![CDATA[ */ {literal}
  	function showHide(obj)
  	{
  		var item = obj.parentNode.nextSibling.nextSibling;
		if (item.style.display == 'none') {
			item.style.display = 'block';
		} else {
			item.style.display = 'none';
		}
	}{/literal}
 	/* ]]> */
 	</script>
    Rijen weergeven :&nbsp;
    <select onchange="window.location='photoalbum.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="photoalbum" cellpadding="2" cellspacing="0">
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 30%"></colgroup>
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 20%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="4" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}photoalbum/photoalbum.php?action=overview&items={$items}&sort=title&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Naam</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}photoalbum/photoalbum.php?action=overview&items={$items}&sort=category_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Categorie</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}photoalbum/photoalbum.php?action=overview&items={$items}&sort=date&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Project datum</a>
                </td>
                <td class="first-row-last-column">
                    <a href="{$SiteRoot}photoalbum/photoalbum.php?action=overview&items={$items}&sort=filled&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Stats</a>
                </td>
            </tr>
            {if !$photos}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                        Er zijn nog geen photoalbum items gepost.          
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$photos item=photo name="iCounter"}
    			<tr id="{$photo.id}" order_number="{$photo.order_number}" ondblclick="window.location.href = '{$SiteRoot}photoalbum/photoalbum.php?action=edit&id={$photo.id}'" class="pointer" >
                    {if $smarty.foreach.iCounter.iteration % 2 == 1}   
    				<td class="first-column-gray">
    				    {$photo.title|stripslashes}
    				</td>
    				<td class="column-gray">
    				    {$photo.category_name|stripslashes}
    				</td>
    				<td class="column-gray">
    				    {$photo.date_format}
    				</td>
                    <td class="last-column-gray">
    				    <span class="{if $photo.filled == $languages|@count}green{else}red{/if}">{$photo.filled} / {$languages|@count}</span>
    				</td>
                    {else}
                    <td class="first-column-white">
                        {$photo.title|stripslashes}
                    </td>
                    <td class="column-white">
                        {$photo.category_name|stripslashes}
                    </td>
                    <td class="column-white">
                        {$photo.date_format}
                    </td>
                    <td class="last-column-white">
                        <span class="{if $photo.filled == $languages|@count}green{else}red{/if}">{$photo.filled} / {$languages|@count}</span>
                    </td>
                    {/if}
    			</tr>	
        	{/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>     	
        </tbody>
    </table>
       <div style="padding-top: 0.6em; text-align: center;">
		{$links}
	</div>
{elseif $smarty.get.action == 'edit'}  
    <form method="post" action="" enctype="multipart/form-data">
		<table style="width: 100%;">
		    <colgroup style="width: 10%"></colgroup>
            <colgroup style="width: 90%"></colgroup>
            <tbody>
            {if isset($error)}
			<tr>
				<td colspan="2">
					De volgende velden zijn niet (goed) ingevuld: <br />
					<ol>
					{foreach from=$error item=value}
						<li><span style="color: #FF0000;">{$value}</span></li>
					{/foreach}
					</ol>
				</td>
			</tr>
			{/if}
            <tr>
				<td class="fieldName">
				    <label for="category">Categorie</label>: 
				</td>
				<td>
				    <select name="category" id="category" onchange="checkIfDisabled(this);">
				        {if isset($nodes)}
                            {foreach from=$nodes item=node}
                                {if $node.name != '_root'}
                                {if $node.oid|in_array:$leafNodes && $node.oid != 0}
                                    <option value="{$node.oid}" {if $node.oid == $photo.category_id} selected="selected" {/if}>{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                                {else}
                                    <option value="{$node.oid}" disabled="disabled" style="color: #CCCCCC;">{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                                {/if}
                                {/if}
                            {/foreach}
                        {else}
                            <option value="0">Er zijn geen categorie&euml;n aangemaakt.</option>
                        {/if}
                    </select>
				</td>
			</tr>
			{foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
            
            <tr>
				<td class="fieldName">
				    <label>Afbeelding</label>: 
				</td>
				<td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_{$id}"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_{$id}" ></span><br class="clear" />
					<span id="photos_{$id}">
                    {if !empty($photo.$id.photos)}
                    {foreach from=$photo.$id.photos item=photos}
                    	<img id="{$photos.id}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photos.id}/100/image.php" />
						<input name="photos[{$id}][{$photos.id}]" value="{$photos.id}" type="hidden" />
					{/foreach}
                    {/if}
                    </span>
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label for="title_{$id}">Titel {$item.language}</label>: 
				</td>
				<td>
				    <input type="text" class="input" name="title[{$id}]" value="{if isset($photo.$id.title)}{$photo.$id.title}{/if}" id="title_{$id}" size="40" />
				</td>
			</tr>
			<tr>
				<td class="fieldName">
				    <label>Omschrijving {$item.language}</label>: 
				</td>
				<td>
				    {$FCKeditor.$id}
				</td>
			</tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($photo.$id.page_description)}{$photo.$id.page_description}{/if}</textarea>
                </td>
            </tr>
            {/foreach}
            {if isset($keywords)}
            <tr>
                <td class="fieldName">
                    Keywords    
                </td>
                <td>
                    {foreach from=$keywords item=item key=k}
                    {$k} - {$item}<br />
                    {/foreach}
                </td>
            </tr>
            {/if}
			<tr>
                <td></td>
				<td> 
                    <br />
                    <input type="hidden" id="module"  value="photoalbum" name="module"  />
				    <input type="submit" src="" value="" id="save"> 
                    <a class="verwijderen" href="{$SiteRoot}/photoalbum/photoalbum.php?action=delete&amp;id={$photo.id}" onclick="return confirm('Wilt u deze afbeelding verwijderen?!');"></a>
			        <input type="submit" class="keywords" value="" name="keywords"  />  
            	</td>
			</tr>
		</table>
	</form>
{/if}
{/strip}
