<script type="text/javascript">
/* <![CDATA[ */{literal}
	function checkIfDisabled(obj)
	{
		if (obj.options[obj.selectedIndex].disabled == true) {
			alert('Deze categorie kan niet gekozen worden omdat deze midden in de tree zit!');	
			for (var i =0; i < obj.options.length; i++) {
				if (!obj.options[i].disabled) {
					obj.options[i].selected = true;
					return;
				}
			}
			
		}
	}
	
  {/literal}
/* ]]> */
</script>
{if $smarty.get.action == 'new'}
    <form method="post" action="" >
        <table style="width: 100%;">
            <colgroup style="width: 30%"></colgroup>
            <colgroup style="width: 70%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="2">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName">
                     <label for="country_name">Country name</label>:
                </td>
                <td>
                     <input type="text" class="input" name="country_name" id="country_name" autocomplete="off" size="40" value="" />
                 </td>
            </tr>
            <tr>
                <td class="fieldName">
                     <label for="pass">Change country password</label>:
                </td>
                <td>
                     <input type="password" class="input" name="country_pass" id="country_pass" autocomplete="off" size="40" value="" />
                 </td>
            </tr>
            <tr>
                <td class="fieldName">
                     <label for="country_pass_conf">Confirm country password</label>:
                </td>
                <td>
                     <input type="password" class="input" name="country_pass_conf" id="pass_conf" size="40" value="" />
                 </td>
            </tr>

            <tr>
                <td></td>
                <td>
                    <input type="hidden" id="module"  value="country" name="module"  />
                    <br />
                    <a href="#" onclick="document.forms[0].submit();"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="" /></a>
                </td>
            </tr>
        </table>
    </form>

{elseif $smarty.get.action == 'overview'}  
    <ul class="tree">
        <li class="first-child">
            <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
            <h2 class="font">Overzicht</h2>
            <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
        </li>
        <li class="fields">
            <img src="{$SiteRoot}/images/first_row_left_white.jpg" class="left" alt="" />
            <div style="width:30%"><a href="{$SiteRoot}photoalbum/cats.php?action=overview&sort=name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Countries</a></div>
            <img src="{$SiteRoot}/images/first_row_right_white.jpg" class="right" alt="" />
        </li>
    </ul>
    <ul class="tree" id="tmenu">
        {if !$existsCats}
        <li class="gray">
            <img src="{$SiteRoot}/images/first_column_gray_left.jpg" class="left" alt="" />
            <div style="width:30%">Er zijn nog geen categorie&euml;n aangemaakt</div>
            <img src="{$SiteRoot}/images/last_column_gray_right.jpg" class="right" alt="" /> 
        </li>
        {/if}
        {foreach from=$parents item=item name="iCounter" key=k}
			<li class="pointer {if $smarty.foreach.iCounter.iteration % 2 == 1}gray{else}white{/if}">
                <div class="row" ondblclick="window.location.href = '{$SiteRoot}photoalbum/cats.php?action=edit&id={$item.id}'">
                    <img src="{$SiteRoot}/images/first_column_gray_left.jpg" class="left" alt="" />
                    <div style="width:30%">{$item.name|stripslashes}</div>
                    <img src="{$SiteRoot}/images/last_column_gray_right.jpg" class="right" alt="" />
                </div>
            </li>   
        {/foreach}
    </ul>
    <ul class="tree">
        <li class="bottom">
            <img src="{$SiteRoot}/images/last_row_left.jpg" class="left" alt="" /><img src="{$SiteRoot}/images/last_row_right.jpg" class="right" alt="" />
            <div style="width:30%;"></div>        
        </li>
    </ul>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{elseif $smarty.get.action == 'edit'}
     <form method="post" action="" >
    	<table style="width: 100%;">
    	    <colgroup style="width: 30%"></colgroup>
            <colgroup style="width: 70%"></colgroup>
            <tbody>
            {if isset($error)}
    		<tr>
    			<td colspan="2">
    				De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
    			</td>
    		</tr>
    		{/if}
            <tr>
    			<td class="fieldName">
    			     <label for="country_name">Country name</label>:
    			</td>
    			<td>
    			     <input type="text" class="input" name="country_name" id="country_name" size="40" value="{if isset($country.name)}{$country.name}{/if}" />
    			 </td>
            </tr>
            <tr>
                <td class="fieldName">
                     <label for="pass">Change country password</label>:
                </td>
                <td>
                     <input type="password" class="input" name="country_pass" id="country_pass" size="40" value="" />
                 </td>
            </tr>
            <tr>
                <td class="fieldName">
                     <label for="country_pass_conf">Confirm country password</label>:
                </td>
                <td>
                     <input type="password" class="input" name="country_pass_conf" id="pass_conf" size="40" value="" />
                 </td>
            </tr>

            <tr>
                <td></td>
    			<td>
                    <input type="hidden" id="module"  value="country" name="module"  />
                    <br />
    				<a href="#" onclick="document.forms[0].submit();" id="save"></a>
                    <a href="{$SiteRoot}/photoalbum/cats.php?action=delete&amp;id={$country.id}" onclick="return confirm('Are you sure you want to delete this country? Registered memebers to this country will be deleted from database also.');" class="verwijderen"></a>
    			</td>
    		</tr>
        </table>
    </form>
{/if}
