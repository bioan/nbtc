{strip}
<script type="text/javascript">
/* <![CDATA[ */{literal}
  function showDiv($divId)
  {
    var $obj = document.getElementById($divId);
    $obj.style.display = 'block';
  }
  
	function checkForm()
	{
		var obj = document.getElementById('photo_area');
		if (!obj.hasChildNodes()) {
			alert('Foto\'s toevoegen!');
			return false;	
		}
		for (var i=0; i < obj.childNodes.length; i++) {
			if (obj.childNodes.item(i).nodeName.match(/^IMG$/))
				return true;
		}
		alert('Foto\'s toevoegen!');
		return false;	
	}

  	function checkIfDisabled(obj)
	{
		if (obj.options[obj.selectedIndex].disabled == true) {
			alert('Deze categorie kan niet gekozen worden omdat deze midden in de tree zit!');
			for (var i =0; i < obj.options.length; i++) {
				if (!obj.options[i].disabled) {
					obj.options[i].selected = true;
					return;
				}
			}
			
		}
	}
  {/literal}
/* ]]> */
</script> 
<form method="post" action="" enctype="multipart/form-data">
	<table style="width: 100%;">
	    <colgroup style="width: 10%"></colgroup>
        <colgroup style="width: 90%"></colgroup>
        <tbody>
        {if isset($error)}
		<tr>
			<td colspan="2">
				De volgende velden zijn niet (goed) ingevuld: <br />
				<ol>
				{foreach from=$error item=value}
					<li><span style="color: #FF0000;">{$value}</span></li>
				{/foreach}
				</ol>
			</td>
		</tr>
		{/if}
		<tr>
			<td class="formoptieNaam">
			    <label for="category">Categorie</label>: 
			</td>
			<td>
			    <select name="category" id="category" onchange="checkIfDisabled(this);">
                    {if isset($nodes)}
                        {foreach from=$nodes item=node}
                            {if $node.oid|in_array:$leafNodes && $node.oid != 0}
                                <option value="{$node.oid}" {if isset($smarty.post.category) && $smarty.post.category == $node.oid} selected="selected" {/if}>{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                            {else}
                                <option value="{$node.oid}" disabled="disabled" style="color: #CCCCCC;">{str_repeat("&nbsp;",$node.level*2)}{$node.name|stripslashes}</option>
                            {/if}
                        {/foreach}
                    {else}
                        <option value="0">Er zijn geen categorie&euml;n aangemaakt.</option>
                    {/if}
                </select>
			</td>
		</tr>
		<tr>
            <td class="formoptieNaam">
			    <label for="category">Zip</label>: 
			</td>
            <td>
                <input type="file" name="zip" value="" />
            </td>
        </tr>
		<tr>
            <td></td>
			<td>
                <br />
			    <input type="image" src="{$SiteRoot}/images/save.png">
			</td>
		</tr>
	  </tbody>
   </table>
</form>
{/strip}
