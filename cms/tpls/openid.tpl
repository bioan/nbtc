{strip}
	<table style="width: 100%;" id="t1" class="sortable" cellpadding="2" cellspacing="0">
        <colgroup style="width: 33%"></colgroup>
        <colgroup style="width: 33%"></colgroup>
		<colgroup style="width: 33%"></colgroup>
        <tbody>
            <tr>
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr>
                <td class="first-row-first-column-white">
                <a href="{$SiteRoot}openid.php?action=overview&sort=email&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Email</a>
                </td>
                <td class="first-row-column-white">
                <a href="{$SiteRoot}openid.php?action=overview&sort=name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Naam</a>
                </td>
                <td class="first-row-last-column">
                <a href="{$SiteRoot}openid.php?action=overview&sort=date_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum aangemaakt</a>
                </td>
            </tr>	

			{if !isset($users)}
            <tr>
                    <td class="first-column-gray">
                    Er zijn nog geen gebruikers toegevoegd
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
			{else}
                {foreach from=$users item=user name=iCounter}
					
                        <tr ondblclick="window.location.href = '{$SiteRoot}openid.php?action=edit&id={$user.id}'" class="pointer">
                        {if $smarty.foreach.iCounter.iteration % 2 == 1}   
                    	<td class="first-column-gray">
						    {$user.email}
						</td>
						<td class="column-gray">
							{$user.name}
						</td>
						<td class="last-column-gray">
							{$user.date_format}
						</td>
                        {else}
                        <td class="first-column-white">
                            {$user.email}
                        </td>
                        <td class="column-white">
                            {$user.name}
                        </td>
                        <td class="last-column-white">
                            {$user.date_format}
                        </td>
                        {/if}
					</tr>
				{/foreach}
			{/if}
            <tr>
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
		{if isset($links)}{$links}{/if}
	</div>
	<br />

{/strip}
