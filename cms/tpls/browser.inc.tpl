{*
	Default template for the images manage system of inforitus
	
	$author: info@inforitus.nl
	@copyright: inforitus.nl
	@website: http://www.inforitus.nl
	@date: 26-12-2006
*}
{strip}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl">
<head>
<title>inforitus image browser</title> <!--@import url("http://www.inforitus.nl/cms/css/default.css");-->
<style type="text/css" media="all">@import url("{$SiteRoot}/css/default.css");</style>
<style type="text/css">{literal}
/* <![CDATA[ */
 body {
 	margin: 0px;
 	padding: 0px;
 }
 
 #photoPlace {
 	background-color: #F5F5F5;
 }
 
 button {
 	border: black 1px solid;
 	background-color: #059EFE;
 }

{/literal}/* ]]> */
</style>
<script src="{$SiteRoot}/js/browserDetect.js" type="text/javascript"></script>
<script src="{$SiteRoot}/js/browser.js" type="text/javascript"></script>
</script>
</head>
<body>
<div style="height: 600px;">
	<div style="width: 18%; float: left; height: 100%; border-right: black solid 0.08em;">
		<div style="height: 100%; border-bottom: 1px dashed #D5D5D5; overflow: auto" id="folderPlace"></div>
	</div>
	<div style="width: 81%; float: left; height: 100%;">
		<div style="height: 10%; border-bottom: 1px dashed #D5D5D5">
			<img src="{$SiteRoot}/images/folder_photoalbum.gif" alt="" name="photoalbum" />
			<div style="position: absolute; top: 1px; left: 55px; font-size: 140%; font-weight: bold;">Afbeeldingen overzicht</div>
			<div style="position: absolute; top: 25px; left: 55px;" id="folderPlaceShow">
			{$currentFolder}
			</div>
			<div style="position: absolute; font-weight: bold; right: 5px; top: 1px;">
				<a href="#" onclick="reload(); return false;">Herlaad afbeelding</a>
			</div>
		</div>
		<div style="height: 80%; border-bottom: 1px dashed #D5D5D5; overflow: auto" id="photoPlace"></div>
		<div style="height: 10%; ">
				<table style="width: 100%;">
					<colgroup style="width: 40%;"></colgroup>
					<colgroup style="width: 10%;"></colgroup>
					<colgroup style="width: 50%;"></colgroup>
					<form method="post" action="{$SiteRoot}/browser/upload_image.php" onsubmit="return checkForm(this);" enctype="multipart/form-data" target="edit_image">
					<tr>
						<td style="font-weight: bold;">
						Afbeelding
						</td>
						<td>
						<input type="file" name="image" />
						</td>
						<td>
						<button type="submit" name="submit" onclick="window.open('','edit_image','height=600,width=600,resizable=yes,scrollbars=yes');">Ok</button>
						</td>
					</tr>
					</form>
					<form action="" method="post" onsubmit="return checkFolderForm(this, '{$SiteRoot}');">
					<tr>
						<td style="font-weight: bold;">
						Map aanmaken
						</td>
						<td>
						<input type="text" name="folderName" size="30" />
						</td>
						<td>
						<button type="submit" >Ok</button>
						</td>
					</tr>
					</form>
				</table>
			</form>
		</div>
		<div style="position: absolute; bottom: 1px; right: 5px; font-weight: bold;">
		<a href="#" onclick="window.close(); return false">Sluit venster</a>
		</div>
	</div>
</div>
<script type="text/javascript">
/* <![CDATA[ */
	{if isset($smarty.get.type) && $smarty.get.type == 'fck'}
		reload('fck', '{$SiteRoot}');
	{else}
		reload('not', '{$SiteRoot}');
	{/if}
	getFolders();
	var $browserHeight = (navigator.appName.match(/^Net/i)) ? 720 : 680;
	window.resizeTo(755, $browserHeight);
/* ]]> */
</script>

</body>
</html>
{/strip}
