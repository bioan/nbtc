{strip}
{if $smarty.get.action == 'new'}
    <form method="post" action="" >
        <table style="width: 100%;">
            <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="2">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}

            <tr>
                <td class="fieldName">
                    <label for="link">Link</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="link" id="link" value="{if isset($smarty.post.link)}{$smarty.post.link}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="date_start">Datum</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="date_start" id="date_start" value="{$sCurrentDate}" size="10" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                <label for="date_end">Datum tot</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="date_end" id="date_end" value="{if isset($smarty.post.date_end)}{$smarty.post.date_end}{/if}" size="10" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="group_id">Nieuwsgroep</label>: 
                </td>
                <td>
                    <select name="group_id" id="group_id">
                        {foreach from=$NewsGroups item=value}
                            <option value="{$value.id}" {if isset($smarty.post.group_id) && $smarty.post.group_id == $value.id} selected="selected" {/if} >{$value.group_name|stripslashes}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
            
            <tr>
                <td class="fieldName">
                    <label>Afbeelding {$item.language}</label>: 
                </td>
                <td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_{$id}"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_{$id}" ></span><br class="clear" />
                    <span id="photos_{$id}">
                    {if isset($smarty.post.photos.$id)}
                        {foreach from=$smarty.post.photos.$id item=photoOid}
                            <img id="{$photoOid}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photoOid}/100/image.php" />
                            <input name="photos[{$id}][{$photoOid}]" value="{$photoOid}" type="hidden" />
                        {/foreach}
                    {/if}
                    </span>
                    
                </td>
            </tr>
            <tr>
                <td class="fieldName" style="width:200px;">
                    <label for="title_{$item.language}">Titel {$item.language}</label>: 
                </td>
                <td style="width:200px;">
                    <input type="text" class="input" name="title[{$id}]" id="title_{$item.language}" value="{if isset($smarty.post.title.$id)}{$smarty.post.title.$id}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label>Bericht {$item.language}</label>: 
                </td>
                <td>
                    {$FCKeditor.$id}
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_{$item.language}">Pagina omschrijving {$item.language}</label>:
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description_{$item.language}">{if isset($smarty.post.page_description.$id)}{$smarty.post.page_description.$id}{/if}</textarea>
                </td>
            </tr>
            {/foreach}
            <tr>
                <td class="fieldName">
                    <label for="twitter_text">Twitter text</label>:
                </td>
                <td>
                    {if isset($publishTwitter) && $publishTwitter == 'true'}
                        <textarea cols="113" rows="10" name="twitter_text" id="twitter_text">{if isset($smarty.post.twitter_text)}{$smarty.post.twitter_text}{/if}</textarea><br />
                        Number of characters remaining: <span id="count">140</span>
                    {else}
                        {$publishTwitter}
                    {/if}
                </td>
            </tr>
            {if isset($publishTwitter) && $publishTwitter == 'true'}
            <tr>
                <td class="fieldName">
                    Post on twitter:
                </td>
                <td>
                    <input type="checkbox" name="postTwitter" {if isset($smarty.post.postTwitter)}checked="checked"{/if} />
                </td>
            </tr>
            {/if}
            {if isset($publishLinkedin) && $publishLinkedin == 'true'}
            <tr>
                <td class="fieldName">
                    Post on linkedin:
                </td>
                <td>
                    <input type="checkbox" name="postLinkedin" {if isset($smarty.post.postLinkedin)}checked="checked"{/if} />
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName">
                    Post facebook:
                </td>
                <td>
                    {if isset($publishFacebook) && $publishFacebook == 'true'}
                        <input type="checkbox" name="postFacebook" {if isset($smarty.post.postFacebook)}checked="checked"{/if} />
                    {else}
                        {$publishFacebook}
                    {/if}
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br />
                    <input type="hidden" id="module"  value="news" name="module"  />
                    <a href="#" onclick="document.forms[0].submit();" class="save"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="" /></a>
                    <input type="submit" class="keywords" value="" name="keywords"  />
                </td>
            </tr>
            {if isset($keywords)}
            <tr>
                <td class="fieldName">
                    Keywords    
                </td>
                <td>
                    {foreach from=$keywords item=item key=k}
                    {$k} - {$item}<br />
                    {/foreach}
                </td>
            </tr>
            {/if}
            </tbody>
        </table>
    </form>
    <script>
    {literal}
	$(function() {
		$( "#date_start" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    $(function() {
		$( "#date_end" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    {/literal}
	</script>
{elseif $smarty.get.action == 'overview'}
    Rijen weergeven :&nbsp;
    <select onchange="window.location='news.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="news" cellpadding="2" cellspacing="0">
        <colgroup style="width: 25%"></colgroup>
        <colgroup style="width: 25%"></colgroup>
        <colgroup style="width: 21%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        <colgroup style="width: 10%"></colgroup>
        
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="5" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}news/news.php?action=overview&items={$items}&sort=title&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Titel</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}news/news.php?action=overview&items={$items}&sort=group_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Groepsnaam</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}news/news.php?action=overview&items={$items}&sort=date_register_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum toegevoegd</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}news/news.php?action=overview&items={$items}&sort=filled&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Stats</a>
                </td>
                <td class="first-row-last-column">
                    Social Media
                </td>
            </tr>
            {if !$news}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                    Er zijn nog geen nieuws items gepost.    
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$news item=item name="iCounter"}
                
                <tr id="{$item.id}" order_number="{$item.order_number}" ondblclick="window.location.href = '{$SiteRoot}news/news.php?action=edit&id={$item.id}'" class="pointer {if isset($item.user)}tooltip{/if}" {if isset($item.user) && isset($item.user.user_name)}title="{$item.user.user_name}"{/if}>
                    {if $smarty.foreach.iCounter.iteration % 2 == 1} 
                    <td class="first-column-gray">
                        {$item.title|stripslashes}
                    </td>
                    <td class="column-gray">
                        {$item.group_name}
                    </td>
                    <td class="column-gray">
                        {$item.date_register_format}
                    </td>
                    <td class="column-gray">
                        <span class="{if $item.filled == $languages|@count}green{else}red{/if}">{$item.filled} / {$languages|@count}</span>
                    </td>
                    <td class="last-column-gray social">
                        {if $item.facebook != 0}<img src="/cms/images/facebook_icon.jpg" width="16" height="16"/>{/if}
                        {if $item.twitter != 0}<img src="/cms/images/twitter_icon.jpg" width="16" height="16"/>{/if}
                        {if $item.linkedin != 0}<img src="/cms/images/linkedin.gif" width="16" height="16"/>{/if}
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$item.title|stripslashes}
                    </td>
                    <td class="column-white">
                        {$item.group_name}
                    </td>
                    <td class="column-white">
                        {$item.date_register_format}
                    </td>
                    <td class="column-white">
                        <span class="{if $item.filled == $languages|@count}green{else}red{/if}">{$item.filled} / {$languages|@count}</span>
                    </td>
                    <td class="last-column-white social">
                        {if $item.facebook != 0}<img src="/cms/images/facebook_icon.jpg" width="16" height="16"/>{/if}
                        {if $item.twitter != 0}<img src="/cms/images/twitter_icon.jpg" width="16" height="16"/>{/if}
                        {if $item.linkedin != 0}<img src="/cms/images/linkedin.gif" width="16" height="16"/>{/if}
                    </td>
                    {/if}
                </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{elseif $smarty.get.action == 'edit'}
    <form method="post" action="" >
        <table style="width: 100%;">
            <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="3">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName">
                    <label for="link">Link</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="link" id="link" value="{$values.link}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="date_start">Datum</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="date_start" id="date_start" value="{$values.date_start_format}" size="10" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="date_end">Datum tot</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="date_end" id="date_end" value="{$values.date_end_format}" size="10" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="group_id">Nieuwsgroep</label>: 
                </td>
                <td>
                    <select name="group_id" id="group_id">
                        {foreach from=$NewsGroups item=value}
                            <option value="{$value.id}" {if $values.group_id == $value.id} selected="selected" {/if}>{$value.group_name|stripslashes}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            {foreach from=$languages item=item key=k}
            {assign var=id value=$item.id}
            
            <tr>
                <td class="fieldName">
                    <label>Afbeelding {$item.language}</label>: 
                </td>
                <td>
                    <ul class="photo-menu">
                        <li>
                            <div id="upload_{$id}"><span>kiezen</span></div>
                        </li>
                        <li>
                            <a class="greybox" area="photos_{$id}" href="{$SiteRoot}/crop.php">kiezen + bijsnijden</a>
                        </li>
                        <li class="last">
                            <a class="greybox browser" area="photos_{$id}" href="{$SiteRoot}/browser.php">bladeren</a>
                        </li>
                    </ul>
                    <span id="status_{$id}" ></span><br class="clear" />
                    <span id="photos_{$id}">
                    {if isset($values.$id.photos) && $values.$id.photos != ''}
                    {foreach from=$values.$id.photos item=photo}
                        <img id="{$photo.id}" onclick="removePhoto(this);" style="cursor: pointer;" src="{$SiteRoot}/foto.php/{$photo.id}/100/image.php" />
                        <input name="photos[{$id}][{$photo.id}]" value="{$photo.id}" type="hidden" />
                    {/foreach}
                    {/if}
                    </span>
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="title_{$id}">Titel {$item.language}</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="title[{$id}]" id="title_{$id}" value="{if isset($values.$id.title)}{$values.$id.title}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="f_id_2">Bericht {$item.language}</label>: 
                </td>
                <td>
                    {$FCKeditor.$id}
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                </td>
                <td>
                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($values.$id.page_description)}{$values.$id.page_description}{/if}</textarea>
                </td>
            </tr>
            {/foreach}
            <tr>
                <td class="fieldName">
                    <label for="twitter_text">Twitter_text</label>:
                </td>
                <td>
                    {if isset($publishTwitter) && $publishTwitter == 'true'}
                        <textarea cols="113" rows="10" name="twitter_text" id="twitter_text">{if isset($values.twitter_text)}{$values.twitter_text}{/if}</textarea><br />
                        Number of characters remaining: <span id="count">140</span>
                    {else}
                        {$publishTwitter}
                    {/if}
                </td>
            </tr>
            {if isset($publishTwitter) && $publishTwitter == 'true'}
            <tr>
                <td class="fieldName">
                    Post on twitter:
                </td>
                <td>
                    <input type="checkbox" name="postTwitter" {if isset($values.twitter) && $values.twitter != 0}checked="checked"{/if} />
                </td>
            </tr>
            {/if}
            {if isset($publishLinkedin) && $publishLinkedin == 'true'}
            <tr>
                <td class="fieldName">
                    Post on linkedin:
                </td>
                <td>
                    <input type="checkbox" name="postLinkedin" {if isset($values.linkedin) && $values.linkedin != 0}checked="checked"{/if} />
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName">
                    Post facebook:
                </td>
                <td>
                    {if isset($publishFacebook) && $publishFacebook == 'true'}
                        <input type="checkbox" name="postFacebook" {if $values.facebook != 0}checked="checked"{/if} />
                    {else}
                        {$publishFacebook}
                    {/if}
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br />
                    <input type="hidden" id="module"  value="news" name="module"  />
                    <input class="button" type="submit" name="submit" id="save" value="" />
                    <a class="verwijderen" href="{$SiteRoot}/news/news.php?action=delete&amp;id={$values.id}" onclick="return confirm('Weet u zeker dat u dit nieuws item wilt verwijderen?'); return false;"></a>
                    <input type="submit" class="keywords" value="" name="keywords"  />
                </td>
            </tr>
            {if isset($keywords)}
            <tr>
                <td class="fieldName">
                    Keywords    
                </td>
                <td>
                    {foreach from=$keywords item=item key=k}
                    {$k} - {$item}<br />
                    {/foreach}
                </td>
            </tr>
            {/if}
            </tbody>
        </table>
    </form>
    <script>
    {literal}
	$(function() {
		$( "#date_start" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    $(function() {
		$( "#date_end" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    {/literal}
	</script>
{else}
    Deze pagina is niet helemaal ok!
{/if}
{/strip}
