{strip}
{if $smarty.get.action == 'overview'}
    {if isset($smarty.get.news_id) && isset($smarty.get.lang_id)}
    Rijen weergeven :&nbsp;
    <select onchange="window.location='news.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="news" cellpadding="2" cellspacing="0">
        <colgroup style="width: 33%"></colgroup>
        <colgroup style="width: 33%"></colgroup>
        <colgroup style="width: 33%"></colgroup>
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}news/news.php?action=overview&items={$items}&sort=title&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">User name</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}news/news.php?action=overview&items={$items}&sort=group_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Message</a>
                </td>
                <td class="first-row-last-column">
                    <a href="{$SiteRoot}news/news.php?action=overview&items={$items}&sort=date_register_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum toegevoegd</a>
                </td>
            </tr>
            {if !$comments}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                    Er zijn nog geen nieuws items gepost.    
                    </td>
                    <td class="column-gray">
                    
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$comments item=item name="iCounter"}
                
                <tr id="{$item.id}" onclick="window.location.href = '{$SiteRoot}forum/comment.php?action=edit&id={$item.id}'" class="pointer">
                    {if $smarty.foreach.iCounter.iteration % 2 == 1} 
                    <td class="first-column-gray">
                        {"&nbsp;"|str_repeat:($item.level*3)}{$item.username|stripslashes}
                    </td>
                    <td class="column-gray">
                        {$item.message|substr:0:150}
                    </td>
                    <td class="last-column-gray">
                        {$item.date_register_format}
                    </td>
                    {else}
                    <td class="first-column-white">
                        {"&nbsp;"|str_repeat:($item.level*3)}{$item.username|stripslashes}
                    </td>
                    <td class="column-white">
                        {$item.message|substr:0:150}
                    </td>
                    <td class="last-column-white">
                        {$item.date_register_format}
                    </td>
                    {/if}
                </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
    {elseif !isset($smarty.get.news_id) && !isset($smarty.get.lang_id)}
    Rijen weergeven :&nbsp;
    <select onchange="window.location='comment.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="news" cellpadding="2" cellspacing="0">
        <colgroup style="width: 25%"></colgroup>
        <colgroup style="width: 25%"></colgroup>
        <colgroup style="width: 21%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        <colgroup style="width: 10%"></colgroup>
        
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="5" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}news/comment.php?action=overview&items={$items}&sort=title&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Titel</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}news/comment.php?action=overview&items={$items}&sort=group_name&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Groepsnaam</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}news/comment.php?action=overview&items={$items}&sort=date_register_format&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Datum toegevoegd</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}news/comment.php?action=overview&items={$items}&sort=filled&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Stats</a>
                </td>
                <td class="first-row-last-column">
                    Social Media
                </td>
            </tr>
            {if !$news}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                    Er zijn nog geen nieuws items gepost.    
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$news item=item name="iCounter"}
                
                <tr id="{$item.id}" order_number="{$item.order_number}" ondblclick="window.location.href = '{$SiteRoot}news/comment.php?action=overview&news_id={$item.id}'" class="pointer {if isset($item.user)}tooltip{/if}" {if isset($item.user) && isset($item.user.user_name)}title="{$item.user.user_name}"{/if}>
                    {if $smarty.foreach.iCounter.iteration % 2 == 1} 
                    <td class="first-column-gray">
                        {$item.title|stripslashes}
                    </td>
                    <td class="column-gray">
                        {$item.group_name}
                    </td>
                    <td class="column-gray">
                        {$item.date_register_format}
                    </td>
                    <td class="column-gray">
                        <span class="{if $item.filled == $languages|@count}green{else}red{/if}">{$item.filled} / {$languages|@count}</span>
                    </td>
                    <td class="last-column-gray social">
                        {if $item.facebook != 0}<img src="/cms/images/facebook_icon.jpg" width="16" height="16"/>{/if}
                        {if $item.twitter != 0}<img src="/cms/images/twitter_icon.jpg" width="16" height="16"/>{/if}
                        {if $item.linkedin != 0}<img src="/cms/images/linkedin.gif" width="16" height="16"/>{/if}
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$item.title|stripslashes}
                    </td>
                    <td class="column-white">
                        {$item.group_name}
                    </td>
                    <td class="column-white">
                        {$item.date_register_format}
                    </td>
                    <td class="column-white">
                        <span class="{if $item.filled == $languages|@count}green{else}red{/if}">{$item.filled} / {$languages|@count}</span>
                    </td>
                    <td class="last-column-white social">
                        {if $item.facebook != 0}<img src="/cms/images/facebook_icon.jpg" width="16" height="16"/>{/if}
                        {if $item.twitter != 0}<img src="/cms/images/twitter_icon.jpg" width="16" height="16"/>{/if}
                        {if $item.linkedin != 0}<img src="/cms/images/linkedin.gif" width="16" height="16"/>{/if}
                    </td>
                    {/if}
                </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
    {elseif isset($smarty.get.news_id) && !isset($smarty.get.lang_id)}
    Rijen weergeven :&nbsp;
    <select onchange="window.location='language.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="news" cellpadding="2" cellspacing="0">
        <colgroup style="width: 40%"></colgroup>
        <colgroup style="width: 41%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        
        <tbody>
            <tr>
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr>
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}language/language.php?action=overview&items={$items}&sort=language&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Name</a>
                </td>
                <td class="first-row-column-white">
                </td>
                <td class="first-row-last-column">
                </td>
            </tr>
            {if !$langs}
                <tr>
                    <td class="first-column-gray">
                    Er zijn nog geen nieuws items gepost.    
                    </td>
                    <td class="column-gray">
                    
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$langs item=item name="iCounter"}
                <tr id="{$item.id}" ondblclick="window.location.href = '{$SiteRoot}news/comment.php?action=overview&news_id={$smarty.get.news_id}&lang_id={$item.id}'" class="pointer">
                    {if $smarty.foreach.iCounter.iteration % 2 == 1} 
                    <td class="first-column-gray">
                        {$item.language|stripslashes}
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$item.language|stripslashes}
                    </td>
                    <td class="column-white">
                    </td>
                    <td class="last-column-white">
                    </td>
                    {/if}
                </tr>
            {/foreach}
            <tr>
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
    {/if}
{elseif $smarty.get.action == 'edit'}
    <form method="post" action="" >
        <table style="width: 100%;">
            <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="3">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName">
                    <label for="f_id_2">Message</label>: 
                </td>
                <td>
                    {$FCKeditor}
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br />
                    <input type="hidden" id="module"  value="forum" name="module"  />
                    <input class="button" type="submit" name="submit" id="save" value="" />
                    <a class="verwijderen" href="{$SiteRoot}/forum/comment.php?action=delete&amp;id={$values.id}" onclick="return confirm('Weet u zeker dat u dit nieuws item wilt verwijderen?'); return false;"></a>
                    <input type="submit" class="keywords" value="" name="keywords"  />
                </td>
            </tr>
            {if isset($keywords)}
            <tr>
                <td class="fieldName">
                    Keywords    
                </td>
                <td>
                    {foreach from=$keywords item=item key=k}
                    {$k} - {$item}<br />
                    {/foreach}
                </td>
            </tr>
            {/if}
            </tbody>
        </table>
    </form>
    <script>
    {literal}
	$(function() {
		$( "#date_start" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    $(function() {
		$( "#date_end" ).datepicker({ dateFormat: 'dd-mm-yy' });
	});
    {/literal}
	</script>
{else}
    Deze pagina is niet helemaal ok!
{/if}
{/strip}
