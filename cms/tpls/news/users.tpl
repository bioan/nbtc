{strip}
{if $smarty.get.action == 'new'}
    <form method="post" action="" >
        <table style="width: 100%;">
            <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="2">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName">
                    <label for="username">Username</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="username" id="username" value="{if isset($smarty.post.username)}{$smarty.post.username}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="email">Email</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="email" id="email" value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="password">Password</label>: 
                </td>
                <td>
                    <input type="password" class="input" name="password" id="password" value="{if isset($smarty.post.password)}{$smarty.post.password}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="password2">Rewrite Password</label>: 
                </td>
                <td>
                    <input type="password" class="input" name="password2" id="password2" value="{if isset($smarty.post.password2)}{$smarty.post.password2}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br />
                    <a href="#" onclick="document.forms[0].submit();" class="save"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="" /></a>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
{elseif $smarty.get.action == 'overview'}
    Rijen weergeven :&nbsp;
    <select onchange="window.location='users.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="news" cellpadding="2" cellspacing="0">
        <colgroup style="width: 33%"></colgroup>
        <colgroup style="width: 33%"></colgroup>
        <colgroup style="width: 33%"></colgroup>
        
        <tbody>
            <tr nodrag="true" nodrop="true">
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr nodrag="true" nodrop="true">
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}news/users.php?action=overview&items={$items}&sort=username&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Username</a>
                </td>
                <td class="first-row-column-white">
                    <a href="{$SiteRoot}news/users.php?action=overview&items={$items}&sort=email&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Email</a>
                </td>
                <td class="first-row-last-column">
                    <a href="{$SiteRoot}news/users.php?action=overview&items={$items}&sort=created&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Created</a>
                </td>
            </tr>
            {if !$users}
                <tr nodrag="true" nodrop="true">
                    <td class="first-column-gray">
                    Er zijn nog geen nieuws items gepost.    
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$users item=item name="iCounter"}
                
                <tr id="{$item.id}" nodrag="true" nodrop="true" ondblclick="window.location.href = '{$SiteRoot}news/users.php?action=edit&id={$item.id}'" class="pointer">
                    {if $smarty.foreach.iCounter.iteration % 2 == 1} 
                    <td class="first-column-gray">
                        {$item.username}
                    </td>
                    <td class="column-gray">
                        {$item.email}
                    </td>
                    <td class="last-column-gray">
                        {$item.created}
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$item.username}
                    </td>
                    <td class="column-white">
                        {$item.email}
                    </td>
                    <td class="last-column-white">
                        {$item.created}
                    </td>
                    {/if}
                </tr>
            {/foreach}
            <tr nodrag="true" nodrop="true">
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{elseif $smarty.get.action == 'edit'}
    <form method="post" action="" >
        <table style="width: 100%;">
            <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="3">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName">
                    <label for="username">Username</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="username" id="username" value="{$values.username}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="email">Email</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="email" id="email" value="{$values.email}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="password">Password</label>: 
                </td>
                <td>
                    <input type="password" class="input" name="password" id="password" value="" size="40" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br />
                    <input type="hidden" name="old_password" value="{$values.password}" />
                    <input class="button" type="submit" name="submit" id="save" value="" />
                    <a class="verwijderen" href="{$SiteRoot}/news/users.php?action=delete&amp;id={$values.id}" onclick="return confirm('Weet u zeker dat u dit user wilt verwijderen?'); return false;"></a>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
{else}
    Deze pagina is niet helemaal ok!
{/if}
{/strip}
