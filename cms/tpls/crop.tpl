{strip}

	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl">
	<head>
	      <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	      <meta name="generator" content="PSPad editor, www.pspad.com" />
	      <style type="text/css" media="all">@import url("{$SiteRoot}/css/default.css");</style>
	      {if isset($cssBody)}
	            {foreach from=$cssBody item=css}
					<style type="text/css" media="all">@import url("{$SiteRoot}/{$css}");</style>
				{/foreach}
			{/if}
	      <title>::. CMS .:: </title>
			{if $javascriptBody}
				{foreach from=$javascriptBody item=javascript}
					<script type="text/javascript" src="{$SiteRoot}/js/{$javascript}"></script>
				{/foreach}
			{/if}
			<script type="text/javascript" src="{$SiteRoot}/js/default.js"></script>
	</head>
	<body>
    {if !isset($smarty.post.id)}
        <form method="post" id="form" action="" enctype="multipart/form-data" onsubmit="return checkCoords();window.opener.reloadRemote();">
            <input type="hidden" id="module"  value="{$module}" name="module"  />
            {if isset($smarty.get.template)}
            <input type="hidden" id="template"  value="{$smarty.get.template}" name="template"  />
            {/if}
            <div class="step_1">
                <p>
                    Selecteer een foto van uw computer en voeg deze toe
                </p>
                <div id="cropupload"><span>upload</span></div>
                <span id="cropstatus" ></span><br /><br /><br />
            </div>
            <table>
                <tr>
                    <td>
                        <div id="cropbox">
                        </div>
                    </td>
                    <td>
                        <div class="preview">
                            <div class="index_image">
                            </div>
                            <select name="ratio" id="ratio" onchange="changeRatio ()">
                            {foreach from=$sizes item=item}
                                {if isset($item.width) && isset($item.height) && $item.display == true}
                                <option value="{$item.width}/{$item.height}">{$item.crop_name}</option>
                                {/if}
                            {/foreach}
                            </select>
                        </div>
                    </td>
                </tr>
            </table>
            </table>
            <br class="clear" />
            <div class="submit-but">
                <input type="hidden" id="x" name="x" value="" />
                <input type="hidden" id="y" name="y" value="" />
                <input type="hidden" id="w" name="w" value="" />
                <input type="hidden" id="h" name="h" value="" />
                <input type="hidden" id="w_original" name="w_original" value="" />
                <input type="hidden" id="h_original" name="h_original" value="" />
                <input type="hidden" id="id" value="" name="id" />        
                
                <input type="submit" name="crop" id="crop_image" value="Crop" />
            </div>
        </form>
    {else}
        <script type="text/javascript">parent.setPhoto({$smarty.post.id}, '{$url}');parent.GB_hide();</script>
    {/if}
    </body>
 </html>