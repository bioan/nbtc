{strip}
<table class="cms-menu">
<tr>
<td>
<a id="home" href="{$SiteRoot}/authorization.php"><img src="{$SiteRoot}/images/home_button.jpg" alt="home" /></a>    
</td>
<td>
<table id="nav">
    <tr>
        {if 'pages'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "pagina"}selected{/if}" {if $folder.page == "pagina"}id="pagina"{/if} href="{$SiteRoot}/pages/pages.php">Paginabeheer</a></td>
        {/if}
        
        {if 'news'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
             <td><a class="font {if $folder.page == "news"}selected{/if}" {if $folder.page == "news"}id="nieuws"{/if} href="{$SiteRoot}/news/news.php?action=overview">Nieuws</a></td> 
        {/if}        
        
        {if 'photoalbum'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td> 
            <td><a class="font {if $folder.page == "foto"}selected{/if}" {if $folder.page == "foto"}id="foto"{/if} href="{$SiteRoot}/photoalbum/cats.php?action=overview">Countries</a></td>
        {/if}
        
        {if 'videoalbum'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "videoalbum"}selected{/if}" {if $folder.page == "videoalbum"}id="videoalbum"{/if} href="{$SiteRoot}/videoalbum/videoalbum.php?action=overview">Videoalbum</a></td>
        {/if}
        
        {if 'guestbook'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "guestbook"}selected{/if}" {if $folder.page == "guestbook"}id="guestbook"{/if} href="{$SiteRoot}/guestbook/guestbook.php">Gastenboek</a></td>              
        {/if}
        
        {if 'agenda'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "agenda"}selected{/if}" {if $folder.page == "agenda"}id="agenda"{/if} href="{$SiteRoot}/agenda/agenda.php?action=overview" >Agenda</a></td> 
        {/if}
        
        {if 'webshop'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "webshop"}selected{/if}" {if $folder.page == "webshop"}id="webwinkel"{/if} href="{$SiteRoot}/webshop/webshop.php?action=overview">Webwinkel</a></td>
        {/if}
        
        {if 'forum'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "forum"}selected{/if}" {if $folder.page == "forum"}id="agenda"{/if} href="{$SiteRoot}/forum/comment.php?action=overview" >Forum</a></td> 
        {/if}
        
        {if 'social_media'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
             <td><a class="font {if $folder.page == "social_media"}selected{/if}" {if $folder.page == "social_media"}id="pagina"{/if} href="{$SiteRoot}/social_media/social_media.php?action=new">Social Media</a></td> 
        {/if}
        
        {if 'newsletter'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "newsletter" || $folder.page == "template"}selected{/if}" {if $folder.page == "newsletter" || $folder.page == "template"}id="videoalbum"{/if} href="{$SiteRoot}/newsletter/templates.php?action=overview">Newsletter</a></td>
        {/if}
        
        {if 'ga'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "ga"}selected{/if}" {if $folder.page == "ga"}id="webwinkel"{/if} href="{$SiteRoot}/ga/ga.php?action=ga">Google</a></td>
        {/if}
        
        {if 'openid'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "openid"}selected{/if}" {if $folder.page == "openid"}id="webwinkel"{/if} href="{$SiteRoot}/openid.php?action=overview">Open Id</a></td>
        {/if}
        
        {if 'language'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "language"}selected{/if}" {if $folder.page == "language"}id="talen"{/if} href="{$SiteRoot}/language/language.php?action=overview">Talen</a></td>
        {/if}
        
        {if 'errors'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "errors"}selected{/if}" {if $folder.page == "errors"}id="talen"{/if} href="{$SiteRoot}/errors/errors.php?action=overview">Errors</a></td>
        {/if}
        
        {if 'setting'|in_array:$securePages}
            <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
            <td><a class="font {if $folder.page == "setting"}selected{/if}" {if $folder.page == "setting"}id="setting"{/if} href="{$SiteRoot}/setting.php?type=logo&action=new">Settings</a></td>
        {/if}
        
        <td><img src="{$SiteRoot}/images/menu_bar.jpg" class="menu_bar" alt="" /></td>
    </tr>
</table> 
</td>
{/strip}
