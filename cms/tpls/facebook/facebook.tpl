<form action="" method="post">
    <table cellspacing="0" cellpadding="0" style="100%">
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 80%"></colgroup>
        <tr>
            <td class="fieldTitle" colspan="2">
               1. Facebook Connection
            </td>
        </tr>
        {if isset($successMessage) && $appErrors|count == 0}
        <tr>
            <td class="successMessage" colspan="2">
               {$successMessage}
            </td>
        </tr>
        {/if}
        {if isset($successAccToken)}
        <tr>
            <td class="successMessage" colspan="2">
               {$successAccToken}
            </td>
        </tr>
        {/if}
        {if isset($appErrors.appID)}
        <tr>
            <td class="error" colspan="2">
               {$appErrors.appID}
            </td>
        </tr>
        {/if}
        <tr>
            <td class="fieldName">
                Application ID:
            </td>
            <td>
                <input type="text" class="input" name="appID" value="{if isset($smarty.post.appID)}{$smarty.post.appID}{elseif isset($appValues.appID)}{$appValues.appID}{else}{/if}" size="30">
            </td>
        </tr>
        {if isset($appErrors.appSecret)}
        <tr>
            <td class="error" colspan="2">
               {$appErrors.appSecret}
            </td>
        </tr>
        {/if}
        <tr>
            <td class="fieldName">
                Application secret:
            </td>
            <td>
                <input type="text" class="input" name="appSecret" value="{if isset($smarty.post.appSecret)}{$smarty.post.appSecret}{elseif isset($appValues.appSecret)}{$appValues.appSecret}{else}{/if}" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName" colspan="2">
                <input type="submit" value="Save Changes" name="saveChangesApp" />
                {if isset($loginUrl) && $loginUrl != '' && $loginUrl != 'false'}
                    <a href="{$loginUrl}" title="Grant Permissions">Grant permissions</a>
                {elseif isset($loginUrl) && $loginUrl == 'false' && $loginUrl != ''}
                    You can't obtain permissions for this app. Enter some valid data !
                {else}
                    Access Granted
                {/if}
           </td>
        </tr>
    </table>
</form>

{if isset($facebookUser.pages)}
<form action="" method="post">
    <table cellspacing="0" cellpadding="0" style="100%">
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 80%"></colgroup>
        <tr>
            <td class="fieldTitle" colspan="2">
               2. Publishing
            </td>
        </tr>
        <tr>
            <td colspan="2" class="fieldMessage">
               Specify the posts to publish and decide on which Facebook wall they will appear.
            </td>
        </tr>
        <tr>
            <td class="fieldName">
               Page or profile ID:
            </td>
            <td>
                <input type="text" class="input" name="profileID" value="{if isset($facebookUser)}{$facebookUser.publishID}{/if}" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName" colspan="2">
                <ul>
                    {foreach from=$facebookUser.pages item=item key=k}
                        {if isset($item.name)}
                        <li><a href="/cms//social_media/social_media.php?media=facebook&action=new&page={$k}" title="{$item.name}">{$item.name}</a></li>
                        {/if}
                    {/foreach}
                </ul>
            </td>
        </tr>
    </table>
</form>
{/if}