{*
    Default template for manage the added pages
    
    &copy;copyright inforitus
    
*}
{strip}

{if $action == 'new'}

{elseif $action == 'change'}
    <table>
    <tr>
    <td>
    <div style="float: left; width: 283px; height: 90%;">
    <ul class="menu">
        <li class="title">
            <img src="{$SiteRoot}/images/menu_left_top_corner.jpg" class="left-top" alt="" />
            <img src="{$SiteRoot}/images/menu_right_top_corner.jpg" class="right-top" alt="" />
            <p>Menu-items<p>
        </li>
    </ul>
    <ul class="menu" id="tmenu">
        {foreach from=$parents item=node key=k}
        <li class="parent-li {if $node.childs|@count == 0 || (isset($node.fold) && $node.fold == 1 && $node.childs|@count > 0)}add-border-bottom{/if}" id="i_1_2_{$node.oid}">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <img src="{$SiteRoot}/images/arrow.png" alt="move" width="9" height="9" class="handle-tmenu arrow-drag" />
                        <a title="Wijzigen" class="add-parent" style="padding-left: {$node.level*7}px;" href="{$SiteRoot}/pages/pages.php?add=1&catoid={$node.oid}"><img style="float:left;" src="{$SiteRoot}/images/add_page.gif" alt=""  /></a> 
                        {if $node.childs|@count == 0}<a class="add-subpage" title="Wijzigen" href="{$SiteRoot}/pages/pages.php?catoid={$node.oid}&nochilds=1"><img src="{$SiteRoot}/images/add_subpage.gif" alt="" style="float:left;" /></a>{/if}
                        {if $node.childs|@count > 0}<a title="Wijzigen" class="add-subpage" href="{$SiteRoot}/pages/pages.php?paroid={$node.childs.0.oid}"><img src="{$SiteRoot}/images/add_subpage.gif" alt="" style="float:left;" /></a>{/if}
                        <a title="Wijzigen" class="parent" style="padding-left: {$node.level*7}px;" href="{$SiteRoot}/pages/pages.php?action=change&amp;oid={$node.oid}">{$node.name|stripslashes|truncate:20:'..':true}</a>
                        {if $node.childs|@count > 0}<a onclick="fold_menu(this, '{$node.oid}')"><img src="{$SiteRoot}/images/arrow_{if isset($node.fold) && $node.fold == 1}down{else}up{/if}.gif" class="arrow_{if isset($node.fold) && $node.fold == 1}down{else}up{/if}" alt="" /></a>{/if}
                        <a href="{$SiteRoot}/pages/pages.php?action=private&oid={$node.oid}&private={if $node.private == 1}0{else}1{/if}"><img src="{$SiteRoot}/images/{if $node.private == 0}enable{else}disable{/if}.gif" class="{if $node.private == 0}non-active{else}active{/if}" alt="" /></a>
                    </td>
                </tr>
            </table>  
            {if is_array($node.childs) && $node.childs|@count > 0}
            <ul class="cmenu" id="t{$node.oid}" {if isset($node.fold) && $node.fold == 1} style="display:none;"{/if}>
            {foreach from=$node.childs item=cnode key=ck}
                <li id="i_2_{$node.oid}_{$cnode.oid}" class="subpage-li {if $cnode.childs|@count > 0} white {/if} {if ($node.childs|@count - 1) == $ck  || (isset($cnode.fold) && $cnode.fold == 1 && $cnode.childs|@count > 0)}add-border-bottom{/if}">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td style="padding-left: {$cnode.level*8}px;">   
                                <img src="{$SiteRoot}/images/arrow.png" alt="move" width="9" height="9" class="handle-t{$node.oid} arrow-drag" />                                                                                                                                                                                                                                                                
                                {if $cnode.childs|@count == 0}<a class="add-subpage second" title="Wijzigen" href="{$SiteRoot}/pages/pages.php?catoid={$cnode.oid}&nochilds=1"><img src="{$SiteRoot}/images/add_subsubpage.gif" alt="" style="float:left;" /></a>{/if}
                                {if $cnode.childs|@count > 0}<a title="Wijzigen" class="add-subpage second" href="{$SiteRoot}/pages/pages.php?paroid={$cnode.childs.0.oid}"><img src="{$SiteRoot}/images/add_subsubpage.gif" alt="" style="float:left;" /></a>{/if}
                                <a class="subpage" title="Wijzigen" style="padding-left:8px;" href="{$SiteRoot}/pages/pages.php?action=change&amp;oid={$cnode.oid}">{$cnode.name|stripslashes|truncate:25:'..'}</a>
                                {if $cnode.childs|@count > 0}<a onclick="fold_menu(this, '{$cnode.oid}')"><img src="{$SiteRoot}/images/arrow_{if isset($cnode.fold) && $cnode.fold == 1}down{else}up{/if}.gif" class="arrow_{if isset($cnode.fold) && $cnode.fold == 1}down{else}up{/if}" alt="" /></a>{/if}
                                <a href="{$SiteRoot}/pages/pages.php?action=private&oid={$cnode.oid}&private={if $cnode.private == 1}0{else}1{/if}"><img src="{$SiteRoot}/images/{if $cnode.private == 0}enable{else}disable{/if}.gif" class="{if $cnode.private == 0}non-active{else}active{/if}" alt="" /></a>
                                {if ($node.childs|@count - 1) != $ck}<img class="subpage-border" src="{$SiteRoot}/images/subpage_border.jpg" alt="" />{/if}
                                
                            </td>
                        </tr>
                    </table>
                    {if is_array($cnode.childs) && $cnode.childs|@count > 0}
                    <ul class="ccmenu" id="t{$cnode.oid}" {if isset($cnode.fold) && $cnode.fold == 1} style="display:none;"{/if}>
                    {foreach from=$cnode.childs item=ccnode key=cck}
                        <li id="i_3_{$cnode.oid}_{$ccnode.oid}" {if $ccnode.level == 3}class="subpage-li"{/if}>
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-left: {$ccnode.level*8}px;">    
                                        <img src="{$SiteRoot}/images/arrow.png" alt="move" width="9" height="9" class="handle-t{$cnode.oid} arrow-drag" />                                                                                                                                                                                                                                                               
                                        <a class="add-subpage second" title="Wijzigen" href="{$SiteRoot}/pages/pages.php?catoid={$ccnode.oid}&nochilds=1"><img src="{$SiteRoot}/images/add_subsubsubpage.gif" alt="" style="float:left;" /></a>
                                        <a class="subpage" title="Wijzigen" style="padding-left:8px;{if $cck != ($cnode.childs|@count - 1)}{else}{/if}" href="{$SiteRoot}/pages/pages.php?action=change&amp;oid={$ccnode.oid}">{$ccnode.name|stripslashes|truncate:25:'..'}</a>
                                        {if $ccnode.childs|@count > 0}<a onclick="fold_menu(this, '{$ccnode.oid}')"><img src="{$SiteRoot}/images/arrow_{if isset($ccnode.fold) && $ccnode.fold == 1}down{else}up{/if}.gif" class="arrow_{if isset($ccnode.fold) && $ccnode.fold == 1}down{else}up{/if}" alt="" /></a>{/if}
                                        {if $cck != ($cnode.childs|@count - 1)}<img class="subpage-border" src="{$SiteRoot}/images/subpage_border.jpg" alt="" />{/if}
                                        
                                    </td>
                                </tr>
                            </table>
                            {if is_array($ccnode.childs) && $ccnode.childs|@count > 0}
                            <ul class="ccmenu" id="t{$ccnode.oid}" {if isset($ccnode.fold) && $ccnode.fold == 1} style="display:none;"{/if}>
                            {foreach from=$ccnode.childs item=cccnode key=ccck}
                                <li id="i_4_{$ccnode.oid}_{$cccnode.oid}" {if $cccnode.level == 4}class="subsubpage-li"{/if}>
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-left: {$cccnode.level*8}px;">    
                                                <img src="{$SiteRoot}/images/arrow.png" alt="move" width="9" height="9" class="handle-t{$ccnode.oid} arrow-drag" />                                                                                                                                                                                                                                                               
                                                <a class="subpage" title="Wijzigen" style="padding-left:8px;{if $ccck != ($ccnode.childs|@count - 1)}{else}{/if}" href="{$SiteRoot}/pages/pages.php?action=change&amp;oid={$cccnode.oid}">{$cccnode.name|stripslashes|truncate:25:'..'}</a>
                                                {if $ccck != ($ccnode.childs|@count - 1)}<img class="subpage-border" src="{$SiteRoot}/images/subpage_border.jpg" alt="" />{/if}
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </li>
                            {/foreach}  
                            </ul>
                            {/if}
                        </li>
                    {/foreach}  
                    </ul>
                    {/if}
                </li>
            {/foreach}  
            </ul>
            {/if}
        </li>
        {/foreach}   
   </ul>
   <ul class="menu"> 
        <li  class="menu-bottom"> 
            <img src="{$SiteRoot}/images/menu_left_bottom_corner.jpg" class="left-bottom" alt="" />
            <img src="{$SiteRoot}/images/menu_right_bottom_corner.jpg" class="right-bottom" alt="" />
        </li>
    </ul>
    </div>
    </td>
    <td>
    {if isset($smarty.get.oid) || isset($smarty.get.catoid) || isset($smarty.get.paroid) || isset($smarty.get.existCat)}
    <div class="pagina-form">
        <div id="pagina-title" class="pagina-titel">Pagina</div>   
        <div id="content_in_content_div">
        <form method="post" action="" >
            <table>
                <tbody>
                {if isset($error)}
                <tr>
                    <td colspan="2">
                        De volgende velden zijn niet (goed) ingevuld: <br />
                        <ol>
                        {assign var=id value=$default_lang.id}
                        {foreach from=$error item=value}
                            <li><span style="color: #FF0000;">{$value.$id}</span></li>
                        {/foreach}
                        </ol>
                    </td>
                </tr>
                {/if}
                <tr>
                    <td style="vertical-align: top;">
                        <table style="padding-left: 12px;">
                            <!--<tr>
                                <td class="fieldName">
                                <label for="f_id_1">Foto(s)</label>: 
                                </td>
                                <td>
                                    <span id="photo_area">
                                    {if $nodeData.photos}
                                        {assign var=photos value=','|explode:$nodeData.photos}
                                        {foreach from=$photos item=photo}
                                            <img id="{$photo.id}" onclick="removePhoto(this);" style="cursor: pointer;" src="/cms/foto.php/{$photo.id}/100/image.php" />
                                            <input name="photos[{$photo.id}]" value="{$photo.id}" type="hidden" />
                                        {/foreach}
                                    {/if}
                                    </span>
                                    <a href="#" onclick="popup('/cms/browser.php',600, 735); return false">
                                        <img src="/cms/images/image.jpg" alt="Afbeelding uploaden" />
                                    </a>
                                </td>
                            </tr>-->
                            <tr>
                                <td class="fieldName">
                                    <label for="private">Private</label>:
                                </td>
                                <td>
                                    <input type="checkbox" value="1" name="private" id="private" {if $nodeData.private == 1} checked="checked" {/if} />
                                </td>
                            </tr>
                            {foreach from=$languages item=item key=k}
                            {assign var=id value=$item.id}
                            <tr>
                                <td class="fieldName">
                                    <label for="name_{$id}">Pagina titel {$item.language}</label>: 
                                </td>
                                <td>
                                    
                                    <input class="input" type="text" name="name[{$id}]" id="name_{$id}" value="{if isset($nodeData.$id.name)}{$nodeData.$id.name|stripslashes}{/if}" size="40" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldName">
                                    <label for="slug_{$id}">Pagina naam {$item.language}</label>: 
                                </td>
                                <td>
                                    <input class="input" type="text" name="slug[{$id}]" id="slug_{$id}" value="{if isset($nodeData.$id.slug)}{$nodeData.$id.slug}{/if}" size="40" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldName">
                                    <label>Content {$item.language}</label>: 
                                </td>
                                <td>
                                    {$FCKeditor.$id}
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldName">
                                    <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                                </td>
                                <td>
                                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($nodeData.$id.page_description)}{$nodeData.$id.page_description}{/if}</textarea>
                                </td>
                            </tr>
                            {/foreach}
                            {if isset($keywords)}
                            <tr>
                                <td class="fieldName">
                                    Keywords    
                                </td>
                                <td>
                                    {foreach from=$keywords item=item key=k}
                                    {$k} - {$item}<br />
                                    {/foreach}
                                </td>
                            </tr>
                            {/if}
                            <tr>
                                <td>                          
                                    <input type="hidden" name="active" value="1" />
                                    <input type="hidden" name="menuOid" value="{$nodeData.oid}" />
                                </td>
                                <td>
                                    <br />
                                  {if $nodeData.oid != 26 && $nodeData.oid != 27 && $nodeData.oid != 28 && $nodeData.oid != 89}
                                    <input type="image" src="{$SiteRoot}/images/save.png" style="cursor:pointer;float:left;" />
                                  {/if}  
                                    <input onclick="return confirm('Weet u zeker dat u wilt verwijderen?');" type="submit" class="verwijderen" value="" name="dell"  />
                                    <input type="submit" class="keywords" value="" name="keywords"  />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                </tbody>
            </table>
        </form>
    
    </div>
    
    <img src="{$SiteRoot}/images/pagina_bottom.jpg" alt="" class="pagina_bottom" />
    </div>
    {/if}
    </td>
    <td>
    {if $settings.functie_private == 0}
    <div class="functies">
        <div class="title">{$settings.functie_title}</div>
            {$settings.functie_description}<br class="clear" />
        <img src="{$SiteRoot}/images/functies_bottom.jpg" alt="" class="functies_bottom" />
    </div>
    {/if}
    </td>
    </tr>
    </table>
{elseif isset($succes)}
    <p>
        {$succes}
    </p>
{else}
    <table>
    <tr>
    <td>
    <div style="float: left; width: 283px; height: 90%;">
    <ul class="menu">
        <li class="title">
            <img src="{$SiteRoot}/images/menu_left_top_corner.jpg" class="left-top" alt="" />
            <img src="{$SiteRoot}/images/menu_right_top_corner.jpg" class="right-top" alt="" />
            <p>Menu-items<p>
        </li>
    </ul>
    <ul class="menu" id="tmenu">
        {foreach from=$parents item=node key=k}
        <li class="parent-li {if $node.childs|@count == 0 || (isset($node.fold) && $node.fold == 1 && $node.childs|@count > 0)}add-border-bottom{/if}" id="i_1_2_{$node.oid}">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <img src="{$SiteRoot}/images/arrow.png" alt="move" width="9" height="9" class="handle-tmenu arrow-drag" />
                        <a title="Wijzigen" class="add-parent" style="padding-left: {$node.level*7}px;" href="{$SiteRoot}/pages/pages.php?add=1&catoid={$node.oid}"><img style="float:left;" src="{$SiteRoot}/images/add_page.gif" alt=""  /></a> 
                        {if $node.childs|@count == 0}<a class="add-subpage" title="Wijzigen" href="{$SiteRoot}/pages/pages.php?catoid={$node.oid}&nochilds=1"><img src="{$SiteRoot}/images/add_subpage.gif" alt="" style="float:left;" /></a>{/if}
                        {if $node.childs|@count > 0}<a title="Wijzigen" class="add-subpage" href="{$SiteRoot}/pages/pages.php?paroid={$node.childs.0.oid}"><img src="{$SiteRoot}/images/add_subpage.gif" alt="" style="float:left;" /></a>{/if}
                        <a title="Wijzigen" class="parent" style="padding-left: {$node.level*7}px;" href="{$SiteRoot}/pages/pages.php?action=change&amp;oid={$node.oid}">{$node.name|stripslashes|truncate:20:'..':true}</a>
                        {if $node.childs|@count > 0}<a onclick="fold_menu(this, '{$node.oid}')"><img src="{$SiteRoot}/images/arrow_{if isset($node.fold) && $node.fold == 1}down{else}up{/if}.gif" class="arrow_{if isset($node.fold) && $node.fold == 1}down{else}up{/if}" alt="" /></a>{/if}
                        <a href="{$SiteRoot}/pages/pages.php?action=private&oid={$node.oid}&private={if $node.private == 1}0{else}1{/if}"><img src="{$SiteRoot}/images/{if $node.private == 0}enable{else}disable{/if}.gif" class="{if $node.private == 0}non-active{else}active{/if}" alt="" /></a>
                    </td>
                </tr>
            </table>  
            {if is_array($node.childs) && $node.childs|@count > 0}
            <ul class="cmenu" id="t{$node.oid}" {if isset($node.fold) && $node.fold == 1} style="display:none;"{/if}>
            {foreach from=$node.childs item=cnode key=ck}
                <li id="i_2_{$node.oid}_{$cnode.oid}" class="subpage-li {if $cnode.childs|@count > 0} white {/if} {if ($node.childs|@count - 1) == $ck  || (isset($cnode.fold) && $cnode.fold == 1 && $cnode.childs|@count > 0)}add-border-bottom{/if}">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td style="padding-left: {$cnode.level*8}px;">   
                                <img src="{$SiteRoot}/images/arrow.png" alt="move" width="9" height="9" class="handle-t{$node.oid} arrow-drag" />                                                                                                                                                                                                                                                                
                                {if $cnode.childs|@count == 0}<a class="add-subpage second" title="Wijzigen" href="{$SiteRoot}/pages/pages.php?catoid={$cnode.oid}&nochilds=1"><img src="{$SiteRoot}/images/add_subsubpage.gif" alt="" style="float:left;" /></a>{/if}
                                {if $cnode.childs|@count > 0}<a title="Wijzigen" class="add-subpage second" href="{$SiteRoot}/pages/pages.php?paroid={$cnode.childs.0.oid}"><img src="{$SiteRoot}/images/add_subsubpage.gif" alt="" style="float:left;" /></a>{/if}
                                <a class="subpage" title="Wijzigen" style="padding-left:8px;" href="{$SiteRoot}/pages/pages.php?action=change&amp;oid={$cnode.oid}">{$cnode.name|stripslashes|truncate:25:'..'}</a>
                                {if $cnode.childs|@count > 0}<a onclick="fold_menu(this, '{$cnode.oid}')"><img src="{$SiteRoot}/images/arrow_{if isset($cnode.fold) && $cnode.fold == 1}down{else}up{/if}.gif" class="arrow_{if isset($cnode.fold) && $cnode.fold == 1}down{else}up{/if}" alt="" /></a>{/if}
                                <a href="{$SiteRoot}/pages/pages.php?action=private&oid={$cnode.oid}&private={if $cnode.private == 1}0{else}1{/if}"><img src="{$SiteRoot}/images/{if $cnode.private == 0}enable{else}disable{/if}.gif" class="{if $cnode.private == 0}non-active{else}active{/if}" alt="" /></a>
                                {if ($node.childs|@count - 1) != $ck}<img class="subpage-border" src="{$SiteRoot}/images/subpage_border.jpg" alt="" />{/if}
                                
                            </td>
                        </tr>
                        <!--<tr>
                            <td style="padding-left: {$node.level*8}px;">
                                <a title="Wijzigen" style="height:1px;float:left;padding-left: 14px;" href="{$SiteRoot}/pages/pages.php?action=change&amp;oid={$node.oid}"><img style="float:left;" src="{$SiteRoot}/images/small_green_bar.jpg" alt=""  /></a>
                            </td>
                        </tr> -->
                    </table>
                    {if is_array($cnode.childs) && $cnode.childs|@count > 0}
                    <ul class="ccmenu" id="t{$cnode.oid}" {if isset($cnode.fold) && $cnode.fold == 1} style="display:none;"{/if}>
                    {foreach from=$cnode.childs item=ccnode key=cck}
                        <li id="i_3_{$cnode.oid}_{$ccnode.oid}" {if $ccnode.level == 3}class="subpage-li"{/if}>
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-left: {$ccnode.level*8}px;">    
                                        <img src="{$SiteRoot}/images/arrow.png" alt="move" width="9" height="9" class="handle-t{$cnode.oid} arrow-drag" />                                                                                                                                                                                                                                                               
                                        <a class="add-subpage second" title="Wijzigen" href="{$SiteRoot}/pages/pages.php?catoid={$ccnode.oid}&nochilds=1"><img src="{$SiteRoot}/images/add_subsubsubpage.gif" alt="" style="float:left;" /></a>
                                        <a class="subpage" title="Wijzigen" style="padding-left:8px;{if $cck != ($cnode.childs|@count - 1)}{else}{/if}" href="{$SiteRoot}/pages/pages.php?action=change&amp;oid={$ccnode.oid}">{$ccnode.name|stripslashes|truncate:25:'..'}</a>
                                        {if $ccnode.childs|@count > 0}<a onclick="fold_menu(this, '{$ccnode.oid}')"><img src="{$SiteRoot}/images/arrow_{if isset($ccnode.fold) && $ccnode.fold == 1}down{else}up{/if}.gif" class="arrow_{if isset($ccnode.fold) && $ccnode.fold == 1}down{else}up{/if}" alt="" /></a>{/if}
                                        {if $cck != ($cnode.childs|@count - 1)}<img class="subpage-border" src="{$SiteRoot}/images/subpage_border.jpg" alt="" />{/if}
                                        
                                    </td>
                                </tr>
                            </table>
                            {if is_array($ccnode.childs) && $ccnode.childs|@count > 0}
                            <ul class="ccmenu" id="t{$ccnode.oid}" {if isset($ccnode.fold) && $ccnode.fold == 1} style="display:none;"{/if}>
                            {foreach from=$ccnode.childs item=cccnode key=ccck}
                                <li id="i_4_{$ccnode.oid}_{$cccnode.oid}" {if $cccnode.level == 4}class="subsubpage-li"{/if}>
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-left: {$cccnode.level*8}px;">    
                                                <img src="{$SiteRoot}/images/arrow.png" alt="move" width="9" height="9" class="handle-t{$ccnode.oid} arrow-drag" />                                                                                                                                                                                                                                                               
                                                <a class="subpage" title="Wijzigen" style="padding-left:8px;{if $ccck != ($ccnode.childs|@count - 1)}{else}{/if}" href="{$SiteRoot}/pages/pages.php?action=change&amp;oid={$cccnode.oid}">{$cccnode.name|stripslashes|truncate:25:'..'}</a>
                                                {if $ccck != ($ccnode.childs|@count - 1)}<img class="subpage-border" src="{$SiteRoot}/images/subpage_border.jpg" alt="" />{/if}
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </li>
                            {/foreach}  
                            </ul>
                            {/if}
                        </li>
                    {/foreach}  
                    </ul>
                    {/if}
                </li>
            {/foreach}  
            </ul>
            {/if}
        </li>
        {/foreach}   
     </ul>
     <ul class="menu"> 
        <li  class="menu-bottom"> 
            <img src="{$SiteRoot}/images/menu_left_bottom_corner.jpg" class="left-bottom" alt="" />
            <img src="{$SiteRoot}/images/menu_right_bottom_corner.jpg" class="right-bottom" alt="" />
        </li>
    </ul>
    </div>
    </td>
    <td>
    {if isset($smarty.get.catoid) || isset($smarty.get.paroid) || isset($smarty.get.existCat)}
    <div class="pagina-form">
        <div id="pagina-title" class="pagina-titel">Pagina</div>
        <div id="content_in_content_div">
        <form method="post" action="" >
            <table>
                <tbody>
                {if isset($error)}
                <tr>
                    <td colspan="2">
                        De volgende velden zijn niet (goed) ingevuld: <br />
                        <ol>
                        {assign var=id value=$default_lang.id}
                        {foreach from=$error item=value}
                            <li><span style="color: #FF0000;">{$value.$id}</span></li>
                        {/foreach}
                        </ol>
                    </td>
                </tr>
                {/if}
                <tr>
                    <td style="vertical-align: top;">
                        <table style="padding-left: 2em;">
                            <!--<tr>
                                <td class="fieldName">
                                <label for="f_id_1">Foto(s)</label>: 
                                </td>
                                <td>
                                <span id="photo_area"></span>
                                <a href="#" onclick="popup('/cms/browser.php',600, 750); return false">
                                    <img src="/cms/images/image.jpg" alt="Afbeelding uploaden" />
                                </a>
                                </td>
                            </tr>-->
                            <tr>
                                <td class="fieldName">
                                    <label for="private">Private</label>:
                                </td>
                                <td>
                                    <input type="checkbox" value="1" name="private" id="private" {if isset($smarty.post.private) && $smarty.post.private == 1} checked="checked" {/if} />       
                                </td>
                            </tr>
                            {foreach from=$languages item=item key=k}
                            {assign var=id value=$item.id}
                            <tr>
                                <td class="fieldName">
                                <label for="name_{$id}">Pagina titel {$item.language}</label>: 
                                </td>
                                <td>
                                    <input class="input" type="text" name="name[{$id}]" id="name_{$id}" value="{if isset($smarty.post.name.$id)}{$smarty.post.name.$id}{/if}" size="40" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldName">
                                    <label for="slug_{$id}">Pagina naam {$item.language}</label>: 
                                </td>
                                <td>
                                    <input class="input" type="text" name="slug[{$id}]" id="slug_{$id}" value="{if isset($smarty.post.slug.$id)}{$smarty.post.slug.$id}{/if}" size="40" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldName">
                                    <label>Content {$item.language}</label>: 
                                </td>
                                <td>
                                    {$FCKeditor.$id}
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldName">
                                    <label for="page_description_{$id}">Pagina omschrijving {$item.language}</label>:
                                </td>
                                <td>
                                    <textarea name="page_description[{$id}]" id="page_description_{$id}">{if isset($smarty.post.page_description.$id)}{$smarty.post.page_description.$id}{/if}</textarea>
                                </td>
                            </tr>
                            {/foreach}
                            {if isset($keywords)}
                            <tr>
                                <td class="fieldName">
                                    Keywords    
                                </td>
                                <td>
                                    {foreach from=$keywords item=item key=k}
                                    {$k} - {$item}<br />
                                    {/foreach}
                                </td>
                            </tr>
                            {/if}
                            <!--<tr>
                                <td class="fieldName">
                                Pagina
                                </td>
                                <td style="vertical-align: top;">
                                <select name="existCat" size="10" style="width: 25em;">
                                    {foreach from=$existsCats item=node name=couter}
                                        <option  {if $smarty.post.existCat == $node.oid} selected="selected" {/if} value="{$node.oid}" >{"&nbsp;"|str_repeat:$node.level*2}{$node.name|stripslashes|regex_replace:'#_root#':$smarty.server.SERVER_NAME|regex_replace:'#_#':''}</option>
                                    {/foreach}
                                </select>
                                </td>
                            </tr>-->
                            <tr>
                                <td style="text-align: right;">
                                </td>
                                <td>
                                    <br />
                                    <input type="hidden" name="active" value="1" />
                                    <input type="hidden" name="menuOid" value="{$nodeData.oid}" />
                                    <input id="save" src="" type="submit" class="button" value=""  name="add_to" style="cursor:pointer;" />
                                    <input type="submit" class="keywords" value="" name="keywords"  />
                                    <!--<input type="submit" class="button" value="Toevoegen onder" name="add_under" />
                                    <input type="submit" class="button" value="Toevoegen boven" name="add_up" />  -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                </tbody>
            </table>    
        </form>
        </div>
        <br class="clear" />
        <img src="{$SiteRoot}/images/pagina_bottom.jpg" alt="" class="pagina_bottom" />
    </div>
    {/if}
    </td>
    <td>
    {if $settings.functie_private == 0}
    <div class="functies">
        <div class="title">{$settings.functie_title}</div>
            {$settings.functie_description}<br class="clear" />
        <img src="{$SiteRoot}/images/functies_bottom.jpg" alt="" class="functies_bottom" />
    </div>
    {/if}
    </td>
    </tr>
    </table>
{/if}

{/strip}
