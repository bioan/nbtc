{strip}
<script type="text/javascript">
/* <![CDATA[ */{literal}
  function showDiv($divId)
  {
    var $obj = document.getElementById($divId);
    $obj.style.display = 'block';
  }
  
    function checkForm()
    {
        var obj = document.getElementById('photo_area');
        if (!obj.hasChildNodes()) {
            alert('Foto\'s toevoegen!');
            return false;    
        }
        for (var i=0; i < obj.childNodes.length; i++) {
            if (obj.childNodes.item(i).nodeName.match(/^IMG$/))
                return true;
        }
        alert('Foto\'s toevoegen!');
        return false;    
    }

      function checkIfDisabled(obj)
    {
        if (obj.options[obj.selectedIndex].disabled == true) {
            alert('Deze categorie kan niet gekozen worden omdat deze midden in de tree zit!');
            for (var i =0; i < obj.options.length; i++) {
                if (!obj.options[i].disabled) {
                    obj.options[i].selected = true;
                    return;
                }
            }
            
        }
    }
  {/literal}
/* ]]> */
</script>
{if $smarty.get.action == 'new'}
    <form method="post" action="" enctype="multipart/form-data">
        <table style="width: 100%;">
            <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="2">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName" style="width:200px;">
                    <label for="private">Private</label>: 
                </td>
                <td style="width:200px;">
                    <input type="checkbox" class="input" name="private" id="private" value="1" {if isset($smarty.post.private)} checked="checked" {/if} size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName" style="width:200px;">
                    <label for="language">Name</label>: 
                </td>
                <td style="width:200px;">
                    <input type="text" class="input" name="language" id="language" value="{if isset($smarty.post.language)}{$smarty.post.language}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="short_name">Short Name</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="short_name" id="short_name" value="{if isset($smarty.post.short_name)}{$smarty.post.short_name}{/if}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label>Image</label>: 
                </td>
                <td>
                    <input type="file" name="image"  value="" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br />
                    <a href="#" onclick="document.forms[0].submit();" class="save"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="" /></a>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
{elseif $smarty.get.action == 'overview'}
    Rijen weergeven :&nbsp;
    <select onchange="window.location='language.php?action=overview&items=' + this.value + '{if isset($smarty.get.sort)}&sort={$smarty.get.sort}{/if}{if isset($smarty.get.sorttype)}&sorttype={$smarty.get.sorttype}{/if}'">
        <option value="5" {if isset($items) && $items == 5} selected="selected"{/if}>5</option>
        <option value="10" {if isset($items) && $items == 10} selected="selected"{/if}>10</option>
        <option value="25" {if isset($items) && $items == 25} selected="selected"{/if}>25</option>
        <option value="50" {if isset($items) && $items == 50} selected="selected"{/if}>50</option>
        <option value="100" {if isset($items) && $items == 100} selected="selected"{/if}>100</option>
        <option value="10000" {if isset($items) && $items == 10000} selected="selected"{/if}>all</option>
    </select>
    <table style="width: 100%;" id="t1" class="news" cellpadding="2" cellspacing="0">
        <colgroup style="width: 40%"></colgroup>
        <colgroup style="width: 41%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        
        <tbody>
            <tr>
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr>
                <td class="first-row-first-column-white">
                    <a href="{$SiteRoot}language/language.php?action=overview&items={$items}&sort=language&sorttype={if $sorttype == 'desc'}asc{else}desc{/if}&pageID={$pageID}">Name</a>
                </td>
                <td class="first-row-column-white">
                </td>
                <td class="first-row-last-column">
                </td>
            </tr>
            {if !$langs}
                <tr>
                    <td class="first-column-gray">
                    Er zijn nog geen nieuws items gepost.    
                    </td>
                    <td class="column-gray">
                    
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            {foreach from=$langs item=item name="iCounter"}
                <tr id="{$item.id}" ondblclick="window.location.href = '{$SiteRoot}language/language.php?action=edit&id={$item.id}'" class="pointer">
                    {if $smarty.foreach.iCounter.iteration % 2 == 1} 
                    <td class="first-column-gray">
                        {$item.language|stripslashes}
                    </td>
                    <td class="column-gray">
                    </td>
                    <td class="last-column-gray">
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$item.language|stripslashes}
                    </td>
                    <td class="column-white">
                    </td>
                    <td class="last-column-white">
                    </td>
                    {/if}
                </tr>
            {/foreach}
            <tr>
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="padding-top: 0.6em; text-align: center;">
        {$links}
    </div>
{elseif $smarty.get.action == 'edit'}
    <form method="post" action="" enctype="multipart/form-data">
        <table style="width: 100%;">
            <colgroup style="width: 20%"></colgroup>
            <colgroup style="width: 80%"></colgroup>
            <tbody>
            {if isset($error)}
            <tr>
                <td colspan="3">
                    De volgende velden zijn niet (goed) ingevuld: <br />
                    <ol>
                    {foreach from=$error item=value}
                        <li><span style="color: #FF0000;">{$value}</span></li>
                    {/foreach}
                    </ol>
                </td>
            </tr>
            {/if}
            <tr>
                <td class="fieldName">
                    <label for="private">Private</label>: 
                </td>
                <td>
                    <input type="checkbox" name="private" id="private" value="1" {if $values.private == 1} checked="checked" {/if} />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="language">Name</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="language" id="language" value="{$values.language|stripslashes}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label for="short_name">Short Name</label>: 
                </td>
                <td>
                    <input type="text" class="input" name="short_name" id="short_name" value="{$values.short_name}" size="40" />
                </td>
            </tr>
            <tr>
                <td class="fieldName">
                    <label>Image</label>: 
                </td>
                <td>
                    <input type="file" name="image"  value="" />
                </td>
            </tr>
            <tr>
                <td class="fieldName" colspan="2">
                    <a href="language.php?action=texts&id={$values.id}">Standaard teksten aanpassen</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br />
                    <input class="button" type="submit" name="submit" id="save" value="" />
                    {if $languages|count > 1}
                    <a class="verwijderen" href="{$SiteRoot}/language/language.php?action=delete&amp;id={$values.id}" onclick="return confirm('Weet je zeker dat je taalset wil verwijderen? Hiermee verwijder je alle inhoud'); return false;"></a>
                    {/if}
                </td>
            </tr>
            </tbody>
        </table>
    </form>
{elseif $smarty.get.action == 'stats'}
    <table style="width: 100%;" id="t1" class="news" cellpadding="2" cellspacing="0">
        <colgroup style="width: 40%"></colgroup>
        <colgroup style="width: 41%"></colgroup>
        <colgroup style="width: 19%"></colgroup>
        
        <tbody>
            <tr>
                <td colspan="3" class="head">
                    <img src="{$SiteRoot}/images/head_left.jpg" id="head_left" alt="" />
                    <h2 class="font">Overzicht</h2>
                    <img src="{$SiteRoot}/images/head_right.jpg" id="head_right" alt="" />    
                </td>
            </tr>
            <tr>
                <td class="first-row-first-column-white">
                    Name
                </td>
                <td class="first-row-column-white">
                    Progress
                </td>
                <td class="first-row-last-column">
                </td>
            </tr>
            {if !$languages}
                <tr>
                    <td class="first-column-gray">
                    Er zijn nog geen nieuws items gepost.    
                    </td>
                    <td class="column-gray">
                    
                    </td>
                    <td class="last-column-gray">
                    </td>
                </tr>
            {/if}
            
            {if !isset($smarty.get.lang)}
            
            {foreach from=$languages item=item name="iCounter"}
                <tr id="{$item.id}" class="pointer" onclick="window.location.href = '{$SiteRoot}language/language.php?action=stats&lang={$item.id}'">
                    {if $smarty.foreach.iCounter.iteration % 2 == 1} 
                    <td class="first-column-gray">
                        {$item.language|stripslashes}
                    </td>
                    <td class="column-gray">
                        <span class="{if $item.filled != $item.all}red{else}green{/if}">{$item.filled} / {$item.all}</span>
                    </td>
                    <td class="last-column-gray">
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$item.language|stripslashes}
                    </td>
                    <td class="column-white">
                        <span class="{if $item.filled != $item.all}red{else}green{/if}">{$item.filled} / {$item.all}</span>
                    </td>
                    <td class="last-column-white">
                    </td>
                    {/if}
                </tr>
            {/foreach}
            
            {elseif isset($smarty.get.lang) && !isset($smarty.get.module)}
            
            {assign var=id value=$smarty.get.lang}
            {foreach from=$languages.$id.modules item=item name="iCounter" key=k}
                <tr class="pointer" onclick="window.location.href = '{$SiteRoot}language/language.php?action=stats&lang={$id}&module={$k}'">
                    {if $smarty.foreach.iCounter.iteration % 2 == 1} 
                    <td class="first-column-gray">
                        {$k}
                    </td>
                    <td class="column-gray">
                        <span class="{if $item.filled != $item.all}red{else}green{/if}">{$item.filled} / {$item.all}</span>
                    </td>
                    <td class="last-column-gray">
                    </td>
                    {else}
                    <td class="first-column-white">
                        {$k}
                    </td>
                    <td class="column-white">
                        <span class="{if $item.filled != $item.all}red{else}green{/if}">{$item.filled} / {$item.all}</span>
                    </td>
                    <td class="last-column-white">
                    </td>
                    {/if}
                </tr>
            {/foreach}
            
            {elseif isset($smarty.get.module)}
            
            {assign var=id value=$smarty.get.lang}
            {assign var=module value=$smarty.get.module}
            {foreach from=$languages.$id.modules.$module.items item=item name="iCounter" key=k}
                <tr class="pointer" onclick="window.location.href = '{if isset($item.bitem)}{$languages.$id.modules.$module.item}{else}{$languages.$id.modules.$module.cat}{/if}{if $module != 'pages'}id={if !isset($item.id)}{$item.oid}{else}{$item.id}{/if}{else}oid={$item.oid}{/if}'">
                    {if $smarty.foreach.iCounter.iteration % 2 == 1} 
                    <td class="first-column-gray">
                        {if $module != 'pages'}{if isset($item.title)}{$item.title}{elseif isset($item.group_name)}{$item.group_name}{else}{$item.name}{/if}{else}{$item.name}{/if}
                    </td>
                    <td class="column-gray">
                        <span class="{if !isset($item.$id.filled)}red{else}green{/if}">{if !isset($item.$id.filled)}not filled{else}filled{/if}</span>
                    </td>
                    <td class="last-column-gray">
                    </td>
                    {else}
                    <td class="first-column-white">
                        {if $module != 'pages'}{if isset($item.title)}{$item.title}{elseif isset($item.group_name)}{$item.group_name}{else}{$item.name}{/if}{else}{$item.name}{/if}
                    </td>
                    <td class="column-white">
                        <span class="{if !isset($item.$id.filled)}red{else}green{/if}">{if !isset($item.$id.filled)}not filled{else}filled{/if}</span>
                    </td>
                    <td class="last-column-white">
                    </td>
                    {/if}
                </tr>
            {/foreach}
            
            {/if}
            <tr>
                <td class="last-row-first-column">
                </td>
                <td class="last-row-column-white">
                </td>
                <td class="last-row-last-column">
                </td>
            </tr>
        </tbody>
    </table>
{elseif $smarty.get.action == 'texts' && isset($smarty.get.id)}
    <form method="post" action="">
        <table style="width: 100%;">
            <colgroup style="width: 30%"></colgroup>
            <colgroup style="width: 70%"></colgroup>
            <tbody>
            {foreach from=$texts item=item key=k}
            <tr>
                <td class="fieldName" style="width:200px;">
                    <label for="{$k}">{$k|replace:'_':' '}</label>: 
                </td>
                <td style="width:200px;">
                    <input type="text" class="input" name="{$k}" id="{$k}" value="{$item}"  size="60" />
                </td>
            </tr>
            {/foreach}
            <tr>
                <td></td>
                <td>
                    <br />
                    <a href="#" onclick="document.forms[0].submit();" class="save"><img name="verzenden" src="{$SiteRoot}/images/save.png" alt="" /></a>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
{/if}
{/strip}
