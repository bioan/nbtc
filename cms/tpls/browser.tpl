{*
	Default template for the images manage system of inforitus
	
	$author: info@inforitus.nl
	@copyright: inforitus.nl
	@website: http://www.inforitus.nl
	@date: 26-12-2006
*}
{strip}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl">
<head>
<title>inforitus image browser</title> <!--@import url("http://www.inforitus.nl/cms/css/default.css");-->
<style type="text/css" media="all">@import url("{$SiteRoot}/css/default.css");</style>
<style type="text/css">{literal}
/* <![CDATA[ */
 body {
 	margin: 0px;
 	padding: 0px;
 }
 
 #photoPlace {
 	background-color: #F5F5F5;
 }
 
 button {
 	border: black 1px solid;
 	background-color: #059EFE;
 }

{/literal}/* ]]> */
</style>
<script src="{$SiteRoot}/js/jquery.min.js" type="text/javascript"></script>
<script src="{$SiteRoot}/js/browser.js" type="text/javascript"></script>
</head>
<body>
    <div class="browser">
        <ul class="folders">
            <li><img src="../images/folder.gif" alt="" /> <a href="{$SiteRoot}/browser.php?module=agenda" {if $module == 'agenda'}class="selected"{/if}>agenda</a></li>
            <li><img src="../images/folder.gif" alt="" /> <a href="{$SiteRoot}/browser.php?module=news" {if $module == 'news'}class="selected"{/if}>news</a></li>
            <li><img src="../images/folder.gif" alt="" /> <a href="{$SiteRoot}/browser.php?module=photoalbum" {if $module == 'photoalbum'}class="selected"{/if}>photoalbum</a></li>
            <li><img src="../images/folder.gif" alt="" /> <a href="{$SiteRoot}/browser.php?module=videoalbum" {if $module == 'videoalbum'}class="selected"{/if}>videoalbum</a></li>
            <li><img src="../images/folder.gif" alt="" /> <a href="{$SiteRoot}/browser.php?module=webshop" {if $module == 'webshop'}class="selected"{/if}>webshop</a></li>
        </ul>
        <ul class="images">
        {foreach from=$images item=image key=k}
            <li id="image_{$k}">
                <img src="../{$image.url}" onclick="parent.setPhoto ({$k}, '../{$image.url}');parent.GB_hide();" alt="" style="cursor:pointer" />
                <img src="../images/delete.png" alt="Afbeelding verwijderen" style="cursor:pointer;" onclick="delPhoto ({$k}, '{$module}')" />
            </li>
        {/foreach}
    </div>
</body>
</html>
{/strip}
