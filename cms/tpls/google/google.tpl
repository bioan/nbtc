<form action="" method="post">
    <table cellspacing="0" cellpadding="0" style="100%">
        <colgroup style="width: 20%"></colgroup>
        <colgroup style="width: 80%"></colgroup>
        <tr>
            <td class="fieldTitle" colspan="2">
               1. Google Connection
            </td>
        </tr>
        {if isset($appErrors.app_name)}
        <tr>
            <td class="error" colspan="2">
               {$appErrors.app_name}
            </td>
        </tr>
        {/if}
        <tr>
            <td class="fieldName">
                Application Name:
            </td>
            <td>
                {if isset($smarty.post.app_name)}
                    {assign var="appName" value=$smarty.post.app_name}
                {elseif isset($appValues.app_name)}
                    {assign var="appName" value=$appValues.app_name}
                {else}
                    {assign var="appName" value=""}
                {/if}
                <input type="text" class="input" name="app_name" value="{$appName}" size="30">
            </td>
        </tr>
        {if isset($appErrors.client_id)}
        <tr>
            <td class="error" colspan="2">
               {$appErrors.client_id}
            </td>
        </tr>
        {/if}
        <tr>
            <td class="fieldName">
                Client ID:
            </td>
            <td>
                {if isset($smarty.post.client_id)}
                    {assign var="clientID" value=$smarty.post.client_id}
                {elseif isset($appValues.client_id)}
                    {assign var="clientID" value=$appValues.client_id}
                {else}
                    {assign var="clientID" value=""}
                {/if}
                <input type="text" class="input" name="client_id" value="{$clientID}" size="30">
            </td>
        </tr>
        {if isset($appErrors.client_secret)}
        <tr>
            <td class="error" colspan="2">
               {$appErrors.client_secret}
            </td>
        </tr>
        {/if}
        <tr>
            <td class="fieldName">
                Client Secret:
            </td>
            <td>
                {if isset($smarty.post.client_secret)}
                    {assign var="clientSecret" value=$smarty.post.client_secret}
                {elseif isset($appValues.client_secret)}
                    {assign var="clientSecret" value=$appValues.client_secret}
                {else}
                    {assign var="clientSecret" value=""}
                {/if}
                <input type="text" class="input" name="client_secret" value="{$clientSecret}" size="30">
            </td>
        </tr>
        {if isset($appErrors.redirect_uri)}
        <tr>
            <td class="error" colspan="2">
               {$appErrors.redirect_uri}
            </td>
        </tr>
        {/if}
        <tr>
            <td class="fieldName">
                Redirect Url:
            </td>
            <td>
                {if isset($smarty.post.redirect_uri)}
                    {assign var="redirectUrl" value=$smarty.post.redirect_uri}
                {elseif isset($appValues.redirect_uri)}
                    {assign var="redirectUrl" value=$appValues.redirect_uri}
                {else}
                    {assign var="redirectUrl" value=""}
                {/if}
                <input type="text" class="input" name="redirect_uri" value="{$redirectUrl}" size="30">
            </td>
        </tr>
        {if isset($appErrors.developer_key)}
        <tr>
            <td class="error" colspan="2">
               {$appErrors.developer_key}
            </td>
        </tr>
        {/if}
        <tr>
            <td class="fieldName">
                Developer key:
            </td>
            <td>
                {if isset($smarty.post.developer_key)}
                    {assign var="developerKey" value=$smarty.post.developer_key}
                {elseif isset($appValues.developer_key)}
                    {assign var="developerKey" value=$appValues.developer_key}
                {else}
                    {assign var="developerKey" value=""}
                {/if}
                <input type="text" class="input" name="developer_key" value="{$developerKey}" size="30">
            </td>
        </tr>
        <tr>
            <td class="fieldName" colspan="2">
                <input type="submit" value="Save Changes" name="saveChangesApp" />
                {if isset($url) && $url != ''}
                <a href="{$url}" title="connect to Google">Grant permissions</a>
                {else}
                You have access for Google application
                {/if}
           </td>
        </tr>
    </table>
</form>
{if isset($user)}
<table>
    <tr>
        <td class="fieldTitle" colspan="2">
           2. Google User Data
        </td>
    </tr>
    {foreach from=$user item=prop key=k}
    <tr>
        <td class="fieldName">
            {$k}
        </td>
        <td>
            {if $k=='picture'}
                <img src="{$prop}" alt="" />
            {else}
                {$prop}
            {/if}
        </td>
    </tr>
    {/foreach}
</table>
{/if}