<?php
######### COPYRIGHT #################
 
/*
	ALL RIGHTS RESERVED
	homepage: http://inforitus.nl
	file: setting
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Forum.class.php');

$oComment = new Forum ();

$aFolderData = array (
        'url' => 'configuratie.gif',
        'text' => 'Forum',
        'page' => 'forum'
);

if (!empty ($_POST)) 
{
        if ($oComment -> updateSettings ($_POST))
                redirect ($SiteRoot . '/forum/settings.php?action=new'); 
}

$aSettings = $oComment -> getSettings ();

$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'updateForm.js'
                                )
                            );
                            

$oTemplate -> assign ('settings', $aSettings);
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'forum/settings.tpl');
$oTemplate -> display ('default.tpl');
?>
