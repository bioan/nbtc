<?php
######### COPYRIGHT #################
 
/*
    homepage: http://inforitus.nl
    file: news
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/News.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Group.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/User.class.php');

$aFolderData = array(
        'url' => 'nieuws.gif',
        'text' => 'Nieuws',
        'page' => 'news'
);

$aActions = array(
        1 => 'new',
        2 => 'overview',
        3 => 'edit',
        4 => 'delete'
);

$oNews      = new News ();
$oUser      = new User ();
$oNewsGroup = new Group ('news');
$oLanguage  = new Language ();
$oCKeditor  = new CKEditor () ;

$oCKeditor -> basePath          = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/';
$oCKeditor -> returnOutput      = true;
$oCKeditor -> config ['width']  = 590; 
$oCKeditor -> config ['height'] = 300; 
$oCKeditor -> config ['extraAllowedContent'] = 'img[alt,border,width,height,align,vspace,hspace,!src];';
$oCKeditor -> config ['allowedContent'] = true;

$aConfig = array ();
$aConfig ['toolbar'] = array (
        array ( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),
        array ( 'Image', 'Link', 'Unlink', 'Anchor', 'MediaEmbed' )
);

$oCKeditor -> config ['filebrowserBrowseUrl']      = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserImageBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserFlashBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserUploadUrl']      = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=File';
$oCKeditor -> config ['filebrowserImageUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
$oCKeditor -> config ['filebrowserFlashUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        $aLanguages = $oLanguage -> getLanguages ();
        
        $oTemplate -> assign ('languages', $aLanguages);
        
        if ($_GET ['action'] == 'new') 
        {
                $aContent = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("description[" . $aLanguages [$i]['id'] . "]", (isset ($_POST ['description'][$aLanguages [$i]['id']]) ? $_POST ['description'][$aLanguages [$i]['id']] : ''), $aConfig);                 
                }
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent);
                $oTemplate -> assign ('NewsGroups', $oNewsGroup -> getGroups ());          
                
                if (isset ($_POST ['keywords']))
                {
                        $aKeywords = keywords ($_POST ['description']);
                        
                        $oTemplate -> assign ('keywords', $aKeywords);
                }
                elseif (!empty ($_POST)) 
                {
                        $_POST ['userId'] = $_SESSION ['user_id']; 
                        if ($ok && isset($_POST ['postFacebook']))
                        { 
                                $_POST ['facebook'] = 1;
                                $_POST ['publish_id'] = $aFacebookUser ['publishID'];
                                $_POST ['publish_accessToken'] = $aFacebookUser ['publishAccessToken'];
                        }

                        $link = ($_POST ['link'] != '' ? $_POST ['link'] : 'http://' . $_SERVER ['SERVER_NAME'] . '/news/' . $oNews -> urlize($_POST ['title'][$aLanguages [0]['id']]) . '.html' );

                        if ($oNews -> addNews ($_POST)) 
                        {
                               $aNews = $oNews -> getNews (false, 0,  $oNews -> urlize ($_POST ['title'][$aLanguages [0]['id']]));
                            
                               //post on Twitter
                                if ($okTwitter && isset ($_POST ['twitter_text']) && $_POST ['twitter_text'] != '' && isset ($_POST ['postTwitter']) && $today == $_POST ['date_start'])
                                {                                      
                                        $parameters = array('status' => $_POST ['twitter_text']);
                                        $status = $connection->post('statuses/update', $parameters);
                                }
                                
                                //post on Linkedin
                                if ($okLinkedin && isset ($_POST ['postLinkedin']) && $today == $_POST ['date_start'])
                                {         
                                        $content = array ('comment' => substr (strip_tags($_POST ['description'][$aLanguages [0]['id']]), 0, 100),
                                                          'title' => $_POST ['title'][$aLanguages [0]['id']],
                                                          'submitted-url' => $link,
                                                          'submitted-image-url' => $GLOBALS ['cfg']['sWebsiteUrl'].$aNews [$aLanguages [0]['id']]['photos'][0]['image_name'],
                                                          'description' => substr (strip_tags($_POST ['description'][$aLanguages [0]['id']]), 0, 100) 
                                                         );
                                        $statusLinkedin = $connectionLinkedin ->share('new', $content);
                                }
                                
                                //post on Facebook
                                if ($ok && isset($_POST ['postFacebook']) && $today == $_POST ['date_start'])
                                {   
                                        $aData = array (0 => $aFacebookUser ['publishID']);
                                        $ch = curl_init ();
                                        
                                        curl_setopt ($ch, CURLOPT_URL, "https://graph.facebook.com/" . $aFacebookUser ['publishID'] . "/feed");
                                        $data = array (
                                                'access_token' => $aFacebookUser ['publishAccessToken'], 
                                                'message' => substr (strip_tags($_POST ['description'][$aLanguages [0]['id']]), 0, 100),
                                                'picture' => $GLOBALS ['cfg']['sWebsiteUrl'].$aNews [$aLanguages [0]['id']]['photos'][0]['image_name'],
                                                'link' => $link,
                                                'name' => $_POST ['title'][$aLanguages [0]['id']],
                                                'description' => substr (strip_tags($_POST ['description'][$aLanguages [0]['id']]), 0, 100),
                                                //'privacy' => json_encode (array ('value' => 'CUSTOM', 'friends' => 'SELF')),
                                                'tags' => json_encode ($aData),
                                                'place' => 111777152182368,
                                                //'to' => json_encode (array ('data' => $aData)),
                                                //'message_tags' => json_encode (array ('data' => $aData)),
                                                'from' => json_encode (array ('name' => $aData [0]['name'], 'id' => $aData [0]['id']))
                                        );

                                        curl_setopt($ch, CURLOPT_POST, 1);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                                        $result = curl_setopt ($ch, CURLOPT_POSTFIELDS, $data);

                                        // grab URL and pass it to the browser
                                        $result = curl_exec ($ch);
                                        // close cURL resource, and free up system resources
                                        curl_close ($ch);
                                }
                                redirect ($SiteRoot . '/news/news.php?action=overview');  
                        } 
                        else 
                        {
                                
                                $oTemplate -> assign ('error', $oNews -> getLastError ());
                        }
                } 
        }  
        elseif ($_GET ['action'] == 'overview') 
        {
                require_once 'Pager/Pager.php';  
                
                $oTemplate -> assign ('news_overview', true);
                
                $aParams = array (
                        'mode'                  => 'Sliding',
                        'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 50), //show 50 items per page 
                        'delta'                 => 3,
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
                        'delta'                 => 2,
                        'clearIfVoid'           => false,
                        'itemData'              => $oNews -> getNews (true, 0, '', 0, '', (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                );
                
                $oPager = & Pager::factory ($aParams);
                $aData  =& $oPager -> getPageData ();
                foreach ($aData as $key => $data)
                {
                        if(isset ($data ['userId']))
                        {
                                $user = $oUser -> getUsers ($data ['userId']); 
                                $aData [$key]['user'] = $user;
                        } 
                }
                
                $oTemplate -> assign ('links', $oPager -> links);
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
                $oTemplate -> assign ('news', $aData);
            
        } 
        elseif ($_GET ['action'] == 'edit') 
        {
                $aNieuwsById = $oNews -> getNews (true, $_GET ['id']);
                
                $aContent = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("description[" . $aLanguages [$i]['id'] . "]", (isset ($aNieuwsById [$aLanguages [$i]['id']]['description']) ? html_entity_decode ($aNieuwsById [$aLanguages [$i]['id']]['description']) : ''), $aConfig);                 
                }
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent);     
                $oTemplate -> assign ('values', $aNieuwsById);
                $oTemplate -> assign ('NewsGroups', $oNewsGroup -> getGroups ());     
                
                if (isset ($_POST ['keywords']))
                {
                        $aKeywords = keywords ($_POST ['description']);
                        
                        $oTemplate -> assign ('keywords', $aKeywords);
                }
                elseif (!empty ($_POST)) 
                {
                        $_POST ['userId'] = $_SESSION ['user_id'];

                        if ($ok && isset($_POST ['postFacebook']))
                        { 
                                $_POST ['facebook'] = 1;
                                $_POST ['publish_id'] = $aFacebookUser ['publishID'];
                                $_POST ['publish_accessToken'] = $aFacebookUser ['publishAccessToken'];
                        }
                        
                        if ($oNews -> updateNews ($_GET ['id'], $_POST)) 
                        {
                                $aNews = $oNews -> getNews (false, 0,  $oNews -> urlize ($_POST ['title'][$aLanguages [0]['id']]));
                                $link = ($_POST ['link'] != '' ? $_POST ['link'] : 'http://' . $_SERVER ['SERVER_NAME'] .  '/news/' .  $oNews -> urlize($_POST ['title'][$aLanguages [0]['id']]) . '.html' );
                                //post on Twitter
                                if ($okTwitter && isset ($_POST ['twitter_text']) && $_POST ['twitter_text'] != '' && isset ($_POST ['postTwitter']) && $today == $_POST ['date_start'])
                                {                                      
                                        $parameters = array('status' => $_POST ['twitter_text']);
                                        $status = $connection->post('statuses/update', $parameters);
                                }
                                
                                //post on Linkedin
                                if ($okLinkedin && isset ($_POST ['postLinkedin']) && $today == $_POST ['date_start'])
                                {     
                                       
                                        
                                        $content = array (
                                                          'comment' => substr (strip_tags($_POST ['description'][$aLanguages [0]['id']]), 0, 100),
                                                          'title' => $_POST ['title'][$aLanguages [0]['id']],
                                                          'submitted-url' => $link,
                                                          'submitted-image-url' => $GLOBALS ['cfg']['sWebsiteUrl'].$aNews [$aLanguages [0]['id']]['photos'][0]['image_name'],
                                                          'description' => substr (strip_tags($_POST ['description'][$aLanguages [0]['id']]), 0, 100) 
                                                         );
                                        $statusLinkedin = $connectionLinkedin ->share('new', $content);
                                }
                                
                                //post on Facebook
                                if ($ok && isset($_POST ['postFacebook']) && $today == $_POST ['date_start'])
                                {   
                                        $aData = array (0 => $aFacebookUser ['publishID']);
                                        $ch = curl_init ();
                                        
                                        curl_setopt ($ch, CURLOPT_URL, "https://graph.facebook.com/" . $aFacebookUser ['publishID'] . "/feed");
                                        $data = array (
                                                'access_token' => $aFacebookUser ['publishAccessToken'], 
                                                'message' => substr (strip_tags($_POST ['description'][$aLanguages [0]['id']]), 0, 100),
                                                'picture' => $GLOBALS ['cfg']['sWebsiteUrl'].$aNews [$aLanguages [0]['id']]['photos'][0]['image_name'],
                                                'link' => $link,
                                                'name' => $_POST ['title'][$aLanguages [0]['id']],
                                                'description' => substr (strip_tags($_POST ['description'][$aLanguages [0]['id']]), 0, 100),
                                                //'privacy' => json_encode (array ('value' => 'CUSTOM', 'friends' => 'SELF')),
                                                'tags' => json_encode ($aData),
                                                'place' => 111777152182368,
                                                //'to' => json_encode (array ('data' => $aData)),
                                                //'message_tags' => json_encode (array ('data' => $aData)),
                                                'from' => json_encode (array ('name' => $aData [0]['name'], 'id' => $aData [0]['id']))
                                        );

                                        curl_setopt($ch, CURLOPT_POST, 1);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                                        $result = curl_setopt ($ch, CURLOPT_POSTFIELDS, $data);

                                        // grab URL and pass it to the browser
                                        $result = curl_exec ($ch);
                                        // close cURL resource, and free up system resources
                                        curl_close ($ch);
                                }
                                redirect ($SiteRoot . '/news/news.php?action=overview'); 
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oNews -> getLastError ());
                        }
                    
                } 
        } 
        elseif ($_GET ['action'] == 'delete') 
        {
                if ($oNews -> deleteNews ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/news/news.php?action=overview');
                }
        }
     
} 
else 
{
        redirect ($SiteRoot . '/news/news.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'updateForm.js',
                                'jquery-ui-1.8.17.custom.min.js',
                                ($_GET ['action'] == 'overview' ? 'tablednd.js' : ''),
                                ($_GET ['action'] == 'overview' ? 'sort_table.js' : ''),
                                'ajaxupload.3.5.js',
                                'greybox.js',
                                'tooltip.js',
                                'upload.js'
                                )
                            );
              
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 50));              
$oTemplate -> assign ('cssBody', array ('css/jquery-ui-1.8.17.custom.css', 'css/greybox.css'));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'news/news.tpl');

$oTemplate -> display ('default.tpl');
?>
