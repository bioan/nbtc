<?php
######### COPYRIGHT #################
 
/*
    homepage: http://inforitus.nl
    file: comments
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Forum.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/News.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Group.class.php');

$aFolderData = array(
        'url' => 'nieuws.gif',
        'text' => 'Comments',
        'page' => 'news'
);

$aActions = array(
        1 => 'new',
        2 => 'overview',
        3 => 'edit',
        4 => 'delete',
        5 => 'overview-news'
);

$oNews      = new News ();
$oComment   = new Forum ();
$oLanguage  = new Language ();
$oCKeditor  = new CKEditor () ;

$oCKeditor -> basePath          = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/';
$oCKeditor -> returnOutput      = true;
$oCKeditor -> config ['width']  = 590; 
$oCKeditor -> config ['height'] = 300; 
$oCKeditor -> config ['extraAllowedContent'] = 'img[alt,border,width,height,align,vspace,hspace,!src];';
$oCKeditor -> config ['allowedContent'] = true;

$aConfig = array ();
$aConfig ['toolbar'] = array (
        array ( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),
        array ( 'Image', 'Link', 'Unlink', 'Anchor', 'MediaEmbed' )
);

$oCKeditor -> config ['filebrowserBrowseUrl']      = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserImageBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserFlashBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserUploadUrl']      = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=File';
$oCKeditor -> config ['filebrowserImageUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
$oCKeditor -> config ['filebrowserFlashUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        $aLanguages = $oLanguage -> getLanguages ();
        
        $oTemplate -> assign ('languages', $aLanguages);
        
        if ($_GET ['action'] == 'overview') 
        {
                require_once 'Pager/Pager.php';  
                
                $oTemplate -> assign ('comments_overview', true);
                
                if (!isset ($_GET ['news_id']))
                {
                        $aParams = array (
                                'mode'                  => 'Sliding',
                                'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 50), //show 50 items per page 
                                'delta'                 => 3,
                                'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                                'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                                'altPrev'               => 'Vorige pagina',
                                'altNext'               => 'Volgende pagina',
                                'altPage'               => 'Pagina',
                                'separator'             => '',
                                'spacesBeforeSeparator' => 1,
                                'spacesAfterSeparator'  => 1,
                                'delta'                 => 2,
                                'clearIfVoid'           => false,
                                'itemData'              => $oNews -> getNews (true, 0, '', 0, '', (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                        );
                        
                        $oPager = & Pager::factory ($aParams);
                        $aData  =& $oPager -> getPageData ();
                        
                        
                        $oTemplate -> assign ('links', $oPager -> links);
                        $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                        $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
                        $oTemplate -> assign ('news', $aData);
                }
                elseif (isset ($_GET ['news_id']) && isset ($_GET ['lang_id']))
                {
                        $aParams = array (
                                'mode'                  => 'Sliding',
                                'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 99999), //show 50 items per page 
                                'delta'                 => 3,
                                'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                                'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                                'altPrev'               => 'Vorige pagina',
                                'altNext'               => 'Volgende pagina',
                                'altPage'               => 'Pagina',
                                'separator'             => '',
                                'spacesBeforeSeparator' => 1,
                                'spacesAfterSeparator'  => 1,
                                'delta'                 => 2,
                                'clearIfVoid'           => false,
                                'itemData'              => $oComment -> getComments (0, $_GET ['news_id'], $_GET ['lang_id'], true, false, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                        );
                        
                        $oPager = & Pager::factory ($aParams);
                        $aData  =& $oPager -> getPageData ();
                        
                        $oTemplate -> assign ('links', $oPager -> links);
                        $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                        $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
                        $oTemplate -> assign ('comments', $aData);
                }
                elseif (isset ($_GET ['news_id']) && !isset ($_GET ['lang_id']))
                {
                        $aParams = array (
                                'mode'                  => 'Sliding',
                                'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 10), //show 10 items per page 
                                'delta'                 => 3,
                                'altPrev'               => 'Vorige pagina',
                                'altNext'               => 'Volgende pagina',
                                'altPage'               => 'Pagina',
                                'separator'             => '',
                                'spacesBeforeSeparator' => 1,
                                'spacesAfterSeparator'  => 1,
                                'delta'                 => 2,
                                'clearIfVoid'           => false,
                                'itemData'              => $oLanguage -> getLanguages (0, '', (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'ASC'))
                        );
                        
                        $oPager = & Pager::factory ($aParams);
                        $aData  =& $oPager -> getPageData ();
                        
                        $oTemplate -> assign ('links', $oPager -> links);
                        $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                        $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
                        $oTemplate -> assign ('langs', $aData);
                }
            
        } 
        elseif ($_GET ['action'] == 'edit') 
        {
                $aCommentById = $oComment -> getComments ($_GET ['id'], 0, false);
                
                $aContent = $oCKeditor -> editor ("message", (isset ($aCommentById ['message']) ? html_entity_decode ($aCommentById ['message']) : ''), $aConfig);                 
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent);     
                $oTemplate -> assign ('values', $aCommentById);
                
                if (!empty ($_POST)) 
                {
                        if ($oComment -> updateComment ($_GET ['id'], $_POST)) 
                        {
                                redirect ($SiteRoot . '/news/comment.php?action=overview'); 
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oComment -> getLastError ());
                        }
                } 
        } 
        elseif ($_GET ['action'] == 'delete') 
        {
                $aComments = $oComment -> getComments (0, 0, true, false, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'));
                $iLevel = -1;
                $aDeleteIds = array ();
                $aDeleteIds[] = $_GET ['id'];
                
                foreach ($aComments as $item)
                {
                        if ($item ['id'] == $_GET ['id'])
                        {
                                $iLevel = $item ['level'];   
                        }
                        
                        if ($iLevel != -1 && $iLevel < $item ['level'])
                        {
                                $aDeleteIds[] = $item ['id'];       
                        }
                        elseif ($iLevel != -1 && $iLevel >= $item ['level'])
                        {
                                break;
                        }
                }
                
                if ($oComment -> deleteComment ($aDeleteIds)) 
                {
                        redirect ($SiteRoot . '/news/comment.php?action=overview');
                }
        }
     
} 
else 
{
        redirect ($SiteRoot . '/news/comment.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'updateForm.js',
                                'jquery-ui-1.8.17.custom.min.js',
                                'ajaxupload.3.5.js',
                                'greybox.js',
                                'upload.js'
                                )
                            );
              
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 50));              
$oTemplate -> assign ('cssBody', array ('css/jquery-ui-1.8.17.custom.css', 'css/greybox.css'));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'news/forum.tpl');

$oTemplate -> display ('default.tpl');
?>
