<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: agenda
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');      
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Forum.class.php');

$aFolderData = array (
        'url'  => 'nieuws.gif',
        'text' => 'News',
        'page' => 'news',
);

$aActions = array (
        1 => 'new',
        2 => 'overview',
        3 => 'edit',
        4 => 'delete'
);
		
$oUser = new Forum ();


if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        
	    if ($_GET ['action'] == 'new') 
        {
		        if (!empty ($_POST)) 
                {
                        if ($oUser -> addUser ($_POST)) 
                        {
                                redirect ($SiteRoot . '/news/users.php?action=overview');
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oAgenda -> getLastError ());
                        }
                }
            
        } 
        elseif ($_GET ['action'] == 'overview') 
        {
                require_once 'Pager/Pager.php';    
                
                $aParams = array (
                        'mode'                  => 'Sliding',
                        'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 50), //show 50 items per page 
                        'delta'                 => 3, 
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
                        'delta'                 => 2,
                        'clearIfVoid'           => false,
                        'itemData'              => $oUser -> getUsers (0, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                );
                
                $oPager = & Pager::factory ($aParams);
                $aData  =& $oPager -> getPageData ();
                
                $oTemplate -> assign ('links', $oPager -> links);
                $oTemplate -> assign ('users', $aData);
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));
        } 
        elseif ($_GET ['action'] == 'edit') 
        {
    	        $aUserById = $oUser -> getUsers ($_GET ['id']);
                
                $oTemplate -> assign ('values', $aUserById);
                
                if (!empty ($_POST)) 
                {
                        if ($oUser -> updateUser ($_GET ['id'], $_POST)) 
                        {
                                redirect ($SiteRoot . '/news/users.php?action=overview');
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oAgenda -> getLastError ());
                        }
                }
        } 
        elseif ($_GET ['action'] == 'delete') 
        {
                if ($oUser -> deleteUser ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/news/users.php?action=overview');
                }
        } 
} 
else 
{
        redirect ($SiteRoot . '/news/users.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', 
	    array (
                'jquery.min.js', 
	            'cufon-yui.js', 
                'Calibri_400.font.js', 
                'title.js', 
	            'updateForm.js',
                'jquery-ui-1.8.17.custom.min.js',
	            ($_GET ['action'] == 'overview' ? 'tablednd.js' : ''),
                ($_GET ['action'] == 'overview' ? 'sort_table.js' : ''),
                'ajaxupload.3.5.js',
                'greybox.js',
                'upload.js'
	    )
);
$oTemplate -> assign ('cssBody', array ('css/jquery-ui-1.8.17.custom.css', 'css/greybox.css'));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'news/users.tpl');
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 50));
$oTemplate -> display ('default.tpl');
?>
