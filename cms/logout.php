<?php
######### COPYRIGHT #################
 
/*
	ALL RIGHTS RESERVED
	homepage: http://inforitus.nl
	file: logout
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once 'includes/default.inc.php';

if (preg_match ('#^(?:https?://)?(?:www\.)?(.*?)$#', $_SERVER ['SERVER_NAME'], $matches)) 
{
	    setcookie ('user_id', '', time (), '/', $matches [1]); 
	    setcookie ('user_name', '', time (), '/', $matches [1]);
        setcookie ('home_slide', '', time (), '/', $matches [1]); 
	    
	    session_start ();
	    session_destroy ();
	    header (sprintf ('Location: %s', $SiteRoot));
} 
?> 
