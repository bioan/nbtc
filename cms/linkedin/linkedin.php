<?php
######### COPYRIGHT #################
 
/*
    homepage: http://inforitus.nl
    file: linkedin
    @author Laurentiu Ghiur <laurentiu@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Linkedin.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/OAuth/oauth.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/linkedin/linkedin.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/linkedin/secret.php');

$aFolderData ['text'] .= ' Linkedin';

$aActions = array(
        1 => 'overview',
        2 => 'new',
        3 => 'delete'
);

$oLinkedin   = new LinkedinCms ();

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        if ($_GET ['action'] == 'new')
        {                
                $aFolderData ['text'] .= ' - Connect to application';
            
                if (isset($_POST) && isset ($_POST ['saveChangesApp']))
                {
                        if ($oLinkedin ->checkApplication($_POST))
                        {
                                // display constants
                                $API_CONFIG = array(
                                        'appKey'       => $_POST ['consumer_key'],
                                        'appSecret'    => $_POST ['consumer_secret'],
                                        'callbackUrl'  => OAUTH_CALLBACK_LINKEDIN 
                                );
                                $connection = new Linkedin($API_CONFIG);
                                
                                /* Get temporary credentials. */
                                $request_token = $connection->retrieveTokenRequest();
                                
                                (!isset ($request_token ['error'])) ? $oLinkedin -> updateApplicationStatus(1) : $oLinkedin -> updateApplicationStatus(0);
                                
                                $oTemplate -> assign ('successMessage', 'Application\'s data were saved');                
                                unset($_POST);
                        }
                }
                
                $appData = $oLinkedin ->getApplication();
                
                if (!empty ($appData))
                {
                        $oTemplate -> assign ('appValues', $appData);

                        //Application ID
                        $consumer_key = $appData ['consumer_key'];
                        
                        //Application secret code
                        $consumer_secret = $appData ['consumer_secret'];
                                        
                        // create connection
                        $API_CONFIG = array(
                                'appKey'       => $consumer_key,
                                'appSecret'    => $consumer_secret,
                                'callbackUrl'  => OAUTH_CALLBACK_LINKEDIN 
                        );

                        $connection = new Linkedin($API_CONFIG);
                        
                        //linkedin user data
                        $linkedinUser = $oLinkedin ->getLinkedinUser($_SESSION ['user_id']);
                        
                        $ok = false;
                        if (!empty ($linkedinUser) && $appData ['correct'] == 1) 
                        {
                                $access_token_array = array (
                                        'oauth_token' => $linkedinUser ['oauth_token'],
                                        'oauth_token_secret' => $linkedinUser ['oauth_token_secret']
                                );
                                $connection ->setTokenAccess ($access_token_array);
                                $profile = $connection -> profile ();
                                if (isset ($profile ['error']) || empty ($profile)) {
                                        $ok = false;
                                        $connection = new Linkedin($API_CONFIG);
                                }
                                else 
                                {
                                       $ok = true;
                                }
                        }
                        
                        if ($ok)
                        {
                                $oTemplate -> assign ('errorCode', 'You have permissions for this linkedin app');
                        }
                        else
                        {
                                /* Build Linkedin object with client credentials. */
                                if (!isset ($_GET ['oauth_token']))
                                {

                                        /* Get temporary credentials. */
                                        $request_token = $connection->retrieveTokenRequest();
                                        if (!isset ($request_token ['error']))
                                        {
                                                $_SESSION['oauth_token'] = $token = $request_token['linkedin']['oauth_token'];
                                                $_SESSION['oauth_token_secret'] = $request_token['linkedin']['oauth_token_secret'];
                                                $token = $request_token['linkedin']['oauth_token'];
                                                $url = 'https://www.linkedin.com/uas/oauth/authenticate?' . 'oauth_token=' . $token;
                                                //echo $url;
                                                $oTemplate -> assign ('url', $url);
                                                $oLinkedin -> updateApplicationStatus(1);
                                        }
                                        else 
                                        {   
                                            $oLinkedin -> updateApplicationStatus(0);
                                            echo $request_token ['error'];
                                            $oTemplate -> assign ('errorCode', $request_token ['error']);
                                        }
                                }
                                else 
                                {
                                        //get Acces Token for linkedin user
                                        $access_token = $connection->retrieveTokenAccess($_GET['oauth_token'], $_SESSION['oauth_token_secret'], $_GET ['oauth_verifier']);
                                        
                                        //success case for getting Access Token
                                        if (isset ($access_token ['success']))
                                        {
                                                $aValues = array ();
                                                $aValues ['user_id'] = $_SESSION ['user_id'];
                                                $aValues ['oauth_token'] = $access_token ['linkedin']['oauth_token'];
                                                $aValues ['oauth_token_secret'] = $access_token ['linkedin']['oauth_token_secret'];

                                                /* Save the access tokens. Normally these would be saved in a database for future use. */

                                                $oLinkedin ->checkLinkedinUser($aValues);

                                                /* Remove no longer needed request tokens */
                                                unset($_SESSION['oauth_token']);
                                                unset($_SESSION['oauth_token_secret']);
                                                unset($_SESSION['access_token']);
                                                //die('aaaa');
                                                $oTemplate -> assign ('errorCode', 'You have permissions for this linkedin app');
                                        }
                                        //Error case for getting Access Token
                                        else 
                                        {
                                                $oTemplate -> assign ('errorCode', $access_token ['error']);
                                        }
                                }
                        }
                }
                else 
                {
                        $oTemplate -> assign ('url', 'false');
                }
                
        }
}     

$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'updateForm.js',
                                'jscalendar/calendar.js',
                                'jscalendar/lang/calendar-en.js',
                                'jscalendar/calendar-setup.js',
                                ($_GET ['action'] == 'overview' ? 'tablednd.js' : ''),
                                ($_GET ['action'] == 'overview' ? 'sort_table.js' : ''),
                                'ajaxupload.3.5.js',
                                'greybox.js',
                                'upload.js'
                                )
                            );

$oTemplate -> assign ('appErrors', $oLinkedin -> aError);              
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 10));              
$oTemplate -> assign ('cssBody', array ('js/jscalendar/calendar-win2k-1.css', 'css/greybox.css'));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'linkedin/linkedin.tpl');

$oTemplate -> display ('default.tpl');
?>
