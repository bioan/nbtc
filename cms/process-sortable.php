<?php
######### COPYRIGHT #################
 
/*
	ALL RIGHTS RESERVED
	homepage: http://inforitus.nl
	file: process-sortable
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once 'includes/default.inc.php';
require_once 'includes/Database.class.php';
//require_once 'includes/Tree.class.php';
//require_once 'includes/Language.class.php';

//$oMenu = new Tree ($_GET ['type_oid']);

foreach ($_GET as $k => $array)
{
        if ($k == 'type_oid')
                continue;
                
        $aGET = explode ('_', $k);
        
        $iLevel = $aGET [1];
        $iId    = $aGET [2];
        
        foreach ($_GET [$k] as $ik => $item) 
        {
                Database :: getInstance () -> query ('
                        UPDATE 
                            tree
                        SET
                            order_number = ' . ($ik + 1) . '
                        WHERE
                            oid = ' . $item . '
                ');   
        }
}
//$aChilds = $oMenu -> getChilds ($iId, '', $iLevel);

?>