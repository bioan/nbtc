<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: cats
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Setting.class.php');
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Tree.class.php';  
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/ValidateFields.class.php';    

$aActions = array (
        1 => 'new',
        2 => 'overview',
        3 => 'delete',
        4 => 'edit'
);

$aFolderData = array (
        'url'  => 'webshop.gif',
        'text' => 'Webshop categorieŽn',
        'page' => 'webshop'
);

$oSetting  = new Setting ();
$oLanguage = new Language ();
$oTree     = new Tree (1);
$oValidate = new ValidateFields ();
$oCKeditor   = new CKEditor ();

$oCKeditor -> basePath          = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/';
$oCKeditor -> returnOutput      = true;
$oCKeditor -> config ['width']  = 590; 
$oCKeditor -> config ['height'] = 300; 
$oCKeditor -> config ['extraAllowedContent'] = 'img[alt,border,width,height,align,vspace,hspace,!src];';
$oCKeditor -> config ['allowedContent'] = true;

$aConfig = array ();
$aConfig ['toolbar'] = array (
        array ( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),
        array ( 'Image', 'Link', 'Unlink', 'Anchor', 'MediaEmbed' )
);

$oCKeditor -> config ['filebrowserBrowseUrl']      = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserImageBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserFlashBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserUploadUrl']      = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=File';
$oCKeditor -> config ['filebrowserImageUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
$oCKeditor -> config ['filebrowserFlashUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';

$aLanguages = $oLanguage -> getLanguages ();
        
$aDefaultLanguage = $oLanguage -> getLanguages(0, '', 'id', 'asc', true);
        
$aRequiredFields = array (
        'name'   => array ($aDefaultLanguage ['id'] => '')
);

$aFieldsNames = array (
        'name'   => 'Page titel'
);

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        $oTemplate -> assign ('languages', $aLanguages);
        $oTemplate -> assign ('settings', $oSetting -> getSettings ());
        
        if ($_GET ['action'] == 'new') 
        {   
                $aContent = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("content[" . $aLanguages [$i]['id'] . "]", (isset ($_POST ['content'][$aLanguages [$i]['id']]) ? $_POST ['content'][$aLanguages [$i]['id']] : ''), $aConfig);                 
                }
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent);
                
                if (!empty ($_POST)) 
                {     
                        $aNode    = $oTree -> GettreeRaw ($_POST ['category']);
                        $aParents = $oTree -> getParents ($_POST ['category']);
                        $aChilds  = $oTree -> getChilds ($_POST ['category'], '', $aNode ['level'] + 1, array (), "default", true);
                        $bExist   = false;
                        
                        foreach ($aChilds as $item)
                        {
                                foreach ($oTree -> aLanguages as $lang)
                                {
                                        if ($item [$lang ['id']]['slug'] == $oTree -> urlize ($_POST ['name'][$lang ['id']]) && $_POST ['name'][$lang ['id']] != '')
                                        {
                                                $bExist = true;
                                                break;   
                                        }
                                }
                        }
                        
                        if (!$oValidate -> checkFields ($aRequiredFields) || $bExist == true)
                        {
                                foreach ($oValidate -> getLastError () as $k => $v)
                                {
                                        $oValidate -> aError [$k] = $aFieldsNames [$k];
                                }
                                
                                if ($bExist == true)
                                {
                                        $oValidate -> aError ['slug'] = 'Deze naam voor de pagina bestaat al, kies een andere \'Pagina naam\'';       
                                }
                                
                                $oTemplate -> assign ('error', $oValidate -> aError);
                        }
                        else
                        {
                                $oTree -> insert ($_POST, $_POST ['category'], 'add');
                                redirect ($SiteRoot . '/webshop/cats.php?action=overview');  
                        }    
                }
                
                $oTemplate -> assign ('Trees', $oTree -> getAllNodes (0, 0, 'lft', 'asc'));   
        } 
        elseif ($_GET ['action'] == 'overview') 
        {
                require_once 'Pager/Pager.php';
                
                $aParents = $oTree -> getAllNodes (1, 0, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'asc'));

                for ($i = 0; $i < count ($aParents); $i++)
                {       
                        $aParents [$i]['childs'] = $oTree -> getChilds ($aParents [$i]['oid'], '', 2, array (), "default", true); 
                        
                        for ($j = 0; $j < count ($aParents [$i]['childs']); $j++)
                        {
                                $aParents [$i]['childs'][$j]['childs'] = $oTree -> getChilds ($aParents [$i]['childs'][$j]['oid'], '', 3, array (), "default", true);   
                        }   
                }
                
		        $aParams = array (
		                'mode'                  => 'Sliding',
		                'perPage'               => 50, //show 50 items per page 
                        'delta'                 => 3, 
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')),
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
		                'delta'                 => 2,
		                'clearIfVoid'           => false,
		                'itemData'              => $oTree -> getAllNodes (0, 1, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'ASC'))
		        );
                
		        $oPager = & Pager::factory ($aParams);
		        $aData  =& $oPager -> getPageData ();
		        $sLinks =& $oPager -> getLinks ();
                
		        $oTemplate -> assign ('links', $oPager -> links);
		        $oTemplate -> assign ('existsCats', $aData);
                $oTemplate -> assign ('overview', true);
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1)); 
                $oTemplate -> assign ('parents', $aParents);
        } 
        elseif ($_GET ['action'] == 'delete' && is_numeric ($_GET ['id']))
        {
                if ($oTree -> deleteNode ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/webshop/cats.php?action=overview');
                }
        } 
        elseif ($_GET ['action'] == 'edit' && is_numeric ($_GET ['id'])) 
        {
                $aCategory = $oTree -> getNodeData ($_GET ['id']);
                
                if (empty ($_POST)) 
                {
                        
                } 
                else 
                {
                        $aNode = $oTree -> GettreeRaw ($_GET ['id']);
                        $aParents = $oTree -> getParents ($_GET ['id']);
                        $aChilds = $oTree -> getChilds ($aParents [0]['oid'], '', $aParents [0]['level'] + 1, array (), "default", true);
                        $bExist = false;
                        
                        foreach ($aChilds as $item)
                        {
                                foreach ($oTree -> aLanguages as $lang)
                                {
                                        if ($item [$lang ['id']]['slug'] == $oTree -> urlize ($_POST ['name'][$lang ['id']]) && $_POST ['name'][$lang ['id']] != '' && $item ['oid'] != $_GET ['id'])
                                        {
                                                $bExist = true;
                                                break;   
                                        }
                                }
                        }
                        
                        if (!$oValidate -> checkFields ($aRequiredFields) || $bExist == true)
                        {
                                foreach ($oValidate -> getLastError () as $k => $v)
                                {
                                        $oValidate -> aError [$k] = $aFieldsNames [$k];
                                }
                                
                                if ($bExist == true)
                                {
                                        $oValidate -> aError ['slug'] = 'Deze naam voor de pagina bestaat al, kies een andere \'Pagina naam\'';       
                                }
                                
                                $oTemplate -> assign ('error', $oValidate -> aError);
                        }
                        else
                        {
                                $oTree -> changeNode ($_GET ['id'], $_POST);     
                                redirect ($SiteRoot . '/webshop/cats.php?action=overview'); 
                        }   
                }
                
                $aContent = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("content[" . $aLanguages [$i]['id'] . "]", (isset ($aCategory [$aLanguages [$i]['id']]['content']) ? html_entity_decode ($aCategory [$aLanguages [$i]['id']]['content']) : ''), $aConfig);                 
                }
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent);
                
                $oTemplate -> assign ('category', $aCategory);
        } 
        else 
        {
                redirect ($SiteRoot . '/webshop/cats.php?action=overview');
        }
} 
else 
{
        redirect ($SiteRoot . '/webshop/cats.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
        'jquery.min.js', 
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js',
        'tablednd.js',
        'sort_table.js',
        'jquery-ui-1.7.1.custom.min.js',
        'pages.js.php?type_oid=1',
        'ajaxupload.3.5.js',
        'greybox.js',
        'upload.js'
        )
        );
$oTemplate -> assign ('cssBody', array ('css/greybox.css'));
        
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'webshop/cats.tpl');
$oTemplate -> display ('default.tpl');
?>
