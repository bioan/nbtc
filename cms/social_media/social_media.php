<?php
######### COPYRIGHT #################
 
/*
    homepage: http://inforitus.nl
    file: linkedin
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';


$aFolderData = array(
        'url' => 'nieuws.gif',
        'text' => 'Social Media',
        'page' => 'social_media'
);

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && isset ($_GET ['media'])) 
{
        switch ($_GET ['media'])
        {
                case 'facebook' :
                        include_once ($GLOBALS ['cfg']['include'] . '/cms/facebook/facebook.php');
                        break;
                case 'twitter' :
                        include_once ($GLOBALS ['cfg']['include'] . '/cms/twitter/twitter.php');
                        break;
                case 'linkedin' :
                        include_once ($GLOBALS ['cfg']['include'] . '/cms/linkedin/linkedin.php');
                        break;
                case 'google' :
                        include_once ($GLOBALS ['cfg']['include'] . '/cms/google/google.php');
                        break;
                default :
                        break;
                    
        }
}  
else 
{
        include_once ($GLOBALS ['cfg']['include'] . '/cms/facebook/facebook.php');
}

$oTemplate -> assign ('folder', $aFolderData);
?>
