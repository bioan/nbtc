<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: groups
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Group.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Mail.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Newsletter.class.php'); 

$aActions = array (
        1 => 'overview',
        2 => 'delete',
        3 => 'edit',
        4 => 'send_test'
);

$aFolderData = array (
        'url'  => 'groepen.png',
        'text' => 'Monitor',
        'page' => 'newsletter'
);

$oLanguage = new Language ();

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        $oNewsGroup = new Group ('templates');
        $oSGroup = new Group ('subscribers');
        $oTemplate -> assign ('groups', $oSGroup -> getGroups ());  
        $oNewsletter = new Newsletter ();
        
        $aLanguages = $oLanguage -> getLanguages ();
        
        $oTemplate -> assign ('languages', $aLanguages);
        
        if ($_GET ['action'] == 'new') 
        {
                if (!empty ($_POST)) 
                {
                        if ($oNewsGroup -> addGroup ($_POST)) 
                        {
                                redirect ($SiteRoot . '/newsletter/templates.php?action=overview');   
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oNewsGroup -> getLastError ());
                        }
                    
                }
        } 
        elseif ($_GET ['action'] == 'overview') 
        {
                if (!isset ($_GET ['id']))
                {
                        require_once 'Pager/Pager.php'; 
                         
                        $aParams = array (
                                'mode'                  => 'Sliding',
                                'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 50), //show 50 items per page 
                                'delta'                 => 3,
                                'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                                'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                                'altPrev'               => 'Vorige pagina',
                                'altNext'               => 'Volgende pagina',
                                'altPage'               => 'Pagina',
                                'separator'             => '',
                                'spacesBeforeSeparator' => 1,
                                'spacesAfterSeparator'  => 1,
                                'delta'                 => 2,
                                'clearIfVoid'           => false,
                                'itemData'              => $oNewsletter -> getMailings (0, (isset ($_GET ['template_id']) ? $_GET ['template_id'] : 0), (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                        );
                        
                        $oPager = & Pager::factory ($aParams);
                        $aData  =& $oPager -> getPageData ();
                        //print_r($aData);
                        $oTemplate -> assign ('links', $oPager -> links);
                        $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                        $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
                        $oTemplate -> assign ('newsGroup', $aData);
                }
                else
                {
                        require_once 'Pager/Pager.php'; 
                         
                        $aParams = array (
                                'mode'                  => 'Sliding',
                                'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 50), //show 50 items per page 
                                'delta'                 => 3,
                                'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                                'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                                'altPrev'               => 'Vorige pagina',
                                'altNext'               => 'Volgende pagina',
                                'altPage'               => 'Pagina',
                                'separator'             => '',
                                'spacesBeforeSeparator' => 1,
                                'spacesAfterSeparator'  => 1,
                                'delta'                 => 2,
                                'clearIfVoid'           => false,
                                'itemData'              => $oNewsletter -> getMonitor ($_GET ['id'], 0, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                        );
                        
                        $oPager = & Pager::factory ($aParams);
                        $aData  =& $oPager -> getPageData ();
                        
                        $oTemplate -> assign ('links', $oPager -> links);
                        $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                        $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
                        $oTemplate -> assign ('monitor', $aData);
                        
                        $aTemplate      = $oNewsletter -> getMailings ($_GET ['id'], '', (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'));
                        $aSubscribers   = $oNewsletter -> getSubscribers (0, $aTemplate ['group_id']);
                        $aMonitor       = $oNewsletter -> getMonitor ($_GET ['id']);
                        //print_r($aTemplate ['links']);
                        $oTemplate -> assign ('template', $aTemplate);
                        $oTemplate -> assign ('subscribers', $aSubscribers);
                        
                        if (isset ($_GET ['export']))
                        {
                                $aItems     = $oNewsletter -> getMonitor ($_GET ['id'], 0, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'));
                                $aTemplate  = $oNewsletter -> getMailings ($_GET ['id'], '', (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'));
                                
                                header ("Pragma: public");
                		        header ("Expires: 0");
                		        header ("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                		        header ("Cache-Control: private", false);
                		        header ("Content-Type: application/vnd.ms-excel");
                		        header ("Content-Disposition: attachment; fileName=gebruikers.xls;" );
                		        //header("Content-Transfer-Encoding: binary");
                		        header ("Content-Transfer-Encoding: base64");
                		        //header ("Content-Length: " . strlen ($data));
                                $sRow = "Email\t";
                                
                                foreach ($aTemplate ['links'] as $value)
                                        $sRow .= ($value ['name'] == '' ? $value ['link'] : $value ['name']) . "\t";
                                        
                                $aDataDump [] = $sRow;
                                
                                foreach ($aItems as $k => $item)
                                {
                                        $sRow = sprintf ("%s\t", trim ($item ['email']));
                                        
                                        foreach ($item ['values'] as $value)
                                        {
                                                $sRow .= sprintf ("%s\t", trim ($value));
                                        }
                                        
                                        $aDataDump [] = $sRow;
                                }
                                
                                echo implode ("\n",$aDataDump);
                                exit;
                        }
                }
                 
        } 
        elseif (($_GET ['action'] == 'delete') && is_numeric ($_GET ['id']))
        {
                if ($oNewsGroup -> deleteGroup ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/newsletter/monitor.php?action=overview');
                }
            
        } 
        else 
        {
                redirect ($SiteRoot . '/newsletter/monitor.php?action=overview');
        }
} 
else 
{
        redirect ($SiteRoot . '/newsletter/monitor.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
        'jquery.min.js',
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js',
        'tablednd.js',
        'sort_table.js')
        );
        
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 50));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'newsletter/monitor.tpl');
$oTemplate -> display ('default.tpl');
?>
