<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: groups
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Group.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php'); 

$aActions = array (
        1 => 'new',
        2 => 'overview',
        3 => 'delete',
        4 => 'edit'
);

$aFolderData = array (
        'url'  => 'groepen.png',
        'text' => 'Template',
        'page' => 'newsletter'
);

$oLanguage = new Language ();
$oCKeditor = new CKEditor ();

$oCKeditor -> basePath          = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/';
$oCKeditor -> returnOutput      = true;
$oCKeditor -> config ['width']  = 590; 
$oCKeditor -> config ['height'] = 300; 

$aConfig = array ();
$aConfig ['toolbar'] = array (
        array ( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),
        array ( 'Image', 'Link', 'Unlink', 'Anchor', 'MediaEmbed' )
);

$oCKeditor -> config ['filebrowserBrowseUrl']      = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserImageBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserFlashBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserUploadUrl']      = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=File';
$oCKeditor -> config ['filebrowserImageUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
$oCKeditor -> config ['filebrowserFlashUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';
 

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        $oNewsGroup = new Group ('subscribers');
        
        $aLanguages = $oLanguage -> getLanguages ();
        
        $oTemplate -> assign ('languages', $aLanguages);
        
        if ($_GET ['action'] == 'new') 
        {
                $aContent = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("description[" . $aLanguages [$i]['id'] . "]", (isset ($_POST ['description'][$aLanguages [$i]['id']]) ? $_POST ['description'][$aLanguages [$i]['id']] : ''), $aConfig);                 
                }
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent);
                
                if (!empty ($_POST)) 
                {
                        if ($oNewsGroup -> addGroup ($_POST)) 
                        {
                                redirect ($SiteRoot . '/newsletter/subscribers_groups.php?action=overview');   
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oNewsGroup -> getLastError ());
                        }
                    
                }
        } 
        elseif ($_GET ['action'] == 'overview') 
        {
            require_once 'Pager/Pager.php'; 
             
            $aParams = array (
                    'mode'                  => 'Sliding',
                    'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 50), //show 50 items per page 
                    'delta'                 => 3,
                    'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                    'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                    'altPrev'               => 'Vorige pagina',
                    'altNext'               => 'Volgende pagina',
                    'altPage'               => 'Pagina',
                    'separator'             => '',
                    'spacesBeforeSeparator' => 1,
                    'spacesAfterSeparator'  => 1,
                    'delta'                 => 2,
                    'clearIfVoid'           => false,
                    'itemData'              => $oNewsGroup -> getGroups (0, '', (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
            );
            
            $oPager = & Pager::factory ($aParams);
            $aData  =& $oPager -> getPageData ();
            
            $oTemplate -> assign ('overview', true);          
            $oTemplate -> assign ('links', $oPager -> links);
            $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
            $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
            $oTemplate -> assign ('newsGroup', $aData);
                 
        } 
        elseif (($_GET ['action'] == 'delete') && is_numeric ($_GET ['id']))
        {
                if ($oNewsGroup -> deleteGroup ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/newsletter/subscribers_groups.php?action=overview');
                }
            
        } 
        elseif (($_GET ['action'] == 'edit') && is_numeric ($_GET ['id'])) 
        {
                $aGroup = $oNewsGroup -> getGroups ($_GET ['id']);
                $oTemplate -> assign ('newsGroup', $aGroup);   
                
                $aContent = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("description[" . $aLanguages [$i]['id'] . "]", (isset ($aGroup [$aLanguages [$i]['id']]['description']) ? html_entity_decode ($aGroup [$aLanguages [$i]['id']]['description']) : ''), $aConfig);                 
                }
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent); 
                
                if (!empty ($_POST)) 
                {
                        if ($oNewsGroup -> changeGroup ($_GET ['id'], $_POST)) 
                        {
                                redirect ($SiteRoot . '/newsletter/subscribers_groups.php?action=overview');   
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oNewsGroup -> getLastError ());
                        }
                    
                }
        } 
        else 
        {
                redirect ($SiteRoot . '/newsletter/subscribers_groups.php?action=overview');
        }
} 
else 
{
        redirect ($SiteRoot . '/newsletter/subscribers_groups.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
        'jquery.min.js',
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js',
        'tablednd.js',
        'sort_table.js')
        );
        
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 50));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'newsletter/subscribers_groups.tpl');
$oTemplate -> display ('default.tpl');
?>
