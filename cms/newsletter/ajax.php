<?php 
require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Newsletter.class.php'); 

$oNewsletter = new Newsletter ();

if ($_GET ['action'] == 'link')
{
        $aTemplate = $oNewsletter -> getMailings ($_GET ['mailing_id']);
        $aTemplate ['links'][$_GET ['link_id']]['name'] = $_GET ['text'];
        
        Database :: getInstance () -> query ("
                UPDATE
                    newsletter_mailings
                SET
                    links  = '" . json_encode ($aTemplate ['links']) . "'
                WHERE
                    id = " . $_GET ['mailing_id'] . "
                    
        ");
        
}