<?php
ignore_user_abort(true);

######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: groups
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
set_time_limit (0);
require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Group.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Mail.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Newsletter.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php'); 
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/swiftmail/lib/swift_required.php'); 

$transport = Swift_SmtpTransport::newInstance('localhost', 25)
-> setUsername ('andrei@inforitus.nl')
-> setPassword ('Andrei88');
$mailer = Swift_Mailer::newInstance($transport);
$mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(100));
$mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(100, 10));
$numSent = 0;
$failedRecipients = array();
/*

// Create a messageasd
$message = Swift_Message::newInstance('Wonderdful Subject');
$message -> setFrom (array('info@inforitus.nl' => 'Johaszxdn Doe'));
$message -> setBody ('xcvHere is the messagzzxxce iitself');

// Send the message
$failedRecipients = array();
$numSent = 0;
$to = array('andrei.dragos88@gmail.com' => 'dddd', 
'andrei.dragos26@yahoo.com' => 'A nname', 
'ioan@inforitus.nl' => 'A nnasdfsdame',
'andrei@inforitus.nl' => 'A sdfnnasdame');

foreach ($to as $address => $name)
{
  if (is_int($address)) {
    $message->setTo($name);
  } else {
    $message->setTo(array($address => $name));
  }
  $message ->setBody('xcvHere is the messagzzxxce iitself' . $name);
  $numSent += $mailer->send($message, $failedRecipients);
}

printf("Sent %d messages\n", $numSent);
*/

$aActions = array (
        1 => 'new',
        2 => 'overview',
        3 => 'delete',
        4 => 'edit',
        5 => 'send_test',
        6 => 'private',
        7 => 'copy',
        8 => 'progress'
);

$aFolderData = array (
        'url'  => 'groepen.png',
        'text' => 'Template',
        'page' => 'template'
);

$oLanguage = new Language ();
$oCKeditor = new CKEditor ();

$oCKeditor -> basePath          = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/';
$oCKeditor -> returnOutput      = true;
$oCKeditor -> config ['width']  = 590; 
$oCKeditor -> config ['height'] = 300; 
$oCKeditor -> config ['enterMode'] = 2; 

$aConfig = array ();
$aConfig ['toolbar'] = array (
        array ( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),
        array ( 'Image', 'Link', 'Unlink', 'Anchor', 'MediaEmbed', 'BulletedList', 'TextColor' )
);

$oCKeditor -> config ['filebrowserBrowseUrl']      = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserImageBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserFlashBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserUploadUrl']      = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=File';
$oCKeditor -> config ['filebrowserImageUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
$oCKeditor -> config ['filebrowserFlashUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';
 

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        $oNewsGroup = new Group ('templates');
        $oSGroup = new Group ('subscribers');
        $oTemplate -> assign ('groups', $oSGroup -> getGroups ());  
        $oNewsletter = new Newsletter ();
        
        $aLanguages = $oLanguage -> getLanguages ();
        
        $oTemplate -> assign ('languages', $aLanguages);
        
        if ($_GET ['action'] == 'new') 
        {
                $aContent = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("description[" . $aLanguages [$i]['id'] . "]", (isset ($_POST ['description'][$aLanguages [$i]['id']]) ? $_POST ['description'][$aLanguages [$i]['id']] : ''), $aConfig);                 
                }
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent);
                
                /*$aContent = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("page_description[" . $aLanguages [$i]['id'] . "]", (isset ($_POST ['page_description'][$aLanguages [$i]['id']]) ? $_POST ['page_description'][$aLanguages [$i]['id']] : ''), $aConfig);                 
                }
                
                $oTemplate -> assignByRef ('page_description', $aContent);
                */
                if (!empty ($_POST)) 
                {
                        if ($oNewsGroup -> addGroup ($_POST)) 
                        {
                                redirect ($SiteRoot . 'newsletter/templates.php?action=overview');   
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oNewsGroup -> getLastError ());
                        }
                    
                }
        } 
        elseif ($_GET ['action'] == 'overview') 
        {
            require_once 'Pager/Pager.php'; 
             
            $aParams = array (
                    'mode'                  => 'Sliding',
                    'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 50), //show 50 items per page 
                    'delta'                 => 3,
                    'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                    'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                    'altPrev'               => 'Vorige pagina',
                    'altNext'               => 'Volgende pagina',
                    'altPage'               => 'Pagina',
                    'separator'             => '',
                    'spacesBeforeSeparator' => 1,
                    'spacesAfterSeparator'  => 1,
                    'delta'                 => 2,
                    'clearIfVoid'           => false,
                    'itemData'              => $oNewsletter -> getTemplates (0, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
            );
            
            $oPager = & Pager::factory ($aParams);
            $aData  =& $oPager -> getPageData ();
            
            $oTemplate -> assign ('overview', true);          
            $oTemplate -> assign ('links', $oPager -> links);
            $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
            $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
            $oTemplate -> assign ('newsGroup', $aData);
                 
        } 
        elseif (($_GET ['action'] == 'delete') && is_numeric ($_GET ['id']))
        {
                if ($oNewsletter -> deleteTemplate ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . 'newsletter/templates.php?action=overview');
                }
            
        }
        elseif (($_GET ['action'] == 'edit') && is_numeric ($_GET ['id'])) 
        {
                $aGroup = $oNewsletter -> getTemplates ($_GET ['id']);
                //$aGroup ['description'] = htmlspecialchars ($aGroup ['description'], ENT_NOQUOTES, 'UTF-8');
                //print_r($aGroup ['description']);die();
                $aGroup ['data'] = json_decode ($aGroup ['values'], true);
                $aPrivate = json_decode ($aGroup ['private'], true);
                //echo '<pre>' . print_r($aGroup ['data'],true);
                $oTemplate -> assign ('newsGroup', $aGroup);
                $oTemplate -> assign ('private', $aPrivate);
                $iMailing = $oNewsletter -> getLastMailingId ();
                
                $sTemplate = file_get_contents ($GLOBALS ['cfg']['sWebsiteUrl'] . 'mail/' . $aGroup ['id'] . '/' . $iMailing . '.html?links=1');//$oNewsletter -> template ($aGroup ['id']);
                
                if (preg_match_all ('%<a class="browser"[^>]*>(.*?)</a>%s', $sTemplate, $aMatch))
                {
                        $aBrowser = $aMatch [0];
                }
                
                if (isset($aBrowser))
                {
                        
                        
                        foreach ($aBrowser as $title)
                        {
                                $sBrowser = str_replace ('href=""', 'href="http://' . $_SERVER ['SERVER_NAME'] . '/mail/history/' . $iMailing . '.html"', $title);
                                $sTemplate = str_replace ($title, $sBrowser, $sTemplate);              
                        }
                }
                
                if (preg_match_all ('%href="http[^"]+"%s', $sTemplate, $aMatch))
                {
                        $aLinks = $aMatch [0];
                        
                        $oTemplate -> assign ('links', $aLinks);
                }

                $aLinks = array ();
                $aTexts = array ();
                $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
                if(preg_match_all("/$regexp/siU", $sTemplate, $matches, PREG_SET_ORDER))
                { 
                    $aLinks = array ();
                    $aTexts = array ();
                    //echo '<pre>'. print_r($matches,true);die;
                    foreach($matches as $match)
                    {
                        if (strpos($match [2], 'mailto') === false && $match [2] != '')
                        {
                            $aLinks [] = $match [2];
                            $aTexts [] = $match [3];
                        }
                    }

                    $oTemplate -> assign ('links', $aLinks);
                    $oTemplate -> assign ('texts', $aTexts);
                }
                
                $aTexts = array (
                        'text_text1',
                        'text_text2',
                        'text_text3',
                        'text_text4',
                        'text_text5',
                        'text_text6',
                        'text_text7',
                        'text_text8',
                        'text_text9'
                );
                
                $aContent = array ();
                
                for ($i = 0; $i < count ($aTexts); $i++)
                {
                        $aContent [$aTexts [$i]] = $oCKeditor -> editor ($aTexts [$i], (isset ($aGroup ['data'][$aTexts [$i]]) ? $aGroup ['data'][$aTexts [$i]] : ''), $aConfig);                 
                }
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent);
                
                if (!empty ($_POST)) 
                {   
                        $sValues = json_encode ($_POST);
                        //echo $sValues;die();
                        
                        if (isset ($_POST ['send']))
                        {
                                $aPost = array ();
                                $aPost [-1] = array ('link' => 'open', 'name' => 'open');
                                
                                if (isset ($_POST ['links']))
                                        foreach ($_POST ['links'] as $k => $item)
                                                $aPost [$k] = array ('link' => $item, 'name' => '');
                                
                                $iMailing = $oNewsletter -> addMailing (array (
                                        'template_id'       => $_GET ['id'],
                                        'group_id'          => $_POST ['group_id'],
                                        'values'            => $aGroup ['values'],
                                        'private'           => $aGroup ['private'],
                                        'template_content'  => $sTemplate,
                                        'links'             => json_encode ($aPost)
                                ));
                                
                                
                                if (is_writable('progress.js')) {

                                    // In our example we're opening $filename in append mode.
                                    // The file pointer is at the bottom of the file hence
                                    // that's where $somecontent will go when we fwrite() it.
                                    if (!$handle = fopen('progress.js', 'w')) {
                                         echo "Cannot open file (progress.js)";
                                         exit;
                                    }
                                    
                                    // Write $somecontent to our opened file.
                                    if (fwrite($handle, json_encode (array ('progress' => 0))) === FALSE) {
                                        echo "Cannot write to file (progress.js)";
                                        exit;
                                    }
                                    
                                    fclose($handle);
                                }
                                
                                $sTemplate = file_get_contents ($GLOBALS ['cfg']['sWebsiteUrl'] . 'mail/' . $aGroup ['id'] . '/' . $iMailing . '.html?links');
                                
                                $sWriteTemplate = file_get_contents ($GLOBALS ['cfg']['sWebsiteUrl'] . 'mail/' . $aGroup ['id'] . '.html');
                                
                                if (preg_match_all ('%<a class="browser"[^>]*>(.*?)</a>%s', $sTemplate, $aMatch))
                                {
                                        $aBrowser = $aMatch [0];
                                }
                                
                                if (isset($aBrowser))
                                        foreach ($aBrowser as $title)
                                        {
                                                $sBrowser = str_replace ('href=""', 'href="http://' . $_SERVER ['SERVER_NAME'] . '/mail/history/' . $iMailing . '.html"', $title);
                                                $sWriteTemplate = str_replace ($title, '', $sWriteTemplate);  
                                                $sTemplate = str_replace ($title, $sBrowser, $sTemplate); 
                                        }
                                
                                $file = fopen ($_SERVER ['DOCUMENT_ROOT'] . '/mail/history/' . $iMailing . '.html', 'w');
                                fwrite ($file, $sWriteTemplate);
                                fclose ($file);
                                
                                $sName    = ($_POST ['send_fname'] == '' ? false : filter_var ($_POST ['send_fname'], FILTER_SANITIZE_SPECIAL_CHARS));
                                $sEmail   = ($_POST ['send_femail'] == '' ? false : filter_var ($_POST ['send_femail'], FILTER_VALIDATE_EMAIL));
                                
                                $aSubscribers = $oNewsletter -> getSubscribers (0, $_POST ['group_id']);
                                $aSubscribers = array ();
                                $aSubscribers [0]['email'] = 'andrei@inforitus.nl';
                                $aSubscribers [0]['name'] = 'tsest';
                                $aSubscribers [0]['gender'] = 'andr';
                                $aSubscribers [1]['email'] = 'andrei.dragos88@gmail.com';
                                $aSubscribers [1]['name'] = 'tsest';
                                $aSubscribers [1]['gender'] = 'andr';
                                $aSubscribers [2]['email'] = 'andrei.dragos26@yahoo.com';
                                $aSubscribers [2]['name'] = 'tsest';
                                $aSubscribers [2]['gender'] = 'andr';
                                $aSubscribers [3]['email'] = 'andrei.dragos26@hotmail.com';
                                $aSubscribers [3]['name'] = 'tsest';
                                $aSubscribers [3]['gender'] = 'andr';
                                //$sCopyTemplate = $sTemplate;
                                
                                if ($sName != false && $sEmail != false)
                                {
                                        $message = Swift_Message::newInstance($_POST ['subject'] == '' ? Configuration :: WEBSITE_NAME : substr ($_POST ['subject'], 0, 76));
                                        $message -> setFrom (array($sEmail => $sName));
                                        
                                        foreach ($aSubscribers as $sk => $item)
                                        {
                                                $sCopyTemplate = $sTemplate;
                                                $aUnsubscribe = $oNewsletter -> getPhotos (0, 'unsubscribe', $_GET ['id']);
                                                $sCopyTemplate = str_replace ('{unsubscribe}', $GLOBALS ['cfg']['sWebsiteUrl'] . 'mail/unsubscribe.php?subscriber_id=' . $item ['id'] . '&c=' . $item ['code'], $sCopyTemplate);
                                                $sCopyTemplate = str_replace (array (
                                                        '{name}', '{email}', '{gender}'
                                                ), array (
                                                        $item ['name'],
                                                        $item ['email'],
                                                        $item ['gender']
                                                ), $sCopyTemplate);
                                                //
                                                if (isset ($aLinks))
                                                        foreach ($aLinks as $k => $link)
                                                        {
                                                                foreach ($aPost as $pk => $plink)
                                                                {
                                                                        if ($k == $pk)
                                                                        {
                                                                                $iCount = 0;
                                                                                $iSCount = 0;
                                                                                
                                                                                foreach ($aLinks as $ck => $clink)
                                                                                {
                                                                                        if ($plink ['link'] == str_replace (array ('href="', '"'), '', $clink))
                                                                                        {
                                                                                                $iCount++;
                                                                                                
                                                                                                if ($ck == $pk)
                                                                                                {
                                                                                                        $iSCount = $iCount;       
                                                                                                }
                                                                                        }
                                                                                        
                                                                                        if ($iSCount != 0)
                                                                                                for ($i = 0; $i < $pk; $i++)
                                                                                                {
                                                                                                        if (isset ($aPost [$i]) && $aPost [$i]['link'] == $plink ['link'])
                                                                                                                $iSCount--;
                                                                                                }
                                                                                }
                                                                                
                                                                                $aParts = array ();
                                                                                
                                                                                $aParts = explode ('href="' . $plink ['link'] . '"', $sCopyTemplate);
                                                                                
                                                                                if (isset ($aParts [$iSCount]))
                                                                                {
                                                                                        $aParts [$iSCount] = 'href="' . $GLOBALS ['cfg']['sWebsiteUrl'] . 
                                                                                                'mail/monitor.php?mailing_id=' . $iMailing . '&subscriber_id=' . $item ['id'] . '&email=' . $item ['email'] . '&l=' . $pk . '&redirect=' . $plink ['link'] . '"' . $aParts [$iSCount];
                                                                                }
                                                                                
                                                                                $sCopyTemplate = implode ('href="' . $plink ['link'] . '"', $aParts);
                                                                                
                                                                                $sCopyTemplate = str_replace ('href="' . $plink ['link'] . '"href', 'href', $sCopyTemplate);
                                                                                            
                                                                                //$aTemplate ['description'] = str_replace ($text, strip_tags ($aText ['description'], '<strong><b><i><u><span>'), $aTemplate ['description']);
                                                                        }
                                                                        
                                                                        
                                                                }
                                                        }
                                                
                                                if (preg_match_all ('%<img src="[^"]+"%s', $sCopyTemplate, $aMatch))
                                                {
                                                        $aImg = $aMatch [0];
                                                }
                                                
                                                if (isset ($aImg))
                                                {
                                                        $sImgSrc = str_replace (array ('<img src="', '"'), '', $aImg [0]);
                                                        $sCopyTemplate = preg_replace ('%<img src="[^"]+"%s', '<img src="' . $GLOBALS ['cfg']['sWebsiteUrl'] . 'mail/monitor.php?mailing_id=' . $iMailing . '&subscriber_id=' . $item ['id'] . '&email=' . $item ['email'] . '&l=-1&img=' . $sImgSrc . '"', $sCopyTemplate, 1);       
                                                }
                                                
                                                /*
                                                $pMail = Mail :: createMessage ();
                                                $pMail -> setSender ($_POST ['send_fname'], $_POST ['send_femail']);
                                                $pMail -> setRecipient ($item ['email']);
                                                $pMail -> setXMailer ('Site/1.0');
                                                $pMail -> setSubject ($_POST ['subject'] == '' ? Configuration :: WEBSITE_NAME : substr ($_POST ['subject'], 0, 76));
                                                $pMail -> setBody ($sCopyTemplate, MailMessage:: CONTENT_TYPE_HTML);
                                                
                                                Mail :: sendMessage ($pMail);*/
                                                
                                                $message->setTo(array($item ['email'] => $item ['name']));
                                                $message ->setBody($sCopyTemplate, 'text/html');
                                                $numSent += $mailer->send($message, $failedRecipients);
                                                
                                                if (is_writable('progress.js')) {

                                                    // In our example we're opening $filename in append mode.
                                                    // The file pointer is at the bottom of the file hence
                                                    // that's where $somecontent will go when we fwrite() it.
                                                    if (!$handle = fopen('progress.js', 'w')) {
                                                         echo "Cannot open file (progress.js)";
                                                         exit;
                                                    }
                                                    
                                                    // Write $somecontent to our opened file.
                                                    if (fwrite($handle, json_encode (array ('progress' => round (($sk+1) * 100 / count ($aSubscribers))))) === FALSE) {
                                                        echo "Cannot write to file (progress.js)";
                                                        exit;
                                                    }
                                                    
                                                    fclose($handle);
                                                }
                                        }
                                        
                                        foreach ($failedRecipients as $email)
                                        {
                                                Database :: getInstance () -> query ("
                                                        INSERT INTO
                                                                newsletter_mailings_error
                                                        (mailing_id, email)
                                                        VALUES 
                                                        (" . $iMailing . ", '" . $email . "')
                                                ");
                                        }
                                }
                                else
                                {
                                        $oTemplate -> assign ('send_v', array (
                                                'name'  => $sName,
                                                'email' => $sEmail
                                        ));
                                }
                        }
                        elseif (isset ($_POST ['send_test']))
                        {
                                //$sTemplate = file_get_contents ('../../mail/' . $aGroup ['group_name'] . '.html');
                                //$sTemplate = html_entity_decode ($sTemplate, ENT_COMPAT, 'UTF-8');
                                
                                //$sTemplate = 'asad &nbsp; asd&nbsp;';
                                //$sTemplate = $aGroup ['description'];
                                //echo $sTemplate;die();
                                if (filter_var ($_POST ['send_email'], FILTER_VALIDATE_EMAIL)
                                && strlen ($_POST ['subject']) < 78 && strlen ($_POST ['subject']) > 0)
                                {
                                        $sTemplate = str_replace (array (
                                                '{name}', '{email}', '{gender}'
                                        ), array (
                                                $_POST ['send_name'],
                                                $_POST ['send_email'],
                                                $_POST ['send_gender']
                                        ), $sTemplate);
                                        
                                        $pMail = Mail :: createMessage ();
                                        $pMail -> setSender ($_POST ['send_fname'], $_POST ['send_femail']);
                                        $pMail -> setRecipient ($_POST ['send_email']);
                                        $pMail -> setXMailer ('Site/1.0');
                                        $pMail -> setSubject ($_POST ['subject'] == '' ? Configuration :: WEBSITE_NAME : substr ($_POST ['subject'], 0, 76));
                                        $pMail -> setBody ($sTemplate, MailMessage:: CONTENT_TYPE_HTML);
                                        
                                        Mail :: sendMessage ($pMail);
                                        
                                        $oTemplate -> assign ('send_test', 1);
                                }
                        }
                        elseif (isset ($_POST ['new_template']))
                        {
                                $aGroup = $oNewsletter -> getTemplates ($_GET ['id']);
                                $aGroup ['title'] = $_POST ['template_name'];
                                $oNewsletter -> addTemplate ($aGroup);       
                                redirect ($SiteRoot . 'newsletter/templates.php?action=overview'); 
                        }
                        elseif ($oNewsletter -> changeTemplate ($_GET ['id'], $_POST, $sValues, json_encode ($aPrivate))) 
                        {
                                redirect ($SiteRoot . 'newsletter/templates.php?action=edit&id='.$_GET ['id']);   
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oNewsletter -> getLastError ());
                        }
                    
                }
                
                //$oTemplate -> assign ('fields', $aFields);
        } 
        elseif ($_GET ['action'] == 'private')
        {
                $aGroup = $oNewsletter -> getTemplates ($_GET ['id']);
                $aPrivate = json_decode ($aGroup ['private'], true);
                $aPrivate [$_GET ['field']] = $_GET ['value'];
                $sPrivate = json_encode ($aPrivate);
                $oNewsletter -> changeTemplate ($_GET ['id'], $_POST, $aGroup ['values'], $sPrivate);
                redirect ($SiteRoot . 'newsletter/templates.php?action=edit&id='.$_GET ['id']);
                
        }
        elseif ($_GET ['action'] == 'copy')
        {
                $aGroup = $oNewsletter -> getTemplates ($_GET ['id']);
                $aGroup ['title'] = 'Copy of ' . $aGroup ['title'];
                $oNewsletter -> addTemplate ($aGroup);       
                redirect ($SiteRoot . 'newsletter/templates.php?action=overview'); 
        }
        else 
        {
                redirect ($SiteRoot . 'newsletter/templates.php?action=overview');
        }
} 
else 
{
        redirect ($SiteRoot . 'newsletter/templates.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
        'jquery.min.js',
        'jquery-ui-1.8.17.custom.min.js',
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js',
        ($_GET ['action'] == 'overview' ? 'tablednd.js' : ''),
        ($_GET ['action'] == 'overview' ? 'sort_table.js' : ''),
        'ajaxupload.3.5.js',
        'greybox.js',
        'upload.js')
        );

$oTemplate -> assign ('cssBody', array ('css/greybox.css', 'css/jquery-ui-1.8.17.custom.css'));        
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 50));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'newsletter/templates.tpl');
$oTemplate -> display ('default.tpl');
//printf("Sent %d messages\n", $numSent);

?>
