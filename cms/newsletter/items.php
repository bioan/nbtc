<?php
######### COPYRIGHT #################
 
/*
    homepage: http://inforitus.nl
    file: news
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Newsletter.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Group.class.php');

$aFolderData = array(
        'url' => 'nieuws.gif',
        'text' => 'Photo',
        'page' => 'content'
);

$aActions = array(
        1 => 'new',
        2 => 'overview',
        3 => 'edit',
        4 => 'delete'
);

$oNewsletter = new Newsletter ();
$oTemplates  = new Group ('templates');
$oLanguage   = new Language ();
$oCKeditor   = new CKEditor () ;

$oCKeditor -> basePath          = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/';
$oCKeditor -> returnOutput      = true;
$oCKeditor -> config ['width']  = 590; 
$oCKeditor -> config ['height'] = 300; 

$aConfig = array ();
$aConfig ['toolbar'] = array (
        array ( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),
        array ( 'Image', 'Link', 'Unlink', 'Anchor', 'MediaEmbed' )
);

$oCKeditor -> config ['filebrowserBrowseUrl']      = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserImageBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserFlashBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserUploadUrl']      = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=File';
$oCKeditor -> config ['filebrowserImageUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
$oCKeditor -> config ['filebrowserFlashUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        $aLanguages = $oLanguage -> getLanguages ();
        
        $oTemplate -> assign ('languages', $aLanguages);
        
        if ($_GET ['action'] == 'new') 
        {
                $aContent = $oCKeditor -> editor ("description", (isset ($_POST ['description']) ? $_POST ['description'] : ''), $aConfig);                 
                $oTemplate -> assignByRef ('FCKeditor', $aContent);
                $oTemplate -> assign ('templates', $oTemplates -> getGroups ());          
                
                if (!empty ($_POST)) 
                {
                        if (!isset ($_POST ['test']))
                        {
                                if ($oNewsletter -> addPhoto ($_POST)) 
                                {
                                        redirect ($SiteRoot . '/newsletter/items.php?action=overview');  
                                } 
                                else 
                                {
                                        
                                        $oTemplate -> assign ('error', $oNewsletter -> getLastError ());
                                }
                        }
                        else
                        {
                                $sTemplate = $oNewsletter -> template ($_POST ['group_id'], $_POST);
                                $oTemplate -> assign ('template', $sTemplate);  
                        }
                } 
        } 
        elseif ($_GET ['action'] == 'overview') 
        {
                require_once 'Pager/Pager.php';  
                
                $oTemplate -> assign ('news_overview', true);
                
                $aParams = array (
                        'mode'                  => 'Sliding',
                        'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 50), //show 50 items per page 
                        'delta'                 => 3,
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
                        'delta'                 => 2,
                        'clearIfVoid'           => false,
                        'itemData'              => $oNewsletter -> getPhotos (0, '', 0, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                );
                
                $oPager = & Pager::factory ($aParams);
                $aData  =& $oPager -> getPageData ();
                
                $oTemplate -> assign ('links', $oPager -> links);
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
                $oTemplate -> assign ('photos', $aData);
            
        } 
        elseif ($_GET ['action'] == 'edit') 
        {
                $aPhoto = $oNewsletter -> getPhotos ($_GET ['id']);
                
                $aContent = $oCKeditor -> editor ("description", (isset ($_POST ['description']) ? $_POST ['description'] : html_entity_decode ($aPhoto ['description'])), $aConfig);                 
                $oTemplate -> assignByRef ('FCKeditor', $aContent);     
                $oTemplate -> assign ('values', $aPhoto);
                $oTemplate -> assign ('templates', $oTemplates -> getGroups ());     
                //$aTemplate = $oTemplates -> getGroups ();
                if (!empty ($_POST)) 
                {
                        if (!isset ($_POST ['test']))
                        {
                                if ($oNewsletter -> updatePhoto ($_GET ['id'], $_POST)) 
                                {
                                        redirect ($SiteRoot . '/newsletter/items.php?action=overview'); 
                                } 
                                else 
                                {
                                        $oTemplate -> assign ('error', $oNewsletter -> getLastError ());
                                }
                        }
                        else
                        {
                                $sTemplate = $oNewsletter -> template ($_POST ['group_id'], $_POST);
                                $oTemplate -> assign ('template', $sTemplate);
                        }
                } 
        } 
        elseif ($_GET ['action'] == 'delete') 
        {
                if ($oNewsletter -> deletePhoto ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/newsletter/items.php?action=overview');
                }
        }
     
} 
else 
{
        redirect ($SiteRoot . '/newsletter/items.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'updateForm.js',
                                'jquery-ui-1.8.17.custom.min.js',
                                ($_GET ['action'] == 'overview' ? 'tablednd.js' : ''),
                                ($_GET ['action'] == 'overview' ? 'sort_table.js' : ''),
                                'ajaxupload.3.5.js',
                                'greybox.js',
                                'upload.js'
                                )
                            );
              
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 50));              
$oTemplate -> assign ('cssBody', array ('css/jquery-ui-1.8.17.custom.css', 'css/greybox.css'));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'newsletter/items.tpl');

$oTemplate -> display ('default.tpl');
?>
