<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: fold_menu
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once 'includes/default.inc.php';
require_once 'includes/Page.class.php';

$pResult = Database :: getInstance () -> query ("
        SELECT 
            * 
        FROM 
            menu_settings 
        WHERE 
            user_id = " . $_SESSION ['user_id'] . " 
        AND 
            tree_oid = " . $_POST ['id'] . ""
);

if ($pResult -> num_rows > 0)
{
        Database :: getInstance () -> query ('
                UPDATE 
                    menu_settings
                SET
                    fold = ' . $_POST ['fold'] . '
                WHERE
                    user_id = ' . $_SESSION ['user_id'] . '
                AND 
                    tree_oid = ' . $_POST ['id'] . '
        ');   
}
else
{
        Database :: getInstance () -> query ('
                INSERT INTO
                    menu_settings
                    (user_id, tree_oid, fold)
                VALUES 
                    (' . $_SESSION ['user_id'] . ', ' . $_POST ['id'] . ', ' . $_POST ['fold'] . ')
        ');
}

?>