<?php
######### COPYRIGHT #################
 
/*
	ALL RIGHTS RESERVED
	@author Andrei Dragos <andrei@inforitus.nl>
	file: cats
*/

######### COPYRIGHT #################
require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Setting.class.php');
require_once  $GLOBALS ['cfg']['include'] . '/cms/includes/Tree.class.php';
require_once  $GLOBALS ['cfg']['include'] . '/cms/includes/Cyclist.class.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/ValidateFields.class.php';

$aActions = array (
        1 => 'new',
        2 => 'overview',
        3 => 'delete',
        4 => 'edit'
);

$aFolderData = array (
        'url'  => 'webshop.gif',
        'text' => 'Countries',
        'page' => 'foto'
);

$oSetting  = new Setting ();
$oTree     = new Tree (3);
$ciclists  = new Cyclist();
$oValidate = new ValidateFields ();
$aFieldsNames = array (
        'name'   => 'Page titel'
);

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        $oTemplate -> assign ('settings', $oSetting -> getSettings ());
        
        if ($_GET ['action'] == 'new') 
        {       
                if (!empty ($_POST)) 
                {

                    $newCountryId = $ciclists -> insertNewCountryName($aCountry["id"], $_POST["name"]);
                    
                    if (trim($_POST['country_pass']) != ""){

                        if ($_POST['country_pass'] != $_POST['country_pass_conf']){
                            $oValidate -> aError  = array (
                                    'country_pass'   => 'Country password and password confirm don\'t match'
                            );
                            $oTemplate -> assign ('error', $oValidate -> aError);
                        }      
                        else{
                            $encodedPass = md5($_POST['country_pass']);
                            $ciclists -> saveCountryPass($aCountry["id"], $encodedPass);
                        } 
                    }
                }
        } 
        elseif ($_GET ['action'] == 'overview') 
        {
                require_once 'Pager/Pager.php';
                
                $aParents = $ciclists -> getCountries();
                
		        $aParams = array (
		                'mode'                  => 'Sliding',
		                'perPage'               => 50, //show 10 items per page 
                        'delta'                 => 3, 
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')),
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
		                'delta'                 => 2,
		                'clearIfVoid'           => false,
		                'itemData'              => $oTree -> getAllNodes (0, 1, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'lft'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'ASC'))
		        );
                
                $oPager = & Pager::factory ($aParams);
		        $aData  =& $oPager -> getPageData ();
		        $sLinks =& $oPager -> getLinks ();
                
		        $oTemplate -> assign ('links', $oPager -> links);
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1)); 
		        $oTemplate -> assign ('existsCats', $aData);
                $oTemplate -> assign ('parents', $aParents);
                $oTemplate -> assign ('overview', true);
        } 
        elseif ($_GET ['action'] == 'delete' && is_numeric ($_GET ['id']))
        {
                if ($ciclists -> deleteCountry ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/photoalbum/cats.php?action=overview');
                }
        } 
        elseif ($_GET ['action'] == 'edit' && is_numeric ($_GET ['id'])) 
        {
                $aCountry = $ciclists -> getCountry ($_GET ['id']);
                //echo "<pre>".print_r($aCountry,true);die();
                if (empty ($_POST)) 
                {} 
                else 
                {
                    $ciclists -> saveCountryName($aCountry["id"], $_POST["name"]);
                    
                    if (trim($_POST['country_pass']) != ""){

                        if ($_POST['country_pass'] != $_POST['country_pass_conf']){
                            $oValidate -> aError  = array (
                                    'country_pass'   => 'Country password and password confirm don\'t match'
                            );
                            $oTemplate -> assign ('error', $oValidate -> aError);
                        }      
                        else{
                            $encodedPass = md5($_POST['country_pass']);
                            $ciclists -> saveCountryPass($aCountry["id"], $encodedPass);
                        } 
                    }
                }
                $oTemplate -> assign ('country', $aCountry);
        } 
        else 
        {
                redirect ($SiteRoot . '/photoalbum/cats.php?action=overview');
        }
} 
else 
{
        redirect ($SiteRoot . '/photoalbum/cats.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', 
    array (
        'jquery.min.js', 
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js',
        'jquery-ui-1.7.1.custom.min.js',
        'pages.js.php?type_oid=3',
        'ajaxupload.3.5.js',
        'greybox.js',
        'upload.js'
        )
);

$oTemplate -> assign ('cssBody', array ('css/greybox.css'));
        
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'photoalbum/cats.tpl');
$oTemplate -> display ('default.tpl');
?>
