<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: upload
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
set_time_limit (0);
require_once '../includes/default.inc.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/ImageLibrary.class.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Tree.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/PhotoAlbum.class.php');

$aFolderData = array (
        'url'  => 'folder_photoalbum.gif',
        'text' => 'Massa Upload',
        'page' => 'foto'
);

$oTree       = new Tree (3);
$oImage      = new ImageLibrary ();
$oPhotoAlbum = new PhotoAlbum ();
$pStatement  = Database :: getInstance ();
$oZip        = new ZipArchive;

$aLeafNodesArray = array ();
                
foreach ($oTree -> getLeafNodes () as $value) 
{
        $aLeafNodesArray [$value ['oid']] = $value ['oid'];
}

$oTemplate -> assign ('leafNodes', $aLeafNodesArray);
$oTemplate -> assign ('nodes', $oTree -> getAllNodes (0, 0, 'lft', 'asc'));

if (!empty ($_POST)) 
{
        $aCategory = $oTree -> getNodeData ($_POST ['category']);
        
        copy ($_FILES ['zip']['tmp_name'], '../../Cache/zip.zip');
        
        if ($oZip -> open ('../../Cache/zip.zip') === TRUE) 
        {
                for($i = 0; $i < $oZip -> numFiles; $i++) 
                {
                        $entry = $oZip -> getNameIndex ($i);
                        
                        $oImage -> sExtensie = '.' . end (explode (".", $entry));
                       
                        if (preg_match ('#(jpg|png|gif|jpeg|JPG)$#i', $oImage -> sExtensie, $matches))
                        {
                                $fileinfo = pathinfo ($entry);
                                
                                $pStatement -> query ("INSERT INTO images(date) VALUES (NOW())");
		        
                                $iLastInsertId = $pStatement -> insert_id;
                                
                                $sPath = '../../files/photoalbum/';
                                
                                
                                
                                copy ("zip://../../Cache/zip.zip#".$entry, $sPath.$iLastInsertId.$oImage -> sExtensie);
                                
                                $oImage -> sLocation = $GLOBALS ['cfg']['include'] . '/' . substr ($sPath, 6, -1);
                                $oImage -> sImageName = $iLastInsertId; 
                                
                                
                                $aImageOptions = getimagesize ($oImage -> sLocation . "/" . $oImage -> sImageName . $oImage -> sExtensie);
                        
                                $oImage -> iOrig_height = $aImageOptions [1];
                                $oImage -> iOrig_width  = $aImageOptions [0];
                                $oImage -> sMimeType    = $aImageOptions ['mime'];                      
            
                                 
                                
                                $sFunctionName = sprintf ('imagecreatefrom%s', ($matches [1] == 'jpg' || $matches [1] == 'JPG' ? 'jpeg' : $matches [1]));
                        
                                $oImage -> OrginalImage = $sFunctionName ($oImage -> sLocation . "/" . $oImage -> sImageName . $oImage -> sExtensie);    
                                	             
                                $file = $sPath . $oImage -> sImageName . $oImage -> sExtensie;
                                list($src_w, $src_h, $type, $attr) = getimagesize ($file);
                                
                                $info      = pathinfo ($entry);
                                $file_name =  basename ($entry, '.'.$info['extension']);
                                
                                $_SESSION ['image'] = array (
                                                                'name'               => preg_replace ('#.*/([^/]+)$#','${1}', $oImage -> sImageName . $oImage -> sExtensie),
                                                                'extensie'           => $oImage -> sExtensie,
                                                                'width'              => $src_w,
                                                                'orig_width'         => $src_w,
                                                                'height'             => $src_h,
                                                                'orig_height'        => $src_h,
                                                                'mime_type'          => $oImage -> sMimeType,
                                                                'location'           => preg_replace (sprintf ('#%s(.*)$#', $GLOBALS ['cfg']['include']), '${1}', $oImage -> sLocation),
                                                                'oid'                => $oImage -> sImageName,
                                                                'title'              => $file_name
                                                        );
                                        
                                /*$images = array(
                    		        1 => array(
                    		            'width'     => 600, 
                    		            'height'    => 0, 
                    		            'location'  => $oImage -> sLocation,
                    		            'name'      => ''
                    		         )
                    		    );
                    		    if (!$newImageData = $oImage->reduceAndCreateThumbnail($images))
                                {
                                        die('a');
                                }*/
                                
                                $oImage -> orginalImage = $sFunctionName ($oImage -> sLocation . "/" . $oImage -> sImageName . $oImage -> sExtensie); 
                                list($src_w, $src_h, $type, $attr) = getimagesize ($file);
                                
                                $source = $oImage -> OrginalImage;
                                $tn_w = 800;
                                $tn_h = 600;
                                
                                $x_ratio = $tn_w / $src_w;
                                $y_ratio = $tn_h / $src_h;
                    
                                if (($src_w <= $tn_w) && ($src_h <= $tn_h)) 
                                {
                                    $new_w = $src_w;
                                    $new_h = $src_h;
                                } 
                                elseif (($x_ratio * $src_h) < $tn_h) 
                                {
                                    $new_h = ceil($x_ratio * $src_h);
                                    $new_w = $tn_w;
                                } 
                                else 
                                {
                                    $new_w = ceil($y_ratio * $src_w);
                                    $new_h = $tn_h;
                                }
                                
                                $newpic = imagecreatetruecolor(round($new_w), round($new_h));
                                imagecopyresampled($newpic, $source, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
                                
                                $sFunctionName = sprintf ('image%s', ($matches [1] == 'jpg' || $matches [1] == 'JPG' ? 'jpeg' : $matches [1]));
                                $sFunctionName ($newpic, $file, 100);
                                
                                $sFunctionName = sprintf ('imagecreatefrom%s', ($matches [1] == 'jpg' || $matches [1] == 'JPG' ? 'jpeg' : $matches [1]));
                                $oImage -> OrginalImage = $sFunctionName ($oImage -> sLocation . "/" . $oImage -> sImageName . $oImage -> sExtensie);
                                
                                list($src_w, $src_h, $type, $attr) = getimagesize ($file);
                                
                                $iThumbWidth  = $GLOBALS ['cfg']['images']['thumbWidth'];
                                $dDeelfactor  = 100 / $iThumbWidth; 
                                $iThumbHeight = round (100 / $dDeelfactor); 
                                
                                $aImageOpts = array ();
                                
                                foreach ($aImageSizes ['photoalbum'] as $size)
                                {
                                        $aImageOpts [] = array (
                                		        'x1'     => 0,
                                		        'x2'     => $src_w,
                                		        'y1'     => 0,
                                		        'y2'     => $src_h,
                                		        'width'  => $size ['width'],
                                		        'height' => $size ['height'],
                                                'upload' => 1
                           	            );  
                                }
                                
                                
                                if ($aNewImageValues = $oImage -> cropImage ($_SESSION ['image'],$aImageOpts)) 
                                {
                                	    if (empty ($_GET ['action']) || $_GET ['action'] != 'change') 
                                        {
                                		        foreach ($aNewImageValues as $newImageValue) 
                                                {
                                			            if (!$oImage -> updateImage (array_merge ($newImageValue, array (
                                					            'orig_width'  => $_SESSION ['image']['orig_width'], 
                                					            'orig_height' => $_SESSION ['image']['orig_height'], 
                                					            'extensie'    => $_SESSION ['image']['extensie'],
                                					            'oid'         => $_SESSION ['image']['oid'],
                                					            'type'        => 'photoalbum',
                                                                'title'       => $_SESSION ['image']['title'])
                                                                ))
                                				            ) 
                                                        {
                                				                trigger_error (E_CORE_WARNING . ' Er is een fout opgetreden!');
                                				                exit;
                                			            }
                                		        }
                                	    } 
                                        elseif (!empty ($_GET ['action']) && $_GET ['action'] == 'change') 
                                        {
                                		        require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php';
                                		        
                                                $pStatement = Database :: getInstance (); 
                                		        
                                                $pResult = $pStatement  ->  query ("
                                			            SELECT
                                				            *
                                			            FROM
                                				            images
                                			            WHERE
                                				            id = " . $_SESSION ['image']['oid'] . "
                                		        ");
                                						        
                                		        $aResult = $pResult  ->  fetch_assoc ();
                                                
                                		        if (!$oImage -> updateImage (array_merge ($aResult, current ($aNewImageValues)), $_SESSION ['image']['oid'])) 
                                                {
                                			            trigger_error (E_CORE_WARNING . ' Er is een fout opgetreden!');
                                			            exit;
                                		        }
                                	    }
                                     
                                	    
                                }
                                
                                $aValues ['category'] = $_POST ['category'];
                                
                                foreach ($oTree -> aLanguages as $lang)
                                {
                                        $aValues ['title'][$lang ['id']] = '';
                                        $aValues ['description'][$lang ['id']] = '';
                                        $aValues ['page_description'][$lang ['id']] = '';
                                        $aValues ['photos'][$lang ['id']] = array ($iLastInsertId);
                                }
                                
                                $oPhotoAlbum -> addPhoto ($aValues);
                        }
                } 
                
                $oZip->close();
                unlink ('../../Cache/zip.zip');
        }
}

$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('javascriptBody', array (
        'jquery.min.js', 
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js','lightbox.js')
        );
        
$oTemplate -> assign ('contentInclude', 'photoalbum/upload.tpl');
$oTemplate -> display ('default.tpl');
?>