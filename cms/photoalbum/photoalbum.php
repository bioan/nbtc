<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: photoalbum
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php'); 
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Tree.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/PhotoAlbum.class.php');    

$aFolderData = array (
        'url'  => 'folder_photoalbum.gif',
        'text' => 'Fotogalerij',
        'page' => 'foto'
);

$oPhotoAlbum = new PhotoAlbum ();  
$oLanguage   = new Language ();     
$oTree       = new Tree (3);
$oCKeditor   = new CKEditor ();

$oCKeditor -> basePath          = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/';
$oCKeditor -> returnOutput      = true;
$oCKeditor -> config ['width']  = 590; 
$oCKeditor -> config ['height'] = 300; 
$oCKeditor -> config ['extraAllowedContent'] = 'img[alt,border,width,height,align,vspace,hspace,!src];';
$oCKeditor -> config ['allowedContent'] = true;

$aConfig = array ();
$aConfig ['toolbar'] = array (
        array ( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),
        array ( 'Image', 'Link', 'Unlink', 'Anchor', 'MediaEmbed' )
);

$oCKeditor -> config ['filebrowserBrowseUrl']      = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserImageBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserFlashBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserUploadUrl']      = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=File';
$oCKeditor -> config ['filebrowserImageUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
$oCKeditor -> config ['filebrowserFlashUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';
                          

if (!empty ($_GET ['action'])) 
{
        $aLanguages = $oLanguage -> getLanguages ();
        
        $oTemplate -> assign ('languages', $aLanguages);
        
        if ($_GET ['action'] == 'new') 
        {
                $aLeafNodesArray = array ();
                
                foreach ($oTree -> getLeafNodes () as $value) 
                {
                        $aLeafNodesArray [$value ['oid']] = $value ['oid'];
                }
                
                $aContent = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("description[" . $aLanguages [$i]['id'] . "]", (isset ($_POST ['description'][$aLanguages [$i]['id']]) ? $_POST ['description'][$aLanguages [$i]['id']] : ''), $aConfig);                 
                }
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent);
                $oTemplate -> assign ('leafNodes', $aLeafNodesArray);
                $oTemplate -> assign ('nodes', $oTree -> getAllNodes (0, 0, 'lft', 'asc'));
                
                if (isset ($_POST ['keywords']))
                {
                        $aKeywords = keywords ($_POST ['description']);
                        
                        $oTemplate -> assign ('keywords', $aKeywords);
                }
                elseif (!empty ($_POST)) 
                {
                        if ($oPhotoAlbum -> addPhoto ($_POST)) 
                        { 
                                redirect ($SiteRoot . '/photoalbum/photoalbum.php?action=overview');        
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oPhotoAlbum -> getLastError ());
                        }
                }
        } 
        elseif ($_GET ['action'] == 'overview') 
        {   
    	        require_once 'Pager/Pager.php';
                
		        $aParams = array (
		                'mode'                  => 'Sliding',
		                'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 50), //show 50 items per page 
                        'delta'                 => 3, 
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')),
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
		                'delta'                 => 2,
		                'clearIfVoid'           => false,
		                'itemData'              => $oPhotoAlbum -> getPhotos (0, '', 0, '', (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
		        );
                
		        $oPager = & Pager::factory ($aParams);
		        $aData  =& $oPager -> getPageData ();
                
		        $oTemplate -> assign ('links', $oPager -> links);
		        $oTemplate -> assign ('photos', $aData);
                $oTemplate -> assign ('action', 'overview'); 
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1)); 
                   
        } elseif ($_GET ['action'] == 'delete') 
        {
                if ($oPhotoAlbum -> deletePhoto ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/photoalbum/photoalbum.php?action=overview');
                }
        } 
        elseif ($_GET ['action'] == 'edit') 
        {
                $aPhoto = $oPhotoAlbum -> getPhotos ($_GET ['id']);
                
                $aLeafNodesArray = array ();
                
                foreach ($oTree -> getLeafNodes() as $value) 
                {
                        $aLeafNodesArray [$value ['oid']] = $value ['oid'];
                }
                
                $aContent = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [$aLanguages [$i]['id']] = $oCKeditor -> editor ("description[" . $aLanguages [$i]['id'] . "]", (isset ($aPhoto [$aLanguages [$i]['id']]['description']) ? html_entity_decode ($aPhoto [$aLanguages [$i]['id']]['description']) : ''), $aConfig);                 
                }
                
                $oTemplate -> assignByRef ('FCKeditor', $aContent); 
                
                $oTemplate -> assign ('leafNodes', $aLeafNodesArray);
                $oTemplate -> assign ('photo', $aPhoto);
                $oTemplate -> assign ('nodes', $oTree -> getAllNodes (0, 0, 'lft', 'asc'));
                
                if (isset ($_POST ['keywords']))
                {
                        $aKeywords = keywords ($_POST ['description']);
                        
                        $oTemplate -> assign ('keywords', $aKeywords);
                }
                elseif (!empty ($_POST)) 
                {
                        if ($oPhotoAlbum -> updatePhoto ($_GET ['id'], $_POST))
                        {
                                redirect ($SiteRoot . '/photoalbum/photoalbum.php?action=overview');        
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oPhotoAlbum -> getLastError());
                        }
                }
        }
}

$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('javascriptBody', array (
        'jquery.min.js', 
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js',
        'lightbox.js',
        ($_GET ['action'] == 'overview' ? 'tablednd.js' : ''),
        ($_GET ['action'] == 'overview' ? 'sort_table.js' : ''),
        'ajaxupload.3.5.js',
        'greybox.js',
        'upload.js'
        )
        );

$oTemplate -> assign ('cssBody', array ('css/greybox.css'));
        
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 50));        
$oTemplate -> assign ('contentInclude', 'photoalbum/photoalbum.tpl');
$oTemplate -> display ('default.tpl');
?>
