<?php

/**
     @description: twitter class
     @link: http://www.inforitus.nl
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class Twitter 
{ 
    
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * @desc contains all languages 
        * 
        * @var array
        */
        
        var $aLanguages = array ();
        
        /**
        * @desc contains default language 
        * 
        * @var array
        */
        
        var $aDefaultLang = array ();

        /**
        *@desc: contructor 
        *
        */
                      
        public function Twitter () 
        { 
                $this -> aLanguages   = Language :: getLanguages ();
                $this -> aDefaultLang = Language :: getLanguages (0, '', 'id', 'asc', true);  
        }
        
        /**
        * @desc check App fields and insert or update database
        * 
        * @param array $aValues - array of values containing app id and app secret
        * 
        * @returns boolean
        */
             
        public function checkApplication ($aValues = array ())
        {
                if (empty ($aValues ['consumer_key']))
                {
                        $this -> aError ['consumer_key'] = "Consumer key is required";
                }
                
                if (empty ($aValues ['consumer_secret']))
                {
                        $this -> aError ['consumer_secret'] = "Consumer secrets. is required";
                }
                
                if (count ($this -> aError) == 0)
                {
                        $facebookAppData = $this ->getApplication();
                        
                        if (!$facebookAppData)
                        {
                                $this ->addApplication($aValues);
                        }
                        else 
                        {
                                $this ->updateApplication($aValues);
                        }
                        return true;
                }
                else
                {
                        return false;
                }
        } 
        
        /**
        * @desc get the twitter Application
        * 
        * 
        * @returns array, data of application
        */
             
        public function getApplication ()
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            twitter_app
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;          
                
		        return $aResults;
        } 
        
              
        /**
	    * @desc update Application
	    *
        * @param array $aValues - the values that have to be set
        *  
	    * @return boolean
	    */
        
	    public function updateApplication ($aValues = array ())
	    {
            
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            twitter_app
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;
            
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            twitter_app
                        SET
                            consumer_key     = ?,
                            consumer_secret  = ?,
                            date             = NOW()
                        WHERE
                            id         = " . $aResults['id'] ."
                ");
                
                $pStatement -> bind_param ('ss',    
                        $aValues ['consumer_key'],	
                        $aValues ['consumer_secret']
                );

                if (!$pStatement -> execute ()) 
                {	
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    

                    return false;
                } 

                return true;
            }
        
         
        
              
        /**
	    * @desc update Application status (if correct or not)
	    *
        * @param array $aValues - the values that have to be set
        *  
	    * @return boolean
	    */
        
	    public function updateApplicationStatus ($correct)
	    {
            
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            twitter_app
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;
            
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            twitter_app
                        SET
                            correct     = ?
                        WHERE
                            id         = " . $aResults['id'] ."
                ");
                
                $pStatement -> bind_param ('i',    
                        $correct
                );

                if (!$pStatement -> execute ()) 
                {	
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    

                    return false;
                } 

                return true;
            }
       
        
        /**
	    * @desc add twitter application
	    *
        * @param array $aValues - the values that have to be set
        * 
	    * @return boolean
	    */
	    
	    public function addApplication ($aValues = array ())
	    {	
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT
                        INTO
                            twitter_app
                                (
                                consumer_key,
                                consumer_secret,
                                date                
                                )      
                        VALUES
                                (
                                ?,
                                ?,
                                NOW()               
                                ) 
                ");

                $pStatement -> bind_param ('ss',
                        $aValues ['consumer_key'],
                        $aValues ['consumer_secret']				
                );

                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   

                        return false;
                }

                return true;
        }
        
        /**
        * @desc: get one or all the tweets 
        *
        * @param int $iItemId - id of the item
        * @param string $sSortField - sort the result by this field
        * @param string $sSortType - desc/asc
        *  
        * @return array OR booleaan by error
        */ 
                                           
        public function getTwitter ($iItemId = 0, $sSortField = 'date', $sSortType = 'desc')
        {        
                if (!is_numeric ($iItemId))
                {          
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            *,
                            DATE_FORMAT(date, '%d-%m-%Y') AS date_format
                        FROM 
                            twitter
                        WHERE 
                            deleted = 0 
                            " . (!empty ($iItemId)        ? sprintf ("AND id = %d ", $iItemId) : '') . "
                                                        
                        ORDER BY 
                            " . $sSortField . " " . $sSortType . "    
                    ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                $aResults [] = $aRow;
                        }
                }
                
                $aResults = (!empty ($iItemId)) ? current ($aResults) : $aResults;
            
                return $aResults;
        }
        
                /**
        * @desc: get one or all the tweets from search 
        *
        * @param int $iItemId - id of the item
        * @param string $sSortField - sort the result by this field
        * @param string $sSortType - desc/asc
        *  
        * @return array OR booleaan by error
        */ 
                                           
        public function getTwitterSearch ($iItemId = 0, $sSortField = 'date', $sSortType = 'desc')
        {        
                if (!is_numeric ($iItemId))
                {          
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            *,
                            DATE_FORMAT(date, '%d-%m-%Y') AS date_format
                        FROM 
                            twitter_search
                        WHERE 
                            deleted = 0 
                            " . (!empty ($iItemId)        ? sprintf ("AND id = %d ", $iItemId) : '') . "
                                                        
                        ORDER BY 
                            " . $sSortField . " " . $sSortType . "    
                    ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                $aResults [] = $aRow;
                        }
                }
                
                $aResults = (!empty ($iItemId)) ? current ($aResults) : $aResults;
            
                return $aResults;
        }
        
        /**
        * @desc: delete a twitter item
        *
        * @param int $iId - id of the item
        * 
        * @return booleaan   
        */
                              
        public function deleteTwitter ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            twitter
                        SET
                            deleted = 1
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                return true;
        }
        
         /**
        * @desc: delete a twitter search item
        *
        * @param int $iId - id of the item
        * 
        * @return booleaan   
        */
                              
        public function deleteTwitterSearch ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            twitter_search
                        SET
                            deleted = 1
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                return true;
        }
        
        
        public function checkTwitterUser ($aValues = array ())
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            twitter_users
                        WHERE
                            user_id = " . $aValues ['user_id'] . "
			            
                ");	
                
                if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                
                if ($pResult -> num_rows > 0)
                {         
                        $this -> updateTwitterUser ($aValues);
                }
                else
                {
                        $this ->addTwitterUser($aValues);
                }

                return true;
        } 
        
         /**
	    * @desc add twitter users
	    *
        * @param array $aValues - the values that have to be set
        * 
	    * @return boolean
	    */
	    
	    public function addTwitterUser ($aValues = array ())
	    {	
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT
                        INTO
                            twitter_users
                                (
                                user_id,
                                twitter_id,
                                twitter_name,
                                oauth_token,
                                oauth_token_secret,
                                date                
                                )      
                        VALUES
                                (
                                ?,
                                ?,
                                ?,
                                ?,
                                ?,
                                NOW()               
                                ) 
                "); 
                $pStatement -> bind_param ('issss', 
                        $aValues ['user_id'],
                        $aValues ['twitter_id'],
                        $aValues ['twitter_name'],
                        $aValues ['oauth_token'],
                        $aValues ['oauth_token_secret']
                );

                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   

                        return false;
                }

                return true;
        }
        
        
         /**
	    * @desc update facebook user
	    *
        * @param array $aValues - the values that have to be set
        *  
	    * @return boolean
	    */
        
	    public function updateTwitterUser ($aValues = array ())
	    {            
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            twitter_users
                        SET
                            user_id             = ?,
                            twitter_id          = ?,
                            twitter_name        = ?,
                            oauth_token         = ?,
                            oauth_token_secret  = ?,
                            date                = NOW()
                        WHERE
                            user_id         = " . $aValues['user_id'] ."
                ");

                $pStatement -> bind_param ('issss',
                        $aValues ['user_id'],
                        $aValues ['twitter_id'],
                        $aValues ['twitter_name'],
                        $aValues ['oauth_token'],
                        $aValues ['oauth_token_secret']
                );

                if (!$pStatement -> execute ()) 
                {	
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    

                    return false;
                } 

                return true;
            }
            
                     /**
        * @desc get Twitter user if exists
        * 
        * @param array parameters 
        * @returns array
        */
        
        public function getTwitterUser ($userID = 0)
        {
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            twitter_users " .
                        ($userID != 0 ? "WHERE user_id = " . $userID : "")        
                );	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;          
                
		        return $aResults;
        }
        /**
	    * @desc add twitter word
	    *
        * @param array $aValues - the values that have to be set
        * 
	    * @return boolean
	    */
	    
	    public function addTwitterWord ($aValues = array ())
	    {	
                $sQuery = sprintf ("
                        SELECT
                            *
                        FROM
                            twitter_words
                        WHERE
                            word = '" . $aValues ['twitterWord'] . "' 
			            
                ");	
                
		        if (($sQuery = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                if ($sQuery -> num_rows == 0)
                {
            
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT
                                INTO
                                    twitter_words
                                        (
                                        word,
                                        date            
                                        )      
                                VALUES
                                        (
                                        ?,
                                        NOW()               
                                        ) 
                        ");

                        $pStatement -> bind_param ('s',
                                $aValues ['twitterWord']			
                        );

                        if (!$pStatement -> execute ()) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   

                                return false;
                        }

                        return true;
                }
                return false;
        }
        
        /**
        * @desc get the twitter words
        * 
        * 
        * @returns array, data of application
        */
             
        public function getTwitterWords ($orderBy = 'word')
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            twitter_words
                        ORDER BY 
                            word
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (!isset ($aResults [0])) ? current ($aResults) : $aResults;          
                
                return $aResults;
        } 
        
        
        /**
        * @desc get the twitter word after id
        * 
        * 
        * @returns array, data of application
        */
             
        public function getTwitterWord ($id)
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            twitter_words
                        WHERE
                            id = " . $id . "
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;          
                
		        return $aResults;
        } 
        
         /**
        * @desc: edit a twitter word item
        *
        * @param int $iId - id of the item
        * 
        * @return booleaan   
        */
                              
        public function editTwitterWord ($iId, $aValues)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            twitter_words
                        SET
                            word = '" . $aValues ['twitterWord'] . "'
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                return true;
        }
        
        /**
        * @desc: delete a twitter word item
        *
        * @param int $iId - id of the item
        * 
        * @return booleaan   
        */
                              
        public function deleteTwitterWord ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE FROM
                            twitter_words
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                $sQuery = sprintf ("
                        SELECT
                            *
                        FROM
                            twitter_search
                        WHERE
                            wordID = " . $iId . "
			            
                ");	
                
                if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                
                if ($pResult -> num_rows > 0)
                {         
                        $pStatement2 = Database :: getInstance () -> prepare ("
                                DELETE FROM
                                    twitter
                                WHERE
                                    wordID = ?
                        ");
                        $pStatement2 -> bind_param ('i',
                                $iId   
                        );

                        if (!$pStatement2 -> execute ())
                        {
                                return false;
                        }
                }
                
                return true;
        }
        
        /**
        * @desc get the twitter Conn data
        * 
        * 
        * @returns array, data of conn data
        */
             
        public function getTwitterConn ()
        {
                $sQuery = sprintf ("
                        SELECT
                            *
                        FROM
                            twitter_conn
                ");	


		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }

                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;          
                
		        return $aResults;
        } 
        
        /**
        * @desc check Twitter conn data fields and insert or update database
        * 
        * @param array $aValues - array of values containing app id and app secret
        * 
        * @returns boolean
        */
             
        public function checkConn ($aValues = array ())
        {
                if (empty ($aValues ['oauth_token']))
                {
                        $this -> aError ['oauth_token'] = "Oauth token is required";
                }
                
                if (empty ($aValues ['oauth_token_secret']))
                {
                        $this -> aError ['oauth_token_secret'] = "Oauth secret is required";
                }
                
                if (count ($this -> aError) == 0)
                {
                        $twitterConn = $this ->getTwitterConn();
                        if (!$twitterConn)
                        {
                                $this ->addTwitterConn($aValues);
                        }
                        else 
                        {
                                $this ->updateTwitterConn($aValues);
                        }
                        return true;
                }
                else
                {
                        return false;
                }
        } 
        
              
        /**
	    * @desc update twitter connection data
	    *
        * @param array $aValues - the values that have to be set
        *  
	    * @return boolean
	    */
        
	    public function updateTwitterConn ($aValues = array ())
	    {
            
                $sQuery = sprintf ("
                        SELECT
                            *
                        FROM
                            twitter_conn
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;
            
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            twitter_conn
                        SET
                            oauth_token     = ?,
                            oauth_token_secret  = ?
                        WHERE
                            id         = " . $aResults['id'] ."
                ");
                
                $pStatement -> bind_param ('ss',    
                        $aValues ['oauth_token'],	
                        $aValues ['oauth_token_secret']
                );

                if (!$pStatement -> execute ()) 
                {	
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    

                    return false;
                } 

                return true;
            }
               
        
        /**
	    * @desc add twitter connection data
	    *
        * @param array $aValues - the values that have to be set
        * 
	    * @return boolean
	    */
	    
	    public function addTwitterConn ($aValues = array ())
	    {	
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT
                        INTO
                            twitter_conn
                                (
                                oauth_token,
                                oauth_token_secret               
                                )      
                        VALUES
                                (
                                ?,
                                ?            
                                ) 
                ");

                $pStatement -> bind_param ('ss',
                        $aValues ['oauth_token'],
                        $aValues ['oauth_token_secret']				
                );

                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   

                        return false;
                }

                return true;
        }
}   
?>
