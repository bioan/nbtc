﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.extraPlugins = 'MediaEmbed';
    
    config.toolbar = 'MyToolbar';
 
	config.toolbar_MyToolbar =
	[
		 ['Source'], ['Bold'], ['Italic'], ['Underline'], ['TextColor'], ['BulletedList'], ['Link'], ['Unlink'], ['Image'], ['BulletedList'] 
	];
};
