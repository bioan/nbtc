<?php

/**
     @description: agenda class
     @link: http://www..inforitus.nl
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class Agenda
{ 
    
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
	    var $aError = array ();
        
        /**
        * @desc contains all languages 
        * 
        * @var array
        */
        
        var $aLanguages = array ();
        
        /**
        * @desc contains default language 
        * 
        * @var array
        */
        
        var $aDefaultLang = array ();
	    
	    /**
        * @desc: contructor 
        *
        */
                      
        public function Agenda () 
        { 
                $this -> aLanguages   = Language :: getLanguages ();
                $this -> aDefaultLang = Language :: getLanguages (0, '', 'id', 'asc', true);      
        }
        
        /**
         * @Desc: get the last errors
         *
         * @returns array
         **/
         
        public function getLastError ()
        {
                return $this -> aError;
        }      
         
        /**
         * @desc: get one or all the agendaitems by specific field
         *
         * @param int $iItemId - id of the item
         * @param string $sItemSlug - slug of the item
         * @param int $iAgendaGroupId - id of the group of agenda
         * @param string $sAgendaGroupSlug - slug of the group of agenda
         * @param string $sSortField - sort results by this field
         * @param string $sSortType - desc/asc
         *           
         * @returns array     
        */
                                           
        public function getAgenda ($iItemId = 0, $sItemSlug = '', $iAgendaGroupId = 0, $sAgendaGroupSlug = '', $sSortField = 'order_number', $sSortType = 'desc', $sLanguage = 'default', $sSearch = '')
        {
                if (!is_numeric ($iItemId) || !is_numeric ($iAgendaGroupId))
                {
                        return false;
                }
                     
                $pResult = Database :: getInstance () -> query ("
                        SELECT
                            a.*,
                            ad.title,
                            ad.description,
                            ad.photos,
                            ad.page_description,
                            ad.slug,
                            DATE_FORMAT(a.date,'%d-%m-%Y') AS date_format,
                            gd.group_name,
                            gd.slug as group_slug,
                            g.id as group_id,
                            (SELECT COUNT(*) FROM agenda_data WHERE agenda_id = a.id AND title != '' AND description != '') as filled 
                        FROM
                            agenda a
                        LEFT JOIN
                            agenda_data ad
                                ON
                            a.id = ad.agenda_id
                        INNER JOIN
                            groups g
                                ON
                            a.group_id = g.id    
                                AND
                            g.module = 'agenda'
                        LEFT JOIN
                            groups_data gd
                                ON
                            gd.group_id = g.id
                        LEFT JOIN
                            languages l
                                ON
                            (ad.lang_id = l.id AND gd.lang_id = l.id) 
                        WHERE 
                            1
                                " . ($sLanguage == 'default'    ? sprintf ("AND l.default = 1") : sprintf ("AND l.short_name = '%s'", Database :: getInstance () -> real_escape_string ($sLanguage))) . "
                                " . (!empty ($iItemId)          ? sprintf ("AND a.id = %d ", $iItemId) : '') . "
                                " . (!empty ($sItemSlug)        ? sprintf ("AND ad.slug = '%s' ", Database :: getInstance () -> real_escape_string ($sItemSlug)) : '') . "        
                                " . (!empty ($iAgendaGroupId)   ? sprintf ("AND a.group_id = %d ", $iAgendaGroupId) : '') . "
                                " . (!empty ($sAgendaGroupSlug) ? sprintf ("AND gd.slug = '%s' ", Database :: getInstance () -> real_escape_string ($sAgendaGroupSlug)) : '') . "
                                " . (!empty ($sSearch)        ? "AND (ad.description LIKE '%" . Database :: getInstance () -> real_escape_string ($sSearch) . "%' OR ad.title LIKE '%" . Database :: getInstance () -> real_escape_string ($sSearch) . "%')" : '') . "
                        ORDER BY
                            " . $sSortField . " " . $sSortType . "
                ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                $aRow ['photos']      = $this -> getPhotosByOids ($aRow ['photos']);
                                $aRow ['description'] = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                        
                                if (preg_match_all ('%<a[^>]+>%s', $aRow ['description'], $aMatch))
                                {
                                        $aLinks = $aMatch [0];
                                        
                                        foreach ($aLinks as $link)
                                        {
                                                if (strpos ($link, 'onclick="') === false && 
                                                strpos ($link, 'target="_blank"') !== false && 
                                                preg_match ('%href="[^"]+"%s', $link, $matches))
                                                {
                                                        $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                        $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['title'] . '\', \'' . $sAddress . '\')"', $link);
                                                        $aRow ['description'] = str_replace ($link, $sNewlink, $aRow ['description']); 
                                                }
                                        }
                                }
                        
                                $aResults []          = $aRow;
                        }
                }
                
                foreach ($aResults as $k => $value)
                {
                        if (count ($this -> aLanguages) > 1)
                        {
                                foreach ($this -> aLanguages as $lang)
                                {
                                        $pResult = Database :: getInstance () -> query ("
                                                SELECT
                                                    a.*,
                                                    ad.title,
                                                    ad.description,
                                                    ad.photos,
                                                    ad.page_description,
                                                    ad.slug,
                                                    DATE_FORMAT(a.date,'%d-%m-%Y') AS date_format,
                                                    gd.group_name,
                                                    gd.slug as group_slug,
                                                    g.id as group_id
                                                FROM
                                                    agenda a
                                                LEFT JOIN
                                                    agenda_data ad
                                                        ON
                                                    a.id = ad.agenda_id
                                                INNER JOIN
                                                    groups g
                                                        ON
                                                    a.group_id = g.id    
                                                        AND
                                                    g.module = 'agenda'
                                                LEFT JOIN
                                                    groups_data gd
                                                        ON
                                                    gd.group_id = g.id
                                                LEFT JOIN
                                                    languages l
                                                        ON
                                                    (ad.lang_id = l.id AND gd.lang_id = l.id) 
                                                WHERE 
                                                    ad.lang_id = " . $lang ['id'] . "
                                                AND
                                                    gd.lang_id = " . $lang ['id'] . "
                                                AND
                                                    a.id = " . $value ['id'] . "
                                                        " . (!empty ($sSearch)        ? "AND (ad.description LIKE '%" . Database :: getInstance () -> real_escape_string ($sSearch) . "%' OR agenda.title LIKE '%" . Database :: getInstance () -> real_escape_string ($sSearch) . "%')" : '') . "
                                                
                                        ");
                                        
                                        if (!$pResult)
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                
                                                return false;
                                        }  
                                        
                                        if ($pResult -> num_rows > 0)
                                        {         
                                                while ($aRow = $pResult -> fetch_assoc ()) 
                                                {          
                                                        if ($aRow ['title'] != '' || $aRow ['description'] != '')
                                                                $aRow ['filled'] = true;
                                                        $aRow ['photos']      = $this -> getPhotosByOids ($aRow ['photos']);
                                                        $aRow ['description'] = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                                                
                                                        if (preg_match_all ('%<a[^>]+>%s', $aRow ['description'], $aMatch))
                                                        {
                                                                $aLinks = $aMatch [0];
                                                                
                                                                foreach ($aLinks as $link)
                                                                {
                                                                        if (strpos ($link, 'onclick="') === false && 
                                                                        strpos ($link, 'target="_blank"') !== false && 
                                                                        preg_match ('%href="[^"]+"%s', $link, $matches))
                                                                        {
                                                                                $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                                                $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['title'] . '\', \'' . $sAddress . '\')"', $link);
                                                                                $aRow ['description'] = str_replace ($link, $sNewlink, $aRow ['description']); 
                                                                        }
                                                                }
                                                        }
                                                
                                                        $aResults [$k][$lang ['id']] = $aRow;
                                                }
                                        }      
                                } 
                        }
                        else
                        {
                                unset ($value ['filled']);
                                if ($value ['title'] != '' || $value ['description'] != '')
                                        $value ['filled'] = true;
                                        
                                $aResults [$k][$this -> aDefaultLang ['id']] = $value;
                        }
                }
                
                $aResults = (!empty ($iItemId) || !empty ($sItemSlug)) ? current ($aResults) : $aResults;
                
                return $aResults;
        }
        
        /**
        * @desc: return an array with all data about images
        * 
        * @param string $sImages
        * 
        * @returns: array
        **/ 
         
        private function getPhotosByOids ($sImages)
        {
                if (empty ($sImages))
                {
                        return false;
                }
                
                $aImages        = explode (',', $sImages);
                $aProductPhotos = array ();
                
                foreach ($aImages as $image_id)
                {
                        if ($image_id != '')
                        {
                                $aDetailPhoto = $this -> getPhoto ($image_id);
                                
                                if (isset ($aDetailPhoto ['id']) && $aDetailPhoto ['id'] != '')
                                { 
                                        array_push ($aProductPhotos, $aDetailPhoto);
                                }
                        }
                }
                
                
                for ($i = 0; $i < count ($aProductPhotos); $i++)
                { 
                  
                        $aPathImages = explode ('/', $aProductPhotos [$i]['image_name'], -1);
                        $sPath       = '';

                        foreach ($aPathImages as $folder)
                        {
                                if ($folder != '')
                                {
                                        $sPath .= '/' . $folder;
                                }
                        }

                        $aProductPhotos [$i]['pathphoto'] = $sPath;
                }
                
                return $aProductPhotos;
        }
        
        /**
        * @desc: get the data of an image 
        * 
        * @param int $iId
        * 
        * @returns array
        **/
        
        public function getPhoto ($iId = '')
        {
                if ($iId == '' || !is_numeric ($iId))
                {
                        return array ();
                }
                  
                $pQuery = Database :: getInstance () -> query ("
                        SELECT
                        *
                        FROM
                            images
                        WHERE
                            id = " . $iId . "
                        ORDER BY
                            id DESC
                        "
                );
                  
                if ($pQuery -> num_rows > 0)
                {
                        while ($aRow = $pQuery -> fetch_assoc ())
                        {
                                return $aRow;
                        }
                }
                
                return array ();
        }
	    
        /**
        * @desc: check the fields
        * 
        * @param array $aValues - fields that have to be checked
        *
        * @returns booleaan
        */
         
        private function checkFields ($aValues = array ())
        {
                if (empty ($aValues ['title'][$this -> aDefaultLang ['id']])) 
                {
			            $this -> aError [] = "Titel";
		        } 
                
		        if (empty ($aValues ['description'][$this -> aDefaultLang ['id']])) 
                {
			            $this -> aError [] = "Bericht";
		        }
                 
		        if (!empty ($aValues ['date'])) 
                {
                        $aDate = explode ('-', $aValues ['date']);
                    
                        if (count ($aDate) == 3)
                        {
                                foreach ($aDate as $number)
                                {
                                        if (is_numeric ($number))
                                        {
                                                if ($number != (int) $number)
                                                {
                                                        $this -> aError [] = "Datum";
                                                }
                                        }
                                        else
                                        {
                                                $this -> aError [] = "Datum";
                                        }
                                }
                        }
                        else
                        {
                                $this -> aError [] = "Datum";
                        }
                }
                
                if ($this -> aError) 
                {
                        return false;
                }
                
                return true;
        }
        
        /**
        * create the slug of an item
        * 
        * @param string $sParams - string to be converted
        * 
        * @returns string
        */
         
        public function urlize ($sParams)
        {
                $sTitle   = strtolower ($sParams);
                $aAllowed = preg_split ('//', 'abcdefghijklmnopqrstuvwxyz0123456789-+_');

                for ($i = 0, $j = strlen ($sTitle); $i < $j; $i++)
                {
                        if (!in_array ($sTitle  [$i], $aAllowed))
                        {
                                $sTitle  [$i] = '-';
                        }
                }

                for ($i = 0; $i < 3; $i++)
                {
                        $sTitle = str_replace ('--', '-', $sTitle);
                }
                
                return preg_replace ('/^ [\-]*(.+?) [\-]*$/s', '\\1', $sTitle);
        }

        /**
        * set nl date to ISO date
        *
        * @param string $sDate
        * 
        * @returns string 
        */
        
        private function nlToIsoDate ($sDate)
        {
    	        preg_match ('#([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})+#' , $sDate, $aMatches);
    	        $sNewDate = $aMatches [3] . '-' . $aMatches [2] . '-' . $aMatches [1];
    	        
                return $sNewDate;
        }
        
        /**
        * set ISO to nl data
        *
        * @param string $sDate
        * 
        * @returns string 
        **/
        
        public function isoToNlDate ($sDate)
        {
    	        preg_match ('#([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})+#' , $sDate, $aMatches);
    	        $sNewDate = $aMatches [3] . '-' . $aMatches [2] . '-' . $aMatches [1];
    	        
                return $sNewDate;
        }
        
        /**
        * @desc: insert the agenda item in the database
        *
        * @param array $aValues values of the item 
        * 
        * @returns booleaan
        */
                                      
        public function addAgenda ($aValues = array ())
        {
                if ($this -> checkFields ($aValues)) 
                {           
                    
                        // take the item with the biggest sort number
                        $aAgendaItems = $this -> getAgenda ();
                        $aAgendaItem  = (isset ($aAgendaItems [0]) ? $aAgendaItems [0] : '');
                        
                        // add post item
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT INTO
                                    agenda
                                    (
                                        group_id,
                                        price,
                                        date,
                                        order_number
                                    )
                                VALUES
                                    (
                                        ?,
                                        ?,
                                        ?,
                                        ?
                                    )
                        ");    
                        
                        $sDateStart   = $this -> nlToIsoDate ($aValues ['date']);
                        $iOrderNumber = (isset ($aAgendaItem ['order_number']) ? $aAgendaItem ['order_number'] + 1 : 0);
                        
                        $pStatement -> bind_param ('issi',
                                $aValues ['group_id'],
                                $aValues ['price'], 
                                $sDateStart,
                                $iOrderNumber
                        );
                        
                        if (!$pStatement -> execute ()) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        $iAgendaId = Database :: getInstance () -> insert_id;
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                                $pStatement = Database :: getInstance () -> prepare ("
                                        INSERT INTO
                                            agenda_data
                                            (
                                                agenda_id,
                                                lang_id,
                                                title,
                                                description,
                                                photos,
                                                page_description,
                                                slug
                                            )
                                        VALUES
                                            (
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?
                                            )
                                ");    
                                
                                $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]);
                                $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                
                                $pStatement -> bind_param ('iisssss',
                                        $iAgendaId,
                                        $lang ['id'],
                                        $aValues ['title'][$lang ['id']],
                                        $aValues ['description'][$lang ['id']],
                                        $sPhotos,
                                        $aValues ['page_description'][$lang ['id']],
                                        $sSlug
                                );
                                
                                if (!$pStatement -> execute ()) 
                                {
                                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                        
                                        return false;
                                }
                        }
                        
                        return true;
                }
                
                return false;
        }

        /**
        * @desc: update a agenda item
        *
        * @param int $iId - id of the item - mandatory
        * @param array $aValues - the values of the item
        * 
        * @returns booleaan
        */
                                 
        public function updateAgenda ($iId, $aValues = array ())
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                
                if ($this -> checkFields ($aValues)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    agenda
                                SET
                                    group_id         = ?,
                                    price            = ?,
                                    date             = ?
                                WHERE
                                    id               = ? 
                        ");
                        
                        $sDateStart = $this -> nlToIsoDate ($aValues ['date']);
                        
                        $pStatement -> bind_param ('issi',
                                $aValues ['group_id'],
                                $aValues ['price'],
                                $sDateStart,
                                $iId
                        );
                        
                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        $aLangValues = $this -> getAgenda ($iId, '', 0, '', 'order_number', 'desc');
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                                if (isset ($aLangValues [$lang ['id']]))
                                {
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                UPDATE
                                                    agenda_data
                                                SET
                                                    agenda_id        = ?,
                                                    title            = ?,
                                                    description      = ?,
                                                    photos           = ?,
                                                    page_description = ?,
                                                    slug             = ?     
                                                WHERE
                                                    agenda_id        = ?
                                                AND
                                                    lang_id          = ? 
                                        ");
                                        
                                        $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]);
                                        $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                        
                                        $pStatement -> bind_param ('isssssii',
                                                $iId,
                                                $aValues ['title'][$lang ['id']],
                                                $aValues ['description'][$lang ['id']],
                                                $sPhotos,
                                                $aValues ['page_description'][$lang ['id']],
                                                $sSlug,  
                                                $iId,
                                                $lang ['id']
                                        );
                                        
                                        if (!$pStatement -> execute ())
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                
                                                return false;
                                        }
                                }
                                else
                                {
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                INSERT INTO
                                                    agenda_data
                                                    (
                                                        agenda_id,
                                                        lang_id,
                                                        title,
                                                        description,
                                                        photos,
                                                        page_description,
                                                        slug
                                                    )
                                                VALUES
                                                    (
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?
                                                    )
                                        ");    
                                        
                                        $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]);
                                        $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                        
                                        $pStatement -> bind_param ('iisssss',
                                                $iId,
                                                $lang ['id'],
                                                $aValues ['title'][$lang ['id']],
                                                $aValues ['description'][$lang ['id']],
                                                $sPhotos,
                                                $aValues ['page_description'][$lang ['id']],
                                                $sSlug
                                        );
                                        
                                        if (!$pStatement -> execute ()) 
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                
                                                return false;
                                        }   
                                }
                        }
                        
                        return true;
                }
                
                return false;
        }
	    
	    /**
	    *@desc: delete an agenda item
	    *
        * @param int $iId - the id of the item
        * 
	    * @returns booleaan   
	    */
                      	    
	    public function deleteAgenda ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }   
                 
                $pStatement = Database :: getInstance () -> prepare ("
                    DELETE
                    FROM    
                        agenda
                    WHERE
                        id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                    DELETE
                    FROM    
                        agenda_data
                    WHERE
                        agenda_id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                return true;
        }
}   
?>
