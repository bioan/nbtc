<?php

/**
     @description: linkedin class
     @link: http://www.inforitus.nl
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class LinkedinCms
{ 
    
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * @desc contains all languages 
        * 
        * @var array
        */
        
        var $aLanguages = array ();
        
        /**
        * @desc contains default language 
        * 
        * @var array
        */
        
        var $aDefaultLang = array ();

        /**
        *@desc: contructor 
        *
        */
                      
        public function Linkedin () 
        { 
                $this -> aLanguages   = Language :: getLanguages ();
                $this -> aDefaultLang = Language :: getLanguages (0, '', 'id', 'asc', true);  
        }
        
        /**
        * @desc check App fields and insert or update database
        * 
        * @param array $aValues - array of values containing app id and app secret
        * 
        * @returns boolean
        */
             
        public function checkApplication ($aValues = array ())
        {
                if (empty ($aValues ['consumer_key']))
                {
                        $this -> aError ['consumer_key'] = "Consumer key is required";
                }
                
                if (empty ($aValues ['consumer_secret']))
                {
                        $this -> aError ['consumer_secret'] = "Consumer secrets. is required";
                }
                
                if (count ($this -> aError) == 0)
                {
                        $facebookAppData = $this ->getApplication();
                        
                        if (!$facebookAppData)
                        {
                                $this ->addApplication($aValues);
                        }
                        else 
                        {
                                $this ->updateApplication($aValues);
                        }
                        return true;
                }
                else
                {
                        return false;
                }
        } 
        
        /**
        * @desc get the linkedin Application
        * 
        * 
        * @returns array, data of application
        */
             
        public function getApplication ()
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            linkedin_app
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;          
                
		        return $aResults;
        } 
        
              
        /**
	    * @desc update Application
	    *
        * @param array $aValues - the values that have to be set
        *  
	    * @return boolean
	    */
        
	    public function updateApplication ($aValues = array ())
	    {
            
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            linkedin_app
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;
            
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            linkedin_app
                        SET
                            consumer_key     = ?,
                            consumer_secret  = ?,
                            date             = NOW()
                        WHERE
                            id         = " . $aResults['id'] ."
                ");
                
                $pStatement -> bind_param ('ss',    
                        $aValues ['consumer_key'],	
                        $aValues ['consumer_secret']
                );

                if (!$pStatement -> execute ()) 
                {	
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    

                    return false;
                } 

                return true;
            }
        
         
        
              
        /**
	    * @desc update Application status (if correct or not)
	    *
        * @param array $aValues - the values that have to be set
        *  
	    * @return boolean
	    */
        
	    public function updateApplicationStatus ($correct)
	    {
            
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            linkedin_app
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;
            
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            linkedin_app
                        SET
                            correct     = ?
                        WHERE
                            id         = " . $aResults['id'] ."
                ");
                
                $pStatement -> bind_param ('i',    
                        $correct
                );

                if (!$pStatement -> execute ()) 
                {	
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    

                    return false;
                } 

                return true;
            }
       
        
        /**
	    * @desc add linkedin application
	    *
        * @param array $aValues - the values that have to be set
        * 
	    * @return boolean
	    */
	    
	    public function addApplication ($aValues = array ())
	    {	
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT
                        INTO
                            linkedin_app
                                (
                                consumer_key,
                                consumer_secret,
                                date                
                                )      
                        VALUES
                                (
                                ?,
                                ?,
                                NOW()               
                                ) 
                ");

                $pStatement -> bind_param ('ss',
                        $aValues ['consumer_key'],
                        $aValues ['consumer_secret']				
                );

                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   

                        return false;
                }

                return true;
        }
        
        public function checkLinkedinUser ($aValues = array ())
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
                            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            linkedin_users
                        WHERE
                            user_id = " . $aValues ['user_id'] . "
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                
                if ($pResult -> num_rows > 0)
                {         
                        $this -> updateLinkedinUser ($aValues);
                }
                else
                {
                        $this ->addLinkedinUser($aValues);
                }

                return true;
        } 
        
         /**
	    * @desc add linkedin users
	    *
        * @param array $aValues - the values that have to be set
        * 
	    * @return boolean
	    */
	    
	    public function addLinkedinUser ($aValues = array ())
	    {	
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT
                        INTO
                            linkedin_users
                                (
                                user_id,
                                oauth_token,
                                oauth_token_secret,
                                date                
                                )      
                        VALUES
                                (
                                ?,
                                ?,
                                ?,
                                NOW()               
                                ) 
                "); 

                $pStatement -> bind_param ('iss', 
                        $aValues ['user_id'],
                        $aValues ['oauth_token'],
                        $aValues ['oauth_token_secret']
                );

                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   

                        return false;
                }

                return true;
        }
        
        
         /**
	    * @desc update facebook user
	    *
        * @param array $aValues - the values that have to be set
        *  
	    * @return boolean
	    */
        
	    public function updateLinkedinUser ($aValues = array ())
	    {            
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            linkedin_users
                        SET
                            user_id             = ?,
                            oauth_token         = ?,
                            oauth_token_secret  = ?,
                            date                = NOW()
                        WHERE
                            user_id         = " . $aValues['user_id'] ."
                ");

                $pStatement -> bind_param ('iss',
                        $aValues ['user_id'],
                        $aValues ['oauth_token'],
                        $aValues ['oauth_token_secret']
                );

                if (!$pStatement -> execute ()) 
                {	
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    

                    return false;
                } 

                return true;
            }
            
                     /**
        * @desc get Linkedin user if exists
        * 
        * @param array parameters 
        * @returns array
        */
        
        public function getLinkedinUser ($userID)
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            linkedin_users
                        WHERE
                            user_id = " . $userID . "
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;          
                
		        return $aResults;
        }
}   
?>
