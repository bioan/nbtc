<?php

/**
 * base class for validate the fields of a form.
 * @author Andrei Dragos <andrei@inforitus.nl>
 */
   
class ValidateFields
{
        /**
        * @desc array of errors
        * 
        * @var array
        */
        
        var $aErrors = array ();
        
        /**
        * @desc constructor
        *
        */
        
        public function ValidateFields () {}
        
        /**
        * @desc returns the last error
        *
        * @returns array
        */
         
        public function getLastError ()
        {
                return $this -> aErrors;
        }                        
        
        /**
        * @desc Validate input. Only valid date allowed
        * 
        * @param string $sValue
        * 
        * @return booleaan
        */
         
        public function validateDate ($sValue)
        {
                preg_match ('#^( [0-9]{4})-(\d\d)-(\d\d)$#', $sValue, $matches);
                
                return checkdate ($matches [2],$matches [3],$matches [1]);
        }
        
        /**
        * @desc Validate input. Only digits allowed.
        * 
        * @param string $sValue
        * 
        * @return booleaan
        */
                                      
        public function onlyDigits ($sValue)
        {
                return preg_match ('#^ [0-9]+$#', $sValue);
        }
        
        /**
        * @desc Validate input. email
        * 
        * @param string $value
        * 
        * @return booleaan
        */
         
        public function validateEmail ($sValue)
        {
                return preg_match ('#^ [A-Za-z0-9_-]+( [\.]{1} [A-Za-z0-9_-]+)*@ [A-Za-z0-9-]+( [\.]{1} [A-Za-z0-9-]+)+#', $value);
        }
        
        /**
        * @desc Validate input. amount
        * 
        * @param string $sValue
        * 
        * @returns booleaan
        */
         
        public function validateAmount ($sValue)
        {
                if ($sValue > 19.99) 
                {
                        return true;
                }
            
                return false;
        }
        
        /**
        * @desc Get the posten value of a field
        * 
        * @param string $sField
        */
             
        public function getValue ($sField)
        {
                return (!empty ($_POST [$sField])) ? $_POST [$sField] : '';
        }

        /**
        * @desc Check the fields
        *
        * @param array $aFields     
        * 
        * @return booleaan
        */
         
        public function checkFields ($aFields)
        {
                foreach ($aFields as $f => $l)
                {
                        foreach ($aFields [$f] AS $field => $check) 
                        {
                                if (empty ($_POST [$f][$field]) && empty ($check)) 
                                {
                                        $this -> aErrors [$f][$field] = $field;
                                } 
                                elseif ((empty ($_POST [$f][$field]) || !empty ($_POST [$f][$field])) && !empty ($check)) 
                                {
                                        if (!$this -> $check ($_POST [$f][$field])) 
                                        {
                                                $this -> aErrors [$f][$field] = $field;
                                        }
                                }
                        }
                }
                
                return (!empty ($this -> aErrors)) ? false : true;
        }                
          
}
?>
