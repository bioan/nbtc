<?php   

/**
    @description: Setting class - manage the settings of cms/site
    @author Andrei Dragos <andrei@inforitus.nl>
**/
    
class Setting
{
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * @desc constructor
        *
        */
        
	    public function Setting () 
        { 
            
        }  
        
        /**
	    * @desc get the last errors
	    *
	    * @return array
	    */
	    
	    public function getLastError ()
        {
            return $this -> aError;
        }
        
        /**
        * @desc update the settings of cms/site
        * 
        * @param array $aValues - the settings of cms/site
        * 
        * @return boolean
        */
        
        public function updateSettings ($aValues = array ()) 
        {
                foreach ($aValues as $k => $v)
                {
                        $pResult = Database :: getInstance () -> query ("
                                UPDATE
                                    settings
                                SET
                                    value   = '" . $v . "'
                                WHERE
                                    field = '" . $k . "' 
                        ");
                        
                        if (!$pResult) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                } 
                
                return true;
        }
        
        /**
        * @desc get the settings of cms/site
        * 
        * @return array
        */
        
        public function getSettings ()
        {
                $pResult = Database :: getInstance () -> query ("
                        SELECT
                            *
                        FROM
                            settings
                ");
                
                
                $aResults = array ();   
                  
                if ($pResult -> num_rows > 0)
                {
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                $aResults [$aRow ['field']] = $aRow ['value'];
                        }
                }
                 
                return $aResults;
        }
    
}
?>
