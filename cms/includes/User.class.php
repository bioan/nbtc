<?php   

/**
    @description: user class 
    @link: http://www.inforitus.nl
    @author Andrei Dragos <andrei@inforitus.nl>
**/
    
class User
{
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * A simple array containing all data which is relevant for this user. It will
        * be set in the constructor if we're logged in, or, for low-traffic websites,
        * once for every request to ensure the data is up-to-date.
        *
        * @var array
        */
        
        private $m_aUserInfo;
        
        /**
        * @desc constructor
        *
        */
        
	    public function User () 
        { 
            
        } 
        
        /**
        * Getting all user information, for debug-layout rendering for example,
        * can be done by using this mtehod. Everything will be properly returned.
        *
        * @return array An array with all relevant user information.
        */
        
        public function getUserInfo ()
        {
                return $this -> m_aUserInfo;
        }
        
        /**
	    * @desc get the last errors
	    *
	    * @return array
	    **/
	    
	    public function getLastError ()
        {
                return $this -> aError;
        }
        
        /**
	    * @desc check the data of a user by adding a user
	    *
	    * @param bool $sControlUser 
        * @param array $aValues - the values that have to be checked
	    * 
        * @return booleaan
	    */
	    
        private function controlByAddAUser ($bControlUser, $aValues = array ())
        {
                if (empty ($aValues ['user_name'])) 
                {
			            $this -> aError [] = "Gebruikersnaam";
     	        }
                
     	        if ($bControlUser === true) 
                {
    		            $sQuery = sprintf ("
                                SELECT
                                    *
                                FROM
                                    users
                                WHERE
                                    user_name = '%s'
                                ",
                                Database :: getInstance () -> real_escape_string ($aValues ['user_name'])
                        );
                        
                        if (($pResult = Database :: getInstance () -> query ($sQuery))===false) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   
                                
                                return false;
                        }
                        else 
                        {
                                if ($pResult -> num_rows > 0 && !empty ($aValues ['user_name']))
                                {
                                        $this -> aError [] = "De ingevoerde gebruikersnaam is al in gebruik. Kies een andere naam. ";   
                                }    
                        }
                }       
                
		        if (empty ($aValues ['password']) && empty ($aValues ['password_hidden'])) 
                {
			            $this -> aError [] = "Wachtwoord";
     	        }
                                                 
                $sEmail = filter_var ($aValues  ['email'], FILTER_VALIDATE_EMAIL);     
     	        
                if ($sEmail == false) 
                {
                        $this -> aError [] = "Emailadres: geen juiste notatie!";    
                }
                
                if (!$this -> aError) 
                {
                        return true;
                }
                
                return false;
        }
        
        /**
	    * @desc add user
	    *
        * @param array $aValues - the values that have to be set
        * 
	    * @return boolean
	    */
	    
	    public function addUser ($aValues = array ())
	    {	
		        if ($this -> controlByAddAUser ($controlUser = true, $aValues)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT
                                INTO
                                    users
                                       (
						                user_name,
						                password,
						                email,
						                date,
						                status, 
                                        active                 
                                       )      
                                VALUES
                                       (
                                        ?,
                                        ?,
                                        ?,
						                NOW(),
						                ?,
                                        1                 
                                       ) 
                        ");
                        
                        $sPassword = md5 ($aValues ['password']);
                        
                        $pStatement -> bind_param ('sssi',
				                $aValues ['user_name'],
				                $sPassword,
                                $aValues ['email'],
				                $aValues ['status']				
                        );
                        
                        if (!$pStatement -> execute ()) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   
                                 
                                return false;
                        }
             	            
			            return true;
                }
            
	            return false;	
        }  
        
        /**
	    * @desc get user(s)
	    *
	    * @param int $iUserId - the id of user
	    * @param string $sSortField - the user are sorted by this field
        * @param string $sSortType - desc/asc
	    * 
        * @return array
	    */
        
	    public function getUsers ($iUserId = 0, $sSortField = 'id', $sSortType = 'desc') 
        {                     
                if (!is_numeric ($iUserId))
                { 
                        return false;
                }
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            users
			            %s
			            ORDER BY
				            %s %s
                        ",
                        (!empty ($iUserId)) ? sprintf ("WHERE id = %d", $iUserId) : '',
                         Database :: getInstance () -> real_escape_string ($sSortField),
                         Database :: getInstance () -> real_escape_string ($sSortType)
                );	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (!empty ($iUserId)) ? current ($aResults) : $aResults;          
                
		        return $aResults;
        }
        
        /**
	    * @desc update user
	    *
	    * @param int $iId - id of user
        * @param array $aValues - the values that have to be set
        *  
	    * @return boolean
	    */
        
	    public function updateUser ($iId, $aValues = array ())
	    {
		        if (!is_numeric ($iId))
                { 
                        return false;
                }
    	        
                if ($this -> controlByAddAUser ($bControlUser = false, $aValues)) 
                {
    		            $pStatement = Database :: getInstance () -> prepare ("
    				            UPDATE
    					            users
    				            SET
    					            user_name = ?,
                                    password  = ?,
                                    email     = ?,
    					            status    = ?,
    					            active    = ?
    			                WHERE
    	                            id        = ? 
    		            ");
                        
                        $sPassword = (!empty ($aValues ['password']) ? md5 ($aValues ['password']) : $aValues ['password_hidden']);
                        
                        $pStatement -> bind_param ('ssssii',    
    			                $aValues ['user_name'],
    			                $sPassword,
                                $aValues ['email'],
                                $aValues ['status'],	
                                $aValues ['active'],
    			                $iId
    		            );
                        
    		            if (!$pStatement -> execute ()) 
                        {	
    			            trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    
        		            
                            return false;
                        } 
                        
                        return true;
                }
                
                return false;
	    }
	    
	    /**
	    * @desc get the logins of the user
	    *
	    * @param int $iId - id of user 
	    * 
        * @return array or false by error
	    */
	    
	    public function getLogins ($iId)
	    {
                if (is_int ($iId))
                {
                        return false;
                }
                
                $sQuery = sprintf ("
                        SELECT
                            *
                        FROM
                            logins
                        WHERE
                            gebruikers_id = %d
                        ORDER BY
                            tijdstip DESC
                        LIMIT
                            0,25
                    ",
                        $iId
                );
                
    	        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {	
			            trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                             
    		            return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                return $aResults;
        }
        
        /**
        * @desc controll - changing old password
        *
        * @param int $iId 
        * 
        * @returns booleaan
        */
        
        private function controlChangePassword ($iId)
        {
                if (!is_numeric ($iId))
                { 	
			            return false;  
                }
                
                if (empty ($_POST ['old_password'])) 
                {
                        $this -> aError [] = 'Nieuwe wachtwoord moet ingevuld zijn om het te kunnen wijzigen.';
                }
                
                $sQuery = sprintf ("
                        SELECT
                            password
                        FROM
                            users
                        WHERE
                            id = %d
                        ",
                        $id
                );
                
                if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
    		            return false;
                } 
                else 
                {
                        $aRow = $pResult -> fetch_assoc ();
                        
                        if ((md5 ($_POST ['old_password']) != $aRow ['password']) && $_POST ['old_password'] != '')
                        { 
                                $this -> aError [] = 'Uw oude wachtwoord is niet juist ingevuld.';
                        }
                }
                
                if (empty ($_POST ['new_password']))
                { 
                        $this -> aError [] = 'Er dient wel een nieuw wachtwoord te zijn ingevoerd.';
		        }
                
                if (!preg_match ('#^( [0-9A-Za-z_-])+$#', $_POST ['new_password'], $matches) && !empty ($_POST ['new_password'])) 
		        {
        	            $this -> aError [] = 'Het nieuwe wachtwoord mag geen vreemde tekens bevatten';
		        }
                
                if (empty ($_POST ['control_password'])) 
                {
                        $this -> aError [] = 'Er dient wel een controle wachtwoord te zijn ingevoerd.';
                }
                
                if (($_POST ['control_password']) != $_POST ['new_password']) 
                {
                        $this -> aError [] = 'Wachtwoorden zijn niet gelijk aan elkaar.';
                }
                
                if ((md5 ($_POST ['new_password']) == $aRow ['password']) && $_POST ['new_password'] != '')
                {
                        $this -> aError [] = 'Het nieuwe wachtwoord kan niet gelijk zijn aan het nieuwe wachtwoord.';
		        }
                
                if (!$this -> aError) 
                {
                        return true;
                }
                
                return false;
        }
        
        /**
	    * @desc: Update password
	    *
	    * @param int $iId - id of the user
        *  
	    * @returns boolean
	    */

	    public function changePassword ($iId)
	    {
                if ($this -> controlChangePassword ($iId)) 
                {
    		            $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    users
                                SET
                                    password = ?
    				            WHERE
    					            id       = ?
                        ");
                        
                        $sNewPassword = md5 ($_POST ['new_password']);
                        
                        $pStatement -> bind_param ('si',
                                $sNewPassword,
    				            $iId
                        );
                        
                        if (!$pStatement -> execute ()) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);            
    		  	                
                                return false;
                        }
                        
                        return true;
                }
                
                return false;
        }
        
        /**
	    * @desc: Delete user
	    *
	    * @param int $iId - id of user 
	    * 
        * @return boolean
	    */

	    public function deleteUser ($iId)
	    {
               if (!is_numeric ($iId))
               { 
                        return false;
               }
               
               // delete rights
               $oRights = new UserRights;
               
               if ($oRights -> deletePermissions ($iId)) 
               {
    		            $pStatement = Database :: getInstance () -> prepare ("
                                DELETE
                                FROM
                                    users
    				            WHERE
    					            id = ?
                        ");
                        
                        $pStatement -> bind_param ('i',
    				            $iId
                        );
                        
                        if (!$pStatement -> execute ()) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   
                                
    		  	                return false;
                        }
                        
                        return true;
                }
                
                return true;
        }
        
        /**
        * @desc: update the active of the user
        *
        * @param int $iId - id of user
        * @param int $iActive - an user can be active or not
        * 
        * @returns booleaan
        */
                                           
        public function updateActiveUser ($iId, $iActive)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            users
                        SET
                            active  = ?
                        WHERE
                            id      = ?
                ");
                
                $pStatement -> bind_param ('ii',
                        $iActive,
                        $iId
                );
                
                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                         
		  	            return false;
                }
                
                return true;
        }
        
        /**
        * @desc check if the email from forgot password exists is correct
        *
        * @param string $sEmail - the email the have to be checked
        * 
        * @return booleaan
        */
      
        public function checkIfEmailExist ($sEmail = '')
        {
                $pResult = Database :: getInstance () -> query ("
                      SELECT 
                          *
                      FROM 
                          users
                      WHERE 
                          email = '" . $sEmail . "'
                              "
                );

                if ($pResult -> num_rows == 0)
                {     
                        return false;
                } 

                return true;
        }
        
        /**
        * @desc Create a random password
        *
        * @param int $iLength number of characters
        * 
        * @return  string
        */

        public function random_password ($iLength = 8)
        {          
                $sChars    = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
                $sPassword = '';

                for ($i=0; $i < $iLength; $i++)
                {
                        $sPassword .= substr ($sChars, mt_rand (0, strlen ($sChars) -1), 1);
                }

                return $sPassword;
        }
        
        /**
        * @desc Log out the logged in user to the logged in user is able to log in again
        * if they like. Of course we only process the log-less log out! All cookies,
        * sessions and associated information will be removed.
        * 
        * @return bool
        */
        
        public function logout ()
        {
                unset ($_SESSION ['user_info']);
                
                if (isset ($_SESSION ['session_storage']))
                {
                        unset ($_SESSION ['session_storage']);
                }
                
                setcookie (session_name (), '', time () - 3600, '/', ini_get ('session.cookie_domain'), false, true);
                session_destroy ();
                
                return true ;
        }
        
        /**
	    * @desc function for export the emails in a xls format data
	    * 
	    * @param string $aData
        * 
	    */
	     
	    public function export ($sData)
	    {
		        header ("Pragma: public");
		        header ("Expires: 0");
		        header ("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		        header ("Cache-Control: private", false);
		        header ("Content-Type: application/vnd.ms-excel");
		        header ("Content-Disposition: attachment; fileName=gebruikers.xls;" );
		        //header("Content-Transfer-Encoding: binary");
		        header ("Content-Transfer-Encoding: base64");
		        header ("Content-Length: " . strlen ($data));
		        echo $sData;
		        exit;
	    }	
}
?>
