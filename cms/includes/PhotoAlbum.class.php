<?php
     
/**
     @description: PhotoAlbum class
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class PhotoAlbum extends Tree
{ 
        /**
        * @desc the tree id specific to pages
        * 
        * @var integer
        */
        
        var $iTreeId = 3;
        
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
	    
        /**
        * @desc: contructor.
        *
        */
                      
        public function PhotoAlbum ()
        {         
                parent::Tree ($this -> iTreeId);
            
                $this -> InitData ();    
        }
        
        /**
        * @desc: get the last errors
        * 
        * @returns array
        */
         
        public function getLastError ()
        {
                return $this -> aError;
        }      
         
        /**
        * @desc: check the data of the form
        * 
        * @param array $aValues - the values that have to be checked  
        * 
        * @returns: booleaan
        */
                                 
        private function checkForm ($aValues) 
        {
                /*if (empty ($aValues ['title'][$this -> aDefaultLang ['id']])) 
                {
                        $this -> aError [] = 'Titel';
                }*/
                
                if (empty ($aValues ['photos'][$this -> aDefaultLang ['id']])) 
                {
                        $this -> aError [] = 'Foto\'s';
                }
                
                if (empty ($this -> aError)) 
                {
                        return true;
                }
                
                return false;
        }
        
        /**
        * @desc: add a photo to a group
        * 
        * @param array $aValues 
        * 
        * @returns: booleaan
        */
                                 
        public function addPhoto ($aValues)
        {
                if ($this -> checkForm ($aValues)) 
                {
                        // take the item with the biggest order number
                        $aPhotoItems = $this -> getPhotos ();
                        $aPhotoItem  = (isset ($aPhotoItems [0]) ? $aPhotoItems [0] : '');
                        
                        // add post item
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT
                                INTO
                                    photoalbum
                                    (
                    	                category_id,
                                        date,
                                        order_number
                                    )
                                VALUES
                                    (
                    	                ?,
                                        NOW(),
                                        ?
                                    )
                                "
                        );
                        
                        $iOrderNumber = (isset ($aPhotoItem ['order_number']) ? $aPhotoItem ['order_number'] + 1 : 0);
                        
                        $pStatement -> bind_param ('ii',
                                    $aValues ['category'],    
                                    $iOrderNumber
                        );
                            
                        if (!$pStatement -> execute ()) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        } 
                        
                        $iPhotoalbumId = Database :: getInstance () -> insert_id;
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                                $pStatement = Database :: getInstance () -> prepare ("
                                        INSERT
                                        INTO
                                            photoalbum_data
                                            (
                            	                photoalbum_id,
                                                lang_id,
                                                title,
                                                description,
                                                photos,
                                                page_description,
                                                slug
                                            )
                                        VALUES
                                            (
                            	                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?
                                            )
                                        "
                                );
                                
                                $sSlug   = $this -> urlize ($aValues  ['title'][$lang ['id']]);
                                $sPhotos = (!empty ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                
                                $pStatement -> bind_param ('iisssss',
                                            $iPhotoalbumId,
                                            $lang ['id'],
                                            $aValues ['title'][$lang ['id']],
                                            $aValues ['description'][$lang ['id']],
                                            $sPhotos,
                                            $aValues ['page_description'][$lang ['id']],
                                            $sSlug
                                );
                                    
                                if (!$pStatement -> execute ()) 
                                {
                                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                        
                                        return false;
                                } 
                        }
                        
			            return true;           
                }
                
                return false;
        }
         
        /**
        * @desc: return photos
        * 
        * @param int $iItemId Optional
        * @param string $sItemSlug Optional 
        * @param int $iCatSlug Optional
        * @param string $sCatSlug Optional
        * @param string $sSortField Optional
        * @param string $sSortType Optional 
        * 
        * @returns array 
        */
                                       
        public function getPhotos ($iItemId = 0, $sItemSlug = '', $iCatId = 0, $sCatSlug = '', $sSortField = 'order_number', $sSortType = 'desc', $sLanguage = 'default')
        {          
                if (!is_numeric ($iItemId) || !is_numeric ($iCatId))
                { 
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT
                            p.*,
                            pd.title,
                            pd.description,
                            pd.photos,
                            pd.page_description,
                            pd.slug,
                            DATE_FORMAT(p.date,'%d-%m-%Y') AS date_format,
					        pages.name AS category_name,
                            (SELECT COUNT(*) FROM photoalbum_data WHERE photoalbum_id = p.id AND title != '' AND description != '') as filled
                        FROM
                            photoalbum p
                		        INNER JOIN
                	        tree t
                		        ON
                			        p.category_id = t.oid
                				        AND
                			        t.type_oid = 3
                        LEFT JOIN 
                            photoalbum_data pd
                                ON
                            pd.photoalbum_id = p.id
                        LEFT JOIN 
                            pages 
                                ON
                            pages.tree_oid = t.oid
                        LEFT JOIN
                            languages l
                                ON
                            (pages.lang_id = l.id AND pd.lang_id = l.id)
                        WHERE
                            1
                            " . ($sLanguage == 'default' ? sprintf ("AND l.default = 1") : sprintf ("AND l.short_name = '%s'", Database :: getInstance () -> real_escape_string ($sLanguage))) . "
                            " . (!empty ($iItemId)       ? sprintf ("AND p.id = %d", $iItemId) : '') . "
                            " . (!empty ($sItemSlug)     ? sprintf ("AND pd.slug = '%s'", Database :: getInstance () -> real_escape_string ($sItemSlug)) : '') . "
                            " . (!empty ($iCatId)        ? sprintf ("AND p.category_id = %d", $iCatId) : '') . "
                            " . (!empty ($sCatSlug)      ? sprintf ("AND pages.slug = '%s'", Database :: getInstance () -> real_escape_string ($sCatSlug)) : '') . " 
                        ORDER BY
                            " . $sSortField . " " . $sSortType . " 
                ");     
                
                $aResults = array ();   
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;   
                }
                  
                if ($pResult -> num_rows > 0)
                {
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                $aRow ['photos'] = $this -> getPhotosByIds ($aRow ['photos']);
                                $aRow ['description'] = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                                
                                if (preg_match_all ('%<a[^>]+>%s', $aRow ['description'], $aMatch))
                                {
                                        $aLinks = $aMatch [0];
                                        
                                        foreach ($aLinks as $link)
                                        {
                                                if (strpos ($link, 'onclick="') === false && 
                                                strpos ($link, 'target="_blank"') !== false && 
                                                preg_match ('%href="[^"]+"%s', $link, $matches))
                                                {
                                                        $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                        $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['title'] . '\', \'' . $sAddress . '\')"', $link);
                                                        $aRow ['description'] = str_replace ($link, $sNewlink, $aRow ['description']); 
                                                }
                                        }
                                }
                                
                                $aResults []     = $aRow;
                        }
                }
                
                foreach ($aResults as $k => $value)
                {
                        if (count ($this -> aLanguages) > 1)
                        {
                                foreach ($this -> aLanguages as $lang)
                                {
                                        $pResult = Database :: getInstance () -> query ("
                                                SELECT
                                                    p.*,
                                                    pd.title,
                                                    pd.description,
                                                    pd.photos,
                                                    pd.page_description,
                                                    pd.slug,
                                                    DATE_FORMAT(p.date,'%d-%m-%Y') AS date_format,
                        					        pages.name AS category_name
                                                FROM
                                                    photoalbum p
                                        		        INNER JOIN
                                        	        tree t
                                        		        ON
                                        			        p.category_id = t.oid
                                        				        AND
                                        			        t.type_oid = 3
                                                LEFT JOIN 
                                                    photoalbum_data pd
                                                        ON
                                                    pd.photoalbum_id = p.id
                                                LEFT JOIN 
                                                    pages 
                                                        ON
                                                    pages.tree_oid = t.oid
                                                LEFT JOIN
                                                    languages l
                                                        ON
                                                    (pages.lang_id = l.id AND pd.lang_id = l.id)
                                                WHERE
                                                    l.id = " . $lang ['id'] . "
                                                AND
                                                    p.id = " . $value ['id'] . "
                                        ");      
                                        
                                        if (!$pResult)
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                
                                                return false;   
                                        }
                                        
                                        if ($pResult -> num_rows > 0)
                                        {
                                                while ($aRow = $pResult -> fetch_assoc ()) 
                                                {
                                                        if ($aRow ['title'] != '' || $aRow ['description'] != '')
                                                                $aRow ['filled'] = true;
                                                        $aRow ['photos'] = $this -> getPhotosByIds ($aRow ['photos']);        
                                                        $aRow ['description'] = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                                                        
                                                        if (preg_match_all ('%<a[^>]+>%s', $aRow ['description'], $aMatch))
                                                        {
                                                                $aLinks = $aMatch [0];
                                                                
                                                                foreach ($aLinks as $link)
                                                                {
                                                                        if (strpos ($link, 'onclick="') === false && 
                                                                        strpos ($link, 'target="_blank"') !== false && 
                                                                        preg_match ('%href="[^"]+"%s', $link, $matches))
                                                                        {
                                                                                $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                                                $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['title'] . '\', \'' . $sAddress . '\')"', $link);
                                                                                $aRow ['description'] = str_replace ($link, $sNewlink, $aRow ['description']); 
                                                                        }
                                                                }
                                                        }
                                                             
                                                        $aResults [$k][$lang ['id']] = $aRow;
                                                }
                                        }               
                                }
                        }
                        else
                        {   
                                unset ($value ['filled']);
                                if ($value ['title'] != '' || $value ['description'] != '')
                                        $value ['filled'] = true;
                                        
                                $aResults [$k][$this -> aDefaultLang ['id']] = $value;
                        }
                }
                
                $aResults = (!empty ($iItemId) || !empty ($sItemSlug)) ? current ($aResults) : $aResults;
                                                                              
                return $aResults;
        }
	     
	    /**
        * @desc: changed the specified photo 
        * 
        * @param int $iId
        * @param array $aValues
        * 
        * @returns booleaan
        */
                                       
        public function updatePhoto ($iId, $aValues) 
        {
                if (!is_numeric ($iId)) 
                {
                        return false;
                }
                     
                if ($this -> checkForm ($aValues)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    photoalbum
                                SET
                                    category_id      = ?
                                WHERE
                                    id               = ?
                        ");
                        
                        $pStatement -> bind_param ('ii',
                                $aValues  ['category'],
                                $iId     
                        );       

                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        } 
                        
                        $aLangValues = $this -> getPhotos ($iId, '', 0, '', 'order_number', 'desc');
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                                if (isset ($aLangValues [$lang ['id']]))
                                {
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                UPDATE
                                                    photoalbum_data
                                                SET
                                                    title            = ?,
                                                    description      = ?,
                                                    photos           = ?,
                                                    page_description = ?,
                                                    slug             = ?
                                                WHERE
                                                    photoalbum_id    = ?
                                                AND 
                                                    lang_id          = ?
                                        ");
                                        
                                        $sSlug   = $this -> urlize ($aValues  ['title'][$lang ['id']]);  
                                        $sPhotos = (!empty ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                        
                                        $pStatement -> bind_param ('sssssii',
                                                $aValues ['title'][$lang ['id']],
                                                $aValues ['description'][$lang ['id']],
                                                $sPhotos,
                                                $aValues ['page_description'][$lang ['id']],
                                                $sSlug,
                                                $iId,
                                                $lang ['id']     
                                        );       
                
                                        if (!$pStatement -> execute ())
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                
                                                return false;
                                        } 
                                }
                                else
                                {
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                INSERT
                                                INTO
                                                    photoalbum_data
                                                    (
                                    	                photoalbum_id,
                                                        lang_id,
                                                        title,
                                                        description,
                                                        photos,
                                                        page_description,
                                                        slug
                                                    )
                                                VALUES
                                                    (
                                    	                ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?
                                                    )
                                                "
                                        );
                                        
                                        $sSlug   = $this -> urlize ($aValues  ['title'][$lang ['id']]);
                                        $sPhotos = (!empty ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                        
                                        $pStatement -> bind_param ('iisssss',
                                                    $iId,
                                                    $lang ['id'],
                                                    $aValues ['title'][$lang ['id']],
                                                    $aValues ['description'][$lang ['id']],
                                                    $sPhotos,
                                                    $aValues ['page_description'][$lang ['id']],
                                                    $sSlug
                                        );
                                            
                                        if (!$pStatement -> execute ()) 
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                
                                                return false;
                                        }        
                                }
                        }
                        
                        return true;
                }
                
                return false;
        }
         
        /**
        * @desc: delete a photo
        * 
        * @param int $iId - id of item
        * 
        * @returns: booleaan
        */
          
	    public function deletePhoto ($iId)
	    {
		        if (!is_numeric ($iId))
                { 
			            return false;
                }
                    
		        $pStatement = Database :: getInstance () -> prepare ("
		                DELETE
		                FROM
		                    photoalbum
		                WHERE
		                    id = ?
		        ");
                
                $pStatement -> bind_param ('d',
		                $iId
		        );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
		                DELETE
		                FROM
		                    photoalbum_data
		                WHERE
		                    photoalbum_id = ?
		        ");
                
                $pStatement -> bind_param ('d',
		                $iId
		        );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
		        return true;
	    }                             
     
}
?>
