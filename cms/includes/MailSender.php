<?php
/**
 * Inforitus Server Control Panel v1.1
 *
 * This is the second revision of the Inforitus Server Control panel. In order to
 * keep performance at a maximum, without loosing flexibility or cleanness of the
 * code (which was, in all fairness, becoming a mess), a large part of the kernel
 * has been written to meet higher standards.
 *
 * This second revision supports unit tests, has a better architecture and is
 * better optimalized for front-end websites. Lots of components have been moved
 * to their own file rather than being included in the Kernel or one of the
 * Managers, making the code a lot easier to maintain.
 *
 * Author: Andrei Dragos
 *         andrei@inforitus.nl
 *
 */

class MailSender
{
	/**
	 * Constants: SMTP_ERROR_*
	 *
	 * These constants define the stages of the SMTP delivery process, which will
	 * indicate the position at which we currently are.
	 */
	
	const	SMTP_SUCCESS				= 1;
	
	const	SMTP_ERROR_UNSURE			= 2;
	const	SMTP_ERROR_UNAVAILABLE			= 3;
	const	SMTP_ERROR_MAILBOX_UNAVAILABLE		= 4;
	const	SMTP_ERROR_INSUFFICIENT_STORAGE		= 5;
	const	SMTP_ERROR_INSUFFICIENT_SYS_STORAGE	= 6;
	const	SMTP_ERROR_LOCAL_ERROR			= 7;
	
	/**
	 * Property: m_pMailMessage
	 *
	 * Defines the e-mail message that will be sent using this mailsender
	 * instance, to be defined using the constructor method.
	 */
	
	private $m_pMailMessage;

	/**
	 * Function: __construct
	 * Argument: pMailMessage (MailMessage) - The e-mail message to sent
	 * 
	 * This function will initialise the mailsender class which will allow
	 * us to distribute the message in question.
	 */
	
	public function __construct (MailMessage $pMailMessage)
	{
		if ($pMailMessage === null || $pMailMessage -> validate () === false)
			throw new Exception ('The e-mail message is not valid, please check your information.');
		
		$this -> m_pMailMessage = $pMailMessage;
	}
	
	/**
	 * Function: send
	 *
	 * The send method will attempt to distribute the e-mail using the sendmail
	 * daemon which should be around on this very server.
	 */
	
	public function send ()
	{
		$sFilename = tempnam ('/tmp', 'infMsnd');
		$aCommandSequence = array ();
		
		file_put_contents ($sFilename, $this -> m_pMailMessage -> render ());
		
		$aMailOutput = explode (PHP_EOL, shell_exec ('/usr/sbin/sendmail -t -i -v < ' . $sFilename . ' 2>&1'));
	        array_unshift ($aMailOutput, 'SMTP>> CONNECT ');
		
		for ($i = 0, $j = count ($aMailOutput); $i < $j; $i ++)
		{
			$sLine = trim ($aMailOutput [$i]) . ' ';
                        if (strpos ($sLine, 'virtual_localdelivery') !== false)
                                return self :: SMTP_SUCCESS;
                        
			if (substr ($sLine, 0, 4) != 'SMTP')
				continue ;
			
			if (substr ($sLine, 10, 1) == '-')
				$sLine [10] = ' ';
			
			$sCommand = strstr (substr ($sLine, 7), ' ', true);
			if ($sLine [4] == '>') // outgoing command
			{
				$aCommandSequence [] = array
				(
					'Command'	=> $sCommand,
					'Reply'		=> false
				);
			}
			else // incoming reply
			{
				foreach ($aCommandSequence as & $aCommand)
				{
					if ($aCommand ['Reply'] === false)
					{
						$aCommand ['Reply'] = $sCommand;
						break;
					}
				}
			}
		}
		
		$aStages = array (false, false, false);
		foreach ($aCommandSequence as $aCommandInfo)
		{
			switch ($aCommandInfo ['Command'])
			{
				case 'CONNECT':
				case 'EHLO':
				{
					if ($aCommandInfo ['Reply'] != 220 && $aCommandInfo ['Reply'] != 250)
						return self :: SMTP_ERROR_UNAVAILABLE;
					
					break;
				}
				
				case 'MAIL':
				{
					$aStages [0] = true;
					switch ((int)$aCommandInfo ['Reply'])
					{
						case 552:	return self :: SMTP_ERROR_INSUFFICIENT_STORAGE;
						case 451:	return self :: SMTP_ERROR_LOCAL_ERROR;
						case 452:	return self :: SMTP_ERROR_INSUFFICIENT_SYS_STORAGE;
						case 550:	return self :: SMTP_ERROR_MAILBOX_UNAVAILABLE;
						case 553:	return self :: SMTP_ERROR_MAILBOX_UNAVAILABLE;
					}
					
					break;
				}
				
				case 'RCPT':
				{
					$aStages [1] = true ;
					switch ((int)$aCommandInfo ['Reply'])
					{
						case 550:	return self :: SMTP_ERROR_MAILBOX_UNAVAILABLE;
						case 551:	return self :: SMTP_ERROR_MAILBOX_UNAVAILABLE;
						case 552:	return self :: SMTP_ERROR_INSUFFICIENT_STORAGE;
						case 553:	return self :: SMTP_ERROR_MAILBOX_UNAVAILABLE;
						case 450:	return self :: SMTP_ERROR_MAILBOX_UNAVAILABLE;
						case 451:	return self :: SMTP_ERROR_LOCAL_ERROR;
						case 452:	return self :: SMTP_ERROR_INSUFFICIENT_SYS_STORAGE;
						case 550:	return self :: SMTP_ERROR_MAILBOX_UNAVAILABLE;
					}
					
					break;
				}
				
				case 'writing':
				{
					$aStages [2] = true;
					switch ((int)$aCommandInfo ['Reply'])
					{
						case 552:	return self :: SMTP_ERROR_INSUFFICIENT_STORAGE;
						case 451:	return self :: SMTP_ERROR_LOCAL_ERROR;
						case 452:	return self :: SMTP_ERROR_INSUFFICIENT_SYS_STORAGE;
					}
					
					break;
				}
			} // end switch
		} // end foreach
		
		if ($aStages [0] === false || $aStages [1] === false || $aStages [2] === false)
			return self :: SMTP_ERROR_UNSURE;
		
		return self :: SMTP_SUCCESS;
	}
	
	/**
	 * Function: getMessage
	 * Argument: nCode (integer) - Code that was returned by the send () method
	 *
	 * This function will transform an error code to a decent string explaining what
	 * exactly went wrong, or good if that's the case. Quite easy actually.
	 */ 
	
	public function getMessage ($nCode)
	{
		switch ($nCode)
		{
			case self :: SMTP_SUCCESS:				return 'Success';
			case self :: SMTP_ERROR_UNSURE:				return 'Success (odd SMTP-behaviour)';
			case self :: SMTP_ERROR_UNAVAILABLE:			return 'SMTP Server Unavailable';
			case self :: SMTP_ERROR_MAILBOX_UNAVAILABLE:		return 'Mailbox Unavailable';
			case self :: SMTP_ERROR_INSUFFICIENT_STORAGE:		return 'Mailbox full (insufficient storage)';
			case self :: SMTP_ERROR_INSUFFICIENT_SYS_STORAGE:	return 'Mailbox full (insufficient system storage)';
			case self :: SMTP_ERROR_LOCAL_ERROR:			return 'Remote SMTP processing error';
		}
		
		return 'Unknown';
	}
	
}

?>