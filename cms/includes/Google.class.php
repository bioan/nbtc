<?php   

/**
     @description: news class
     @link: http://www.inforitus.nl
     @author Laurentiu Ghiur <laurentiu@inforitus.nl>
**/


class Google
{ 
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * @desc: get the last errors
        *
        * @returns array
        */
         
        public function getLastError ()
        {
                return $this -> aError;
        }  
         
        /**
        * @desc create the slug of an item
        * 
        * @param string $sParams - string to be converted
        * 
        * @returns string
        */
             
        public function urlize ($sParams)
        {
                $sTitle   = strtolower ($sParams);
                $aAllowed = preg_split ('//', 'abcdefghijklmnopqrstuvwxyz0123456789-+_');

                for ($i = 0, $j = strlen ($sTitle); $i < $j; $i++)
                {
                        if (!in_array ($sTitle  [$i], $aAllowed))
                        {
                                $sTitle  [$i] = '-';
                        }
                }

                for ($i = 0; $i < 3; $i++)
                {
                        $sTitle = str_replace ('--', '-', $sTitle);
                }
                
                return preg_replace ('/^ [\-]*(.+?) [\-]*$/s', '\\1', $sTitle);
        } 
        
        /**
        * @desc check App fields and insert or update database
        * 
        * @param array $aValues - array of values containing app id and app secret
        * 
        * @returns boolean
        */
             
        public function checkApplication ($aValues = array ())
        {
                if (empty ($aValues ['app_name']))
                {
                        $this -> aError ['app_name'] = "Application's name is required";
                }
                
                if (empty ($aValues ['client_id']))
                {
                        $this -> aError ['client_id'] = "Client's id is required";
                }
                
                if (empty ($aValues ['client_secret']))
                {
                        $this -> aError ['client_secret'] = "Client's secret is required";
                }
                
                if (empty ($aValues ['redirect_uri']))
                {
                        $this -> aError ['redirect_uri'] = "Redirect url is required";
                }
                
                if (count ($this -> aError) == 0)
                {
                        $googleAppData = $this ->getApplication();
                        
                        if (!$googleAppData)
                        {
                                $this ->addApplication($aValues);
                        }
                        else 
                        {
                                $this ->updateApplication($aValues);
                        }
                        return true;
                }
                else
                {
                        return false;
                }
        } 
        
        
        /**
        * @desc get the google Application
        * 
        * 
        * @returns array, data of application
        */
             
        public function getApplication ()
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            google_app
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;          
                
		        return $aResults;
        } 
        
        
        
        /**
	    * @desc add google application
	    *
        * @param array $aValues - the values that have to be set
        * 
	    * @return boolean
	    */
	    
	    public function addApplication ($aValues = array ())
	    {	
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT
                        INTO
                            google_app
                                (
                                app_name,
                                client_id,
                                client_secret,
                                redirect_uri,
                                developer_key,
                                date                
                                )      
                        VALUES
                                (
                                ?,
                                ?,
                                ?,
                                ?,
                                ?,
                                NOW()               
                                ) 
                ");

                $pStatement -> bind_param ('sssss',
                        $aValues ['app_name'],
                        $aValues ['client_id'],
                        $aValues ['client_secret'],
                        $aValues ['redirect_uri'],
                        $aValues ['developer_key']				
                );

                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   

                        return false;
                }

                return true;
        }
        
        /**
	    * @desc update Application
	    *
        * @param array $aValues - the values that have to be set
        *  
	    * @return boolean
	    */
        
	    public function updateApplication ($aValues = array ())
	    {
            
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            google_app
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;
            
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            google_app
                        SET
                            app_name       = ?,
                            client_id      = ?,
                            client_secret  = ?,
                            redirect_uri   = ?,
                            developer_key  = ?,
                            date           = NOW()
                        WHERE
                            id         = " . $aResults['id'] ."
                ");

                $pStatement -> bind_param ('sssss',
                        $aValues ['app_name'],
                        $aValues ['client_id'],
                        $aValues ['client_secret'],
                        $aValues ['redirect_uri'],
                        $aValues ['developer_key']  
                );

                if (!$pStatement -> execute ()) 
                {	
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    

                    return false;
                } 

                return true;
        }

         /**
        * @desc get Google user if exists
        * 
        * @param array parameters 
        * @returns array
        */
        
        public function getGoogleUser ($userID)
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
                            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            google_users
                        WHERE
                            user_id = " . $userID . "
                        
                "); 
                
                if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;          
                
                return $aResults;
        }

        public function checkGoogleUser ($aValues = array ())
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
                            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            google_users
                        WHERE
                            user_id = " . $aValues ['user_id'] . "
                        
                "); 
                
                if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                
                if ($pResult -> num_rows > 0)
                {         
                        $this -> updateGoogleUser ($aValues);
                }
                else
                {
                        $this ->addGoogleUser($aValues);
                }

                return true;
        } 

        /**
        * @desc add google users
        *
        * @param array $aValues - the values that have to be set
        * 
        * @return boolean
        */
        
        public function addGoogleUser ($aValues = array ())
        {   
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT
                        INTO
                            google_users
                                (
                                user_id,
                                access_token,
                                date                
                                )      
                        VALUES
                                (
                                ?,
                                ?,
                                NOW()               
                                ) 
                ");

                $pStatement -> bind_param ('ss',
                        $aValues ['user_id'],
                        $aValues ['access_token']
                );

                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   

                        return false;
                }

                return true;
        }

        /**
        * @desc update facebook user
        *
        * @param array $aValues - the values that have to be set
        *  
        * @return boolean
        */
        
        public function updateGoogleUser ($aValues = array ())
        {            
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            google_users
                        SET
                            user_id             = ?,
                            access_token        = ?
                            date               = NOW()
                        WHERE
                            userID         = " . $aValues['user_id'] ."
                ");

                $pStatement -> bind_param ('ss',    
                        $aValues ['user_id'],       
                        $aValues ['access_token']
                );

                if (!$pStatement -> execute ()) 
                {   
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    

                    return false;
                } 

                return true;
            }
}   
?>
