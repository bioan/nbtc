<?php   

/**
     @description: news class
     @link: http://www..inforitus.nl
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class News   
{ 
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * @desc contains all languages 
        * 
        * @var array
        */
        
        var $aLanguages = array ();
        
        /**
        * @desc contains default language 
        * 
        * @var array
        */
        
        var $aDefaultLang = array ();
        
        /**
        *@desc: contructor 
        *
        */
                      
        public function News () 
        { 
                $this -> aLanguages   = Language :: getLanguages ();
                $this -> aDefaultLang = Language :: getLanguages (0, '', 'id', 'asc', true);   
        }
        
        /**
        * @desc: get the last errors
        *
        * @returns array
        */
         
        public function getLastError ()
        {
                return $this -> aError;
        }  
         
        /**
        * @desc create the slug of an item
        * 
        * @param string $sParams - string to be converted
        * 
        * @returns string
        */
             
        public function urlize ($sParams)
        {
                $sTitle   = strtolower ($sParams);
                $aAllowed = preg_split ('//', 'abcdefghijklmnopqrstuvwxyz0123456789-+_');

                for ($i = 0, $j = strlen ($sTitle); $i < $j; $i++)
                {
                        if (!in_array ($sTitle  [$i], $aAllowed))
                        {
                                $sTitle  [$i] = '-';
                        }
                }

                for ($i = 0; $i < 3; $i++)
                {
                        $sTitle = str_replace ('--', '-', $sTitle);
                }
                
                return preg_replace ('/^ [\-]*(.+?) [\-]*$/s', '\\1', $sTitle);
        }   
        
        /**
        * @desc create news rss
        * 
        * @returns bool
        */
        
        public function create_rss () 
        {
                $aItems = $this -> getNews (false);
                
                $sRSS = '<?xml version="1.0" encoding="UTF-8"?>
                        <rss version="2.0">
                            <channel>
                                <title>' . Configuration::WEBSITE_NAME . '</title>
                                <description>' . Configuration::WEBSITE_NAME . ' nieuws</description>
                                <link>http://' . $_SERVER ['SERVER_NAME'] . '/nieuws.xml</link>
                                <docs>http://blogs.law.harvard.edu/tech/rss</docs>
                                <generator>' . Configuration::WEBSITE_NAME . '</generator>';
                setlocale (LC_ALL, 'en_EN');
                
                foreach ($aItems as $item)
                {
                        $iTime = strtotime ($item ['date_register']);
                        $sDate = strftime ("%a, %e %b %Y %H:%M:%S %z", $iTime);
                        
                        $sRSS .= '
                                <item>
                                    <title>' . $item ['title'] . '</title>
                                    <description>' . substr (strip_tags ($item ['description']), 0, 100) . '</description>
                                    <link>http://' . $_SERVER ['SERVER_NAME'] . '/nieuws/' . $item ['slug'] . '.html</link>
                                    <pubDate>' . $sDate . '</pubDate>
                                </item>';
                }
                
                $sRSS .= '</channel>
                    </rss>';
                 
                file_put_contents ('../../nieuws.xml', $sRSS);
                
        }  
         
        /**
        * @desc: get one or all the newsitems 
        *
        * @param bool $bAdmin - if true means that the cms is requesting the news else somebody from site
        * @param int $iItemId - id of the item
        * @param string $ItemSlug - slug of the item
        * @param int $iNewsGroupId - id of group of news
        * @param string $sNewsGroupSlug - slug of group
        * @param string $sSortField - sort the result by this field
        * @param string $sSortType - desc/asc
        * @param string $sLanguage - language
        * @param int $iCount - the number of news 
        *  
        * @return array OR booleaan by error
        */
                                           
        public function getNews ($bAdmin = true, $iItemId = 0,  $sItemSlug = '', $iNewsGroupId = 0, $sNewsGroupSlug = '', $sSortField = 'order_number', $sSortType = 'desc', $sLanguage = 'default', $iCount = 0, $startDate = '', $twitter = false, $facebook = false, $linkedin = false, $photos = false)
        {        
                if (!is_numeric ($iItemId) || !is_numeric ($iNewsGroupId))
                {          
                        return false;
                }
                
                $sTodayDate = date ('Y-m-d'); 
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            n.*,
                            nd.title,
                            nd.description,
                            nd.photos,
                            nd.facebook,
                            nd.twitter,
                            nd.linkedin,
                            nd.publish_id,
                            nd.publish_accessToken,
                            nd.twitter_text,
                            nd.page_description,
                            nd.slug,
                            DATE_FORMAT(n.date_start, '%d-%m-%Y') AS date_start_format,
                            DATE_FORMAT(n.date_end, '%d-%m-%Y') AS date_end_format,
                            DATE_FORMAT(n.date_register, '%d-%m-%Y om %H:%i') AS date_register_format,
                            g.id AS newsgroup_id,
                            g.blog,
                            gd.group_name,
                            (SELECT COUNT(*) FROM news_data WHERE news_id = n.id AND title != '' AND description != '') as filled
                        FROM 
                            news n
                        LEFT JOIN
                            news_data nd
                                ON
                            n.id = nd.news_id
                        LEFT JOIN
                            groups g
                                ON
                            n.group_id = g.id
                        LEFT JOIN
                            groups_data gd
                                ON
                            gd.group_id = n.group_id
                        LEFT JOIN
                            languages l
                                ON
                            (nd.lang_id = l.id AND gd.lang_id = l.id)
                        WHERE 
                            1
                            " . ($twitter   ? sprintf ("AND nd.twitter !=0") : "") . "
                            " . ($facebook  ? sprintf ("AND nd.facebook !=0") : "") . "
                            " . ($linkedin  ? sprintf ("AND nd.linkedin !=0") : "") . "
                            " . ($photos    ? sprintf ("AND nd.photos != ''") : "") . "
                            " . ($startDate != ''         ? sprintf ("AND n.date_start =" . "'" . $startDate . "'") : "") . "
                            " . ($sLanguage == 'default'  ? sprintf ("AND l.default = 1") : sprintf ("AND l.short_name = '%s'", Database :: getInstance () -> real_escape_string ($sLanguage))) . "
                            " . ($bAdmin == false         ? sprintf ("AND n.date_start <= '" . $sTodayDate . "' AND (n.date_end >= '" . $sTodayDate . "' OR n.date_end = '0000-00-00')") : '') . "
                            " . (!empty ($iItemId)        ? sprintf ("AND n.id = %d ", $iItemId) : '') . "
                            " . (!empty ($sItemSlug)      ? sprintf ("AND nd.slug = '%s' ", Database :: getInstance () -> real_escape_string ($sItemSlug)) : '') . "  
                            " . (!empty ($iNewsGroupId)   ? sprintf ("AND n.group_id = %d ", $iNewsGroupId) : '') . "
                            " . (!empty ($sNewsGroupSlug) ? sprintf ("AND gd.slug = '%s' ", Database :: getInstance () -> real_escape_string ($sNewsGroupSlug)) : '') . "
                                                        
                        ORDER BY 
                            " . $sSortField . " " . $sSortType . " " . ($iCount != 0 ? sprintf ("LIMIT 0, %d ", $iCount) : '') . "    
                    ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                $aRow ['photos'] = $this -> getPhotosByOids ($aRow ['photos']);
                                $aRow ['description'] = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                                
                                if (preg_match_all ('%<a[^>]+>%s', $aRow ['description'], $aMatch))
                                {
                                        $aLinks = $aMatch [0];
                                        
                                        foreach ($aLinks as $link)
                                        {
                                                if (strpos ($link, 'onclick="') === false && 
                                                strpos ($link, 'target="_blank"') !== false && 
                                                preg_match ('%href="[^"]+"%s', $link, $matches))
                                                {
                                                        $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                        $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['title'] . '\', \'' . $sAddress . '\')"', $link);
                                                        $aRow ['description'] = str_replace ($link, $sNewlink, $aRow ['description']); 
                                                }
                                        }
                                }
                                
                                $aResults [] = $aRow;
                        }
                }
                
                foreach ($aResults as $k => $value)
                {
                        if (count ($this -> aLanguages) > 1)
                        {
                                foreach ($this -> aLanguages as $lang)
                                {
                                        $pResult = Database :: getInstance () -> query ("
                                                SELECT 
                                                    nd.title,
                                                    nd.description,
                                                    nd.page_description,
                                                    nd.slug,
                                                    nd.photos,
                                                    nd.facebook,
                                                    nd.twitter,
                                                    nd.linkedin,
                                                    nd.publish_id,
                                                    nd.publish_accessToken,
                                                    nd.twitter_text
                                                FROM 
                                                    news n
                                                LEFT JOIN
                                                    news_data nd
                                                        ON
                                                    n.id = nd.news_id
                                                LEFT JOIN
                                                    groups g
                                                        ON
                                                    n.group_id = g.id
                                                LEFT JOIN
                                                    groups_data gd
                                                        ON
                                                    gd.group_id = n.group_id
                                                LEFT JOIN
                                                    languages l
                                                        ON
                                                    (nd.lang_id = l.id AND gd.lang_id = l.id)
                                                WHERE 
                                                    l.id = " . $lang ['id'] . "
                                                AND
                                                    n.id = " . $value ['id'] . "                                
                                            ");
                                        
                                        if (!$pResult)
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                
                                                return false;
                                        }  
                                          
                                        if ($pResult -> num_rows > 0)
                                        {         
                                                while ($aRow = $pResult -> fetch_assoc ()) 
                                                {       
                                                        if ($aRow ['title'] != '' || $aRow ['description'] != '')
                                                                $aRow ['filled'] = true;
                                                        $aRow ['photos'] = $this -> getPhotosByOids ($aRow ['photos']);
                                                        $aRow ['description'] = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                                                        
                                                        if (preg_match_all ('%<a[^>]+>%s', $aRow ['description'], $aMatch))
                                                        {
                                                                $aLinks = $aMatch [0];
                                                                
                                                                foreach ($aLinks as $link)
                                                                {
                                                                        if (strpos ($link, 'onclick="') === false && 
                                                                        strpos ($link, 'target="_blank"') !== false && 
                                                                        preg_match ('%href="[^"]+"%s', $link, $matches))
                                                                        {
                                                                                $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                                                $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['title'] . '\', \'' . $sAddress . '\')"', $link);
                                                                                $aRow ['description'] = str_replace ($link, $sNewlink, $aRow ['description']); 
                                                                        }
                                                                }
                                                        }
                                                        
                                                        $aResults [$k][$lang ['id']] = $aRow;
                                                }
                                        }  
                                }     
                        }
                        else
                        {
                                unset ($value ['filled']);
                                if ($value ['title'] != '' || $value ['description'] != '')
                                        $value ['filled'] = true;
                                        
                                $aResults [$k][$this -> aDefaultLang ['id']] = $value;
                        }
                }
                
                $aResults = (!empty ($iItemId) || !empty ($sItemSlug)) ? current ($aResults) : $aResults;
            
                return $aResults;
        }
        
        /**
        * @desc: return an array with all data about images
        * 
        * @param string $sImages
        * 
        * @return array
        **/ 
         
        private function getPhotosByOids ($sImages)
        {
                if (empty ($sImages))
                {
                        return false;
                }
                
                $aImages        = explode (',', $sImages);
                $aProductPhotos = array ();
                
                foreach ($aImages as $image)
                {
                        if ($image != '')
                        {
                                $aDetailPhoto = $this -> getPhoto ($image);
                                
                                if (isset ($aDetailPhoto ['id']) && $aDetailPhoto ['id'] != '')
                                { 
                                        array_push ($aProductPhotos, $aDetailPhoto);
                                }
                        }
                }
                
                
                for ($i = 0; $i < count ($aProductPhotos); $i++)
                { 
                  
                        $aPathImages = explode ('/', $aProductPhotos [$i]['image_name'], -1);
                        $sPath       = '';

                        foreach ($aPathImages as $folder)
                        {
                                if ($folder != '')
                                {
                                        $sPath .= '/' . $folder;
                                }
                        }

                        $aProductPhotos [$i]['pathphoto'] = $sPath;
                }
                
                 return $aProductPhotos;
        }
        
        /**
        * @desc: get the data of an image 
        * 
        * @param int $iId
        * 
        * @return array
        */
        
        public function getPhoto ($iId = '')
        {
                if ($iId == '' || !is_numeric ($iId))
                {
                        return array ();
                }
                  
                $pQuery = Database :: getInstance () -> query ("
                        SELECT
                        *
                        FROM
                            images
                        WHERE
                            id = " . $iId . "
                        ORDER BY
                            id DESC
                        "
                );
                  
                if ($pQuery -> num_rows > 0)
                {
                        while ($aRow = $pQuery -> fetch_assoc ())
                        {
                                return $aRow;
                        }
                }
                
                return array ();
        }
        
        /**
        * @desc: check the fields
        *
        * @param array $aValues - fields that have to be checked
        * 
        * @returns booleaan
        */
                            
        private function checkFields ($aValues = array ())
        {
                if (empty ($aValues ['title'][$this -> aDefaultLang ['id']])) 
                {
                        $this -> aError [] = "Titel";
                }
                
                if (!empty ($aValues ['date_end'])) 
                {
                        $aDate = explode ('-', $aValues ['date_end']);
                    
                        if (count ($aDate) == 3)
                        {
                                foreach ($aDate as $number)
                                {
                                        if (is_numeric ($number))
                                        {
                                                if ($number != (int) $number)
                                                {
                                                        $this -> aError [] = "Datum tot";
                                                }
                                        }
                                        else
                                        {
                                                $this -> aError [] = "Datum tot";
                                        }
                                }
                        }
                        else
                        {
                                $this -> aError [] = "Datum tot";
                        }
                }
                 
                if (empty ($aValues ['description'][$this -> aDefaultLang ['id']])) 
                {
                        $this -> aError [] = "Bericht";
                }
                 
                if (empty ($aValues ['group_id'])) 
                {
                        $this -> aError [] = "Nieuwsgroep";
                }
                
                if ($this -> aError) 
                {          
                        return false;
                }
                
                return true;
        }


        /**
        * @desc set nl date to ISO date
        *
        * @param string $sDate
        * 
        * @returns string 
        */
        
        private function nlToIsoDate ($sDate)
        {
                preg_match ('#([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})+#' ,$sDate, $aMatches);
                $sNewDate = $aMatches [3] . '-' . $aMatches [2] . '-' . $aMatches [1];
                
                return $sNewDate;
        }
        
        /**
        * @desc set ISO to nl data
        *
        * @param string $sDate
        * 
        * @returns string 
        **/
        
        public function isoToNlDate ($sDate)
        {
                preg_match ('#([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})+#' ,$sDate, $aMatches);
                $sNewDate = $aMatches [3] . '-' . $aMatches [2] . '-' . $aMatches [1];
                
                return $sNewDate;
        }
        
        /**
        * @desc: insert the news item in the database
        *
        * @param array $aValues - the data of the item
        * 
        * @returns booleaan
        */
                                      
        public function addNews ($aValues = array ())
        {
                if ($this -> checkFields ($aValues)) 
                {
                        // take the item with the biggest sort number
                        
                        $aNewsItems = $this -> getNews ();
                        $aNewsItem  = (isset ($aNewsItems [0]) ? $aNewsItems [0] : '');
                        
                        // add post item
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT INTO
                                    news
                                    (
                                        group_id,
                                        date_start,
                                        date_end,
                                        date_register,
                                        link,
                                        userId,
                                        order_number
                                    )
                                VALUES
                                    (
                                        ?,
                                        ?,
                                        ?,
                                        NOW(),
                                        ?,
                                        ?,
                                        ?
                                    )
                        ");
                        
                        $sDateStart       = $this -> nlToIsoDate ($aValues ['date_start']);
                        $sDateEnd         = $this -> nlToIsoDate (!empty ($aValues ['date_end']) ? $aValues ['date_end'] : '00-00-0000');
                        $iOrderNumber     = (isset ($aNewsItem ['order_number']) ? $aNewsItem ['order_number'] + 1 : 0);
                        
                        $sLink     = (isset ($aValues ['select_link']) && $aValues ['select_link'] != '' ?  $aValues ['select_link'] : $aValues ['link']);
                        
                        $pStatement -> bind_param ('isssii',
                                $aValues ['group_id'],
                                $sDateStart,
                                $sDateEnd,
                                $sLink,
                                $aValues ['userId'],
                                $iOrderNumber
                        );
                        
                        if (!$pStatement -> execute ()) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        $iNewsId = Database :: getInstance () -> insert_id;
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                        
                                $pStatement = Database :: getInstance () -> prepare ("
                                        INSERT INTO
                                            news_data
                                            (
                                                news_id,
                                                lang_id,
                                                title,
                                                description,
                                                photos,
                                                facebook,
                                                twitter,
                                                linkedin,
                                                publish_id,
                                                publish_accessToken,
                                                twitter_text,
                                                slug,
                                                page_description
                                            )
                                        VALUES
                                            (
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?
                                            )
                                ");
                                
                                $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]);
                                $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                $iFacebook = (isset ($aValues ['postFacebook'])) ? $_SESSION ['user_id'] : 0;
                                $iTwitter = (isset ($aValues ['postTwitter'])) ? $_SESSION ['user_id'] : 0;
                                $iLinkedin = (isset ($aValues ['postLinkedin'])) ? $_SESSION ['user_id'] : 0;
                                $sPublishId = (isset ($aValues ['publish_id'])) ? $aValues ['publish_id'] : '';
                                $sPublishAccessToken = (isset ($aValues ['publish_accessToken'])) ? $aValues ['publish_accessToken'] : '';
                                $sTwitterText = (isset ($aValues ['twitter_text'])) ? $aValues ['twitter_text'] : '';
                                                                
                                $pStatement -> bind_param ('iisssiiisssss',
                                        $iNewsId,
                                        $lang ['id'],
                                        $aValues ['title'][$lang ['id']],
                                        $aValues ['description'][$lang ['id']],
                                        $sPhotos,
                                        $iFacebook,
                                        $iTwitter,
                                        $iLinkedin,
                                        $sPublishId,
                                        $sPublishAccessToken,
                                        $sTwitterText,
                                        $sSlug,
                                        $aValues ['page_description'][$lang ['id']]
                                );
                                
                                if (!$pStatement -> execute ()) 
                                {
                                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                        
                                        return false;
                                }
                        }
                        
                        $this -> create_rss ();
                        
                        return true;
                }
                
                return false;
        }

        /**
        * @desc: update a news item
        *
        * @param int $iId - id of the item
        * @param array $aValues - the data of the item
        * 
        * @return booleaan
        */
                                 
        public function updateNews ($iId, $aValues = array ())
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                
                if ($this -> checkFields ($aValues)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    news
                                SET
                                    group_id         = ?,
                                    date_start       = ?,
                                    date_end         = ?,
                                    link             = ?,
                                    userId           = ?
                                WHERE
                                    id               = ? 
                        ");
                        
                        $sDateStart = $this -> nlToIsoDate ($aValues ['date_start']);
                        $sDateEnd   = $this -> nlToIsoDate (!empty ($aValues ['date_end']) ? $aValues ['date_end'] : '00-00-0000');                     
                        
                        $sLink     = (isset ($aValues ['select_link']) && $aValues ['select_link'] != '' ?  $aValues ['select_link'] : $aValues ['link']);
                        //print_r($aValues); die;
                        $pStatement -> bind_param ('isssii',
                                $aValues ['group_id'],
                                $sDateStart,
                                $sDateEnd,
                                $sLink,
                                $aValues ['userId'],
                                $iId
                        );
                        
                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        $aLangValues = $this -> getNews (true, $iId, '', 0, '', 'order_number', 'desc');
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                                if (isset ($aLangValues [$lang ['id']]))
                                {
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                UPDATE
                                                    news_data
                                                SET
                                                    news_id          = ?,
                                                    title            = ?,
                                                    description      = ?,
                                                    photos           = ?,
                                                    facebook         = ?,
                                                    twitter          = ?,
                                                    linkedin         = ?,
                                                    publish_id       = ?,
                                                    publish_accessToken = ?,
                                                    twitter_text     = ?,
                                                    slug             = ?,
                                                    page_description = ?     
                                                WHERE
                                                    news_id          = ? 
                                                AND 
                                                    lang_id          = ?
                                        ");
                                        $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]);
                                        $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                        $iFacebook = (isset ($aValues ['postFacebook'])) ? $_SESSION ['user_id'] : 0;
                                        $iTwitter = (isset ($aValues ['postTwitter'])) ? $_SESSION ['user_id'] : 0;
                                        $iLinkedin = (isset ($aValues ['postLinkedin'])) ? $_SESSION ['user_id'] : 0;
                                        $sPublishId = (isset ($aValues ['publish_id'])) ? $aValues ['publish_id'] : '';
                                        $sPublishAccessToken = (isset ($aValues ['publish_accessToken'])) ? $aValues ['publish_accessToken'] : '';
                                        $sTwitterText = (isset ($aValues ['twitter_text'])) ? $aValues ['twitter_text'] : '';
                                        
                                        $pStatement -> bind_param ('isssiiisssssii',
                                                $iId,
                                                $aValues ['title'][$lang ['id']],
                                                $aValues ['description'][$lang ['id']],
                                                $sPhotos,
                                                $iFacebook,
                                                $iTwitter,
                                                $iLinkedin,
                                                $sPublishId,
                                                $sPublishAccessToken,
                                                $sTwitterText,
                                                $sSlug,  
                                                $aValues ['page_description'][$lang ['id']],
                                                $iId,
                                                $lang ['id']
                                        );
                                
                                        if (!$pStatement -> execute ())
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                
                                                return false;
                                        }
                                }
                                else
                                {
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                INSERT INTO
                                                    news_data
                                                    (
                                                        news_id,
                                                        lang_id,
                                                        title,
                                                        description,
                                                        photos,
                                                        facebook,
                                                        twitter,
                                                        linkedin,
                                                        publish_id,
                                                        publish_accessToken,
                                                        twitter_text,
                                                        slug,
                                                        page_description
                                                    )
                                                VALUES
                                                    (
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?
                                                    )
                                        ");
                                        
                                        $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]);
                                        $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                        $iFacebook = (isset ($aValues ['postFacebook'])) ? $_SESSION ['user_id'] : 0;
                                        $iTwitter = (isset ($aValues ['postTwitter'])) ? $_SESSION ['user_id'] : 0;
                                        $iLinkedin = (isset ($aValues ['postLinkedin'])) ? $_SESSION ['user_id'] : 0;
                                        $sPublishId = (isset ($aValues ['publish_id'])) ? $aValues ['publish_id'] : '';
                                        $sPublishAccessToken = (isset ($aValues ['publish_accessToken'])) ? $aValues ['publish_accessToken'] : '';
                                        $sTwitterText = (isset ($aValues ['twitter_text'])) ? $aValues ['twitter_text'] : '';
                                        
                                        $pStatement -> bind_param ('iisssiiisssss',
                                                $iId,
                                                $lang ['id'],
                                                $aValues ['title'][$lang ['id']],
                                                $aValues ['description'][$lang ['id']],
                                                $sPhotos,
                                                $iFacebook,
                                                $iTwitter,
                                                $iLinkedin,
                                                $sPublishId,
                                                $sPublishAccessToken,
                                                $sTwitterText,
                                                $sSlug,
                                                $aValues ['page_description'][$lang ['id']]
                                        );
                                        
                                        if (!$pStatement -> execute ()) 
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                
                                                return false;
                                        }       
                                }
                        }
                        
                        $this -> create_rss ();
                        
                        return true;
                }
                
                return false;
        }
        
        /**
        * @desc: delete a news item
        *
        * @param int $iId - id of the item
        * 
        * @return booleaan   
        */
                              
        public function deleteNews ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            news
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            news_data
                        WHERE
                            news_id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                $this -> create_rss ();
                
                return true; 
        }
}   
?>
