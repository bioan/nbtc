<?php      
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id: Database.php
 */

/**
 * Database access gets controlled by this class. It's a wrapper around PHP's own
 * MySQLi system which provides fast, efficient access to the server.
 *
 * @package kernel
 */         

class Database extends \ MySQLi
{
        /**
         * The instance for current site.
         * In order to maintain one instance of the Database class we'll be applying
         * the Singleton pattern. Since multiple inheritance is not possible in PHP
         * (which is a good thing) a seperate implementation has been created. This
         * variable will hold the instance for the MySQLi object.
         *
         * @var Database Instance of the active database-class.
         */
        
        private static $m_sInstance;
        
        /**
         * The instance for eppdigitaal site.
         * In order to maintain one instance of the Database class we'll be applying
         * the Singleton pattern. Since multiple inheritance is not possible in PHP
         * (which is a good thing) a seperate implementation has been created. This
         * variable will hold the instance for the MySQLi object.
         *
         * @var Database Instance of the active database-class.
         */
        
        private static $m_sEppInstance;
        
        
        
        /**
         * Here you get the instance for current site.
         * In order to get the active instance of the MySQLi class, this function may
         * be used. By using singleton we force that there's only one active connection
         * at any time, while every part of the code is allowed to get that connection.
         * If no connection is available, a new one will be opened.
         *
         * @return Database The only instance of the database-connection that will be used.
         */
        
        public static function getInstance ()
        {                       
                if (self :: $m_sInstance == null)
                {                      
                        self :: $m_sInstance = @ new self (\ Configuration :: DATABASE_HOSTNAME, \ Configuration :: DATABASE_USERNAME, \ Configuration :: DATABASE_PASSWORD, \ Configuration :: DATABASE_DATABASE);
                        
                        $sSql = "SET NAMES 'utf8'";
                        self :: $m_sInstance -> query ($sSql);
                        
                        self :: $m_sInstance  -> checkConnection ();
                }
                
                return self :: $m_sInstance;
        }
        
        /**
         * Here you get the instance for eppdigitaal site.  
         * In order to get the active instance of the MySQLi class, this function may
         * be used. By using singleton we force that there's only one active connection
         * at any time, while every part of the code is allowed to get that connection.
         * If no connection is available, a new one will be opened.
         *
         * @return Database The only instance of the database-connection that will be used.
         */
        
        public static function getEppInstance ()
        {
                if (self :: $m_sEppInstance == null)
                {
                        self :: $m_sEppInstance = @ new self (\ Configuration :: DATABASE_HOSTNAME, \ Configuration :: EPP_DATABASE_USERNAME, \ Configuration :: EPP_DATABASE_PASSWORD, \ Configuration :: EPP_DATABASE_DATABASE);
                        self :: $m_sEppInstance  -> checkConnection ();
                }
                
                return self :: $m_sEppInstance;
        }
        
        /**
         * The checkConnection method will check whether we are able to properly initialise
         * a connection with the database. If that is not the case, proper error handling
         * should be included to make sure visitors don't get to see any errors.
         */
        
        private function checkConnection ()
        {
                if ($this -> connect_errno == 0)
                        return ;
                
                KernelError :: ThrowError ('Could not establish a connection with the database (%d): %s', $this -> connect_errno, $this -> connect_error);
        }
        
};

?>