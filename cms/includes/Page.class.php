<?php             

/**
    @description: page class 
    @link: http://www.inforitus.nl
    @author Andrei Dragos <andrei@inforitus.nl>
**/   

class Page extends Tree
{
        /**
        * @desc the tree id specific to pages
        * 
        * @var integer
        */
        
        var $iTreeId = 2;
	    
        /**
        * @desc: contructor 
        *
        */
        
	    public function Page ()
	    {
		        parent::Tree ($this -> iTreeId);
                
                $this -> InitData ();
	    }
        
        /**
        * @desc add a page in the database
        * 
        * @param array $aData - the data of the page
        * @param int $iTreeId - the tree id that must be before/after/end
        * @param string $sAction - the action that will be applied before/after/end
        * 
        * @return boolean
        */
        
        public function add ($aData, $iTreeId, $sAction = 'add')
        {                         
    	        if (empty ($aData) || !is_array ($aData) || !is_numeric ($iTreeId))
                {
    		            return false;
                }
                
		        if ($this -> insert ($aData, $iTreeId, $sAction)) 
                {       
			            return true;
		        }
                
		        return false;   
	    }
        
        /**
        * @desc update a page
        * 
        * @param int $iId - id of page
        * @param array $aData - the data with must be updated
        * 
        * @return boolean
        */
	    
	    public function updateNodeData ($iId, $aData)
	    {                      
		        if (empty ($iId) || !is_numeric ($iId))
                {
			            return false;
                }
                    
		        if ($this -> changeNode ($iId, $aData)) 
                {      
			            return true;
		        }
                 
		        return false;
	    }
}
?>
