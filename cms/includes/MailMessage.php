<?php
/**
 * Inforitus Server Control Panel v1.1
 *
 * This is the second revision of the Inforitus Server Control panel. In order to
 * keep performance at a maximum, without loosing flexibility or cleanness of the
 * code (which was, in all fairness, becoming a mess), a large part of the kernel
 * has been written to meet higher standards.
 *
 * This second revision supports unit tests, has a better architecture and is
 * better optimalized for front-end websites. Lots of components have been moved
 * to their own file rather than being included in the Kernel or one of the
 * Managers, making the code a lot easier to maintain.
 *
 * Author: Andrei Dragos
 *         andrei@inforitus.nl
 *
 */

class MailMessage
{
	/**
	 * Constants: RECEIVER_TYPE_*
	 *
	 * These constants define the types of receivers these mails support,
	 * being main receivers and the two kinds of carbon copies.
	 */
	
	const	RECIPIENT_TYPE_TO 		= 0; // main receiver(s)
	const	RECIPIENT_TYPE_CC 		= 1; // carbon copy
	const	RECIPIENT_TYPE_BCC 		= 2; // blind carbon copy
	
	/**
	 * Constants: PRIORITY_*
	 *
	 * These indicate the different priorities an e-mail can have, allowing
	 * yet another level of flexibility in composing the messages.
	 */
	
	const	PRIORITY_LOW			= 0; // low priority
	const	PRIORITY_NORMAL			= 1; // normal priority
	const	PRIORITY_HIGH			= 2; // high priority
	
	/**
	 * Constants: ATTACHMENT_TYPE_*
	 *
	 * The type of attachment that will be added to the e-mail, which can be
	 * any one of the following defines. Explainations are included as well.
	 */
	
	const	ATTACHMENT_TYPE_FILE		= 0; // a filename
	const	ATTACHMENT_TYPE_STREAM		= 1; // a stream (fopen etc)
	const	ATTACHMENT_TYPE_DATA		= 2; // a string with data
	
	const	IMAGE_TYPE_FILE			= 0; // image by its filename
	const	IMAGE_TYPE_GD			= 1; // image by the gd object
	const	IMAGE_TYPE_DATA			= 2; // image by plain data
	
	/**
	 * Constants: CONTENT_TYPE_*
	 *
	 * These two constants define the types of e-mail bodies that can be sent
	 * using this class. This should be the second argument of setBody.
	 */
	
	const	CONTENT_TYPE_PLAIN		= 0; // plain text e-mail
	const	CONTENT_TYPE_HTML		= 1; // html e-mail
	
	/**
	 * Property: m_aHeaders
	 *
	 * This array contains an overview of the headers which will be associated with
	 * this very e-mail. They can't be controlled directly by the user.
	 */
	
	private $m_aHeaders = array 
	(
		'MIME-Version'		=> '2.0',
		'X-Mailer'		=> 'Inforitus'
	);
	
	/**
	 * Property: m_aRecipients
	 *
	 * An array of the people who will be receiving this e-mail, devided in those in
	 * the To: header, the cc: header and the bcc: header.
	 */
	
	private $m_aRecipients = array 
	(
		/* RECEIVER_TYPE_TO: */ 	array (), 
		/* RECEIVER_TYPE_CC: */ 	array (), 
		/* RECEIVER_TYPE_BCC: */	array ()
	);
	
	/**
	 * Property: m_sHostname
	 *
	 * This property defines the hostname that will be used for sending this e-mail,
	 * which will be determained using the sender of the e-mail address.
	 */
	
	private $m_sHostname;
	
	/**
	 * Property: m_aAttachments
	 *
	 * An array containing all the attachments that should be included with this
	 * e-mail message, indicated by a mime-type, length, contents, etcetera.
	 */
	
	private $m_aAttachments = array ();
	
	/**
	 * Property: m_aBody
	 *
	 * The array which defines the contents of this e-mail, every entry being
	 * an alternative (text/html/etcetera). If there's only one then that will
	 * be the one that'll be used and all.
	 */
	
	private $m_aBody = array ();
	
	/**
	 * Property: m_sCharset
	 *
	 * The character set to apply to the e-mail bodies, which will happen for
	 * both HTML as plaintext e-mail messages. Defaults to some iso standard.
	 */
	
	private $m_sCharset = 'utf-8';
	
	/**
	 * Function: getSender
	 *
	 * Of course we want to be able to retrieve the sender of this e-mail message,
	 * regardless whether that's us or anyone else in this world.
	 */
	
	public function getSender ()
	{
		if (!isset ($this -> m_aHeaders ['From']))
			return false ;
		
		return $this -> m_aHeaders ['From'];
	}
	
	/**
	 * Function: setSender
	 * Argument: sName (string) - Name of the sender of this e-mail.
	 * Argument: sEmail (string) - Email address of the person sending this e-mail.
	 *
	 * This function will set the sender of this e-mail by changing the From:, Return-
	 * Path: and Reply-To: headers. These can be changed manually however.
	 */

	public function setSender ($sName, $sEmail = null)
	{
		if ($sEmail === null)
			$sEmail = $sName;
		
		if (filter_var ($sEmail, FILTER_VALIDATE_EMAIL) === false)
			throw new InvalidArgumentException ('Expected an e-mail address, got "' . $sEmail . '"');
		
		$sName = '"' . filter_var ($sName, FILTER_SANITIZE_SPECIAL_CHARS) . '"';
		$sFrom = $sName . ' <' . $sEmail . '>';
		
		$this -> m_sHostname = substr (strrchr ($sEmail, '@'), 1);
		
		$this -> m_aHeaders ['From'] = $sFrom;
		$this -> m_aHeaders ['Return-Path'] = $sFrom;
		$this -> m_aHeaders ['Reply-To'] = $sFrom;
	}
	
	/**
	 * Function: getReplyTo
	 *
	 * A simple function to get the reply address the e-mail should be replied
	 * to, not all that interesting yet available information so meh.
	 */
	
	public function getReplyTo ()
	{
		if (!isset ($this -> m_aHeaders ['Reply-To']))
			return false ;
		
		return $this -> m_aHeaders ['Reply-To'];
	}
	
	/**
	 * Function: setReplyTo
	 * Argument: sName (string) - Name of the person to receive the message's replies.
	 * Argument: sEmail (string) - E-mail address that should be receiving the reply.
	 *
	 * This function may be used to set the person that replies to the e-mail should be
	 * delivered at. It gets checked in the same way as the setSender function.
	 */
	
	public function setReplyTo ($sName, $sEmail = null)
	{
		if ($sEmail === null)
			$sEmail = $sName;
		
		if (filter_var ($sEmail, FILTER_VALIDATE_EMAIL) === false)
			throw new InvalidArgumentException ('Expected an e-mail address, got "' . $sEmail . '"');
		
		$sName = filter_var ($sName, FILTER_SANITIZE_SPECIAL_CHARS);
		$sFrom = $sName . ' <' . $sEmail . '>';
		
		$this -> m_aHeaders ['Return-Path'] = $sFrom;
		$this -> m_aHeaders ['Reply-To'] = $sFrom;
	}
	
	/**
	 * Function: getRecipient
	 *
	 * This function may be used to get an array of the possible recipients of this e-mail,
	 * in a fairly easy and usable way.
	 */
	
	public function getRecipient ()
	{
		return $this -> m_aRecipients [self :: RECIPIENT_TYPE_TO];
	}
	
	/**
	 * Function: setRecipient
	 * Argument: mEmail (array) - Either an array or a string with the recipient(s)
	 * Argument: nType (string) - Type of recipient that should be set using this function.
	 * Argument: bReplace (boolean) - Replace the current recipients with those in this call.
	 *
	 * This function may be used to set the recipients of this e-mail. Multiple recipients
	 * may be set by passing an array of addresses to this function.
	 */
	
	public function setRecipient ($mEmail, $nType = self :: RECIPIENT_TYPE_TO, $bReplace = true)
	{
		if ($bReplace === true)
		{
			$this -> m_aRecipients [$nType] = array ();
		}
		
		if (is_array ($mEmail))
		{
			foreach ($mEmail as $sEmail)
			{
				if (filter_var ($sEmail, FILTER_VALIDATE_EMAIL) === false)
					throw new InvalidArgumentException ('Expected an e-mail address, got "' . $sEmail . '"');
			
				$this -> m_aRecipients [$nType] [] = $sEmail;
			}
		}
		else
		{
			if (filter_var ($mEmail, FILTER_VALIDATE_EMAIL) === false)
				throw new InvalidArgumentException ('Expected an e-mail address, got "' . $mEmail . '"');
			
			$this -> m_aRecipients [$nType] [] = $mEmail;
		}
	}
	
	/**
	 * Function: addRecipient
	 * Argument: mEmail (array) - Either an array or a string with the recipient(s)
	 *
	 * A simple function which will add a new recipient (or recipients) to those who should
	 * receive this e-mail, in the To: header. Can be an array or a string value.
	 */
	
	public function addRecipient ($mEmail)
	{
		$this -> setRecipient ($mEmail, self :: RECIPIENT_TYPE_TO, false);
	}
	
	/**
	 * Function: clearRecipients
	 * Argument: nType (integer) - The type of recipients that should be cleared.
	 * 
	 * This function may be used to clear the list of recipients who will receive the
	 * e-mail. The optional argument defines the type of recipient list to clear.
	 */
	
	public function clearRecipient ($nType = self :: RECIPIENT_TYPE_TO)
	{
		$this -> m_aRecipients [$nType] = array ();
	}
	
	/**
	 * Function: getCC
	 *
	 * Obviously sending an e-mail to an alternative list of people using the carbon
	 * copy system is a possibility as well, so we want to fetch the data.
	 */
	
	public function getCC ()
	{
		return $this -> m_aRecipients [self :: RECIPIENT_TYPE_CC];
	}
	
	/**
	 * Function: setCC
	 * Argument: mEmail (array) - Either an array or a string with the cc recipient(s)
	 *
	 * This function may be used to set the people who will receive a carbon copy of
	 * this e-mail message, thus will reply in everyone's client.
	 */
	
	public function setCC ($mEmail)
	{
		$this -> setRecipient ($mEmail, self :: RECIPIENT_TYPE_CC, true);
	}
	
	/**
	 * Function: addCC
	 * Argument: mEmail (array) - Either an array or a string with the cc recipient(s)
	 * 
	 * A simple function to add a carbon copy person to the receiving list of this
	 * e-mail. Basically it just invokes the setRecipient function from earlier on.
	 */
	
	public function addCC ($mEmail)
	{
		$this -> setRecipient ($mEmail, self :: RECIPIENT_TYPE_CC, false);
	}
	
	/**
	 * Function: clearCC
	 *
	 * This function clears the list of people within the carbon copy list, with the
	 * purpose to allow adding new people. setCC would be adviced however.
	 */
	
	public function clearCC ()
	{
		$this -> clearRecipient (self :: RECIPIENT_TYPE_CC);
	}
	
	/**
	 * Function: setBCC
	 * Argument: mEmail (array) - Either an array or a string with the bcc recipient(s)
	 *
	 * This function may be used to set the people who will receive a carbon copy of
	 * this e-mail message, thus will reply in everyone's client.
	 */
	
	public function setBCC ($mEmail)
	{
		$this -> setRecipient ($mEmail, self :: RECIPIENT_TYPE_BCC, true);
	}
	
	/**
	 * Function: addBCC
	 * Argument: mEmail (array) - Either an array or a string with the bcc recipient(s)
	 * 
	 * A simple function to add a carbon copy person to the receiving list of this
	 * e-mail. Basically it just invokes the setRecipient function from earlier on.
	 */
	
	public function addBCC ($mEmail)
	{
		$this -> setRecipient ($mEmail, self :: RECIPIENT_TYPE_BCC, false);
	}
	
	/**
	 * Function: clearBCC
	 *
	 * This function clears the list of people within the carbon copy list, with the
	 * purpose to allow adding new people. setCC would be adviced however.
	 */
	
	public function clearBCC ()
	{
		$this -> clearRecipient (self :: RECIPIENT_TYPE_BCC);
	}
	
	/**
	 * Function: getMimeVersion
	 *
	 * This function simply returns the mime-version of this message if it's available,
	 * allowing us to identify, yes, the mime version.
	 */
	
	public function getMimeVersion ()
	{
		if (!isset ($this -> m_aHeaders ['MIME-Version']))
			return false ;
		
		return $this -> m_aHeaders ['MIME-Version'];
	}
	
	/**
	 * Function: setMimeVersion
	 * Argument: nMimeVersion (integer) - The mime version to use for this e-mail.
	 *
	 * This function may be used to adjust the mime version that will be used for this
	 * e-mail. This should either be one, two or three.
	 */
	
	public function setMimeVersion ($nMimeVersion)
	{
		if (!is_integer ($nMimeVersion) || $nMimeVersion < 1 || $nMimeVersion > 3)
			throw new OutOfRangeException ('Expected a MIME-Version between 1 and 3, got "' . $nMimeVersion . '"');
		
		$this -> m_aHeaders ['MIME-Version'] = $nMimeVersion . '.0';
	}
	
	/**
	 * Function: getXMailer
	 *
	 * This function returns the X-Mailer of the message if it is available, so we
	 * can figure out the daemon which has been used for sending the message.
	 */
	
	public function getXMailer ()
	{
		if (!isset ($this -> m_aHeaders ['X-Mailer']))
			return false ;
		
		return $this -> m_aHeaders ['X-Mailer'];
	}
	
	/**
	 * Function: setXMailer
	 * Argument: sXMailer (string) - The X-Mailer string to apply to this e-mail.
	 *
	 * A simple function that may be used to set the X-Mailer associated with the
	 * e-mail. This indicates the program/website responsible for sending the mail.
	 */
	
	public function setXMailer ($sXMailer)
	{
		if (!is_string ($sXMailer) || !isset ($sXMailer [1]) || strlen ($sXMailer) > 16)
			throw new LengthException ('The X-Mailer must be a string between one and 16 characters.');
		
		$this -> m_aHeaders ['X-Mailer'] = $sXMailer;
	}
	
	/**
	 * Function: getSubject
	 *
	 * This function will attempt to return the subject of the e-mail message
	 * we're currently containing, for whatever purpose required.
	 */
	
	public function getSubject ()
	{
		if (!isset ($this -> m_aHeaders ['Subject']))
			return false ;
		
		return $this -> m_aHeaders ['Subject'];
	}
	
	/**
	 * Function: setSubject
	 * Argument: sSubject (string) - The subject to be associated with this message.
	 *
	 * A subject is a short line of text indicating the contents of this very e-mail
	 * message. Subjects are limited to 78 characters as RFC 5322 recommends.
	 */
	
	public function setSubject ($sSubject)
	{
		if (!is_string ($sSubject) || !isset ($sSubject [1]) || strlen ($sSubject) > 78)
			throw new LengthException ('The Subject must be a string between one and 78 characters.');
		
		$this -> m_aHeaders ['Subject'] = $sSubject;
	}
	
	/**
	 * Function: getPriority
	 *
	 * A simple function to retrieve the priority of the e-mail message as has been
	 * received by this awesome class. That's about it.
	 */
	
	public function getPriority ()
	{
		if (!isset ($this -> m_aHeaders ['X-Priority']))
			return self :: PRIORITY_NORMAL ;
		
		switch ($this -> m_aHeaders ['X-Priority'])
		{
			case 1: case 2:		return self :: PRIORITY_HIGH;
			case 5: case 6:		return self :: PRIORITY_LOW;
			default:		return self :: PRIORITY_NORMAL;
		}
	}
	
	/**
	 * Function: setPriority
	 * Argument: nPriority (integer) - Constant defining the priority of this message.
	 *
	 * This simple function is capable of changing the priority associated with this
	 * e-mail message, per the constants defined earlier in this class.
	 */
	
	public function setPriority ($nPriority)
	{
		if (!is_integer ($nPriority) || $nPriority < self :: PRIORITY_LOW || $nPriority > self :: PRIORITY_HIGH)
			throw new OutOfRangeException ('Expected a priority between 0 (low) and 2 (high), got "' . $nMimeVersion . '"');
		
		$aRfcImplementation 	  = array (5, 3, 1);
		$aMicrosoftImplementation = array ('Low', 'Normal', 'High');
		
		$this -> m_aHeaders ['Priority'] 		= $aRfcImplementation [$nPriority];
		$this -> m_aHeaders ['X-Priority']		= $aRfcImplementation [$nPriority];
		$this -> m_aHeaders ['X-MSMail-Priority']	= $aMicrosoftImplementation [$nPriority];
		$this -> m_aHeaders ['X-MimeOLE']		= 'Message created by ' . $this -> m_aHeaders ['X-Mailer'];
	}
	
	/**
	 * Function: setLanguage
	 * Argument: sLanguage (string) - Two character language code for the contents.
	 *
	 * A simple function to change the language that will be used in this e-mail message,
	 * as a two character country code as specified in some random ISO standard.
	 */
	
	public function setLanguage ($sLanguage)
	{
		if (!is_string ($sLanguage) || strlen ($sLanguage) != 2)
			throw new InvalidArgumentException ('Expected a two-character country code, got "' . $sLanguage . '"');
		
		$this -> m_aHeaders ['Content-Language'] = $sLanguage;
	}
	
	/**
	 * Function: getReceiveNotification
	 *
	 * A very simple function that is capable of getting the receive notification e-mail
	 * address, has to be parsed manually afterwards though.
	 */
	
	public function getReceiveNotification ()
	{
		if (!isset ($this -> m_aHeaders ['Disposition-Notification-To']))
			return false ;
		
		return $this -> m_aHeaders ['Disposition-Notification-To'] ;
	}
	
	/**
	 * Function: setReceiveNotification
	 * Argument: sAddress (string) - Email address the notification should be sent to
	 *
	 * This function adds a header which enables clients to distribute receive 
	 * notifications of this message, so we know when they have been received.
	 */
	
	public function setReceiveNotification ($sAddress)
	{
		if (!is_string ($sAddress) || filter_var ($sAddress, FILTER_VALIDATE_EMAIL) === false)
			throw new InvalidArgumentException ('Expected an e-mailaddress, got "' . $sAddress . '"');
		
		$this -> m_aHeaders ['Disposition-Notification-To'] = '<' . $sAddress . '>';
	}
	
	/**
	 * Function: getAttachments
	 *
	 * This method is capable of returning the attachments which are present in this
	 * e-mail message; it just returns the array which we store locally.
	 */
	
	public function getAttachments ()
	{
		return $this -> m_aAttachments;
	}
	
	/**
	 * Function: addAttachment
	 * Argument: sName (string) - Filename of the attachment which will appear in the message.
	 * Argument: nType (integer) - Type of attachment to add, based on the constant.
	 * Argument: mData (mixed) - Data, filename or stream that contains this attachment.
	 * Argument: sMimeType (string) - MimeType of this very attachment, including primary type.
	 * 
	 *
	 * This function adds a new attachment to the e-mail, identified by either the filename
	 * or as direct content (use the last parameter!). Mime-types and such are required as well.
	 */
	
	public function addAttachment ($sName, $nType, $mData, $sMimeType)
	{
		$sAttachmentData = '';
		if ($nType == self :: ATTACHMENT_TYPE_FILE)
		{
			if (!file_exists ($mData))
				throw new InvalidArgumentException ('Attachment file "' . $mData . '" does not exist.');
			
			$sAttachmentData = file_get_contents ($mData);
		}
		elseif ($nType == self :: ATTACHMENT_TYPE_STREAM)
		{
			if (!is_resource ($mData))
				throw new InvalidArgumentException ('Cannot read from the incoming attachment stream.');
			
			$sAttachmentData = stream_get_contents ($mData);
		}
		elseif ($nType == self :: ATTACHMENT_TYPE_DATA)
		{
			$sAttachmentData = $mData;
		}
		else
		{
			throw new InvalidArgumentException ('Invalid attachment-type passed on.');
		}
		
		// Todo: implement mime-type validation
		
		$this -> m_aAttachments [] = array
		(
			'Content'	=> chunk_split (base64_encode ($sAttachmentData)),
			'Length'	=> strlen ($sAttachmentData),
			'Name'		=> $sName,
			
			'Headers'	=> array
			(
				'Content-Type'			=> $sMimeType . '; name="' . $sName . '"',
				'Content-Transfer-Encoding'	=> 'base64',
				'Content-Disposition'		=> 'attachment'
			)
		);
	}
	
	/**
	 * Function: addImage
	 * Argument: sName (string) - Filename of the image which will appear in the message.
	 * Argument: nType (integer) - Type of image to add, based on the constant earlier.
	 * Argument: mData (mixed) - Data, filename or stream that contains this image.
	 * Argument: sMimeType (string) - MimeType of this very image, including primary type.
	 *
	 * This function may be used to add a new image to the message, which will be made
	 * available in the message itself identified by the name.
	 */
	
	public function addImage ($sName, $nType, $mData, $sMimeType)
	{
		$sImageData = '';
		if ($nType == self :: IMAGE_TYPE_FILE)
		{
			if (!file_exists ($mData))
				throw new InvalidArgumentException ('Image file "' . $mData . '" does not exist.');
			
			$sImageData = file_get_contents ($mData);
		}
		elseif ($nType == self :: IMAGE_TYPE_GD)
		{
			if (!is_resource ($mData))
				throw new InvalidArgumentException ('Cannot read from the GD-image stream.');
			
			$sFilename = tempnam ('/tmp', '_img_gd');
			ImageJpeg ($mData, $sFilename, 95);
			
			$sMimeType  = 'image/jpeg';
			$sImageData = file_get_contents ($sFilename);
		}
		elseif ($nType == self :: IMAGE_TYPE_DATA)
		{
			$sImageData = $mData;
		}
		else
		{
			throw new InvalidArgumentException ('Invalid image-type passed on.');
		}
		
		// Todo: implement mime-type validation
		
		$this -> m_aAttachments [] = array
		(
			'Content'	=> chunk_split (base64_encode ($sImageData)),
			'Length'	=> strlen ($sImageData),
			'Name'		=> $sName,
			
			'Headers'	=> array
			(
				'Content-Type'			=> $sMimeType . '; name="' . $sName . '"',
				'Content-Disposition'		=> 'inline; filename="' . $sName . '"',
				'Content-Transfer-Encoding'	=> 'base64',
				'Content-ID'			=> $sName
			)
		);
	}
	
	/**
	 * Function: getBody
	 * Argument: nType (integer) - Type of body you wish to retrieve;
	 *
	 * This function returns the message body as associated with the e-mail that has been
	 * received, quite simply. Either the HTML or the Text/plain version.
	 */
	
	public function getBody ($nType = -1)
	{
		if ($nType == -1 || !isset ($this -> m_aBody [$nType]))
			return current ($this -> m_aBody);
		
		return $this -> m_aBody [$nType];
	}
	
	/**
	 * Function: setBody
	 * Argument: sContent (string) - Contents of the e-mail message
	 * Argument: nType (integer) - Type of content, either PLAIN or HTML (see the constants)
	 *
	 * This function may be used to set the contents of an e-mail message, probably the most
	 * important function of this entire class. Specifying both kinds of content (plain and
	 * html) will result in alternative headers during the rendering stage).
	 */
	
	public function setBody ($sContent, $nType = self :: CONTENT_TYPE_PLAIN)
	{
		if (!is_integer ($nType) || ($nType != self :: CONTENT_TYPE_PLAIN && $nType > self :: CONTENT_TYPE_HTML))
			throw new OutOfRangeException ('Invalid body content-type received, expected 0 or 1, got "' . $nMimeVersion . '"');
		
		$this -> m_aBody [$nType] = $sContent;
	}
	
	/**
	 * Function: setCharacterSet
	 * Argument: sCharset (string) - The character set to apply to this e-mail message.
	 *
	 * This function can be used to set the character set that should be applied to this very
	 * e-mail, which defaults to iso-8859-1, frequently used for e-mail messages.
	 */
	
	public function setCharacterSet ($sCharset)
	{
		$this -> m_sCharset = $sCharset;
	}
	
	/**
	 * Function: getDate
	 *
	 * This function may be used to retrieve the date the e-mail has been sent, or will
	 * be sent, or just false in case no information is available.
	 */
	
	public function getDate ()
	{
		if (!isset ($this -> m_aHeaders ['Date']))
			return false ;
		
		return $this -> m_aHeaders ['Date'];
	}
	
	/**
	 * Function: render
	 *
	 * This final function renders the e-mail as one large message including all the headers
	 * and the e-mail's body. That's basically everything which is required.
	 */
	
	public function render ()
	{
		if (count ($this -> m_aRecipients [self :: RECIPIENT_TYPE_TO]) == 0 || !isset ($this -> m_aHeaders ['From']) || !isset ($this -> m_aHeaders ['Subject']) || count ($this -> m_aBody) == 0)
			throw new Exception ('Not all required fields (recipient, sender and subject) have been set');
		
		$this -> m_aHeaders ['Message-ID'] = sprintf ('<%s@%s>', uniqid (), $this -> m_sHostname);
		$this -> m_aHeaders ['To'] = implode (',', $this -> m_aRecipients [self :: RECIPIENT_TYPE_TO]);
		
		if (count ($this -> m_aRecipients [self :: RECIPIENT_TYPE_CC]) > 0)
			$this -> m_aHeaders ['Cc'] = implode (',', $this -> m_aRecipients [self :: RECIPIENT_TYPE_CC]);
		
		if (count ($this -> m_aRecipients [self :: RECIPIENT_TYPE_BCC]) > 0)
			$this -> m_aHeaders ['Bcc'] = implode (',', $this -> m_aRecipients [self :: RECIPIENT_TYPE_BCC]);
		
		$sMessageBody   = '';
		$sMessageFooter = '';
		$sMessage       = '';
		
		// Attachments?
		if (count ($this -> m_aAttachments) > 0) 
		{
			$sEmailBoundary = 'Inforitus-mixed-' . uniqid ();
			$this -> m_aHeaders ['Content-Type'] = 'multipart/mixed; boundary="' . $sEmailBoundary . '"';
			
			$sMessageBody = '--' . $sEmailBoundary . PHP_EOL;
			foreach ($this -> m_aAttachments as $aAttachmentInfo)
			{
				$sMessageFooter .= '--' . $sEmailBoundary . PHP_EOL;
				foreach ($aAttachmentInfo ['Headers'] as $sHeaderName => $sHeaderValue)
				{
					$sMessageFooter .= $sHeaderName . ': ' . $sHeaderValue . PHP_EOL;
				}
				
				$sMessageFooter .= PHP_EOL . $aAttachmentInfo ['Content'] . PHP_EOL;
			}
			
			$sMessageFooter .= '--' . $sEmailBoundary . '--' . PHP_EOL;
		}
		
		// Message Body
		if (count ($this -> m_aBody) == 1)
		{
			$sContentType = key ($this -> m_aBody) == self :: CONTENT_TYPE_PLAIN ? 'plain' : 'html';
			if (count ($this -> m_aAttachments) > 0)
			{
				$sMessageBody .= 'Content-Type: text/'  . $sContentType . '; charset="' . $this -> m_sCharset . '"' . PHP_EOL;
				$sMessageBody .= 'Content-Transfer-Encoding: 7bit'  . PHP_EOL . PHP_EOL;
			}
			else
			{
				$this -> m_aHeaders ['Content-Type'] = 'text/'  . $sContentType . '; charset="' . $this -> m_sCharset . '"';
				$this -> m_aHeaders ['Content-Transfer-Encoding'] = '7bit';
			}
			
			$sMessageBody .= current ($this -> m_aBody) . PHP_EOL;
		}
		else
		{
			$sUniqueId = 'Inforitus-alt-' . uniqid ();
			if (count ($this -> m_aAttachments) > 0)
			{
				$sMessageBody .= 'Content-Type: multipart/alternative; boundary="' . $sUniqueId . '"' . PHP_EOL;
			}
			else
			{
				$this -> m_aHeaders ['Content-Type'] = 'multipart/alternative; boundary="' . $sUniqueId . '"';
			}
			
			foreach ($this -> m_aBody as $nType => $sContents)
			{
				$sContentType = $nType == self :: CONTENT_TYPE_PLAIN ? 'plain' : 'html';
				
				$sMessageBody .= PHP_EOL . '--' . $sUniqueId . PHP_EOL;
				$sMessageBody .= 'Content-Type: text/'  . $sContentType . '; charset="' . $this -> m_sCharset . '"' . PHP_EOL;
				$sMessageBody .= 'Content-Transfer-Encoding: 7bit'  . PHP_EOL . PHP_EOL;
				
				$sMessageBody .= $sContents . PHP_EOL;
			}
			
			$sMessageBody .= PHP_EOL . '--' . $sUniqueId . '--' . PHP_EOL;
		}
		
		// Compose the final message
		foreach ($this -> m_aHeaders as $sName => $sValue)
			$sMessage .= $sName . ': ' . $sValue . PHP_EOL;
		
		$sMessage .= PHP_EOL . $sMessageBody;
		$sMessage .= PHP_EOL . $sMessageFooter;
		
		return $sMessage;
	}
	
	/**
	 * Function: decode
	 * Argument: aLines (array) - An array with all lines in this e-mail message
	 *
	 * A simple function which will return all information available in this e-mail
	 * message, usually received by the MailClient class.
	 */
	
	public function decode ($aLines)
	{
		$aMessageHeaders = array ();
		$aMessageBodies  = array ();
		
		while (true)
		{
			$sLine = array_shift ($aLines);
			if ($sLine === false || !isset ($sLine [1])) // empty
				break ;
			
			list ($sName, $sValue) = explode (':', $sLine, 2);
			$sHeaderName = trim (strtolower ($sName));
			
			if (isset ($aMessageHeaders [$sHeaderName]))
			{
				$aMessageHeaders [$sHeaderName] .= ' ' . trim ($sValue);
			}
			else
			{
				$aMessageHeaders [$sHeaderName] = trim ($sValue);
			}
		}
		
		$aPriorityHeaders = array ('Low' => 5, 'Normal' => 3, 'High' => 1);
		
		$this -> m_aHeaders ['Subject'] = isset ($aMessageHeaders ['subject']) ? $aMessageHeaders ['subject'] : '';
		$this -> m_aHeaders ['X-Mailer'] = isset ($aMessageHeaders ['x-mailer']) ? $aMessageHeaders ['x-mailer'] : '';
		$this -> m_aHeaders ['MIME-Version'] = isset ($aMessageHeaders ['mime-version']) ? $aMessageHeaders ['mime-version'] : 2;
		
		if (isset ($aMessageHeaders ['x-priority'])) 		$this -> m_aHeaders ['X-Priority'] = (int) $aMessageHeaders ['x-priority'];
		elseif (isset ($aMessageHeaders ['x-msmail-priority']))	$this -> m_aHeaders ['X-Priority'] = (int) $aPriorityHeaders [$aMessageHeaders ['x-msmail-priority']];
		elseif (isset ($aMessageHeaders ['priority']))		$this -> m_aHeaders ['X-Priority'] = (int) $aMessageHeaders ['x-priority'];
		
		$this -> m_aHeaders ['Disposition-Notification-To'] = isset ($aMessageHeaders ['disposition-notification-to']) ? $aMessageHeaders ['disposition-notification-to'] : '';
		$this -> m_aHeaders ['Date'] = isset ($aMessageHeaders ['date']) ? strtotime ($aMessageHeaders ['date']) : time ();
		$this -> m_aHeaders ['Reply-To'] = isset ($aMessageHeaders ['reply-to']) ? $aMessageHeaders ['reply-to'] : '';
		$this -> m_aHeaders ['From'] = $aMessageHeaders ['from'];
		
		if (!isset ($aMessageHeaders ['to'])) /** gah... exceptions */
			$aMessageHeaders ['to'] = 'me@localhost.com';
		
		$this -> m_aRecipients [self :: RECIPIENT_TYPE_TO] = $this -> parseAddresses ($aMessageHeaders ['to']);
		
		if (isset ($aMessageHeaders ['cc']))
			$this -> m_aRecipients [self :: RECIPIENT_TYPE_CC] = $this -> parseAddresses ($aMessageHeaders ['cc']);
		
		if (isset ($aMessageHeaders ['content-type']) && substr ($aMessageHeaders ['content-type'], 0, 10) == 'multipart/')
		{
			preg_match ('/boundary="?([^"]+?)"/s', $aMessageHeaders ['content-type'] . '"', $aBoundary);
			if (!isset ($aBoundary [1]))
				throw new Exception ('Syntax error in the e-mail boundaries');
			
			$sBoundary = $aBoundary [1];
			$aChunks   = preg_split ('/\-\-' . preg_quote ($sBoundary) . '(\-\-)?/s', implode (PHP_EOL, $aLines));
			
			for ($i = 1, $j = count ($aChunks) - 1; $i < $j; $i ++)
			{
				$this -> decodeChunk (trim ($aChunks [$i]));
			}
			
		}
		else
		{
			if (isset ($aMessageHeaders ['content-type']) && strpos ($aMessageHeaders ['content-type'], 'html') !== false)
			{
				$this -> m_aBody [self :: CONTENT_TYPE_HTML]  = implode (PHP_EOL, $aLines);
			}
			else
			{
				$this -> m_aBody [self :: CONTENT_TYPE_PLAIN] = implode (PHP_EOL, $aLines);
			}
		}
	}
	
	/**
	 * Function: decodeChunk
	 * Argument: sChunk (string) - The chunk that you wish to decode.
	 *
	 * This function decodes a specific chunk of the e-mail, which may be the body,
	 * attachments or whatever else it might be.
	 */
	
	private function decodeChunk ($sChunk)
	{
		list ($sHeaders, $sContents) = preg_split ('/\r?\n\r?\n/s', $sChunk, 2);
		$aUnparsedHeaders = explode ("\n", $sHeaders);
		$aHeaders = array ();
		
		foreach ($aUnparsedHeaders as $sHeaderLine)
		{
			list ($sName, $sValue) = explode (':', trim ($sHeaderLine), 2);
			$aHeaders [strtolower ($sName)] = ltrim ($sValue);
		}
		
		if (isset ($aHeaders ['content-type']) && strpos ($aHeaders ['content-type'], 'multipart/') !== false)
		{
			preg_match ('/boundary="?([^"]+?)"/s', $aHeaders ['content-type'] . '"', $aBoundary);
			if (!isset ($aBoundary [1]))
				throw new Exception ('Syntax error in the partial e-mail boundaries');
				
			$sBoundary = $aBoundary [1];
			$aChunks   = preg_split ('/\-\-' . preg_quote ($sBoundary) . '(\-\-)?/s', $sContents);
			
			for ($i = 1, $j = count ($aChunks) - 1; $i < $j; $i ++)
			{
				$this -> decodeChunk (trim ($aChunks [$i]));
			}
			
			return true ;
		}
		
		if (isset ($aHeaders ['content-disposition']) || (isset ($aHeaders ['content-type']) && substr ($aHeaders ['content-type'], 0, 4) != 'text')) // attachment
		{
			$aFilename = array ();
			if (isset ($aHeaders ['content-type']) && strpos ($aHeaders ['content-type'], 'name') !== false)
			{
				preg_match ('/name="?([^"]+?)"/s', $aHeaders ['content-type'] . '"', $aFilename);
			}
			elseif (isset ($aHeaders ['content-disposition']))
			{
				preg_match ('/filename="?([^"]+?)"/s', $aHeaders ['content-disposition'] . '"', $aFilename);
			}
			else
			{
				if (isset ($aHeaders ['content-id']))
				{
					$sContentId = str_replace (array ('<', '>'), '', $aHeaders ['content-id']);
					$sExtension = 'dat';
					
					switch ($aHeaders ['content-type'])
					{
						case 'image/jpeg':	$sExtension = 'jpg';		break;
						case 'image/png':	$sExtension = 'png';		break;
						case 'image/gif':	$sExtension = 'gif';		break;
					}
					
					if (strpos ($sContentId, '@') !== false)
						$sContentId = strstr ($sContentId, '@', true);
					
					$aFilename [1] = $sContentId . '.' . $sExtension;
				}
				else
				{
					throw new Exception ('Could not determine attachment filename.');
				}
			}
			
			if (!isset ($aFilename [1]))
				throw new Exception ('Syntax error in the e-mail boundaries');
			
			$sContents = $this -> handleContentEncoding (trim ($sContents), $aHeaders ['content-transfer-encoding']);
			$sMimetype = strstr ($aHeaders ['content-type'], ';', true);
			$sFilename = $aFilename [1];
			
			$this -> m_aAttachments [] = array
			(
				'Content'	=> $sContents,
				'Length'	=> strlen ($sContents),
				'Name'		=> $sFilename,
				
				'Headers'	=> array
				(
					'Content-Type'			=> $sMimetype,
					'Content-Transfer-Encoding'	=> $aHeaders ['content-transfer-encoding'],
					'Content-Disposition'		=> 'attachment'
				)
			);
			
			return true ;
		}
		
		if (!isset ($aHeaders ['content-type']))
			$aHeaders ['content-type'] = 'plain';
		
		if (!isset ($aHeaders ['content-transfer-encoding']))
			$aHeaders ['content-transfer-encoding'] = '7bit';
		
		$nType = strpos ($aHeaders ['content-type'], 'html') !== false ? self :: CONTENT_TYPE_HTML : (strpos ($aHeaders ['content-type'], 'plain') !== false ? self :: CONTENT_TYPE_PLAIN : -1);
		if ($nType == -1)
			throw new Exception ('Unable to determine filetype of an attachment.');
		
		$this -> m_aBody [$nType] = $this -> handleContentEncoding (trim ($sContents), $aHeaders ['content-transfer-encoding']);
	}
	
	/**
	 * Function: handleContentEncoding
	 * Argument: sContent (string) - Contents of this piece which could have to be decoded
	 * Argument: sEncoding (string) - Type of encoding that has to be applied.
	 *
	 * This function can decode chunks in the e-mail contents with various settings,
	 * quite useful for making understandable text out of gibberish.
	 */
	
	private function handleContentEncoding ($sContent, $sEncoding)
	{
		switch (strtolower ($sEncoding))
		{
			case 'base64':
			{
				return base64_decode ($sContent);
			}
			
			case 'quoted-printable': // credits: PEAR::MimeDecode
			{
				$sContent = preg_replace ('/=\r?\n/', '', $sContent);
				return preg_replace ('/=([a-f0-9]{2})/ie', "chr(hexdec('\\1'))", $sContent);
			}
			
			default:
			{
				return $sContent;
			}
		}
	}
	
	/**
	 * Function: parseAddresses
	 * Argument: sAddress (string) - E-mail addresses 
	 *
	 * In order to convert a number of e-mail addresses to an array you need to use
	 * this very function, which does exactly that (suprisingly!)
	 */
	
	private function parseAddresses ($sAddresses)
	{
		$aMatchedAddresses = array ();
		
		preg_match_all ('/([\s]*)([_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*([ ]+|)@([ ]+|)([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,}))([\s]*)/i', $sAddresses, $aMatchedAddresses);
		
		$aAddresses = array ();
		if (isset ($aMatchedAddresses [0]))
		{
			$aAddresses = $aMatchedAddresses [0];
		}
		
		return $aAddresses;
	}
	
	/**
	 * Function: validate
	 *
	 * This function checks whether the e-mail is complete and can be rendered
	 * without any problems, thus will return a simple boolean.
	 */
	
	public function validate ()
	{
		if (count ($this -> m_aRecipients [self :: RECIPIENT_TYPE_TO]) == 0 || !isset ($this -> m_aHeaders ['From']) || !isset ($this -> m_aHeaders ['Subject']) || count ($this -> m_aBody) == 0)
			return false ;
		
		return true ;
	}
}

?>