<?php

/**
     @description: Group class
     @link: http://www.inforitus.nl
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class Group
{    
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
	    
	    /**
        * @desc the module of the group
        * 
        * @var string
        */
        
        var $sModule = null;
        
        /**
        * @desc contains all languages 
        * 
        * @var array
        */
        
        var $aLanguages = array ();
        
        /**
        * @desc contains default language 
        * 
        * @var array
        */
        
        var $aDefaultLang = array ();
       
        /**
        * @desc: contructor and set the module of the group.
        *
        * @param string $sModule 
        **/
                      
        public function Group ($sModule) 
        { 
                if (!empty ($sModule)) 
                {
        	            $this -> sModule      = $sModule;
                        $this -> aLanguages   = Language :: getLanguages ();
                        $this -> aDefaultLang = Language :: getLanguages (0, '', 'id', 'asc', true);
                } 
                else
                {
			            trigger_error (E_CORE_ERROR . ' a module must be given');
                        
			            return false;
		        }
        }
        
        /**
        * @desc: get the last errors
        *
        * @return array
        **/
         
        public function getLastError ()
        {
                return $this -> aError;
        }      
         
        /**
        * @desc: checkt the fields
        * 
        * @param array $aValues - values that have to be checked
        * 
        * @return booleaan
        **/
                                 
        private function checkForm ($aValues = array ()) 
        {
                if (empty ($aValues ['group_name'][$this -> aDefaultLang ['id']])) 
                {
                        $this -> aError [] = 'Groepnaam';
                }
                
                if (!$this -> aError) 
                {
                        return true;
                }
                
                return false;
        }    
         
        /**
        * Handle the name to go in slug
        * 
        * @param string $sParams - string to be converted
        *  
        * @returns string
        */
         
        public function urlize ($sParams)
        {
                $sTitle   = strtolower ($sParams);
                $aAllowed = preg_split ('//', 'abcdefghijklmnopqrstuvwxyz0123456789-+_');

                for ($i = 0, $j = strlen ($sTitle); $i < $j; $i++)
                {
                        if (!in_array ($sTitle  [$i], $aAllowed))
                        {
                                $sTitle  [$i] = '-';
                        }
                }

                for ($i = 0; $i < 3; $i++)
                {
                        $sTitle = str_replace ('--', '-', $sTitle);
                }
                
                return preg_replace ('/^ [\-]*(.+?) [\-]*$/s', '\\1', $sTitle);
        }
         
         
         
        /**
        * @desc: add a new group
        * 
        * @param array $aValues - data of the group
        * 
        * @returns: booleaan
        */
                                 
        public function addGroup ($aValues)
        {
                if ($this -> checkForm ($aValues)) 
                {
                        // take the item with the biggest sort number
                        $aGroupItems = $this -> getGroups ();
                        $aGroupItem  = (isset ($aGroupItems [0]) ? $aGroupItems [0] : '');
                        
                        // add post item
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT
                                INTO
                                    groups
                                    (
                                        module,
                                        date,
                                        private,
                                        order_number,
                                        blog
                                    )
                                VALUES
                                    (
                                        ?,
                                        NOW(),
                                        ?,
                                        ?,
                                        ?
                                    )
                        ");
                        
                        $iPrivate               = (isset ($aValues ['private']) ? 1:0);
                        $iBlog                  = (isset ($aValues ['blog']) ? $aValues ['blog']:0);
                        $iOrderNumber           = (isset ($aGroupItem ['order_number']) ? $aGroupItem ['order_number'] + 1 : 0);
                        
                        $pStatement -> bind_param ('siii',
                                $this -> sModule,
                                $iPrivate,
                                $iOrderNumber,
                                $iBlog 
                        );
                        
                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                                
                                return false;
                        }
                        
                        $iGroupId = Database :: getInstance () -> insert_id;
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                                $pStatement = Database :: getInstance () -> prepare ("
                                        INSERT
                                        INTO
                                            groups_data
                                            (
                                                group_id,
                                                lang_id,
                                                group_name,
                                                description,
                                                page_description,
                                                slug
                                            )
                                        VALUES
                                            (
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?
                                            )
                                ");
                                
                                $sGroupSlug = $this -> urlize ($aValues ['group_name'][$lang ['id']]); 
                                
                                $pStatement -> bind_param ('iissss',
                                        $iGroupId,
                                        $lang ['id'],
                                        $aValues ['group_name'][$lang ['id']],
                                        $aValues ['description'][$lang ['id']],
                                        $aValues ['page_description'][$lang ['id']],
                                        $sGroupSlug 
                                );
                                
                                if (!$pStatement -> execute ())
                                {
                                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                                        
                                        return false;
                                }        
                        }
                        
                        return true;
                }
                
                return false;
        }
         
        /**
        * @desc: get the groups
        * 
        * @param int $iItemId  Optional - id of group
        * @param string $sItemSlug Optional - the slug of group
        * @param string $sSortField - sort results by this field
        * @param string $sSortType - desc/asc
        * 
        * @returns array OR false by error
        */
                                       
        public function getGroups ($iItemId = 0, $sItemSlug = '', $sSortField = 'order_number', $sSortType = 'desc', $sLanguage = 'default')
        {
                if (!is_numeric ($iItemId)) 
                {
                        return false;
                }
                
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT
                            g.*,gd.group_name,gd.description,gd.page_description,gd.slug,
                            DATE_FORMAT(g.date, '%d-%m-%Y om %H:%i') AS date_format,
                            (SELECT COUNT(*) FROM groups_data WHERE group_id = g.id AND group_name != '' AND description != '') as filled          
                        FROM
                            groups g
                        LEFT JOIN
                            groups_data gd
                                ON
                            gd.group_id = g.id
                        LEFT JOIN
                            languages l 
                                ON
                            gd.lang_id = l.id   
                        WHERE
                            g.module = '" . $this -> sModule . "'
                            " . ($sLanguage == 'default'    ? sprintf ("AND l.default = 1") : sprintf ("AND l.short_name = '%s'", Database :: getInstance () -> real_escape_string ($sLanguage))) . "
                            " . (!empty ($iItemId)   ? sprintf ("AND g.id = %d", $iItemId) : '') . "
                            " . (!empty ($sItemSlug) ? sprintf ("AND gd.slug = '%s'", Database :: getInstance () -> real_escape_string ($sItemSlug)) : '') . "
                        ORDER BY
                        " . $sSortField . " " . $sSortType . "
                ");
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                        
                        return false;
                }
                
                $aResults = array ();   
                  
                if ($pResult -> num_rows > 0)
                {
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                //$aRow ['description']        = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                                $aResults [] = $aRow;
                        }
                }
                
                foreach ($aResults as $k => $value)
                {
                        if (count ($this -> aLanguages) > 1)
                        {
                                foreach ($this -> aLanguages as $lang)
                                {
                                        $pResult = Database :: getInstance () -> query ("
                                                SELECT
                                                    g.*,gd.*,
                                                    DATE_FORMAT(g.date, '%d-%m-%Y om %H:%i') AS date_format          
                                                FROM
                                                    groups g
                                                LEFT JOIN
                                                    groups_data gd
                                                        ON
                                                    gd.group_id = g.id
                                                LEFT JOIN
                                                    languages l 
                                                        ON
                                                    gd.lang_id = l.id   
                                                WHERE
                                                    g.module = '" . $this -> sModule . "'
                                                AND
                                                    l.id = " . $lang ['id'] . "
                                                AND
                                                    g.id = " . $value ['id'] . "
                                        ");
                                        
                                        if (!$pResult)
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                                                
                                                return false;
                                        }
                                        
                                        if ($pResult -> num_rows > 0)
                                        {
                                                while ($aRow = $pResult -> fetch_assoc ()) 
                                                {
                                                        if ($aRow ['group_name'] != '' || $aRow ['description'] != '')
                                                                $aRow ['filled'] = true;
                                                        $aRow ['description']        = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                                                        //$aRow ['page_description']   = html_entity_decode ($aRow ['page_description'], ENT_COMPAT, 'UTF-8');
                                                        $aRow ['page_description']   = htmlspecialchars ($aRow ['page_description'], ENT_COMPAT, 'UTF-8');
                                                        $aResults [$k][$lang ['id']] = $aRow;
                                                }
                                        }
                                }
                        }
                        else
                        {
                                unset ($value ['filled']);
                                if ($value ['group_name'] != '' || $value ['description'] != '')
                                        $value ['filled'] = true;
                                        
                                $aResults [$k][$this -> aDefaultLang ['id']] = $value;
                        }
                }       
                
                return (!empty ($iItemId) || !empty ($sItemSlug)) ? current ($aResults) : $aResults;          
        }           
         
        /**
        * @desc: changed the specified group
        * 
        * @param int iId 
        * @param array $aValues - data of the group
        * 
        * @returns booleaan
        */
                                       
        public function changeGroup ($iId, $aValues) 
        {
                if (!is_numeric ($iId))
                { 
                        return false;
                }
                
                if ($this -> checkForm ($aValues)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    groups
                                SET
                                    private          = ?,
                                    blog             = ?
                                WHERE
                                    id               = ?
                        ");
                        
                        $iPrivate               = (isset ($aValues ['private']) ? 1:0);
                        $iBlog                  = (isset ($aValues ['blog']) ? $aValues ['blog']:0);
                        
                        $pStatement -> bind_param ('iii',
                                $iPrivate,
                                $iBlog,
                                $iId
                        );
                        
                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                                $aLangValues = $this -> getGroups ($iId, '', 'order_number', 'desc');
                                
                                if (isset ($aLangValues [$lang ['id']]))
                                { 
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                UPDATE
                                                    groups_data
                                                SET
                                                    group_name       = ?,
                                                    description      = ?,
                                                    page_description = ?,
                                                    slug             = ?
                                                WHERE
                                                    group_id         = ?
                                                AND 
                                                    lang_id          = ?
                                        ");
                                        
                                        $sGroupSlug = $this -> urlize ($aValues ['group_name'][$lang ['id']]);
                                        
                                        $pStatement -> bind_param ('ssssii',
                                                $aValues ['group_name'][$lang ['id']],
                                                $aValues ['description'][$lang ['id']],
                                                $aValues ['page_description'][$lang ['id']],
                                                $sGroupSlug,         
                                                $iId,
                                                $lang ['id']
                                        );
                                        
                                        if (!$pStatement -> execute ())
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                
                                                return false;
                                        }
                                }
                                else
                                {
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                INSERT
                                                INTO
                                                    groups_data
                                                    (
                                                        group_id,
                                                        lang_id,
                                                        group_name,
                                                        description,
                                                        page_description,
                                                        slug
                                                    )
                                                VALUES
                                                    (
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?
                                                    )
                                        ");
                                        
                                        $sGroupSlug = $this -> urlize ($aValues ['group_name'][$lang ['id']]); 
                                        
                                        $pStatement -> bind_param ('iissss',
                                                $iId,
                                                $lang ['id'],
                                                $aValues ['group_name'][$lang ['id']],
                                                $aValues ['description'][$lang ['id']],
                                                $aValues ['page_description'][$lang ['id']],
                                                $sGroupSlug 
                                        );
                                        
                                        if (!$pStatement -> execute ())
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                                                
                                                return false;
                                        }       
                                }
                        }
                            
                        return true;
                }
            
                return false;
        }
         
        /**
        * @desc: delete the specified group
        * 
        * @param int iId 
        * 
        * @returns: booleaan
        */
          
        public function deleteGroup ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM
                            groups
                        WHERE
                            id = ?
                ");

                $pStatement -> bind_param ('i',
                        $iId
                );

                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM
                            groups_data
                        WHERE
                            group_id = ?
                ");

                $pStatement -> bind_param ('i',
                        $iId
                );

                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }

                return true;  
        }                             
     
}
?>
