<?php              
/**
 * Inforitus Server Control Panel v1.1
 *
 * This is the second revision of the Inforitus Server Control panel. In order to
 * keep performance at a maximum, without loosing flexibility or cleanness of the
 * code (which was, in all fairness, becoming a mess), a large part of the kernel
 * has been written to meet higher standards.
 *
 * This second revision supports unit tests, has a better architecture and is
 * better optimalized for front-end websites. Lots of components have been moved
 * to their own file rather than being included in the Kernel or one of the
 * Managers, making the code a lot easier to maintain.
 *
 * Author: Andrei Dragos
 *         andrei@inforitus.nl
 *
 */

require 'MailMessage.php';
require 'MailSender.php';

class Mail
{
	const	POP3		= 0;
	const	IMAP		= 1;

	/**
	 * Function: createMessage
	 *
	 * The create message function will initialise a new instance of the
	 * MailWriter class, which is capable or creating e-mails.
	 */
	
	public static function createMessage ()
	{
		return new MailMessage ();
	}
	
	/**
	 * Function: sendMessage
	 * Argument: pMessage (MailWriter) - The message that should be send.
	 *
	 * This function may be used to send an e-mail message over the internet
	 * utilizing the local SMTP server, or any other one when specified.
	 */
	
	public static function sendMessage (MailMessage $pMessage)
	{
		try
		{
			$pSendObject = new MailSender ($pMessage);
			$pResultObject = new stdClass;
			
			$pResultObject -> code    = $pSendObject -> send ();
			$pResultObject -> message = $pSendObject -> getMessage ($pResultObject -> code);
			
			return $pResultObject ;
		
		}
		catch (Exception $e) {}
		
		return false ;
	}

}

?>