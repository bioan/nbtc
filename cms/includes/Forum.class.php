<?php   

/**
     @description: forum class
     @link: http://www.inforitus.nl
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class Forum 
{ 
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * @desc contains the ordered list of comments 
        * 
        * @var array
        */
        
        var $aComments = array ();
        
        /**
        *@desc: contructor 
        *
        */
                      
        public function Forum () 
        { 
                
        }
        
        /**
        * @desc: get the last errors
        *
        * @returns array
        */
         
        public function getLastError ()
        {
                return $this -> aError;
        } 
        
        /**
        * @desc create the slug of an item
        * 
        * @param string $sParams - string to be converted
        * 
        * @returns string
        */
             
        public function urlize ($sParams)
        {
                $sTitle   = strtolower ($sParams);
                $aAllowed = preg_split ('//', 'abcdefghijklmnopqrstuvwxyz0123456789-+_');

                for ($i = 0, $j = strlen ($sTitle); $i < $j; $i++)
                {
                        if (!in_array ($sTitle  [$i], $aAllowed))
                        {
                                $sTitle  [$i] = '-';
                        }
                }

                for ($i = 0; $i < 3; $i++)
                {
                        $sTitle = str_replace ('--', '-', $sTitle);
                }
                
                return preg_replace ('/^ [\-]*(.+?) [\-]*$/s', '\\1', $sTitle);
        }
        
        /**
        * @desc Create a random password
        *
        * @param int $iLength number of characters
        * 
        * @return  string
        */

        public function random_password ($iLength = 8)
        {          
                $sChars    = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
                $sPassword = '';

                for ($i=0; $i < $iLength; $i++)
                {
                        $sPassword .= substr ($sChars, mt_rand (0, strlen ($sChars) -1), 1);
                }

                return $sPassword;
        }
        
        /**
        * @desc: check if email and username are unique
        * 
        * @param string $sEmail
        * @param string $sUsername 
        * 
        * @returns bool
        */
         
        public function checkData ($sEmail = '', $sUsername = '')
        {
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            id
                        FROM 
                            forum_users
                        WHERE 
                        type=0
                            " . (!empty ($sEmail) ? "AND email = '" . Database :: getInstance () -> real_escape_string ($sEmail) . "'" : '') . "
                            " . (!empty ($sUsername) ? "AND username = '" . Database :: getInstance () -> real_escape_string ($sUsername) . "'" : '') . "
                        
                    ");
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        return false;
                }
                
                return true;
        }   
        
        /**
        * @desc: check if code exist
        * 
        * @param string $sEmail
        * @param string $sUsername 
        * 
        * @returns bool
        */
         
        public function checkCode ($sCode = '')
        {
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            id
                        FROM 
                            forum_users
                        WHERE 
                            code = '" . Database :: getInstance () -> real_escape_string ($sCode) . "'
                    ");
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        return false;
                }
                
                return true;
        }   
         
        /**
        * @desc: get one or all the comments 
        *
        * @param int $iItemId - id of the item
        * @param int $iNewsId - id of the news 
        * @param string $sSortField - sort the result by this field
        * @param string $sSortType - desc/asc
        *  
        * @return array OR booleaan by error
        */
                                           
        public function getComments ($iItemId = 0, $iNewsId = 0, $iLangId = 0, $bChilds = true, $bTopics = false, $sSortField = 'id', $sSortType = 'desc')
        {        
                $this -> aComments = array ();
                
                if (!is_numeric ($iItemId))
                {          
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            c.*,u.username,u.email,
                            DATE_FORMAT(c.created, '%d-%m-%Y om %H:%i') AS date_register_format,
                            (SELECT COUNT(*) FROM comments_rates cr WHERE cr.comment_id = c.id) as count_rates 
                        FROM 
                            comments c
                        LEFT JOIN
                            forum_users u
                        ON
                            u.id = c.user_id
                        WHERE 
                            1
                            " . (!empty ($iLangId)   ? sprintf ("AND c.lang_id = %d ", $iLangId) : '') . "
                            " . (!empty ($iItemId)   ? sprintf ("AND c.id = %d ", $iItemId) : 'AND parent_id = 0') . "
                            " . (!empty ($iNewsId)   ? sprintf ("AND c.news_id = %d ", $iNewsId) : '') . "
                                                        
                        ORDER BY 
                            " . $sSortField . " " . $sSortType . "    
                    ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {         
                                $aRow ['message'] = strip_tags ($aRow ['message'], '<a><strong><b>');
                                $aRow ['level'] = 0;
                                $this -> aComments[] = $aRow;
                                
                                if ($bTopics == false)
                                        $this -> getOrderComments ($aRow, 0);  
                                
                        }
                }
                
                $aResults = (!empty ($iItemId) && $bChilds == false) ? current ($this -> aComments) : $this -> aComments;
                
                return $aResults;
        }
        
        /**
         * This creates the list of comments
         */
        
        private function getOrderComments ($aRow, $iLevel) 
        {  
                /* The following sql checks whether there's any reply for the comment */  
                $sQuery = "SELECT 
                                c.*,
                                DATE_FORMAT(c.created, '%d-%m-%Y om %H:%i') AS date_register_format,
                                u.username,
                                u.email,
                                (SELECT COUNT(*) FROM comments_rates cr WHERE cr.comment_id = c.id) as count_rates  
                            FROM 
                                comments c
                            LEFT JOIN
                                forum_users u
                            ON
                                u.id = c.user_id 
                            WHERE 
                                parent_id = " . $aRow ['id'] . "
                            ORDER BY id DESC";  
                                
                $pResult = Database :: getInstance () -> query ($sQuery);  
                
                if ($pResult -> num_rows > 0) // there is at least reply  
                {  
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                $aRow ['level'] = $iLevel+1;
                                $aRow ['message'] = strip_tags ($aRow ['message'], '<a><strong><b>');
                                $this -> aComments[] = $aRow;
                                $this -> getOrderComments ($aRow, $iLevel+1);  
                        }  
                }
                  
        } 
        
        /**
        * @desc: get one or all the users 
        *
        * @param int $iItemId - id of the item
        * @param string $sSortField - sort the result by this field
        * @param string $sSortType - desc/asc
        *  
        * @return array OR booleaan by error
        */
                                           
        public function getUsers ($iItemId = 0, $sSortField = 'id', $sSortType = 'desc', $sEmail = '')
        {        
                if (!is_numeric ($iItemId))
                {          
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            u.*
                        FROM 
                            forum_users u
                        WHERE 
                            1
                            " . (!empty ($iItemId)   ? sprintf ("AND u.id = %d ", $iItemId) : '') . "
                            " . (!empty ($sEmail)   ? sprintf ("AND u.email = '%s' ", $sEmail) : '') . "
                        ORDER BY 
                            " . $sSortField . " " . $sSortType . "    
                    ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                $aResults [] = $aRow;
                        }
                }
                
                $aResults = (!empty ($iItemId) || !empty ($sEmail)) ? current ($aResults) : $aResults;
            
                return $aResults;
        }
        
        /**
        * @desc: return an array with all data about images
        * 
        * @param string $sImages
        * 
        * @return array
        **/ 
         
        private function getPhotosByOids ($sImages)
        {
                if (empty ($sImages))
                {
                        return false;
                }
                
                $aImages        = explode (',', $sImages);
                $aProductPhotos = array ();
                
                foreach ($aImages as $image)
                {
                        if ($image != '')
                        {
                                $aDetailPhoto = $this -> getPhoto ($image);
                                
                                if (isset ($aDetailPhoto ['id']) && $aDetailPhoto ['id'] != '')
                                { 
                                        array_push ($aProductPhotos, $aDetailPhoto);
                                }
                        }
                }
                
                
                for ($i = 0; $i < count ($aProductPhotos); $i++)
                { 
                  
                        $aPathImages = explode ('/', $aProductPhotos [$i]['image_name'], -1);
                        $sPath       = '';

                        foreach ($aPathImages as $folder)
                        {
                                if ($folder != '')
                                {
                                        $sPath .= '/' . $folder;
                                }
                        }

                        $aProductPhotos [$i]['pathphoto'] = $sPath;
                }
                
                 return $aProductPhotos;
        }
        
        /**
        * @desc: get the data of an image 
        * 
        * @param int $iId
        * 
        * @return array
        */
        
        public function getPhoto ($iId = '')
        {
                if ($iId == '' || !is_numeric ($iId))
                {
                        return array ();
                }
                  
                $pQuery = Database :: getInstance () -> query ("
                        SELECT
                        *
                        FROM
                            images
                        WHERE
                            id = " . $iId . "
                        ORDER BY
                            id DESC
                        "
                );
                  
                if ($pQuery -> num_rows > 0)
                {
                        while ($aRow = $pQuery -> fetch_assoc ())
                        {
                                return $aRow;
                        }
                }
                
                return array ();
        }
        
        /**
        * @desc: check the fields
        *
        * @param array $aValues - fields that have to be checked
        * 
        * @returns booleaan
        */
                            
        private function checkFields ($aValues = array ())
        {
                
                
                
                if ($this -> aError) 
                {          
                        return false;
                }
                
                return true;
        }


        /**
        * @desc set nl date to ISO date
        *
        * @param string $sDate
        * 
        * @returns string 
        */
        
        private function nlToIsoDate ($sDate)
        {
                preg_match ('#([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})+#' ,$sDate, $aMatches);
                $sNewDate = $aMatches [3] . '-' . $aMatches [2] . '-' . $aMatches [1];
                
                return $sNewDate;
        }
        
        /**
        * @desc set ISO to nl data
        *
        * @param string $sDate
        * 
        * @returns string 
        **/
        
        public function isoToNlDate ($sDate)
        {
                preg_match ('#([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})+#' ,$sDate, $aMatches);
                $sNewDate = $aMatches [3] . '-' . $aMatches [2] . '-' . $aMatches [1];
                
                return $sNewDate;
        }
        
        /**
        * @desc: insert the comment item in the database
        *
        * @param array $aValues - the data of the item
        * 
        * @returns booleaan
        */
                                      
        public function addComment ($aValues = array ())
        {
                
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT INTO
                            comments
                            (
                                user_id,
                                parent_id,
                                news_id,
                                message,
                                slug,
                                created,
                                lang_id
                            )
                        VALUES
                            (
                                ?,
                                ?,
                                ?,
                                ?,
                                ?,
                                NOW(),
                                ?
                            )
                ");
                
                $aValues ['slug'] = $this -> urlize ($aValues ['message']);
                $aValues ['news_id'] = (isset ($aValues ['news_id']) ? $aValues ['news_id'] : 0);
                
                $pStatement -> bind_param ('iiissi',
                        $aValues ['user_id'],
                        $aValues ['parent_id'],
                        $aValues ['news_id'],
                        $aValues ['message'],
                        $aValues ['slug'],
                        $aValues ['lang_id']
                );
                
                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                return false;
        }
        
        /**
        * @desc: insert the comment item in the database
        *
        * @param array $aValues - the data of the item
        * 
        * @returns booleaan
        */
                                      
        public function addUser ($aValues = array ())
        {
                
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT INTO
                            forum_users
                            (
                                type,
                                username,
                                email,
                                password,
                                created
                            )
                        VALUES
                            (
                                ?,
                                ?,
                                ?,
                                ?,
                                NOW()
                            )
                ");
                
                $sPassword = (isset ($aValues ['password']) ? md5 ($aValues ['password']) : '');
                
                $pStatement -> bind_param ('isss',
                        $aValues ['type'],
                        $aValues ['username'],
                        $aValues ['email'],
                        $sPassword
                );
                
                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                return $pStatement -> insert_id;
        }

        /**
        * @desc: update a news item
        *
        * @param int $iId - id of the item
        * @param array $aValues - the data of the item
        * 
        * @return booleaan
        */
                                 
        public function updateComment ($iId, $aValues = array ())
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                
                if ($this -> checkFields ($aValues)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    comments
                                SET
                                    message = ?,
                                    slug = ?
                                WHERE
                                    id      = ? 
                        ");
                        
                        $aValues ['slug'] = $this -> urlize ($aValues ['message']);
                        
                        $pStatement -> bind_param ('ssi',
                                $aValues ['message'],
                                $aValues ['slug'],
                                $iId
                        );
                        
                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        return true;
                }
                
                return false;
        }
        
        /**
        * @desc: update a news item
        *
        * @param int $iId - id of the item
        * @param array $aValues - the data of the item
        * 
        * @return booleaan
        */
                                 
        public function updateUser ($iId = 0, $aValues = array (), $sEmail = '')
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                
                if (!isset ($aValues ['code']))
                {
                        if ($this -> checkFields ($aValues)) 
                        {
                                $pStatement = Database :: getInstance () -> prepare ("
                                        UPDATE
                                            forum_users
                                        SET
                                            username         = ?,
                                            email            = ?,
                                            password         = ?
                                        WHERE
                                            id               = ? 
                                ");
                                
                                $sPassword = (!empty ($aValues ['password']) ? md5 ($aValues ['password']) : $aValues ['old_password']);
                                
                                $pStatement -> bind_param ('sssi',
                                        $aValues ['username'],
                                        $aValues ['email'],
                                        $sPassword,
                                        $iId
                                );
                                
                                if (!$pStatement -> execute ())
                                {
                                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                        
                                        return false;
                                }
                                
                                return true;
                        }
                }
                elseif (isset ($aValues ['code']) && isset ($aValues ['password']))
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    forum_users
                                SET
                                    password         = ?,
                                    code             = ''
                                WHERE
                                    code             = ? 
                        ");
                        
                        $sPassword = (!empty ($aValues ['password']) ? md5 ($aValues ['password']) : $aValues ['old_password']);
                        
                        $pStatement -> bind_param ('ss',
                                $sPassword,
                                $aValues ['code']
                        );
                        
                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        return true;
                }
                else
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    forum_users
                                SET
                                    code  = ?
                                WHERE
                                    email = ? 
                        ");
                        
                        $pStatement -> bind_param ('ss',
                                $aValues ['code'],
                                $sEmail
                        );
                        
                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        return true;
                }
                
                return false;
        }
        
        /**
        * @desc: delete a comment and his childs
        *
        * @param int $aIds - ids of the items
        * 
        * @return booleaan   
        */
                              
        public function deleteComment ($aIds)
        {
                $pResult = $pStatement = Database :: getInstance () -> query ("
                        DELETE
                        FROM    
                            comments
                        WHERE
                            id IN (" . implode ($aIds) . ")
                ");
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                
                return true;
        }
        
        /**
        * @desc: delete a user item
        *
        * @param int $iId - id of the item
        * 
        * @return booleaan   
        */
                              
        public function deleteUser ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            forum_users
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                return true;
        }
        
        /**
        * @desc get the settings of forum
        * 
        * @return array
        */
        
        public function getSettings ()
        {
                $pResult = Database :: getInstance () -> query ("
                        SELECT
                            *
                        FROM
                            forum_settings
                ");
                
                
                $aResults = array ();   
                  
                if ($pResult -> num_rows > 0)
                {
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                $aResults [$aRow ['field']] = $aRow ['value'];
                        }
                }
                 
                return $aResults;
        }
        
        /**
        * @desc update the settings of forum
        * 
        * @param array $aValues - the settings of forum
        * 
        * @return boolean
        */
        
        public function updateSettings ($aValues = array ()) 
        {
                foreach ($aValues as $key => $value)
                {
                        if (in_array ($key, array ('login')))
                        {
                                $pResult = Database :: getInstance () -> query ("
                                        UPDATE
                                            forum_settings
                                        SET
                                            value   = '" . $value . "'
                                        WHERE
                                            field = '" . $key . "' 
                                ");
                                
                                
                                if (!$pResult) 
                                {
                                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                        
                                        return false;
                                }
                        }
                } 
                
                return true;
        }
        
        /**
	    * @desc check if user exist
	    *
	    * @param string $sEmail 
        * @param string $sPassword 
        * 
        * @return booleaan
	    */
	    
        public function checkUser ($sEmail, $sPassword)
        {
                $sQuery = sprintf ("
                        SELECT
                            *
                        FROM
                            forum_users
                        WHERE
                            email = '%s'
                        AND
                            password = '%s'
                        AND
                            type = 0
                        AND 
                            code = ''
                        ",
                        Database :: getInstance () -> real_escape_string ($sEmail),
                        md5 (Database :: getInstance () -> real_escape_string ($sPassword))
                );
                
                if (($pResult = Database :: getInstance () -> query ($sQuery))===false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   
                        
                        return false;
                }
                else 
                {
                        if ($pResult -> num_rows > 0 && !empty ($sEmail))
                        {
                                return true;   
                        }    
                }
                
                return false;
        }
}   
?>
