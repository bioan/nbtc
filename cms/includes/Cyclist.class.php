<?php 

class Cyclist{

    public $name;
    private $con;

    function __construct(){
        $this -> con = mysqli_connect("localhost","ionut_nbtc","kMp4emqH","ionut_nbtc");
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to database: " . mysqli_connect_error();
            die();
        }
    }

    public function getCountries(){
        $aResults = array();

        $q = "
            SELECT
                *
            FROM
                cyclists_countries";

        if ($result = mysqli_query($this -> con, $q))
        {
            if (mysqli_num_rows($result) > 0)
            {
                while ($row = mysqli_fetch_assoc($result))
                {
                    $aResults[] = $row;
                }    
            }
        }

        return $aResults;
    }

    public function getCountry($id){
        $aResults = array();

        $q = "
            SELECT
                *
            FROM
                cyclists_countries
            WHERE 
                id = '".$id."'";

        if ($result = mysqli_query($this -> con, $q))
        {
            if (mysqli_num_rows($result) > 0)
            {
                while ($row = mysqli_fetch_assoc($result))
                {
                    return $row;
                }    
            }
        }

        //return $aResults;
    }

    public function saveCountryName($id, $name){

        $pStatement = mysqli_prepare ($this -> con,"
                UPDATE
                    cyclists_countries
                SET
                    name   = ?
                WHERE
                    id = ?
        ");

        mysqli_stmt_bind_param($pStatement,'si',
                $name,
                $id
        );

        mysqli_stmt_execute($pStatement);
        mysqli_stmt_close($pStatement);
    }

    public function saveCountryPass($id, $pass){
        $pStatement = mysqli_prepare ($this -> con,"
                UPDATE
                    cyclists_countries
                SET
                    pass   = ?
                WHERE
                    id = ?
        ");

        mysqli_stmt_bind_param($pStatement,'si',
                $pass,
                $id
        );

        mysqli_stmt_execute($pStatement);
        mysqli_stmt_close($pStatement);
    }

    public function deleteCountry($id){

        $pStatement = mysqli_prepare ($this -> con,"
                DELETE
                FROM
                    cyclists_countries
                WHERE
                    id = ?
        ");
       
        mysqli_stmt_bind_param($pStatement,'i',
                $id
        );
        mysqli_stmt_execute($pStatement);
        mysqli_stmt_close($pStatement);

        // $pStatement2 = mysqli_prepare ($this -> con,"
        //         DELETE
        //         FROM
        //             cyclists_logins_ipad
        //         WHERE
        //             country_id = ?
        // ");

        // //  if ($pStatement2 === FALSE) {
        // //     die(mysqli_error($this->con));
        // // }
        // mysqli_stmt_bind_param($pStatement2,'i',
        //         $id
        // );
        // mysqli_stmt_execute($pStatement2);


        // $pStatement3 = mysqli_prepare ($this -> con,"
        //         DELETE
        //         FROM
        //             cyclists_logins_pc
        //         WHERE
        //             country_id = ?
        // ");

        // mysqli_stmt_bind_param($pStatement3,'i',
        //         $id
        // );
        // mysqli_stmt_execute($pStatement3);

        // mysqli_stmt_close($pStatement2);
        // mysqli_stmt_close($pStatement3);

        return true;
    }


    public function insertNewCountryName($id, $name){
        // $pStatement = mysqli_prepare ($this -> con,"
        //         INSERT INTO
        //             cyclists_ranking (name, points, datee)
        //         VALUES (?, 0, NOW())
        // ");
        
        // mysqli_stmt_bind_param($pStatement,'s',
        //         $name
        // );

        // mysqli_stmt_execute($pStatement);
    }

    public function getTodayScores(){
        $aResults = array();

        $q = "
            SELECT
                name, email, points, total_timer
            FROM
                cyclists_ranking
            WHERE 
                DATE_FORMAT(datee,'%Y-%m-%d') = '".date('Y-m-d')."'
            ORDER BY points DESC, total_timer ASC";

        if ($result = mysqli_query($this -> con, $q))
        {
            if (mysqli_num_rows($result) > 0)
            {
                while ($row = mysqli_fetch_assoc($result))
                {
                    $aResults[] = $row;
                }    
            }
        }

        return $aResults;
    }
    

    public function insertNewPlayer($name, $email){

        $pStatement = mysqli_prepare ($this -> con,"
                INSERT INTO
                    cyclists_ranking (name, email, points, datee)
                VALUES (?, ?, 0, NOW())
        ");
        
        mysqli_stmt_bind_param($pStatement,'ss',
                $name, $email
        );

        mysqli_stmt_execute($pStatement);

        
        $currentPlayerId = $this -> getCurrentCyclistId();

        //set the id in cyclist_playing
        $result = mysqli_query($this -> con, "
                SELECT
                    *
                FROM
                    cyclists_playing");

        if (mysqli_num_rows($result) > 0){
                $pStatement = mysqli_prepare ($this -> con,"
                        UPDATE
                            cyclists_playing
                        SET
                            cyclist_id   = ?,
                            playing      = 1,
                            question_nr  = 0,
                            video_nr     = 1
                ");

                mysqli_stmt_bind_param($pStatement,'i',
                        $currentPlayerId
                );

                mysqli_stmt_execute($pStatement);
                mysqli_stmt_close($pStatement);
        }
        else{
                $pStatement = mysqli_prepare ($this -> con,"
                        INSERT INTO
                            cyclists_playing (cyclist_id, playing, question_nr, video_nr)
                        VALUES (?, 1, 0, 1)
                        ");

                
                mysqli_stmt_bind_param($pStatement,'i',
                        $currentPlayerId
                );

                mysqli_stmt_execute($pStatement);
                mysqli_stmt_close($pStatement);
        }


    }

    public function isSomeonePlaying(){

        $result = mysqli_query($this -> con, "
                SELECT
                    *
                FROM
                    cyclists_playing");

        if (mysqli_num_rows($result) > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
                if ($row["playing"] == 1)
                {
                    return true;
                }
            }    
        }

        return false;
    }


	public function isSomeoneCyclingAtVideo($videoNr){

        $result = mysqli_query($this -> con, "
                SELECT
                    *
                FROM
                    cyclists_playing");

        if (mysqli_num_rows($result) > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
                if ($row["video_nr"] == $videoNr)
                {
                    return $row["cyclist_id"];
                }
            }    
        }

    	return false;
	}

    public function setVariantsOfQuestion($qId){

        $pStatement = mysqli_prepare ($this -> con,"
                UPDATE
                    cyclists_playing
                SET`
                    video_nr      = ?
        ");

        mysqli_stmt_bind_param($pStatement,'i',
                $qId
        );

        mysqli_stmt_execute($pStatement);
        mysqli_stmt_close($pStatement);

    }

    public function getAnswerCorrectivenes($ciclistId, $questionNr){
        $result = mysqli_query($this -> con, "
                SELECT
                    answer
                FROM
                    cyclists_answers
                WHERE
                    cyclist_id = '".$ciclistId."'
                AND 
                    question_nr = '".$questionNr."'");

        if (mysqli_num_rows($result) > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
                if ($row["answer"] == 1)
                        return 1;//1 means correct answer
            }    
        }
        else{//not yet answered the question
            return 2;
        }

        return 0; //0 means uncorrect answer

    }

    public function getVariantsForIpad($questionNr){
        $correctAnswer = $this -> getCorrectAnswerForQuestion($questionNr);

        $result = mysqli_query($this -> con, "
                SELECT
                    variants, current_order
                FROM
                    cyclists_variants
                WHERE
                    question_id=".$questionNr);

        $allAnswers = array();                    
        if (mysqli_num_rows($result) > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
                $allAnswers[$row["current_order"]] = $row["variants"];
            }  
            
            if (!isset($allAnswers[0]))
            {
                $allAnswers[0] = $correctAnswer;
            }
            if (!isset($allAnswers[1]))
            {
                $allAnswers[1] = $correctAnswer;
            }
            if (!isset($allAnswers[2]))
            {
                $allAnswers[2] = $correctAnswer;
            }
            if (count($allAnswers) > 3)
            {
                if (!isset($allAnswers[3]))
                {
                    $allAnswers[3] = $correctAnswer;
                }    
            }
            
        }

        return $allAnswers;
    }



    //here set the variants order for the ipad to take too
    public function getVariantsForPC($questionNr){
        $correctAnswer = $this -> getCorrectAnswerForQuestion($questionNr);


        $result2 = mysqli_query($this -> con, "
                SELECT
                    variants
                FROM
                    cyclists_variants
                WHERE
                    question_id=".$questionNr);

        $allAnswers = array();                    
        if (mysqli_num_rows($result2) > 0)
        {
            while($row2 = mysqli_fetch_assoc($result2))
            {
                $allAnswers[] = $row2["variants"];
            }  
            
            $allAnswers[] = $correctAnswer;

        }

        $poz = array();
        for($i=0; $i<count($allAnswers); $i++)
        {
            $poz[$i]=rand(0,1000000);
        }

        for($i=0; $i<count($poz)-1; $i++)
        {
            for($j=1; $j<count($poz); $j++) 
            {
                if ($poz[$i] < $poz[$j])
                {
                    $auv = $allAnswers[$j];
                    $allAnswers[$j]=$allAnswers[$i];
                    $allAnswers[$i]=$auv;
                }
            }
        }

        foreach ($allAnswers as $key => $value) {
            if ($value != $correctAnswer){
                $this -> setVariantOrder($questionNr, $value, $key);
            }
        }

        return $allAnswers;
    }

    public function setVariantOrder($questionNr, $variant, $order){
        $pStatement = mysqli_prepare ($this -> con,"
                UPDATE
                    cyclists_variants
                SET
                    current_order = ?
                WHERE 
                    question_id = ?
                AND
                    variants = ?");

        mysqli_stmt_bind_param($pStatement,'iis',
                $order,
                $questionNr,
                $variant
        );

        mysqli_stmt_execute($pStatement);
        mysqli_stmt_close($pStatement);
    }

    public function getCurrentCyclistId(){
        $q = "
            SELECT
                MAX(id) as maxid
            FROM
                cyclists_ranking";
        //return the id of the new inserted
        $aResults = array();

        if ($result = mysqli_query($this -> con, $q))
        {
            if (mysqli_num_rows($result) > 0)
            {
                $row = mysqli_fetch_assoc($result);
            }
        }

        return $row["maxid"];
    }
    

    public function getCorrectAnswerForQuestion($questionNr){
        $result = mysqli_query($this -> con, "
                SELECT
                    *
                FROM
                    cyclists_questions
                WHERE
                    question_id=".$questionNr);

        $correctAnswer = -1;
        if (mysqli_num_rows($result) > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
                $correctAnswer = $row["answer"];
            }    
        }

        return $correctAnswer;
    }

    public function getCurrentPointsForCyclist($currentPlayerId){
         $q = "
            SELECT
                points
            FROM
                cyclists_ranking 
            WHERE 
                id = ".$currentPlayerId;

        $currentPoints = 0;
        if ($result = mysqli_query($this -> con, $q))
        {
            if (mysqli_num_rows($result) > 0)
            {
                while($row = mysqli_fetch_assoc($result))
                {
                    $currentPoints = $row["points"];
                }
            }
        }

        return $currentPoints;
    }

    public function registerAnswer($questionNr, $answer, $timer){
        $correctAnswer = $this -> getCorrectAnswerForQuestion($questionNr);
        $currentPlayerId = $this -> getCurrentCyclistId();
       
        if ($correctAnswer == $answer){
            $ans = 1;
            
            $currentPoints = $this -> getCurrentPointsForCyclist($currentPlayerId);
            $currentPoints = $currentPoints + 1;
        }
        else{
            $ans = 0;
        }

        $pStatement2 = mysqli_prepare ($this -> con,"
                INSERT INTO
                    cyclists_answers (cyclist_id, question_nr, answer, timer)
                VALUES (?, ?, ?, ?)
                ");

        
        mysqli_stmt_bind_param($pStatement2,'iiii',
                $currentPlayerId,$questionNr,$ans, $timer
        );

        mysqli_stmt_execute($pStatement2);
        mysqli_stmt_close($pStatement2);

        return $ans;
    }

    public function registerHighscore(){
        $currentPlayerId = $this -> getCurrentCyclistId();

         $q = "
            SELECT
                cyclist_id, SUM(answer) as points, SUM(timer) as total_time
            FROM
                cyclists_answers
            GROUP BY cyclist_id
            HAVING cyclist_id=".$currentPlayerId;

        if ($result = mysqli_query($this -> con, $q))
        {
            if (mysqli_num_rows($result) > 0)
            {
                $row = mysqli_fetch_assoc($result);
            }
        }

        $points = $row["points"];
        $totalTime = $row["total_time"];

        $pStatement = mysqli_prepare ($this -> con,"
                UPDATE
                    cyclists_ranking
                SET
                    points      = ?,
                    total_timer = ?
                WHERE 
                    id = ".$currentPlayerId);

        mysqli_stmt_bind_param($pStatement,'ii',
                $points,
                $totalTime
        );

        mysqli_stmt_execute($pStatement);
        mysqli_stmt_close($pStatement);
    }



    public function setQuestionNr($qNr){
        $pStatement = mysqli_prepare ($this -> con,"
                UPDATE
                    cyclists_playing
                SET
                    question_nr      = ?
        ");

        mysqli_stmt_bind_param($pStatement,'i',
                $qNr
        );

        mysqli_stmt_execute($pStatement);
        mysqli_stmt_close($pStatement);
    }



    public function getQuestionText($qNr){
        $result = mysqli_query($this -> con, "
                SELECT
                    text
                FROM
                    cyclists_questions
                WHERE
                    question_id=".$qNr);

        $text = "";
        if (mysqli_num_rows($result) > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
                $text = $row["text"];
            }    
        }

        return $text;
    }



    public function questionPopped($qNr){
        $result = mysqli_query($this -> con, "
                SELECT
                    question_nr
                FROM
                    cyclists_playing");

        if (mysqli_num_rows($result) > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
                if ($row["question_nr"] == $qNr)
                {
                    return true;
                }
            }    
        }

        return false;
    }



    public function setNextVideo($qNr){
        $pStatement = mysqli_prepare ($this -> con,"
                UPDATE
                    cyclists_playing
                SET
                    video_nr      = ?
        ");

        $nextQuestionNr = $questionNr+1;
        mysqli_stmt_bind_param($pStatement,'i',
                $nextQuestionNr
        );

        mysqli_stmt_execute($pStatement);
        mysqli_stmt_close($pStatement);
    }


    public function setVideoNr($videoNr){
        $pStatement = mysqli_prepare ($this -> con,"
                UPDATE
                    cyclists_playing
                SET
                    video_nr      = ?
        ");

        mysqli_stmt_bind_param($pStatement,'i',
                $videoNr
        );

        mysqli_stmt_execute($pStatement);
        mysqli_stmt_close($pStatement);
    }





    public function getHighscore(){
        $currentPlayerId = $this -> getCurrentCyclistId();

        $q = "
            SELECT
                points
            FROM
                cyclists_ranking
            WHERE
                id=".$currentPlayerId;
       

        $points = 0;

        if ($result = mysqli_query($this -> con, $q))
        {
            if (mysqli_num_rows($result) > 0)
            {
                $row = mysqli_fetch_assoc($result);
            }
        }

        $points = $row["points"];


        $pStatement = mysqli_prepare ($this -> con,"
                DELETE
                FROM
                    cyclists_playing
        ");
        
        mysqli_stmt_execute($pStatement);
        mysqli_stmt_close($pStatement);

        return $points;
    }



    public function resett(){
        $pStatement = mysqli_prepare ($this -> con,"
                DELETE
                FROM
                    cyclists_playing
        ");
        
        mysqli_stmt_execute($pStatement);
        mysqli_stmt_close($pStatement);
    }









    function __destruct() {
       mysqli_close($this -> con);
    }

}

?>