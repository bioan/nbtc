<?php
          
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php';   
/**
    @description: login class 
    @link: http://www.inforitus.nl
    @author Andrei Dragos <andrei@inforitus.nl>
**/
    
class Authentication
{   
        /**
        * @desc the user name
        * 
        * @var string
        */
        
        var $sUserName;
        
        /**
        * @desc the password of user
        * 
        * @var string 
        */
        
        var $sPassword;
        
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * @desc the id of user
        * 
        * @var integer
        */
        
        var $iUserId;

        /**
        * @desc value true if is logged in else false
        * 
        * @var bool
        */
        
        var $bLogin = false;
           
	    /**
        * @desc the host of the site
        * 
        * @var string
        */
        
        var $sHostName;
        
        /**
        * constructor
        *
        * @param string $sUsername Optioneel
        * @param string $sPassword Optioneel
        */
        
	    public function Authentication ($sUsername = '', $sPassword = '') 
        {                           
                $this -> sUserName = (isset ($_POST ['user_name']) && !empty ($_POST ['user_name'])) ? $_POST ['user_name'] : '';
                $this -> sPassword = (isset ($_POST ['password']) && !empty ($_POST ['password'])) ? $_POST ['password'] : '';
		          
                str_replace ('www.', '', $_SERVER ['SERVER_NAME'], $count);
                
                if (strpos ($_SERVER ['SERVER_NAME'], 'www.') == 0 && $count == 1)  
                        $this -> sHostName = str_replace ('www.', '', $_SERVER ['SERVER_NAME']);
		        else
                        $this -> sHostName = $_SERVER ['SERVER_NAME'];
		        
        }  

        /**
	    * @desc get last error(s)
	    *
	    * @return array
	    */
	    
	    public function getLastError ()
   	    {
       	        return $this -> aError;
  	    } 
  	    
  	    /**
	    * @desc check if the data is corrected
	    *
	    * @return booleaan
	    */
	    
	    private function checkIfUserIsValid ()
	    {
                $sQuery = sprintf ("
                        SELECT 
            	            user_name,
				            password
                        FROM 
                            users
                        WHERE 
                            user_name = '%s'
                                AND 
                            password = '%s'
                        ",
                    
                        Database :: getInstance () -> real_escape_string ($this -> sUserName),
                        Database :: getInstance () -> real_escape_string (md5 ($this -> sPassword))
                );
                
                if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                        
    		            return false;
                } 
                else 
                {
                        if ($pResult -> num_rows == 0) 
                        {
                                return false;
                        }
                }
                
                return true;
        }
        
        /**
	    * @desc check the forms on errors
	    *
	    * @return bool
	    */
	    
	    private function checkForm ()
   	    {
       	        if (empty ($this -> sUserName) && empty ($this -> sPassword)) 
                {
                        $this -> aError ['user_pass'] = true;   
                }
                
                if (empty ($this -> sUserName)) 
                {
                        $this -> aError ['user'] = true;
                }
                
                if (empty ($this -> sPassword)) 
                {
                        $this -> aError ['pass'] = true;
                }
                
                if ((!$this -> checkIfUserIsValid ()) && !empty ($this -> sUserName) && !empty ($this -> sPassword)) 
                {
                        $this -> aError ['unknown'] = 'Deze gegevens komen niet voor bij ons in de database.';   
                }
                
                if (!empty ($this -> aError))
                {
                        return false;
                }
                
                return true;
  	    } 
	    
        /**
	    * @desc get the user id of the specified user
	    *
	    * @return booleaan
	    */
	    
	    protected function getUserId ()
	    {
            $pResult = Database :: getInstance () -> query ("
                    SELECT 
            	        id
                    FROM 
                        users
                    WHERE 
                        user_name = '" . Database :: getInstance () -> real_escape_string ($this -> sUserName) . "'
            ");
            
            if (!$pResult) 
            {
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                    
    		        return false;
            } 
            else 
            {
                    $aResult = $pResult -> fetch_assoc ();
                    
                    if ($pResult -> num_rows !=0) 
                    {
                            $this -> iUserId =& $aResult ['id'];
                            
                            return true;
                    }
                
                    return false;
            }
        }
        
        /**
	    * @desc set the last login of the specified user
	    *
	    * @return booleaan
	    */
	    
        private function setLastLogin ()
        {   
                $pStatement = Database :: getInstance () -> prepare ("	
                        INSERT
                        INTO
                            logins
                            (
                                time,
                                client_ip,
					            user_id
                            )
                        VALUES
                            (
                                NOW(),
                                ?,
                                ?
                            )
                ");
                
                $pStatement -> bind_param ('si',
                        $_SERVER ['REMOTE_ADDR'],
                        $this -> iUserId
                );
                
                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                          
                        return false;
                } 
                
                return true;
        }
        
        /**
	    * @desc set the sessions and cookies of the user
	    *
        * @return boolean
	    */
	    
        private function setSessionAndCookie ()
        {
                setcookie ('user_id', $this -> iUserId, time () + 60 * 60 * 1 * 1, '/', $this -> sHostName); 
                setcookie ('home_slide', 1, time () + 60 * 60 * 1 * 1, '/', $this -> sHostName); 
		        setcookie ('user_name', $this -> sUserName, time () + 60 * 60 * 1 * 1, '/', $this -> sHostName);
                
                $_SESSION ['user_id']   =  $this -> iUserId;
		        $_SESSION ['login'] 	= TRUE;
		        $_SESSION ['ipadres'] 	= $_SERVER ['REMOTE_ADDR'];
                
                return true;	
        }
        
        /**
	    * @desc check the user by login
	    *
	    * @return booleaan
	    **/
	    
	    public function loginControleFirstTime ()
	    {                  
		        if ($this -> checkForm ()) 
                {            
		                if ($this -> getUserId ()) 
                        {
		                        if ($this -> setLastLogin ()) 
                                {
                                       if ($this -> setSessionAndCookie ()) 
                                       {
                                                return true;
                                       } 
				                }
        	            }
                }
                
		        return false;
	    }

        /**
	    * @desc Control of user has been logged in
	    *
	    * @return boolean
	    */
	    
	    public function validateUser ()
        {	if(isset ($_COOKIE ['user_name'])){
                                        //echo $_COOKIE ['user_name'];die('a');
                                        }
                if ((isset ($_COOKIE ['user_id']) && isset ($_COOKIE ['user_name']) && (preg_match ('#^\d{1,5}$#', $_COOKIE ['user_id'])))
                                                    || 
                (isset ($_SESSION ['login']) && $_SESSION ['login'] === TRUE)) 
                {            
    		            $sQuery = sprintf ("
                                SELECT
                                    client_ip
                                FROM
                                    logins
                                WHERE 
                                    user_id = %d
    						                AND
    				                time > DATE_SUB(NOW(),INTERVAL 1 DAY)
    			                ORDER BY
    			                     time DESC
    			                LIMIT 
    			                     1
    				                ",
    				                (isset ($_COOKIE ['user_id']) ? $_COOKIE ['user_id'] : 982734)
                        );
                        
                        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                
    		                    return false;
                        } 
                        else 
                        {         
				                if ($pResult -> num_rows == 0) 
                                {
					                    unset ($_COOKIE ['user_id']);
					                    unset ($_SESSION ['login']);
					                    unset ($_COOKIE ['user_name']);
					                    return false;
				                }
                                
				                $aRow = $pResult -> fetch_assoc ();
                                
                                if ($_SERVER ['REMOTE_ADDR'] == $aRow ['client_ip']) 
                                {
                                        $_SESSION ['login']    = TRUE;
                                        $_SESSION ['ipadres']  = $_SERVER ['REMOTE_ADDR'];
                                        $_SESSION ['user_id']  = $_COOKIE ['user_id'];
                                        
                                        setcookie ('user_id', $_COOKIE ['user_id'], time () + 60 * 60 * 1 * 1,'/', $this -> sHostName);
    		                            setcookie ('user_name', $_COOKIE ['user_name'], time () + 60 * 60 * 1 * 1, '/', $this -> sHostName);
                                        
                                        return true;
                                }
                                
                                return false;
                        }
                }
                
		        return false;	
	    }
	
		
}      
?>
