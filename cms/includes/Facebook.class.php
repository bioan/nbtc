<?php   

/**
     @description: news class
     @link: http://www.inforitus.nl
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class Facebook
{ 
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * @desc contains all languages 
        * 
        * @var array
        */
        
        var $aLanguages = array ();
        
        /**
        * @desc contains default language 
        * 
        * @var array
        */
        
        var $aDefaultLang = array ();
        
        /**
        *@desc: contructor 
        *
        */
                      
        public function Facebook () 
        { 
                $this -> aLanguages   = Language :: getLanguages ();
                $this -> aDefaultLang = Language :: getLanguages (0, '', 'id', 'asc', true);   
        }
        
        /**
        * @desc: get the last errors
        *
        * @returns array
        */
         
        public function getLastError ()
        {
                return $this -> aError;
        }  
         
        /**
        * @desc create the slug of an item
        * 
        * @param string $sParams - string to be converted
        * 
        * @returns string
        */
             
        public function urlize ($sParams)
        {
                $sTitle   = strtolower ($sParams);
                $aAllowed = preg_split ('//', 'abcdefghijklmnopqrstuvwxyz0123456789-+_');

                for ($i = 0, $j = strlen ($sTitle); $i < $j; $i++)
                {
                        if (!in_array ($sTitle  [$i], $aAllowed))
                        {
                                $sTitle  [$i] = '-';
                        }
                }

                for ($i = 0; $i < 3; $i++)
                {
                        $sTitle = str_replace ('--', '-', $sTitle);
                }
                
                return preg_replace ('/^ [\-]*(.+?) [\-]*$/s', '\\1', $sTitle);
        } 
        
        /**
        * @desc check App fields and insert or update database
        * 
        * @param array $aValues - array of values containing app id and app secret
        * 
        * @returns boolean
        */
             
        public function checkApplication ($aValues = array ())
        {
                if (empty ($aValues ['appID']))
                {
                        $this -> aError ['appID'] = "Application's id is required";
                }
                
                if (empty ($aValues ['appSecret']))
                {
                        $this -> aError ['appSecret'] = "Application's secret code is required";
                }
                
                if (count ($this -> aError) == 0)
                {
                        $facebookAppData = $this ->getApplication();
                        
                        if (!$facebookAppData)
                        {
                                $this ->addApplication($aValues);
                        }
                        else 
                        {
                                $this ->updateApplication($aValues);
                        }
                        return true;
                }
                else
                {
                        return false;
                }
        } 
        
        
        /**
        * @desc get the facebook Application
        * 
        * 
        * @returns array, data of application
        */
             
        public function getApplication ()
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            facebook_app
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;          
                
		        return $aResults;
        } 
        
        
        
        /**
	    * @desc add facebook application
	    *
        * @param array $aValues - the values that have to be set
        * 
	    * @return boolean
	    */
	    
	    public function addApplication ($aValues = array ())
	    {	
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT
                        INTO
                            facebook_app
                                (
                                appID,
                                appSecret,
                                date                
                                )      
                        VALUES
                                (
                                ?,
                                ?,
                                NOW()               
                                ) 
                ");

                $pStatement -> bind_param ('ss',
                        $aValues ['appID'],
                        $aValues ['appSecret']				
                );

                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   

                        return false;
                }

                return true;
        }
        
        /**
	    * @desc update Application
	    *
        * @param array $aValues - the values that have to be set
        *  
	    * @return boolean
	    */
        
	    public function updateApplication ($aValues = array ())
	    {
            
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            facebook_app
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;
            
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            facebook_app
                        SET
                            appID      = ?,
                            appSecret  = ?,
                            date       = NOW()
                        WHERE
                            id         = " . $aResults['id'] ."
                ");

                $pStatement -> bind_param ('ss',    
                        $aValues ['appID'],	
                        $aValues ['appSecret']
                );

                if (!$pStatement -> execute ()) 
                {	
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    

                    return false;
                } 

                return true;
            }
        
        /**
	    * @desc check Application's secret key and id
	    *
	    * @param array parameters (secret key, app id, redirect uri)
        *  
	    * @return boolean
	    */
        
	    public function checkAppIfValid ($params)
	    {
                
                $url = 'https://graph.facebook.com/oauth/access_token?';
                $ch = curl_init();
                curl_setopt ($ch, CURLOPT_URL, $url);
                curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt ($ch, CURLOPT_POSTFIELDS, http_build_query($params, null, '&'));
                $ret = curl_exec ($ch);
                curl_close ($ch);
                
                $response = json_decode ($ret, true);
                
                if (array_key_exists ('error', $response))
                {
                        if ($response ['error']['message'] == 'Invalid verification code format.')
                                return true;
                        else if ($response ['error']['message'] == 'Error validating application. Invalid application ID.')
                        {
                                $this -> aError ['appID'] = 'Invalid application ID.';
                                return false;
                        }
                        else if ($response ['error']['message'] == 'Error validating client secret.')
                        {
                                $this -> aError ['appID'] = 'Invalid application secret key.';
                                return false;
                        }
                }
                
                return true;
        }
        
         /**
        * @desc get Faceook user if exists
        * 
        * @param array parameters 
        * @returns array
        */
        
        public function getFacebookUser ($userID, $redirect_url)
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            facebook_users
                        WHERE
                            userID = " . $userID . "
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                if (!empty ($aResults) && $this -> checkAccessToken($aResults [0]['facebookID'], $aResults [0]['accessToken'], $redirect_url))
                        $aResults [0]['pages'] = $this -> getFacebookUserPages ($aResults [0]['facebookID'], $aResults [0]['accessToken']);
                                
                $aResults = (count ($aResults) == 1) ? current ($aResults) : $aResults;          
                
		        return $aResults;
        }
        
        /**
        * @desc get manageable pages of facebook user
        * 
        * @param facebook id of user and access_token of user
        * @returns array
        */
        
        public function getFacebookUserPages ($facebookID, $access_token)
        {
                $graph_url = "https://graph.facebook.com/" . $facebookID . "/accounts?" . "access_token=" . $access_token;
                $response = $this -> curl_get_file_contents($graph_url);
                $arrayPages = json_decode($response, true);
                
                if (count ($arrayPages ['data'] > 0))
                {
                        $aPages = array ();
                        foreach ($arrayPages ['data'] as $page)
                        {
                                $aPages [$page ['id']] = $page;
                        }
                }
                $aPages [$facebookID]['access_token'] = $access_token;
                $aPages [$facebookID]['name'] = 'Profile page';
                return $aPages;
        }
        
        public function checkFacebookUser ($aValues = array ())
        {
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            facebook_users
                        WHERE
                            userID = " . $aValues ['userID'] . "
			            
                ");	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                
                if ($pResult -> num_rows > 0)
                {         
                        $this -> updateFacebookUser ($aValues);
                }
                else
                {
                        $this ->addFacebookUser($aValues);
                }

                return true;
        } 
        
        /**
	    * @desc add facebook users
	    *
        * @param array $aValues - the values that have to be set
        * 
	    * @return boolean
	    */
	    
	    public function addFacebookUser ($aValues = array ())
	    {	
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT
                        INTO
                            facebook_users
                                (
                                userID,
                                facebookID,
                                accessToken,
                                publishID,
                                publishAccessToken,
                                date                
                                )      
                        VALUES
                                (
                                ?,
                                ?,
                                ?,
                                ?,
                                ?,
                                NOW()               
                                ) 
                ");
                
                $publishID = (isset ($aValues ['publishID'])) ? $aValues ['publishID'] : $aValues ['facebookID'];
                $publishAccessToken = (isset ($aValues ['publishAccessToken'])) ? $aValues ['publishID'] : $aValues ['accessToken'];

                $pStatement -> bind_param ('sssss',
                        $aValues ['userID'],
                        $aValues ['facebookID'],
                        $aValues ['accessToken'],
                        $publishID,
                        $publishAccessToken
                );

                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   

                        return false;
                }

                return true;
        }
        
         /**
	    * @desc update facebook user
	    *
        * @param array $aValues - the values that have to be set
        *  
	    * @return boolean
	    */
        
	    public function updateFacebookUser ($aValues = array ())
	    {            
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            facebook_users
                        SET
                            userID             = ?,
                            facebookID         = ?,
                            accessToken        = ?,
                            publishID          = ?,
                            publishAccessToken = ?,
                            date               = NOW()
                        WHERE
                            userID         = " . $aValues['userID'] ."
                ");
                
                $publishID = (isset ($aValues ['publishID'])) ? $aValues ['publishID'] : $aValues ['facebookID'];
                $publishAccessToken = (isset ($aValues ['publishAccessToken'])) ? $aValues ['publishAccessToken'] : $aValues ['accessToken'];

                $pStatement -> bind_param ('sssss',    
                        $aValues ['userID'],	   
                        $aValues ['facebookID'],	
                        $aValues ['accessToken'],
                        $publishID,
                        $publishAccessToken
                );

                if (!$pStatement -> execute ()) 
                {	
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);    

                    return false;
                } 

                return true;
            }
         
         
        
         /**
	    * @desc check for user's permissions
	    *
        * @param 
        *  
	    * @return boolean
	    */
         public function checkFacebookUserPermission ($facebookID, $access_token, $permissions, $request_url)
	     {            
                
                if ($this ->checkAccessToken($facebookID, $access_token, $request_url))
                {
                        /*$fqlPermissions = "https://api.facebook.com/method/fql.query?query=select%20" . urlencode (implode (', ', $permissions)) . "%20from%20permissions%20where%20uid='" . $facebookID . "'%20&%20access_token='" . $access_token . "'";

                        $XML = simplexml_load_file ($fqlPermissions);
                        $json = json_encode($XML);
                        $arrayPermissions = json_decode($json, TRUE);*/
                        
                        $graph_url = "https://graph.facebook.com/" . $facebookID . "/permissions?" . "access_token=" . $access_token;
                        $response = $this -> curl_get_file_contents($graph_url);
                        $arrayPermissions = json_decode($response, true);
                        
                        foreach ($permissions as $perm)
                        {
                                if (!isset ($arrayPermissions ['data'][0][$perm]) || $arrayPermissions ['data'][0][$perm] != 1)
                                        return false;
                        }
                        
                        return true;
                }
                else 
                {
                        return false;
                }
                
         }
            
        /**
	    * @desc check if access token is valid
	    *
        * @param 
        *  
	    * @return boolean
	    */
         public function checkAccessToken ($facebookID, $access_token, $request_url)
	     {        
                $graph_url = "https://graph.facebook.com/" . $facebookID . "?" . "access_token=" . $access_token;
                $response = $this -> curl_get_file_contents($graph_url);
                $decoded_response = json_decode($response);

                //Check for errors 
                if (isset($decoded_response->error)) 
                {
                // check to see if this is an oAuth error:
                    if ($decoded_response->error->type== "OAuthException") 
                    {
                            // Retrieving a valid access token. 
                            return false;
                            $dialog_url= "https://www.facebook.com/dialog/oauth?" . "client_id=" . $app_id . "&redirect_uri=" . urlencode($request_url);
                            header('Location: ' . $dialog_url);
                    }
                    else 
                    {
                            return false;
                    }
                }
                else 
                {
                        return true;
                }

                return true;
         }
         
         

        public function curl_get_file_contents($URL) {
                $c = curl_init();
                curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($c, CURLOPT_URL, $URL);
                $contents = curl_exec($c);
                $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
                curl_close($c);
                if ($contents) return $contents;
                else return FALSE;
        }
        
        
}   
?>
