<?php   

/**
    @description: OpenIdUser class 
    @link: http://www.inforitus.nl
    @author Andrei Dragos <andrei@inforitus.nl>
**/
    
class OpenIdUser
{
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * A simple array containing all data which is relevant for this user. It will
        * be set in the constructor if we're logged in, or, for low-traffic websites,
        * once for every request to ensure the data is up-to-date.
        *
        * @var array
        */
        
        private $m_aUserInfo;
        
        /**
        * @desc constructor
        *
        */
        
	    public function OpenIdUser () 
        { 
            
        } 
        
        /**
        * Getting all user information, for debug-layout rendering for example,
        * can be done by using this mtehod. Everything will be properly returned.
        *
        * @return array An array with all relevant user information.
        */
        
        public function getUserInfo ()
        {
                return $this -> m_aUserInfo;
        }
        
        /**
	    * @desc get the last errors
	    *
	    * @return array
	    **/
	    
	    public function getLastError ()
        {
                return $this -> aError;
        }
        
        /**
	    * @desc check the data of a user by adding a user
	    *
	    * @param bool $sControlUser 
        * @param array $aValues - the values that have to be checked
	    * 
        * @return booleaan
	    */
	    
        private function controlByAddAUser ($bControlUser, $aValues = array ())
        {
                if ($bControlUser === true) 
                {
    		            $sQuery = sprintf ("
                                SELECT
                                    *
                                FROM
                                    open_id
                                WHERE
                                    email = '%s'
                                ",
                                Database :: getInstance () -> real_escape_string ($aValues ['contact/email'])
                        );
                        
                        if (($pResult = Database :: getInstance () -> query ($sQuery))===false) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   
                                
                                return false;
                        }
                        else 
                        {
                                if ($pResult -> num_rows > 0 && !empty ($aValues ['contact/email']))
                                {
                                        $this -> aError [] = "De ingevoerde gebruikersnaam is al in gebruik. Kies een andere naam. ";   
                                }    
                        }
                }       
                
		        if (!$this -> aError) 
                {
                        return true;
                }
                
                return false;
        }
        
        /**
	    * @desc add user
	    *
        * @param array $aValues - the values that have to be set
        * 
	    * @return boolean
	    */
	    
	    public function addUser ($aValues = array ())
	    {	
		        if ($this -> controlByAddAUser ($controlUser = true, $aValues)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT
                                INTO
                                    open_id
                                       (
						                email,
						                name,
						                date                 
                                       )      
                                VALUES
                                       (
                                        ?,
                                        ?,
                                        NOW()                 
                                       ) 
                        ");
                        
                        $aValues ['namePerson'] = isset ($aValues ['namePerson']) ? $aValues ['namePerson'] : '';
                        
                        $pStatement -> bind_param ('ss',
				                $aValues ['contact/email'],
				                $aValues ['namePerson']				
                        );
                        
                        if (!$pStatement -> execute ()) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   
                                 
                                return false;
                        }
             	            
			            return true;
                }
            
	            return false;	
        }  
        
        /**
	    * @desc get user(s)
	    *
	    * @param int $iUserId - the id of user
	    * @param string $sSortField - the user are sorted by this field
        * @param string $sSortType - desc/asc
	    * 
        * @return array
	    */
        
	    public function getUsers ($iUserId = 0, $sSortField = 'id', $sSortType = 'desc') 
        {                     
                if (!is_numeric ($iUserId))
                { 
                        return false;
                }
                
                $sQuery = sprintf ("
                        SELECT
                            *,
				            DATE_FORMAT(date, '%%d-%%m-%%Y') AS date_format
                        FROM
                            open_id
			            %s
			            ORDER BY
				            %s %s
                        ",
                        (!empty ($iUserId)) ? sprintf ("WHERE id = %d", $iUserId) : '',
                         Database :: getInstance () -> real_escape_string ($sSortField),
                         Database :: getInstance () -> real_escape_string ($sSortType)
                );	
                
		        if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
                
                $aResults = (!empty ($iUserId)) ? current ($aResults) : $aResults;          
                
		        return $aResults;
        }
        
        /**
        * @desc Log out the logged in user to the logged in user is able to log in again
        * if they like. Of course we only process the log-less log out! All cookies,
        * sessions and associated information will be removed.
        * 
        * @return bool
        */
        
        public function logout ()
        {
                unset ($_SESSION ['user_info']);
                
                if (isset ($_SESSION ['session_storage']))
                {
                        unset ($_SESSION ['session_storage']);
                }
                
                setcookie (session_name (), '', time () - 3600, '/', ini_get ('session.cookie_domain'), false, true);
                session_destroy ();
                
                return true ;
        }
        
        /**
	    * @desc function for export the emails in a xls format data
	    * 
	    * @param string $aData
        * 
	    */
	     
	    public function export ($sData)
	    {
		        header ("Pragma: public");
		        header ("Expires: 0");
		        header ("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		        header ("Cache-Control: private", false);
		        header ("Content-Type: application/vnd.ms-excel");
		        header ("Content-Disposition: attachment; fileName=gebruikers.xls;" );
		        //header("Content-Transfer-Encoding: binary");
		        header ("Content-Transfer-Encoding: base64");
		        header ("Content-Length: " . strlen ($data));
		        echo $sData;
		        exit;
	    }	
}
?>
