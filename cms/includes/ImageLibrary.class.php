<?php
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php';   
/**
    $file /inc/imageLibrary.class.php
    @description: Image library
    @author Andrei Dragos <andrei@inforitus.nl>
    @link: http://www.inforitus.nl
    @version: 1.0
    @date: 2006-01-22 00:36
**/
    
class ImageLibrary
{

        /**
        * @desc the extension of file
        * 
        * @var string
        */
        
        var $sExtensie;
        
        /**
        * @desc MIME type of the image
        * 
        * @var string
        */
        
        var $sMimeType;
        
        /**
        * @desc the location of the image
        * 
        * @var string
        */
        
        var $sLocation;
        
        /**
        * @desc the name of the image
        * 
        * @var string
        */
        
        var $sImageName = 1;
        
        /**
        * @desc the name of the thumnail
        * 
        * @var string
        */
        
        var $sThumbnailUrl;
        
        /**
        * @desc origional width 
        * 
        * @var integer
        */
        
        var $iOrig_Width;
        
        /**
        * @desc origional height
        * 
        * @var int
        */
        
        var $iOrig_Height;
        
        /**
        * @desc new width
        * 
        * @var int
        */
        
        var $iNew_Width;
        
        /**
        * @desc new height
        * 
        * @var int
        */
        
        var $iNew_Height;
        
        /**
        * @desc new location
        * 
        * @var string
        */
        
        var $sNew_Location;
        
        /**
        * @desc image
        */
        
        var $ResizedImage;
        
        /**
        * @desc the mime types allowed
        */
        
        var $_mimeIndex = array (
                1 => 'image/gif',
                2 => 'image/jpeg',
                3 => 'image/png',
                4 => 'application/x-shockwave-flash',
                5 => 'image/psd',
                6 => 'image/bmp',
                7 => 'image/tiff',
                8 => 'image/tiff',
                9 => 'image/jpc',
                10 => 'image/jp2',
                11 => 'image/jpx',
                14 => 'video/x-iff'
        );
        
        /**
        * @desc the extension that are allowed
        */
        
        var $_extIndex = array (
                'image/gif' => '.gif',
                'image/jpeg' => '.jpg',
                'image/pjpeg' => '.jpg',
                'image/png' => '.png',
                'application/x-shockwave-flash' => 'swf',
                'image/psd' => '.psd',
                'image/bmp' => '.bmp',
                'image/tiff' => '.tif',
                'image/jpc' => '.jpc',
                'image/jp2' => '.jp2',
                'image/jpx' => '.jpx',
                'video/x-iff' => '.iff'
        );

        /**
        * @desc image
        */
        
        var $OrginalImage;
        
        /**
        * @desc constructor
        */
        
        public function ImageLibrary () {}

        /**
        * @desc fupload a image
        *
        * @param string $sLocation 
        * @param int $iItemOid - the id of item   
        * @param bool $bRewriteImage - resize or not the image
        * 
        * @return booleaan
        */
        
        public function uploadImage ($sLocation, $iItemOid = 0, $bRewriteImage = true)
        {
                $this -> sLocation = $sLocation;                      
            
                $this -> sImageName = $iItemOid;
                
                if (preg_match ('#\.([^\.]+)$#', $_FILES ['image']['name'], $matches))
                {
                        $this -> sExtensie = strtolower ($matches [0]);
                }
                
                // unique name for each file
                
                if ($bRewriteImage) 
                {
                        while (file_exists ($this -> sLocation . "/" . $this -> sImageName . $this -> sExtensie)) 
                        {
                                $this -> sImageName = $this -> sImageName + 1;
                        }
                }
                
                if (!move_uploaded_file ($_FILES ['image']['tmp_name'], $this -> sLocation . "/" . $this -> sImageName . $this -> sExtensie )) 
                {
                        return false;
                }
                
                // set the height and width of the orignal image
                
                $aImageOptions = getimagesize ($this -> sLocation . "/" . $this -> sImageName . $this -> sExtensie);
                
                $this -> iOrig_height = $aImageOptions [1];
                $this -> iOrig_width  = $aImageOptions [0];
                $this -> sMimeType    = $aImageOptions ['mime'];
                
                chmod ($this -> sLocation. "/" . $this -> sImageName . $this -> sExtensie, 0777);
                
                if (!$this -> createNewImageFromFile ($this -> sMimeType, $this -> sLocation . "/" . $this -> sImageName . $this -> sExtensie))
                {
                        return false;
                }
                
                return true;
        }   

        /**
        * @desc set the mimeType of the image
        * 
        * @param string mimeType
        * 
        * @return booleaan
        */
                                
        public function setMimeType ($sMimetype)
        {
                return $this -> sMimeType = $sMimetype;
        }

        /**
        * @desc Create an new image from file or url
        * 
        * @param string $sMimeType
        * @param string $sLocation
        * 
        * @return boolean 
        */
         
        public function createNewImageFromFile ($sMimeType, $sLocation)
        {
                if (empty ($sMimeType) || empty ($sLocation))
                {
                        return false;
                }
                
                if (preg_match ('#(jpeg|png|gif|png)$#i', $sMimeType, $matches)) 
                {
                        $sFunctionName = sprintf ('imagecreatefrom%s', $matches [1]);
                        
                        $this -> OrginalImage = $sFunctionName ($sLocation);       
                             
                        return true;
                }

        }

        /**
        * @desc stipuldate the width and height of the uploaded image
        *
        * @param int $iWidth  ( width of the image)
        * @param int $iHeight  ( height of the image)
        * @param array $aOrigPhotoOptions - options of original image
        * 
        */
        
        public function stipulateWidthHeightImage ($iWidth, $iHeight, $aOrigPhotoOptions = array ())
        {
                if (empty ($aOrigPhotoOptions) || !is_array ($aOrigPhotoOptions)) 
                {
                        $this -> iOrig_width  = imagesx ($this -> OrginalImage);
                        $this -> iOrig_height = imagesy ($this -> OrginalImage);
                } 
                else 
                {
                        $this -> iOrig_width  = $aOrigPhotoOptions ['width'];
                        $this -> iOrig_height = $aOrigPhotoOptions ['height'];
                }
                
                if (!empty ($iWidth) && !empty ($iHeight)) 
                {
                        $x_ratio = $iWidth / $this -> iOrig_width;
                        $y_ratio = $iHeight / $this -> iOrig_height;
            
                        if (($this -> iOrig_width <= $iWidth) && ($this -> iOrig_height <= $iHeight)) {
                            $this -> iNew_Width = $this -> iOrig_width;
                            $this -> iNew_Height = $this -> iOrig_height;
                        } elseif (($x_ratio * $this -> iOrig_height) < $iHeight) {
                            $this -> iNew_Height = ceil($x_ratio * $this -> iOrig_height);
                            $this -> iNew_Width = $iWidth;
                        } else {
                            $this -> iNew_Width = ceil($y_ratio * $this -> iOrig_width);
                            $this -> iNew_Height = $iHeight;
                        }
                }
                
                
                
                /*if (!empty ($iWidth) && empty ($iHeight)) 
                {
                        if ($iWidth < $this -> iOrig_width) 
                        {
                                $dRaport = $this -> iOrig_width / $iWidth; 
                                $this -> iNew_Width  = $iWidth;
                                $this -> iNew_Height = round ($this -> iOrig_height / $dRaport);
                        }
                        else 
                        {
                                $this -> iNew_Width  = $this -> iOrig_width;
                                $this -> iNew_Height = $this -> iOrig_height;
                        } 
                } 
                else 
                {
                        if ($this -> iOrig_width > $iWidth || $this -> iOrig_height > $iHeight) 
                        { 
                                if (($this -> iOrig_width > $this -> iOrig_height) && $this -> iOrig_height < $iHeight)
                                { 
                                        $dRaport = $this -> iOrig_width / $iWidth; 
                                        $this -> iNew_Width  = $iWidth; 
                                        $this -> iNew_Height = round ($this -> iOrig_height / $dRaport);
                                } 
                                else 
                                { 
                                        $dRaport = $this -> iOrig_height / $iHeight; 
                                        $this -> iNew_Height = $iHeight; 
                                        $this -> iNew_Width  = round ($this -> iOrig_width / $dRaport);
                                } 
                        } 
                        else 
                        { 
                                $this -> iNew_Width  = $this -> iOrig_width; 
                                $this -> iNew_Height = $this -> iOrig_height; 
                        } 
                }*/
        }

        /**
        * @desc reduce a image
        *
        * @param int $iWidth  ( width of the image)
        * @param int $iHeight  (height of the image)
        * @param string $sSize Optional
        * @param string $sThumbnail Optional    
        * @param bool $bSetSizes 
        * @param bool $bReturnImage    
        * 
        * @return booleaan
        */
        
        public function reduceImage ($iWidth, $iHeight, $sSize = '', $sThumbnail = '', $bSetSizes = true, $bReturnImage = false)
        {
            if ($bSetSizes === true)
            { 
                    $this -> stipulateWidthHeightImage ($iWidth, $iHeight);
            }
            
            if ($this -> sMimeType == 'image/gif') 
            {
                    $this -> ResizedImage = imagecreate ($this -> iNew_Width, $this -> iNew_Height);
            } 
            else 
            {
                    $this -> ResizedImage = imagecreatetruecolor ($this -> iNew_Width, $this -> iNew_Height);
                    if ($this -> sMimeType == 'image/png') 
                    {
                            $text_color = imagecolorallocate($this -> ResizedImage, 255, 255, 255);
                            //imagecolortransparent($stamp, $text_color);
                            imagefill ($this -> ResizedImage, 0, 0, $text_color);
                    }
            }

            imagesavealpha ($this -> OrginalImage, true);
            imagealphablending ($this -> OrginalImage, false);
            
            if (imagecopyresampled ($this -> ResizedImage, $this -> OrginalImage, 0, 0, 0, 0, $this -> iNew_Width, $this -> iNew_Height, $this -> iOrig_width, $this -> iOrig_height)) 
            {
                    $this -> sNew_Location = (!empty ($sThumbnail)) ? $sThumbnail : $this -> sLocation . "/" . $this -> sImageName . $sSize . $this -> sExtensie;
                    $handle = fopen ($this -> sNew_Location, 'w+');
                    ob_start ();        
                    
                    if ($this -> sMimeType == 'image/gif' )
                    {
                            imagegif ($this -> ResizedImage, $this -> sNew_Location, 100); // data, location, quality
                    } 
                    elseif ($this -> sMimeType == 'image/jpeg' || $this -> sMimeType == 'image/pjpeg')
                    {
                            imagejpeg ($this -> ResizedImage, $this -> sNew_Location, 100); 
                    } 
                    elseif ($this -> sMimeType == 'image/png')
                    {
                            
                            imagepng ($this -> ResizedImage, $this -> sNew_Location, 9); 
                    } 
                    elseif ($this -> sMimeType == 'image/bmp') 
                    {
                            imagewbmp ($this -> ResizedImage, $this -> sNew_Location, 100);
                    }
                    
                    fwrite ($handle, ob_get_contents ());
                    fclose ($handle);
                    ob_end_clean ();
                    chmod ($this -> sNew_Location, 0777);
                    
                    return true;
            }
            
            return false;
        }

        /**
        * @desc create a thumbnail of the uploaded image
        *
        * @param array $aValues
        * 
        * @return booleaan
        */
        
        public function reduceAndCreateThumbnail ($aValues)
        {
                if (is_array ($aValues)) 
                {    
                        foreach (array_values ($aValues) as $item) 
                        {
                                $sThumbnailLocation = $item ['location'] . "/" . $this -> sImageName . $item ['name'] . $this -> sExtensie;
                                
                                if (!$this -> reduceImage ($item ['width'], $item ['height'], '', $sThumbnailLocation))
                                {
                                        return false;
                                }
                                
                                $aNewDataOfImages [] = array (
                                        'name'               => preg_replace ('#.*/([^/]+)$#','${1}', $this -> sNew_Location),
                                        'extensie'           => $this -> sExtensie,
                                        'width'              => $this -> iNew_Width,
                                        'orig_width'         => $this -> iNew_Width,
                                        'height'             => $this -> iNew_Height,
                                        'orig_height'        => $this -> iNew_Height,
                                        'mime_type'          => $this -> sMimeType,
                                        'location'           => preg_replace (sprintf ('#%s(.*)$#', $GLOBALS ['cfg']['include']), '${1}', $this -> sLocation),
                                        'oid'                => $this -> sImageName
                                );
                        }
                        
                        return $aNewDataOfImages;
                }
                
                return false;
        }
        
        /**
        * @desc Crop the image
        * 
        * @param array $aValues 
        * @param array $aImageOpts 
        * @param array $aImageValues 
        * 
        * @return array or false by error
        */
                                         
        public function cropImage ($aValues, $aImageOpts, $aImageValues = array ()) 
        {
                $i = 0;
                
                foreach ($aImageOpts as $sizes) 
                {
                        if (isset ($sizes ['upload']))
                        {
                                $source = $this -> OrginalImage;
                                $tn_w = $sizes ['width'];
                                $tn_h = $sizes ['height'];
                                $file = $this -> sLocation . "/" . $this -> sImageName . "_" . (!isset ($sizes ['name']) || (isset ($sizes ['name']) && $sizes ['name'] == '') ? ($sizes ['width']) . 'x' . ($sizes ['height']) : $sizes ['name']) . $this -> sExtensie;
                                $imageFullPath = $GLOBALS ['cfg']['include'] . $aValues ['location'] . '/' . $aValues ['oid'] . '_' . (!isset ($sizes ['name']) || (isset ($sizes ['name']) && $sizes ['name'] == '') ? ($sizes ['width']) . 'x' . ($sizes ['height']) : $sizes ['name']) . $aValues ['extensie'];
                                
                                list($src_w, $src_h, $type, $attr) = getimagesize ($this -> sLocation . "/" . $this -> sImageName . $this -> sExtensie);
                                
                                preg_match ('#(jpg|png|gif|jpeg|JPG)$#i', $this -> sExtensie, $matches);
                                
                                $x_ratio = $tn_w / $src_w;
                                $y_ratio = $tn_h / $src_h;
                    
                                if (($src_w <= $tn_w) && ($src_h <= $tn_h)) 
                                {
                                    $new_w = $src_w;
                                    $new_h = $src_h;
                                } 
                                elseif (($x_ratio * $src_h) < $tn_h) 
                                {
                                    $new_h = ceil ($x_ratio * $src_h);
                                    $new_w = $tn_w;
                                } 
                                else 
                                {
                                    $new_w = ceil ($y_ratio * $src_w);
                                    $new_h = $tn_h;
                                }
                                
                                $newpic = imagecreatetruecolor( round ($new_w), round ($new_h));

                                if ($this -> sExtensie == '.png')
                                {
                                        $white = ImageColorAllocate ($newpic, 255, 255, 255);
                                        imagefill ($newpic, 0, 0, $white);
                                }

                                imagecopyresampled ($newpic, $source, 0, 0, $sizes ['x1'], $sizes ['y1'], $new_w, $new_h, ($sizes ['x2']-$sizes ['x1']), ($sizes ['y2']-$sizes ['y1']));
                                
                                if (!isset ($sizes ['name']) || (isset ($sizes ['name']) && $sizes ['name'] == ''))
                                {
                                        $final = imagecreatetruecolor ($tn_w, $tn_h);
                                        $backgroundColor = imagecolorallocate ($final, 237, 237, 237);
                                        imagefill ($final, 0, 0, $backgroundColor);
                                        
                                        if ($tn_w > $new_w && $tn_h > $new_h)
                                                imagecopy ($final, $newpic, (($tn_w - $new_w)/ 2), (($tn_h - $new_h) / 2), 0, 0, $new_w, $new_h);
                                        else
                                                imagecopy ($final, $newpic, (($tn_w - $new_w)/ 2), (($tn_h - $new_h) / 2), 0, 0, $new_w, $new_h);        
                                }
                                //imagecopyresampled($final, $newpic, 0, 0, ($x_mid - ($tn_w / 2)), ($y_mid - ($tn_h / 2)), $tn_w, $tn_h, $tn_w, $tn_h);
                                
                                $sFunctionName = sprintf ('image%s', ($matches [1] == 'jpg' || $matches [1] == 'JPG' ? 'jpeg' : $matches [1]));
                                
                                if ($this -> sExtensie == '.png')
                                        $sFunctionName ((!isset ($sizes ['name']) || (isset ($sizes ['name']) && $sizes ['name'] == '') ? $final : $newpic), $file, 9);
                                else
                                        $sFunctionName ((!isset ($sizes ['name']) || (isset ($sizes ['name']) && $sizes ['name'] == '') ? $final : $newpic), $file, 100);
                        }
                        else
                        {
                                if (!$this -> createNewImageFromFile ($aValues ['mime_type'], $GLOBALS ['cfg']['include'] . $aValues ['location'] . '/' . $aValues ['name']))
                                {
                                        return false;
                                }
                                
                                $thumbImage = imagecreatetruecolor ($sizes ['width'], $sizes ['height']); // create thumbnail image
                                
                                if (!imagecopyresampled ($thumbImage, $this -> OrginalImage, 0, 0, $sizes ['x1'], $sizes ['y1'], $sizes ['width'], $sizes ['height'], ($sizes ['x2']-$sizes ['x1']), ($sizes ['y2']-$sizes ['y1'])))
                                {
                                        return false;
                                }
                                
                                imagedestroy ($this -> OrginalImage); // destroy image resource
                                
                                $imageFullPath = $GLOBALS ['cfg']['include'] . $aValues ['location'] . '/' . $aValues ['oid'] . '_' . ($sizes ['width']) . 'x' . ($sizes ['height']) . $aValues ['extensie'];
                                
                                if (!imagejpeg ($thumbImage, $imageFullPath, 100))
                                {
                                        return false;
                                }
                                
                                imagedestroy ($thumbImage); // destroy thumbnail resource
                                chmod ($imageFullPath, 0777);
                        }
                        
                        if (empty ($i))
                        {
                                $aImageValues [] = array ('width' => ($sizes ['width']), 'height' => ($sizes ['height']), 'mime_type' => $aValues ['mime_type'],'path' => $imageFullPath);
                        }
                        
                        $i++;
                }
                
                return $aImageValues;
        }
        
        /**
        * @desc insert the image data into the database
        * 
        * @param array $aValues
        * 
        * @return booleaan     
        */
                                                     
        public function updateImage ($aValues, $iOid = 0)
        {
                if (empty ($aValues) || !is_array ($aValues))
                {
                        return false;
                }
                
                require_once 'Database.class.php';
                
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            images
                        SET
                            image_name         = ?,
                            image_ext          = ?,
                            orig_image_width   = ?,
                            orig_image_height  = ?,
                            image_width        = ?,
                            image_height       = ?,
                            mime_type          = ?,
                            type               = ?,
                            date = NOW()
                        WHERE
                            id                 = ?
                        
                ");
                
                $sImageName        = preg_replace (sprintf ('#%s(.*)$#',$GLOBALS ['cfg']['include']), '${1}',$aValues ['path']);
                $sImageExt         = (empty ($iOid) ? $aValues ['extensie'] : $aValues ['image_ext']);      
                $sOrigImageWidth   = (empty ($iOid) ? $aValues ['orig_width'] : $aValues ['orig_image_width']);
                $sOrigImageHeight  = (empty ($iOid) ? $aValues ['orig_height'] : $aValues ['orig_image_height']); 
                $iId               = (!empty ($iOid) ? $iOid : $aValues ['oid']);
                
                $pStatement -> bind_param ('ssssssssi',
                        $sImageName,
                        $sImageExt,
                        $sOrigImageWidth,
                        $sOrigImageHeight,
                        $aValues ['width'],
                        $aValues ['height'],
                        $aValues ['mime_type'],
                        $aValues ['type'],
                        $iId
                );
                
                if (!$pStatement -> execute ()) 
                {   
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   
                        
                        return false;
                }
                
                return true;
        }
        
        
        /**
        * @desc show the specified image    
        * 
        * @param int $iQuality
        * @param bool $bOriginalImage
        * 
        * @return ouput of imageCreateFunctions
        */
                                 
        public function show ($iQuality = 0, $bOrginalImage = false)
        {
                if (empty ($iQuality))
                {
                        $iQuality = 100;
                }
                
                if (headers_sent ())
                {
                        return false;
                }
                
                header (sprintf ('Content-type:', $this -> sMimeType));
                header ("Content-type: image/png");
                header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
                header ("Last-Modified: " . gmdate ("D, d M Y H:i:s") . " GMT");
                header ("Cache-Control: no-store, no-cache, must-revalidate");
                header ("Cache-Control: post-check=0, pre-check=0", false);
                header ("Pragma: no-cache");
                
                if ($this -> sMimeType == 'image/jpeg' || $this -> sMimeType == 'image/pjpeg') 
                {
                        $result = imagejpeg (($bOrginalImage) ? $this -> OrginalImage : $this -> ResizedImage, '', $iQuality);
                } 
                elseif ($this -> sMimeType == 'image/png') 
                {
                        $result = imagepng (($bOrginalImage) ? $this -> OrginalImage : $this -> ResizedImage, '', $iQuality);
                } 
                elseif ($this -> sMimeType == 'image/gif') 
                {
                        $result = imagegif (($bOrginalImage) ? $this -> OrginalImage : $this -> ResizedImage, '', $iQuality);
                } 
                elseif ($this -> sMimeType == 'image/bmp') 
                {
                        $result = imagewbmp (($bOrginalImage) ? $this -> OrginalImage : $this -> ResizedImage, '', $iQuality);
                } 
                
                return $result;
        }
}
    
?>
