<?php     

/**
     @description: Webshop class 
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class Webshop extends Tree  
{ 
    
        /**
        * @desc the tree id specific to pages
        * 
        * @var integer
        */
        
        var $iTreeId = 1;
        
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * @desc: contructor
        *
        */
                      
        public function Webshop ()
        { 
                parent::Tree ($this -> iTreeId);
            
                $this -> InitData ();   
        }
        
        /**
        * @desc get the last errors
        *
        * @return array
        */
         
        public function getLastError ()
        {
                return $this -> aError;
        }       
            
        /**
        * @desc check the data of form
        * 
        * @param array $aValues - values of form
        * 
        * @return booleaan
        */
                                 
        private function checkForm ($aValues) 
        {
                if (empty ($aValues ['title'][$this -> aDefaultLang ['id']])) 
                {
                        $this -> aError [] = 'Product naam';
                }

                if (empty ($aValues ['description'][$this -> aDefaultLang ['id']])) 
                {
                        $this -> aError [] = 'Omschrijving';
                }

                if (empty ($aValues ['category'])) 
                {
                        $this -> aError [] = 'Categorie';
                }

                if (!empty ($_POST ['price']))
                {
                        if (!is_numeric ($aValues ['price']))
                        {
                                $this -> aError [] = 'Prijs';
                        }
                        elseif ($aValues ['price'] != (int) $aValues ['price'])
                        {
                                $this -> aError [] = 'Prijs';
                        }
                }

                if (!$this -> aError) 
                {
                        return true;
                }

                return false;
        }
        
        /**
        * @desc add a product
        * 
        * @param array $aValues - the data that have to be set for product
        * 
        * @return booleaan
        */
                                 
        public function addProduct ($aValues = array ())
        {
                if ($this -> checkForm ($_POST)) 
                {
                        // take the item with the biggest sort number
                        $aWebshopItems = $this -> getProducts ();
                        $aWebshopItem  = (isset ($aWebshopItems [0]) ? $aWebshopItems [0] : '');
                        
                        // add post item
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT
                                INTO
                                    webshop_products
                                    (
                                        category_id,
                                        price,
                                        date,
                                        order_number
                                    )
                                VALUES
                                    (
                                        ?,
                                        ?,
                                        NOW(),
                                        ?
                                    )
                        ");
                        
                        $iPrice       = ($aValues ['price'] != '' ? $aValues ['price'] : 0);
                        $iOrderNumber = (isset ($aWebshopItem ['order_number']) ? $aWebshopItem ['order_number'] + 1 : 0);
                        
                        $pStatement -> bind_param ('iii',
                                $aValues ['category'],
                                $iPrice,
                                $iOrderNumber        
                        );    
                                  
                        if (!$pStatement -> execute ()) 
                        {   
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                
                                return false;
                        }
                        
                        $iWebshopProductId = Database :: getInstance () -> insert_id;
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                                $pStatement = Database :: getInstance () -> prepare ("
                                        INSERT
                                        INTO
                                            webshop_products_data
                                            (
                                                webshop_products_id,
                                                lang_id,
                                                title,
                                                description,
                                                photos,
                                                page_description,
                                                slug
                                            )
                                        VALUES
                                            (
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?
                                            )
                                ");
                                
                                $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]);
                                $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                
                                $pStatement -> bind_param ('iisssss',
                                        $iWebshopProductId,
                                        $lang ['id'],
                                        $aValues ['title'][$lang ['id']],
                                        $aValues ['description'][$lang ['id']],
                                        $sPhotos,
                                        $aValues ['page_description'][$lang ['id']],
                                        $sSlug        
                                );    
                                          
                                if (!$pStatement -> execute ()) 
                                {   
                                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                        
                                        return false;
                                }
                        }
                        
                        return true;
                }
                
                return false;
        }
        
        /**
        * @desc This method insert the order in database 
        * 
        * @param array $aValues - the values of order
        * 
        * @return boolean 
        */
     
        public function insertOrder ($aValues = array ())
        {
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT INTO
                            command
                        (id_client, id_product, quantity, price, date)
                        VALUES
                            (?, ?, ?, ?, NOW())"
                );           
                
                $pStatement -> bind_param ('iiii',
                        $aValues ['id_client'],
                        $aValues ['id_product'],
                        $aValues ['quantity'],
                        $aValues ['price']
                );    
                
                if (!$pStatement -> execute ()) 
                {   
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);      
                        
                        return false;
                }
                
                return true;
        }
         
        /**
        * @desc: get product/products
        * 
        * @param int $iItemId - the id of the item
        * @param string $sItemSlug - the slug of the item
        * @param int $iCategoryId - the id of a category of products
        * @param string $sCategorySlug - the slug of a category
        * @param string $sSortField - the results will be sorted by this field
        * @param string $sSortType - desc/asc
        * 
        * @return array OR false by error
        */
                                       
        public function getProducts ($iItemId = 0, $sItemSlug = '', $iCategoryId = 0, $sCategorySlug = '', $sSortField = 'order_number', $sSortType = 'desc', $sLanguage = 'default')
        {
                if (!is_numeric ($iItemId))
                { 
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT
                            w.*,
                            wd.title,
                            wd.description,
                            wd.photos,
                            wd.page_description,
                            wd.slug,
                            DATE_FORMAT(w.date,'%d-%m-%Y om %H:%i') AS date_format,
                            cat.name                                AS category_name,
                            t.oid                                   AS cat_id,
                            (SELECT COUNT(*) FROM webshop_products_data WHERE webshop_products_id = w.id AND title != '' AND description != '') as filled  
                        FROM
                            webshop_products AS w
                        INNER JOIN
                            tree AS t
                                ON  
                            w.category_id = t.oid
                        LEFT JOIN
                            pages cat
                                ON
                            cat.tree_oid = t.oid
                        LEFT JOIN
                            webshop_products_data wd
                                ON
                            wd.webshop_products_id = w.id
                        LEFT JOIN
                            languages l
                                ON
                            (wd.lang_id = l.id AND l.id = cat.lang_id) 
                        WHERE  
                            1
                            " . ($sLanguage == 'default' ? sprintf ("AND l.default = 1") : sprintf ("AND l.short_name = '%s'", Database :: getInstance () -> real_escape_string ($sLanguage))) . "        
                            " . (!empty ($iItemId)       ? sprintf ("AND w.id = %d", $iItemId) : '') . "
                            " . (!empty ($sItemSlug)     ? sprintf ("AND wd.slug = '%s'", Database :: getInstance () -> real_escape_string ($sItemSlug)) : '') . "
                            " . (!empty ($iCategoryId)   ? sprintf ("AND w.category_id = %d", $iCategoryId) : '') . "
                            " . (!empty ($sCategorySlug) ? sprintf ("AND cat.slug = '%s'", Database :: getInstance () -> real_escape_string ($sCategorySlug)) : '') . "
                        ORDER BY
                            " . $sSortField . " " . $sSortType . "
                ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                $aRow ['photos']      = $this -> getPhotosByIds ($aRow ['photos']);
                                $aRow ['description'] = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                                
                                if (preg_match_all ('%<a[^>]+>%s', $aRow ['description'], $aMatch))
                                {
                                        $aLinks = $aMatch [0];
                                        
                                        foreach ($aLinks as $link)
                                        {
                                                if (strpos ($link, 'onclick="') === false && 
                                                strpos ($link, 'target="_blank"') !== false && 
                                                preg_match ('%href="[^"]+"%s', $link, $matches))
                                                {
                                                        $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                        $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['title'] . '\', \'' . $sAddress . '\')"', $link);
                                                        $aRow ['description'] = str_replace ($link, $sNewlink, $aRow ['description']); 
                                                }
                                        }
                                        
                                        
                                }
                                
                                $aResults []          = $aRow;
                        }
                }
                
                foreach ($aResults as $k => $value)
                {
                        if (count ($this -> aLanguages) > 1)
                        {
                                foreach ($this -> aLanguages as $lang)
                                {
                                        $pResult = Database :: getInstance () -> query ("
                                                SELECT
                                                    w.*,
                                                    wd.title,
                                                    wd.description,
                                                    wd.photos,
                                                    wd.page_description,
                                                    wd.slug,
                                                    DATE_FORMAT(w.date,'%d-%m-%Y om %H:%i') AS date_format,
                                                    cat.name                                AS category_name,
                                                    t.oid                                   AS cat_id  
                                                FROM
                                                    webshop_products AS w
                                                INNER JOIN
                                                    tree AS t
                                                        ON  
                                                    w.category_id = t.oid
                                                LEFT JOIN
                                                    pages cat
                                                        ON
                                                    cat.tree_oid = t.oid
                                                LEFT JOIN
                                                    webshop_products_data wd
                                                        ON
                                                    wd.webshop_products_id = w.id
                                                LEFT JOIN
                                                    languages l
                                                        ON
                                                    (wd.lang_id = l.id AND l.id = cat.lang_id) 
                                                WHERE  
                                                    l.id = " . $lang ['id'] . "
                                                AND
                                                    w.id = " . $value ['id'] . "
                                        ");
                                        
                                        if (!$pResult)
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                                
                                                return false;
                                        }  
                                          
                                        if ($pResult -> num_rows > 0)
                                        {         
                                                while ($aRow = $pResult -> fetch_assoc ()) 
                                                {          
                                                        if ($aRow ['title'] != '' || $aRow ['description'] != '')
                                                                $aRow ['filled'] = true;
                                                        
                                                        $aRow ['photos']             = $this -> getPhotosByIds ($aRow ['photos']);        
                                                        $aRow ['description']        = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                                                
                                                        if (preg_match_all ('%<a[^>]+>%s', $aRow ['description'], $aMatch))
                                                        {
                                                                $aLinks = $aMatch [0];
                                                                
                                                                foreach ($aLinks as $link)
                                                                {
                                                                        if (strpos ($link, 'onclick="') === false && 
                                                                        strpos ($link, 'target="_blank"') !== false && 
                                                                        preg_match ('%href="[^"]+"%s', $link, $matches))
                                                                        {
                                                                                $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                                                $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['title'] . '\', \'' . $sAddress . '\')"', $link);
                                                                                $aRow ['description'] = str_replace ($link, $sNewlink, $aRow ['description']); 
                                                                        }
                                                                }
                                                        }
                                                
                                                        $aResults [$k][$lang ['id']] = $aRow;
                                                }
                                        }       
                                }
                        }
                        else
                        {
                                unset ($value ['filled']);
                                if ($value ['title'] != '' || $value ['description'] != '')
                                        $value ['filled'] = true;
                                        
                                $aResults [$k][$this -> aDefaultLang ['id']] = $value;
                        }
                }
                
                $aResults = (!empty ($iItemId) || !empty ($sItemSlug)) ? current ($aResults) : $aResults;
                
                return $aResults;
        }
	    
        /**
        * @desc change the specified product 
        * 
        * @param int $iId - the id of the product
        * @param array $aValues - the data that have to be set
        * 
        * @return booleaan
        */
                                       
        public function updateProduct ($iId, $aValues = array ()) 
        {
                if (!is_numeric ($iId))
                { 
			            return false;
                }
                    
                $oTree = new Tree (1); 
                      
                if ($this -> checkForm ($_POST)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    webshop_products
                                SET
                                    category_id = ?,
                                    price       = ?
                                WHERE
                                    id          = ?
                        ");
                        
                        $iPrice = (empty ($aValues ['price'])  ? 0 : $aValues ['price']);
                        
                        $pStatement -> bind_param ('iii',
                                $aValues ['category'],
                                $iPrice,
                                $iId
                        );
                        
                        if (!$pStatement -> execute ()) 
                        {   
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                
                                return false;
                        }
                        
                        $aLangValues = $this -> getProducts ($iId, '', 0, '', 'order_number', 'desc');
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                                if (isset ($aLangValues [$lang ['id']]))
                                {
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                UPDATE
                                                    webshop_products_data
                                                SET
                                                    title               = ?,
                                                    description         = ?,
                                                    photos              = ?,
                                                    page_description    = ?,
                                                    slug                = ?
                                                WHERE
                                                    webshop_products_id = ?
                                                AND 
                                                    lang_id             = ?
                                        ");
                                        
                                        $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]);
                                        $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                        
                                        $pStatement -> bind_param ('sssssii',
                                                $aValues ['title'][$lang ['id']],
                                                $aValues ['description'][$lang ['id']],
                                                $sPhotos,
                                                $aValues ['page_description'][$lang ['id']],
                                                $sSlug,       
                                                $iId,
                                                $lang ['id']
                                        );
                                        
                                        if (!$pStatement -> execute ()) 
                                        {   
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                                
                                                return false;
                                        }
                                }
                                else
                                {
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                INSERT
                                                INTO
                                                    webshop_products_data
                                                    (
                                                        webshop_products_id,
                                                        lang_id,
                                                        title,
                                                        description,
                                                        photos,
                                                        page_description,
                                                        slug
                                                    )
                                                VALUES
                                                    (
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?
                                                    )
                                        ");
                                        
                                        $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]);
                                        $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                        
                                        $pStatement -> bind_param ('iisssss',
                                                $iId,
                                                $lang ['id'],
                                                $aValues ['title'][$lang ['id']],
                                                $aValues ['description'][$lang ['id']],
                                                $sPhotos,
                                                $aValues ['page_description'][$lang ['id']],
                                                $sSlug        
                                        );    
                                                  
                                        if (!$pStatement -> execute ()) 
                                        {   
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                                
                                                return false;
                                        }       
                                }
                        }
                                  
                        return true;
                }
                
                return false;
        }
         
        /**
        * @desc: delete a product
        * 
        * @param int $iId - the id of the product
        * 
        * @returns: booleaan
        */
          
	    public function deleteProduct ($iId)
	    {
	            if (!is_numeric ($iId))
                { 
			            return false;
                }
	            
                $pStatement = Database :: getInstance () -> prepare ("
	                    DELETE
	                    FROM
	                        webshop_products
	                    WHERE
	                        id = ?
	            ");
                
                $pStatement -> bind_param ('i',
	                    $iId
	            );
                
	            if (!$pStatement -> execute ()) 
                {   
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                        
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
	                    DELETE
	                    FROM
	                        webshop_products_data
	                    WHERE
	                        webshop_products_id = ?
	            ");
                
                $pStatement -> bind_param ('i',
	                    $iId
	            );
                
	            if (!$pStatement -> execute ()) 
                {   
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                        
                        return false;
                }
                
	            return true;
	    }                             
         
        /**
        * @desc Add a product in the session.
        * 
        * @param int $iId - the id of product
        * @param int $iNumber - the count of product
        * 
        * @return booleaan
        */
                                           
        public function addCustomerProduct ($iItemId, $iNumber)
        {
                if (is_numeric ($iItemId) && is_numeric ($iNumber)) 
                {
                        if (!empty ($_SESSION ['cart'][$iItemId])) 
                        {
                                $_SESSION ['cart'][$iItemId]['count'] += $iNumber;
                        } 
                        else 
                        {
                                $_SESSION ['cart'][$iItemId]['count'] = $iNumber;
                        }
                        
                        return true;
                }
            
                return false;
        }
         
        /**
        * @desc Delete a product from cart by specified id
        * 
        * @param int $iId - the id of product
        * 
        * @return booleaan
        */
                          
        public function dellCustomerProduct ($iId)
        {
                if (!is_numeric ($iId)) 
                {
                        return false;
                }
                
                if (!empty ($_SESSION ['cart'][$iId])) 
                {
                        if ($_SESSION ['cart'][$iId]['count'] >= 2) 
                        {
                                $_SESSION ['cart'][$iId]['count'] = $_SESSION ['cart'][$iId]['count'] - 1;
                        } 
                        else 
                        {
                                unset ($_SESSION ['cart'][$iId]);
                        }
                        
                        return true;
                }  
                
                return false;
        }

        /**
        * @desc Delete the complete cart session
        * 
        * @return boolean
        */
          
        public function dellCart ()
        {
                if (!empty ($_SESSION ['cart'])) 
                {
                        unset ($_SESSION ['cart']);
                        
                        return true;
                }
                
                return false;
        }
}
?>
