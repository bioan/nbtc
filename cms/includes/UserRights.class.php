<?php
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php';      

/**
    @description: Permissions class 
    @link: http://www.inforitus.nl
    @author Andrei Dragos <andrei@inforitus.nl>
**/
    
class UserRights
{
        /**
        * @desc the id of user
        * 
        * @var integer
        */
        
        var $iUserId;
        
        /**
        * @desc constructor
        *
        */
        
	    public function UserRights () 
        { 
                $this -> iUserId = (isset ($_COOKIE ['user_id'])) ? $_COOKIE ['user_id'] : '';
        }  
        
        /**
        * @desc check if user is admin or consumer
        *
        * @return booleaan    
        */
        
        public function getStatusOfUser ()
        {
                $sQuery = sprintf ("
                        SELECT
                            status
                        FROM
                            users
                        WHERE
                            id = %d
                        ",
                        $this -> iUserId
                );
                
                if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                        
    		            return false;
    	        } 
                else 
                {
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                $aResults [] = $pResult -> fetch_assoc ();
                                
                                return $aResults [0]['status']; 
                        }
                        else
                        {
                                return false;
                        }   
                }
                
                return false;
        }
        
        /**
        * @desc Set permissions of the specified user
        * 
        * @param int $iUserId - id of user
        * @param array $aValues - modules
        * 
        * @return booleaan     
        */
                                      
        public function setPermissions ($iUserId, $aValues)
        {
                if (!is_numeric ($iUserId) || !is_array ($aValues))
                {
                        return false;
                }
                
                foreach ($aValues as $permission) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT
                                INTO
                                    permissions
                                    (
                                        user_id,
                                        menu_id,
                                        date
                                    )
                                VALUES
                                    (
                                        ?,
                                        ?,
                                        NOW()
                                    )
                        ");
                        
                        $pStatement -> bind_param ('ii',
                                $iUserId,
                                $permission
                        );
                        
                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   
                                
                                return false;   
                        }
                }
                
                return true;
        }
        
        /**
        * @desc delete the permissions by the specified userId
        * 
        * @param int $iUserId - id of user
        * 
        * @return booleaan     
        */
         
        public function deletePermissions ($iUserId)
        {
                if (!is_numeric ($iUserId))
                { 
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM
                            permissions
                        WHERE
                            user_id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iUserId
                );
                
                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);   
    		            
                        return false;
                }
                
                return true;
        }
        
        /**
        * @desc update the permissions by the specified userId
        * 
        * @param int $iUserId
        * @param array $aValue - modules
        * 
        * @return booleaan     
        */
                                           
        public function updatePermissions ($iUserId, $aValue)
        {
                if (!is_numeric ($iUserId) || !is_array ($aValue))
                { 
                        return false;
                }
                
                if ($this -> deletePermissions ($iUserId)) 
                {
                        return $this -> setPermissions ($iUserId, $aValue); 
                }
        }
        
        /**
        * @desc get the permissions by the specified userId
        * 
        * @param int $iUserId
        * 
        * @returns array OR false by error    
        */

        public function getPermissions ($iUserId)
        {  
            if (!is_numeric ($iUserId))
            {
                    return false;
            }
            
            $sQuery = sprintf ("
                    SELECT
                        menu_id
                    FROM
                        permissions
                    WHERE
                        user_id = %d
                    ",
                    $iUserId
            );
            
            if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
            {
                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
    		        
                    return false;
            }
            else
            {
                    $aResults = array ();   
                  
                    if ($pResult -> num_rows > 0)
                    {
                            while ($aRow = $pResult -> fetch_assoc ()) 
                            {
                                    $aResults [] = $aRow;
                            }
                    }
            }
            
            return $aResults;
        }
         
         
        /**
        * @desc define the rights of the specified user
        * 
        * @param int $iUserId - id of user
        * 
        * @returns array
        */
                                             
        public function defineRightsOfUser ($iUserId)
        {
                if (!is_numeric ($iUserId))
                { 
                        return false;
                }
                
                $sQuery = sprintf ("
                        SELECT
                            menu.id             AS menu_id,
                            menu.check_name
                        FROM
                            admin_menu          AS menu
                                INNER JOIN
                            permissions
                                ON
                                    permissions.menu_id = menu.id 
                                        AND
                                    permissions.user_id = %d
                        ",
                        $iUserId
                );
                
                if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
    		            
                        return false;
                } 
                else 
                {
        	            $aResult = array ();
                        
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                $aResult [$aRow ['menu_id']] = $aRow ['check_name'];
                        }
			            
                        return $aResult;
		        }
                
                return false;
        }
}
?>
