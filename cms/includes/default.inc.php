<?php 
session_start ();

require $_SERVER ['DOCUMENT_ROOT'] . '/cms/fixpath.php'; 

require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Smarty.class.php';
require_once $GLOBALS ['cfg']['include'] . '/Configuration.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/UserRights.class.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/AdminMenu.class.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Functions.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/dump_r.php';

//Include social media files
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Facebook.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/OAuth/oauth.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Twitter.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/twitter/twitteroauth.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/twitter/secret.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Linkedin.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/linkedin/linkedin.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/linkedin/secret.php');

$aImageSizes = array (

        'agenda' => array (
        
                0 => array ('width' => 100, 'height' => 100, 'crop_name' => '100x100', 'display' => false),
                1 => array ('width' => 250, 'height' => 200, 'crop_name' => '250x200', 'display' => true),
                
        ),
        
        'news' => array (
        
                0 => array ('width' => 100, 'height' => 100, 'crop_name' => '100x100', 'display' => false),
                1 => array ('width' => 650, 'height' => 500, 'crop_name' => '650x500', 'display' => true),
                
        ),
        
        'photoalbum' => array (
        
                0 => array ('width' => 100, 'height' => 100, 'crop_name' => '100x100', 'display' => false),
                1 => array ('width' => 400, 'height' => 300, 'crop_name' => '400x300', 'display' => true),
                
        ),
        
        'videoalbum' => array (
        
                0 => array ('width' => 100, 'height' => 100, 'crop_name' => '100x100', 'display' => false),
                1 => array ('width' => 250, 'height' => 200, 'crop_name' => '250x200', 'display' => true),
               
        ),
        
        'webshop' => array (
        
                0 => array ('width' => 100, 'height' => 100, 'crop_name' => '100x100', 'display' => false),
                1 => array ('width' => 250, 'height' => 200, 'crop_name' => '250x200', 'display' => true),
                
        ),
        
        'newsletter' => array (
                39 => array (
                        0 => array ('width' => 100, 'height' => 100, 'crop_name' => '100x100', 'display' => false),
                        1 => array ('width' => 93, 'height' => 95, 'crop_name' => 'logo', 'display' => true),
                        2 => array ('width' => 473, 'height' => 37, 'crop_name' => 'bar', 'display' => true),
                        3 => array ('width' => 182, 'height' => 182, 'crop_name' => 'item', 'display' => true)
                )
        ),
);
   
function checkAuth ()
{
	    if ((isset ($GLOBALS ['cfg']['auth']) && $GLOBALS ['cfg']['auth'] === true)
		    ||
		    (!isset ($GLOBALS ['cfg']['auth'])))
        {
			    return true;
        }
        
	    return false;
}

// smarty config

$oTemplate = new Smarty ();

$oTemplate  ->  template_dir = $GLOBALS ['cfg']['include'] . '/cms/tpls/';
$oTemplate  ->  compile_dir  = $GLOBALS ['cfg']['include'] . '/Cache/admin/';

$GLOBALS ['Template'] =& $oTemplate;

$oUserRights = new UserRights ();
$oMenu       = new AdminMenu ();
 
/* get the secure pages */

foreach ((array) $oMenu -> getMenu () as $menu) 
{
        $aStrictedPages [] =  $menu ['check_name'];
}


/* get the pages where the specified user have the rights for */

if (isset ($_COOKIE ['user_id']) && !empty ($_COOKIE ['user_id']) && is_numeric ($_COOKIE ['user_id'])) 
{
        $aStrictedPages =& $oUserRights -> defineRightsOfUser ($_COOKIE ['user_id']);
        ksort ($aStrictedPages);
        $oTemplate -> assign ('securePages', $oUserRights -> defineRightsOfUser ($_COOKIE ['user_id']));
	    $aStrictedPages [] = 'authorization';    
	    $aStrictedPages [] = 'browser';    
	    $aStrictedPages [] = 'install';
        $aStrictedPages [] = 'logout';
        $aStrictedPages [] = 'config';
        $aStrictedPages [] = 'setOrder'; 
        $aStrictedPages [] = 'upload-file';
        $aStrictedPages [] = 'crop';  
        $aStrictedPages [] = 'removeImage';   
        $aStrictedPages [] = 'language';   
}

######CONFIG###### 

ini_set ('include_path', sprintf ('.:%s/cms/includes/PEAR', $GLOBALS ['cfg']['include']));

// set a global db connection 

$GLOBALS ['Db']['Connection']            = Database :: getInstance (); 
$GLOBALS ['cfg']['images']['thumbWidth'] = 100;
$GLOBALS ['cfg']['images']['location']   = $GLOBALS ['cfg']['include'] . '/files';

####EINDE CONFIG#############


// control for login

require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Authentication.class.php';
         
$oLogin = new Authentication ();
    
if ((preg_match ('#^//?' . ($GLOBALS ['cfg']['relativ'] != '' ? substr ($GLOBALS ['cfg']['relativ'], 1) . '/' : '') . 'cms/#', $_SERVER ['PHP_SELF']) && $_SERVER ['PHP_SELF'] != $GLOBALS ['cfg']['relativ'] . '/cms/index.php') && checkAuth ()) 
{           
        if (!$oLogin -> validateUser ()) 
        {                    
                header (sprintf ('Location: %s', $SiteRoot));
        } 
        else 
        { 
                $sCurrentPage   = preg_replace ('#^.*/([^/]+)\.(php|html)$#', '${1}', $_SERVER ['PHP_SELF']);
                $sCurrentFolder = preg_replace ('#^.*/(.+)/.+$#', '${1}', $_SERVER ['PHP_SELF']); 
                  
                if ((!in_array ($sCurrentPage, $aStrictedPages) && !in_array ($sCurrentFolder, $aStrictedPages)) && !preg_match ('#^' . $GLOBALS ['cfg']['relativ'] . '/cms/js/#i', $_SERVER ['PHP_SELF']) && $sCurrentPage != 'browser' && $sCurrentPage != 'config' && $sCurrentPage != 'dynamic_images_browser.js' && $sCurrentPage != 'dynamic_folders.js' && $sCurrentPage != 'categorys.js' && $sCurrentPage != 'pages.js' && $sCurrentPage != 'process-sortable' && $sCurrentPage != 'fold_menu')
                {                                                                
                        //header(sprintf('Location: %s/uitloggen.php', $SiteRoot));
                    
                        exit;
                }
                
                $oTemplate -> assign ('userIsIngelogd', true);
        }
} 
elseif ($oLogin -> validateUser ()) 
{
        $oTemplate -> assign ('userIsIngelogd', true);
}


/**
* function to redirect the user
*
* @param string $sUrl - address
* @access public
* @returns nothing
**/

function redirect ($sUrl)
{
        if (!headers_sent ()) 
        {
                header ('location: ' . $sUrl);
        } 
        else 
        {
                echo '<meta http-equiv="refresh" content="0; URL=' . $sUrl . '" />';
        }
}
	
// get the patharray

if (isset ($_SERVER ['PATH_INFO']) && strlen ($_SERVER ['PATH_INFO']) > 0)
{
	    $pathArray = preg_split ('#/#', preg_replace ("#\. [^\./]+$#","", $_SERVER ['PATH_INFO']), -1, PREG_SPLIT_NO_EMPTY);
}
    $url = explode('/', $_SERVER ['REQUEST_URI']);

$today = date ('d-m-Y');
$socialMediaModules = array (
        '0' => 'news',
        '1' => 'webshop'
);

if (isset ($url [2]))
    $module = (empty ($url [2]) ? (empty ($url [3]) ? '' : $url [3]) : $url [2]);

if (in_array($module, $socialMediaModules))
{
        //check connectivity with social media
        $oFacebook  = new Facebook ();
        $oTwitter   = new Twitter ();
        $oLinkedin  = new LinkedinCms ();


        /*Check if the connection with facebook is established*/
        $appData = $oFacebook ->getApplication();
        if (!empty ($appData))
        {
                //Application ID
                $app_id = $appData ['appID'];

                //Application secret code
                $app_secret = $appData ['appSecret'];

                //list of permissions
                $permissions = array ('user_about_me', 'manage_pages', 'user_groups', 'publish_stream');

                //redirect url
                $redirect_url = 'http://'.$_SERVER ['SERVER_NAME'].'/cms//facebook/facebook.php?action=new';

                $params = array (
                    'client_id' => $app_id,
                    'client_secret' => $app_secret,
                    'redirect_uri' => $redirect_url,
                    'code' => 'SOME_INVALID_CODE'
                );

                $aFacebookUser = $oFacebook ->getFacebookUser($_SESSION ['user_id'], $redirect_url);
                $ok = false;
                if (!empty ($aFacebookUser) && $oFacebook ->checkAppIfValid($params))
                {
                        if ($oFacebook ->checkAccessToken($aFacebookUser ['facebookID'], $aFacebookUser ['accessToken'], $redirect_url))
                        {
                                $oTemplate -> assign ('publishFacebook', 'true');
                                $ok = true;
                        }
                        else
                        {
                                $oTemplate -> assign ('publishFacebook', 'You have to accept permissions for facebook App');
                        }
                }
                else 
                {

                                $oTemplate -> assign ('publishFacebook', 'Faceook user doesn\'t exists or Application\'s data are not valid');
                }
        }

        /*Check if the connection with twitter is established*/
        $appTwitterData = $oTwitter ->getApplication();

        $oTemplate -> assign ('publishTwitter', '');
        $okTwitter = false;

        if (!empty ($appTwitterData))
        {
                $aTwitterUser = $oTwitter ->getTwitterUser($_SESSION ['user_id']);

                if (!empty ($aTwitterUser) && $appTwitterData ['correct'] == 1)
                {
                        $connection = new TwitterOAuth($appTwitterData ['consumer_key'], $appTwitterData ['consumer_secret'], $aTwitterUser['oauth_token'], $aTwitterUser['oauth_token_secret']);
                        $method = 'account/verify_credentials';
                        $status = $connection->get($method);

                        if (isset ($status -> error)) 
                        {
                                $okTwitter = false;
                                $oTemplate -> assign ('publishTwitter', 'Twitter user doesn\'t exists');
                        }
                        else 
                        {
                                $okTwitter = true;
                                $oTemplate -> assign ('publishTwitter', 'true');
                        }
                }
                else
                {
                        $oTemplate -> assign ('publishTwitter', 'Twitter user doesn\'t exists or Application\'s data are not valid');
                }
        }

        /*Check if the connection with linkedin is established*/
        $appLinkedinData = $oLinkedin ->getApplication();
        $oTemplate -> assign ('publishLinkedin', '');
        $okLinkedin = false;

        if (!empty ($appLinkedinData))
        {
                $aLinkedinUser = $oLinkedin ->getLinkedinUser($_SESSION ['user_id']);
                if (!empty ($aLinkedinUser) && $appLinkedinData ['correct'] == 1)
                {
                        // create connection
                        $API_CONFIG = array(
                                'appKey'       => $appLinkedinData ['consumer_key'],
                                'appSecret'    => $appLinkedinData ['consumer_secret'],
                                'callbackUrl'  => OAUTH_CALLBACK_LINKEDIN
                        );

                        $connectionLinkedin = new Linkedin($API_CONFIG);

                        $access_token_array = array (
                                'oauth_token' => $aLinkedinUser ['oauth_token'],
                                'oauth_token_secret' => $aLinkedinUser ['oauth_token_secret']
                        );
                        $connectionLinkedin ->setTokenAccess ($access_token_array);
                        $profile = $connectionLinkedin -> profile ();
                        if (isset ($profile ['error']) || empty ($profile)) {
                                $okLinkedin = false;
                                $connection = new Linkedin($API_CONFIG);
                                $oTemplate -> assign ('publishLinkedin', 'Linkedin user doesn\'t exists');
                        }
                        else 
                        {
                               $okLinkedin = true;
                               $oTemplate -> assign ('publishLinkedin', 'true');
                        }
                }
                else
                {
                        $oTemplate -> assign ('publishLinkedin', 'Linkedin user doesn\'t exists or Application\'s data are not valid');
                }
        }
}

//setlocale (LC_ALL,'nl_NL') or setlocale (LC_ALL,'nld_NLD');
$sCurrentDate = date ('d-m-Y');

// vars assigned to smarty
$oTemplate -> assign ('imagesizes', $aImageSizes);
$oTemplate -> assign ('SiteRoot', $SiteRoot);
$oTemplate -> assign ('SiteRootShort', $GLOBALS ['cfg']['relativ'] . '/cms');
$oTemplate -> assign ('sWebsiteUrl', $GLOBALS ['cfg']['sWebsiteUrl']);	
$oTemplate -> assign ('sCurrentDate', $sCurrentDate);
$oTemplate -> assign ('datum', date ('d-m-Y H:i'));
?>
