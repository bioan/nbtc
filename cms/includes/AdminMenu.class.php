<?php

require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php';
/**
    @description: admin menu class 
    @link: http://www.inforitus.nl
    @author Andrei Dragos <andrei@inforitus.nl>
**/
    
class AdminMenu
{

        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * constructor
        *
        */
        
	    public function AdminMenu() 
        { 
            
        }  
        
        /**
	    * @desc get the last errors
	    *
	    * @return array
	    */
	    
	    public function getLastError ()
        {
                return $this -> aError;
        }
        
        /**
        * @desc get the menu
        * 
        * @param int $iId - the id of the menu item
        * 
        * @return array or false by error
        */
        
        public function getMenu ($iId = 0) 
        {
                $sQuery = sprintf ("
                        SELECT
                            id,
                            check_name,
                            show_name
                        FROM
                            admin_menu
                        %s
                    ",
                        (!empty ($iId) ? sprintf ("WHERE id = %d", $iId) : '')
                );
                 
                if (($pResult = Database :: getInstance () -> query ($sQuery)) === false) 
                {           
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                else
                {      
                        $aResults = array ();   
                      
                        if ($pResult -> num_rows > 0)
                        {
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aResults [] = $aRow;
                                }
                        }
                }
               
                return $aResults;
        }
    
    
    
}
?>
