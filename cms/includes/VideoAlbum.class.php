<?php     

/**
     @description: VideoAlbum class
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class VideoAlbum extends Tree
{ 
        /**
        * @desc the tree id specific to pages
        * 
        * @var integer
        */
        
        var $iTreeId = 4;
        
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();

        /**
        * @desc: contructor
        *
        */
                      
        public function VideoAlbum ()
        { 
                parent::Tree ($this -> iTreeId);
            
                $this -> InitData ();   
        }
        
        /**
        * @desc: get the last errors
        *
        * @return array
        */
         
        public function getLastError ()
        {
                return $this -> aError;
        }      
         
        /**
        * @desc: controleerd de velden van een formulier
        *
        * @param array $aValues  
        * 
        * @returns: booleaan
        */
                                 
        private function checkForm ($aValues = array ()) 
        {
                if ($aValues ['video']['size'] != 0)
                {
                        if ($aValues ['video']['size'] > 90016734 || ($aValues ['video']['type'] != 'video/mp4' && $aValues ['video']['type'] != 'video/quicktime') || (substr($aValues ['video']['name'], -3)  != 'mp4' && substr($aValues ['video']['type'], -3) != '.mov'))
                        {
                                $this -> aError [] = 'Video';
                        }
                } 
                 
                if (!isset ($aValues ['photos'][$this -> aDefaultLang ['id']]) || empty ($aValues ['photos'][$this -> aDefaultLang ['id']])) 
                {
                        $this -> aError [] = 'Foto\'s';
                }
                
                if (empty ($aValues ['title'][$this -> aDefaultLang ['id']])) 
                {
                        $this -> aError [] = 'Naam';
                }
                
                if (!$this -> aError) 
                {
                        return true;
                }
                
                return false;
        }
        
        /**
        * @desc: add video
        * 
        * @param array $aValues - the data of item
        * 
        * @returns: booleaan
        */
                                 
        public function addVideo ($aValues = array ())
        {
                if ($this -> checkForm ($aValues)) 
                {  
                        // take the item with the biggest sort number
                        $aVideoItems = $this -> getVideos ();
                        $aVideoItem  = (isset ($aVideoItems [0]) ? $aVideoItems [0] : '');

                        // add post item
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT
                                INTO
                                    videoalbum
                                    (
                                        category_id,
                                        date,
                                        order_number
                                    )
                                VALUES
                                    (
                                        ?,
                                        NOW(),
                                        ?
                                    )
                        ");
                        
                        $iOrderNumber = (isset ($aVideoItem ['order_number']) ? $aVideoItem ['order_number'] + 1 : 0); 
                        
                        $pStatement -> bind_param ('ii',           
                                $aValues ['category'],
                                $iOrderNumber        
                        );
                                
                        if (!$pStatement -> execute ()) 
                        {   
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                
                                return false;
                        }
                        
                        $iInsert_id = $pStatement -> insert_id;
                        
                        if (isset ($aValues ['video']['tmp_name']) && $aValues ['video']['size'] > 0)
                                copy ($aValues ['video']['tmp_name'], '../../video/' . $iInsert_id . '.' . substr ($aValues ['video']['name'], -3));   
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                                $pStatement = Database :: getInstance () -> prepare ("
                                        INSERT
                                        INTO
                                            videoalbum_data
                                            (
                                                videoalbum_id,
                                                lang_id,
                                                title,
                                                description,
                                                photos,
                                                page_description,
                                                slug
                                            )
                                        VALUES
                                            (
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?,
                                                ?
                                            )
                                ");
                                
                                $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]); 
                                $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                
                                $pStatement -> bind_param ('iisssss',           
                                        $iInsert_id,
                                        $lang ['id'],
                                        $aValues ['title'][$lang ['id']],
                                        $aValues ['description'][$lang ['id']],
                                        $sPhotos,
                                        $aValues ['page_description'][$lang ['id']],
                                        $sSlug        
                                );
                                        
                                if (!$pStatement -> execute ()) 
                                {   
                                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                        
                                        return false;
                                }
                        }
                        
                        return true;           
                }
                
                return false;
        }
         
        /**
        * @desc: get the videos
        * 
        * @param int $iItemId - the id of item
        * @param string $sItemSlug - the slug of item
        * @param int $iCategoryId - the id of category of video
        * @param string $sCategorySlug - the slug of category
        * @param string $sSortField - the field on which the results are sorted
        * @param string $sSortType - the type of sort of results  desc/asc  
        * 
        * @return array OR false by error
        */
                                       
        public function getVideos ($iItemId = 0, $sItemSlug = '', $iCategoryId = 0, $sCategorySlug = '', $sSortField = 'order_number', $sSortType = 'desc', $sLanguage = 'default')
        {          
                if (!is_numeric ($iItemId)) 
                {
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT
                            v.*,
                            vd.title,
                            vd.description,
                            vd.photos,
                            vd.page_description,
                            vd.slug,
                            DATE_FORMAT(v.date, '%d-%m-%Y') AS date_format,
                            p.name AS category_name,
                            (SELECT COUNT(*) FROM videoalbum_data WHERE videoalbum_id = v.id AND title != '' AND description != '') as filled
                        FROM
                            videoalbum v
                                INNER JOIN
                            tree t
                                ON
                            v.category_id = t.oid
                                AND
                            t.type_oid = 4
                        LEFT JOIN 
                            pages p
                                ON
                            p.tree_oid = t.oid
                        LEFT JOIN
                            videoalbum_data vd
                                ON
                            vd.videoalbum_id = v.id
                        LEFT JOIN
                            languages l
                                ON
                            (p.lang_id = l.id AND vd.lang_id = l.id)
                        WHERE
                            1
                            " . ($sLanguage == 'default' ? sprintf ("AND l.default = 1") : sprintf ("AND l.short_name = '%s'", Database :: getInstance () -> real_escape_string ($sLanguage))) . "
                            " . (!empty ($iItemId)       ? sprintf ("AND v.id = %d", $iItemId) : '') . "
                            " . (!empty ($sItemSlug)     ? sprintf ("AND vd.slug = '%s'", Database :: getInstance () -> real_escape_string ($sItemSlug)) : '') . "
                            " . (!empty ($iCategoryId)   ? sprintf ("AND v.category_id = %d", $iCategoryId) : '') . "
                            " . (!empty ($sCategorySlug) ? sprintf ("AND p.slug = '%s'", Database :: getInstance () -> real_escape_string ($sCategorySlug)) : '') . "
                        ORDER BY
                            " . $sSortField . " " . $sSortType . "
                ");
                        
                $aResults = array (); 
                
                if (!$pResult)
                {     
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                $aRow ['photos']      = $this -> getPhotosByIds ($aRow ['photos']);
                                $aRow ['description'] = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                                
                                if (preg_match_all ('%<a[^>]+>%s', $aRow ['description'], $aMatch))
                                {
                                        $aLinks = $aMatch [0];
                                        
                                        foreach ($aLinks as $link)
                                        {
                                                if (strpos ($link, 'onclick="') === false && 
                                                strpos ($link, 'target="_blank"') !== false && 
                                                preg_match ('%href="[^"]+"%s', $link, $matches))
                                                {
                                                        $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                        $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['title'] . '\', \'' . $sAddress . '\')"', $link);
                                                        $aRow ['description'] = str_replace ($link, $sNewlink, $aRow ['description']); 
                                                }
                                        }
                                }
                                
                                $aResults []          = $aRow;
                        }
                }
                
                foreach ($aResults as $k => $value)
                {
                        if (count ($this -> aLanguages) > 1)
                        {
                                foreach ($this -> aLanguages as $lang)
                                {
                                        $pResult = Database :: getInstance () -> query ("
                                                SELECT
                                                    v.*,
                                                    vd.title,
                                                    vd.description,
                                                    vd.photos,
                                                    vd.page_description,
                                                    vd.slug,
                                                    DATE_FORMAT(v.date, '%d-%m-%Y') AS date_format,
                                                    p.name AS category_name
                                                FROM
                                                    videoalbum v
                                                        INNER JOIN
                                                    tree t
                                                        ON
                                                    v.category_id = t.oid
                                                        AND
                                                    t.type_oid = 4
                                                LEFT JOIN 
                                                    pages p
                                                        ON
                                                    p.tree_oid = t.oid
                                                LEFT JOIN
                                                    videoalbum_data vd
                                                        ON
                                                    vd.videoalbum_id = v.id
                                                LEFT JOIN
                                                    languages l
                                                        ON
                                                    (p.lang_id = l.id AND vd.lang_id = l.id)
                                                WHERE
                                                    l.id = " . $lang ['id'] . "
                                                AND
                                                    v.id = " . $value ['id'] . "
                                        ");
                                                
                                        if (!$pResult)
                                        {     
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                                
                                                return false;
                                        }  
                                          
                                        if ($pResult -> num_rows > 0)
                                        {         
                                                while ($aRow = $pResult -> fetch_assoc ()) 
                                                {         
                                                        if ($aRow ['title'] != '' || $aRow ['description'] != '')
                                                                $aRow ['filled'] = true;
                                                        $aRow ['photos']             = $this -> getPhotosByIds ($aRow ['photos']);
                                                        $aRow ['description']        = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');        
                                                        
                                                        if (preg_match_all ('%<a[^>]+>%s', $aRow ['description'], $aMatch))
                                                        {
                                                                $aLinks = $aMatch [0];
                                                                
                                                                foreach ($aLinks as $link)
                                                                {
                                                                        if (strpos ($link, 'onclick="') === false && 
                                                                        strpos ($link, 'target="_blank"') !== false && 
                                                                        preg_match ('%href="[^"]+"%s', $link, $matches))
                                                                        {
                                                                                $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                                                $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['title'] . '\', \'' . $sAddress . '\')"', $link);
                                                                                $aRow ['description'] = str_replace ($link, $sNewlink, $aRow ['description']); 
                                                                        }
                                                                }
                                                        }
                                                        
                                                        $aResults [$k][$lang ['id']] = $aRow;
                                                }
                                        }       
                                }
                        }
                        else
                        {
                                unset ($value ['filled']);
                                if ($value ['title'] != '' || $value ['description'] != '')
                                        $value ['filled'] = true;
                                        
                                $aResults [$k][$this -> aDefaultLang ['id']] = $value;
                        }
                }
                
                $aResults = (!empty ($iItemId) || !empty ($sItemSlug)) ? current ($aResults) : $aResults;
                
                return $aResults;
        }
         
        /**
        * @desc: change the specified video 
        * 
        * @param int $iId - the id of the video
        * @param array $aValues - the data that have to be set
        * 
        * @return booleaan
        */
                                       
        public function updateVideo ($iId = 0, $aValues = array ()) 
        {               
                if (!is_numeric ($iId)) 
                {
                        return false;
                }
                 
                if ($this -> checkForm ($aValues)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    videoalbum
                                SET
                                    category_id      = ?
                                WHERE
                                    id               = ?
                        ");
                        
                        $pStatement -> bind_param ('ii',
                                $aValues ['category'],
                                $iId
                        );      
                        
                        if (!$pStatement -> execute ()) 
                        {   
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        if ($aValues ['video']['size'] > 0)
                        {
                                $aVideo = glob ('../../video/' . $iId . '*');
                        
                                foreach ($aVideo as $item)
                                {
                                        unlink ($item);
                                }
                                
                                copy ($aValues ['video']['tmp_name'], '../../video/' . $iId . '.' . substr ($aValues ['video']['name'], -3));          
                        }
                        
                        $aLangValues = $this -> getVideos ($iId, '', 0, '', 'order_number', 'desc');
                        
                        foreach ($this -> aLanguages as $lang)
                        {
                                if (isset ($aLangValues [$lang ['id']]))
                                {
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                UPDATE
                                                    videoalbum_data
                                                SET
                                                    title            = ?,
                                                    description      = ?,
                                                    photos           = ?,
                                                    page_description = ?,
                                                    slug             = ?
                                                WHERE
                                                    videoalbum_id    = ?
                                                AND
                                                    lang_id          = ?
                                        ");
                                        
                                        $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]);
                                        $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : ''); 
                                        
                                        $pStatement -> bind_param ('sssssii',
                                                $aValues ['title'][$lang ['id']],
                                                $aValues ['description'][$lang ['id']],
                                                $sPhotos,
                                                $aValues ['page_description'][$lang ['id']],
                                                $sSlug,
                                                $iId,
                                                $lang ['id']
                                        );      
                                        
                                        if (!$pStatement -> execute ()) 
                                        {   
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                
                                                return false;
                                        }
                                }
                                else
                                {
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                INSERT
                                                INTO
                                                    videoalbum_data
                                                    (
                                                        videoalbum_id,
                                                        lang_id,
                                                        title,
                                                        description,
                                                        photos,
                                                        page_description,
                                                        slug
                                                    )
                                                VALUES
                                                    (
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?,
                                                        ?
                                                    )
                                        ");
                                        
                                        $sSlug   = $this -> urlize ($aValues ['title'][$lang ['id']]);
                                        $sPhotos = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : ''); 
                                        
                                        $pStatement -> bind_param ('iisssss',           
                                                $iId,
                                                $lang ['id'],
                                                $aValues ['title'][$lang ['id']],
                                                $aValues ['description'][$lang ['id']],
                                                $sPhotos,
                                                $aValues ['page_description'][$lang ['id']],
                                                $sSlug        
                                        );
                                                
                                        if (!$pStatement -> execute ()) 
                                        {   
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                                
                                                return false;
                                        }       
                                }
                        }
                        
                        return true;
                }
                
                return false;
        }
         
        /**
        * @desc delete a specified video
        * 
        * @param int $iId
        * 
        * @returns: booleaan
        */
          
        public function deleteVideo ($iId)
        {
            
                if (!is_numeric ($iId)) 
                {
                        return false;
                }
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM
                            videoalbum
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId
                );
                
                if (!$pStatement -> execute ()) 
                {   
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM
                            videoalbum_data
                        WHERE
                            videoalbum_id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId
                );
                
                if (!$pStatement -> execute ()) 
                {   
                        return false;
                }
                
                $aVideo = glob ('../../video/' . $iId . '*');
                
                foreach ($aVideo as $item)
                {
                        unlink ($item);
                }
                    
                return true;
        }                             
     
}
?>
