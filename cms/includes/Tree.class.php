<?php

/**
 *
 * Base class for manage the category structure. This class is the base class for several classes.
 * Each class have a different treeOid and the id is also unique 
 *       
 * @author Andrei Dragos <andrei@inforitus.nl>
*/

class Tree 
{
        /**
        * @desc contains the temporary values of a tree item 
        *
        * @var array 
        */          
        
        var $aCategoryRaw = array ();

        /**
        * @desc the id of specific tree
        * 
        * @var integer
        */
        
        var $iOid = 0;

        /**
        * @desc the parents of nodes
        * 
        * @var array
        */
        
        var $aParents = array ();

        /**
        * @desc the menu items
        * 
        * @var array
        */
        
        var $aMenu = array ();
        
        /**
        * @desc contains all languages 
        * 
        * @var array
        */
        
        var $aLanguages = array ();
        
        /**
        * @desc contains default language 
        * 
        * @var array
        */
        
        var $aDefaultLang = array ();

        /**
        * Constructor 
        * 
        * @returns void	   	   	   
        */

        public function Tree ($iTypeId = 0)
        {
                $this -> aLanguages   = Language :: getLanguages ();
                $this -> aDefaultLang = Language :: getLanguages (0, '', 'id', 'asc', true);
                
	            if (!empty ($iTypeId))
                {
		                $this -> iTypeOid = $iTypeId;
                }
        }

        /**
        * @desc Make the used vars empty
        * 
        * @return void
        */
		 		 		 		 		        
        public function InitData () 
        {
	            $this -> iOid         = 0;
	            $this -> aCategoryRaw = array ();
	            $this -> aParents     = array ();
        }

        /**
        * @desc create the slug of the item
        * 
        * @param string $sParam - the string that will be converted
        * 
        * @returns string
        */
         
        public function urlize ($sParams)
        {
                $sTitle   = strtolower ($sParams);
                $aAllowed = preg_split ('//', 'abcdefghijklmnopqrstuvwxyz0123456789-+_');

                for ($i = 0, $j = strlen ($sTitle); $i < $j; $i++)
                {
                        if (!in_array ($sTitle  [$i], $aAllowed))
                        {
                                $sTitle  [$i] = '-';
                        }
                }

                for ($i = 0; $i < 3; $i++)
                {
                        $sTitel = str_replace ('--', '-', $sTitle);
                }
                
                return preg_replace ('/^ [\-]*(.+?) [\-]*$/s', '\\1', $sTitle);
        }

        /**
        * @desc Get the parents of the specified oid
        * 
        * @param int $iId - id of item
        * 
        * @returns array ()
        */
         
	    public function getParents ($iId = 0)
        {
	            if (empty ($iId) || !is_numeric ($iId))
                {
	  	                return false;
                }
                
	            if ($iId != 0) 
                {
	                    $this -> iOid = $iId;
	                    $this -> GettreeRaw ($iId);
	            }
	            
                $pResult = Database :: getInstance () -> query ("
		                  SELECT
		                      * 
		                  FROM 
		                      tree 
		                  WHERE 
		                      lft < " . $this -> aCategoryRaw ['lft'] . "
		                        AND
		                      rgt > " . $this -> aCategoryRaw ['rgt'] . "
		                        AND
		                      type_oid = " . $this -> iTypeOid . "
		                  ORDER BY
		                      lft desc
		              "
	            );
                
	            $aResults = array ();   
                $aRows = array (); 
              
                if ($pResult -> num_rows > 0)
                {
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {

                                $aRows[] = $aRow;
                        }
                }

                foreach ($aRows as $aRow)
                {
                        foreach ($this -> aLanguages as $lang)
                        {  
                                $pResult = Database :: getInstance () -> query ("
                                        SELECT
                                            name,
                                            photos,
                                            content,
                                            page_description,
                                            slug
                                        FROM
                                            pages
                                        WHERE
                                            tree_oid = " . $aRow ['oid'] . "
                                        AND
                                            lang_id = " . $lang ['id'] . "
                                ");
                                
                                $aLang               = $pResult -> fetch_assoc ();
                                $aLang ['photos']    = $this -> getPhotosByIds ($aLang ['photos']);
                                $aLang ['content']   = html_entity_decode ($aLang ['content'], ENT_COMPAT, 'UTF-8');
                                $aRow [$lang ['id']] = $aLang;
                                
                                if ($lang ['default'] == 1)
                                {
                                        $aRow ['name']             = $aLang ['name'];
                                        $aRow ['photos']           = $aLang ['photos'];
                                        $aRow ['content']          = $aLang ['content'];
                                        $aRow ['page_description'] = $aLang ['page_description'];
                                        $aRow ['slug']             = $aLang ['slug'];  
                                }
                        
                        }
                        
                        $aResults []     = $aRow;
                        
                }
                
                return $aResults;
        }

        /**
        * @desc Set the values of the selected item
        *                   
        * @param int $iItemId - the id of item
        * @param string $sItemSlug - the slug of item
        * @param array $aParent - parent of item`s
        * 
        * @returns array
        */
		 		 		 		 		 		        
        public function GettreeRaw ($iItemId = 0, $sItemSlug = '', $aParent = array (), $sLanguage = 'default', $iLevel = 0) 
        {
	            if (empty ($iItemId) && $sItemSlug == '')
                {
	                    return false;
                }
                    
	            if ($iItemId != 0 && $iItemId != $this -> iOid) 
                {
	                    $this -> InitData ();
	                    $this -> iOid = $iItemId;
	            }
                
	            $pResult = Database :: getInstance () -> query ("
	                    SELECT 
	                        tree.* 
	                    FROM 
	                        tree
                        LEFT JOIN
                            pages
                                ON
                            pages.tree_oid = tree.oid   
                        LEFT JOIN
                            languages
                                ON
                            pages.lang_id = languages.id 
	                    WHERE
                            tree.type_oid = " . $this -> iTypeOid . "  
	                        " . ($iItemId != 0 ? "AND tree.oid = " . $this -> iOid . "" : "") . "
                            " . ($iLevel != 0 ? "AND level = " . $iLevel . "" : "") . "
                            " . ($sItemSlug != '' ? "AND pages.slug = '" . Database :: getInstance () -> real_escape_string ($sItemSlug) . "'" : "") . "
                            " . (!empty ($aParent) && is_array ($aParent) ? "AND tree.lft > " . $aParent ['lft'] . " AND tree.rgt < " . $aParent ['rgt'] . " AND tree.level = " . ($aParent ['level'] + 1) . "" : "") . "
	                        " . ($sLanguage == 'default' ? sprintf ("AND languages.default = 1") : sprintf ("AND languages.short_name = '%s'", Database :: getInstance () -> real_escape_string ($sLanguage))) . "
                ");
                
	            if ($pResult -> num_rows == 1)
                {          
                        $aResult = $pResult -> fetch_assoc ();
                }
                
                
                $this -> aCategoryRaw = $aResult; 
                
                if ($iItemId == 0)
                {
                        $this -> iOid = $aResult ['oid']; 
                } 
                
                if (isset ($rQuery ['title']))
                {
                        $this -> aCategoryRaw ['title'] = $aResult ['title']; 
                }
                
                 
	            if (empty ($this -> aCategoryRaw) ) 
                {
	                    return false;
	            }	
                
	            return $this -> aCategoryRaw;
        }
        
        /**
        * @desc gets the data of a specific page
        * 
        * @param int $iId - id of page
        * @param string $sItemSlug - slug of page
        * @param string $sLanguage - the language
        * 
        * @return array or boolean by error
        */
        
        public function getNodeData ($iId = 0, $sItemSlug = '', $sLanguage = 'default', $aParent = array (), $iLevel = 0)
        {
                if (($iId == 0 || !is_numeric ($iId)) && $sItemSlug == '')
                {
                        return false;
                }
                    
                $this -> GettreeRaw ($iId, $sItemSlug, $aParent, $sLanguage, $iLevel);
                
                foreach ($this -> aLanguages as $lang)
                {  
                        $pResult = Database :: getInstance () -> query ("
                                SELECT
                                    name,
                                    photos,
                                    content,
                                    page_description,
                                    slug
                                FROM
                                    pages
                                WHERE
                                    tree_oid = " . $this -> aCategoryRaw ['oid'] . "
                                AND
                                    lang_id = " . $lang ['id'] . "
                        ");
                        
                        if ($pResult -> num_rows > 0)
                        {
                                $aRow                   = $pResult -> fetch_assoc ();
                                $aRow ['content']       = html_entity_decode ($aRow ['content'], ENT_COMPAT, 'UTF-8');
                                $aRow ['photos']        = $this -> getPhotosByIds ($aRow ['photos']);
                                
                                if (preg_match_all ('%<a[^>]+>%s', $aRow ['content'], $aMatch))
                                {
                                        $aLinks = $aMatch [0];
                                        
                                        foreach ($aLinks as $link)
                                        {
                                                if (strpos ($link, 'onclick="') === false && 
                                                strpos ($link, 'target="_blank"') !== false && 
                                                preg_match ('%href="[^"]+"%s', $link, $matches))
                                                {
                                                        $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                        $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['name'] . '\', \'' . $sAddress . '\')"', $link);
                                                        $aRow ['content'] = str_replace ($link, $sNewlink, $aRow ['content']); 
                                                }
                                        }
                                }
                                
                                $aResult [$lang ['id']] = $aRow;
                                
                                if ($sLanguage == $lang ['short_name'] || ($sLanguage == 'default' && $lang ['default'] == 1))
                                {
                                        $aResult ['name']             = $aRow ['name'];
                                        $aResult ['photos']           = $aRow ['photos'];
                                        $aResult ['content']          = $aRow ['content'];
                                        $aResult ['page_description'] = $aRow ['page_description'];
                                        $aResult ['slug']             = $aRow ['slug'];  
                                        
                                        if (preg_match_all ('%<a[^>]+>%s', $aResult ['content'], $aMatch))
                                        {
                                                $aLinks = $aMatch [0];
                                                
                                                foreach ($aLinks as $link)
                                                {
                                                        if (strpos ($link, 'onclick="') === false && 
                                                        strpos ($link, 'target="_blank"') !== false && 
                                                        preg_match ('%href="[^"]+"%s', $link, $matches))
                                                        {
                                                                $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                                $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aResult ['name'] . '\', \'' . $sAddress . '\')"', $link);
                                                                $aResult ['content'] = str_replace ($link, $sNewlink, $aResult ['content']); 
                                                        }
                                                }
                                        }
                                }
                                
                        }
                        
                }
                
                $aResult ['oid']      = $this -> aCategoryRaw ['oid'];
                $aResult ['level']    = $this -> aCategoryRaw ['level'];
                $aResult ['lft']      = $this -> aCategoryRaw ['lft'];
                $aResult ['type_oid'] = $this -> aCategoryRaw ['type_oid'];
                $aResult ['rgt']      = $this -> aCategoryRaw ['rgt']; 
                $aResult ['private']  = $this -> aCategoryRaw ['private'];
                
                return $aResult;
        }

        /**
        * @desc Delete a specified node
        * 
        * @param int $iId - id of node
        * @param boolean $bDeleteContentOfPage - if true deletes also the content of the node else not
        * 
        * @return booleaan
        */
		 		 		 		 		 		        
        public function deleteNode ($iId = 0, $bDeleteContentOfPage = false) 
        {
    	        if (empty ($iId) || !is_numeric ($iId))
                {
    	                return false;
                }
                    
    	        $this -> GettreeRaw ($iId);
    
    	        $pStatement = Database :: getInstance () -> prepare ("
    		            DELETE 
    		            FROM
    		                tree
    		            WHERE
    		                lft >= ?
    		                    AND
    		                rgt <= ?
    		                    AND
    		                type_oid = ?
    		            
    	        ");
                
                $pStatement -> bind_param ('ddd',
    		            $this -> aCategoryRaw ['lft'],
    		            $this -> aCategoryRaw ['rgt'],
    		            $this -> iTypeOid
    	        );
                
                $pStatement -> execute ();
                $pStatement -> close ();
                
    	        $iDifference = intval ($this -> aCategoryRaw ['lft']) -  intval ($this -> aCategoryRaw ['rgt']) - 1;
    	         
                if ($this -> _UpdateCelko ($this -> aCategoryRaw ['lft'], $this -> aCategoryRaw ['rgt'], $iDifference)) 
                {   
    	    		        
                        if ($bDeleteContentOfPage) 
                        {
    			                $pStatement = Database :: getInstance () -> prepare ("
    				                    DELETE
    				                    FROM
    					                    pages
    				                    WHERE
    					                    tree_oid = ?
    			                ");
                                
                                $pStatement -> bind_param ('i',
    				                    $iId
    			                );
                                     
    			                if (!$pStatement -> execute ())
                                {
                                        return false;
                                }
                        }
                                
    		            $this -> checkTree ();         
    		            
                        return true;
    	        }
                
    	        return false;
        }

        /**
        * @desc Insert a new item into the tree
        * 
        * @param array $aData - data that have to be inserted
        * @param int $iId - insert node before/after of this id or at end
        * @param string $sAction - describe the place where should be putted the item
        * 
        * @return boolean
        */
		 		 		 		 		 		 		        
        public function insert ($aData, $iId, $sAction)   
        {
	            if ((!is_array ($aData) || empty ($aData)) || (empty ($iId) || !is_numeric ($iId)) || empty ($sAction))
                {
	  	                return false;
                }
                    
	            $this -> GettreeRaw ($iId);   
	            $iDifference = 2;
                
	            if ($sAction == 'after' || $sAction == 'insert_after') 
                {
	                    $newLeftCell  = intval ($this -> aCategoryRaw ['rgt']) + 1;
		                $newRightCell = $newLeftCell + 1;
		                $newLevelCell = intval ($this -> aCategoryRaw ['level']);
		                $this -> _UpdateCelko ($newLeftCell -1, $newRightCell -2, $iDifference);
	            } 
                elseif ($sAction == 'before' || $sAction == 'insert_before') 
                {        
		                $newLeftCell  = intval ($this -> aCategoryRaw ['lft']);
		                $newRightCell = $newLeftCell + 1;
                        $newLevelCell = intval ($this -> aCategoryRaw ['level']);   
		                $this -> _UpdateCelko ($newLeftCell -1, $newRightCell -2, $iDifference);  
	            } 
                elseif ($sAction == 'add') 
                {
		                $newLeftCell  = intval ($this -> aCategoryRaw ['rgt']);
		                $newRightCell = $newLeftCell + 1;
		                $newLevelCell = intval ($this -> aCategoryRaw ['level']) + 1;
		                $this -> _UpdateCelko ($newLeftCell, $newRightCell -2, $iDifference);
	            } 
                
	            $pStatement = Database :: getInstance () -> prepare ("
		                INSERT
		                INTO 
	                        tree 
			                (
	                            type_oid, lft, rgt, level, private, date
	                        ) 
		                VALUES 
			                ( 
				                ?, ?, ?, ?, ?, NOW()
			                )
	            ");
                
                $sPhotos  = (isset ($aData ['photos']) && $aData ['photos'] != '' ? implode (',', $aData ['photos']) : '');
                $iPrivate = (isset ($_POST ['private']) ? 1 : 0);
                
                $pStatement -> bind_param ('iiiii',
		                $this -> iTypeOid,
	                    $newLeftCell,
	                    $newRightCell,
	                    $newLevelCell,
	                    $iPrivate
	            );    
                 
	            if (!$pStatement -> execute ()) 
                {   
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                        
	                    return false;
	            } 
                
                $this -> iOid = Database :: getInstance () -> insert_id;  
                $this -> checkTree ();
                
                foreach ($this -> aLanguages as $lang)
	            {
	                    $pStatement = Database :: getInstance () -> prepare ("
        		                INSERT
        		                INTO 
        	                        pages 
        			                (
        	                            tree_oid, lang_id, name, content, photos, page_description, slug
        	                        ) 
        		                VALUES 
        			                ( 
        				                ?, ?, ?, ?, ?, ?, ?
        			                )
        	            ");
                        
                        $sContent = (isset ($aData ['content'][$lang ['id']]) ? $aData ['content'][$lang ['id']] : '');
                        $sSlug    = $this -> urlize (isset ($aData ['slug'][$lang ['id']]) ? $aData ['slug'][$lang ['id']] : $aData ['name'][$lang ['id']]);
                        $sPhotos  = (isset ($aData ['photos'][$lang ['id']]) && $aData ['photos'][$lang ['id']] != '' ? implode (',', $aData ['photos'][$lang ['id']]) : '');
                        
                        $pStatement -> bind_param ('iisssss',
        		                $this -> iOid,
        	                    $lang ['id'],
        	                    $aData ['name'][$lang ['id']],
        	                    $sContent,
                                $sPhotos,
        	                    $aData ['page_description'][$lang ['id']],
                                $sSlug
        	            );    
                         
        	            if (!$pStatement -> execute ()) 
                        {   
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                                 
        	                    return false;
        	            }               
                }
                
                return true;	
	            
        }

        /**
        * @desc Update the right side of the tree
        * 
        * @param int $iNodeLeft
        * @param int $iDifference
        * 
        * @return boolean
        */
		 		 		 		 		 		        
        private function _UpdateNodeRight ($iNodeLeft, $iDifference) 
        {
	            if (empty ($iNodeLeft))
                {
	                    return false;
                }
	            
                $pStatement = Database :: getInstance () -> prepare ("
	                    UPDATE
	                            tree
	                        SET
	                            lft = lft + ?,
	                            rgt = rgt + ?
	                        WHERE
	                            lft > ?
	                                AND
	                            type_oid = ?
	                        
	            ");
                
                $pStatement -> bind_param ('dddd',
	                    $iDifference,
	                    $iDifference,
	                    $iNodeLeft,
	                    $this -> iTypeOid
	            );
                
                if (!$pStatement -> execute ())
                {            
                        return false;   
                }
	            
	            return true;
        }

        /**
        * @desc Update the left side of the tree
        * 
        * @param int $iNodeLeft
        * @param int $iNodeRight		 
        * @param int $iDifference
        * 
        * @return boolean
        */
         
        private function _UpdateNodeParents ($iNodeLeft, $iNodeRight, $iDifference) 
        {
	            if (empty ($iNodeLeft) || empty ($iNodeRight))
                {
	                    return false;
                }
	            
                $pStatement = Database :: getInstance () -> prepare ("
	                    UPDATE
	                        tree
	                    SET
	                        rgt = rgt + ?
	                    WHERE
	                        lft <= ?
	                            AND
	                        rgt > ?
	                            AND
	                        type_oid = ?
	            ");
                
                $pStatement -> bind_param ('iiii',
	                    $iDifference,
	                    $iNodeLeft,
	                    $iNodeRight,
	                    $this -> iTypeOid
	            );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
	            
                return true;
        }

        /**
        * @desc commit the update functions
        * 
        * @access private
        * @param int $iNodeLeft - node left
        * @param int $iNodeRight - node right
        * @param int $iDifference
        * 
        * @return boolean
        */
         
	    private function _UpdateCelko ($iNodeLeft, $iNodeRight, $iDifference) 
        {
	            if ($this -> _UpdateNodeRight ($iNodeLeft,$iDifference) && $this -> _UpdateNodeParents ($iNodeLeft,$iNodeRight,$iDifference))
                {
	                    return true;
                }
                
	            return false;
        }	

        /**
        * @desc Update a specified node
        * 
        * @param int $iCatId
        * @param array $aValues - the data that will be set
        * 
        * @return boolean
        */
           
	    public function changeNode ($iCatId = 0, $aValues = array ()) 
        {                
	            if (empty ($iCatId))
                {
	  	                return false;
                }
	            
                // initialise the arguments of the object
	            $this -> GettreeRaw ($iCatId);    	
	            
                $pStatement = Database :: getInstance () -> prepare ("
	                    UPDATE
	                        tree
	                    SET
		                    private          = ? 
	                    WHERE
	                        oid              = ?
	            ");
                
                $iPrivate = (isset ($_GET ['private']) ? $_GET ['private'] : (isset ($_POST ['private']) ? 1 : 0));
                
                $pStatement -> bind_param ('ii',
	                    $iPrivate, 
                        $this -> iOid
	            );
                
	            if (!$pStatement -> execute ()) 
                {    
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                        
	                    return false;              
	            }
                
                $aLangValues = $this -> getNodeData ($iCatId, '');
                
                foreach ($this -> aLanguages as $lang)
	            {
	                    if (isset ($aLangValues [$lang ['id']]) && !isset ($_GET ['private']))
                        { 
        			            $pStatement = Database :: getInstance () -> prepare ("
        				                UPDATE
        					                pages
        				                SET
                                            name             = ?,
        					                content          = ?,
                                            photos           = ?,
    					                    page_description = ?,
                                            slug             = ?
        				                WHERE	
        					                tree_oid         = ?
                                        AND
                                            lang_id          = ?
        			            ");
                                
                                $sContent = (isset ($aValues ['content'][$lang ['id']]) ? $aValues ['content'][$lang ['id']] : '');
                                $sSlug    = $this -> urlize ((isset ($aValues ['slug'][$lang ['id']]) ? $aValues ['slug'][$lang ['id']] : $aValues ['name'][$lang ['id']]));
                                $sPhotos  = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                
                                $pStatement -> bind_param ('sssssii',
        				                $aValues ['name'][$lang ['id']],
                                        $sContent,
                                        $sPhotos,
        				                $aValues ['page_description'][$lang ['id']],
                                        $sSlug,   
                                        $this -> iOid,
                                        $lang ['id']
        			            );     
                                
        			            if (!$pStatement -> execute ())
                                {
                                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                                        
        				                return false;
                                }
                       }
                       elseif (!isset ($_GET ['private']))
                       {
                                $pStatement = Database :: getInstance () -> prepare ("
				                INSERT
				                INTO
					                pages
					                (
						                tree_oid, 
                                        lang_id, 
                                        name, 
                                        content, 
                                        photos, 
                                        page_description, 
                                        slug
					                )
				                VALUES
					                (
						                ?, ?, ?, ?, ?, ?, ?
					                )
        			            ");
                                
                                $sContent = (isset ($aValues ['content'][$lang ['id']]) ? $aValues ['content'][$lang ['id']] : '');
                                $sSlug    = $this -> urlize ((isset ($aValues ['slug'][$lang ['id']]) ? $aValues ['slug'][$lang ['id']] : $aValues ['name'][$lang ['id']]));
                                $sPhotos  = (isset ($aValues ['photos'][$lang ['id']]) ? implode (',', $aValues ['photos'][$lang ['id']]) : '');
                                
                                $pStatement -> bind_param ('iisssss',
        				                $this -> iOid,
                                        $lang ['id'],
                                        $aValues ['name'][$lang ['id']],
        				                $sContent,
                                        $sPhotos,
        				                $aValues ['page_description'][$lang ['id']],
                                        $sSlug
        			            );
                                
        			            if (!$pStatement -> execute ())
                                {
                                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                                        
        				                return false;
                                }
                       }
                }
                                                  
	            return true;
        }	

        /**
        * @desc Get the leafnodes of the tree
        * 
        * @returns array OR false by error
        */
		 		 		 		 		 		        
        public function getLeafNodes ()
        {
	            $pResult = Database :: getInstance () -> query ("
		                SELECT 
			                * 
		                FROM
			                tree 
		                WHERE 
			                lft+1 = rgt 
				                AND
			                type_oid = " . $this -> iTypeOid . "
		                ORDER BY 
			                lft
		        ");
                
                if ($pResult)
                {
                        $aResults = array ();
                        
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                $aResults [] = $aRow;
                        }
                }
                else
                {
                        return false;
                }
                
	            return $aResults;
        }
        
        /**
        * @desc This method show the index of the item that is selected 
        * 
        * @param string $sSlug - slug of the page
        * 
        * @return int the index of the page 
        */
        
        public function getIndexOfPage ($sSlug)
        {
                $pQuery = Database :: getInstance () -> query ('
                    SELECT
                          oid, name, lft, rgt, level, type_oid, slug, page_description   
                    FROM
                        tree
                    WHERE
                        type_oid = 2
                    AND
                        level = 1
                    ORDER BY
                        lft

                ');    
                 
                $iIndex = 0;  
                
                if ($pQuery -> num_rows > 0)
                {
                        while ($aRow = $pQuery -> fetch_assoc ())
                        {
                                if ($aRow ['slug'] == $sSlug)
                                {
                                        break; 
                                }
                                
                                $iIndex ++;  
                        }
                }
                
                return $iIndex;
        }    
        
        /**
        * @desc get all nodes
        * 
        * @param int $iLevel - the level of nodes
        * @param int $iRoot - take and root or not
        * @param string $sSortField - the results will be sorted by this field
        * @param string $sSortType - desc/asc
        * @param bool $bAdmin - cms/site
        * 
        * @return array or false by error
        */

        public function getAllNodes ($iLevel = 0, $iRoot = 0, $sSortField = 'order_number', $sSortType = 'asc', $bAdmin = false, $sLanguage = 'default')
        {                     
	            $sQuery = sprintf ("
		                SELECT 
			                t.*,
                            p.name,
                            p.content,
                            p.photos,
                            p.page_description,
                            p.slug,
                            (SELECT COUNT(*) FROM pages WHERE tree_oid = t.oid AND name != '' " . ($this -> iTypeOid == 2 ? "AND content != ''" : '') . ") as filled" . ($bAdmin == true ? ",(SELECT fold FROM menu_settings WHERE tree_oid = t.oid AND user_id = " . $_SESSION ['user_id'] . ") as fold" : "") . "  
		                FROM 
			                tree t
                        LEFT JOIN
                            pages p
                                ON
                            p.tree_oid = t.oid   
                        LEFT JOIN
                            languages
                                ON
                            p.lang_id = languages.id 
		                WHERE 
			                t.type_oid = %d 
                            %s
			                %s
                            %s
                            %s
                        ORDER BY 
			                %s
                            %s
                        
		            ", 
		                $this -> iTypeOid,
                        ($bAdmin == false ? ' AND t.private = 0' : ''),
                        ($sLanguage == 'default' ? ' AND languages.default = 1' : ' AND languages.short_name = "' . $sLanguage . '"'),
                        ($iLevel == 0 ? '' : ' AND t.level = ' . $iLevel . ''), 
                        ($iRoot == 0  ? '' : ' AND t.level != 0'),
                        Database :: getInstance () -> real_escape_string ($sSortField),
                        Database :: getInstance () -> real_escape_string ($sSortType)
	            );      
                
	            if (($pResult = Database :: getInstance () -> query ($sQuery))===false) 
                {       
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                        
                        return false;
                } 
                else 
                {
                        $aResults = array ();
                        
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                $aRow ['photos']      = $this -> getPhotosByIds ($aRow ['photos']);
                                $aRow ['content']     = html_entity_decode ($aRow ['content'], ENT_COMPAT, 'UTF-8');
                                $aResults []          = $aRow;
                        }
                }
                
                foreach ($aResults as $k => $value)
                {
                        if (count ($this -> aLanguages) > 1)
                        {
                                foreach ($this -> aLanguages as $lang)
                                {
                                        $sQuery = sprintf ("
                        		                SELECT 
                        			                t.*,p.name,p.content,p.page_description,p.slug" . ($bAdmin == true ? ",(SELECT fold FROM menu_settings WHERE tree_oid = t.oid AND user_id = " . $_SESSION ['user_id'] . ") as fold" : "") . "  
                        		                FROM 
                        			                tree t
                                                LEFT JOIN
                                                    pages p
                                                        ON
                                                    p.tree_oid = t.oid   
                                                LEFT JOIN
                                                    languages
                                                        ON
                                                    p.lang_id = languages.id 
                        		                WHERE 
                        			                t.oid = %d
                                                AND 
                        			                p.lang_id = %d
                                                    
                        		            ", 
                                                $value ['oid'],
                        		                $lang ['id']
                                        );      
                                        
                        	            if (($pResult = Database :: getInstance () -> query ($sQuery))===false) 
                                        {       
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                                                
                                                return false;
                                        } 
                                        else 
                                        {
                                                while ($aRow = $pResult -> fetch_assoc ()) 
                                                {
                                                        if ($aRow ['name'] != '' || $aRow ['content'] != '')
                                                                $aRow ['filled'] = true;
                                                                
                                                        $aRow ['content']            = html_entity_decode ($aRow ['content'], ENT_COMPAT, 'UTF-8');
                                                        $aResults [$k][$lang ['id']] = $aRow;
                                                }
                                        }       
                                }
                        }
                        else
                        {
                                unset ($value ['filled']);
                                if ($value ['name'] != '' || $value ['description'] != '')
                                        $value ['filled'] = true;
                                        
                                $aResults [$k][$this -> aDefaultLang ['id']] = $value;
                        }
                }
                
                return $aResults;
        }

        /**
        * @desc Get the childs of the specified id
        * 
        * @param int $iItemId - the id of the parent
        * @param string $sItemSlug - the slug of parent
        * @param int $iLevel - the level of childs
        * @parem array $aParent - parent of childs
        * 
        * @returns array or false by error
         */
		 	 	 	         
        public function getChilds ($iItemId = 0, $sItemSlug = '', $iLevel = 0, $aParent = array (), $sLanguage = 'default', $bAdmin = false, $sSearch = '')
        {
                if (empty ($iItemId) && $sItemSlug == '')
                {
                          return false;
                }
                
                $this -> GettreeRaw ($iItemId, $sItemSlug, $aParent);
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT
                              t.*,
                              p.name,
                              p.content,
                              p.photos,
                              p.page_description,
                              p.slug,
                              (SELECT COUNT(*) FROM pages WHERE tree_oid = t.oid AND name != '' " . ($this -> iTypeOid == 2 ? "AND content != ''" : '') . ") as filled" . ($bAdmin == true ? ",(SELECT fold FROM menu_settings WHERE tree_oid = t.oid AND user_id = " . $_SESSION ['user_id'] . ") as fold" : "") . "  
                        FROM
                            tree t
                        LEFT JOIN
                            pages p
                                ON
                            t.oid = p.tree_oid
                        LEFT JOIN
                            languages
                                ON
                            languages.id = p.lang_id
                        WHERE
                            t.lft > " . $this -> aCategoryRaw ['lft'] . "
                                AND
                            t.rgt < " . $this -> aCategoryRaw ['rgt'] . "
                                AND
                            t.type_oid = " . $this -> iTypeOid . "
                                " . ($bAdmin == false ? ' AND t.private = 0' : '') . "
                                " . (!empty ($iLevel) ? sprintf ("AND t.level = %d", $iLevel) : '') . "
                                " . ($sLanguage == 'default' ? sprintf ("AND languages.default = 1") : sprintf ("AND languages.short_name = '%s'", Database :: getInstance () -> real_escape_string ($sLanguage))) . "
                                ". ($sSearch == "" ? '' : ' AND (p.name LIKE "%' . Database :: getInstance () -> real_escape_string ($sSearch) . '%" OR
                        p.content LIKE "%' . Database :: getInstance () -> real_escape_string ($sSearch) . '%")') . "
                        ORDER BY
                            t.order_number
                ");
                
                $aResults = array ();   
              
                if ($pResult -> num_rows > 0)
                {
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                $aRow ['photos']  = $this -> getPhotosByIds ($aRow ['photos']);
                                $aRow ['content'] = html_entity_decode ($aRow ['content'], ENT_COMPAT, 'UTF-8');
                                
                                if (preg_match_all ('%<a[^>]+>%s', $aRow ['content'], $aMatch))
                                {
                                        $aLinks = $aMatch [0];
                                        
                                        foreach ($aLinks as $link)
                                        {
                                                if (strpos ($link, 'onclick="') === false && 
                                                strpos ($link, 'target="_blank"') !== false && 
                                                preg_match ('%href="[^"]+"%s', $link, $matches))
                                                {
                                                        $sAddress = str_replace (array ('href="', '"', 'http://', 'www.'), '', $matches [0]);
                                                        $sNewlink = str_replace ($matches [0], $matches [0] . ' onclick="trackOutboundLink(this, \'' . $aRow ['name'] . '\', \'' . $sAddress . '\')"', $link);
                                                        
                                                        $aRow ['content'] = str_replace ($link, $sNewlink, $aRow ['content']); 
                                                }
                                        }
                                }
                                
                                if (count ($this -> aLanguages) > 1)
                                {
                                        foreach ($this -> aLanguages as $lang)
                                        {  
                                                $pLang = Database :: getInstance () -> query ("
                                                        SELECT
                                                            name,
                                                            content,
                                                            page_description,
                                                            slug
                                                        FROM
                                                            pages
                                                        WHERE
                                                            tree_oid = " . $aRow ['oid'] . "
                                                        AND
                                                            lang_id = " . $lang ['id'] . "
                                                ");
                                                
                                                $aLang               = $pLang -> fetch_assoc ();
                                                $aLang ['content']   = html_entity_decode ($aLang ['content'], ENT_COMPAT, 'UTF-8');
                                                $aRow [$lang ['id']] = $aLang;
                                                
                                                if ($lang ['default'] == 1 && $sLanguage == 'default')
                                                {
                                                        $aRow ['name']             = $aLang ['name'];
                                                        $aRow ['content']          = $aLang ['content'];
                                                        $aRow ['page_description'] = $aLang ['page_description'];
                                                        $aRow ['slug']             = $aLang ['slug'];
                                                }
                                        
                                        }
                                }
                                else
                                {
                                        $aRow [$this -> aDefaultLang ['id']] = $aRow;
                                }
                                
                                $aResults []      = $aRow;
                        }
                }
                
                return $aResults;
        }

        /**
        * @desc get the page and his content, oid or name
        * 
        * @param int $iId Optional
        * @param int $iTypeId    
        * @param string $sSlug Optional
        * 
        * @returns array OR false by error
        */
                                                                  
        public function get ($iId = 0, $iTypeId = 0, $sSlug = '')
        {                      
                if ((!empty ($iId) && !is_numeric ($iId)) || empty ($iTypeId))
                {
                        return false;
                }
                
                if (!empty ($iTypeId))
                {
                        $this -> iTypeOid = $iTypeId;
                }
                
                $aWhereLines = array();
                
                if (!empty ($iId) && is_numeric ($iId))
                {
                        $aWhereLines [] = sprintf ('tree.oid = %d', $iId);
                }
                
                if (!empty ($sSlug))
                {
                        $aWhereLines [] = sprintf ('tree.slug = "%s"', Database :: getInstance () -> real_escape_string($sSlug));
                }
                
                if (!empty ($iTypeId))
                {
                        $aWhereLines [] = sprintf ('tree.type_oid = %d', $this -> iTypeOid);
                }    
                
                $sQuery = sprintf ("    
                        SELECT 
                            tree.name,
                            tree.level,
                            pages.content,
                            pages.photos
                        FROM 
                            tree 
                                INNER JOIN
                            pages
                                ON
                                    tree.oid = pages.tree_oid
                                        AND
                                    pages.active = 1
                        %s
                        ", 
                        (!empty ($aWhereLines)) ? sprintf ('WHERE %s', implode ("\nAND\n", $aWhereLines)) : ''
                );

                $pResult = Database :: getInstance () -> query ($sQuery);
                
                $aResult = $pResult -> fetch_assoc ();
                $aResult ['content'] = html_entity_decode ($aResult ['content'], ENT_COMPAT, 'UTF-8');
                
                if (!empty ($aResult ['photos']))
                {
                        $aResult ['photos'] = $this -> getPhotosByIds ($aResult ['photos']);
                }
                
                return $aResult;
        }

        /**
        * @desc selects the tree of a module
        * 
        * @param int $iItemId - id of an item
        * @param string $sItemSlug - slug of an item 
        * 
        * @return array or false by error
        */

        public function getOnlyTree ($iItemId = 0, $sItemSlug = '', $sLanguage = 'default')
        {                 
                if (!is_numeric ($iItemId))
                {
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                    SELECT
                        t.oid,
                        t.order_number,
                        p.photos,
                        t.private,
                        p.name,
                        p.content,
                        p.page_description,
                        p.slug
                    FROM
                        tree t
                    LEFT JOIN 
                        pages p
                            ON
                        t.oid = p.tree_oid
                    WHERE
                        t.type_oid = " . $this -> iTypeOid . "
                        " . (!empty ($iItemId) ? sprintf ("AND t.oid = %d", $iItemId) : '') . "
                        " . (!empty ($sItemSlug) ? sprintf ("AND p.slug = '%s'", $sItemSlug) : '') . "
                ");
                
                $aResults = array();
                    
                while ($aRow = $pResult -> fetch_assoc ()) 
                {
                        $aRow ['photos']  = $this -> getPhotosByIds ($aRow ['photos']);
                        $aRow ['content'] = html_entity_decode ($aRow ['content'], ENT_COMPAT, 'UTF-8');
                        $aResults []      = $aRow;
                }
                
                if ($iItemId != 0 || $sItemSlug != '')
                {
                        foreach ($this -> aLanguages as $lang)
                        {
                                $pResult = Database :: getInstance () -> query ("
                                        SELECT
                                            p.name,
                                            p.content,
                                            p.photos,
                                            p.page_description,
                                            p.slug
                                        FROM
                                            tree t
                                        LEFT JOIN 
                                            pages p
                                                ON
                                            t.oid = p.tree_oid
                                        LEFT JOIN
                                            languages l
                                                ON
                                            l.id = p.lang_id
                                        WHERE
                                            l.id = " . $lang ['id'] . "
                                                AND
                                            t.type_oid = " . $this -> iTypeOid . "
                                            " . (!empty ($iItemId) ? sprintf ("AND t.oid = %d", $iItemId) : '') . "
                                            " . (!empty ($sItemSlug) ? sprintf ("AND p.slug = '%s'", $sItemSlug) : '') . "
                                ");
                                
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {
                                        $aRow ['content']           = html_entity_decode ($aRow ['content'], ENT_COMPAT, 'UTF-8');
                                        $aResults [0][$lang ['id']] = $aRow;
                                }       
                        }
                }
                
                $aResults = (!empty ($iItemId) || !empty ($sItemSlug)) ? current ($aResults) : $aResults;
                
                return $aResults;
        }

        /**
        * @desc: return an array with all data about images
        * 
        * @param string $sImages
        * 
        * @returns: array
        */ 
         
        public function getPhotosByIds ($sImages)
        {
                if (empty ($sImages))
                {
                        return false;
                }
                
                $aImages        = explode (',', $sImages);
                $aProductPhotos = array ();
                
                foreach ($aImages as $image_id)
                {
                        if ($image_id != '')
                        {
                                $aDetailPhoto = $this -> getImage ($image_id);
                                
                                if (isset ($aDetailPhoto ['id']) && $aDetailPhoto ['id'] != '')
                                {
                                        array_push ($aProductPhotos, $aDetailPhoto);
                                }
                        }
                }
                
                
                for ($i = 0; $i < count ($aProductPhotos); $i++)
                { 
                        $aPathImages = explode ('/', $aProductPhotos [$i]['image_name'], -1);
                        $sPath       = '';

                        foreach ($aPathImages as $folder)
                        {
                                if ($folder != '')
                                {
                                        $sPath .= '/' . $folder;
                                }
                        }

                        $aProductPhotos [$i]['pathphoto'] = $sPath;
                }
                
                 return $aProductPhotos;
        }
        
        /**
        * @desc: changed the specified news 
        * 
        * @param int $iId
        * 
        * @returns booleaan
        **/
        
        public function getImage ($iId = '')
        {
                if ($iId == '' || !is_numeric ($iId))
                {
                        return array ();
                }
                  
                $pQuery = Database :: getInstance () -> query ("
                        SELECT
                        *
                        FROM
                            images
                        WHERE
                            id = " . $iId . "
                        ORDER BY
                            id DESC
                        "
                );
                
                $array = array (); 
                  
                if ($pQuery -> num_rows > 0)
                {
                        while ($aRow = $pQuery -> fetch_assoc ())
                        {
                                return $aRow;
                        }
                }
                
                return array();
        }



        /**
        * @desc get the parents and children
        * 
        * 
        * @returns array;
        */
		 	 	 	        
        public function getMenu ()
        {
	            $this -> aMenu = null;
	            
                $pResult = Database :: getInstance () -> query ("
		                SELECT
			                *	
		                FROM
			                tree
		                WHERE
			                level = 1
				                AND
			                type_oid = " . $this -> iTypeOid . "
		                ORDER BY 
			                lft
		        ");
                
	            if ($pResult)
                {
                        $aResults = array();
                        
                        while ($row = $pResult -> fetch_assoc ()) 
                        {
                                $aResults [] = $row;
                        }
                }
                else
                {
		                return false;
                }
                
	            foreach ($aResults as $parent) 
                {
		                $this -> aParents [$parent ['oid']] = array (
			                    'name'  => $parent ['name'],
			                    'level' => $parent ['level']
		                );
	            }
                
	            foreach ($this -> aParents as $oid => $parent) 
                {
		                if ($aChilds = $this -> getChilds ($oid, '', $parent ['level'] + 1)) 
                        {
			                    $this -> aMenu [$oid]['name']     = $parent ['name'];
			                    $this -> aMenu [$oid]['children'] = $aChilds;
                                
			                    if (!empty($this -> aMenu [$oid]['children'])) 
                                {
				                        foreach ($this -> aMenu [$oid]['children'] as $key => $child) 
                                        {
					                            if ($aSubChilds = $this -> getChilds ($child ['oid'], '', $child ['level']+1)) 
                                                {
						                                $this -> aMenu [$oid]['children'][$key]['children'] = $aSubChilds;
					                            }
				                        }
			                    }
		                } 
                        else 
                        {
			                    $this -> aMenu [$oid]['name'] = $parent ['name'];
		                }
	            }
                
	            return $this -> aMenu;
        }

        /**
        * @desc check the structure of tree
        */

        public function checkTree () 
        {          
                $pResult = Database :: getInstance () -> query ("
	                    SELECT 
                            oid,
		                    lft, 
		                    rgt
                        FROM 
		                    tree
                        WHERE 
		                    type_oid = " . $this -> iTypeOid . "
                        ORDER BY 
		                    lft ASC
                        "
                );

                if ($pResult -> num_rows > 0)
                {
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                $aTree [$aRow ['oid']] = $aRow;
                        }
                }
                           
                $aWrongTree = array_keys ($aTree);  
                  
                foreach ($aTree as $tak) 
                {
                        if (in_array ($tak, $aWrongTree)) 
                        {
		                        error_log ('Tree repaired by ' . $_SERVER ['HTTP_HOST'], 1, 'info@inforitus.nl');
                                $this -> RepairTree ();
                        } 
                        else 
                        {
                                $aWrongTree [] = $tak;
                        }
                }
        }
        
        /**
        * @desc repaire the tree
        */

        public function RepairTree () 
        {
                $pResult = Database :: getInstance () -> query ("
                        SELECT lft, rgt ,level, oid, name
                            FROM tree
                        WHERE
                            type_oid = " . $this -> iTypeOid . "
                            ORDER BY lft ASC
                ");
                    
                $aTree = array ();   
                  
                if ($pStatement -> num_rows > 0)
                {
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                $aTree [] = $aRow;
                        }
                }

                $iNodeValue  = 1;
                $aStack      = array ();
                $iCounter    = 0;    
                $aNewTree    = array ();

                for ($i = 0; $i < count ($aTree); $i++) 
                {
                        $iCounter++;
                    
                        if (intval ($aTree [$i]['level']) == intval ($aTree [$i+1]['level'])) 
                        {
                                // two children the same age
                                // update lft and rgt
                                $aNewTree [$aTree [$i]['oid']] = array (
                                        'oid'  => $aTree [$i]['oid'],
                                        'name' => $aTree [$i]['name'],
                                        'lft'  => $iNodeValue,
                                        'rgt'  => $iNodeValue + 1,
                                );
                            
                                $iNodeValue = $iNodeValue + 2;
                        } 
                        elseif (intval ($aTree [$i]['level']) < intval ($aTree [$i+1]['level'])) 
                        {
                            // have childs
                                $aNewTree [$aTree [$i]['oid']] = array (
                                        'oid'  => $aTree [$i]['oid'],
                                        'name' => $aTree [$i]['name'],
                                        'lft'  => $iNodeValue,
                                );
                                
                                // throw on the stack;
                                array_push ($aStack, $aTree [$i]);
                                // update next iNodeValue
                                $iNodeValue++;
                        } 
                        else 
                        {
                                if (intval ($aTree [$i]['level']) == intval ($aTree [$i+1]['level'])) 
                                {
                                        // two children the same age
                                        // update lft and rgt
                                        $aNewTree [$aTree [$i]['oid']] = array (
                                                'oid'  => $aTree [$i]['oid'],
                                                'name' => $aTree [$i]['name'],
                                                'lft'  => $iNodeValue,
                                                'rgt'  => $iNodeValue + 1,
                                        );
                                        
                                        $iNodeValue = $iNodeValue + 2;
                                } 
                                elseif (intval ($Tree [$i]['level']) < intval ($Tree [$i+1]['level'])) 
                                {
                                        // have childs
                                        $aNewTree [$aTree [$i]['oid']] = array (
                                                'oid'  => $aTree [$i]['oid'],
                                                'name' => $aTree [$i]['name'],
                                                'lft'  => $iNodeValue,
                                        );
                                        // throw on the stack;
                                        array_push ($aStack, $aTree [$i]);
                                        // update next iNodeValue
                                        $iNodeValue++;
                                } 
                                else 
                                {
                                    // This is a category without children and the last of its level
                                        if (!empty ($newTree [$aTree [$i]['oid']])) 
                                        {
                                                $aNewTree [$aTree [$i]['oid']]['rgt'] = $iNodeValue;
                                                $iNodeValue++;
                                            
                                        } 
                                        else 
                                        {
                                                $aNewTree [$aTree [$i]['oid']] = array (
                                                        'oid'  => $aTree [$i]['oid'],
                                                        'name' => $aTree [$i]['name'],
                                                        'lft'  => $iNodeValue,
                                                        'rgt'  => $iNodeValue + 1,
                                                );
                                                
                                                $iNodeValue++;
                                                $iNodeValue++;
                                        }
                                }
                                
                                $iLevel      = intval ($aTree [$i+1]['level']);
                                $stackLength = $iLevel;
                                
                                while (!empty ($aStack) && count ($aStack) >= $stackLength) 
                                {
                                        $TreeElement                            = array_pop ($aStack);
                                        $aNewTree [$TreeElement ['oid']]['rgt'] = $iNodeValue;
                                        $iNodeValue++;
                                }                
                        }
                }

                while (!empty ($aStack)) 
                {
                        $TreeElement                            = array_pop ($aStack);
                        $aNewTree [$TreeElement ['oid']]['rgt'] = $iNodeValue;
                        $iNodeValue++;
                }

                foreach ($aNewTree as $newTreeElement) 
                {
                        $pStatement = Database :: getInstance () -> prepare (
                                "Update tree set lft = %d, rgt = %d where oid = %d"
                        );
                        
                        $pStatement -> bind_param ('ddd',
                                $newTreeElement ['lft'],
                                $newTreeElement ['rgt'],
                                $newTreeElement ['oid']
                        );
                        
                        $pStatement -> execute ();
                }
        }

}

?>
