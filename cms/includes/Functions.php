<?php

/**
* @desc builds pagination
* @author Andrei Dragos <andrei@inforitus.nl>
* 
* @param int $iStartItem      - determine the current page
* @param int $iMaxItems       - the total number of items
* @param int $iItemsOnPage    - the number of items that should appear on a page
* @param string $sParams      - the parameters that will appear in the links of pagination
* @param string $sActiveClass - the class that will be putted on active page
* 
* @return string
*/

function pagination ($iStartItem, $iMaxItems, $iItemsOnPage, $sParams, $sActiveClass = '') 
{
        if ($iStartItem > $iMaxItems) 
        {
                return '';
        }
        
        // Compute current page
        if ($iStartItem != 0)
        {
                $iCurrentPage = round ($iStartItem / $iItemsOnPage + 1);
        }
        else
        {
                $iCurrentPage = 1;
        }
        
        // Compute starting page
        if ($iCurrentPage % 10 != 0) 
        {
                $iStart = ($iCurrentPage - $iCurrentPage % 10) + 1;
        } 
        else 
        {
                $iStart = ($iCurrentPage - $iCurrentPage % 10) - 9;
        }
        
        if ($iStart < 0) 
        {
                $iStart = 1;
        }
            
        $iEnd = $iStart + 9;
        
        // Compute total no. of pages
        if ($iMaxItems != 0) 
        {
                $iTotalPages = $iMaxItems / $iItemsOnPage;
        }
        
        if ($iMaxItems % $iItemsOnPage != 0) 
        {
                $iTotalPages ++;
        }
        
        if ($iEnd > $iTotalPages) 
        {
            $iEnd = $iTotalPages;
        }
        
        $sPages = '';
        // Previous list
        if (($iStart - 10) >= 0) 
        {
                $sPages .= "<a href=\"http://" . $_SERVER['SERVER_NAME'] . "/" . $sParams . ($iStart - 2) . ".html\">...</a> &nbsp;|&nbsp; ";
        }
        
        // Previous page
        if ($iStartItem != 0) 
        {
                $sPages .= "<a class=\"previous-page\" href=\"http://" . $_SERVER['SERVER_NAME'] . "/" . $sParams . ($iCurrentPage - 2) . ".html\">&laquo;</a> ";
        }
        
        // List of pages
        for ($i = $iStart; $i <= $iEnd; $i++) 
        {
                $sPages .= " <a href=\"http://" . $_SERVER['SERVER_NAME'] . "/" . $sParams . (($i - 1)) . ".html\" " . ($i == $iCurrentPage ? 'class="' . $sActiveClass . '"' : '') . ">" . $i . "</a> -";
        }
        
        $sPages = substr ($sPages, 0, -1);
        
        // Next page   
        if (ceil ($iStartItem / $iItemsOnPage) != (floor ($iTotalPages) - 1) && $iMaxItems != 0) 
        {
                $sPages .= "<a class=\"next-page\" href=\"http://" . $_SERVER['SERVER_NAME'] . "/" . $sParams . $iCurrentPage . ".html\">&raquo;</a>";
        }
            
        // Next list
        if (($iStart + 10) <= $iTotalPages) 
        {
                $sPages .= "<a href=\"http://" . $_SERVER['SERVER_NAME'] . "/" . $sParams . $iEnd . "/\">...</a>";
        }           
        
        return $sPages;
}

/**
* @desc make a number in string format
*
* @param float $fValue
* 
* @return string
*/

function createStringNumber ($fValue)
{
        $fValue   = round ($fValue, 2);
        $aNumber = explode ('.', $fValue);
        $sNumber = '';
        $iCount  = 0;
        
        while(ceil ($aNumber [0] / 1000) > 0)
        {   
                if(strlen ($aNumber [0] % 1000) == 3 || !((int)($aNumber [0] / 1000) > 0))
                        $sNumber = '.' . ($aNumber [0] % 1000) . $sNumber;
                        
                elseif (ceil ($aNumber [0] / 1000) > 0 && strlen ($aNumber [0] % 1000) == 2)
                        $sNumber = '.0' . ($aNumber [0] % 1000) . $sNumber;   
                        
                elseif (ceil ($aNumber [0] / 1000) > 0 && strlen ($aNumber [0] % 1000) == 1)
                        $sNumber = '.00' . ($aNumber [0] % 1000) . $sNumber;   
                           
                $aNumber[0] = (int)($aNumber [0] / 1000);
        }
        
        if (!isset ($aNumber [1]))
        {
                $aNumber [1] = 0;
        }
        
        $sNumber = ($sNumber != '' ? substr ($sNumber, 1) : 0) . ',' . ($aNumber[1] < 10 && $aNumber[1] > 0 ? $aNumber[1] . '0' : ($aNumber [1] > 10 ? round ($aNumber[1], 2) : '00'));
        
        return $sNumber;
}



/**
* @desc prints on screen the values of an array or object
*
* @param array/object/variable $aValues
* 
*/
function dump ($aValues = array ())
{
        echo '<pre>'; print_r($aValues); echo '<pre>'; die;
}

/**
* @desc return the keywords of a content
*
* @param string $sContent
* 
* @return array
*/

function keywords ($sContent = '')
{
        $sIgnoreWords = file_get_contents ('../../words.txt');
        $aIgnoreWords = explode ("\n", $sIgnoreWords);
        
        preg_match_all ( '/[a-zA-Z0-9\-\_\.]+/' , strip_tags ($sContent), $aWords);
        
        $aWords = current ($aWords);
        $aCount = array ();
        
        foreach ($aWords as $sWord) 
        {
                $sWord = strtolower ($sWord); // ignore case 
                
                if (strlen ($sWord) < 3 || in_array ($sWord, $aIgnoreWords)) continue; // ignore words shorter than 3 letters
                
                $aCount [$sWord] = (isset ($aCount [$sWord]) ? $aCount [$sWord] + 1 : 0);
        }
        
        $aKeywords = array ();
        
        foreach ($aCount as $sWord => $iNumber) if ((100*$iNumber)/count($aWords) > 1.5) $aKeywords [$sWord] = round ((100*$iNumber)/count($aWords), 2);
        
        arsort ($aKeywords);
        
        return $aKeywords;
}

/**
* @desc return truncated content counting except html tags
*
* @param string $sText
* @param string $iMaxLength  - number of chars
* 
* @return string
*/

function html_cut ($sText, $iMaxLength)
{
        $aTags   = array();
        $sResult = "";
    
        $bIsOpen   = false;
        $bGrabOpen = false;
        $bIsClose  = false;
        $sTag = "";
    
        $i = 0;
        $iStripped = 0;
    
        $sStrippedText = strip_tags ($sText);
    
        while ($i < strlen ($sText) && $iStripped < strlen ($sStrippedText) && $iStripped < $iMaxLength)
        {
                $sSymbol  = $sText{$i};
                $sResult .= $sSymbol;
        
                switch ($sSymbol)
                {
                        case '<':
                                $bIsOpen   = true;
                                $bGrabOpen = true;
                                break;
            
                        case '/':
                                if ($bIsOpen)
                                {
                                        $bIsClose  = true;
                                        $bIsOpen   = false;
                                        $bGrabOpen = false;
                                }
                
                                break;
            
                        case ' ':
                                if ($bIsOpen)
                                        $bGrabOpen = false;
                                else
                                        $iStripped++;
                
                                break;
            
                        case '>':
                                if ($bIsOpen)
                                {
                                        $bIsOpen   = false;
                                        $bGrabOpen = false;
                                        array_push ($aTags, $sTag);
                                        $sTag = "";
                                }
                                else if ($bIsClose)
                                {
                                        $bIsClose = false;
                                        array_pop($aTags);
                                        $sTag = "";
                                }
                
                                break;
            
                        default:
                                if ($bGrabOpen || $bIsClose)
                                        $sTag .= $sSymbol;
                
                                if (!$bIsOpen && !$bIsClose)
                                        $iStripped ++;
                }
        
                $i++;
        }
    
        while ($aTags)
                $sResult .= "</" . array_pop ($aTags) . ">";
    
        return $sResult;
}
?>
