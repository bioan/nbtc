<?php

/**
     @description: language class
     @link: http://www.inforitus.nl
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class Language 
{ 
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        *@desc: contructor 
        *
        */
                      
        public function Language () 
        { 
            
        }
        
        /**
        * @desc: get the last errors
        *
        * @returns array
        */
         
        public function getLastError ()
        {
                return $this -> aError;
        }  
         
        /**
        * @desc create the slug of an item
        * 
        * @param string $sParams - string to be converted
        * 
        * @returns string
        */
             
        public function urlize ($sParams)
        {
                $sTitle   = strtolower ($sParams);
                $aAllowed = preg_split ('//', 'abcdefghijklmnopqrstuvwxyz0123456789-+_');

                for ($i = 0, $j = strlen ($sTitle); $i < $j; $i++)
                {
                        if (!in_array ($sTitle  [$i], $aAllowed))
                        {
                                $sTitle  [$i] = '-';
                        }
                }

                for ($i = 0; $i < 3; $i++)
                {
                        $sTitle = str_replace ('--', '-', $sTitle);
                }
                
                return preg_replace ('/^ [\-]*(.+?) [\-]*$/s', '\\1', $sTitle);
        }    
         
        /**
        * @desc: get one or all the languages 
        *
        * @param int $iItemId - id of the item
        * @param string $ItemSlug - slug of the item
        * @param string $sSortField - sort the result by this field
        * @param string $sSortType - desc/asc
        *  
        * @return array OR booleaan by error
        */
                                           
        public static function getLanguages ($iItemId = 0,  $sItemShortName = '', $sSortField = 'id', $sSortType = 'asc', $bDefault = false)
        {        
                if (!is_numeric ($iItemId))
                {          
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            *
                        FROM 
                            languages
                        WHERE 
                                1
                                " . (!empty ($iItemId)        ? sprintf ("AND languages.id = %d ", $iItemId) : '') . "
                                " . (!empty ($sItemShortName) ? sprintf ("AND languages.short_name = '%s' ", Database :: getInstance () -> real_escape_string ($sItemShortName)) : '') . "                         
                                " . ($bDefault == true        ? sprintf ("AND `default` = 1") : '') . "
                        ORDER BY 
                            " . $sSortField . " " . $sSortType . "    
                    ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                $aResults [] = $aRow;
                        }
                }
                
                $aResults = (!empty ($iItemId) || !empty ($sItemShortName) || $bDefault == true) ? current ($aResults) : $aResults;
            
                return $aResults;
        }
        
        /**
        * @desc: check the fields
        *
        * @param array $aValues - fields that have to be checked
        * 
        * @returns booleaan
        */
                            
        private function checkFields ($aValues = array ())
        {
                if (empty ($aValues ['language'])) 
                {
                        $this -> aError [] = "Name";
                }
                
                if (empty ($aValues ['short_name'])) 
                {
                        $this -> aError [] = "Short Name";
                }
                 
                if ($this -> aError) 
                {          
                        return false;
                }
                
                return true;
        }
        
        /**
        * @desc: insert the language item in the database
        *
        * @param array $aValues - the data of the item
        * 
        * @returns booleaan
        */
                                      
        public function addLanguage ($aValues = array ())
        {
                if ($this -> checkFields ($aValues)) 
                {
                        
                        $aDefaultLang = $this -> getLanguages (0, '', 'id', 'asc', true);
                        
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT INTO
                                    languages
                                    (
                                        language,
                                        short_name,
                                        slug,
                                        private
                                    )
                                VALUES
                                    (
                                        ?,
                                        ?,
                                        ?,
                                        ?
                                    )
                        ");
                        
                        $sSlug                = $this -> urlize ($aValues ['language']);
                        $iPrivate             = (isset ($aValues ['private']) ? 1 : 0); 
                        
                        $pStatement -> bind_param ('sssi',
                                $aValues ['language'],
                                $aValues ['short_name'],
                                $sSlug,
                                $iPrivate
                        );
                        
                        if (!$pStatement -> execute ()) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                
                                return false;
                        }
                        
                        $iInsert_id = $pStatement -> insert_id;
                        
                        if (isset ($aValues ['image']['tmp_name']) && $aValues ['image']['size'] > 0)
                                copy ($aValues ['image']['tmp_name'], '../../images/lang/' . $iInsert_id . '.' . end(explode(".", $aValues ['image']['name'])));   
                        
                        copy ('../../lang/' . $aDefaultLang ['short_name'] .'.inc', '../../lang/' . $aValues ['short_name'] . '.inc');   
                        
                        $pResult = Database :: getInstance () -> query ("
                                SELECT 
                                    *
                                FROM 
                                    pages
                                GROUP BY tree_oid
                            ");
                        
                        $aResults = array (); 
                        
                        if (!$pResult)
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }  
                          
                        if ($pResult -> num_rows > 0)
                        {         
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {          
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                INSERT INTO
                                                    pages
                                                    (
                                                        tree_oid,
                                                        lang_id
                                                    )
                                                VALUES
                                                    (
                                                        ?,
                                                        ?
                                                    )
                                        ");
                                        
                                        $pStatement -> bind_param ('ii',
                                                $aRow ['tree_oid'],
                                                $iInsert_id
                                        );
                                        
                                        if (!$pStatement -> execute ()) 
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                                
                                                return false;
                                        }
                                }
                        }
                        
                        $pResult = Database :: getInstance () -> query ("
                                SELECT 
                                    *
                                FROM 
                                    groups_data
                                GROUP BY group_id 
                            ");
                        
                        $aResults = array (); 
                        
                        if (!$pResult)
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }  
                          
                        if ($pResult -> num_rows > 0)
                        {         
                                while ($aRow = $pResult -> fetch_assoc ()) 
                                {          
                                        $pStatement = Database :: getInstance () -> prepare ("
                                                INSERT INTO
                                                    groups_data
                                                    (
                                                        group_id,
                                                        lang_id
                                                    )
                                                VALUES
                                                    (
                                                        ?,
                                                        ?
                                                    )
                                        ");
                                        
                                        $pStatement -> bind_param ('ii',
                                                $aRow ['group_id'],
                                                $iInsert_id
                                        );
                                        
                                        if (!$pStatement -> execute ()) 
                                        {
                                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);  
                                                
                                                return false;
                                        }
                                }
                        }
                        
                        return true;
                }
                
                return false;
        }

        /**
        * @desc: update a language item
        *
        * @param int $iId - id of the item
        * @param array $aValues - the data of the item
        * 
        * @return booleaan
        */
                                 
        public function updateLanguage ($iId, $aValues = array ())
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                
                if ($this -> checkFields ($aValues)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    languages
                                SET
                                    language         = ?,
                                    short_name       = ?,
                                    slug             = ?,
                                    private          = ?    
                                WHERE
                                    id               = ? 
                        ");
                        
                        $sSlug                = $this -> urlize ($aValues ['language']);  
                        $iPrivate             = (isset ($aValues ['private']) ? 1 : 0);      
                        
                        $pStatement -> bind_param ('sssii',
                                $aValues ['language'],
                                $aValues ['short_name'],
                                $sSlug,
                                $iPrivate,
                                $iId
                        );
                        
                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        if (isset ($aValues ['image']['tmp_name']) && $aValues ['image']['size'] > 0)
                                copy ($aValues ['image']['tmp_name'], '../../images/lang/' . $iId . '.' . end (explode (".", $aValues ['image']['name'])));   
                        
                        
                        return true;
                }
                
                return false;
        }
        
        /**
        * @desc: delete a language item
        *
        * @param int $iId - id of the item
        * 
        * @return booleaan   
        */
                              
        public function deleteLanguage ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                
                $aLang = $this -> getLanguages ($iId);
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            languages
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            agenda_data
                        WHERE
                            lang_id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            groups_data
                        WHERE
                            lang_id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            news_data
                        WHERE
                            lang_id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            pages
                        WHERE
                            lang_id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            photoalbum_data
                        WHERE
                            lang_id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            videoalbum_data
                        WHERE
                            lang_id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            webshop_products_data
                        WHERE
                            lang_id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                unlink ('../../lang/' . $aLang ['short_name'] . '.inc');
                
                return true;
        }
}   
?>
