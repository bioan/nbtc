<?php   

/**
     @description: newsletter class
     @link: http://www.inforitus.nl
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class Newsletter 
{ 
        /**
        * @desc contains the errors of the form data 
        * 
        * @var array
        */
        
        var $aError = array ();
        
        /**
        * @desc contains the list of the users with errors 
        * 
        * @var array
        */
        
        var $aList = array ();
        
        /**
        * @desc contains all languages 
        * 
        * @var array
        */
        
        var $aLanguages = array ();
        
        /**
        * @desc contains default language 
        * 
        * @var array
        */
        
        var $aDefaultLang = array ();
        
        /**
        *@desc: contructor 
        *
        */
                      
        public function Newsletter () 
        { 
                $this -> aLanguages   = Language :: getLanguages ();
                $this -> aDefaultLang = Language :: getLanguages (0, '', 'id', 'asc', true);   
        }
        
        /**
        * @desc: get the last errors
        *
        * @returns array
        */
         
        public function getLastError ()
        {
                return $this -> aError;
        }  
         
        /**
        * @desc create the slug of an item
        * 
        * @param string $sParams - string to be converted
        * 
        * @returns string
        */
             
        public function urlize ($sParams)
        {
                $sTitle   = strtolower ($sParams);
                $aAllowed = preg_split ('//', 'abcdefghijklmnopqrstuvwxyz0123456789-+_');

                for ($i = 0, $j = strlen ($sTitle); $i < $j; $i++)
                {
                        if (!in_array ($sTitle  [$i], $aAllowed))
                        {
                                $sTitle  [$i] = '-';
                        }
                }

                for ($i = 0; $i < 3; $i++)
                {
                        $sTitle = str_replace ('--', '-', $sTitle);
                }
                
                return preg_replace ('/^ [\-]*(.+?) [\-]*$/s', '\\1', $sTitle);
        }   
         
        /**
        * @desc: get one or all the photos 
        *
        * @param int $iItemId - id of the item
        * @param string $ItemSlug - slug of the item
        * @param int $iTemplateId - id of template
        * @param string $sSortField - sort the result by this field
        * @param string $sSortType - desc/asc
        * @param string $sLanguage - language
        *  
        * @return array OR booleaan by error
        */
                                           
        public function getPhotos ($iItemId = 0,  $sItemSlug = '', $iTemplateId = 0, $sSortField = 'order_number', $sSortType = 'desc', $sLanguage = 'default')
        {        
                if (!is_numeric ($iItemId) || !is_numeric ($iTemplateId))
                {          
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            n.*,
                            DATE_FORMAT(n.date_register, '%d-%m-%Y om %H:%i') AS date_register_format,
                            g.id AS newsgroup_id,
                            gd.group_name
                        FROM 
                            newsletter_items n
                        LEFT JOIN
                            groups g
                                ON
                            n.group_id = g.id
                        LEFT JOIN
                            groups_data gd
                                ON
                            gd.group_id = n.group_id
                        LEFT JOIN
                            languages l
                                ON
                            (gd.lang_id = l.id)
                        WHERE 
                            1
                            " . ($sLanguage == 'default' ? sprintf ("AND l.default = 1") : sprintf ("AND l.short_name = '%s'", Database :: getInstance () -> real_escape_string ($sLanguage))) . "
                            " . (!empty ($iItemId)        ? sprintf ("AND n.id = %d ", $iItemId) : '') . "
                            " . (!empty ($sItemSlug)      ? sprintf ("AND n.title = '%s' ", Database :: getInstance () -> real_escape_string ($sItemSlug)) : '') . "  
                            " . (!empty ($iTemplateId)   ? sprintf ("AND n.group_id = %d ", $iTemplateId) : '') . "
                                                        
                        ORDER BY 
                            " . $sSortField . " " . $sSortType . "    
                    ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                $aRow ['photos'] = $this -> getPhotosByOids ($aRow ['photos']);
                                $aRow ['description'] = html_entity_decode ($aRow ['description'], ENT_COMPAT, 'UTF-8');
                                $aResults [] = $aRow;
                        }
                }
                
                $aResults = (!empty ($iItemId) || !empty ($sItemSlug)) ? current ($aResults) : $aResults;
            
                return $aResults;
        }
        
        /**
        * @desc: get one or all the subscribers 
        *
        * @param int $iItemId - id of the item
        * @param int $iGroupId - id of group
        * @param string $sSortField - sort the result by this field
        * @param string $sSortType - desc/asc
        *  
        * @return array OR booleaan by error
        */
                                           
        public function getSubscribers ($iItemId = 0,  $iGroupId = 0, $sSortField = 'name', $sSortType = 'asc')
        {        
                if (!is_numeric ($iItemId) || !is_numeric ($iGroupId))
                {          
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            s.*,
                            DATE_FORMAT(s.date, '%d-%m-%Y') AS date_register_format,
                            gd.group_name
                        FROM 
                            newsletter_subscribers s
                        LEFT JOIN
                            groups g
                                ON
                            s.group_id = g.id
                        LEFT JOIN
                            groups_data gd
                                ON
                            gd.group_id = s.group_id
                        LEFT JOIN
                            languages l
                                ON
                            (gd.lang_id = l.id)
                        WHERE 
                            1
                            " . (!empty ($iItemId)        ? sprintf ("AND s.id = %d ", $iItemId) : '') . "
                            " . (!empty ($iGroupId)   ? sprintf ("AND s.group_id = %d ", $iGroupId) : '') . "
                                                        
                        ORDER BY 
                            " . $sSortField . " " . $sSortType . "    
                    ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                $aResults [] = $aRow;
                        }
                }
                
                $aResults = (!empty ($iItemId)) ? current ($aResults) : $aResults;
            
                return $aResults;
        }
        
        /**
        * @desc: get one or all the mailings 
        *
        * @param int $iItemId - id of the item
        * @param string $sSortField - sort the result by this field
        * @param string $sSortType - desc/asc
        *  
        * @return array OR booleaan by error
        */
                                           
        public function getMailings ($iItemId = 0, $iTemplateId = 0, $sSortField = 'id', $sSortType = 'desc')
        {        
                if (!is_numeric ($iItemId))
                {          
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            m.*,
                            DATE_FORMAT(m.date, '%d-%m-%Y %H:%i') AS date_register_format,
                            gd.group_name as subscribers,
                            tgd.title as group_name,
                            (SELECT count(*) FROM newsletter_mailings m2 LEFT JOIN groups_data tgd on (m2.template_id=tgd.group_id) WHERE m.template_id = m2.template_id) as count_sends
                        FROM 
                            newsletter_mailings m
                        LEFT JOIN
                            groups_data gd
                                ON
                            gd.group_id = m.group_id
                        LEFT JOIN
                            newsletter_templates tgd
                                ON
                            tgd.id = m.template_id
                        LEFT JOIN
                            languages l
                                ON
                            (gd.lang_id = l.id)
                        WHERE 
                            l.default = 1 
                            " . (!empty ($iTemplateId)    ? sprintf ("AND tgd.id = %d ", $iTemplateId) : '') . "
                            " . (!empty ($iItemId)    ? sprintf ("AND m.id = %d ", $iItemId) : '') . "
                            " . (empty ($iItemId) && empty ($iTemplateId)   ? 'GROUP BY tgd.title' : '') . "                             
                        ORDER BY 
                            " . $sSortField . " " . $sSortType . "    
                    ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {       
                                $aRow ['links'] = json_decode ($aRow ['links'], true);
                                $aResults [] = $aRow;
                        }
                }
                
                $aResults = (!empty ($iItemId)) ? current ($aResults) : $aResults;
            
                return $aResults;
        }
        
        /**
        * @desc: get last id of mailings
        *
        *  
        * @return array OR booleaan by error
        */
                                           
        public function getLastMailingId ()
        {        
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            MAX(id) as id_max
                        FROM 
                            newsletter_mailings                      
                        ORDER BY 
                            id desc    
                    ");
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                $aRow = $pResult -> fetch_assoc ();
                  
                if ($aRow ['id_max'] > 0)      
                        return $aRow ['id_max'] + 1;
                else
                {
                        $pResult = Database :: getInstance () -> query ("
                                TRUNCATE newsletter_mailings                      
                                    
                        ");
                        
                        return 1;
                }
                
        }
        
        /**
        * @desc: get clicks of links of subscribers  
        *
        * @param int $iItemId - id of the item
        * @param string $sSortField - sort the result by this field
        * @param string $sSortType - desc/asc
        *  
        * @return array OR booleaan by error
        */
                                           
        public function getMonitor ($iItemId = 0, $iSubscriberId = 0, $sSortField = 'id', $sSortType = 'desc')
        {        
                if (!is_numeric ($iItemId))
                {          
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            m.*,
                            s.email,
                            s.name
                        FROM 
                            newsletter_monitor m
                        LEFT JOIN
                            newsletter_subscribers s
                                ON
                            (s.id = m.subscriber_id)
                        WHERE 
                        1
                            " . (!empty ($iItemId)    ? sprintf (" AND m.mailing_id = %d ", $iItemId) : '') . "
                            " . (!empty ($iSubscriberId)    ? sprintf (" AND m.subscriber_id = %d ", $iSubscriberId) : '') . "
                        ORDER BY 
                            " . $sSortField . " " . $sSortType . "    
                    ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {       
                                $aRow ['values'] = json_decode ($aRow ['values'], true);
                                $aResults [$aRow ['subscriber_id']] = $aRow;
                        }
                }
                
                if (is_numeric ($sSortField))
                {
                        $aSort = array ();
                        
                        foreach ($aResults as $k => $item)
                        {
                                $aSort [$item ['id']] = $item ['values'][$sSortField];              
                        }
                        
                        if (strtolower ($sSortType) == 'asc')
                                asort ($aSort);
                        else
                                arsort ($aSort);
                        
                        $aTResults = array ();
                        
                        foreach ($aSort as $sk => $sitem)
                        {
                                foreach ($aResults as $k => $item)
                                {
                                        if ($item ['id'] == $sk)
                                                $aTResults [] = $aResults [$k];              
                                }
                        }
                        
                        $aResults = $aTResults;
                        
                }
                
                return $aResults;
        }
        
        /**
        * @desc: return an array with all data about images
        * 
        * @param string $sImages
        * 
        * @return array
        **/ 
         
        public function getPhotosByOids ($sImages)
        {
                if (empty ($sImages))
                {
                        return false;
                }
                
                $aImages        = explode (',', $sImages);
                $aProductPhotos = array ();
                
                foreach ($aImages as $image)
                {
                        if ($image != '')
                        {
                                $aDetailPhoto = $this -> getPhoto ($image);
                                
                                if (isset ($aDetailPhoto ['id']) && $aDetailPhoto ['id'] != '')
                                { 
                                        array_push ($aProductPhotos, $aDetailPhoto);
                                }
                        }
                }
                
                
                for ($i = 0; $i < count ($aProductPhotos); $i++)
                { 
                  
                        $aPathImages = explode ('/', $aProductPhotos [$i]['image_name'], -1);
                        $sPath       = '';

                        foreach ($aPathImages as $folder)
                        {
                                if ($folder != '')
                                {
                                        $sPath .= '/' . $folder;
                                }
                        }

                        $aProductPhotos [$i]['pathphoto'] = $sPath;
                }
                
                 return $aProductPhotos;
        }
        
        /**
        * @desc: get the data of an image 
        * 
        * @param int $iId
        * 
        * @return array
        */
        
        public function getPhoto ($iId = '')
        {
                if ($iId == '' || !is_numeric ($iId))
                {
                        return array ();
                }
                  
                $pQuery = Database :: getInstance () -> query ("
                        SELECT
                        *
                        FROM
                            images
                        WHERE
                            id = " . $iId . "
                        ORDER BY
                            id DESC
                        "
                );
                  
                if ($pQuery -> num_rows > 0)
                {
                        while ($aRow = $pQuery -> fetch_assoc ())
                        {
                                return $aRow;
                        }
                }
                
                return array ();
        }
        
        /**
        * @desc: check the fields
        *
        * @param array $aValues - fields that have to be checked
        * 
        * @returns booleaan
        */
                            
        private function checkFields ($aValues = array ())
        {
                if (empty ($aValues ['title'])) 
                {
                        $this -> aError [] = "Titel";
                }
                 
                if (empty ($aValues ['group_id'])) 
                {
                        $this -> aError [] = "Groep";
                }
                
                if ($this -> aError) 
                {          
                        return false;
                }
                
                return true;
        }
        
        /**
        * @desc: check the fields
        *
        * @param array $aValues - fields that have to be checked
        * 
        * @returns booleaan
        */
                            
        private function checkSubscriber ($aValues = array ())
        {
                if (isset($aValues ['file']) && $aValues ['file']['size'] != 0)
                {
                        if ($aValues ['file']['size'] > 90016734 || ($aValues ['file']['type'] != 'text/comma-separated-values') || (substr($aValues ['file']['name'], -3)  != 'csv'))
                        {
                                $this -> aError [] = 'CSV';
                        }
                } 
                else
                {
                        
                        if (empty ($aValues ['name'])) 
                        {
                                $this -> aError [] = "Naam";
                        }
                        
                        if (filter_var ($aValues ['email'], FILTER_VALIDATE_EMAIL) === false) 
                        {
                                $this -> aError [] = "Email";
                        }
                }
                         
                if (empty ($aValues ['group_id'])) 
                {
                        $this -> aError [] = "Groep";
                }
                
                if ($this -> aError) 
                {          
                        return false;
                }
                
                return true;
        }

        /**
        * @desc: insert the content item in the database
        *
        * @param array $aValues - the data of the item
        * 
        * @returns booleaan
        */
                                      
        public function addPhoto ($aValues = array ())
        {
                if ($this -> checkFields ($aValues)) 
                {
                        // take the item with the biggest sort number
                        
                        $aPhotoItems = $this -> getPhotos ();
                        $aPhotoItem  = (isset ($aPhotoItems [0]) ? $aPhotoItems [0] : '');
                        
                        // add post item
                        $pStatement = Database :: getInstance () -> prepare ("
                                INSERT INTO
                                    newsletter_items
                                    (
                                        group_id,
                                        date_register,
                                        photos,
                                        link,
                                        order_number,
                                        title,
                                        description
                                    )
                                VALUES
                                    (
                                        ?,
                                        NOW(),
                                        ?,
                                        ?,
                                        ?,
                                        ?,
                                        ?
                                    )
                        ");
                        
                        $sPhotos          = (isset ($aValues ['photos']) ? implode (',', $aValues ['photos']) : '');
                        $iOrderNumber     = (isset ($aNewsItem ['order_number']) ? $aNewsItem ['order_number'] + 1 : 0);
                        
                        $pStatement -> bind_param ('ississ',
                                $aValues ['group_id'],
                                $sPhotos,
                                $aValues ['link'],
                                $iOrderNumber,
                                $aValues ['title'],
                                $aValues ['description']
                        );
                        
                        if (!$pStatement -> execute ()) 
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        $this -> template ($aValues ['group_id']);
                        
                        return true;
                }
                
                return false;
        }
        
        /**
        * @desc: insert a template
        *
        * @param array $aValues - the data of the item
        * 
        * @returns booleaan
        */
                                      
        public function addTemplate ($aValues = array ())
        {
                // add post item
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT INTO
                            newsletter_templates
                            (
                                title,
                                fields,
                                `values`,
                                private,
                                date
                            )
                        VALUES
                            (
                                ?,
                                ?,
                                ?,
                                ?,
                                NOW()
                            )
                ");
                
                $pStatement -> bind_param ('ssss',
                        $aValues ['title'],
                        $aValues ['fields'],
                        $aValues ['values'],
                        $aValues ['private']
                );
                
                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                return true;
        }
        
        /**
        * @desc: insert mailing
        *
        * 
        * @return booleaan   
        */
        
        public function addMailing ($aValues = array ())
        {
                $pStatement = Database :: getInstance () -> prepare ("
                        INSERT INTO
                            newsletter_mailings
                            (
                                template_id,
                                group_id,
                                template_content,
                                links,
                                date
                            )
                        VALUES
                            (
                                ?,
                                ?,
                                ?,
                                ?,
                                NOW()
                            )
                ");
                
                $pStatement -> bind_param ('iiss',
                        $aValues ['template_id'],
                        $aValues ['group_id'],
                        $aValues ['template_content'],
                        $aValues ['links']
                );
                
                if (!$pStatement -> execute ()) 
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                return Database :: getInstance () -> insert_id;
        }
        
        /**
        * @desc: insert the subscriber in the database
        *
        * @param array $aValues - the data of the item
        * 
        * @returns booleaan
        */
                                      
        public function addSubscriber ($aValues = array ())
        {
                        if ($this -> checkSubscriber ($aValues)) 
                        {
                                $pResult = Database :: getInstance () -> query ("
                                SELECT 
                                    *
                                FROM 
                                     newsletter_subscribers
                                WHERE 
                                    email = '" . $aValues ['email'] . "'  
                            ");

                            if (!$pResult)
                            {
                                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);

                                    return false;
                            }  

                            if ($pResult -> num_rows == 0)
                            {  
                                    if (isset ($aValues ['file']) && $aValues ['file']['size'] != 0)
                                    {
                                            $bValidList = true;

                                            if (($handle = fopen ($aValues ['file']['tmp_name'], "r")) !== FALSE) 
                                            {

                                                    while (($aData = fgetcsv ($handle, 1000, $aValues ['delimiter'])) !== FALSE) 
                                                    {
                                                            $sName = trim ($aData [0]);
                                                            $sEmail = ($aData [1] == '' ? false : filter_var (trim (str_replace (' ', '', $aData [1])), FILTER_VALIDATE_EMAIL));
                                                            $sGender = trim ($aData [2]);
                                                            if ($sEmail !== false)
                                                            {
                                                                    $pStatement = Database :: getInstance () -> prepare ("
                                                                            INSERT INTO
                                                                                newsletter_subscribers
                                                                                (
                                                                                    group_id,
                                                                                    name,
                                                                                    email,
                                                                                    date,
                                                                                    code,
                                                                                    gender
                                                                                )
                                                                            VALUES
                                                                                (
                                                                                    ?,
                                                                                    ?,
                                                                                    ?,
                                                                                    NOW(),
                                                                                    ?,
                                                                                    ?
                                                                                )
                                                                    ");

                                                                    $sCode = $this -> random_password (32);

                                                                    $pStatement -> bind_param ('issss',
                                                                            $aValues ['group_id'],
                                                                            $sName,
                                                                            $sEmail,
                                                                            $sCode,
                                                                            $sGender
                                                                    );

                                                                    if (!$pStatement -> execute ()) 
                                                                    {
                                                                            //trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                                                            $this -> aList [] = $aData;
                                                                            $bValidList = false;
                                                                    }
                                                            }
                                                            else
                                                            {
                                                                    $this -> aList [] = $aData;
                                                                    $bValidList = false;
                                                            }
                                                    }
                                                    //die ();
                                                    return $bValidList;
                                            }

                                            fclose($handle);   

                                    }
                                    else
                                    {
                                            // add post item
                                            $pStatement = Database :: getInstance () -> prepare ("
                                                    INSERT INTO
                                                        newsletter_subscribers
                                                        (
                                                            group_id,
                                                            name,
                                                            email,
                                                            date,
                                                            code
                                                        )
                                                    VALUES
                                                        (
                                                            ?,
                                                            ?,
                                                            ?,
                                                            NOW(),
                                                            ?
                                                        )
                                            ");

                                            $sCode = $this -> random_password (32);

                                            $pStatement -> bind_param ('isss',
                                                    $aValues ['group_id'],
                                                    $aValues ['name'],
                                                    $aValues ['email'],
                                                    $sCode
                                            );
                                            if (!$pStatement -> execute ()) 
                                            {
                                                    trigger_error (Database :: getInstance () -> error, E_USER_WARNING);

                                                    return false;
                                            }
                        
                                            return true;
                                    }
                            }
                            else
                            {
                                return false;
                            }
                }
                
                return false;
        }

        /**
        * @desc: update a content item
        *
        * @param int $iId - id of the item
        * @param array $aValues - the data of the item
        * 
        * @return booleaan
        */
                                 
        public function updatePhoto ($iId, $aValues = array ())
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                
                if ($this -> checkFields ($aValues)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    newsletter_items
                                SET
                                    group_id         = ?,
                                    photos           = ?,
                                    link             = ?,
                                    title            = ?,
                                    description      = ?     
                                WHERE
                                    id               = ? 
                        ");
                        
                        $qPhotos           = (isset ($aValues ['photos']) ? implode (',', $aValues ['photos']) : '');
                        
                        $pStatement -> bind_param ('issssi',
                                $aValues ['group_id'],
                                $qPhotos, 
                                $aValues ['link'],
                                $aValues ['title'],
                                $aValues ['description'],
                                $iId
                        );
                        
                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        $this -> template ($aValues ['group_id']);
                        
                        return true;
                }
                
                return false;
        }
        
        /**
        * @desc: update a subscriber item
        *
        * @param int $iId - id of the item
        * @param array $aValues - the data of the item
        * 
        * @return booleaan
        */
                                 
        public function updateSubscriber ($iId, $aValues = array ())
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                
                if ($this -> checkSubscriber ($aValues)) 
                {
                        $pStatement = Database :: getInstance () -> prepare ("
                                UPDATE
                                    newsletter_subscribers
                                SET
                                    group_id         = ?,
                                    name             = ?,
                                    email            = ?     
                                WHERE
                                    id               = ? 
                        ");
                        
                        $pStatement -> bind_param ('issi',
                                $aValues ['group_id'],
                                $aValues ['name'],
                                $aValues ['email'],
                                $iId
                        );
                        
                        if (!$pStatement -> execute ())
                        {
                                trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                                
                                return false;
                        }
                        
                        return true;
                }
                
                return false;
        }
        
        /**
        * @desc: delete a content item
        *
        * @param int $iId - id of the item
        * 
        * @return booleaan   
        */
                              
        public function deletePhoto ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                
                $aItem = $this -> getPhotos ($iId);
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            newsletter_items
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                $this -> template ($aItem ['group_id']);
                
                return true;
        }
        
        /**
        * @desc: delete a subscriber
        *
        * @param int $iId - id of the subscriber
        * 
        * @return booleaan   
        */
                              
        public function deleteSubscriber ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            newsletter_subscribers
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                return true;
        }
        
        /**
        * @desc: delete a template
        *
        * @param int $iId - id of the template
        * 
        * @return booleaan   
        */
                              
        public function deleteTemplate ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE
                        FROM    
                            newsletter_templates
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                return true;
        }
        
        /**
        * @desc: create/update the template for browser
        *
        * @param int $iId - id of the template
        * 
        * @return booleaan   
        */
        
        public function template ($iId = 0, $iIdSent = 0)
        {
                $aTemplate = $this -> getTemplates ($iId);
                
                $aTemplate ['data'] = json_decode ($aTemplate ['values'], true);
                $sReturnTemplate = $aTemplate ['content'];
                
                if (!file_exists ($_SERVER ['DOCUMENT_ROOT'] . '/mail/' . $iId . '.html'))
                {
                        $file = fopen ($_SERVER ['DOCUMENT_ROOT'] . '/mail/' . $iId . '.html', 'w');
                        fwrite ($file, $aTemplate ['content']);
                        fclose ($file);
                }
                
                if (preg_match_all ('%{text_[^}]+}%s', $aTemplate ['content'], $aMatch))
                {
                        $aTexts = $aMatch [0];
                }
                
                if (isset($aTexts))
                        foreach ($aTexts as $text)
                        {
                                if ($text != '{text_text2}' && isset ($aTemplate ['data'][str_replace (array ('{', '}'), '', $text)]))
                                {
                                        $sText = strip_tags ($aTemplate ['data'][str_replace (array ('{', '}'), '', $text)], '<strong><b><i><u><span><a><br>');
                                        $aTemplate ['content'] = str_replace ($text, str_replace ('[lees_meer]', '', $sText), $aTemplate ['content']);
                                        $aText = explode ('[lees_meer]', $sText); 
                                        
                                        if (isset ($aText [1]))
                                                $aText [0] = $aText [0] . '<a href="http://' . $_SERVER ['SERVER_NAME'] . '/mail/' . ($iIdSent != 0 ? 'history/' . $iIdSent : $aTemplate ['id']) . '.html#' . str_replace (array ('{', '}'), '', $text) . '">Lees meer &raquo;</a>';
                                        
                                        $sReturnTemplate = str_replace ($text, $aText [0], $sReturnTemplate);
                                }
                                elseif ($text == '{text_text2}' && isset ($aTemplate ['data'][str_replace (array ('{', '}'), '', $text)]))
                                {
                                        $sList = str_replace ('<li>', '<li style="margin:0px 0px 0px 0px;padding:0px 0px 0px 10px;color: #123a87;font-family: Verdana;font-size: 12px;font-weight: bold;line-height: 21px;">', $aTemplate ['data'][str_replace (array ('{', '}'), '', $text)]);
                                        $sList = str_replace ('<ul>', '<ul style="float:left;padding: 0px 15px 0px 20px;margin:0px 0px 0px 0px;display: block;height: 100%;">
                                                    <li style="list-style: none;"><span style="height: 13px;line-height: 13px;display: block;">&nbsp;</span></li>', $sList);
                                        $sList = str_replace ('</ul>', '<li style="margin:0px 0px 0px 0px;list-style: none;"><span style="height: 13px;line-height: 13px;display: block;">&nbsp;</span></li>
                                                </ul>', $sList);
                                        
                                        $aTemplate ['content'] = str_replace ($text, $sList, $aTemplate ['content']);
                                        $sReturnTemplate = str_replace ($text, $sList, $sReturnTemplate);
                                }
                        }
                        
                if (preg_match_all ('%{title_[^}]+}%s', $aTemplate ['content'], $aMatch))
                {
                        $aTitles = $aMatch [0];
                }
                
                if (isset($aTitles))
                        foreach ($aTitles as $title)
                        {
                                if (isset ($aTemplate ['data'][str_replace (array ('{', '}'), '', $title)]))
                                {
                                        $aTemplate ['content'] = str_replace ($title, strip_tags ($aTemplate ['data'][str_replace (array ('{', '}'), '', $title)], '<strong><b><i><u><span><a><br>'), $aTemplate ['content']);
                                        $sReturnTemplate = str_replace ($title, strip_tags ($aTemplate ['data'][str_replace (array ('{', '}'), '', $title)], '<strong><b><i><u><span><a><br>'), $sReturnTemplate);
                                }
                        }
                
                if (preg_match_all ('%{image_[^}]+}%s', $aTemplate ['content'], $aMatch))
                {
                        $aImages = $aMatch [0];
                }
                
                if (isset ($aImages))
                        foreach ($aImages as $k => $image)
                        {
                                if ($image != '{image_1}' && isset ($aTemplate ['data'][str_replace (array ('{', '}'), '', $image)]))
                                {
                                        $aImage = $this -> getPhotosByOids (current ($aTemplate ['data'][str_replace (array ('{', '}'), '', $image)]));
                                        
                                        $aEImage = glob ('../../' . substr ($aImage [0]['pathphoto'], 1) . '/' . $aImage [0]['id'] . '_newsletter*');
                                        
                                        if (isset ($aEImage [0]) && (empty ($aTest) || (!empty ($aTest) && $aTest ['title'] != str_replace (array ('{', '}'), '', $image))))
                                        {
                                                $aTemplate ['content'] = str_replace ($image, str_replace ('../../', $GLOBALS ['cfg']['sWebsiteUrl'], $aEImage [0]), $aTemplate ['content']);
                                                $sReturnTemplate = str_replace ($image, str_replace ('../../', $GLOBALS ['cfg']['sWebsiteUrl'], $aEImage [0]), $sReturnTemplate);
                                        }
                                        
                                        if (!empty ($aTest) && $aTest ['title'] == str_replace (array ('{', '}'), '', $image))
                                        {
                                                if (count ($aTest ['photos']))
                                                        $aTEImage = glob ('../../files/newsletter/' . current($aTest ['photos']) . '_newsletter*');
                                                
                                                if (isset ($aTEImage [0]) && str_replace (array ('{', '}'), '', $image) == $aTest ['title'])
                                                {
                                                        $aTemplate ['content'] = str_replace ($image, str_replace ('../../', $GLOBALS ['cfg']['sWebsiteUrl'], $aTEImage [0]), $aTemplate ['content']);
                                                        $sReturnTemplate = str_replace ($image, str_replace ('../../', $GLOBALS ['cfg']['sWebsiteUrl'], $aTEImage [0]), $sReturnTemplate);
                                                }
                                        }
                                }
                                elseif ($image == '{image_1}' && isset ($aTemplate ['data'][str_replace (array ('{', '}'), '', $image)]))
                                {   
                                        $sImage = '';
                                        $aImage = $this -> getPhotosByOids (implode (',', $aTemplate ['data'][str_replace (array ('{', '}'), '', $image)]));
                                        
                                        foreach ($aImage as $kp => $itemp)
                                        {
                                                $aEImage = glob ('../../' . substr ($itemp ['pathphoto'], 1) . '/' . $itemp ['id'] . '_newsletter*');
                                                
                                                if (isset ($aEImage [0]) && (empty ($aTest) || (!empty ($aTest) && $aTest ['title'] != str_replace (array ('{', '}'), '', $image))))
                                                {
                                                        $sImage .= '
                                                            <tr>
                                                                <td valign="top" style="width: 150px;height: 110px;">
                                                                    <img src="' . str_replace ('../../', $GLOBALS ['cfg']['sWebsiteUrl'], $aEImage [0]) . '" alt="" style="display: block;width: 150px;height: 110px;" />
                                                                </td>
                                                            </tr>
                                                            ' . ($kp != count ($aImage) - 1 ? '
                                                            <tr>
                                                                <td valign="top" style="width: 150px;line-height: 12px;height: 12px;">&nbsp;</td>
                                                            </tr>
                                                            ' : '') . '
                                                            
                                                        ';
                                                        //$aTemplate ['content'] = str_replace ($image, str_replace ('../../', $GLOBALS ['cfg']['sWebsiteUrl'], $aEImage [0]), $aTemplate ['content']);
                                                }
                                                
                                                if (!empty ($aTest) && $aTest ['title'] == str_replace (array ('{', '}'), '', $image))
                                                {
                                                        if (count ($aTest ['photos']))
                                                                $aTEImage = glob ('../../files/newsletter/' . current($aTest ['photos']) . '_newsletter*');
                                                        
                                                        if (isset ($aTEImage [0]) && str_replace (array ('{', '}'), '', $image) == $aTest ['title'])
                                                        {
                                                                $aTemplate ['content'] = str_replace ($image, str_replace ('../../', $GLOBALS ['cfg']['sWebsiteUrl'], $aTEImage [0]), $aTemplate ['content']);
                                                                $sReturnTemplate = str_replace ($image, str_replace ('../../', $GLOBALS ['cfg']['sWebsiteUrl'], $aTEImage [0]), $sReturnTemplate);
                                                        }
                                                }
                                        }
                                        
                                        $aTemplate ['content'] = str_replace ($image, $sImage, $aTemplate ['content']);
                                        $sReturnTemplate = str_replace ($image, $sImage, $sReturnTemplate);
                                }
                        }
                
                if (preg_match_all ('%{items_[^}]+}%s', $aTemplate ['content'], $aMatch))
                {
                        $aItems = $aMatch [0];
                }
                
                if (isset ($aItems))
                        foreach ($aItems as $items)
                                if (str_replace (array ('{', '}'), '', $items) == 'items_1' && isset ($aTemplate ['data'][str_replace (array ('{', '}'), '', $items) . '_title']))
                                {
                                        $sItems = '';
                                        $sRItems = '';
                                        $iCount = 0;
                                        
                                        foreach ($aTemplate ['data'][str_replace (array ('{', '}'), '', $items) . '_title'] as $ki => $itemi)
                                        {
                                                $aAImage = array ();
                                                
                                                if (isset ($aTemplate ['data'][str_replace (array ('{', '}'), '', $items) . 'x' . $ki]))
                                                {
                                                        $aPhoto = current ($this -> getPhotosByOids (current ($aTemplate ['data'][str_replace (array ('{', '}'), '', $items) . 'x' .$ki])));
                                                        
                                                        
                                                        $aAImage = glob ('../../' . substr ($aPhoto ['pathphoto'], 1) . '/' . $aPhoto ['id'] . '_newsletter*');
                                                        //echo '../../' . substr ($aPhoto ['pathphoto'], 1) . '/' . $aPhoto ['id'] . '_newsletter*';
                                                        
                                                }
                                                        
                                                $sText = strip_tags ($aTemplate ['data'][str_replace (array ('{', '}'), '', $items) . '_text'][$ki], '<strong><b><i><u><span><a><br>');
                                                $aText = explode ('[lees_meer]', $sText);
                                                
                                                if (isset ($aText [1]))
                                                        $aText [0] = $aText [0] . '<a href="http://' . $_SERVER ['SERVER_NAME'] . '/mail/' . ($iIdSent != 0 ? 'history/' . $iIdSent : $aTemplate ['id']) . '.html#' . str_replace (array ('{', '}'), '', $items) . '_text_' . $ki . '">Lees meer &raquo;</a>';
                                                        
                                                $sItems .= '
                                                <tr>
                                                    <td valign="top" style="width: 110px;">
                                                        ' . (isset ($aAImage [0]) ? '<img src="' . str_replace ('../../', $GLOBALS ['cfg']['sWebsiteUrl'], $aAImage [0]) . '" alt="" style="display: block;" />' : '') . '    
                                                    </td>
                                                    <td style="width: 13px;line-height: 10px;">&nbsp;</td>
                                                    <td valign="top" style="width: 446px;">
                                                        <p id="' . str_replace (array ('{', '}'), '', $items) . '_text_' . $ki . '" style="margin:0px 0px 0px 0px;line-height:20px;font-family: Verdana;font-size: 12px;color: #0b0b0b;">
                                                            <span style="color: #23408b;line-height: 28px;">' . $itemi . '</span><br>
                                                            ' . str_replace ('[lees_meer]', '', $sText) . '
                                                        </p>
                                                    </td>
                                                </tr>
                                                ' . ($iCount != count ($aTemplate ['data'][str_replace (array ('{', '}'), '', $items) . '_title']) - 1 ? '
                                                <tr>
                                                    <td colspan="3" valign="middle" style="width:569px;vertical-align: middle;height: 19px;line-height: 19px;">&nbsp;</td>
                                                </tr>
                                                ' : '') . '
                                                ';
                                                
                                                $sRItems .= '
                                                <tr>
                                                    <td valign="top" style="width: 110px;">
                                                        ' . (isset ($aAImage [0]) ? '<img src="' . str_replace ('../../', $GLOBALS ['cfg']['sWebsiteUrl'], $aAImage [0]) . '" alt="" style="display: block;" />' : '') . '    
                                                    </td>
                                                    <td style="width: 13px;line-height: 10px;">&nbsp;</td>
                                                    <td valign="top" style="width: 446px;">
                                                        <p style="margin:0px 0px 0px 0px;line-height:20px;font-family: Verdana;font-size: 12px;color: #0b0b0b;">
                                                            <span style="color: #23408b;line-height: 28px;">' . $itemi . '</span><br>
                                                            ' . $aText [0] . '
                                                        </p>
                                                    </td>
                                                </tr>
                                                ' . ($iCount != count ($aTemplate ['data'][str_replace (array ('{', '}'), '', $items) . '_title']) - 1 ? '
                                                <tr>
                                                    <td colspan="3" valign="middle" style="width:569px;vertical-align: middle;height: 19px;line-height: 19px;">&nbsp;</td>
                                                </tr>
                                                ' : '') . '
                                                ';
                                                
                                                $iCount++;
                                        }
                                        
                                        $aTemplate ['content'] = str_replace ($items, $sItems, $aTemplate ['content']);
                                        $sReturnTemplate = str_replace ($items, $sRItems, $sReturnTemplate);
                                        
                                }
                                else
                                {
                                        $sItems = '';
                                        $sRItems = '';
                                        $iCount = 0;
                                        
                                        foreach ($aTemplate ['data'][str_replace (array ('{', '}'), '', $items) . '_title'] as $ki => $itemi)
                                        {
                                                $sText = strip_tags ($aTemplate ['data'][str_replace (array ('{', '}'), '', $items) . '_text'][$ki], '<strong><b><i><u><span><a><br>');
                                                $aText = explode ('[lees_meer]', $sText);
                                                
                                                if (isset ($aText [1]))
                                                        $aText [0] = $aText [0] . '<a href="http://' . $_SERVER ['SERVER_NAME'] . '/mail/' . ($iIdSent != 0 ? 'history/' . $iIdSent : $aTemplate ['id']) . '.html#' . str_replace (array ('{', '}'), '', $items) . '_text_' . $ki . '">Lees meer &raquo;</a>';
                                                
                                                $sItems .= '
                                                <tr>
                                                    <td valign="top" style="width: 170px;">
                                                        <p id="' . str_replace (array ('{', '}'), '', $items) . '_text_' . $ki . '" style="text-align:right;margin:0px;font-weight:bold;line-height:21px;font-family: Verdana;font-size: 12px;color: #123a87;">
                                                            ' . $itemi . '
                                                        </p>
                                                    </td>
                                                    <td valign="top" style="width: 40px;border-right:1px solid #123a87;line-height: 22px;">&nbsp;</td>
                                                    <td valign="top" style="width: 32px;line-height: 22px;">&nbsp;</td>
                                                    <td valign="top">
                                                        <p style="margin:0px;color: #123a87;line-height: 22px;font-family: Verdana;font-size: 12px;">
                                                            ' . str_replace ('[lees_meer]', '', $sText) . '
                                                        </p>
                                                    </td>
                                                </tr>
                                                ' . ($iCount != count ($aTemplate ['data'][str_replace (array ('{', '}'), '', $items) . '_title']) - 1 ? '
                                                <tr>
                                                    <td valign="top" style="width: 170px;"></td>
                                                    <td valign="top" style="height:21px;width: 40px;border-right:1px solid #123a87;line-height: 21px;">&nbsp;</td>
                                                    <td valign="top" style="height:21px;width: 32px;line-height: 21px;">&nbsp;</td>
                                                    <td valign="top"></td>
                                                </tr>' : '') . '
                                                ';
                                                
                                                $sRItems .= '
                                                <tr>
                                                    <td valign="top" style="width: 170px;">
                                                        <p style="text-align:right;margin:0px;font-weight:bold;line-height:21px;font-family: Verdana;font-size: 12px;color: #123a87;">
                                                            ' . $itemi . '
                                                        </p>
                                                    </td>
                                                    <td valign="top" style="width: 40px;border-right:1px solid #123a87;line-height: 22px;">&nbsp;</td>
                                                    <td valign="top" style="width: 32px;line-height: 22px;">&nbsp;</td>
                                                    <td valign="top">
                                                        <p style="margin:0px;color: #123a87;line-height: 22px;font-family: Verdana;font-size: 12px;">
                                                            ' . $aText [0] . '
                                                        </p>
                                                    </td>
                                                </tr>
                                                ' . ($iCount != count ($aTemplate ['data'][str_replace (array ('{', '}'), '', $items) . '_title']) - 1 ? '
                                                <tr>
                                                    <td valign="top" style="width: 170px;"></td>
                                                    <td valign="top" style="height:21px;width: 40px;border-right:1px solid #123a87;line-height: 21px;">&nbsp;</td>
                                                    <td valign="top" style="height:21px;width: 32px;line-height: 21px;">&nbsp;</td>
                                                    <td valign="top"></td>
                                                </tr>' : '') . '
                                                ';
                                                
                                                $iCount++;
                                        }
                                        
                                        $aTemplate ['content'] = str_replace ($items, $sItems, $aTemplate ['content']);
                                        $sReturnTemplate = str_replace ($items, $sRItems, $sReturnTemplate);
                                }
                
                $aTemplate ['content'] = str_replace ('{web}', $GLOBALS ['cfg']['sWebsiteUrl'] . 'mail/' . $iId . '.html', $aTemplate ['content']);
                $sReturnTemplate = str_replace ('{web}', $GLOBALS ['cfg']['sWebsiteUrl'] . 'mail/' . $iId . '.html', $sReturnTemplate);
                
                
                $sTemplate = $aTemplate ['content'];
                $sTemplate = preg_replace ('{<a class="browser"[^>]*>(.*?)</a>}', '', $sTemplate);
                
                $sTemplate = '
                        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Site</title>
    </head>
    <body style="text-align:center;margin:0;padding:0;height:auto;font-family: Arial;font-size: 12px;">
                ' . $sTemplate . '</body></html>';
        
                $file = fopen ($_SERVER ['DOCUMENT_ROOT'] . '/mail/' . ($iIdSent != 0 ? 'history/' . $iIdSent : $aTemplate ['id']) . '.html', 'w');
                fwrite ($file, $sTemplate);
                fclose ($file);
                
                
                return $sReturnTemplate;
                
        }
        
        /**
        * @desc: take fields of subscribers
        *
        * 
        * @return booleaan   
        */
        
        public function getFields ()
        {
                $pResult = Database :: getInstance () -> query ("
                        SHOW COLUMNS FROM newsletter_subscribers;
                ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                if ($aRow ['Field'] != 'id' && $aRow ['Field'] != 'date')
                                        $aResults [] = $aRow ['Field'];
                        }
                }
                
                return $aResults;
        }
        
        /**
        * @desc: change template
        * 
        * @param int iId 
        * @param array $aValues - data of the group
        * 
        * @returns booleaan
        */
                                       
        public function changeTemplate ($iId, $aValues = array (), $sContent = '', $sPrivate = '') 
        {
                if (!is_numeric ($iId))
                { 
                        return false;
                }
                
                $pStatement = Database :: getInstance () -> prepare ("
                        UPDATE
                            newsletter_templates
                        SET
                            `title`   = ?,
                            `values`  = ?,
                            `private` = ?
                        WHERE
                            `id`      = ?
                ");
                
                $pStatement -> bind_param ('sssi',
                        $aValues ['title'],
                        $sContent,
                        $sPrivate,
                        $iId
                );
                
                if (!$pStatement -> execute ())
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }
                
                //$this -> template ($iId);
                
                return true;
                
        }
        
        /**
        * @desc: get the templates
        * 
        * @param int $iItemId  Optional - id of group
        * @param string $sItemSlug Optional - the slug of group
        * @param string $sSortField - sort results by this field
        * @param string $sSortType - desc/asc
        * 
        * @returns array OR false by error
        */
                                       
        public function getTemplates ($iItemId = 0, $sSortField = 'id', $sSortType = 'desc', $sLanguage = 'default')
        {
                if (!is_numeric ($iItemId)) 
                {
                        return false;
                }
                
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT
                            *    
                        FROM
                            newsletter_templates
                        WHERE
                            1
                            " . (!empty ($iItemId)   ? sprintf ("AND id = %d", $iItemId) : '') . "
                        ORDER BY
                        " . $sSortField . " " . $sSortType . "
                ");
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING); 
                        
                        return false;
                }
                
                $aResults = array ();   
                  
                if ($pResult -> num_rows > 0)
                {
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {
                                $aResults [] = $aRow;
                        }
                }       
                
                return (!empty ($iItemId)) ? current ($aResults) : $aResults;          
        } 
        
        /**
        * @desc Create a random code
        *
        * @param int $iLength number of characters
        * 
        * @return  string
        */

        public function random_password ($iLength = 8)
        {          
                $sChars    = '0123456789abcdefghijklmnopqrstuvwxyz-'; 
                $sPassword = '';

                for ($i=0; $i < $iLength; $i++)
                {
                        $sPassword .= substr ($sChars, mt_rand (0, strlen ($sChars) -1), 1);
                }

                return $sPassword;
        }
}   
?>
