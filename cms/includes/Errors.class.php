<?php

/**
     @description: Errors class
     @link: http://www.inforitus.nl
     @author Andrei Dragos <andrei@inforitus.nl>
**/


class Errors 
{ 
        /**
        *@desc: contructor 
        *
        */
                      
        public function Errors () 
        { 
                $this -> aLanguages   = Language :: getLanguages ();
                $this -> aDefaultLang = Language :: getLanguages (0, '', 'id', 'asc', true);  
        }
         
        /**
        * @desc: get one or all the Error 
        *
        * @param int $iItemId - id of the item
        * @param string $sSortField - sort the result by this field
        * @param string $sSortType - desc/asc
        *  
        * @return array OR booleaan by error
        */
                                           
        public function getErrors ($iItemId = 0, $sSortField = 'date', $sSortType = 'desc')
        {        
                if (!is_numeric ($iItemId))
                {          
                        return false;
                }
                
                $pResult = Database :: getInstance () -> query ("
                        SELECT 
                            *,
                            DATE_FORMAT(date, '%d-%m-%Y %H:%i') AS date_format
                        FROM 
                            errors
                        WHERE 
                            1 
                            " . (!empty ($iItemId)        ? sprintf ("AND id = %d ", $iItemId) : '') . "
                                                        
                        ORDER BY 
                            " . $sSortField . " " . $sSortType . "    
                    ");
                
                $aResults = array (); 
                
                if (!$pResult)
                {
                        trigger_error (Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                }  
                  
                if ($pResult -> num_rows > 0)
                {         
                        while ($aRow = $pResult -> fetch_assoc ()) 
                        {          
                                $aRow ['ip'] = long2ip ($aRow ['ip']);
                                $aResults [] = $aRow;
                        }
                }
                
                $aResults = (!empty ($iItemId)) ? current ($aResults) : $aResults;
                
                return $aResults;
        }
        
        /**
        * @desc: delete a Error item
        *
        * @param int $iId - id of the item
        * 
        * @return booleaan   
        */
                              
        public function deleteError ($iId)
        {
                if (!is_numeric ($iId))
                {
                        return false;
                }
                    
                $pStatement = Database :: getInstance () -> prepare ("
                        DELETE FROM
                            errors
                        WHERE
                            id = ?
                ");
                
                $pStatement -> bind_param ('i',
                        $iId   
                );
                
                if (!$pStatement -> execute ())
                {
                        return false;
                }
                
                return true;
        }
}   
?>
