<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: browser
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################

require_once 'includes/default.inc.php';
		
if (isset ($_SESSION ['image']))
{
	    unset ($_SESSION ['image']);
}

if (isset ($_SESSION ['browser']))
{
	    unset ($_SESSION ['browser']);
}

if (!empty ($_GET ['action']) && $_GET ['action'] == 'del') 
{
    	
}

$aNewImages = array ();
$aImages    = @glob ('../files/' . $_GET ['module'] . '/*');

foreach ($aImages as $image) 
{
    	$sImageName = preg_replace ('#^[^\d]+#', '', $image);
        
    	if (preg_match (sprintf ('#(\d+)_%d#', $GLOBALS['cfg']['images']['thumbWidth']), $sImageName, $matches)) 
        {
        		$aNewImages[$matches[1]] = array (
        			 'url' => preg_replace (sprintf ('#%s(.*)$#',$_SERVER['DOCUMENT_ROOT']), '${1}', $image),
        		);
    	}
}

$oTemplate -> assign ('url', $GLOBALS ['cfg']['sWebsiteUrl']);
$oTemplate -> assign ('images', $aNewImages);
$oTemplate -> assign ('module', $_GET ['module']);    
$oTemplate -> display ('browser.tpl');
?>
