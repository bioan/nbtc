<?php
######### COPYRIGHT #################
 
/*
	ALL RIGHTS RESERVED
	homepage: http://inforitus.nl
	file: setOrder
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once 'includes/default.inc.php';
require_once $GLOBALS ['cfg']['include'] . '/Configuration.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php';   
    
$aIds          = explode (',', $_POST ['ids']);
$aOrderNumbers = explode (',', $_POST ['order_numbers']);

if (strpos ($aOrderNumbers [0], '_') === false)
{
        
        $bSorted = false; 
        
        while ($bSorted == false)
        {
                $bSorted = true;
                
                for ($i = 0; $i < count ($aOrderNumbers); $i++)
                {
                        for ($j = $i + 1; $j < count ($aOrderNumbers); $j++)
                        {
                                if ($aOrderNumbers [$i] < $aOrderNumbers [$j])
                                {
                                        $iTemp              = $aOrderNumbers [$j];
                                        $aOrderNumbers [$j] = $aOrderNumbers [$i];
                                        $aOrderNumbers [$i] = $iTemp;
                                        $bSorted            = false;
                                }
                        }
                }
        }
        
        foreach ($aIds as $k => $id)
        {       
                $pStatement = Database :: getInstance () -> query ("
                        UPDATE
                            " . $_POST ['table'] . "
                        SET
                            order_number = " . $aOrderNumbers [$k] . "
                            
                        WHERE
                            id           = " . $id . "
                       
                ");
        
        }
}
else
{
        $aGroups     = array ();
        $aCopyGroups = array ();
        
        for ($i = 0; $i < count ($aOrderNumbers); $i++)
        {
                $aValues = explode ('_', $aOrderNumbers [$i]);          
                $aGroups [$aValues [0]][$aIds [$i]] = $aValues [1];
                $aCopyGroups [$aValues [0]][] = $aValues [1];
        }
        
        foreach ($aCopyGroups as $key => $value)
        {
                $bSorted = false;
                
                while ($bSorted == false)
                {
                        $bSorted = true;
                        
                        for ($i = 0; $i < count ($aCopyGroups [$key]); $i++)
                        {
                                for ($j = $i + 1; $j < count ($aCopyGroups [$key]); $j++)
                                {
                                        if ($aCopyGroups [$key][$i] < $aCopyGroups [$key][$j])
                                        {
                                                $iTemp                  = $aCopyGroups [$key][$j];
                                                $aCopyGroups [$key][$j] = $aCopyGroups [$key][$i];
                                                $aCopyGroups [$key][$i] = $iTemp;
                                                $bSorted                = false;
                                        }
                                }
                        }
                }   
        }
        
        foreach ($aGroups as $key => $value)
        {
                $iCount = 0;
                
                foreach ($value as $k => $v)
                {
                        $aGroups [$key][$k] = $aCopyGroups [$key][$iCount];   
                        $iCount++;    
                }
        }
        
        foreach ($aIds as $k => $id)
        {
                foreach ($aGroups as $key => $value)
                {
                        if (isset ($aGroups [$key][$id]))
                        {
                                $pStatement = Database :: getInstance () -> query ("
                                        UPDATE
                                            " . $_POST ['table'] . "
                                        SET
                                            order_number = " . $aGroups [$key][$id] . "
                                            
                                        WHERE
                                            id           = " . $id . "
                                       
                                ");
                        }
                }
        
        }   
}
 
?> 