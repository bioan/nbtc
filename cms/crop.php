<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: crop
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once 'includes/default.inc.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/ImageLibrary.class.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Tree.class.php';

$oTree      = new Tree ();
$oImage     = new ImageLibrary ();

if (!empty ($_POST))
{
        $sPath = '../files/' . $_POST ['module'];
        
        $aImage = $oTree -> getImage ($_POST ['id']);
        
        $oImage -> sExtensie  = $aImage ['image_ext'];
        $oImage -> sMimeType  = $aImage ['mime_type'];
        $oImage -> sLocation  = $GLOBALS ['cfg']['include'] . '/' . substr ($sPath, 3);
        $oImage -> sImageName = $aImage ['id'];
        
        if (preg_match ('#(jpeg|png|gif|png)$#i', $oImage -> sMimeType, $matches)) 
        {
                $sFunctionName = sprintf ('imagecreatefrom%s', $matches [1]);
                
                $oImage -> OrginalImage = $sFunctionName ($oImage -> sLocation . '/' . $oImage -> sImageName . $oImage -> sExtensie);    
        }
        
        $source = $oImage -> OrginalImage;
        $tn_w = 1000;
        $tn_h = 1000;
        $file = $oImage -> sLocation . "/" . $oImage -> sImageName . $oImage -> sExtensie;
        $imageFullPath = $GLOBALS ['cfg']['include'] . preg_replace (sprintf ('#%s(.*)$#', $GLOBALS ['cfg']['include']), '${1}', $oImage -> sLocation) . '/' . $oImage -> sImageName . $oImage -> sExtensie;
        
        list($src_w, $src_h, $type, $attr) = getimagesize ($oImage -> sLocation . "/" . $oImage -> sImageName . $oImage -> sExtensie);
        
        preg_match ('#(jpg|png|gif|jpeg|JPG)$#i', $oImage -> sExtensie, $matches);
        
        $x_ratio = $tn_w / $_POST ['w'];
        $y_ratio = $tn_h / $_POST ['h'];

        if (($_POST ['w'] <= $tn_w) && ($_POST ['h'] <= $tn_h)) 
        {
            $new_w = $_POST ['w'];
            $new_h = $_POST ['h'];
        } 
        elseif (($x_ratio * $_POST ['h']) < $tn_h) 
        {
            $new_h = ceil ($x_ratio * $_POST ['h']);
            $new_w = $tn_w;
        } 
        else 
        {
            $new_w = ceil ($y_ratio * $_POST ['w']);
            $new_h = $tn_h;
        }
        //echo $new_h;die('b');
        $newpic = imagecreatetruecolor(round($new_w), round($new_h));
        imagecopyresampled($newpic, $source, 0, 0, $_POST ['x'], $_POST ['y'], $new_w, $new_h, ($_POST ['w']), ($_POST ['h']));
        imagedestroy ($oImage -> OrginalImage);
        
        $sFunctionName = sprintf ('image%s', ($matches [1] == 'jpg' || $matches [1] == 'JPG' ? 'jpeg' : $matches [1]));
        if ($matches [1] == 'png')
            $sFunctionName ($newpic, $file, 9);
        else
            $sFunctionName ($newpic, $file, 100);

        if (preg_match ('#(jpeg|png|gif|png)$#i', $oImage -> sMimeType, $matches)) 
        {
                $sFunctionName = sprintf ('imagecreatefrom%s', $matches [1]);
                
                $oImage -> OrginalImage = $sFunctionName ($oImage -> sLocation . '/' . $oImage -> sImageName . $oImage -> sExtensie);    
        }
        
        list($src_w, $src_h, $type, $attr) = getimagesize ($sPath . '/' . $aImage ['id'] . $aImage ['image_ext']);
        
        $aImageOpts = array ();
        
        foreach ((!isset ($_POST ['template']) ? $aImageSizes [$_POST ['module']] : $aImageSizes [$_POST ['module']][$_POST ['template']]) as $size)
        {
                if (!isset ($_POST ['template']))
                {
                        if (isset ($size ['width']))
                                array_push ($aImageOpts, array (
                                		        'x1'     => 0,
                                		        'x2'     => $src_w,
                                		        'y1'     => 0,
                                		        'y2'     => $src_h,
                                		        'width'  => $size ['width'],
                      		                    'height' => $size ['height'],
                                                'upload' => true
                                                
                           	    ));
                }
                else
                {
                        $aSize = explode ('/', $_POST ['ratio']);
                        
                        if (isset ($size ['width']) && $aSize [0] == $size ['width'] && $aSize [1] == $size ['height'])
                                array_push ($aImageOpts, array (
                                		        'x1'     => 0,
                                		        'x2'     => $src_w,
                                		        'y1'     => 0,
                                		        'y2'     => $src_h,
                                		        'width'  => $size ['width'],
                      		                    'height' => $size ['height'],
                                                'upload' => true,
                                                'name'   => 'newsletter'
                                                
                           	    ));
                        else
                                array_push ($aImageOpts, array (
                                		        'x1'     => 0,
                                		        'x2'     => $src_w,
                                		        'y1'     => 0,
                                		        'y2'     => $src_h,
                                		        'width'  => $size ['width'],
                      		                    'height' => $size ['height'],
                                                'upload' => true
                                                
                           	    ));
                }
        }
        
        $_SESSION ['image'] = array (
                                        //'name'               => preg_replace ('#.*/([^/]+)$#','${1}', $oImage -> sImageName . $oImage -> sExtensie),
                                        'extensie'           => $oImage -> sExtensie,
                                        'width'              => $src_w,
                                        'orig_width'         => $src_w,
                                        'height'             => $src_h,
                                        'orig_height'        => $src_h,
                                        'mime_type'          => $oImage -> sMimeType,
                                        'location'           => preg_replace (sprintf ('#%s(.*)$#', $GLOBALS ['cfg']['include']), '${1}', $oImage -> sLocation),
                                        'oid'                => $_POST ['id'],
                                        //'title'              => $file_name
                                );
        
        if ($aNewImageValues = $oImage -> cropImage ($_SESSION ['image'], $aImageOpts)) 
        {
        	    if (empty ($_GET ['action']) || $_GET ['action'] != 'change') 
                {
        		        foreach ($aNewImageValues as $newImageValue) 
                        {
        			            if (!$oImage -> updateImage (array_merge ($newImageValue, array (
        					            'orig_width'  => $_SESSION ['image']['orig_width'], 
        					            'orig_height' => $_SESSION ['image']['orig_height'], 
        					            'extensie'    => $_SESSION ['image']['extensie'],
        					            'oid'         => $_SESSION ['image']['oid'],
        					            'type'        => 'photoalbum',
                                        'title'       => '')
                                        ))
        				            ) 
                                {
        				                trigger_error (E_CORE_WARNING . ' Er is een fout opgetreden!');
        				                exit;
        			            }
        		        }
        	    } 
                elseif (!empty ($_GET ['action']) && $_GET ['action'] == 'change') 
                {
        		        require_once $_SERVER ['DOCUMENT_ROOT'] . '/cms/includes/Database.class.php';
        		        
                        $pStatement = Database :: getInstance (); 
        		        
                        $pResult = $pStatement  ->  query ("
        			            SELECT
        				            *
        			            FROM
        				            images
        			            WHERE
        				            id = " . $_SESSION ['image']['oid'] . "
        		        ");
        						        
        		        $aResult = $pResult  ->  fetch_assoc ();
                        
        		        if (!$oImage -> updateImage (array_merge ($aResult, current ($aNewImageValues)), $_SESSION ['image']['oid'])) 
                        {
        			            trigger_error (E_CORE_WARNING . ' Er is een fout opgetreden!');
        			            exit;
        		        }
        	    }
             
        	    
        }     
        
        $oTemplate -> assign ('url', $GLOBALS['cfg']['sWebsiteUrl'] . substr ($sPath, 3) . '/' . $oImage -> sImageName . '_' . 100 . 'x' . 100 . $oImage -> sExtensie);      
}

$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'ajaxupload.3.5.js',
                                'greybox.js',
                                'jquery.livequery',
                                'jquery.Jcrop.js',
                                'upload.js'
                                )
                            ); 
$oTemplate -> assign ('cssBody', array ('css/jquery.Jcrop.css'));

$oTemplate -> assign ('module', $_GET ['module']);

if ($_GET ['module'] != 'newsletter')
        $oTemplate -> assign ('sizes', $aImageSizes [$_GET ['module']]);
else
        $oTemplate -> assign ('sizes', $aImageSizes [$_GET ['module']][$_GET ['template']]);
$oTemplate -> display ('crop.tpl');

?>