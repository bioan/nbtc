<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: foto
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
//error_reporting (E_ALL);
//ini_set ('display_errors', 'On');

$GLOBALS ['cfg']['auth'] = false;

require_once 'includes/default.inc.php';

$aPath      = preg_split ('#/#', $_SERVER ['PATH_INFO'], -1, PREG_SPLIT_NO_EMPTY);
$sImageName = '%d_%dx%d%s';

if (!empty ($aPath [0]) && !empty ($aPath [1])) 
{
	    $iId         = $aPath [0];
	    $iImageWidth = $aPath [1];
        
	    $pResult = $GLOBALS ['Db']['Connection']  ->  query ("
		        SELECT
			        *
		        FROM
			        images
		        WHERE
			        id = " . ((int)$iId) . "
	    ");
				    
	    
	    $aResult    = $pResult  ->  fetch_assoc ();
	    $sImagePath = sprintf ('%s%s', $GLOBALS ['cfg']['include'], preg_replace ('#^(.*/) [^/]+$#', '${1}', $aResult ['image_name']));
        
	    require_once $GLOBALS ['cfg']['include'] . '/cms/includes/ImageLibrary.class.php';
	    
        $oImage = new ImageLibrary ();
                    
	    $oImage -> stipulateWidthHeightImage (100, 100, array ('width' => $aResult ['image_width'], 'height' => $aResult ['image_height']));
        
	    if (!empty ($oImage -> iNew_Height) && !empty ($oImage -> iNew_Width)) 
        {                                 
		        $sFilename =& sprintf ($sImagePath, sprintf ($sImageName, $iId, $oImage -> iNew_Width, $oImage -> iNew_Height, $aResult ['image_ext']));
		        // set the mime_type of the images
		        $oImage -> setMimeType ($aResult ['mime_type']);
                
		        if (!file_exists ($sFilename)) 
                {
			        // file not exists so we make a new one and write it to the image folder
			            if ($oImage -> createNewImageFromFile ($aResult ['mime_type'], sprintf ('%s%s', $GLOBALS ['cfg']['include'], $aResult ['image_name']))) 
                        {
				                if ($image = $oImage -> reduceImage (0, 0, 0, $sFilename, false)) 
                                {
					                    $oImage -> show ();
				                }
			            }
		        } 
                else 
                {        
			            // image exists
			            if ($oImage -> createNewImageFromFile ($aResult ['mime_type'], $sFilename)) 
                        {
				                $oImage -> show (100, true);
                        }
		        }
	    }
	    
}
?>
