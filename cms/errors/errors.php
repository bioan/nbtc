<?php
######### COPYRIGHT #################
 
/*
    homepage: http://inforitus.nl
    file: twitter
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Errors.class.php');


$aFolderData = array(
        'url' => 'nieuws.gif',
        'text' => 'Errors',
        'page' => 'errors'
);

$aActions = array(
        2 => 'overview',
        3 => 'delete'
);

$oErrors   = new Errors ();

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        if ($_GET ['action'] == 'overview') 
        {
                require_once 'Pager/Pager.php';  
                
                $aParams = array (
                        'mode'                  => 'Sliding',
                        'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 10), //show 10 items per page 
                        'delta'                 => 3,
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'date'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
                        'delta'                 => 2,
                        'clearIfVoid'           => false,
                        'itemData'              => $oErrors -> getErrors (0, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'date'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                );
                
                $oPager = & Pager::factory ($aParams);
                $aData  =& $oPager -> getPageData ();
                
                $oTemplate -> assign ('links', $oPager -> links);
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
                $oTemplate -> assign ('errors', $aData);
            
        } 
        elseif ($_GET ['action'] == 'delete') 
        {
                if ($oErrors -> deleteError ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/twitter/twitter.php?action=overview');
                }
        }
     
} 
else 
{
        redirect ($SiteRoot . '/errors/errors.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'updateForm.js',
                                'jscalendar/calendar.js',
                                'jscalendar/lang/calendar-en.js',
                                'jscalendar/calendar-setup.js',
                                ($_GET ['action'] == 'overview' ? 'tablednd.js' : ''),
                                ($_GET ['action'] == 'overview' ? 'sort_table.js' : ''),
                                'ajaxupload.3.5.js',
                                'greybox.js',
                                'upload.js'
                                )
                            );
              
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 10));              
$oTemplate -> assign ('cssBody', array ('js/jscalendar/calendar-win2k-1.css', 'css/greybox.css'));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'errors/errors.tpl');

$oTemplate -> display ('default.tpl');
?>
