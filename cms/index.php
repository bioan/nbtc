<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: index
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################

define ('CMS_BASE',     __DIR__);

require_once 'includes/default.inc.php';
		
$sUserName = (isset ($_POST ['user_name']) && !empty ($_POST ['user_name'])) ? $_POST ['user_name'] : '';
$sPassword = (isset ($_POST ['password']) && !empty ($_POST ['password'])) ? $_POST ['password'] : '';

$oLogin = new Authentication ($sUserName, $sPassword);
   
if (!$oLogin -> validateUser ()) 
{                  
        if (!empty ($_POST)) 
        {
                if (!$oLogin -> loginControleFirstTime ()) 
                {             
                        $oTemplate -> assign ('error', $oLogin -> getLastError ());
                } 
                else 
                {
                        header ('location: ' . $GLOBALS ['cfg']['SiteRoot'] . '/authorization.php');
                }
        }
} 
else 
{
        header ('location: ' . $GLOBALS ['cfg']['SiteRoot'] . '/authorization.php');
}

$oTemplate -> assign ('javascriptBody', array ('default.js'));
$oTemplate -> display ('login.tpl');
?>
