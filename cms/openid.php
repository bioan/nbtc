<?php
######### COPYRIGHT #################
 
/*
	ALL RIGHTS RESERVED
	homepage: http://inforitus.nl
	file: users
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once 'includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/OpenIdUser.class.php');

$oUser = new OpenIdUser ();

$aFolderData = array (
        'url' => 'users.png',
        'text' => 'Gebruikers beheer',
        'page' => 'users'
);

if (isset ($_GET ['action']) && !empty ($_GET ['action'])) 
{
	    if ($_GET ['action'] == 'overview') 
        {            
    	        $oTemplate -> assign ('extraOption','<a href="openid.php?action=export"><img src="' . $GLOBALS ['cfg']['relativ'] . '/cms/images/excel.gif" alt="Exporteren" /></a>');
                
                require_once 'Pager/Pager.php';
                     
		        $aParams = array (
		                'mode'                    => 'Sliding',
		                'perPage'                 => 50, //show 50 items per page 
                        'delta'                   => 3, 
                        'extraVars'               => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'),
                        'sorttype'                => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')),
                        'altPrev'                 => 'Vorige pagina',
                        'altNext'                 => 'Volgende pagina',
                        'altPage'                 => 'Pagina',
                        'separator'               => '',
                        'spacesBeforeSeparator'   => 1,
                        'spacesAfterSeparator'    => 1,
		                'delta'                   => 2,
		                'clearIfVoid'             => false,
		                'itemData'                => $oUser -> getUsers (0, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
		        );
                
		        $oPager = & Pager::factory ($aParams);
		        $aData  =& $oPager -> getPageData ();
                
		        $oTemplate -> assign ('links', $oPager -> links);
		        $oTemplate -> assign ('users', $aData);
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));      
		} 
        elseif ($_GET ['action'] == 'export') 
        {
    	        $aDataDump [] = "Gebruikersnaam\tEmailadres\tStatus\tDatum aangemeld\tActief\n\n";
                
    	        if ($oUser -> getUsers ()) 
                {
			            foreach ($oUser -> getUsers () as $user) 
                        {
				                $aDataDump [] = sprintf ("%s\t%s\t%s\t%s\t%d", 
						                trim (stripslashes ($user ['user_name'])),
						                trim ($user ['email']),
						                $user ['status'],
						                date ('d-m-Y', strtotime ($user ['date'])),
						                $user ['active']
				                );
			            }
		        } 
                else 
                {
			            $aDataDump [] = "Er zijn geen gebruikers gevonden\n";
		        }   
                
		        $oUser -> export (implode ("\n",$aDataDump));
	    }
} 
else 
{
        redirect ($SiteRoot . '/users.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
        'jquery.min.js',
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js')
        );
        
$oTemplate -> assign ('folder', $aFolderData);
//$oTemplate -> assign ('status', $_SESSION ['status']);
$oTemplate -> assign ('contentInclude', 'openid.tpl');
$oTemplate -> display ('default.tpl');
?>
