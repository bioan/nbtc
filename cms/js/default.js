function countAndTruncate(fieldObj, maxCount, counterId)
{
    /* empty counter */
    myCounter = document.getElementById(counterId);
    while (myCounter.hasChildNodes()) {
        myCounter.removeChild(myCounter.lastChild);
    }
    /* truncate field */
    if (fieldObj.value.length > maxCount) {
        fieldObj.value = fieldObj.value.substring(0, maxCount);
        fieldObj.focus();
        alert('U heeft teveel tekens ingevoerd.\nHet veld is automatisch ingekort tot het toegestane aantal tekens (' + maxCount + ').');
    }
    if (fieldObj.value.length == maxCount) {
        myCounterText = document.createTextNode('max.');
    } else {
		if (fieldObj.value.length == 1)  {
        	myCounterText = document.createTextNode(fieldObj.value.length + ' teken');
		} else {
			myCounterText = document.createTextNode(fieldObj.value.length + ' tekens');
		}
    }
    /* update counter */
    myCounter.appendChild(myCounterText);
}

var area = 'photos';

function popup(url, height, width, setarea)
{
	newwindow=window.open(url,'name','height= ' + height + ',width= ' + width + 'scrollbars=no,toolbar=no,location=no');
	
    if (window.focus) 
    {
        newwindow.focus()
    }
    
    if(setarea != undefined)
    {
        area = setarea;
    }
    
	return false;
}

function save_link (link_id, mailing_id)
{
    $.ajax({
            type: "GET",
            url: "../newsletter/ajax.php",
            data: 'action=link&link_id=' + link_id + '&mailing_id=' + mailing_id + '&text=' + $ ('#l' + link_id).val (),
            success: function(msg){
            }
        });
}

function fold_menu (e, c)
{
    if ($ ('#t' + c).is (":visible"))
    {
        $ ('#t' + c).css ('display', 'none');
        $ (e).children ('img').attr ('src', '../images/arrow_down.gif');
        $ ('#t' + c).parent ().addClass ('add-border-bottom');
        $.ajax({
            type: "POST",
            url: "../fold_menu.php",
            data: 'id='+ c + '&fold=1',
            success: function(msg){
             
            }
        });  
    }
    else
    {
        $ ('#t' + c).css ('display', 'block');
        $ (e).children ('img').attr ('src', '../images/arrow_up.gif');
        $ ('#t' + c).parent ().removeClass ('add-border-bottom');
        $.ajax({
            type: "POST",
            url: "../fold_menu.php",
            data: 'id='+ c + '&fold=2',
            success: function(msg){
            }
        });
    }
}


function setPhoto(id, imageUrl)
{
  	var obj = document.getElementById(area);
  	if (obj.hasChildNodes()) {
		for (var i=0; i < obj.childNodes.length; i++) {
			if (obj.childNodes.item(i).nodeName.match(/^IMG$/)) {
				if (obj.childNodes.item(i).id == id) {
					return false;
				}
			}
		}  
	}
    
    var areasplit = area.split ('_', 2);
  	var image = document.createElement('IMG');
  	image.src = imageUrl;
  	image.title='Verwijder afbeelding';
  	image.style.cursor = 'pointer';
  	image.id = id;
  	image.onclick = function() {
	  	removePhoto(this);
	}
    
  	var inputField = document.createElement('INPUT');
  	
    if (area.indexOf("_") == -1)
    {
        inputField.name = area + '[]';       
    }
    else
    {
        if (areasplit [0] != 'image' && areasplit [0] != 'items')
            inputField.name = areasplit [0] + '[' + areasplit [1] + ']' + '[' + id + ']';
        else
            inputField.name = areasplit [0] + '_' + areasplit [1] + '[' + id + ']';
    }
    
    inputField.type = 'hidden';
  	inputField.value = id;
  	obj.appendChild(image);
  	obj.appendChild(inputField);
}

function removePhoto(obj)
{
  	if (confirm('Wilt u deze afbeelding verwijderen?')) {  
		for (var i=0; i < obj.parentNode.childNodes.length; i++) {
			if (obj.parentNode.childNodes.item(i).nodeName.match(/^INPUT$/)) {
				if (obj.parentNode.childNodes.item(i).value == obj.id) {
					obj.parentNode.removeChild(obj.parentNode.childNodes.item(i));
				}
			}
		} 
		obj.parentNode.removeChild(obj);
	}
}

function setWidthOfMenus()
{
  
}



$(document).ready(function(){
    $("#slide").animate({
        marginTop: "0px"
      }, 1500 );
      
    $ ('#nav .selected').parent ().prev ().remove ();
    $ ('#nav .selected').parent ().next ().remove ();
    /*  
    $('.submenu').animate({
        marginTop: "0px"
    }, 1500 );
    */

});
/*
function changePage (e)
{
        $('.submenu').animate({
            marginTop: "-36px"
        }, 1500, function (){
             window.location.href = e.rel;
        });
        
        if($('.submenu').length == 0)
        {
            window.location.href = e.rel;  
        }    
}
*/