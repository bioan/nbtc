var jcrop_api;
var i, ac;
var langs = {};

$(function()
{  
        $.ajax({
			url: "../language/language.php",	
			type: "GET",		
			data: 'action=ajax',		
			cache: false,
			success: function (html) {
				langs = jQuery.parseJSON(html);
                
                for (var i = 0; i <= $ (langs).length-1; i++)
                {
                        if ($ ("#upload_" + langs [i]['id']).length > 0)
                        {
                        		var btnUpload = $ ('#upload_' + langs [i]['id']);
                        		var status1   = $ ('#status_' + langs [i]['id']);
                                var languages = langs;
                                
                        		new AjaxUpload (btnUpload, {
                        			action: '../upload-file.php?module=' + $ ('#module').val () + '&lang=' + langs [i]['id'],
                        			name: 'image',
                        			onSubmit: function (file, ext) 
                                    {
                                        if (! (ext && /^(jpg|png|jpeg|gif)$/.test (ext)))
                                        { 
                                                // extension is not allowed 
                                                status1.text('Only JPG, PNG or GIF files are allowed');
                                                return false;
                                        }
                                        
                        				status1.text ('Uploading...');
                        			},
                        			onComplete: function (file, response)
                                    {
                        				//On completion clear the status
                        				status1.text('');
                       					var result = jQuery.parseJSON(response);
                                        $('<input type="hidden" name="photos[' + result ['lang'] + '][' + result ['id'] + ']" value="' + result ['id'] + '"><img src="' + result ['link'] + '" alt="" id="' + result ['id'] + '" style="cursor:pointer" onclick="removePhoto(this);" />').appendTo('#photos_' + result ['lang']);
                        				
                        			}
                        		});
                        }
                        
                        if ($ ("#upload2_" + langs [i]['id']).length > 0)
                        {
                                var btnUpload = $ ('#upload2_' + langs [i]['id']);
                        		var status    = $ ('#status2_' + langs [i]['id']);
                        		new AjaxUpload (btnUpload, {
                        			action: '../upload-file.php?module=' + $ ('#module').val () + '&lang=' + langs [i]['id'],
                        			name: 'image',
                        			onSubmit: function (file, ext) 
                                    {
                                        if (! (ext && /^(jpg|png|jpeg|gif)$/.test (ext)))
                                        { 
                                                // extension is not allowed 
                                                status.text('Only JPG, PNG or GIF files are allowed');
                                                return false;
                                        }
                                        
                        				status.text ('Uploading...');
                        			},
                        			onComplete: function (file, response)
                                    {
                        				//On completion clear the status
                        				status.text('');
                       					var result = jQuery.parseJSON(response);
                                        $('<input type="hidden" name="photos2[' + result ['lang'] + '][' + result ['id'] + ']" value="' + result ['id'] + '"><img src="' + result ['link'] + '" alt="" id="' + result ['id'] + '" style="cursor:pointer" onclick="removePhoto(this);" />').appendTo('#photos2_' + result ['lang']);
                        				
                        			}
                        		});  
                        }
                        
                        if ($("#upload3_" + langs [i]['id']).length > 0)
                        {
                                var btnUpload = $ ('#upload3_' + langs [i]['id']);
                        		var status    = $ ('#status3_' + langs [i]['id']);
                        		new AjaxUpload (btnUpload, {
                        			action: '../upload-file.php?module=' + $ ('#module').val () + '&lang=' + langs [i]['id'],
                        			name: 'image',
                        			onSubmit: function (file, ext) 
                                    {
                                        if (! (ext && /^(jpg|png|jpeg|gif)$/.test (ext)))
                                        { 
                                                // extension is not allowed 
                                                status.text('Only JPG, PNG or GIF files are allowed');
                                                return false;
                                        }
                                        
                        				status.text ('Uploading...');
                        			},
                        			onComplete: function (file, response)
                                    {
                        				//On completion clear the status
                        				status.text('');
                       					var result = jQuery.parseJSON(response);
                                        $('<input type="hidden" name="photos3[' + result ['lang'] + '][' + result ['id'] + ']" value="' + result ['id'] + '"><img src="' + result ['link'] + '" alt="" id="' + result ['id'] + '" style="cursor:pointer" onclick="removePhoto(this);" />').appendTo('#photos3_' + result ['lang']);
                        				
                        			}
                        		});  
                        }
                        
                        if ($("#upload4_" + langs [i]['id']).length > 0)
                        {
                                var btnUpload = $ ('#upload4_' + langs [i]['id']);
                        		var status    = $ ('#status4_' + langs [i]['id']);
                        		new AjaxUpload (btnUpload, {
                        			action: '../upload-file.php?module=' + $ ('#module').val () + '&lang=' + langs [i]['id'],
                        			name: 'image',
                        			onSubmit: function (file, ext) 
                                    {
                                        if (! (ext && /^(jpg|png|jpeg|gif)$/.test (ext)))
                                        { 
                                                // extension is not allowed 
                                                status.text('Only JPG, PNG or GIF files are allowed');
                                                return false;
                                        }
                                        
                        				status.text ('Uploading...');
                        			},
                        			onComplete: function (file, response)
                                    {
                        				//On completion clear the status
                        				status.text('');
                       					var result = jQuery.parseJSON(response);
                                        $('<input type="hidden" name="photos4[' + result ['lang'] + '][' + result ['id'] + ']" value="' + result ['id'] + '"><img src="' + result ['link'] + '" alt="" id="' + result ['id'] + '" style="cursor:pointer" onclick="removePhoto(this);" />').appendTo('#photos4_' + result ['lang']);
                        				
                        			}
                        		});  
                        }
                }
        
                if ($ ("#cropupload").length > 0)
                {
                        var btnUpload = $ ('#cropupload');
                		var status    = $ ('#cropstatus');
                		new AjaxUpload (btnUpload, {
                			action: '../upload-file.php?module=' + $ ('#module').val () + '&crop=1' + ($ ('#template').length > 0 ? '&template=' + $ ('#template').val () : ''),
                			name: 'image',
                			onSubmit: function (file, ext) 
                            {
                                    if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext)))
                                    { 
                                            // extension is not allowed 
                                            status.text ('Only JPG, PNG or GIF files are allowed');
                                            return false;
                                    }
                                    
                				    status.text ('Uploading...');
                			},
                			onComplete: function (file, response) 
                            {
                    				//On completion clear the status
                    				status.text ('');
                   					
                                    var id = response.substring(response.lastIndexOf('/') + 1, response.lastIndexOf('.'));
                                    $ ('#cropbox').html ('<img src="' + response + '" alt="" id="crop" />');
                                    $ ('.index_image').html ('<img src="' + response + '" alt="" id="preview" />');
                				    
                                    setTimeout (setSize, 1350);
        
                                    $ ('#id').val (id);
                                    
                			}
                		});
                }
        
        }
        		
		});
		
});

                

function changeRatio () 
{
        
        var aSizes = $ ('#ratio').val ().split ('/');
        
        x = aSizes [0];
        y = aSizes [1];
        
        $ ('#x').val (0);
        $ ('#y').val (0);
        $ ('#w').val (x);
        $ ('#h').val (y);
        
        $ ('.index_image').css ('width', x + 'px');
        $ ('.index_image').css ('height', y + 'px');
        
        jcrop_api.setOptions({ aspectRatio: (x / y) });
        jcrop_api.setSelect ([ 0, 0, x, y ]);
}

function setSize () 
{
        if ($ ('#crop').height () < $ (window).height () - 75)
                parent.setIframeSize ($ ('#crop').height ());
        else
                parent.setIframeSize ($ (window).height () - 75);
        initJcrop ();
        
        $ ('.submit-but').css ('display', 'block');
        
        $ ('#w_original').val ($ ('#crop').width ());
        $ ('#h_original').val ($ ('#crop').height ()); 
        
        $ ('.preview').css ('display', 'block');
                            
        changeRatio ();
}

function setIframeSize (height) 
{   
        $ ('#GB_frame').css ('height', height + 160 + 'px');
        $ ('#GB_window').css ('height', height + 210 + 'px');
        
        if ((height + 270) > $ ('#GB_overlay').height ())
                $ ('#GB_overlay').css ('height', height + 270 + 'px');
}

function showPreview (coords)
{
    	var rx = $ ('.index_image').width () / coords.w;
    	var ry = $ ('.index_image').height () / coords.h;
        
    	$('#preview').css({
        		width: Math.round(rx * $('#w_original').val()) + 'px',
        		height: Math.round(ry * $('#h_original').val()) + 'px',
        		marginLeft: '-' + Math.round(rx * coords.x) + 'px',
        		marginTop: '-' + Math.round(ry * coords.y) + 'px'
    	});
        
        updateCoords(coords);
    
}

function checkCoords()
{              
        if (parseInt($('#w').val()))
        { 
                return true; 
        }
        
        alert('Please select a crop region then press submit.');
        
        return false;
};

function updateCoords (c)
{
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
};

function update_title (e)
{
        $.ajax({
                 type: "POST",
                 url: "../save-title.php",
                 data: 'id=' + $ (e).attr ('id') + '&title=' + encodeURIComponent($ (e).prev ().val ()),
                 success: function(msg) {
                     
                 }
        }); 
} 

function delete_image (e, path)
{
        $.ajax({
                 type: "POST",
                 url: "../delete-image.php",
                 data: 'id=' + $ (e).attr ('id') + '&path=' + path,
                 success: function(msg){
                     
                 }
        });
        
        $ (e).parent ().parent (). parent (). parent (). parent ().remove (); 
        
        images = '';
        
        $ ('#files div img').each (function (){
		         images = images + $ (this).attr ('id') + ',';    
		});  
        
        $.ajax({
                 type: "POST",
                 url: "../update-item.php",
                 data: 'images=' + images + '&id=' + gup ('oid'),
                 success: function(msg){
                     
                 }
        });
}

function delete_image2 (e, path, type)
{
        $.ajax({
                 type: "POST",
                 url: "../delete-image.php",
                 data: 'id=' + $ (e).attr ('id') + '&path=' + path,
                 success: function(msg){
                     
                 }
        });
        
        $ (e).parent ().parent (). parent (). parent (). parent ().remove (); 
        
        images = '';
        
        
        $ ('#files' + type + ' div img').each (function (){
		         images = images + $ (this).attr ('id') + ',';    
		});  
        
        $.ajax({
                 type: "POST",
                 url: "../update-item2.php",
                 data: 'images=' + images + '&id=' + gup ('oid') + '&type=' + type,
                 success: function(msg){
                     
                 }
        });
}

$(document).ready(function() {
    
       $("a.greybox").click(function(){
              
              var t = "";
              
              area = $ (this).attr ('area');
              
              if ($ (this).hasClass ('browser'))
                    GB_show (t, this.href + '?module=' + $ ('#module').val () + '&area=' + area, 620, 880);
              else
                    GB_show (t, this.href + '?module=' + $ ('#module').val () + '&area=' + area + ($ ('#module').val () == 'newsletter' ? '&template=' + $ ('#group_id').val () : ''), 730, ($ (window).width () - 100));
              
              return false;
       });

});


function gup (name)
{
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( window.location.href );
        
        if( results == null )
                return "";
        else
                return results[1];
} 

function initJcrop ()
{
        jcrop_api = $.Jcrop('#crop');
        
        jcrop_api.setOptions({ onChange: showPreview });
        jcrop_api.setOptions({ onSelect: showPreview });
        jcrop_api.setOptions({ allowResize: !!true });
};


