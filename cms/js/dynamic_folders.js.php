<?php
/**
 *  Copyright inforitus
 *  Contact info@inforitus.nl
 */
   
require_once '../includes/default.inc.php';

	
header('Content-type: text/javascript');

function &getFolders($location)
{
	$folders = array();
	// walk through the current directory and get the folders
	if ($handle = @opendir($location)) {
	   while (false !== ($file = readdir($handle))) {
	       	if (($file != "." && $file != "..") && is_dir($location.'/'.$file)) {
				$folders[] = $file;
	       	}
	   }
	   closedir($handle);
	   sort($folders);
	   return $folders;
	}
	return false;
}

$currentFolder =& $_SESSION['browser']['currentFolder'];
$back = false;
if (isset($_GET['action']) && !empty($_GET['action'])) {	
	if ($_GET['action'] == 'set') {	
		$_SESSION['browser']['previousBrowserPath'] = preg_replace('#^(.*)/[^/]+$#','${1}',$_SERVER['DOCUMENT_ROOT'] . $_GET['path']);
		$_SESSION['browser']['currentFolder'] = $_SERVER['DOCUMENT_ROOT'] . $_GET['path'];
		printf("
			getFolders();
			reload('%s');
			setPath('".$_GET['path']."');
			",
			(!empty($_GET['type'])) ? 'fck' : 'non'			
		);
	} elseif (($_GET['action'] == 'make') && !empty($_GET['name'])) {
		if (!empty($currentFolder)) {
			if (is_dir($_SESSION['browser']['currentFolder'])) {
				if (!ini_get('safe_mode')) {
					if (preg_match('#^[\w\s_-]+$#', $_GET['name'])) {
						if (!file_exists($_SESSION['browser']['currentFolder'].'/'.$_GET['name'])) {
							mkdir($_SESSION['browser']['currentFolder'].'/'.$_GET['name'], 0777);
							chmod($_SESSION['browser']['currentFolder'].'/'.$_GET['name'], 0777);
							printf("
								getFolders();
							");
						} 
					} else {
						printf("
							alert('Er mogen alleen cijfers, woorden, spaties & -_ voorkomen in de naam van de folder.');
						");
					}
				} else {
					printf("
						alert('Safe mode is ingeschakeld en daarom is het niet mogelijk mappen aan te maken!');
					");
				}
			}
		}
	} elseif (($_GET['action'] == 'rename') && !empty($_GET['folder']) && !empty($_GET['newvalue'])) {
		rename($_SERVER['DOCUMENT_ROOT'] . $_GET['folder'], $_SESSION['browser']['currentFolder'] . '/' . $_GET['newvalue']);
		printf("
			getFolders();
		");
	} elseif (($_GET['action'] == 'removeDir') && !empty($_GET['folder'])) {
		$subdata = glob($_GET['folder'].'/*');
		if (count($subdata) > 0 ) {
			printf("alert('Deze map is niet leeg. Om deze map te kunnen verwijderen dient hij eerst leeg te zijn!');");
		} else {
			$folder = preg_replace(sprintf('#^%s(.*)$#i', $GLOBALS ['cfg']['include']),'${1}', $_GET['folder']);
			if (is_dir($GLOBALS ['cfg']['include'] . $folder)) {
				rmdir($GLOBALS ['cfg']['include'] . $folder);
				printf("
					getFolders();
				");
			}
		}
	} elseif ($_GET['action'] == 'get') {
		$folders =& getFolders($_SESSION['browser']['currentFolder']);
		
		printf("
			var obj = document.getElementById('folderPlace');
		");
		
		if ($_SESSION['browser']['currentFolder'] != $_SESSION['browser']['defaultBrowserPath']) {
			$back = true;
			$previousPath =& preg_replace(sprintf('#%s(.*)$#',$_SERVER['DOCUMENT_ROOT']), '${1}',$_SESSION['browser']['previousBrowserPath']);
			printf("
				var ul = document.createElement('UL');
				ul.style.listStyle = 'none';
				ul.style.listStyleImage = 'url(\'%simages/folderUp.gif\')';
				ul.style.padding = 0;
				ul.style.paddingLeft = 1.2 + 'em';
				ul.style.paddingTop = 0.4 + 'em';
				ul.style.margin = 0;
				ul.style.marginLeft = 1.2 + 'em';
				ul.style.marginTop = 0.4 + 'em';
				var li = document.createElement('LI');
				li.appendChild(document.createTextNode('..'));
				li.style.cursor = 'pointer';
				li.title = 'Vorige map << ".$previousPath."';
				li.onclick = function() {
					getFolder('".$previousPath."');
				}
				ul.appendChild(li);
				obj.appendChild(ul);
			", $GLOBALS ['cfg']['SiteRoot']);
		}
		printf("
			var ul = document.createElement('UL');
			ul.style.padding = 0;
			ul.style.margin = 0;
		");
		if (!$back) {
			printf("
				ul.style.paddingTop = 0.5 + 'em';
				ul.style.marginTop = 0.5 + 'em';
			");
		}
		printf("
			ul.style.paddingLeft = 1.2 + 'em';
			ul.style.marginLeft = 1.2 + 'em';
			ul.style.listStyle = 'none';
			ul.style.listStyleImage = 'url(\'%s/images/folder.gif\')';
		", $GLOBALS ['cfg']['SiteRoot']);
		if (!empty($folders)) {
			foreach ($folders as $folder)
			{	
				// for the show name of the folder we won't a to large name
				$folderName = (strlen($folder) > 13) ?  substr($folder,0,13).'..' : $folder;
				$file = preg_replace(sprintf('#%s(.*)$#',$_SERVER['DOCUMENT_ROOT']), '${1}',$_SESSION['browser']['currentFolder']).'/'.$folder;
				printf("
					var li = document.createElement('LI');
					var textNodeParamCont = document.createElement('SPAN');
					var textNodeParam = document.createTextNode('%s');
					textNodeParamCont.appendChild(textNodeParam);
					textNodeParamCont.onclick = function() {
						getFolder('%s');
					}
					textNodeParamCont.onmouseover = function() {
						this.style.color = 'red';
						this.style.textDecoration = 'underline';
					}
					textNodeParamCont.onmouseout = function() {
						this.style.color = 'black';
						this.style.textDecoration = 'none';
					}
					/* walk through the li nodes and remove al the input elements because we want 1 input element by time 
						After the remove we set the input field for the specified LI item so we can change the input value
					*/
					li.ondblclick = function()  {
						for (var i=0; i < this.parentNode.childNodes.length; i++) {
							childNode = this.parentNode.childNodes.item(i);
							if (childNode.hasChildNodes()) {
								childNode.firstChild.style.fontWeight= 'normal';
								childNode.style.listStyleImage = 'url(\'%s/images/folder.gif\')';
								if (childNode.childNodes.length > 1) {
									childNode.removeChild(childNode.lastChild);
								}
							}
						}
						this.firstChild.style.fontWeight = 'bold';
						this.style.listStyleImage = 'url(\'%simages/folderSelected.gif\')';
						var div = document.createElement('DIV');
						var delImg = document.createElement('IMG');
						delImg.src = '%s/images/delete.png';
						delImg.style.paddingLeft = '1em';
						delImg.alt = 'Map verwijderen';
						delImg.onclick = function() {
							if (confirm('Weet u zeker dat u deze map wilt verwijderen?')) {
								removeDir('%s/%s');
							}
						}
						div.appendChild(delImg);
						this.appendChild(div);
					}
					li.title = 'Mapnaam wijzigen';
					li.appendChild(textNodeParamCont);
					li.style.cursor = 'pointer';
					ul.appendChild(li);
					",
					$folderName, $file, $GLOBALS ['cfg']['SiteRoot'], $GLOBALS ['cfg']['SiteRoot'], $GLOBALS ['cfg']['SiteRoot'], $_SESSION['browser']['currentFolder'], preg_replace('#^.*/([^/]+)$#', '${1}', $file)
				);
                
                printf("
                    var a = '%s';
                    var b = '%s';
                ", $_SESSION['browser']['currentFolder'], preg_replace('#^.*/([^/]+)$#', '${1}', $file));
			}
		} else {
			printf("	
				var ul = document.createElement('UL');
				ul.style.listStyle = 'none';
				ul.style.padding = 0;
				ul.style.paddingLeft = 0.6 + 'em';
				ul.style.margin = 0;
				ul.style.marginLeft = 0.6 + 'em';
				var li = document.createElement('LI');
				li.appendChild(document.createTextNode('geen'));
				ul.appendChild(li);
				
			");
		}
		printf("
			obj.appendChild(ul);
		");
		
	}
}
?>
