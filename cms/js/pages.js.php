<?php

header('Content-type: text/javascript');

printf ('$(document).ready(function() {');

require_once '../includes/default.inc.php';
require_once '../includes/Database.class.php';
require_once '../includes/Language.class.php';
require_once '../includes/Tree.class.php';
require_once '../includes/Page.class.php';

$pPage = new Tree ($_GET ['type_oid']);

$aParents = $pPage -> getAllNodes (1);

$aIds = array ();

array_push ($aIds, 'tmenu');

for ($i = 0; $i < count ($aParents); $i++)
{       
        $aParents [$i]['childs'] = $pPage -> getChilds ($aParents [$i]['oid'], '', 2); 
        
        if (count ($aParents [$i]['childs']) > 0)
        {
                array_push ($aIds, 't' . $aParents [$i]['oid']);
        }
        
        for ($j = 0; $j < count ($aParents [$i]['childs']); $j++)
        {
                  
                $aParents [$i]['childs'][$j]['childs'] = $pPage -> getChilds ($aParents [$i]['childs'][$j]['oid'], '', 3);   
                
                if (count ($aParents [$i]['childs'][$j]['childs']) > 0)
                {
                        array_push ($aIds, 't' . $aParents [$i]['childs'][$j]['oid']);
                }
                
                for ($k = 0; $k < count ($aParents [$i]['childs'][$j]['childs']); $k++)
                {
                          
                        $aParents [$i]['childs'][$j]['childs'][$k]['childs'] = $pPage -> getChilds ($aParents [$i]['childs'][$j]['childs'][$k]['oid'], '', 4);   
                        
                        if (count ($aParents [$i]['childs'][$j]['childs'][$k]['childs']) > 0)
                        {
                                array_push ($aIds, 't' . $aParents [$i]['childs'][$j]['childs'][$k]['oid']);
                        }
                        
                }
                
        }   
}

foreach ($aIds as $id) 
{
        printf ("
$(\"#".$id."\").sortable({
        handle : '.handle-".$id."',
        update : function () {
        var order = $(\"#".$id."\").sortable('serialize');
                $(\"#info\").load(\"../process-sortable.php?\" + order + \"&type_oid=" . $_GET ['type_oid'] . "\");
        }
});");

}

printf ('});');

?>