<?php
/**
 * Page to get and delete dynamic images
 *
 * @author info@inforitus.nl
 *
 */    
 
require_once '../includes/default.inc.php';

	
header('Content-type: text/javascript');


if (!empty($_GET['action']) && $_GET['action'] == 'del') {
	foreach (glob($_SESSION['browser']['currentFolder'] .'/'.$_GET['oid'].'*') as $image) {
		unlink($image);
	}
	Database :: getInstance () -> query (sprintf("
		DELETE 
		FROM 
			images 
		WHERE 
			id = %d", 
		$_GET['oid']
	));
	printf("
		alert('De afbeelding is succesvol verwijderd!');
	");
} else {
	$newImages = array();
	$images = @glob($_SESSION['browser']['currentFolder'].'/*');
	
	foreach ($images as $image) {
		$imageName = preg_replace('#^[^\d]+#', '',$image);
		if (preg_match(sprintf('#(\d+)_%d#', $GLOBALS['cfg']['images']['thumbWidth']), $imageName, $matches)) {
			$newImages[$matches[1]] = array (
				'url' => preg_replace(sprintf('#%s(.*)$#',$_SERVER['DOCUMENT_ROOT']), '${1}',$image),
			);
		}
	}
	printf("
		var obj = document.getElementById('photoPlace');
		var table = document.createElement('TABLE');
		var tbody = document.createElement('TBODY');
		var tr1 = document.createElement('TR');
	");
	$i = 1;
	$j=1;
	foreach ($newImages as $key => $image) {
		$_SESSION['browser']['imagesOids'][$key] = $image;
		printf("
			var td = document.createElement('TD');
			td.id = %d;
				var tableInside = document.createElement('TABLE');
				var tbodyInside = document.createElement('TBODY');
				var tr1Inside = document.createElement('TR');
				var td1Inside = document.createElement('TD');
				td1Inside.colSpan = 2;
					var img = document.createElement('IMG');
					img.name = 'image_%d';
					img.src = '%s';
					if (location.href.match(/fck$/)) {
						img.onclick = function() {
							var parent = window.opener;
							parent.document.getElementById('txtUrl').value = '/cms/foto/%d/175/image.jpg';
							window.close();
						}
					} else {
						img.onclick = function() {
							window.parent.opener.setPhoto(%d, '%s%s');
							if (confirm('Venster sluiten?'))
								window.close();
						}
					}
				",
				$key, $key, $image['url'], $key, $key, $GLOBALS['cfg']['sWebsiteUrl'], $image['url']
			);
			printf("
				td1Inside.appendChild(img);
				tr1Inside.appendChild(td1Inside);
				var tr2Inside = document.createElement('TR');
				var td2Inside = document.createElement('TD');
				td2Inside.style.width = 20 + 'px';
				var imgDel = document.createElement('IMG');
				imgDel.src = '%simages/delete.png';
				imgDel.id = %d;
				imgDel.title = 'Afbeelding verwijderen';
				imgDel.alt = 'Afbeelding verwijderen';
				imgDel.style.cursor = 'pointer';
				imgDel.onclick = function() {
					if (confirm('Wilt u deze foto verwijderen')) {
						var deleteImage = document.getElementById('image_%d');
						var parent = this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
						for (var q=0; q < parent.childNodes.length; q++) {
							if (parent.childNodes.item(q).id == this.id) {
								parent.removeChild(parent.childNodes.item(q));
								var script = document.createElement('SCRIPT');
								script.type = 'text/javascript';
								script.src = '%sjs/dynamic_images_browser.js.php?action=del&oid='+this.id;
								parent.appendChild(script);
							}	
						}
					}
				}
				td2Inside.appendChild(imgDel);
				
				var td3Inside = document.createElement('TD');
				var imgChange = document.createElement('IMG');
				imgChange.src = '%simages/change.png';
				imgChange.id = %d;
				imgChange.title = 'Afbeelding wijzigen';
				imgChange.alt = 'Afbeelding wijzigen';
				imgChange.onclick = function() {
					window.open('%sbrowser/upload_image.php?action=change&oid=%d','edit_image','height=600,width=600,resizable=yes, scrollbars=yes');
				}
				imgChange.style.cursor = 'pointer';
				td3Inside.appendChild(imgChange);
				
				tr2Inside.appendChild(td2Inside);
				tr2Inside.appendChild(td3Inside);
				tbodyInside.appendChild(tr1Inside);
				tbodyInside.appendChild(tr2Inside);
				tableInside.appendChild(tbodyInside);
			td.appendChild(tableInside);
		",
		$GLOBALS ['cfg']['SiteRoot'], $key, $i, $GLOBALS ['cfg']['SiteRoot'], $GLOBALS ['cfg']['SiteRoot'], $key, $GLOBALS ['cfg']['SiteRoot'], $key
		);
		if ($i % 5 ==0) {
			++$j;
			printf("var tr%d = document.createElement('TR');", $j);
		}
		printf("tr%d.appendChild(td);", $j);
		$i++;
	}
	for ($c =1; $c <= $j; $c++) {
		printf('tbody.appendChild(tr%d);', $c);
	}
	printf('
		table.appendChild(tbody);
		obj.appendChild(table);
	');
}
?>
