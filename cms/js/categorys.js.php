<?php

header('Content-type: text/javascript');

printf ('$(document).ready(function() {');

require_once '../includes/default.inc.php';
require_once '../includes/Page.class.php';

$pPage = new Page ();

$aParents = $pPage -> getAllNodes (1);

$aIds = array ();

array_push ($aIds, 'menu');

for ($i = 0; $i < count ($aParents); $i++)
{       
        $aParents [$i]['childs'] = $pPage -> getChilds ($aParents [$i]['oid'], '', 2); 
        
        if (count ($aParents [$i]['childs']) > 0)
        {
                array_push ($aIds, $aParents [$i]['slug']);
        }
        
        for ($j = 0; $j < count ($aParents [$i]['childs']); $j++)
        {
                  
                $aParents [$i]['childs'][$j]['childs'] = $pPage -> getChilds ($aParents [$i]['childs'][$j]['oid'], '', 3);   
                
                if (count ($aParents [$i]['childs'][$j]['childs']) > 0)
                {
                        array_push ($aIds, $aParents [$i]['childs'][$j]['slug']);
                }
                
        }   
}

foreach ($aIds as $id) 
{
        printf ("
$(\"#".$id."\").sortable({
        handle : '.handle-".$id."',
        update : function () {
        var order = $(\"#".$id."\").sortable('serialize');
                //$(\"#info\").load(\"../process-sortable.php?\"+order);
        }
});");

}

printf ('});');

?>