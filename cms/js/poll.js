/**
 * Js functions to make the poll
 * 
 * Use the JSON (JavaScript Object Notation) 
 *  
 * @ made by Leendert Wielink
 * @ date 19-08-2006
 * @ file /cms/js/poll.js  
 */  

LwPoll = {

    makeFields  : function(thisForm, parents)
    {
        aantal = parseInt(document.getElementById('aantal').value-parents);
    
        var obj = document.getElementById('insertField');
        var error = document.getElementById('error');
        // delete the existing options
        while(obj.hasChildNodes()) {    
            obj.removeChild(obj.lastChild);
        }
        while(error.hasChildNodes()) {
            error.removeChild(error.lastChild);
        }   
        if (aantal+parents <= 1 || aantal+parents > 25) {         
            error.appendChild(document.createTextNode('Error! minstens 2 velden en max. 25!!'));
        } else if (aantal+parents < parents) {
            error.appendChild(document.createTextNode('Error! Indien u een optie weg wilt dan moet u deze leeg laten!!'));
        } else {
            // walk through the aantal and create the input fields
            for (var i=parents; i < aantal+parents; i++) {
                var table = document.createElement('TABLE');
                var tbody = document.createElement('TBODY');
                table.cellSpacing = 0;
                table.cellPadding = 0;
                table.id = 'choise' + i;
                var tr = document.createElement('TR');
                tr.style.height = 25 + 'px';
                var fTd = document.createElement('TD');
                fTd.style.width = 125 + 'px';
                var sTd = document.createElement('TD');
                var tTd = document.createElement('TD');
                tTd.appendChild(document.createTextNode('*'));
                tTd.style.display = 'none';
                tTd.style.color = 'red';
                tTd.id = 'error' + i;
                tbody.appendChild(tr);
                tr.appendChild(fTd);
                fTd.appendChild(document.createTextNode('keuze' + (i+1)));
                tr.appendChild(sTd);
                tr.appendChild(tTd);
                input = document.createElement('INPUT');
                input.name = 'choises[]';
                input.size = 40;
                sTd.appendChild(input);
                table.appendChild(tbody);
                obj.appendChild(table);
            }
            
            if (parents == 0) {
                submitField = document.getElementById('submitFieldje');
                
                while(submitField.hasChildNodes()) {
                    submitField.removeChild(submitField.lastChild);
                }
                button = document.createElement('INPUT');
                button.value = 'Toevoegen';
                button.type = 'submit';
                submitField.appendChild(button);
            }
        }
    },
        
        
    checkForm   : function(thisForm)
    {
        var obj = document.getElementById('insertField');
        var inputs = obj.getElementsByTagName('INPUT');
        var error = 0;
        var title = document.getElementById('sTitle');
        for (var i=0; i < inputs.length; i++) {
            if (inputs.item(i).name.match(/^choises/)) {
                var fieldId = inputs.item(i).parentNode.parentNode.parentNode.parentNode.id;
                var errorObj = fieldId.replace('choise','error');
                errorObj = document.getElementById(errorObj);
                var field = document.getElementById(fieldId); 
                if (inputs.item(i).value == '') {
                    error += 1;
                    errorObj.style.display = 'inline';
                } else {
                    error -= 1;
                    errorObj.style.display = 'none';
                }
            }
        }
        titleError = document.getElementById('sTitleError');
        if (title.value == '') {
            titleError.style.display = 'inline';
            error += 1;
        } else {
            titleError.style.display = 'none';
            error -= 1;
        }
        if (error > 0) {
            var errorMelding = document.getElementById('errorMeldingTop');
            errorMelding.style.display = 'inline';
            return false;
        }
        return true;
    },
 
 
    countAantal : function(value)
    {
        var obj = document.getElementById('aantal');
        var ex = document.getElementById('existsFields');
        aantal = ex.getElementsByTagName('INPUT').length;
        if (value == "") {
            obj.value = parseInt(obj.value)-1;
        } else {
            if (aantal > obj.value)
                obj.value = parseInt(obj.value)+1;
        }
    }
    
    
}
