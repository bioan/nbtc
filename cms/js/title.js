Cufon.replace(['.sites'], {
    hover: true
});

Cufon.replace(['.font'], {
    hover: true
});

Cufon.replace(['.title'], {
    hover: true
});

Cufon.replace(['.pagina-titel'], {
    hover: true
});

var marginleft = 0;

$(document).ready(function(){
    $('#right-position').css('width', $(window).width() - $('#nav').width() - $('#home').width() + 'px');
    marginleft = $(window).width() - $('#nav').width() - $('#home').width();
    $(window).resize(function() {
    
    if($(window).width() - $('#nav').width() - $('#home').width() >= 0 && $(window).width() - $('#nav').width() - $('#home').width() <= marginleft)
        $('#right-position').css('width', $(window).width() - $('#nav').width() - $('#home').width() + 'px');        
    
    });  

});