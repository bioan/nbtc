function delPhoto (id, module) 
{
    if (confirm ('Wilt u deze foto verwijderen')) {
            $.ajax({
                    type: "POST",
                    url: "../removeImage.php",
                    data: 'module=' + module + '&id=' + id,
                    success: function()
                    {
                            $ ('#image_' + id).remove ();       
                    }
            });
    }
}
