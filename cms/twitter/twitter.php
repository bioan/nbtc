<?php
######### COPYRIGHT #################
 
/*
    homepage: http://inforitus.nl
    file: twitter
    @author Laurentiu Ghiur <laurentiu@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Twitter.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/twitter/twitteroauth.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/twitter/secret.php');

$aFolderData ['text'] .= ' Twitter';

$aActions = array(
        1 => 'overview',
        2 => 'new',
        3 => 'delete',
        4 => 'newWord',
        5 => 'overviewWords',
        6 => 'overviewSearch',
        7 => 'editWord',
        8 => 'deleteWord',
        9 => 'deleteSearch'
);

$oTwitter   = new Twitter ();
                
$appData = $oTwitter ->getApplication();

if (!empty ($appData))
{
        $oTemplate -> assign ('appValues', $appData);

        //Application ID
        $consumer_key = $appData ['consumer_key'];

        //Application secret code
        $consumer_secret = $appData ['consumer_secret'];
        
        //twitter user data
        $twitterUser = $oTwitter ->getTwitterUser($_SESSION ['user_id']);
        
        $ok = false;
        if (!empty ($twitterUser) && $appData ['correct'] == 1) 
        {
                $twitterConn = new TwitterOAuth($consumer_key, $consumer_secret, $twitterUser['oauth_token'], $twitterUser['oauth_token_secret']);
                
                $method = 'account/verify_credentials';
                $status = $twitterConn->get($method);
                (isset ($status -> error)) ? $ok = false : $ok = true;
        }
}
else 
{
        $oTemplate -> assign ('url', 'false');
}

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        if ($_GET ['action'] == 'new')
        {                
            
                $aFolderData ['text'] .= ' - Connect to application';
            
                if (isset($_POST) && isset ($_POST ['saveChangesApp']))
                {
                        if ($oTwitter ->checkApplication($_POST))
                        {
                                
                                $connection = new TwitterOAuth($_POST ['consumer_key'], $_POST ['consumer_secret']);

                                /* Get temporary credentials. */
                                $request_token = $connection->getRequestToken(OAUTH_CALLBACK);
                                
                                (!isset ($request_token ['Failed to validate oauth signature and token'])) ? $oTwitter -> updateApplicationStatus(1) : $oTwitter -> updateApplicationStatus(0);
                                
                                $oTemplate -> assign ('successMessage', 'Application\'s data were saved');                
                                unset($_POST);
                        }
                }
                        
                if ($ok)
                {
                        $oTemplate -> assign ('errorCode', 'You have permissions for this twitter app');
                }
                else
                {
                        /* Build TwitterOAuth object with client credentials. */
                        if (!isset ($_GET ['oauth_token']))
                        {
                                $connection = new TwitterOAuth($consumer_key, $consumer_secret);

                                /* Get temporary credentials. */
                                $request_token = $connection->getRequestToken(OAUTH_CALLBACK);
                                if (!isset ($request_token ['Failed to validate oauth signature and token']))
                                {
                                        $_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
                                        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
                                        $token = $request_token['oauth_token'];
                                        $url = $connection->getAuthorizeURL($token);
                                        $oTemplate -> assign ('url', $url);
                                        $oTwitter -> updateApplicationStatus(1);
                                }
                                else 
                                {   
                                    $oTwitter -> updateApplicationStatus(0);
                                    $oTemplate -> assign ('errorCode', 'Application id or secret code is not valid');
                                }
                        }
                        else 
                        {
                                /* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
                                $twitterConn = new TwitterOAuth($consumer_key, $consumer_secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

                                /* Request access tokens from twitter */
                                $access_token = $twitterConn->getAccessToken();
                                /* Save the access tokens. Normally these would be saved in a database for future use. */
                                $_SESSION['access_token'] = $access_token;

                                $aValues = array ();
                                $aValues ['user_id'] = $_SESSION ['user_id'];
                                $aValues ['twitter_id'] = $_SESSION ['access_token']['user_id'];
                                $aValues ['twitter_name'] = $_SESSION ['access_token']['screen_name'];
                                $aValues ['oauth_token'] = $_SESSION ['access_token']['oauth_token'];
                                $aValues ['oauth_token_secret'] = $_SESSION ['access_token']['oauth_token_secret'];
                                $oTwitter ->checkTwitterUser($aValues);

                                /* Remove no longer needed request tokens */
                                unset($_SESSION['oauth_token']);
                                unset($_SESSION['oauth_token_secret']);
                                unset($_SESSION['access_token']);

                                /*date_default_timezone_set('GMT');
                                $parameters = array('status' => date(DATE_RFC822));
                                $status = $twitterConn->post('statuses/update', $parameters);*/

                                $oTemplate -> assign ('errorCode', 'You have permissions for this twitter app');
                        }
                }
                
        }
        elseif ($_GET ['action'] == 'overview') 
        {
                require_once 'Pager/Pager.php';  
                
                $aFolderData ['text'] .= ' - List of tweets';
                
                $aParams = array (
                        'mode'                  => 'Sliding',
                        'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 10), //show 10 items per page 
                        'delta'                 => 3,
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'date'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
                        'delta'                 => 2,
                        'clearIfVoid'           => false,
                        'itemData'              => $oTwitter -> getTwitter (0, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'date'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                );
                
                /*if ($ok)
                {
                        $connection = new TwitterOAuth($appData ['consumer_key'], $appData ['consumer_secret'], $twitterUser['oauth_token'], $twitterUser['oauth_token_secret']);
                        $method = 'statuses/user_timeline.json?screen_name=frankekooistra&count=100';
                        $status = $connection->get($method);

                        if (isset ($status -> error))  
                        {
                                $okTwitter = false;
                                $oTemplate -> assign ('publishTwitter', 'Twitter user doesn\'t exists');
                        }
                        else 
                        {
                                $okTwitter = true;
                                $oTemplate -> assign ('publishTwitter', 'true');
                                dump_r($status); die;
                        }
                }
                    */
                $oPager = & Pager::factory ($aParams);
                $aData  =& $oPager -> getPageData ();
                
                $oTemplate -> assign ('links', $oPager -> links);
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
                $oTemplate -> assign ('twitter', $aData);


            
        } 
        elseif ($_GET ['action'] == 'overviewSearch') 
        {
                
                $aFolderData ['text'] .= ' - List of search results';
            
                require_once 'Pager/Pager.php';  
                
                $aParams = array (
                        'mode'                  => 'Sliding',
                        'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 10), //show 10 items per page 
                        'delta'                 => 3,
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'date'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
                        'delta'                 => 2,
                        'clearIfVoid'           => false,
                        'itemData'              => $oTwitter -> getTwitterSearch (0, (isset ($_GET ['sort']) ? $_GET ['sort'] : 'date'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                );
                
                $oPager = & Pager::factory ($aParams);
                $aData  =& $oPager -> getPageData ();
                $oTemplate -> assign ('links', $oPager -> links);
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
                $oTemplate -> assign ('twitter', $aData);
            
        } 
        elseif ($_GET ['action'] == 'delete') 
        {
                if ($oTwitter -> deleteTwitter ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/social_media/social_media.php?media=twitter&action=overview');
                }
        } 
        elseif ($_GET ['action'] == 'deleteSearch') 
        {
                if ($oTwitter -> deleteTwitterSearch ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/social_media/social_media.php?media=twitter&action=overviewSearch');
                }
        }
        elseif ($_GET ['action'] == 'deleteWord') 
        {
                if ($oTwitter -> deleteTwitterWord ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/social_media/social_media.php?media=twitter&action=overviewWords');
                }
        }
        elseif ($_GET ['action'] == 'newWord') 
        {
                $aFolderData ['text'] .= ' - Add new word to search';
                
                if (isset ($_POST) && isset ($_POST ['addWord']))
                {
                      $oTwitter ->addTwitterWord($_POST); 
                      unset ($_POST);
                      redirect ($SiteRoot . '/social_media/social_media.php?media=twitter&action=overviewWords');
                }
        }
        elseif ($_GET ['action'] == 'editWord')
        {
                $aFolderData ['text'] .= ' - Edit word to search';
                
                $twitterWord = $oTwitter -> getTwitterWord($_GET ['id']);
                $oTemplate -> assign ('values', $twitterWord);
                
                if (isset ($_POST) && isset ($_POST ['editWord']))
                {
                      $oTwitter -> editTwitterWord($_GET ['id'], $_POST); 
                      unset ($_POST);
                      redirect ($SiteRoot . '/social_media/social_media.php?media=twitter&action=overviewWords');
                }
        }
        elseif ($_GET ['action'] == 'overviewWords') 
        {
                $aFolderData ['text'] .= ' - List of words to be searched';
                
                require_once 'Pager/Pager.php';  
                
                $aParams = array (
                        'mode'                  => 'Sliding',
                        'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 10), //show 10 items per page 
                        'delta'                 => 3,
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'date'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
                        'delta'                 => 2,
                        'clearIfVoid'           => false,
                        'itemData'              => $oTwitter -> getTwitterWords((isset ($_GET ['sort']) ? $_GET ['sort'] : 'date'))
                );
                
                $oPager = & Pager::factory ($aParams);
                $aData  =& $oPager -> getPageData ();
                $oTemplate -> assign ('links', $oPager -> links);  
                $oTemplate -> assign ('twitter', $aData);
        }
     
} 
else 
{
        redirect ($SiteRoot . '/social_media/social_media.php?media=twitter&action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'updateForm.js',
                                'jscalendar/calendar.js',
                                'jscalendar/lang/calendar-en.js',
                                'jscalendar/calendar-setup.js',
                                ($_GET ['action'] == 'overview' ? 'tablednd.js' : ''),
                                ($_GET ['action'] == 'overview' ? 'sort_table.js' : ''),
                                'ajaxupload.3.5.js',
                                'greybox.js',
                                'upload.js'
                                )
                            );

$oTemplate -> assign ('appErrors', $oTwitter -> aError);              
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 10));              
$oTemplate -> assign ('cssBody', array ('js/jscalendar/calendar-win2k-1.css', 'css/greybox.css'));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'twitter/twitter.tpl');

$oTemplate -> display ('default.tpl');
?>
