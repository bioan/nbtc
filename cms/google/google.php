<?php
######### COPYRIGHT #################
 
/*
    homepage: http://inforitus.nl
    file: news
    @author Laurentiu Ghiur <laurentiu@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Google.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/google/apiClient.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/google/apiOauth2Service.php');

$aFolderData ['text'] .= ' Google - Connect to application';

/*create google client object*/
$client = new apiClient();

$oLanguage  = new Language ();
$oGoogle  = new Google (); 

/*submit form, add or update application data*/
if (isset($_POST) && isset ($_POST ['saveChangesApp']))
{
        if ($oGoogle ->checkApplication($_POST))
        {
                $oTemplate -> assign ('successMessage', 'Application\'s data were saveds');                
                unset($_POST);
        }
} 

$appData = $oGoogle ->getApplication();

/*Verify if data of any facebook application are stored*/
if (!empty ($appData))
{
        /*populate google client object*/
        setGoogleClientData ($appData);
        /*get google user*/
        $userData = $oGoogle -> getGoogleUser ($_SESSION ['user_id']);

        /*get oath*/
        $oauth2 = new apiOauth2Service($client);
        if (isset($_GET['code'])) 
        {
                $appValues = array ();
                $appValues ['user_id'] = $_SESSION ['user_id'];
                $client->authenticate();
                $accessToken = $client->getAccessToken();
                $appValues ['access_token'] = $accessToken;

                $userData = $oGoogle -> checkGoogleUser ($appValues);
        }

        if (!empty ($userData) && isset ($userData ['access_token'])) 
        {
                $actual_time = time ();
                $googleAppTime = strtotime ($userData ['date']);

                $accToken = json_decode($userData ['access_token']);
                $expiresIn = $accToken -> expires_in;
                $refreshToken = $accToken -> refresh_token;
                $accToken = $accToken -> access_token;

                $client->setAccessToken($userData ['access_token']);
        }

        if ($client->getAccessToken())  
        {
                $user = $oauth2->userinfo->get();
        } else 
        {
                $authUrl = $client->createAuthUrl();
        }
}   


$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'updateForm.js',
                                'jquery-ui-1.8.17.custom.min.js',
                                'ajaxupload.3.5.js',
                                'greybox.js',
                                'upload.js'
                                )
                            );


$oTemplate -> assign ('appErrors', $oGoogle -> aError);
$oTemplate -> assign ('url', (isset ($authUrl) ? $authUrl : ''));
$oTemplate -> assign ('user', (isset ($user) ? $user : ''));
$oTemplate -> assign ('appValues', (!empty ($appData) ? $appData : ''));              
$oTemplate -> assign ('cssBody', array ('css/jquery-ui-1.8.17.custom.css', 'css/greybox.css'));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'google/google.tpl');
$oTemplate -> display ('default.tpl');

function setGoogleClientData ($appValues)
{
        global $client;

        $client->setClientId ($appValues ['client_id']);
        $client->setClientSecret ($appValues ['client_secret']);
        $client->setRedirectUri ($appValues ['redirect_uri']);
        $client->setDeveloperKey ($appValues ['developer_key']);
}
?>
