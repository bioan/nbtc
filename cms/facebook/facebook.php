<?php
######### COPYRIGHT #################
 
/*
    homepage: http://inforitus.nl
    file: google.php
    @author Laurentiu Ghiur <laurentiu@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Facebook.class.php');

$aFolderData ['text'] .= ' Facebook - Connect to application';

$oFacebook  = new Facebook (); 

if (isset($_POST) && isset ($_POST ['saveChangesApp']))
{
        if ($oFacebook ->checkApplication($_POST))
        {
                $oTemplate -> assign ('successMessage', 'Application\'s data were saveds');                
                unset($_POST);
        }
}

$appData = $oFacebook ->getApplication();

/*Verify if data of any facebook application are stored*/
if (!empty ($appData))
{
        $oTemplate -> assign ('appValues', $appData);
        
        //Application ID
        $app_id = $appData ['appID'];
        
        //Application secret code
        $app_secret = $appData ['appSecret'];
        
        //list of permissions
        $permissions = array ('user_about_me', 'manage_pages', 'user_groups', 'publish_stream');
        
        //redirect url
        $redirect_url = 'http://'.$_SERVER ['SERVER_NAME'].'/cms//social_media/social_media.php?media=facebook';
        
        /*get accept permissions link*/        
        $oTemplate -> assign ('loginUrl', ''); 
        
        //get user from list of facebook users
        $currentFacebookUser = $oFacebook ->getFacebookUser($_SESSION ['user_id'], $redirect_url);
        
        //update publish page id and publish page access token
        if (!empty ($currentFacebookUser) && isset ($_GET ['page']) && isset ($currentFacebookUser ['pages'][$_GET ['page']]))
        {
                $currentFacebookUser ['publishID'] = $_GET ['page'];
                $currentFacebookUser ['publishAccessToken'] = $currentFacebookUser ['pages'][$_GET ['page']]['access_token'];
                $oFacebook -> updateFacebookUser($currentFacebookUser);
        }
        
        $params = array (
            'client_id' => $app_id,
            'client_secret' => $app_secret,
            'redirect_uri' => $redirect_url,
            'code' => 'SOME_INVALID_CODE'
        );
        
        if ($oFacebook ->checkAppIfValid($params))
        {
        
                $code = isset ($_REQUEST["code"]) ? $_REQUEST["code"] : '';

                if (empty($code)) 
                {
                        if (empty ($currentFacebookUser) || (!empty ($currentFacebookUser) && $oFacebook ->checkFacebookUserPermission ($currentFacebookUser ['facebookID'], $currentFacebookUser ['accessToken'], $permissions, $redirect_url) == false))
                        {
                                $_SESSION['state'] = md5(uniqid(rand(), TRUE)); //CSRF protection
                                $dialog_url = "https://www.facebook.com/dialog/oauth?client_id=". $appData ['appID'] . "&redirect_uri=" . urlencode($redirect_url) . "&state=". $_SESSION['state'];

                                $loginUrl = 'https://www.facebook.com/dialog/oauth?client_id=' . $appData ['appID'] . '&redirect_uri=' . urlencode($redirect_url) . '&scope=' . implode (',', $permissions) . '&state=' . $_SESSION['state'];

                                $oTemplate -> assign ('top', true);
                                $oTemplate -> assign ('loginUrl', $loginUrl);
                        }
                }
                else {
                        $oTemplate -> assign ('loginUrl', '');
                }


                if(isset ($_REQUEST['state']) && $_REQUEST['state'] == $_SESSION['state']) 
                {
                        /*Get Access Token*/
                        $token_url = "https://graph.facebook.com/oauth/access_token?"
                        . "client_id=" . $appData ['appID'] . "&redirect_uri=" . urlencode($redirect_url)
                        . "&client_secret=" . $appData ['appSecret'] . "&code=" . $code;

                        $response = @file_get_contents($token_url);
                        $params = null;
                        parse_str($response, $params);
                        $authToken = $params['access_token'];
                        $oTemplate -> assign ('loginUrl', '');

                        $graph_url = "https://graph.facebook.com/me?access_token=" 
                        . $authToken;

                        $userData = @json_decode(@file_get_contents($graph_url), true);

                        /*save facebook user data in database*/
                        $aValues ['facebookID'] = $userData ['id'];
                        $aValues ['userID'] = $_SESSION ['user_id'];
                        $aValues ['accessToken'] = $params['access_token'];
                        $access_token = $params['access_token'];
                        
                        $oFacebook ->checkFacebookUser($aValues);            
                }

                /*check if access toke has expired*/

                if (!empty ($currentFacebookUser))
                {
                        $access_token = $currentFacebookUser ['accessToken'];

                        if ($oFacebook ->checkAccessToken($currentFacebookUser ['facebookID'], $access_token, $redirect_url))
                        {
                                $oTemplate -> assign ('facebookUser', $currentFacebookUser);
                                $oTemplate -> assign ('successAccToken', 'You have the coresponding rights for this application');
                                $oTemplate -> assign ('loginUrl', '');
                        }
                        else 
                        {
                                $oFacebook -> aError ['accTokenredirect'] = 'AccessToken has expired';
                        }
                }
        }
        else 
        {
                $oTemplate -> assign ('loginUrl', 'false');
        }
        
}   


$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'updateForm.js',
                                'jquery-ui-1.8.17.custom.min.js',
                                'ajaxupload.3.5.js',
                                'greybox.js',
                                'upload.js'
                                )
                            );

$oTemplate -> assign ('appErrors', $oFacebook -> aError);
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 50));              
$oTemplate -> assign ('cssBody', array ('css/jquery-ui-1.8.17.custom.css', 'css/greybox.css'));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'facebook/facebook.tpl');
$oTemplate -> display ('default.tpl');


?>
