<?php
######### COPYRIGHT #################
 
/*
	ALL RIGHTS RESERVED
	homepage: http://inforitus.nl
	file: upload-file
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once $_SERVER ['DOCUMENT_ROOT'] . '/cms/includes/default.inc.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/ImageLibrary.class.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Tree.class.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php';  
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Page.class.php';

if (!ini_get ('safe_mode'))
{
	    set_time_limit (0);
}

ini_set ("memory_limit","60M");
	
$oImage     = new ImageLibrary ();
$pStatement = Database :: getInstance ();
$oPage      = new Page ();




$pStatement -> query ("INSERT INTO images(date) VALUES (NOW())");
		        
$iLastInsertId = $pStatement -> insert_id;

$sPath = '../files/' . $_GET ['module'] . '/';

$uploaddir = $sPath; 

if ($oImage -> uploadImage ($GLOBALS ['cfg']['include'] . '/' . substr ($uploaddir, 3, -1), $iLastInsertId) && !isset ($_SESSION ['image']['uploaded'])) 
{	             
        $aImages = array (
                1 => array (
                        'width'     => 1000, 
                        'height'    => 1000, 
                        'location'  => $GLOBALS ['cfg']['include'] . '/' . substr ($uploaddir, 3, -1),
                        'name'      => ''
                 )
        );
        
        if (!$newImageData = $oImage -> reduceAndCreateThumbnail ($aImages))
        {
                echo 'Er is een fout opgetreden bij het uploaden van de afbeelding.';
        }
        
        if (preg_match ('#(jpeg|png|gif|png)$#i', $oImage -> sMimeType, $matches)) 
        {
                $sFunctionName = sprintf ('imagecreatefrom%s', $matches [1]);
                
                $oImage -> OrginalImage = $sFunctionName ($oImage -> sLocation . '/' . $oImage -> sImageName . $oImage -> sExtensie);    
        } 
                         
        
        $file = $uploaddir . $oImage -> sImageName . $oImage -> sExtensie;
        list($src_w, $src_h, $type, $attr) = getimagesize ($file);
        
        $info      = pathinfo ($_FILES ['image']['name']);
        $file_name =  basename ($_FILES ['image']['name'], '.'.$info['extension']);
        
        $_SESSION ['image'] = array (
                                        'name'               => preg_replace ('#.*/([^/]+)$#','${1}', $oImage -> sImageName . $oImage -> sExtensie),
                                        'extensie'           => $oImage -> sExtensie,
                                        'width'              => $src_w,
                                        'orig_width'         => $src_w,
                                        'height'             => $src_h,
                                        'orig_height'        => $src_h,
                                        'mime_type'          => $oImage -> sMimeType,
                                        'location'           => preg_replace (sprintf ('#%s(.*)$#', $GLOBALS ['cfg']['include']), '${1}', $oImage -> sLocation),
                                        'oid'                => $oImage -> sImageName,
                                        'title'              => $file_name
                                );
                                
        if (isset ($_GET ['crop']))
        {
                if (!$oImage -> updateImage (array (
                        'mime_type'   => $_SESSION ['image']['mime_type'],
                        'orig_width'  => $_SESSION ['image']['orig_width'], 
                        'orig_height' => $_SESSION ['image']['orig_height'], 
                        'extensie'    => $_SESSION ['image']['extensie'],
                        'oid'         => $_SESSION ['image']['oid'],
                        'type'        => 'photoalbum',
                        'title'       => $_SESSION ['image']['title'],
                        'path'        => $aImages [1]['location'] . '/' . $oImage -> sImageName . '_' . 100 . 'x' . 100 . $oImage -> sExtensie)
                        )
                    ) 
                {
                        trigger_error (E_CORE_WARNING . ' Er is een fout opgetreden!');
                        exit;
                }
        }
        
}

//Array ( [name] => 221.jpg [extensie] => .jpg [width] => 600 [orig_width] => 600 
//[height] => 450 [orig_height] => 450 [mime_type] => image/jpeg [location] => /files [oid] => 221 )  
       
#Do some math to figure out which way we'll need to crop the image
#to get it proportional to the new size, then crop or adjust as needed
if (!isset ($_GET ['crop']))
{
        $aImageOpts = array ();
        
        foreach ((!isset ($_GET ['template']) ? $aImageSizes [$_GET ['module']] : $aImageSizes [$_GET ['module']][$_GET ['template']]) as $size)
        {
                if (isset ($size ['width']))
                        array_push ($aImageOpts, array (
                                'x1'     => 0,
                		        'x2'     => $src_w,
                		        'y1'     => 0,
                		        'y2'     => $src_h,
                		        'width'  => $size ['width'],
                		        'height' => $size ['height'],
                                'upload' => true,
                                'name'   => (isset ($size ['name']) ? $size ['name'] : '')        
                        ));   
        }
        
        if ($aNewImageValues = $oImage -> cropImage ($_SESSION ['image'], $aImageOpts)) 
        {
        	    if (empty ($_GET ['action']) || $_GET ['action'] != 'change') 
                {
        		        foreach ($aNewImageValues as $newImageValue) 
                        {
        			            if (!$oImage -> updateImage (array_merge ($newImageValue, array (
        					            'orig_width'  => $_SESSION ['image']['orig_width'], 
        					            'orig_height' => $_SESSION ['image']['orig_height'], 
        					            'extensie'    => $_SESSION ['image']['extensie'],
        					            'oid'         => $_SESSION ['image']['oid'],
        					            'type'        => 'photoalbum',
                                        'title'       => $_SESSION ['image']['title'])
                                        ))
        				            ) 
                                {
        				                trigger_error (E_CORE_WARNING . ' Er is een fout opgetreden!');
        				                exit;
        			            }
        		        }
        	    } 
                elseif (!empty ($_GET ['action']) && $_GET ['action'] == 'change') 
                {
        		        require_once $_SERVER ['DOCUMENT_ROOT'] . '/cms/includes/Database.class.php';
        		        
                        $pStatement = Database :: getInstance (); 
        		        
                        $pResult = $pStatement  ->  query ("
        			            SELECT
        				            *
        			            FROM
        				            images
        			            WHERE
        				            id = " . $_SESSION ['image']['oid'] . "
        		        ");
        						        
        		        $aResult = $pResult  ->  fetch_assoc ();
                        
        		        if (!$oImage -> updateImage (array_merge ($aResult, current ($aNewImageValues)), $_SESSION ['image']['oid'])) 
                        {
        			            trigger_error (E_CORE_WARNING . ' Er is een fout opgetreden!');
        			            exit;
        		        }
        	    }
             
        	    
        }
        
        $aReturn ['link'] = substr ($uploaddir, 2) . $_SESSION ['image']['oid'] . '_100x100' . $_SESSION ['image']['extensie'];
        $aReturn ['lang'] = $_GET ['lang'];
        $aReturn ['id'] = $_SESSION ['image']['oid'];
        print_r (json_encode ($aReturn));
        //echo substr ($uploaddir, 2) . $_SESSION ['image']['oid'] . '_100x100' . $_SESSION ['image']['extensie'];
}
else
{
        echo substr ($uploaddir, 2) . $_SESSION ['image']['oid'] . $_SESSION ['image']['extensie'];   
}

/* 
if (move_uploaded_file($_FILES['image']['tmp_name'], $file)) 
{ 
        
        list($src_w, $src_h, $type, $attr) = getimagesize($file);
        
        #Do some math to figure out which way we'll need to crop the image
        #to get it proportional to the new size, then crop or adjust as needed
        
        $x_ratio = $tn_w / $src_w;
        $y_ratio = $tn_h / $src_h;
        
        if (($src_w <= $tn_w) && ($src_h <= $tn_h)) {
            $new_w = $src_w;
            $new_h = $src_h;
        } elseif (($x_ratio * $src_h) < $tn_h) {
            $new_h = ceil($x_ratio * $src_h);
            $new_w = $tn_w;
        } else {
            $new_w = ceil($y_ratio * $src_w);
            $new_h = $tn_h;
        }
        
         
}  
else 
{
	   echo "error";
}
*/

unset ($_SESSION ['image']);

?>