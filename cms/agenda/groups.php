<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: groups
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Group.class.php');

$aActions = array (
        1 => 'new',
        2 => 'overview',
        3 => 'delete',
        4 => 'edit'
);

$aFolderData = array (
        'url'  => 'groepen.png',
        'text' => 'Agenda groepen',
        'page' => 'agenda'
);

$oLanguage = new Language ();

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        $oAgendaGroup = new Group ('agenda');
        
        $aLanguages = $oLanguage -> getLanguages ();
        
        $oTemplate -> assign ('languages', $aLanguages);
        
        if ($_GET ['action'] == 'new') 
        {
                if (!empty ($_POST)) 
                {
                        if ($oAgendaGroup -> addGroup ($_POST)) 
                        {
                                redirect ($SiteRoot . '/agenda/groups.php?action=overview');  
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oAgendaGroup -> getLastError ());
                        }
                }
            
        } 
        elseif ($_GET ['action'] == 'overview') 
        {
                require_once 'Pager/Pager.php';  
                   
                $aParams = array (
                        'mode'                  => 'Sliding',
                        'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 50), //show 50 items per page 
                        'delta'                 => 3, 
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
                        'delta'                 => 2,
                        'clearIfVoid'           => false,
                        'itemData'              => $oAgendaGroup -> getGroups (0, '', (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                );
                
                $oPager = & Pager::factory ($aParams);
                $aData  =& $oPager -> getPageData ();
                
                $oTemplate -> assign ('overview', true);   
                $oTemplate -> assign ('links', $oPager -> links);
                $oTemplate -> assign ('agendaGroup', $aData);      
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1)); 
        } 
        elseif (($_GET ['action'] == 'delete') && is_numeric ($_GET ['id']))
        {
                if ($oAgendaGroup -> deleteGroup ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/agenda/groups.php?action=overview');
                }
        } 
        elseif ($_GET ['action'] == 'edit' && is_numeric ($_GET ['id'])) 
        { 
                $oTemplate -> assign ('agendaGroup', $oAgendaGroup -> getGroups ($_GET ['id']));   
                 
                if (!empty ($_POST)) 
                {
                        if ($oAgendaGroup -> changeGroup ($_GET ['id'], $_POST)) 
                        {
                                redirect ($SiteRoot . '/agenda/groups.php?action=overview');     
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oAgendaGroup -> getLastError ());
                        }
                }
        } 
        else 
        {
                redirect ($SiteRoot . '/agenda/groups.php?action=overview');
        }
} 
else 
{
        redirect ($SiteRoot . '/agenda/groups.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
        'jquery.min.js', 
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js',
        'tablednd.js',
        'sort_table.js'
        )
        );

$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 50));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'agenda/groups.tpl');
$oTemplate -> display ('default.tpl');
?>
