<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: agenda
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/ckeditor/ckeditor.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php');      
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Agenda.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Group.class.php');

$aFolderData = array (
        'url'  => 'nieuws.gif',
        'text' => 'Agenda',
        'page' => 'agenda',
);

$aActions = array (
        1 => 'new',
        2 => 'overview',
        3 => 'edit',
        4 => 'delete'
);
		
$oAgenda      = new Agenda ();
$oAgendaGroup = new Group ('agenda');
$oLanguage    = new Language ();
$oCKeditor    = new CKEditor ();

$oCKeditor -> basePath          = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/';
$oCKeditor -> returnOutput      = true;
$oCKeditor -> config ['width']  = 590; 
$oCKeditor -> config ['height'] = 300; 
$oCKeditor -> config ['extraAllowedContent'] = 'img[alt,border,width,height,align,vspace,hspace,!src];';
$oCKeditor -> config ['allowedContent'] = true;

$aConfig = array ();
$aConfig ['toolbar'] = array (
        array ( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),
        array ( 'Image', 'Link', 'Unlink', 'Anchor', 'MediaEmbed' )
);

$oCKeditor -> config ['filebrowserBrowseUrl']      = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserImageBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserFlashBrowseUrl'] = $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/connector.php';
$oCKeditor -> config ['filebrowserUploadUrl']      = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=File';
$oCKeditor -> config ['filebrowserImageUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
$oCKeditor -> config ['filebrowserFlashUploadUrl'] = 'http://' . $_SERVER ['SERVER_NAME'] . $GLOBALS ['cfg']['relativ'] . '/cms/includes/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        $aLanguages = $oLanguage -> getLanguages ();
        
        $oTemplate -> assign ('languages', $aLanguages);
        
	    if ($_GET ['action'] == 'new') 
        {
		        $oTemplate -> assign ('AgendaGroups', $oAgendaGroup -> getGroups ());   
                
                $aContent   = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [] = $oCKeditor -> editor ("description[" . $aLanguages [$i]['id'] . "]", (isset ($_POST ['description'][$aLanguages [$i]['id']]) ? $_POST ['description'][$aLanguages [$i]['id']] : ''), $aConfig);
                }
                
	   	        $oTemplate -> assignByRef ('FCKeditor', $aContent);   
                
                if (isset ($_POST ['keywords']))
                {
                        $aKeywords = keywords ($_POST ['description']);
                        
                        $oTemplate -> assign ('keywords', $aKeywords);
                }
                elseif (!empty ($_POST)) 
                {
                        if ($oAgenda -> addAgenda ($_POST)) 
                        {
                                redirect ($SiteRoot . '/agenda/agenda.php?action=overview');
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oAgenda -> getLastError ());
                        }
                }
            
        } 
        elseif ($_GET ['action'] == 'overview') 
        {
                require_once 'Pager/Pager.php';    
                
                $aParams = array (
                        'mode'                  => 'Sliding',
                        'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 50), //show 50 items per page 
                        'delta'                 => 3, 
                        'extraVars'             => array ('sort' => (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'),
                        'sorttype'              => (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC')), 
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
                        'delta'                 => 2,
                        'clearIfVoid'           => false,
                        'itemData'              => $oAgenda -> getAgenda (0, '', 0, '', (isset ($_GET ['sort']) ? $_GET ['sort'] : 'order_number'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'DESC'))
                );
                
                $oPager = & Pager::factory ($aParams);
                $aData  =& $oPager -> getPageData ();
                
                $oTemplate -> assign ('links', $oPager -> links);
                $oTemplate -> assign ('AgendaItems', $aData);
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));
        } 
        elseif ($_GET ['action'] == 'edit') 
        {
    	        $oTemplate -> assign ('AgendaGroups', $oAgendaGroup -> getGroups ());   
                
                $aAgendaById = $oAgenda -> getAgenda ($_GET ['id']);
                
                $aContent   = array ();
                
                for ($i = 0; $i < count ($aLanguages); $i++)
                {
                        $aContent [] = $oCKeditor -> editor ("description[" . $aLanguages [$i]['id'] . "]", (isset ($aAgendaById [$aLanguages [$i]['id']]['description']) ? html_entity_decode ($aAgendaById [$aLanguages [$i]['id']]['description']) : ''), $aConfig);
                }
                
	   	        $oTemplate -> assignByRef ('FCKeditor', $aContent); 
                
                $oTemplate -> assign ('values', $aAgendaById);
                
                if (isset ($_POST ['keywords']))
                {
                        $aKeywords = keywords ($_POST ['description']);
                        
                        $oTemplate -> assign ('keywords', $aKeywords);
                }
                elseif (!empty ($_POST)) 
                {
                        if ($oAgenda -> updateAgenda ($_GET ['id'], $_POST)) 
                        {
                                redirect ($SiteRoot . '/agenda/agenda.php?action=overview');
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oAgenda -> getLastError ());
                        }
                }
        } 
        elseif ($_GET ['action'] == 'delete') 
        {
                if ($oAgenda -> deleteAgenda ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/agenda/agenda.php?action=overview');
                }
        } 
} 
else 
{
        redirect ($SiteRoot . '/agenda/agenda.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', 
	    array (
                'jquery.min.js', 
	            'cufon-yui.js', 
                'Calibri_400.font.js', 
                'title.js', 
	            'jquery-ui-1.8.17.custom.min.js',
	            ($_GET ['action'] == 'overview' ? 'tablednd.js' : ''),
                ($_GET ['action'] == 'overview' ? 'sort_table.js' : ''),
                'ajaxupload.3.5.js',
                'greybox.js',
                'upload.js'
	    )
);
$oTemplate -> assign ('cssBody', array ('css/jquery-ui-1.8.17.custom.css', 'css/greybox.css'));
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('contentInclude', 'agenda/agenda.tpl');
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 50));
$oTemplate -> display ('default.tpl');
?>
