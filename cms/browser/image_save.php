<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: image_save
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once '../includes/default.inc.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/ImageLibrary.class.php';

$oImage = new ImageLibrary ();

$iThumbWidth  = $GLOBALS ['cfg']['images']['thumbWidth'];
$dDeelfactor  = $_POST ['width'] / $iThumbWidth; 
$iThumbHeight = round ($_POST ['height'] / $dDeelfactor); 

// save the image and a thumbnail for use in the imageview
$aImageOpts = array (
	    1 => array (
		        'x1'     => $_POST ['x1'],
		        'x2'     => $_POST ['x2'],
		        'y1'     => $_POST ['y1'],
		        'y2'     => $_POST ['y2'],
		        'width'  => $_POST ['width'],
		        'height' => $_POST ['height']
	    ),
	    2 => array (
		        'x1'     => $_POST ['x1'],
		        'x2'     => $_POST ['x2'],
		        'y1'     => $_POST ['y1'],
		        'y2'     => $_POST ['y2'],
		        'width'  => $iThumbWidth,
		        'height' => $iThumbHeight
	    ),
        3 => array (
                'x1'     => $_POST ['x1'],
                'x2'     => $_POST ['x2'],
                'y1'     => $_POST ['y1'],
                'y2'     => $_POST ['y2'],
                'width'  => 263,
                'height' => 263
        ),
        4 => array (
                'x1'     => $_POST ['x1'],
                'x2'     => $_POST ['x2'],
                'y1'     => $_POST ['y1'],
                'y2'     => $_POST ['y2'],
                'width'  => 156,
                'height' => 156
        ),
        5 => array (
                'x1'     => $_POST ['x1'],
                'x2'     => $_POST ['x2'],
                'y1'     => $_POST ['y1'],
                'y2'     => $_POST ['y2'],
                'width'  => 220,
                'height' => 165
        )	
);

// if existe the old image we delete them
if (!empty ($_GET ['action'])) 
{
	    $aExistingImages = @glob ($_SESSION ['browser']['currentFolder'] . '/' . $_SESSION ['image']['oid'] . '_*');
	    
        if (!empty ($aExistingImages)) 
        {
		        foreach ($aExistingImages as $toDeleteImage) 
                {
			            @unlink ($toDeleteImage);
		        }
	    }
}

if ($aNewImageValues = $oImage -> cropImage ($_SESSION ['image'],$aImageOpts)) 
{
	    if (empty ($_GET ['action']) || $_GET ['action'] != 'change') 
        {
		        foreach ($aNewImageValues as $newImageValue) 
                {
			            if (!$oImage -> updateImage (array_merge ($newImageValue, array (
					            'orig_width'  => $_SESSION ['image']['orig_width'], 
					            'orig_height' => $_SESSION ['image']['orig_height'], 
					            'extensie'    => $_SESSION ['image']['extensie'],
					            'oid'         => $_SESSION ['image']['oid'],
					            'type'        => 'photoalbum')))
				            ) 
                        {
				                trigger_error (E_CORE_WARNING . ' Er is een fout opgetreden!');
				                exit;
			            }
		        }
	    } 
        elseif (!empty ($_GET ['action']) && $_GET ['action'] == 'change') 
        {
		        require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php';
		        
                $pStatement = Database :: getInstance (); 
		        
                $pResult = $pStatement  ->  query ("
			            SELECT
				            *
			            FROM
				            images
			            WHERE
				            id = " . $_SESSION ['image']['oid'] . "
		        ");
						        
		        $aResult = $pResult  ->  fetch_assoc ();
                
		        if (!$oImage -> updateImage (array_merge ($aResult, current ($aNewImageValues)), $_SESSION ['image']['oid'])) 
                {
			            trigger_error (E_CORE_WARNING . ' Er is een fout opgetreden!');
			            exit;
		        }
	    }
        
	    $oTemplate -> assign ('succesvol', true);
	    unset ($_SESSION ['image']);
}

$oTemplate -> display ('popup_image.inc.tpl');
?>
