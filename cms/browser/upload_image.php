<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: upload_image
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once '../includes/default.inc.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/ImageLibrary.class.php';
require_once $GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php';  

if (!ini_get ('safe_mode'))
{
	    set_time_limit (0);
}

ini_set ("memory_limit","60M");
	
$oImage     = new ImageLibrary ();
$pStatement = Database :: getInstance (); 

if ((!isset ($_GET ['action']) || $_GET ['action'] != 'change'))
{
	    if (isset ($_POST) && isset ($_POST ['submit'])) 
        {
		    
                // insert fake image so we have the image_oid
		        $pStatement -> query ("INSERT INTO images(date) VALUES (NOW())");
		        
                $iLastInsertId = $pStatement -> insert_id;
		        
                if ($oImage -> uploadImage ($_SESSION ['browser']['currentFolder'], $iLastInsertId) && !isset ($_SESSION ['image']['uploaded']) && !empty ($_FILES ['image']['name'])) 
                {	                                                      
			            $aImages = array (
		                        1 => array (
		                                'width'     => 600, 
		                                'height'    => 0, 
		                                'location'  => $_SESSION ['browser']['currentFolder'],
		                                'name'      => ''
		                         )
		                );
		                
                        if (!$newImageData = $oImage -> reduceAndCreateThumbnail ($aImages))
                        {
				                echo 'Er is een fout opgetreden bij het uploaden van de afbeelding.';
			            }
                        
                        $_SESSION ['image'] = reset ($newImageData);
		        }
	    }
} 
elseif (!empty ($_GET ['action']) && $_GET ['action'] == 'change')
{
	    $pResult = $pStatement -> query ("
		        SELECT
			        *
		        FROM
			        images
		        WHERE
			        id = " . $_GET ['oid'] . "
		        "
	    );
	    
        $aResult = $pResult -> fetch_assoc ();
        
	    $_SESSION ['image'] = array (
		        'location'       => preg_replace ('#(.*)/([^/]+)$#','${1}', $aResult ['image_name']),
		        'name'           => $aResult ['id'] . $aResult ['image_ext'],
		        'width'          => $aResult ['orig_image_width'],
		        'height'         => $aResult ['orig_image_height'],
		        'cropped_width'  => $aResult ['image_width'],
		        'cropped_height' => $aResult ['image_height'],
		        'mime_type'      => $aResult ['mime_type'],
		        'oid'            => $aResult ['id'],
		        'extensie'       => $aResult ['image_ext']
	    );
}

$oTemplate -> display ('popup_image.inc.tpl');
?>
