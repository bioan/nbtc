<?php
define ('ga_email','andrei.dragos88@gmail.com');
define ('ga_password','Andrei88');
define ('ga_profile_id','54616384');

require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/googleanalytics.class.php');

$aFolderData = array (
        'url'  => 'folder_photoalbum.gif',
        'text' => 'Google Analytics',
        'page' => 'ga'
);

/*$ga = new gapi(ga_email,ga_password);

$ga->requestReportData(ga_profile_id,array('browser','browserVersion'),array('pageviews','visits'), '', '', (isset ($_POST ['date_start']) ? $_POST ['date_start'] : ''), (isset ($_POST ['date_end']) ? $_POST ['date_end'] : ''));

echo '<pre>' . print_r($ga -> getTotalResults (),true);
*/

/*try {
      
      // construct the class
      $oAnalytics = new analytics('andrei.dragos88@gmail.com', 'Andrei88');
      
      // set it up to use caching
      $oAnalytics->useCache();
      
      //$oAnalytics->setProfileByName('[Google analytics accountname]');
      $oAnalytics->setProfileById('ga:54616384');
      
      // set the date range
      //$oAnalytics->setMonth(11, 2011);
      $oAnalytics->setDateRange('2011-11-01', '2012-11-30');
      
      echo '<pre>';
      // print out visitors for given period
      print_r($oAnalytics->getVisitors());
      
      // print out pageviews for given period
      print_r($oAnalytics->getPageviews());
      
      // use dimensions and metrics for output
      // see: http://code.google.com/intl/nl/apis/analytics/docs/gdata/gdataReferenceDimensionsMetrics.html
      print_r($oAnalytics->getData(array(   'dimensions' => 'ga:keyword',
                                            'metrics'    => 'ga:visits',
                                            'sort'       => 'ga:keyword')));
      
  } catch (Exception $e) { 
      echo 'Caught exception: ' . $e->getMessage(); 
  }
*/
try {
	// create an instance of the GoogleAnalytics class using your own Google {email} and {password}
	$ga = new GoogleAnalytics('andrei.dragos88@gmail.com','Andrei88');

	// set the Google Analytics profile you want to access - format is 'ga:123456';
	$ga->setProfile('ga:54616384');

	// set the date range we want for the report - format is YYYY-MM-DD
	$ga->setDateRange('2011-11-01','2012-01-06');

	// get the report for date and country filtered by Australia, showing pageviews and visits
	$aReport = $ga->getReport(
		array('dimensions'=>urlencode('ga:date,ga:country'),
			'metrics'=>urlencode('ga:pageviews,ga:visits'),
			//'filters'=>urlencode('ga:country=@Australia'),
			//'sort'=>'-ga:pageviews'
			)
		);
        
    $aReportPages = $ga->getReport(
    	array('dimensions'=>urlencode('ga:landingPagePath,ga:pageTitle'),
    		'metrics'=>urlencode('ga:entrances,ga:timeOnPage'),
    		'sort'=>'-ga:entrances'
    		)
	);

	//print out the $report array
	//print "<pre>";
	//print_r($aReportPages);
	//print "</pre>";
	
} catch (Exception $e) { 
	print 'Error: ' . $e->getMessage(); 
}



$iTotalPageViews = 0;
$iTotalVisits = 0;
$aResults = array ();

foreach ($aReport as $k => $item)
{
        $aKey = explode ('~~', $k);
        $sDate = substr ($aKey [0], 0, 4) . '-' . substr ($aKey [0], 4, 2) . '-' . substr ($aKey [0], 6, 2);
        $sCountry = $aKey [1];
        
        if (!isset ($aResults [$sDate]))
        {
                $aResults [$sDate]['visits'] = $item ['ga:visits'];
                $aResults [$sDate]['pageviews'] = $item ['ga:pageviews']; 
        }
        else
        {
                $aResults [$sDate]['visits'] += $item ['ga:visits'];
                $aResults [$sDate]['pageviews'] += $item ['ga:pageviews']; 
        } 
        
        $iTotalVisits    += $item ['ga:visits'];
        $iTotalPageViews += $item ['ga:pageviews'];
}

$aPages = array ();

foreach ($aReportPages as $k => $item)
{
        $aKey = explode ('~~', $k);
        $sTitle = $aKey [1];
        $sLink = $aKey [0];
        
        if (!isset ($aPages [$sLink]))
                $aPages [$sLink]['entrances'] = $item ['ga:entrances'];
        else
                $aPages [$sLink]['entrances'] += $item ['ga:entrances'];
}

//print_r($aResults);

$sVisitsChart = '[';
$sPageViewsChart = '[';

foreach ($aResults as $k => $item)
{
        $sVisitsChart .= '["' . $k . '", ' . $item ['visits'] . '],';
        $sPageViewsChart .= '["' . $k . '", ' . $item ['pageviews'] . '],';
}

$sVisitsChart = substr ($sVisitsChart, 0, -1).'];';
$sPageViewsChart = substr ($sPageViewsChart, 0, -1).'];';


$oTemplate -> assign ('date_start', isset ($_POST ['date_start']) ? $_POST ['date_start'] : '');
$oTemplate -> assign ('results', $aResults);
$oTemplate -> assign ('visitschart', $sVisitsChart);
$oTemplate -> assign ('pages', $aPages);
$oTemplate -> assign ('pageviewschart', $sPageViewsChart);
$oTemplate -> assign ('pageviews', $iTotalPageViews);
$oTemplate -> assign ('visits', $iTotalVisits);
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('javascriptBody', array (
        'jquery.min.js', 
        'cufon-yui.js', 
        'Calibri_400.font.js', 
        'title.js',
        'lightbox.js',
        'jscalendar/calendar.js',
        'jscalendar/lang/calendar-en.js',
        'jscalendar/calendar-setup.js',
        'ajaxupload.3.5.js',
        'greybox.js',
        'upload.js',
        'jquery.jqplot.min.js',
        'syntaxhighlighter/scripts/shCore.min.js',
        'syntaxhighlighter/scripts/shBrushJScript.min.js',
        'syntaxhighlighter/scripts/shBrushXml.min.js',
        'plugins/jqplot.canvasTextRenderer.min.js',
        'plugins/jqplot.canvasAxisLabelRenderer.min.js',
        'plugins/jqplot.logAxisRenderer.js',
        'plugins/jqplot.canvasTextRenderer.js',
        'plugins/jqplot.canvasAxisLabelRenderer.js',
        'plugins/jqplot.canvasAxisTickRenderer.js',
        'plugins/jqplot.dateAxisRenderer.js',
        'plugins/jqplot.categoryAxisRenderer.js',
        'plugins/jqplot.barRenderer.js',
        'plugins/jqplot.highlighter.js',
        'plugins/jqplot.cursor.min.js',
        'example.min.js'
        )
        );

$oTemplate -> assign ('cssBody', array ('css/jquery.jqplot.min.css', 'js/syntaxhighlighter/styles/shThemejqPlot.min.css', 'js/syntaxhighlighter/styles/shCoreDefault.min.css', 'js/jscalendar/calendar-win2k-1.css', 'css/greybox.css'));
            
$oTemplate -> assign ('contentInclude', 'ga/ga.tpl');
$oTemplate -> display ('default.tpl');
?>