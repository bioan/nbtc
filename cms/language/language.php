<?php
######### COPYRIGHT #################
 
/*
	homepage: http://inforitus.nl
	file: language
    @author Andrei Dragos <andrei@inforitus.nl>
*/

######### COPYRIGHT #################
require_once '../includes/default.inc.php';
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Database.class.php'); 
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Tree.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Page.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Agenda.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/News.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/PhotoAlbum.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/VideoAlbum.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Webshop.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Group.class.php');
include_once ($GLOBALS ['cfg']['include'] . '/cms/includes/Language.class.php');

$aFolderData = array(
        'url' => 'nieuws.gif',
        'text' => 'Language',
        'page' => 'language'
);

$aActions = array(
        1 => 'new',
        2 => 'overview',
        3 => 'edit',
        4 => 'delete',
        5 => 'stats',
        6 => 'texts',
        7 => 'ajax'
);

$oPage        = new Page ();
$oGroupAgenda = new Group ('agenda');
$oGroupNews   = new Group ('news');
$oAgenda      = new Agenda ();
$oNews        = new News ();
$oPhotoAlbum  = new PhotoAlbum ();
$oVideoAlbum  = new VideoAlbum ();
$oWebshop     = new Webshop ();
$oLanguage    = new Language ();

$aGroupsAgenda = $oGroupAgenda -> getGroups ();
$aGroupsNews   = $oGroupNews -> getGroups ();
$aPages        = $oPage -> getAllNodes (0, 1);
$aAgenda       = $oAgenda -> getAgenda ();
$aNews         = $oNews   -> getNews ();
$aPhotoAlbum   = $oPhotoAlbum -> getPhotos ();
$aCatPhoto     = $oPhotoAlbum -> getAllNodes (0, 1);
$aVideoAlbum   = $oVideoAlbum -> getVideos ();
$aCatVideo     = $oVideoAlbum -> getAllNodes (0, 1);
$aWebshop      = $oWebshop -> getProducts ();
$aCatWebshop   = $oWebshop -> getAllNodes (0, 1);

$aLanguages = $oLanguage -> getLanguages ();

if (isset ($_GET ['action']) && !empty ($_GET ['action']) && in_array ($_GET ['action'], $aActions)) 
{
        if ($_GET ['action'] == 'ajax')
        {
                print_r (json_encode ($aLanguages));
                exit;
        }
        elseif ($_GET ['action'] == 'stats')
        {
                foreach ($aLanguages as $k => $value)
                {
                        $aLang [$value ['id']] = $value;
                        $aLang [$value ['id']]['all']    = count ($aAgenda);
                        $aLang [$value ['id']]['filled'] = 0;
                        $aLang [$value ['id']]['modules']['agenda']['all']    = count ($aAgenda);
                        $aLang [$value ['id']]['modules']['agenda']['filled'] = 0;
                        $aLang [$value ['id']]['modules']['agenda']['item']   = $GLOBALS ['cfg']['relativ'] . '/cms/agenda/agenda.php?action=edit&';
                        $aLang [$value ['id']]['modules']['agenda']['cat']    = $GLOBALS ['cfg']['relativ'] . '/cms/agenda/groups.php?action=edit&';
                        
                        foreach ($aAgenda as $item)
                        {
                                $item ['bitem'] = true;
                                $aLang [$value ['id']]['modules']['agenda']['items'][] = $item;
                                
                                if (isset ($item [$value ['id']]) && ($item [$value ['id']]['title'] != '' || $item [$value ['id']]['description'] != ''))
                                {
                                        $aLang [$value ['id']]['modules']['agenda']['filled']++;
                                        $aLang [$value ['id']]['filled']++;              
                                }
                        }
                        
                        $aLang [$value ['id']]['all']                      += count ($aGroupsAgenda);
                        $aLang [$value ['id']]['modules']['agenda']['all'] += count ($aGroupsAgenda);
                        
                        foreach ($aGroupsAgenda as $item)
                        {
                                $aLang [$value ['id']]['modules']['agenda']['items'][] = $item;
                                
                                if (isset ($item [$value ['id']]) && ($item [$value ['id']]['group_name'] != '' || $item [$value ['id']]['description'] != ''))
                                {
                                        $aLang [$value ['id']]['modules']['agenda']['filled']++;
                                        $aLang [$value ['id']]['filled']++;              
                                }       
                        } 
                        
                        $aLang [$value ['id']]['all']                      += count ($aNews);
                        $aLang [$value ['id']]['modules']['news']['all']    = count ($aNews);
                        $aLang [$value ['id']]['modules']['news']['filled'] = 0;
                        $aLang [$value ['id']]['modules']['news']['item']   = $GLOBALS ['cfg']['relativ']. '/cms/news/news.php?action=edit&';
                        $aLang [$value ['id']]['modules']['news']['cat']    = $GLOBALS ['cfg']['relativ'] . '/cms/news/groups.php?action=edit&';
                        
                        foreach ($aNews as $item)
                        {
                                $item ['bitem'] = true;
                                $aLang [$value ['id']]['modules']['news']['items'][] = $item;
                                
                                if (isset ($item [$value ['id']]) && ($item [$value ['id']]['title'] != '' || $item [$value ['id']]['description'] != ''))
                                {
                                        $aLang [$value ['id']]['modules']['news']['filled']++;
                                        $aLang [$value ['id']]['filled']++;              
                                }       
                        } 
                        
                        $aLang [$value ['id']]['all']                    += count ($aGroupsNews);
                        $aLang [$value ['id']]['modules']['news']['all'] += count ($aGroupsNews);
                        
                        foreach ($aGroupsNews as $item)
                        {
                                $aLang [$value ['id']]['modules']['news']['items'][] = $item;
                                
                                if (isset ($item [$value ['id']]) && ($item [$value ['id']]['group_name'] != '' || $item [$value ['id']]['description'] != ''))
                                {
                                        $aLang [$value ['id']]['modules']['news']['filled']++;
                                        $aLang [$value ['id']]['filled']++;              
                                }       
                        } 
                        
                        $aLang [$value ['id']]['all'] += count ($aPhotoAlbum);
                        $aLang [$value ['id']]['modules']['photoalbum']['all']    = count ($aPhotoAlbum);
                        $aLang [$value ['id']]['modules']['photoalbum']['filled'] = 0;
                        $aLang [$value ['id']]['modules']['photoalbum']['item']   = $GLOBALS ['cfg']['relativ'] . '/cms/photoalbum/photoalbum.php?action=edit&';
                        $aLang [$value ['id']]['modules']['photoalbum']['cat']    = $GLOBALS ['cfg']['relativ'] . '/cms/photoalbum/cats.php?action=edit&';
                        
                        foreach ($aPhotoAlbum as $item)
                        {
                                $item ['bitem'] = true;
                                $aLang [$value ['id']]['modules']['photoalbum']['items'][] = $item;
                                
                                if (isset ($item [$value ['id']]) && ($item [$value ['id']]['title'] != '' || $item [$value ['id']]['description'] != ''))
                                {
                                        $aLang [$value ['id']]['modules']['photoalbum']['filled']++;
                                        $aLang [$value ['id']]['filled']++;              
                                }       
                        } 
                        
                        $aLang [$value ['id']]['all'] += count ($aCatPhoto);
                        $aLang [$value ['id']]['modules']['photoalbum']['all'] += count ($aCatPhoto);
                        
                        foreach ($aCatPhoto as $item)
                        {
                                $aLang [$value ['id']]['modules']['photoalbum']['items'][] = $item;
                                
                                if (isset ($item [$value ['id']]) && $item [$value ['id']]['name'] != '')
                                {
                                        $aLang [$value ['id']]['modules']['photoalbum']['filled']++;
                                        $aLang [$value ['id']]['filled']++;              
                                }       
                        }
                        
                        $aLang [$value ['id']]['all'] += count ($aVideoAlbum);
                        $aLang [$value ['id']]['modules']['videoalbum']['all']    = count ($aVideoAlbum);
                        $aLang [$value ['id']]['modules']['videoalbum']['filled'] = 0;
                        $aLang [$value ['id']]['modules']['videoalbum']['item']   = $GLOBALS ['cfg']['relativ'] . '/cms/videoalbum/videoalbum.php?action=edit&';
                        $aLang [$value ['id']]['modules']['videoalbum']['cat']    = $GLOBALS ['cfg']['relativ'] . '/cms/videoalbum/cats.php?action=edit&';
                        
                        foreach ($aVideoAlbum as $item)
                        {
                                $item ['bitem'] = true;
                                $aLang [$value ['id']]['modules']['videoalbum']['items'][] = $item;
                                
                                if (isset ($item [$value ['id']]) && ($item [$value ['id']]['title'] != '' || $item [$value ['id']]['description'] != ''))
                                {
                                        $aLang [$value ['id']]['modules']['videoalbum']['filled']++;
                                        $aLang [$value ['id']]['filled']++;              
                                }       
                        } 
                        
                        $aLang [$value ['id']]['all'] += count ($aCatVideo);
                        $aLang [$value ['id']]['modules']['videoalbum']['all'] += count ($aCatVideo);
                        
                        foreach ($aCatVideo as $item)
                        {
                                $aLang [$value ['id']]['modules']['videoalbum']['items'][] = $item;
                                    
                                if (isset ($item [$value ['id']]) && $item [$value ['id']]['name'] != '')
                                {
                                        $aLang [$value ['id']]['modules']['videoalbum']['filled']++;
                                        $aLang [$value ['id']]['filled']++;              
                                }       
                        }
                        
                        $aLang [$value ['id']]['all'] += count ($aWebshop);
                        $aLang [$value ['id']]['modules']['webshop']['all']    = count ($aWebshop);
                        $aLang [$value ['id']]['modules']['webshop']['filled'] = 0;
                        $aLang [$value ['id']]['modules']['webshop']['item']   = $GLOBALS ['cfg']['relativ'] . '/cms/webshop/webshop.php?action=edit&';
                        $aLang [$value ['id']]['modules']['webshop']['cat']    = $GLOBALS ['cfg']['relativ'] . '/cms/webshop/cats.php?action=edit&';
                        
                        foreach ($aWebshop as $item)
                        {
                                $item ['bitem'] = true;
                                $aLang [$value ['id']]['modules']['webshop']['items'][] = $item;
                                
                                if (isset ($item [$value ['id']]) && ($item [$value ['id']]['title'] != '' || $item [$value ['id']]['description'] != ''))
                                {
                                        $aLang [$value ['id']]['modules']['webshop']['filled']++;
                                        $aLang [$value ['id']]['filled']++;              
                                }       
                        }       
                        
                        $aLang [$value ['id']]['all'] += count ($aCatWebshop);
                        $aLang [$value ['id']]['modules']['webshop']['all'] += count ($aCatWebshop);
                        
                        foreach ($aCatWebshop as $item)
                        {
                                $aLang [$value ['id']]['modules']['webshop']['items'][] = $item;
                                
                                if (isset ($item [$value ['id']]) && $item [$value ['id']]['name'] != '')
                                {
                                        $aLang [$value ['id']]['modules']['webshop']['filled']++;
                                        $aLang [$value ['id']]['filled']++;  
                                }       
                        }  
                        
                        $aLang [$value ['id']]['all'] += count ($aPages);
                        $aLang [$value ['id']]['modules']['pages']['all']    = count ($aPages);
                        $aLang [$value ['id']]['modules']['pages']['filled'] = 0;
                        $aLang [$value ['id']]['modules']['pages']['item']   = $GLOBALS ['cfg']['relativ'] . '/cms/pages/pages.php?action=change&';
                        
                        foreach ($aPages as $item)
                        {
                                $item ['bitem'] = true;
                                $aLang [$value ['id']]['modules']['pages']['items'][] = $item;
                                if (isset ($item [$value ['id']]) && ($item [$value ['id']]['name'] != '' || $item [$value ['id']]['content'] != ''))
                                {
                                        $aLang [$value ['id']]['modules']['pages']['filled']++;
                                        $aLang [$value ['id']]['filled']++;              
                                }       
                        }
                        
                }
                $oTemplate -> assign ('languages', $aLang);
                
        }
        elseif ($_GET ['action'] == 'new') 
        {
                if (!empty ($_POST)) 
                {
                        $_POST ['image'] = $_FILES ['image'];
                        
                        if ($oLanguage -> addLanguage ($_POST)) 
                        {
                                redirect ($SiteRoot . '/language/language.php?action=overview');  
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oLanguage -> getLastError ());
                        }
                } 
        } 
        elseif ($_GET ['action'] == 'overview') 
        {
                require_once 'Pager/Pager.php';  
                
                $oTemplate -> assign ('language_overview', true);
                
                $aParams = array (
                        'mode'                  => 'Sliding',
                        'perPage'               => (isset ($_GET ['items']) ? $_GET['items'] : 10), //show 10 items per page 
                        'delta'                 => 3,
                        'altPrev'               => 'Vorige pagina',
                        'altNext'               => 'Volgende pagina',
                        'altPage'               => 'Pagina',
                        'separator'             => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
                        'delta'                 => 2,
                        'clearIfVoid'           => false,
                        'itemData'              => $oLanguage -> getLanguages (0, '', (isset ($_GET ['sort']) ? $_GET ['sort'] : 'id'), (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'ASC'))
                );
                
                $oPager = & Pager::factory ($aParams);
                $aData  =& $oPager -> getPageData ();
                
                $oTemplate -> assign ('links', $oPager -> links);
                $oTemplate -> assign ('sorttype', (isset ($_GET ['sorttype']) ? $_GET ['sorttype'] : 'desc'));
                $oTemplate -> assign ('pageID', (isset ($_GET ['pageID']) ? $_GET ['pageID'] : 1));    
                $oTemplate -> assign ('langs', $aData);
            
        } 
        elseif ($_GET ['action'] == 'edit') 
        {
                $aLanguageById = $oLanguage -> getLanguages ($_GET ['id']);
                $oTemplate -> assign ('languages', $oLanguage -> getLanguages ());
                $oTemplate -> assign ('values', $aLanguageById);
                
                if (!empty ($_POST)) 
                {
                        $_POST ['image'] = $_FILES ['image'];
                        
                        if ($oLanguage -> updateLanguage ($_GET ['id'], $_POST)) 
                        {
                                redirect ($SiteRoot . '/language/language.php?action=overview'); 
                        } 
                        else 
                        {
                                $oTemplate -> assign ('error', $oLanguage -> getLastError ());
                        }
                    
                } 
        } 
        elseif ($_GET ['action'] == 'delete') 
        {
                if ($oLanguage -> deleteLanguage ($_GET ['id'])) 
                {
                        redirect ($SiteRoot . '/language/language.php?action=overview');
                }
        }
        elseif ($_GET ['action'] == 'texts')
        {
                if (isset ($_GET ['id']))
                {
                        $aDefaultLang = $oLanguage -> getLanguages ($_GET ['id'], '', 'id', 'asc');
                        
                        if (!empty ($_POST)) 
                        {
                                $sContent = '<?php
';
                                
                                foreach ($_POST as $k => $v)
                                {
                                        $sContent .= '$texts [\'' . $k . '\'] = "' . $v . '";
';       
                                }
                                
                                $sContent .= '
?>';
                                
                                file_put_contents ('../../lang/' . $aDefaultLang ['short_name'] . '.inc', $sContent);
                        }      
                        
                        include_once ($GLOBALS ['cfg']['include'] . '/lang/' . $aDefaultLang ['short_name'] . '.inc');
                        
                        $oTemplate -> assign ('texts', $texts); 
                }
        }
     
} 
else 
{
        redirect ($SiteRoot . '/language/language.php?action=overview');
}

$oTemplate -> assign ('javascriptBody', array (
                                'jquery.min.js',
                                'cufon-yui.js', 
                                'Calibri_400.font.js', 
                                'title.js',
                                'updateForm.js'
                                )
                            );
                            
$oTemplate -> assign ('folder', $aFolderData);
$oTemplate -> assign ('items', (isset ($_GET ['items']) ? $_GET['items'] : 10));
//$oTemplate -> assign ('status', $_SESSION ['status']);
$oTemplate -> assign ('contentInclude', 'language/language.tpl');

$oTemplate -> display ('default.tpl');
?>
