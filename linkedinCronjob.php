<?php
#!/usr/local/bin/php
include(__DIR__.'/Configuration.php');   
include(__DIR__.'/cms/includes/Database.class.php');
include(__DIR__.'/cms/includes/Language.class.php');
include(__DIR__.'/cms/includes/OAuth/oauth.php');
include(__DIR__.'/cms/includes/Linkedin.class.php');
include(__DIR__.'/cms/includes/linkedin/secret.php');
include(__DIR__.'/cms/includes/linkedin/linkedin.class.php');
include(__DIR__.'/cms/includes/News.class.php');
$oNews      = new News ();
$oLinkedin  = new LinkedinCms ();
$today_date = date("Y-m-d");
$aNews = $oNews ->getNews(true, 0, '', 0, '', 'linkedin', 'asc', 'default', 0, $today_date, false, false, true);

$url = "http://cms.cmsforlife.nl/news/";

/*Check if the connection with linkedin is established*/
$appLinkedinData = $oLinkedin ->getApplication();
$userId = $aNews [0]['linkedin'];
$connectionLinkedin = '';
$nrConnection = 0;
$okLinkedin = false;

foreach ($aNews as $k => $item)
{
        if ($item ['date_start'] == $today_date && $item ['linkedin'] != 0)
        {       
                $ok = false;
                $actual_time = time();
                while ($ok == false)
                {       
                        $okLinkedin = false;
                        if (!empty ($appLinkedinData))
                        {
                                if ($userId != $item ['linkedin'] || $nrConnection == 0)
                                {
                                        $userId = $item ['linkedin'];
                                        $aLinkedinUser = $oLinkedin ->getLinkedinUser($item ['linkedin']);
                                        if (!empty ($aLinkedinUser) && $appLinkedinData ['correct'] == 1)
                                        {
                                                // create connection
                                                $API_CONFIG = array(
                                                        'appKey'       => $appLinkedinData ['consumer_key'],
                                                        'appSecret'    => $appLinkedinData ['consumer_secret'],
                                                        'callbackUrl'  => OAUTH_CALLBACK_LINKEDIN
                                                );

                                                $connectionLinkedin = new Linkedin($API_CONFIG);

                                                $access_token_array = array (
                                                        'oauth_token' => $aLinkedinUser ['oauth_token'],
                                                        'oauth_token_secret' => $aLinkedinUser ['oauth_token_secret']
                                                );
                                                $connectionLinkedin ->setTokenAccess ($access_token_array);
                                                $profile = $connectionLinkedin -> profile ();
                                                if (isset ($profile ['error']) || empty ($profile)) {
                                                        $okLinkedin = false;
                                                }
                                                else 
                                                {
                                                       $okLinkedin = true;
                                                }
                                        }
                                }


                                if ($okLinkedin)
                                {
                                        $newtime = time();
                                        if ($newtime > $actual_time + 1)  
                                        {
                                               $link = $url.$item ['link'];
                                               $content = array ('comment' => substr (strip_tags($item ['description']), 0, 100),
                                                                     'title' => $item ['title'],
                                                                     'submitted-url' => $link,
                                                                     'submitted-image-url' => (isset ($item ['photos'][0]['image_name'])) ? $url.$item['photos'][0]['image_name'] : '',
                                                                     'description' => substr (strip_tags($item ['description']), 0, 100) 
                                                                    );
                                              $connectionLinkedin ->share('new', $content);
                                              $ok = true;
                                        }
                                }
                                else
                                {
                                        $ok = true;
                                }
                        }
                }
        }
}   
?>