<?php
/**
 * 
 * This file is part of the source-code for the website officially associated
 * with the site. 
 * 
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id$
 * phpinfo();die('c');
 */


//echo "<pre>".print_r($_SERVER,true);die();

define ('INFORITUS_VERSION',  crc32 ('$Id$'));
define ('INFORITUS_START',    microtime (true));
define ('INFORITUS_BASE',     __DIR__);

define ('INFORITUS_DEBUG',    true);

ob_start ();

require __DIR__ . '/cms/fixpath.php';
require __DIR__ . '/Sources/Kernel.php';
require __DIR__ . '/Sources/Dispatcher.php';
require __DIR__ . '/Configuration.php';

Inforitus \ Kernel \ Kernel     :: Initialize ();
Inforitus \ Kernel \ Dispatcher :: Dispatch 
(
        $_SERVER ['SERVER_NAME'],       /* Address of the site being requested */
        $_SERVER ['REQUEST_URI'],       /* Path of the page being requested */
        $_GET,                          /* Non-default variables (scope: GET) */
        $_POST,                         /* Non-default variables (scope: POST) */
        true                            /* Output the results (true) or return them */
);

?>