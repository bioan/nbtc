<?php
setlocale (LC_TIME, 'nl_NL');
        
$aRawTwitter = json_decode (@file_get_contents ('http://twitter.com/statuses/user_timeline/56722785.json'), true);
$aTwitter    = array ();

foreach ($aRawTwitter as $aEntry)
{
    // links should be links;
    $sMessage = preg_replace ('/http:\/\/([^\s]+)/s', '<a target="_blank" href="http://$1">http://$1</a>', $aEntry ['text']);

    // hash-tags should be.. hash-tags!
    $sMessage = preg_replace ('/#([^\s\-\,\.]+)/s', '<a target="_blank" href="http://twitter.com/#search?q=%23$1" rel="blank">#$1</a>', $sMessage);

    // usernames and references
    $sMessage = preg_replace ('/@([^\s\-\,\.]+)/s', '<a target="_blank" href="http://twitter.com/$1" rel="blank">@$1</a>', $sMessage);

    $aTwitter [] = array
    (
        'twitter_id'    => $aEntry ['id'],
        'twitter_date'  => strtotime ($aEntry ['created_at']),
        'message'       => $sMessage
    );
    
    if (preg_match ('/\b' . preg_quote ('facebook', '/') . '\b/si', $sMessage) == 1)
            echo $aEntry ['id'] . ' ' . $aEntry ['created_at'] . ' ' . strftime ('%Y-%m-%d %H-%M-%S', strtotime ($aEntry ['created_at'])) . ' <br />' . $sMessage . '<br /> <br />';
    
}
?>