<!doctype html>
<html lang="en">
    <head>
        <title>Cycling</title>
        <meta charset="utf-8" />
            
        <link rel="stylesheet" href="/Visual/Style/style.css" type="text/css" media="screen" />
        <!--[if lt IE 9]>
        <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
        <script src="/Visual/Javascript/jquery.min.js"></script> 
        <script src="/Visual/Javascript/cycling.js"></script> 
        <script src="/Visual/Javascript/ga.js"></script> 
        
    </head>
    <body> 
        
    </body>
</html>