<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id: ModuleBase.php
 */

namespace Inforitus \ Kernel;

/**
 * The ModuleBase class contains a collection of methods which are convenient for modules
 * to implement. The class is defined as an abstract class because it may not be initiated
 * by itself, however, requires a parent class.
 *
 */
abstract class ModuleBase
{
        const   CONTENT_TYPE_HTML       = 1;
        const   CONTENT_TYPE_PDF        = 2;
        const   CONTENT_TYPE_EXCEL      = 3;
        const   CONTENT_TYPE_XML        = 4;
        
        const   LOG_TYPE_MISC           = 0;
        const   LOG_TYPE_LOGIN          = 1;
        const   LOG_TYPE_LANGUAGES      = 2;
        const   LOG_TYPE_USERS          = 3;
        
        /**
         * Every method in the Module should be able to access the request the user is
         * making, which will be easily done by this variable. It contains an object
         * with the active request hierarchy, without the file-type or routing verification.
         *
         * @var Request
         */
        
        protected $request;
        
        /**
         * The file-type of the active request will be contained in this variable. While
         * the dispatch method accepts a string for parsing, internally we'll store this
         * as a CONTENT_TYPE_* define. Documentation of these defines isn't required due
         * to the fairly clear naming (unless you don't know what HTML is of course...).
         * 
         * @var integer
         */
        
        protected $type;
        
        /**
         * We store quite some information about the active user. Among this information
         * is their name, IP address and possibly the special FTP log-in details. Users
         * also got quite an extensive ACL system attached to them, however!
         * 
         * @var User
         */
        
        protected $user;
        
        /**
         * VideoBankOnline spreads it's settings over multiple contextes to ensure security
         * for companies. This means that people in Context A are absolutely unable to
         * access videos and files in Context B. This class arranges the access, which will
         * automatically be combined with the User ACL lists using the can() method.
         *
         * @var Context
         */
        
        protected $context;
        
        /**
         * Database access is something that most modules require. If it's not for video- or
         * file access, general information, settings or logfiles might be convenient to access.
         * This property contains a pointer to the Database class, which will be a simple
         * wrapper around the internal MySQLi classes.
         * 
         * @var Database
         */
        
        protected $db;
        
        /**
         * Easy access to the cache is important for the proper working of VideoBankOnline,
         * which is exactly what this property will maintain. The dispatch-method will set the
         * property to contain the active Caching container.
         *
         * @var Cache
         */
        
        protected $cache;
        
        /**
         * All modules should be able to easily access layout configuration to indicate any
         * changes they would like to make to the layout, supply the content and append
         * additional breadcrumbs which indicate their location.
         *
         * @var Layout
         */
        
        protected $layout;
        
        /**
         * Dispatching the request towards the proper class can be done using this method,
         * it will initialize everything that's required by setting the local class properties
         * to their associated values, passed on to us by using the various arguments. This
         * method supports the actual modular boot-up by supplying all required variables,
         * easy access to the ACL lists, caching systems, context- and user classes.
         *
         * @param string $sRequest The request that's being made by the active visitor.
         * @param User $pUser Instance to the User class which contains log-in information.
         * @param Layout $pLayout The layout instance which will be used for this request.
         * @param Database $pDatabase Instance of the Database connection which will be used.
         * @param Cache $pCache Instance of the caching container used by VideoBankOnline.
         */
        
        public static function dispatch ($sRequest, \ User $pUser, Layout $pLayout,\ Database $pDatabase/*, Cache $pCache*/)
        {
                $sClassName = get_called_class ();
                $pInstance  = new $sClassName;
                
                if ($sRequest instanceof Request)
                {
                        $pInstance -> request = $sRequest;
                }
                else
                {
                        $pInstance -> request = new Request ($sRequest);
                }
                
                $pInstance -> user    = $pUser;
                $pInstance -> db      = $pDatabase;
                //$pInstance -> cache   = $pCache;
                $pInstance -> layout  = $pLayout;
                
                /** copy over for legacy reasons **/
                $pInstance -> type = $pInstance -> request -> type;
                
                $pInstance -> onRequest (); 
        }
        
        /**
         * Re-invoking another class to forward the request can be convenient if you would like
         * to seperate functionalities over different files. This mainly is convenient for the
         * administrative section, as that covers a large range of functions.
         * 
         * @param string $sClassName Name of the class which should be invoked.
         */
        
        public function dispatchTo ($sClassName)
        {                       
                $sClassName :: dispatch ($this -> request, $this -> user, $this -> layout, $this -> db, $this -> cache);  
        }
        
        /**
         * The onRequest event will be called automatically after the entire class has been
         * initialized. This method is required in the module classes considering it acts like
         * some kind of delayed constructor. Usage of PHP's internal constructor (__construct)
         * cannot be adviced, mainly because various properties will not be available.
         */
        
        public abstract function onRequest ();
        
        /**
         * Redirect the user to another URL in a safe and fancy way. In case something goes wrong
         * with the header, show a simple page with a link telling them were to go. Eventually
         * this has to be changed into a nice template in the VideoBankOnline style.
         *
         * @param string $sLocation Location of the file the user has to be sent to.
         */
        
        protected function redirect ($sLocation)
        {
                Header ('Location: http://' . $_SERVER ['SERVER_NAME'] . $sLocation);
                
                echo '<html>',
                     '  <head>',
                     '    <title>Inforitus - Redirect page</title>',
                     '    <script type="text/javascript>',
                     '      document.location.href = "http://' . $_SERVER ['SERVER_NAME'] . $sLocation . '";',
                     '    </script>',
                     '  </head>',
                     '  <body>',
                     '    <a href="/">Click here to proceed to your designated page</a>',
                     '  </body>',
                     '</html>';
                
                session_write_close ();
                
                die ();
        }
        
        /**
         * In order to check whether a user is allowed to execute a certain command, this
         * method may be used. The context and ACL will automatically be invoked unless the
         * class has not been initialized yet, but then you shouldn't be such an ass to
         * use PHP's default constructor. onRequest () is the way to go!
         *
         * @param $sRightIdentifier String representation of the right to check.
         * @return boolean Can the visitor do the requested action, or not?
         */
        
        protected function allowedTo ($sRightIdentifier)
        {
                return $this -> user -> getRights () -> hasRight ($sRightIdentifier);
        }
        
};

?>