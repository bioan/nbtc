<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id: Layout.php 
 */

namespace Inforitus \ Kernel;

require INFORITUS_BASE . '/cms/includes/Smarty.class.php';

/**
 * The layout of the Inforitus website gets controlled using this class, which
 * actually is pretty simular to the Template class, but focusses more on things
 * that all pages have in common (javascript, stylesheets, titles).
 *
 * @package kernel
 */
class Layout
{
        /**
         * This array contains the extra stylesheets which should be loaded for
         * this page. Only layout.css is standard, the rest should be abstract.
         *
         * @var array
         */
        
        private $m_aStyleSheets = array ();
        
        /**
         * This array contains the Javascript files which should be included for this
         * page. Since the larger December '09-update, these files are modular as well
         * to minimize the amount of data that has to be loaded and parsed.
         *
         * @var array
         */
        
        private $m_aJavascripts = array ();
        
        /**
         * The template which contains the layout for the current request. Normally
         * this will be the public front-end website, however, this may change
         * depending on the context. Private Video environments will feature a
         * slightly different layout than the normal Inforitus pages.
         *
         * @var Template
         */
        
        private $m_pLayout;
        
        /**
         * All pages need a title, and rather than specifically specifying what this
         * title should be, we're using a breadcrumb-based system. Breadcrumbs should
         * be added by all major systems on the website.
         *
         * @var string
         */
        
        private $m_aBreadcrumbs;
        
        /**
         * Contains the active index for the menu items. These are used to highlight
         * a certain entry to differentiate it from other entries. Be sure to update
         * this variable using the setMenuIndex method in this class.
         *
         * @var string
         */
        
        private $m_sMenuIndex;
        
        /**
         * Obviously we want to change the page's title, allowing people to get a quick
         * impression of the page they're on. This defaults to the name of our company,
         * however, can be changed using the setTitle method.
         *
         * @var string
         */
        
        private $m_sTitle;
        
    /**
     * The constructor will be responsible for loading the actual layout of the
         * Inforitus website, depending on the active context. We should
         * actually use dependency injection in here, but I'll fix that later on.
     */
    
    public function __construct ()
    {
            $this -> m_pLayout      = new \ Smarty ();
            
            $this -> m_pLayout  ->  template_dir = $_SERVER['DOCUMENT_ROOT'].'/Visual/';
            $this -> m_pLayout  ->  compile_dir = $_SERVER['DOCUMENT_ROOT'].'/Cache/Templates/';
            
            $this -> m_aStyleSheets = array
            (
                    '/Visual/layout.css',
                    '/Visual/Style/style.css',
            );
            
            $this -> m_aJavascripts = array
            (
                    '/Visual/Javascript/jquery.min.js', 
                    '/Visual/Javascript/jquery.rater.js',
                    //'/Visual/Javascript/jquery.raty.min.js', 
                    '/Visual/Javascript/cufon-yui.js',
                    '/Visual/Javascript/Calibri_400-Calibri_700.font.js', 
                    '/Visual/Javascript/default.js',
                    '/Visual/Javascript/forum.js',
                    '/Visual/Javascript/ga.js'
            );
            
            $this -> m_aBreadcrumbs = array
            (
                    array (\ Configuration :: WEBSITE_NAME, '/index.html')
            );
            
            $this -> m_pLayout -> assign ('content', '');
            
            $this -> m_nMenuIndex = 0;
            $this -> m_sTitle = \ Configuration :: WEBSITE_NAME;
    }
        
        /**
         * Changing the title of a certain page can be done using this method, which will
         * tell the Layout class, us, that the template should change. Quite easy actually,
         * just fillin' up this comment because I like having them at three lines.
         *
         * @param string $sTitle Title that should be set for this page.
         * @param boolean $bAddSuffix Add the "Inforitus" suffix?
         */
        
        public function setTitle ($sTitle, $bAddSuffix = true)
        {
                $this -> m_sTitle = $sTitle . ($bAddSuffix === false ?: ' - ' . \ Configuration :: WEBSITE_NAME);
        }
        
        /**
         * This method will be able to add a new stylesheet to the Inforitus
         * site, which is why we support modular templates/stylesheets.
         *
         * @param string $sFilename The filename of the to-be-added stylesheet
         */
        
        public function addStyleSheet ($sFilename)
        {
                $this -> m_aStyleSheets [] = $sFilename;
        }
        
        /**
         * Adding a new javascript file to the output has the advantage that we're able
         * to split up the code in several files, rather than just having one. This means
         * modular script development is possible.
         *
         * @param string $sFilename File that has to be added to the output.
         */
        
        public function addJavascript ($sFilename)
        {
                $this -> m_aJavascripts [] = $sFilename;
        }
        
        /**
         * Assigning a value to the layout can be done using this method. It will
         * directly forward the call to the Template class' method.
         *
         * @param mixed $sKey Either a string for the key or an associative array.
         * @param mixed $mValue Value as associated with the key, or simply null.
         * @return boolean An indication of whether the value could be set.
         */
        
        public function assign ($sKey, $mValue = null)
        {
                $this -> m_pLayout -> assign ($sKey, $mValue);
        }
        
    /**
     * After the whole layout has finished parsing, we can request the output
         * and finalize everything that's not yet done. This method will add all
         * not-yet-set variables, include debugging information if required and
         * eventually return the output.
         *
         * @param User $pUserInfo Information about the user who is logged in.
         * @param Cache $pCache The Cache object used to store company info.
         * @return string The entirely parsed layout, ready for outputting.
     */
    
    public function getOutput (\ User $pUserInfo)
    {
            $aLayoutInformation = array
            (
                    'stylesheets'   => $this -> m_aStyleSheets,
                    'javascripts'   => $this -> m_aJavascripts,
                    'menu_items'    => $this -> generateMenu (),      
                    'title'         => $this -> m_sTitle,
                    'stretched'     => false,
                    'debug'         => false
            );
            
            if (count ($this -> m_aBreadcrumbs) > 1)
            {
                    $aLayoutInformation ['title'] = $this -> m_aBreadcrumbs [count ($this -> m_aBreadcrumbs) - 1][0] . ' - ' . \ Configuration :: WEBSITE_NAME;
            }
            
            
            
            $this -> m_pLayout -> assign ($aLayoutInformation);
        
            return $this -> m_pLayout -> display ('layout.tpl');
             
    }
    
    /**
     * This method will generate the menu which should be included on the website,
     * which either is the public menu, or the menu for logged in people. In both
     * cases a full list of rights is available.
     *
     * @return string A string containing the menu for this request.
     */
    
    private function generateMenu ()
    {
            $pPage    = new \ Page ();
            $aParents = array ();
            $aChilds  = array ();   
            $sMenu    = '';
            
            /* Get the parent items of menu */ 
            $aParents = $pPage -> getAllNodes (1, 0, 'order_number', 'asc', false, LANG);
            $iCount   = 0;
            
            for ($i = 0; $i < count ($aParents); $i++)
            {
                    /* Get the child items of menu */   
                    $aParents [$i]['childs'] = $pPage -> getChilds ($aParents [$i]['oid']);   
                    //$aParents[$i]['selectedpage'] =  $pPage -> getSelectedPage($pDatabase, $aParents[$i], $m_sSelectedMenuItem);        
            }
            //echo '<pre>'.print_r($aParents,true);die('a');
            return $aParents;
                    
    }
        
};

?>