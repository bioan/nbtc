<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id: Dispatcher.php
 */

namespace Inforitus \ Kernel;

/** include public interfaces for basic class-support **/
require __DIR__ . '/Interfaces/IUser.php';

/** include the actual working-classes **/
require __DIR__ . '/ModuleBase.php';
require INFORITUS_BASE. '/cms/includes/Database.class.php';
require INFORITUS_BASE. '/cms/includes/User.class.php';      
require __DIR__ . '/Request.php';
require __DIR__ . '/Layout.php';

/**
 * The Dispatcher class primarily ensures that people won't be able to view pages they're
 * not meant to be seeing. Start-up steps like visitor authentication will be done using
 * this class. Eventually the request will be dispatched to a certain module.
 *
 * @package kernel
 */
class Dispatcher
{
        const   MODULE_ACCESS_PUBLIC    = 0;
        const   MODULE_ACCESS_CLIENTS   = 1;
        const   MODULE_ACCESS_ADMIN     = 2;
        
        /**
         * This array contains an overview of all available modules which can be loaded
         * through the Dispatcher. Modules not listed in here can be loaded through other
         * modules, however, they cannot be reached by default.
         * 
         * @var array
         */
        
        private static $m_saAvailableModules = array
        (
                'Frontpage'             => self :: MODULE_ACCESS_PUBLIC        // The public front-page, for logging in
        );
        
        /**
         * Routing a request to its proper path is very important in making sure that
         * we can see the proper page. All of these aspects help initializing the proper
         * context-, user and rightset deciding what we have access to.
         * 
         * @param string $sServerName Domain name which the request is being made on.
         * @param string $sRequestPath The filename on the server which has been requested.
         * @param array $aScopeGet Information about the GET scope, additional parameters.
         * @param array $aScopePost Information about the POST scope, forms etc.
         * @param boolean $bOutput Output the generated page (true) or return the contents (false).
         * @return mixed Depending on $bOutput, either NULL or a string with the generated page.
         */
        
        public static function Dispatch ($sServerName, $sRequestPath, $aScopeGet, $aScopePost, $bOutput = true)
        {          
                $sFileExtension = substr (strrchr ($_SERVER ['REQUEST_URI'], '.'), 1);
                $aRequestPath   = explode ('/', substr ($sRequestPath, 1, 0 - 1 - strlen ($sFileExtension)));
                  
                $pCache    = Kernel   :: getCacheManager ();   
                $pDatabase = \ Database :: getInstance     ();   
                $pUserInfo = new \ User   ($pDatabase, $pCache);
                $pLayout   = new Layout ();
                  
                /**
                 * Handling the logout method has to be handled prior to any other module,
                 * simply because it overrides most default commands.
                 */
                
                if (isset ($aRequestPath [0]) && $aRequestPath [0] === 'logout')
                {
                        $pUserInfo -> logout ();
                }
                
                /**
                 * The next step is routing to the module of choice. This is dependant on the
                 * request path, but primary on whether we're logged in or not.
                 */
                
                require_once __DIR__ . '/Modules/Router.php';         
                \ Inforitus \ Modules \ Router :: Dispatch ($sRequestPath, $pUserInfo, $pLayout, $pDatabase/*, $pCache*/);
                   
                /**
                 * The final step of this method is to actually get the output, which has two
                 * possibilities. We either output the result, or simply return it.
                 */
                 
                if ($bOutput === true)
                {           
                        Header ('Content-Type: text/html; charset=utf-8');
                        die ($pLayout -> getOutput ($pUserInfo, $pCache));
                }
                 
                return $pLayout -> getOutput ($pUserInfo, $pCache);
        }
        
        /**
         * Sanitizing a module name can be useful for various reasons. Most importantly
         * it will allow us to locate a certain module based on the request name, but
         * it will also enable the system to quickly look something up.
         */
        
        private static function sanitizeModuleName ($sModuleName)
        {
                return str_replace (' ', '', ucwords (str_replace ('-', ' ', $sModuleName)));
        }
};

?>