<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id: IUser.php
 */

namespace Inforitus;
 
interface IUser
{
        /**
         * A single method, this one, is important for logging in a user when they desire
         * to do so. All data initialization, scope-preparation and right-deciding code
         * will reside in this method, or a method called from this one.
         *
         * @param string $sUsername Username that will be used for logging in.
         * @param string $sPassword And the associated password for the identification.
         * @return boolean A boolean saying whether we were able to identify ourselfes.
         */
        
        public function identify ($sUsername, $sPassword);
        
        /**
         * Checking whether a user has identified can be convenient for various
         * reasons, mostly to check which pages we're allowed to access.
         *
         * @return boolean Are we logged in (true) or not (false)?
         */
        
        public function isIdentified ();
        
        /**
         * Which types of modules are we allowed to access? This is dependant
         * on the settings associated with this user, which will be defined by the
         * site-dependant implementation. This method has to be included however.
         *
         * @return boolean Is the logged-in user allowed to access this?
         */
        
        public function canAccessModuleType ();
        
        /**
         * Log out the logged in user to the logged in user is able to log in again
         * if they like. Of course we only process the log-less log out! All cookies,
         * sessions and associated information will be removed.
         */
        
        public function logout ();
        
        /**
         * Getting information from the active user session can be done by accessing
         * the property name as a.. property. We do this by overloading __Get.
         *
         * @param string $sKey Key of the setting you wish to retreive.
         * @return mixed Value associated to $sKey, or false if it isn't set.
         */
        
        public function __get ($sKey);

};

?>