<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id$
 */

namespace Inforitus \ Modules;
use \ Inforitus \ Kernel \ Template; 
        
class Start extends \ Inforitus \ Modules \ Router
{
        
        /**
         * The method which will be invoked when a request gets triggered for this class is,
         * how did you guess, onRequest. The dispatcher calls it after initializing some of
         * the required properties so we'll be able to run properly.
         */
        
        public function onRequest ()
        {       

            require INFORITUS_BASE . '/cms/includes/Cyclist.class.php';    
            $ciclists = new \ Cyclist();

            if (isset ($_POST['cname']))
            {
                $sName    = (trim($_POST ['cname']) == '' ? false : filter_var ($_POST ['cname'], FILTER_SANITIZE_SPECIAL_CHARS));
                $sEmail   = ($_POST ['cemail'] == '' ? false : filter_var ($_POST ['cemail'], FILTER_VALIDATE_EMAIL));

                if ($sName !== false && $sEmail !== false){
                    if ($ciclists -> isSomeonePlaying()){
                        $this -> layout -> assign ('error_text', 'Someone is playing now.. please come back later');
                    }
                    else{
                        $ciclists -> insertNewPlayer($_POST['cname'], $_POST['cemail']);
                        header ('Location: http://' . $_SERVER ['SERVER_NAME'] .'/cycling.html');
                    }
                }   
                elseif ($sName === false)
                {
                    $this -> layout -> assign ('error_text', 'Please enter a name');
                }
                else{
                    $this -> layout -> assign ('error_text', 'Please enter a valid email address');
                }
            }
            

            $this -> layout -> assign ('content', 'Templates/start-mobile.tpl');

            $oPage       = new \ Page();
            $aParent     = $oPage -> getNodeData (0, 'start');

            $this -> layout -> assign ('page_name', 'start'); 
            $this -> layout -> assign ('type', 0); 
            $this -> layout -> assign ('page', $aParent);
            $this -> layout -> assign ('page_title', $aParent ['name']);       
                       
        }
        
};

?>