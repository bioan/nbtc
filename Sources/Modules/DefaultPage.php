<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id$
 */

namespace Inforitus \ Modules;
use \ Inforitus \ Kernel \ Template; 
        
class M_DefaultPage extends \ Inforitus \ Modules \ Router
{
        /**
         * The method which will be invoked when a request gets triggered for this class is,
         * how did you guess, onRequest. The dispatcher calls it after initializing some of
         * the required properties so we'll be able to run properly.
         */
        
        public function onRequest ()
        {         
                require INFORITUS_BASE . '/cms/includes/PhotoAlbum.class.php';    
                
                $oPhotoAlbum = new \ PhotoAlbum ();    
                $oPage = new \ Page();
                
                $aParent = $oPage -> getNodeData (0, $this -> request [0], LANG);
                
                $this -> layout -> assign ('page', $aParent); 
                $this -> layout -> assign ('page_name', $this -> request [0]);
                $this -> layout -> assign ('type', 1); 
                $this -> layout -> assign ('page_description', (!empty ($aParent ['page_description']) ? $aParent ['page_description'] : $this -> sIndexDescription));
                $this -> layout -> assign ('page_title', $aParent ['name']);
                $this -> layout -> assign ('content', 'Templates/default.tpl'); 
                       
        }
}