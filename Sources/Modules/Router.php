<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id$
 */

namespace Inforitus \ Modules;
use \ Inforitus \ Kernel \ Template;
require INFORITUS_BASE . '/cms/includes/Tree.class.php';
require INFORITUS_BASE . '/cms/includes/Page.class.php';
require INFORITUS_BASE . '/cms/includes/Language.class.php';
require INFORITUS_BASE . '/cms/includes/Functions.php';

class Router extends \Inforitus\Kernel\ModuleBase
{
        /**
         * The variable is the default page description that will appear in case  
         * the specific one it is not set.
         */
         
        public $sIndexDescription = '';
        
        /**
         * The variable is the default page description that will appear in case  
         * the specific one it is not set.
         */
         
        public $aLanguages = '';
        
        /**
         * The variable is the default page description that will appear in case  
         * the specific one it is not set.
         */
         
        public $sDefaultLang = '';
        
        /**
         * Constructor
         */
        
        public function Router () 
        {
                $oLanguage = new \ Language ();
                $pPage     = new \ Page();
                
                $aIndexPage = $pPage -> getNodeData (0, 'index');
                
                $this -> sIndexDescription = $aIndexPage ['page_description'];
                $this -> aLanguages = $oLanguage -> getLanguages (0, '', 'id', 'asc', false, false);
                $this -> aDefaultLang = (isset ($this -> request [0]) && !in_array ($this -> request [0], $aLangNames) || !isset ($this -> request [0]) ? $oLanguage -> getLanguages (0, '', 'id', 'asc', true) : $oLanguage -> getLanguages (0, $this -> request [0]));
                $this -> sDefaultLang = $this -> aDefaultLang ['short_name'];
        }
        
        /**
         * The method which will be invoked when a request gets triggered for
         * this class is, how did you guess, onRequest. The dispatcher calls it
         * after initializing some of the required properties so we'll be able
         * to run properly.
         */
        
        public function onRequest ()
        {
                $pPage     = new \ Page();
                $oLanguage = new \ Language ();
                
                $aLanguages = $oLanguage -> getLanguages (0, '', 'id', 'asc', false, false);
                
                $aLangNames = array ();
                
                foreach ($aLanguages as $k => $lang)
                {
                        $aImages = glob ('images/lang/' . $lang ['id'] . '*');  
                        
                        if (isset ($aImages [0]))
                        {
                                $aLanguages [$k]['image'] = $aImages [0];   
                        }
                        else
                        {
                                $aLanguages [$k]['image'] = '';
                        } 
                        
                        $aLangNames [] = $lang ['short_name'];
                }
                
                $this -> aDefaultLang = (isset ($this -> request [0]) && !in_array ($this -> request [0], $aLangNames) || !isset ($this -> request [0]) ? $oLanguage -> getLanguages (0, '', 'id', 'asc', true) : $oLanguage -> getLanguages (0, $this -> request [0]));
                $this -> sDefaultLang = $this -> aDefaultLang ['short_name'];
                
                define ('LANG',     $this -> sDefaultLang);
                define ('LANG_ID',     $this -> aDefaultLang ['id']);
                
                $aImages = glob ('images/lang/' . $this -> aDefaultLang ['id'] . '*');  
                        
                if (isset ($aImages [0]))
                {
                        $this -> aDefaultLang ['image'] = $aImages [0];   
                }
                else
                {
                        $this -> aDefaultLang ['image'] = '';
                }
                
                include 'lang/' . $this ->sDefaultLang . '.inc';
                
                $this -> layout -> assign ('texts', $texts);
                $this -> layout -> assign ('website_name', \ Configuration :: WEBSITE_NAME);
                $this -> layout -> assign ('languages', $aLanguages);
                $this -> layout -> assign ('default_language', $this -> aDefaultLang);
                
                $aIndexPage = $pPage -> getNodeData (0, 'index');
                
                $this -> layout -> assign ('page_description', $aIndexPage ['page_description']);  
                $this -> sIndexDescription = $aIndexPage ['page_description'];
                
                if (count ($aLanguages) == 1)
                {
                        $sAction = $this -> request [0] ? $this -> request [0] : 'index';   
                }
                else
                {
                        $sAction = ($this -> request [0] && is_numeric ($this -> request [0]) ? $this -> request [0] : ($this -> request [1] && is_numeric ($this -> request [1]) ? $this -> request [1] : 0));
                }

                require_once INFORITUS_BASE . '/Mobile_Detect.php';
                $detect = new  \ Mobile_Detect();
                
                if (count ($aLanguages) == 1)
                {
                    //Any mobile device (phones or tablets).
                    if ($detect->isMobile()) {
                        $this -> layout -> assign ('device_type', 'mobile');
                        switch ($sAction)
                        {
                                case 'index':case 'welkom': 
                                {

                                        require __DIR__ . '/Frontpage.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\Frontpage');                               
                                        break;
                                }

                                case 'start':
                                {          
                                        require __DIR__ . '/Start.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\Start');
                                         
                                        break;
                                }

                                case 'highscores':
                                {          
                                        require __DIR__ . '/Highscores.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\Highscores');
                                         
                                        break;
                                }

                                case 'cycling':
                                {          
                                        require __DIR__ . '/Cycling.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\Cycling');
                                         
                                        break;
                                }
                        }
                    }
                    else{//on pc
                        $this -> layout -> assign ('device_type', 'pc');

                        switch ($sAction)
                        {
                                case 'index':case 'welkom': 
                                {

                                        require __DIR__ . '/FrontpagePC.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\Frontpage_Pc');
                                        break;
                                }
                        }
                    }
                }
                else
                {
                        switch ($sAction)
                        {
                                case 0: 
                                {
                                        require __DIR__ . '/Frontpage.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\Frontpage');
                                                                                           
                                        break;
                                }
                                
                                case 1: 
                                {
                                        require __DIR__ . '/Agenda.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\M_Agenda');
                                        
                                        break;
                                }
                                
                                case 2: 
                                {
                                        require __DIR__ . '/News.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\M_News');
                                         
                                        break;
                                }
                                
                                case 3: 
                                {
                                        require __DIR__ . '/Photoalbum.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\M_Photoalbum');
                                        
                                        break;
                                }
                                
                                case 4: 
                                {
                                        require __DIR__ . '/Videoalbum.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\M_Videoalbum');
                                        
                                        break;
                                }
                                
                                case 5: 
                                {
                                        require __DIR__ . '/Webshop.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\M_Webshop');
                                        
                                        break;
                                }
                                
                                case 6: 
                                {
                                        require __DIR__ . '/Contact.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\M_Contact');
                                        
                                        break;
                                }
                                
                                case 7:
                                {
                                        require __DIR__ . '/DefaultPage.php'; 
                                        $this -> dispatchTo ('\\Inforitus\\Modules\\M_DefaultPage');
                                        
                                        break;
                                }
                        }
                }
        }
};

?>