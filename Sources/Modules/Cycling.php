<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id$
 */

namespace Inforitus \ Modules;
use \ Inforitus \ Kernel \ Template; 
        
class Cycling extends \ Inforitus \ Modules \ Router
{
	 	
	 	/**
         * The method which will be invoked when a request gets triggered for this class is,
         * how did you guess, onRequest. The dispatcher calls it after initializing some of
         * the required properties so we'll be able to run properly.
         */

       	public function onRequest ()
    	{ 

                $oPage       = new \ Page();
                $aParent     = $oPage -> getNodeData (0, 'cycling');
                
                require INFORITUS_BASE . '/cms/includes/Cyclist.class.php';    
                $ciclists = new \ Cyclist();

                // Any mobile device (phones or tablets).


                $this -> layout -> assign ('content', 'Templates/cycling-mobile.tpl');

                $this -> layout -> assign ('page_name', 'cycling'); 
                $this -> layout -> assign ('type', 0); 
                $this -> layout -> assign ('page', $aParent);
                $this -> layout -> assign ('page_title', $aParent ['name']); 

        }
}
?>