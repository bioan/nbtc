<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id$
 */

namespace Inforitus \ Modules;
use \ Inforitus \ Kernel \ Template;

class Router extends \Inforitus\Kernel\ModuleBase
{
        /**
         * The method which will be invoked when a request gets triggered for
         * this class is, how did you guess, onRequest. The dispatcher calls it
         * after initializing some of the required properties so we'll be able
         * to run properly.
         */
        
        public function onRequest ()
        {
                $sPage = $this -> request [0] ? $this -> request [0] : 'index';
                
                switch ($sPage)
                {
                        
                        case 'inschrijfpagina':
                        {        
                                require __DIR__ . '/Register.php';       
                                $this -> dispatchTo ('\\Inforitus\\Modules\\Register');
                                                                                   
                                break;
                        }
                    
                        default:
                        {                 
                                require __DIR__ . '/Frontpage.php'; 
                                $this -> dispatchTo ('\\Inforitus\\Modules\\Frontpage');
                                         
                                break;
                        }
                }
        }
};

?>