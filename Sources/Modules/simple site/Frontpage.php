<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id$
 */

namespace Inforitus \ Modules;
use \ Inforitus \ Kernel \ Template;

require INFORITUS_BASE . '/cms/includes/Page.class.php';
//require INFORITUS_BASE . '/cms/includes/Language.class.php';
require INFORITUS_BASE . '/cms/includes/Functions.php'; 
        
class Frontpage extends \ Inforitus \ Kernel \ ModuleBase
{
        
        /**
         * The variable is the default page description that will appear in case  
         * the specific one it is not set.
         */
         
        var $sIndexDescription = '';
        
        /**
         * The method which will be invoked when a request gets triggered for this class is,
         * how did you guess, onRequest. The dispatcher calls it after initializing some of
         * the required properties so we'll be able to run properly.
         */
        
        public function onRequest ()
        {         
                $pPage     = new \ Page();
                $oLanguage = new \ Language ();
                
                $aLanguages = $oLanguage -> getLanguages (0, '', 'id', 'asc', false, false);
                
                if (count ($aLanguages) == 1)
                {
                        $sAction = $this -> request [0] ? $this -> request [0] : 'index';   
                }
                else
                {
                        $sAction = ($this -> request [0] && is_numeric ($this -> request [0]) ? $this -> request [0] : ($this -> request [1] && is_numeric ($this -> request [1]) ? $this -> request [1] : 0));
                }
                
                $sUri = $_SERVER ['REQUEST_URI'];
                
                if (strpos ($_SERVER ['REQUEST_URI'], '?') !== false)
                {
                        list ($sUri) = explode ('?', $_SERVER ['REQUEST_URI']);
                }
                
                $aRequest = explode ('/', substr (str_replace (array ('.html'), '', $sUri), 1));
                
                if ($aRequest [0] == 'file' || $aRequest [0] == 'foto')
                {
                        $_GET [$aRequest [0]] = $_GET ['name'];
                        include 'file.php';

                        die ();
                }
                
                $aLangNames = array ();
                
                foreach ($aLanguages as $k => $lang)
                {
                        $aImages = glob ('images/lang/' . $lang ['id'] . '*');  
                        
                        if (isset ($aImages [0]))
                        {
                                $aLanguages [$k]['image'] = $aImages [0];   
                        }
                        else
                        {
                                $aLanguages [$k]['image'] = '';
                        } 
                        
                        $aLangNames [] = $lang ['short_name'];
                }
                
                $this -> aDefaultLang = (isset ($this -> request [0]) && !in_array ($this -> request [0], $aLangNames) || !isset ($this -> request [0]) ? $oLanguage -> getLanguages (0, '', 'id', 'asc', true) : $oLanguage -> getLanguages (0, $this -> request [0]));
                $this -> sDefaultLang = $this -> aDefaultLang ['short_name'];
                
                $aImages = glob ('images/lang/' . $this -> aDefaultLang ['id'] . '*');  
                        
                if (isset ($aImages [0]))
                {
                        $this -> aDefaultLang ['image'] = $aImages [0];   
                }
                else
                {
                        $this -> aDefaultLang ['image'] = '';
                }
                
                include 'lang/' . $this ->sDefaultLang . '.inc';
                
                $this -> layout -> assign ('texts', $texts);
                $this -> layout -> assign ('website_name', \ Configuration :: WEBSITE_NAME);
                $this -> layout -> assign ('languages', $aLanguages);
                $this -> layout -> assign ('default_language', $this -> aDefaultLang);
                
                $aIndexPage = $pPage -> getNodeData (0, 'index');
                
                $this -> layout -> assign ('page_description', $aIndexPage ['page_description']);  
                $this -> sIndexDescription = $aIndexPage ['page_description'];
                
                if (count ($aLanguages) == 1)
                {
                        switch ($sAction)
                        {
                                case 'index':case 'welkom': 
                                {
                                        $this -> onRequestIndex ();  
                                        break;
                                }
                                
                                case 'agenda':
                                {          
                                        $this -> onRequestAgendaPage ();
                                        break ;
                                }
                                
                                case 'news':
                                {          
                                        $this -> onRequestNewsPage ();
                                        break ;
                                }
                                
                                case 'photoalbum':
                                {          
                                        $this -> onRequestPhotoAlbumPage ();
                                        break ;
                                }
                                
                                case 'videoalbum':
                                {          
                                        $this -> onRequestVideoAlbumPage ();
                                        break ;
                                }
                                
                                case 'webshop':
                                {          
                                        $this -> onRequestWebshopPage ();
                                        break ;
                                }
                                
                                case 'contact':
                                {
                                        $this -> onRequestContact ();
                                        break;
                                }
                                
                                default:
                                {
                                        if ($this -> onRequestDefault ($sAction) === false)
                                        {
                                                Header ('HTTP/1.1 301 Moved Permanently');
                                                Header ('Location: http://' . $_SERVER  ['SERVER_NAME'] . '/index.html');
                                                
                                                die ();
                                        }
                                        
                                        break ;
                                }
                        }
                }
                else
                {
                        switch ($sAction)
                        {
                                case 0: 
                                {
                                        $this -> onRequestIndex ();  
                                        break;
                                }
                                
                                case 1: 
                                {
                                        $this -> onRequestAgendaPage ();  
                                        break;
                                }
                                
                                case 2: 
                                {
                                        $this -> onRequestNewsPage ();  
                                        break;
                                }
                                
                                case 3: 
                                {
                                        $this -> onRequestPhotoAlbumPage ();  
                                        break;
                                }
                                
                                case 4: 
                                {
                                        $this -> onRequestVideoAlbumPage ();  
                                        break;
                                }
                                
                                case 5: 
                                {
                                        $this -> onRequestWebshopPage ();  
                                        break;
                                }
                                
                                case 6: 
                                {
                                        $this -> onRequestContact ();  
                                        break;
                                }
                                
                                case 7:
                                {
                                        if ($this -> onRequestDefault ($sAction) === false)
                                        {
                                                Header ('HTTP/1.1 301 Moved Permanently');
                                                Header ('Location: http://' . $_SERVER  ['SERVER_NAME'] . '/index.html');
                                                
                                                die ();
                                        }
                                        
                                        break ;
                                }
                        }
                }
        }
        
        /**
        * @desc index page
        */
        
        private function onRequestIndex ()
        {                         
                require INFORITUS_BASE . '/cms/includes/News.class.php';
                require INFORITUS_BASE . '/cms/includes/PhotoAlbum.class.php';    
                    
                $oPage       = new \ Page();
                $oNews       = new \ News();
                $oPhotoalbum = new \ PhotoAlbum ();
                
                $aPhotoalbum = $oPhotoalbum -> getPhotos (0, '', 0, 'home', 'order_number', 'desc', $this -> sDefaultLang);
                $aSlideshow  = $oPhotoalbum -> getPhotos (0, '', 0, 'slideshow', 'order_number', 'desc', $this -> sDefaultLang);
                $aParent     = $oPage -> getNodeData (0, 'index');
                
                foreach ($aPhotoalbum as $k => $v)
                {
                        $aPhotoalbum [$k]['short_desc'] = substr (strip_tags ($aPhotoalbum [$k]['description']), 0, 100);
                        
                        $aImages = glob (substr ($aPhotoalbum [$k]['photos'][0]['pathphoto'], 1) . '/' . $aPhotoalbum [$k]['photos'][0]['id'] . '_240x73*');  
                    
                        if (isset ($aImages [0]))
                        {
                                $aPhotoalbum [$k]['small_image'] = $aImages [0];   
                        }
                        else
                        {
                                $aPhotoalbum [$k]['small_image'] = '';    
                        }
                }
                
                foreach ($aSlideshow as $k => $item)
                {
                        $aImages = glob (substr ($item ['photos'][0]['pathphoto'], 1) . '/' . $item ['photos'][0]['id'] . '_1000x310*');  
                        
                        if (isset($aImages [0]))
                        {
                                $aSlideshow [$k]['image'] = $aImages [0];   
                        }
                        else
                        {
                                $aSlideshow [$k]['image'] = '';
                        }       
                }
                
                $this -> layout -> assign ('page_name', 'index'); 
                $this -> layout -> assign ('type', 0); 
                $this -> layout -> assign ('page', $aParent);
                $this -> layout -> assign ('campaigns', $aPhotoalbum);
                $this -> layout -> assign ('slideshow', $aSlideshow);
                $this -> layout -> assign ('page_title', $aParent ['name']);       
                $this -> layout -> assign ('content', 'Templates/index.tpl');       
        }
        
        /**
        * @desc Agenda page
        */
        
        private function onRequestAgendaPage ()
        {                         
                require INFORITUS_BASE . '/cms/includes/Agenda.class.php';  
                       
                $pPage   = new \ Page (); 
                $pAgenda = new \ Agenda ();                           
                
                $aAgendas = $pAgenda -> getAgenda ();
                
                if (!isset ($this -> request [1]))
                {
                        $aCurrentAgenda                 = current ($aAgendas);
                        $aCurrentAgenda ['description'] = str_replace (' [meer]', '', $aCurrentAgenda ['description']);
                }
                else
                {                                                             
                        $aCurrentAgenda                 = current ($pAgenda -> getAgenda (0, $this -> request [1])); 
                        $aCurrentAgenda ['description'] = str_replace (' [meer]', '', $aCurrentAgenda ['description']);
                }
                
                $aImages = glob (substr ($aCurrentAgenda ['photos'][0]['pathphoto'], 1) . '/' . $aCurrentAgenda ['photos'][0]['id'] . '_456*');  
                    
                if (isset ($aImages [0]))
                {
                        $aCurrentAgenda ['image'] = $aImages [0];   
                }
                else
                {
                        $aCurrentAgenda ['image'] = '';
                }
                
                $this -> layout -> assign ('agendas', $aAgendas);
                $this -> layout -> assign ('current_agenda', $aCurrentAgenda);
                $this -> layout -> assign ('page_name', 'agenda');  
                $this -> layout -> assign ('page_description', (!empty ($aCurrentAgenda ['page_description']) ? $aCurrentAgenda ['page_description'] : $this -> sIndexDescription));
                $this -> layout -> assign ('page_title', $aCurrentAgenda ['title']);
                $this -> layout -> assign ('content', 'Templates/agenda.tpl');  
        }  
        
        /**
        * @desc News page
        */                                                      
        
        private function onRequestNewsPage ()
        {                         
                require INFORITUS_BASE . '/cms/includes/News.class.php';  
                       
                $pPage = new \ Page (); 
                $pNews = new \ News ();                           
                
                $aNews = $pNews -> getNews (false);
                
                foreach ($aNews as $k => $item)
                {
                        $aImages = glob (substr ($item ['photos'][0]['pathphoto'], 1) . '/' . $item ['photos'][0]['id'] . '_93x70*');  
                        
                        if (isset($aImages [0]))
                        {
                                $aNews [$k]['image'] = $aImages [0];   
                        }
                        else
                        {
                                $aNews [$k]['image'] = '';
                        }
                }
                
                if (!isset ($this -> request [1]))
                {
                        $aCurrentNews                 = current ($aNews);
                        $aCurrentNews ['description'] = str_replace (' [meer]', '', $aCurrentNews ['description']);
                }
                else
                {                                                             
                        $aCurrentNews                 = $pNews -> getNews (false, 0, $this -> request [1]); 
                        $aCurrentNews ['description'] = str_replace (' [meer]', '', $aCurrentNews ['description']);
                }
                
                if (isset ($aCurrentNews ['photos'][0]['pathphoto']))
                {
                        $aImages = glob (substr ($aCurrentNews ['photos'][0]['pathphoto'], 1) . '/' . $aCurrentNews ['photos'][0]['id'] . '_456*');  
                }
                
                if (isset ($aImages [0]))
                {
                        $aCurrentNews ['image'] = $aImages [0];   
                }
                else
                {
                        $aCurrentNews ['image'] = '';
                }
                
                $this -> layout -> assign ('news', $aNews);
                $this -> layout -> assign ('current_news', $aCurrentNews);
                $this -> layout -> assign ('page_name', 'news');   
                $this -> layout -> assign ('page_description', (!empty ($aCurrentNews ['page_description']) ? $aCurrentNews ['page_description'] : $this -> sIndexDescription));
                $this -> layout -> assign ('page_title', $aCurrentNews ['title']);
                $this -> layout -> assign ('content', 'Templates/news.tpl');       
        }
        
        /**
        * @desc PhotoAlbum page
        */                                                      
        
        private function onRequestPhotoAlbumPage ()
        {                         
                require INFORITUS_BASE . '/cms/includes/PhotoAlbum.class.php';  
                       
                $pPage       = new \ Page (); 
                $pPhotoAlbum = new \ PhotoAlbum ();                           
                
                $aPhotoAlbums = $pPhotoAlbum -> getPhotos ();
                
                foreach ($aPhotoAlbums as $k => $item)
                {
                        $aImages = glob (substr ($item ['photos'][0]['pathphoto'], 1) . '/' . $item ['photos'][0]['id'] . '_93x70*');  
                        
                        if (isset($aImages [0]))
                        {
                                $aPhotoAlbums [$k]['image'] = $aImages [0];   
                        }
                        else
                        {
                                $aPhotoAlbums [$k]['image'] = '';
                        }
                }
                
                if (!isset ($this -> request [1]))
                {
                        $aCurrentPhotoAlbum                 = current ($aPhotoAlbums);
                        $aCurrentPhotoAlbum ['description'] = str_replace (' [meer]', '', $aCurrentPhotoAlbum ['description']);
                }
                else
                {                                                             
                        $aCurrentPhotoAlbum                 = current ($pPhotoAlbum -> getPhotos (0, $this -> request [1])); 
                        $aCurrentPhotoAlbum ['description'] = str_replace (' [meer]', '', $aCurrentPhotoAlbum ['description']);
                }
                
                $aImages = glob (substr ($aCurrentPhotoAlbum ['photos'][0]['pathphoto'], 1) . '/' . $aCurrentPhotoAlbum ['photos'][0]['id'] . '_456*');  
                
                if (isset ($aImages [0]))
                {
                        $aCurrentPhotoAlbum ['image'] = $aImages [0];   
                }
                else
                {
                        $aCurrentPhotoAlbum ['image'] = '';    
                }
                
                $this -> layout -> assign ('photoalbums', $aPhotoAlbums);
                $this -> layout -> assign ('current', $aCurrentPhotoAlbum);
                $this -> layout -> assign ('page_name', 'photoalbum');   
                $this -> layout -> assign ('page_description', (!empty ($aCurrentPhotoAlbum ['page_description']) ? $aCurrentPhotoAlbum ['page_description'] : $this -> sIndexDescription));
                $this -> layout -> assign ('page_title', $aCurrentPhotoAlbum ['title']);
                $this -> layout -> assign ('content', 'Templates/photoalbum.tpl');       
        }
        
        /**
        * @desc VideoAlbum page
        */                                                      
        
        private function onRequestVideoAlbumPage ()
        {                         
                require INFORITUS_BASE . '/cms/includes/VideoAlbum.class.php';  
                       
                $pPage       = new \ Page (); 
                $pVideoAlbum = new \ VideoAlbum ();                           
                
                $aVideoAlbums = $pVideoAlbum -> getVideos ();
                
                foreach ($aVideoAlbums as $k => $item)
                {
                        $aImages = glob (substr ($item ['photos'][0]['pathphoto'], 1) . '/' . $item ['photos'][0]['id'] . '_93x70*');  
                        
                        if (isset($aImages [0]))
                        {
                                $aVideoAlbums [$k]['image'] = $aImages [0];   
                        }
                        else
                        {
                                $aVideoAlbums [$k]['image'] = '';
                        }
                }
                
                if (!isset ($this -> request [1]))
                {
                        $aCurrentVideoAlbum                 = current ($aVideoAlbums);
                        $aCurrentVideoAlbum ['description'] = str_replace (' [meer]', '', $aCurrentVideoAlbum ['description']);
                }
                else
                {                                                             
                        $aCurrentVideoAlbum                 = current ($pVideoAlbum -> getVideos (0, $this -> request [1])); 
                        $aCurrentVideoAlbum ['description'] = str_replace (' [meer]', '', $aCurrentVideoAlbum ['description']);
                }
                
                $aImages = glob (substr ($aCurrentVideoAlbum ['photos'][0]['pathphoto'], 1) . '/' . $aCurrentVideoAlbum ['photos'][0]['id'] . '_456*');  
                    
                if (isset ($aImages [0]))
                {
                        $aCurrentVideoAlbum ['image'] = $aImages [0];   
                }
                else
                {
                        $aCurrentVideoAlbum ['image'] = '';    
                }
                
                $this -> layout -> assign ('videoalbums', $aVideoAlbums);
                $this -> layout -> assign ('current_videoalbum', $aCurrentVideoAlbum);
                $this -> layout -> assign ('page_name', 'videoalbum');   
                $this -> layout -> assign ('page_description', (!empty ($aCurrentVideoAlbum ['page_description']) ? $aCurrentVideoAlbum ['page_description'] : $this -> sIndexDescription));
                $this -> layout -> assign ('page_title', $aCurrentVideoAlbum ['title']);
                $this -> layout -> assign ('content', 'Templates/news.tpl');       
        }
        
        /**
        * @desc Webshop page
        */                                                      
        
        private function onRequestWebshopPage ()
        {                         
                require INFORITUS_BASE . '/cms/includes/Webshop.class.php';  
                       
                $pPage     = new \ Page (); 
                $pWebshop  = new \ Webshop ();                           
                
                $aProducts = $pWebshop -> getProducts ();
                
                foreach ($aProducts as $k => $item)
                {
                        $aImages = glob (substr ($item ['photos'][0]['pathphoto'], 1) . '/' . $item ['photos'][0]['id'] . '_93x70*');  
                        
                        if (isset($aImages [0]))
                        {
                                $aProducts [$k]['image'] = $aImages [0];   
                        }
                        else
                        {
                                $aProducts [$k]['image'] = '';
                        }
                }
                
                if (!isset ($this -> request [1]))
                {
                        $aCurrentProduct                 = current ($aProducts);
                        $aCurrentProduct ['description'] = str_replace (' [meer]', '', $aCurrentProduct ['description']);
                }
                else
                {                                                             
                        $aCurrentProduct                 = current ($pWebshop -> getProducts (0, $this -> request [1])); 
                        $aCurrentProduct ['description'] = str_replace (' [meer]', '', $aCurrentProduct ['description']);
                }
                
                $aImages = glob (substr ($aCurrentProduct ['photos'][0]['pathphoto'], 1) . '/' . $aCurrentProduct ['photos'][0]['id'] . '_456*');  
                    
                if (isset ($aImages [0]))
                {
                        $aCurrentProduct ['image'] = $aImages [0];   
                }
                else
                {
                        $aCurrentProduct ['image'] = '';    
                }
                
                $this -> layout -> assign ('products', $aProducts);
                $this -> layout -> assign ('current_product', $aCurrentProduct);
                $this -> layout -> assign ('page_name', 'webshop');   
                $this -> layout -> assign ('page_description', (!empty ($aCurrentProduct ['page_description']) ? $aCurrentProduct ['page_description'] : $this -> sIndexDescription));
                $this -> layout -> assign ('page_title', $aCurrentProduct ['title']);
                $this -> layout -> assign ('content', 'Templates/webshop.tpl');       
        }
        
        /**
        * @desc default page
        */
        
        private function onRequestDefault ()
        {                         
                require INFORITUS_BASE . '/cms/includes/PhotoAlbum.class.php';    
                
                $oPhotoAlbum = new \ PhotoAlbum ();    
                $oPage = new \ Page();
                
                $aParent = $oPage -> getNodeData (0, $this -> request[2], $this -> sDefaultLang);
                $aCampaigns = $oPhotoAlbum -> getPhotos (0, '', 0, $this -> request [2], 'order_number', 'desc', $this -> sDefaultLang);
                
                foreach ($aCampaigns as $k => $item)
                {
                        
                        $aCampaigns [$k]['short_desc'] = str_replace ('[number]', '&euro; ' . $aCampaigns [$k]['price'], substr (strip_tags ($aCampaigns [$k]['description']), 0, 100));
                        
                        if ($k == 0)
                        {
                                $aContent = explode ('&euro; ' . $aCampaigns [$k]['price'], $aCampaigns [$k]['short_desc']); 
                                $aCampaigns [$k]['short_desc'] = '<span class="cufon">' . $aContent [0] . '</span><span class="calibri">&euro; ' . $aCampaigns [$k]['price'] . '</span> <span class="cufon">' . $aContent [1] . '</span>';      
                                //print_r($aContent);die('a');
                        }
                        
                        $aImages = glob (substr ($aCampaigns [$k]['photos'][0]['pathphoto'], 1) . '/' . $aCampaigns [$k]['photos'][0]['id'] . '_280x190*');  
                    
                        if (isset ($aImages [0]))
                        {
                                $aCampaigns [$k]['image'] = $aImages [0];   
                        }
                        else
                        {
                                $aCampaigns [$k]['image'] = '';    
                        }
                        
                        $aImages = glob (substr ($aCampaigns [$k]['photos'][0]['pathphoto'], 1) . '/' . $aCampaigns [$k]['photos'][0]['id'] . '_240x73*');  
                    
                        if (isset ($aImages [0]))
                        {
                                $aCampaigns [$k]['small_image'] = $aImages [0];   
                        }
                        else
                        {
                                $aCampaigns [$k]['small_image'] = '';    
                        }
                }
                
                $this -> layout -> assign ('page', $aParent); 
                $this -> layout -> assign ('campaigns', $aCampaigns);  
                $this -> layout -> assign ('page_name', $this -> request[2]);
                $this -> layout -> assign ('type', 1); 
                $this -> layout -> assign ('page_description', (!empty ($aParent ['page_description']) ? $aParent ['page_description'] : $this -> sIndexDescription));
                $this -> layout -> assign ('page_title', $aParent);
                $this -> layout -> assign ('content', 'Templates/default.tpl');   
        }
        
        /**
        * @desc contact page
        */
        
        private function onRequestContact ()
        {
                $pPage = new \ Page();
                
                $aPage = $pPage -> getNodeData (0, 'contact');     
                
                if ($_SERVER ['REQUEST_METHOD'] == 'POST' && count ($_POST) >= 2)
                {
                        $sName    = filter_var ($_POST ['name'], FILTER_SANITIZE_SPECIAL_CHARS);
                        $sEmail   = filter_var ($_POST ['email'], FILTER_VALIDATE_EMAIL);
                        $sMessage = filter_var ($_POST ['message'], FILTER_SANITIZE_SPECIAL_CHARS);
                        
                        setlocale (LC_TIME, 'nl_NL');
                        
                        if ($sName !== false && $sEmail !== false && $sMessage !== false)
                        {
                                try
                                {
                                        require INFORITUS_BASE . '/cms/includes/Mail.php';  
                                        
                                        $pBody = new \ Smarty ();
                
                                        $pBody  ->  template_dir = $_SERVER['DOCUMENT_ROOT'].'/Visual/';
                                        $pBody  ->  compile_dir  = $_SERVER['DOCUMENT_ROOT'].'/Cache/Templates/';
                                        $pBody  ->  cache_dir    = $_SERVER['DOCUMENT_ROOT'].'/Cache/cache/'; 
                                        
                                        $pBody -> caching = false;
                                        $pBody -> assign (array
                                        (
                                                'name'         => $sName,
                                                'email'        => $sEmail,
                                                'message'      => $sMessage
                                        ));
                                        
                                        $pMail = \ Mail :: createMessage ();
                                        $pMail -> setSender ($sName, $sEmail);
                                        $pMail -> setRecipient ('andrei@inforitus.nl');
                                        $pMail -> setXMailer ('Site/1.0');
                                        $pMail -> setSubject ('E-mail van ' . $sName);
                                        $pMail -> setBody ($pBody -> fetch ('Templates/mail-contact.tpl'), \ MailMessage:: CONTENT_TYPE_HTML);
                                        
                                        \ Mail :: sendMessage ($pMail);
                                        
                                }
                                catch (Exception $e) { }
                        }
                
                }
                
                $this -> layout -> assign ('page_name', 'contact'); 
                $this -> layout -> assign ('page_description', (!empty ($aPage ['page_description']) ? $aPage ['page_description'] : $this -> sIndexDescription));
                $this -> layout -> assign ('page_title', $aPage ['name']);  
                $this -> layout -> assign ('content', 'Templates/contact.tpl');  
        }
        
};

?>