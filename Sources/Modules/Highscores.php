<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id$
 */

namespace Inforitus \ Modules;
use \ Inforitus \ Kernel \ Template; 
        
class Highscores extends \ Inforitus \ Modules \ Router
{
        
        /**
         * The method which will be invoked when a request gets triggered for this class is,
         * how did you guess, onRequest. The dispatcher calls it after initializing some of
         * the required properties so we'll be able to run properly.
         */
        
        public function onRequest ()
        {       
            $oPage       = new \ Page();
            $aParent     = $oPage -> getNodeData (0, 'highscores');

            require INFORITUS_BASE . '/cms/includes/Cyclist.class.php';    
            $ciclists = new \ Cyclist();

            $aHighscores = $ciclists -> getTodayScores();
            
            foreach ($aHighscores as $key => $value) {
                $aHighscores[$key]["total_timer"] = $aHighscores[$key]["total_timer"]/100;
            }
            //echo "<pre>".print_r($aHighscores,true);die();


            if ($ciclists -> isSomeonePlaying(1))// && !isset($_SESSION['current_playing_cyclist_name'])
            {
                $this -> layout -> assign ('error_text', 'someone is playing now.. come later please');
            }

            
            $this -> layout -> assign ('content', 'Templates/highscores-mobile.tpl');
            $this -> layout -> assign ('highscores', $aHighscores);

            $this -> layout -> assign ('page_name', 'highscores'); 
            $this -> layout -> assign ('type', 0); 
            $this -> layout -> assign ('page', $aParent);
            $this -> layout -> assign ('page_title', $aParent ['name']);       
                       
        }
        
};

?>