<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id$
 */

namespace Inforitus \ Modules;
use \ Inforitus \ Kernel \ Template; 
        
class fw02 extends \ Inforitus \ Modules \ Router
{
        
        /**
         * The method which will be invoked when a request gets triggered for this class is,
         * how did you guess, onRequest. The dispatcher calls it after initializing some of
         * the required properties so we'll be able to run properly.
         */
        
        public function onRequest ()
        {       

                $this -> layout -> assign ('device_type', '');

            $this -> layout -> assign ('content', 'Templates/fw02.tpl');


            $oPage       = new \ Page();
            $aParent     = $oPage -> getNodeData (0, 'index');
            
            
            
            $this -> layout -> assign ('page_name', 'index'); 
            $this -> layout -> assign ('type', 0); 
            $this -> layout -> assign ('page', $aParent);
            $this -> layout -> assign ('page_title', $aParent ['name']);       
                       
        }
        
};

?>