<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id: Session.php
 */

namespace Inforitus \ Kernel;

/**
 * Sessions are PHP's way of remembering who you are. By default you're a new person
 * every time you visit a page on Inforitus, but in case of logging in that's not
 * a real convenience. Therefore we store everything in the server's memory.
 *
 * @package kernel
 */
class Session
{
        /**
         * Private encryption key to use with the RSA encryption we apply over the session,
         * which will majorly increase applied security.
         *
         * @var string
         * @ignore
         */
        
        const   SESSION_ENCRYPTION_PRIVATE_KEY  = 'r0oRNd0osj3';
        
        /**
         * This constant defines the interval between requests that has to be kept in mind
         * when re-generating the sessions. This is another measure to increase performance
         * for the Inforitus website. Default value will be something like five.
         *
         * @var integer
         */
        
        const   SESSION_REGENERATION_INTERVAL = 5;
        
        /**
         * This member contains the active instance of the cache handler used for
         * VideoBankOnline, which will either be Memcached or APC. All storing and
         * retreiving session data will be done through this class. 
         *
         * @var Cache
         */
        
        private $m_pCache;
        
        /**
         * A boolean value to indicate whether the Session Id has to be re-generated,
         * which is another system to increase performance for the sessions.
         *
         * @var boolean
         */
        
        private $m_bRegenerateId;
        
        /**
         * This array contains the raw data which will be used to read- or parse the
         * contents of this session, in a decrypted form. Only usable by this class!
         *
         * @var array
         */
        
        private $m_aRawSessionData;
        
        /**
         * In order to properly encode- and/or decode session data, the encryption
         * mechanism requires an initialisation vector (IV). This property will the
         * instance of the vector. In order to generate it, the XTEA encryption way
         * will be used following an ECB procedure, with a pseudo-random size.
         *
         * @var object
         */
        
        private $m_pInitVector;
        
        /**
         * Because the PHP shutdown-order is quite fucked up, we're storing the used
         * encryption key in a local property so it's accessable later on.
         *
         * @var string
         */
        
        private $m_sEncryptionKey;
        
        /**
         * The Session regeneration key contains the sequenced number of this request
         * modulos SESSION_REGENERATION_INTERVAL. We generate the session id every
         * few requests to make sure no unauthorized access gets obtained through
         * session stealing, well, this makes it at least extremely difficult.
         *
         * @var integer
         */
        
        private $m_nRegenerateKey;
        
        /**
         * In order to properly support the push- and pop methods for this session,
         * we need to be able to retreive the session instance later on in this script.
         * That's why we're saving the variable in a static var.
         *
         * @var Session
         */
        
        private static $m_spSession;
        
        /**
         * Starting a new session is something that can be done using this method.
         * The workins are quite simple, start up a new instance of this class,
         * use session_set_save_handler to override certain methods.
         *
         * @param Cache $pCache Instance of the cache-manager to use.
         * @return boolean A boolean whether the session could be started or not.
         */
        
        public static function start (Cache $pCache)
        {
                $pSession  = new self;
                $pSession -> m_pCache = $pCache;
                $pSession -> m_bRegenerateId = false;
                $pSession -> m_pInitVector = mcrypt_create_iv (mcrypt_get_iv_size (MCRYPT_XTEA, MCRYPT_MODE_ECB), MCRYPT_RAND);
                $pSession -> m_nRegenerateKey = 0;
                
                $pSession -> m_sEncryptionKey = self :: SESSION_ENCRYPTION_PRIVATE_KEY;
                
                session_set_save_handler
                (
                        function () { return true; },           // open
                        function () { return true; },           // close
                        array ($pSession, 'onSessionRead'),     // read
                        array ($pSession, 'onSessionWrite'),    // write
                        array ($pSession, 'onSessionDestroy'),  // destroy
                        function () { return true; }            // garbage
                );
                
                /** avoid throwing useless errors by making sure this exists **/
                if (!isset ($_SERVER ['HTTP_USER_AGENT']))
                {
                        $_SERVER ['HTTP_USER_AGENT'] = 'unknown';
                }
                
                session_start ();
                
                if ($pSession -> m_bRegenerateId === true)
                {
                        /** Should the session id be regenerated in favor of security? **/
                        session_regenerate_id (true);
                }
                
                self :: $m_spSession = $pSession;
        }
        
        /**
         * This method will read and decrypt the contents of this session, well,
         * top layer of the active session. This new system is able to support
         * push() and pop() methods, which mainly are convenient to implement the
         * features required to log in as another user.
         *
         * @param string $sSessionId Unique ID of the active session.
         * @returns string Serialized contents of this session.
         */
        
        public function onSessionRead ($sSessionId)
        {
                $aEncryptedSessionData = $this -> m_pCache -> get ('ses_' . $sSessionId);
                if ($aEncryptedSessionData === false)
                {
                        if ($this -> m_pCache -> getDaemon () -> getResultCode () == \ Memcached :: RES_NOTFOUND)
                        {
                                /** The session has not been created yet, so there's nothing to report **/
                                return '';
                        }
                        else
                        {
                                /**
                                 * Apparently there is a problem with the memory caching daemon
                                 * that currently is being used. This means that the website cannot
                                 * continue normal opteration, therefore we kill everything.
                                 */
                                
                                KernelError :: ThrowError ('A fatal error occurred while loading the session data');
                        }
                }
                
                $sVerificationKey = sha1 ($_SERVER ['HTTP_USER_AGENT'] . $_SERVER ['REMOTE_ADDR'] . self :: SESSION_ENCRYPTION_PRIVATE_KEY);
                if (!isset ($aEncryptedSessionData ['verification_key']) || $aEncryptedSessionData ['verification_key'] != $sVerificationKey ||
                    !isset ($aEncryptedSessionData ['inforitus_version']) || $aEncryptedSessionData ['inforitus_version'] != INFORITUS_VERSION)
                {
                        /** The verification key could not be applied, odd, possible hijack? **/
                        $this -> onSessionDestroy ($sSessionId); 
                        return '';
                }
                
                if (!isset ($aEncryptedSessionData ['data']) || !is_string ($aEncryptedSessionData ['data']))
                {
                        /** No session has been started yet for this subdomain, therefore we can't return it. **/
                        return '';
                }
                
                $sSessionData = mcrypt_decrypt (MCRYPT_XTEA, self :: SESSION_ENCRYPTION_PRIVATE_KEY, $aEncryptedSessionData ['data'], MCRYPT_MODE_ECB, $this -> m_pInitVector);
                if (is_string ($sSessionData) && strlen ($sSessionData) > 0)
                {
                        $this -> m_aRawSessionData = unserialize ($sSessionData);
                        $this -> m_nRegenerateKey = $aEncryptedSessionData ['regenerate_key'];
                        
                        /** Set the regeneration flag on this session if required **/
                        if (++ $this -> m_nRegenerateKey > self :: SESSION_REGENERATION_INTERVAL)
                        {
                                $this -> m_nRegenerateKey = 1;
                                $this -> m_bRegenerateId  = true ;
                        }
                        
                        if (!isset ($this -> m_aRawSessionData [$_SERVER ['SERVER_NAME']]) || count ($this -> m_aRawSessionData [$_SERVER ['SERVER_NAME']]) == 0)
                        {
                                /** No information could be retreived about the active session **/
                                return '';
                        }
                        
                        return array_pop ($this -> m_aRawSessionData [$_SERVER ['SERVER_NAME']]);
                }
                
                /** The data could not be decrypted for some reason.. **/
                return '';
        }
        
        /**
         * In order to update the session's information, this method may be called. Fortunately
         * this is a job that PHP will do by default, so you don't have to worry
         * about it. All contents will be generated all over again and stored into the
         * memory cache, available through the Cache handler (right now: Memcached).
         *
         * @param string $sSessionId Unique ID of the active session.
         * @param string $sSessionData Current information of the session, as a string.
         */
        
        public function onSessionWrite ($sSessionId, $sSessionData)
        {
                $sVerificationKey = sha1 ($_SERVER ['HTTP_USER_AGENT'] . $_SERVER ['REMOTE_ADDR'] . $this -> m_sEncryptionKey);
                $aSessionBaseInformation = array
                (
                        'verification_key'      => $sVerificationKey,
                        'regenerate_key'        => $this -> m_nRegenerateKey,
                        'inforitus_version'     => INFORITUS_VERSION,
                        
                        'data'                  => $this -> m_aRawSessionData
                );
                
                /** Did the session load properly, or are we in a new session? **/
                if (!isset ($aSessionBaseInformation ['data'] [$_SERVER ['SERVER_NAME']]) || !is_array ($aSessionBaseInformation ['data'] [$_SERVER ['SERVER_NAME']]))
                {
                        $aSessionBaseInformation ['data'] [$_SERVER ['SERVER_NAME']] = array ();
                }
                
                /** Store the information for this session in the appropriate datastore **/
                $aSessionBaseInformation ['data'] [$_SERVER ['SERVER_NAME']] [] = $sSessionData;
                
                /** Encrypt the session and store it in the cache repository **/
                $aSessionBaseInformation ['data'] = mcrypt_encrypt (MCRYPT_XTEA, $this -> m_sEncryptionKey, serialize ($aSessionBaseInformation ['data']), MCRYPT_MODE_ECB, $this -> m_pInitVector);
                $this -> m_pCache -> set ('ses_' . $sSessionId, $aSessionBaseInformation, 86400);
                
                return true ;
        }
        
        /**
         * When a session will be destroyed, this is the method that will be
         * invoked. Mind that destroying a session means that ALL LEVELS will
         * be removed, different from just the top-level information.
         *
         * @param string $sSessionId Unique ID of the active session.
         */
        
        public function onSessionDestroy ($sSessionId)
        {
                $this -> m_pCache -> delete ('ses_' . $sSessionId);
        }
        
        /**
         * In order to properly support multi-level sessions, we need to have a
         * push method. This method pushes all current data back on the stack and
         * opens up a new instance, while keeping the old data available in the
         * background. This is required for the log-in as user features.
         * 
         * @return boolean Could the session be pushed up the stack?
         */
        
        public static function push ()
        {
                self :: $m_spSession -> m_aRawSessionData [$_SERVER ['SERVER_NAME']] [] = serialize ($_SESSION);
                
                return $_SESSION = array () ;
        }
        
        /**
         * Obviously we would How and like to access older entries as well, which can be
         * done using this method. The session depth will be decreased by one after
         * calling this method, unless it's already zero (or lower).
         * 
         * @return boolean Could an older session be retreived from the stack?
         */
        
        public static function pop ()
        {
                if (self :: getSessionDepth () > 1)
                {
                        $_SESSION = unserialize (array_pop (self :: $m_spSession -> m_aRawSessionData [$_SERVER ['SERVER_NAME']]));
                        return true ;
                }
                else
                {
                        $_SESSION = array ();
                        return false ;
                }
        }
        
        /**
         * Considering we support sessions with various depths, it'd be fairly
         * convenient to be able to decide the depth of the current session. This
         * method returns exactly that depths as an integer.
         *
         * @return integer Current depth of the active session (1..N).
         */
        
        public static function getSessionDepth ()
        {
               return count (self :: $m_spSession -> m_aRawSessionData [$_SERVER ['SERVER_NAME']]) + 1;
        }
        
};

?>