<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id: Kernel.php 
 */

namespace Inforitus \ Kernel;

require __DIR__ . '/KernelError.php';
require __DIR__ . '/Cache.php';
require __DIR__ . '/Session.php';

 /**
  * The Inforitus Kernel registers error handling methods, starts the session-
  * and caching managers and initializes an auto-loader for required classes.
  *
  * @package kernel
  */
class Kernel
{
        /**
         * The Cache Manager is the place where we make sure that things don't get
         * logged up by recalculating things over and over again. Right now we're
         * using Memcached for memory-based storage locally on this server.
         *
         * @var Cache
         */
        
        private static $m_spCache;
        
        /**
         * Initializing the Kernel, and with it the different components of it,
         * can be done using this method. It's static and should only be called
         * once: as the first real PHP execution in index.php.
         */
        
        public static function Initialize ()
        {                              
                date_default_timezone_set ('Europe/Amsterdam');
                error_reporting           (E_ALL | E_NOTICE | E_STRICT);
                
                set_exception_handler (array (__NAMESPACE__ . '\\Kernel', 'onUnhandledException'));
                set_error_handler     (array (__NAMESPACE__ . '\\Kernel', 'onUnhandledError'));
                
                spl_autoload_register (function ($sClassName)      
                {       
                        $aClassPath = explode ('\\', $sClassName);
                        
                        if (array_shift ($aClassPath) == 'Inforitus')
                        {          
                                $sClassPackage = array_shift ($aClassPath);
                                switch ($sClassPackage)
                                {
                                        /**
                                         * Loading kernel packages is quite easy, they're located in the
                                         * Sources folder of the website, so simply require the files.
                                         */
                                        
                                        case 'Kernel':
                                        {        
                                                
                                                
                                                $sFilename = __DIR__ . '/' . $aClassPath[0] . '.php';
                                                
                                                if (file_exists ($sFilename))
                                                {
                                                        require $sFilename;
                                                        return ;
                                                }
                                                
                                        }
                                }
                        }
                        
                        /**
                         * Even after auto-loading, the class could not be found. Let's display
                         * a fatal error page so the user at least gets the Inforitus layout.
                         */
                        
                        KernelError :: ThrowError ('An important component (' . str_replace ('\\', '::', $sClassName) . ') of Inforitus could not be loaded.');
                });
                
                self :: $m_spCache = new Cache ();
                
                session :: start (self :: $m_spCache);
        }
        
        /**
         * In case some component of Inforitus throws an exception which
         * cannot be handled properly, this method will be called automatically.
         * Depending on whether we're in debug mode or not, either a fatal error
         * page will be shown or it'll be displayed in the debug context.
         *
         * @param \Exception The exception which is being thrown.
         */
        
        public static function onUnhandledException ($pException)
        {
                KernelError :: ThrowError ($pException -> getMessage ());
        }
        
        /**
         * Obviously it's a possibility that something breaks and the site throws an error.
         * Considering we do not want to bother visitors with this, they get stashed in a
         * database and only become visible when we're in the administration section, or,
         * if the website is running in debug mode, they get thrown in the debug context.
         *
         * @param integer $nErrorNumber Integer representation of the error being thrown.
         * @param string $sErrorString Textual representation of what went wrong.
         * @param string $sErrorFile Absolute location of the file which triggered the error.
         * @param integer $nErrorLine Position in the file which triggered the error.
         * @param array $aErrorContext Variable context in which the error got thrown.
         */
        
        public static function onUnhandledError ($nErrorNumber, $sErrorString, $sErrorFile, $nErrorLine, $aErrorContext)
        {
                if (error_reporting () == 0)
                {
                        /** this error has been supressed for some reason, return ; **/
                        return ;
                }
                
                KernelError :: ThrowError (str_replace (INFORITUS_BASE . '/', '', $sErrorString) . ' (' .substr (str_replace (INFORITUS_BASE, '', $sErrorFile), 1) . ':' . $nErrorLine . ')');
        }
        
        /**
         * For modules it's convenient to be able to use the Caching systems. This method will
         * return the active Caching Manager for the current request, by default this will be
         * a proxy around either Memcached or PHP's own APC.
         * 
         * @return Cache Returns the active instance of the Cache manager.
         */
        
        public static function getCacheManager ()
        {
                return self :: $m_spCache;
        }
        
};

?>