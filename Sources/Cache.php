<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id: Cache.php 
 */

namespace Inforitus \ Kernel;
 
 /**
  * The Cache class will regulate and normalize all calls being made towards the
  * Memory Cache daemon running on the server itself. Considering this might switch
  * to be APC later on, direct access isn't immediately wanted.
  *
  * @package kernel
  */
class Cache
{
        /**
         * In order to have the memcached variable available everywhere in this class,
         * this property stores the instance of the Memcached-client we're using.
         *
         * @var Memcached
         */
        
        private $m_pMemcached;
        
        /**
         * This property contains the suffix which will be added to any cache keys,
         * which ensures that no overlapping between different websites occurs. The
         * VideoBankOnline testing environment uses another suffix for this purpose
         * as well, so everything can be fucked up badly without anything breaking.
         * 
         * @var string
         */
        
        private $m_sKeySuffix;
        
        /**
         * The constructor will initialise a new instance of the Memcached class which will
         * arrange all communication with the server. Permanent connections are used seeing
         * that slightly speeds up the caching process.
         */
        
        public function __construct ()
        {
                $this -> m_pMemcached = new \ Memcached (\ Configuration :: MEMCACHED_DATA_PREFIX . 'instance');
                $this -> m_sKeySuffix = \ Configuration :: MEMCACHED_DATA_PREFIX;
                
                if (count ($this -> m_pMemcached -> getServerList ()) !== 1)
                {
                        $this -> m_pMemcached -> addServer (\ Configuration :: MEMCACHED_SERVER_IP, \ Configuration :: MEMCACHED_SERVER_PORT);
                        
                }
        }
        
        /**
         * In order to get information from the Cache handler, this method may be
         * used. We'll return the actual value or false incase it cannot be found.
         *
         * @param string $sKey Identifier for the value you wish to retrieve.
         * @param boolean $bIgnoreErrors Ignore it if the cache-server is unavailable.
         * @return mixed The cached value that's being retreived.
         */
        
        public function get ($sKey, $bIgnoreErrors = false)
        {
                $mResult = $this -> m_pMemcached -> get ($sKey . $this -> m_sKeySuffix);
                if ($bIgnoreErrors === false && $mResult === false)
                {
                        if ($this -> m_pMemcached -> getResultCode () == \ Memcached :: RES_UNKNOWN_READ_FAILURE ||
                            $this -> m_pMemcached -> getResultCode () == \ Memcached :: RES_ERRNO)
                        {
                                KernelError :: ThrowError ('The memory-cache daemon is unavailable');
                        }
                }
                
                return $mResult;
        }
        
        /**
         * The set method may be used to update or simply insert a variable in the
         * cache. This method communicates directly with the underlaying cache-
         * engine that we implement, regardless whether it is APC or Memcached.
         *
         * @param string $sKey Key of the value you wish to cache.
         * @param mixed $mValue Value that should be stored, identified by the key.
         * @param integer $nExpire Number of seconds which indicates when it should expire.
         * @return boolean An indication whether the value got stored or not.
         */
         
        public function set ($sKey, $mValue, $nExpire = 0)
        {
                return $this -> m_pMemcached -> set ($sKey . $this -> m_sKeySuffix, $mValue, $nExpire);
        }
        
        /**
         * It would be quite convenient to delete objects from the cache when required,
         * which is exactly what this method will be doing.
         *
         * @param string $sKey Session key which has to be removed from the repository.
         * @return boolean Did the key get deleted from the cache, or not?
         */
        
        public function delete ($sKey)
        {
                return $this -> m_pMemcached -> delete ($sKey);
        }
        
        /**
         * For more advanced checking, components are able to retreive the memory caching
         * daemon. Basically this method returns the Memcached instance.
         *
         * @todo Abstract this in some fancy way so we can easily switch caching methods.
         * @return Memcached Instance of the memcached daemon this class is using.
         */
        
        public function getDaemon ()
        {
                return $this -> m_pMemcached;
        }
};

?>