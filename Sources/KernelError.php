<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id: KernelError.php 
 */

namespace Inforitus \ Kernel;

 /**
  * Throwing fatal errors usually is an inconvenience, especially for the visitor,
  * however, we should show Inforitus-related screens whenever possible.
  *
  * @package kernel
  */
class KernelError 
{
        public function KernelError ()
        {
            
        }
        
        /**
         * Sharing information about fatal errors can be done through this method.
         * Basically all we do is handle the available information- and context and
         * show the error to the user, putting available information in the database.
         *
         * @param string $sErrorMessage Information about the fatal error for the visitor.
         */
        
        public static function ThrowError ($sErrorMessage)
        {
                /** clear active output handlers prior to continuing **/
                if (ob_get_level () > 0)
                {
                        for ($nLevel = ob_get_level (); $nLevel > 0; $nLevel --)
                        {
                                ob_end_clean ();
                        }
                }
                
                $sTemplate = file_get_contents (INFORITUS_BASE . '/Visual/layout_fatal.tpl');
                $sTemplate = str_replace ('{$message}', INFORITUS_DEBUG ? $sErrorMessage : '' ,$sTemplate);
                
                if (\ Configuration :: SAVE_ERROR) KernelError :: SaveError ($sErrorMessage);
                session_write_close ();
                
                die ($sTemplate);
        }
        
        /**
        * @desc send the error on admin mail
        * 
        * @param string $sErrorMessage 
        */
        
        public static function SaveError ($sErrorMessage)
        {           
                $pResult = \ Database :: getInstance () -> query ("
                        SELECT 
                            *
                        FROM 
                            errors
                        WHERE
                            url = '" . $_SERVER ['SERVER_NAME'] . $_SERVER ['REQUEST_URI'] . "'
                        AND
                            ip = '" . ip2long ($_SERVER ['REMOTE_ADDR']) . "'
                        AND
                            message = '" . $sErrorMessage . "'
                ");
                
                if (!$pResult)
                {
                        trigger_error (\ Database :: getInstance () -> error, E_USER_WARNING);
                        
                        return false;
                } 
                
                if ($pResult -> num_rows == 0)
                {
                        
                        $pResult = \ Database :: getInstance () -> query ("
                                INSERT INTO errors (url, ip, message)
                                VALUES ('" . $_SERVER ['SERVER_NAME'] . $_SERVER ['REQUEST_URI'] . "', " . ip2long ($_SERVER ['REMOTE_ADDR']) . ", '" . $sErrorMessage . "')
                        ");
                        
                        if (!$pResult)
                        {
                                trigger_error (\ Database :: getInstance () -> error, E_USER_WARNING);
                                return false;
                        } 
                }   
        }
        
};

?>