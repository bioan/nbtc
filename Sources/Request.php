<?php
/**
 * @copyright Copyright (c) 2008-2010, Inforitus V.O.F.
 * @author Andrei Dragos <andrei@inforitus.nl>
 * @version $Id: Request.php
 */

namespace Inforitus \ Kernel;
 
class Request implements \ ArrayAccess
{
        /**
         * The following constants define the types of requests which may occur on
         * the website. We support various kinds of output, and for the sake of having
         * completely useless statistics, might provide types like Excel-output too.
         */
        
        const   REQUEST_TYPE_HTML       = 1;
        const   REQUEST_TYPE_PDF        = 2;
        const   REQUEST_TYPE_EXCEL      = 3;
        const   REQUEST_TYPE_XML        = 4;
        const   REQUEST_TYPE_MXF        = 5;
        const   REQUEST_TYPE_MOV        = 6;
        const   REQUEST_TYPE_MPEG       = 7;
        
        /**
         * This property defines the components of the request that's being invoked by
         * the user. Routing is mainly based on this. The contents get set using the
         * setRequest method automatically, so it'll always be filled.
         *
         * @var array
         */
        
        private $m_aRequest;
        
        /**
         * This var defines the type of file that's being requested by the visitor,
         * which can be any of the earlier defined constants in this class. The file-
         * type detection gets done in the setRequest method.
         * 
         * @var integer
         */
        
        private $m_nFileType;
        
        /**
         * Are we currently viewing a page using the API, or directly using a normal
         * page? Just a boolean flag, gets determined and set using the setRequest
         * method, and can be retreived as a class property named "api".
         * 
         * @var boolean
         */
        
        private $m_bApi;
        
        /**
         * In the constructor we'll initialize the Request class with information about
         * what we're actually requesting. Things like the file-type and request path
         * are fairly easy to determine, so let's do that.
         *
         * @param string $sRequest The request that's being made by the visitor.
         */
        
        public function __construct ($sRequest)
        {
                $this -> setRequest ($sRequest);
        }
        
        /**
         * Setting the request for this page can be done using this method. It will parse
         * the given URL in chunks and the filetype, enabling us to route a request.
         *
         * @param string $sRequest The request that should be set
         */
        
        public function setRequest ($sRequest)
        {
                $aRequest = explode ('?', substr ($sRequest, 1));
                $this -> m_aRequest = explode ('/', $aRequest [0]);
                $this -> m_bApi = false ;
                
                if ($this -> m_aRequest [0] == 'api')
                {
                        array_shift ($this -> m_aRequest);
                        $this -> m_bApi = true ;
                }
                
                $sFileType = end ($this -> m_aRequest);
                if (is_string ($sFileType) && strlen ($sFileType) > 0 && strpos ($sFileType, '.') !== false)
                {
                        switch (strtolower (strrchr ($sFileType, '.')))
                        {
                                case '.html': { $this -> m_nFileType = self :: REQUEST_TYPE_HTML;       break; }
                                case '.xml':  { $this -> m_nFileType = self :: REQUEST_TYPE_XML;        break; }
                                case '.pdf':  { $this -> m_nFileType = self :: REQUEST_TYPE_PDF;        break; }
                                case '.xls':  { $this -> m_nFileType = self :: REQUEST_TYPE_EXCEL;      break; }
                                case '.xlsx': { $this -> m_nFileType = self :: REQUEST_TYPE_EXCEL;      break; }
                                case '.mxf':  { $this -> m_nFileType = self :: REQUEST_TYPE_MXF;        break; }
                                case '.mov':  { $this -> m_nFileType = self :: REQUEST_TYPE_MOV;        break; }
                                case '.mp4':  { $this -> m_nFileType = self :: REQUEST_TYPE_MPEG;       break; }
                                
                                default:
                                {
                                        $this -> m_nFileType = self :: REQUEST_TYPE_HTML;
                                        break;
                                }
                        }
                }
                else
                {
                        $this -> m_nFileType = self :: REQUEST_TYPE_HTML;
                }
                
                /** quite dirty, but working just fine **/
                $this -> m_aRequest [count ($this -> m_aRequest) - 1] = substr (end ($this -> m_aRequest), 0, strrpos (end ($this -> m_aRequest), '.'));
        }
        
        /**
         * Getting a part of the request can be done using this method, which will safely
         * return the value associated with the offset in our local request array.
         *
         * @param integer $nIndex Index of the parameter you wish to retreive.
         * @return mixed The value associated with the index as requested.
         */
        
        public function offsetGet ($nIndex)
        {
                if (isset ($this -> m_aRequest [$nIndex]))
                {
                        return $this -> m_aRequest [$nIndex];
                }
                
                return false ;
        }
        
        /**
         * Getting to know certain information about the active request can be done using this
         * method, which will supply property access functionalities to this class. Various
         * properties will be available, such as "type" to get the request filetype.
         *
         * @param string $sProperty Name of the property that's being requested.
         * @return mixed Value associated with the property, can be anything really.
         */
        
        public function __get ($sProperty)
        {
                switch ($sProperty)
                {
                        case 'type':
                        {
                                return $this -> m_nFileType;
                        }
                        
                        case 'api':
                        {
                                return $this -> m_bApi;
                        }
                        
                        case 'extension':
                        {
                                switch ($this -> m_nFileType)
                                {
                                        case self :: REQUEST_TYPE_PDF:          return 'pdf';
                                        case self :: REQUEST_TYPE_EXCEL:        return 'xls';
                                        case self :: REQUEST_TYPE_XML:          return 'xml';
                                        case self :: REQUEST_TYPE_MXF:          return 'mxf';
                                        case self :: REQUEST_TYPE_MOV:          return 'mov';
                                        case self :: REQUEST_TYPE_MPEG:         return 'mp4';
                                        default:
                                        {
                                                return 'html';
                                        }
                                }
                        }
                }
                
                return false ;
        }
        
        /**
         * Checking whether the request-tree is empty, or default, can be done by this
         * method. We check for a few common entry-cases.
         *
         * @return boolean Is this an empty request-tree, or not?
         */
        
        public function isEmpty ()
        {
                if ($this -> request [0] == false || ($this -> request [1] == false && $this -> request [0] == 'index'))
                        return true ;
                
                return false ;
        }
        
        /**
         * Checking whether an input offset exists may be done using this method, which 
         * will effectively return a similar check on the aRequest member.
         *
         * @param integer $nIndex Index of the parameter you wish to retreive.
         * @return boolean Indicator whether the offset exists or not.
         */
        
        public function offsetExists ($nIndex)
        {
                return isset ($this -> m_aRequest [$nIndex]);
        }
        
        /**
         * Setting an offset usually can be done using this method. Odd enough however we
         * don't want this, so simply want to block the request.
         *
         * @param integer $nIndex Index of the parameter that should be set.
         * @param mixed $mValue Value of the variable associated with the index.
         */
        
        public function offsetSet ($nIndex, $mValue)
        {
                // void
        }
        
        /**
         * Unsetting a value can be done using this method. Considering we don't want to do
         * exactly this, it simply catches the call and voids it.
         *
         * @param integer $nIndex Index of the parameter that should be unset.
         */
        
        public function offsetUnset ($nIndex)
        {
                // void
        }
        
        /**
         * Getting a value out of the POST array can be done using this method, which
         * is also capable of filtering out the data as requested by the caller.
         *
         * @param string $sKey Key of the field you wish to retreive.
         * @param integer $nValidation Type of validation that should be applied.
         * @return mixed Value associated with the key if it exists, or false
         */
        
        public function post ($sKey, $nValidation = -1)
        {
                if (!isset ($_POST [$sKey]))
                        return false ;
                
                $sValue = $_POST [$sKey];
                if ($nValidation != -1)
                {
                        $sValue = filter_var ($sValue, $nValidation);
                }
                
                return $sValue;
        }
        
        /**
         * We're able to convert back from a request object to a string, which will mainly
         * be used for dispatching-reasons. Preference for this, however, is to copy over
         * the Request object using a reference.
         *
         * @return string Absolute request path including the extension.
         */
        
        public function __toString ()
        {
                $aExtensions = array
                (
                        self :: REQUEST_TYPE_HTML       => 'html',
                        self :: REQUEST_TYPE_PDF        => 'pdf',
                        self :: REQUEST_TYPE_EXCEL      => 'xlsx',
                        self :: REQUEST_TYPE_XML        => 'xml'
                );
                
                return '/' . implode ('/', $this -> m_aRequest) . '.' . $aExtensions [$this -> m_nFileType];
        }
}

?>